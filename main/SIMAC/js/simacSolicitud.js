let tblDocumentosSolicitante,
    tblReferenciasSolicitante,
    tblAditivos,
    tblGarantias;

window.onload = () => {
    configSolicitud.id = encodeURIComponent(getParameterByName('id'));
    $(".preloader").find("span").html("Obteniendo datos de solicitud");
    $(".preloader").fadeIn("fast");
    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "SIMAC",
                archivo: "simacGetCats",
                solicitados: ["tiposDocumento", "geograficos", "actividadEconomica", "lineasFinancieras", "tiposPlazo", "periodicidades", "fuentesFondo", "aditivos", "tiposReferencia", "tiposGarantia"]
            }).then((datosRespuesta) => {
                try {
                    let opciones = [`<option selected disabled value="">seleccione</option>`];
                    if (datosRespuesta.tiposDocumento) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboTipoDocumento");

                        datosRespuesta.tiposDocumento.forEach(tipo => {
                            tiposDocumento[tipo.id] = tipo;
                            tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.lineasFinancieras) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboLineaFinanciera");

                        datosRespuesta.lineasFinancieras.forEach(linea => {
                            tmpOpciones.push(`<option value="${linea.id}">${linea.lineaFinanciera}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.tiposPlazo) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboTipoPlazo");

                        datosRespuesta.tiposPlazo.forEach(tipo => {
                            tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoPlazo}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.periodicidades) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboPeriodicidad");

                        datosRespuesta.periodicidades.forEach(periodicidad => {
                            tmpOpciones.push(`<option value="${periodicidad.id}">${periodicidad.periodicidad}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.fuentesFondo) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboFuenteFondos");

                        datosRespuesta.fuentesFondo.forEach(fuente => {
                            tmpOpciones.push(`<option value="${fuente.id}">${fuente.fuenteFondos}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.actividadesEconomicas) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboActividadEconomica");

                        datosRespuesta.actividadesEconomicas.forEach(actividad => {
                            tmpOpciones.push(`<option value="${actividad.id}">${actividad.actividadEconomica}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.aditivos) {
                        datosRespuesta.aditivos.forEach(aditivo => {
                            configDatosCredito.configCuota.configAditivos.catalogo[aditivo.id] = {
                                ...aditivo
                            };
                        });
                    }

                    if (datosRespuesta.tiposReferencia) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboTipoReferencia");

                        datosRespuesta.tiposReferencia.forEach(tipo => {
                            tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoReferencia}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.tiposGarantia) {
                        datosRespuesta.tiposGarantia.forEach(tipo => {
                            configGarantia.catalogo[tipo.id] = {
                                ...tipo
                            };
                        });
                    }

                    if (datosRespuesta.datosGeograficos) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboPais");

                        datosGeograficos = {
                            ...datosRespuesta.datosGeograficos
                        };

                        for (let key in datosGeograficos) {
                            tmpOpciones.push(`<option value="${datosGeograficos[key].id}">${datosGeograficos[key].nombrePais}</option>`);
                        }

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            }).catch(reject);
        });
    }).catch(generalMostrarError);
    const gettingSolicitud = gettingCats.then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.get({
                modulo: "SIMAC",
                archivo: "simacGetSolicitud",
                params: {
                    q: configSolicitud.id
                }
            }).then((datosRespuesta) => {
                configDatosSolicitante.init(datosRespuesta.datosSolicitante).then(() => {
                    configDatosCredito.init({
                        ...datosRespuesta.datosCredito,
                        fechaRecepcion: datosRespuesta.solicitud.fechaRecepcion,
                        nombreAgencia: datosRespuesta.solicitud.nombreAgencia
                    }).then(resolve).catch(reject);
                }).catch(reject);
            }).catch(reject);
        });
    }).catch(generalMostrarError);

    gettingSolicitud.then(() => {
        let allSelectPickerCbos = document.querySelectorAll("select.selectpicker");
        allSelectPickerCbos.forEach(cbo => {
            $("#" + cbo.id).selectpicker("refresh");
        });
        $(".preloader").fadeOut("fast");
        $(".preloader").find("span").html("")
    }).catch(generalMostrarError);

}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 250;
            if ($("#tblDocumentosSolicitante").length) {
                tblDocumentosSolicitante = $("#tblDocumentosSolicitante").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                            "targets": [0],
                            "orderable": false,
                        },
                        {
                            "targets": [1],
                            "className": "text-start"
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblDocumentosSolicitante.columns.adjust().draw();
            }

            if ($("#tblReferenciasSolicitante").length) {
                tblReferenciasSolicitante = $("#tblReferenciasSolicitante").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                            "targets": [0],
                            "orderable": false,
                        },
                        {
                            "targets": [1],
                            "className": "text-start"
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblReferenciasSolicitante.columns.adjust().draw();
            }

            if ($("#tblAditivos").length) {
                tblAditivos = $("#tblAditivos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de producto';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblAditivos.columns.adjust().draw();
            }

            if ($("#tblGarantias").length) {
                tblGarantias = $("#tblGarantias").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de producto';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblGarantias.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configSolicitud = {
    id: 0,
}

const configDatosSolicitante = {
    codigoCliente: document.getElementById("txtCodigoCliente"),
    tipoDocumento: document.getElementById("cboTipoDocumento"),
    numeroDocumento: document.getElementById("txtNumeroDocumento"),
    nit: document.getElementById("txtNit"),
    nombres: document.getElementById("txtNombres"),
    apellidos: document.getElementById("txtApellidos"),
    conocidoPor: document.getElementById("txtConocido"),
    pais: document.getElementById("cboPais"),
    departamento: document.getElementById("cboDepartamento"),
    municipio: document.getElementById("cboMunicipio"),
    direccion: document.getElementById("txtDireccion"),
    profesion: document.getElementById("txtProfesion"),
    fechaNacimiento: document.getElementById("txtFechaNacimiento"),
    edad: document.getElementById("numEdad"),
    sabeFirmar: document.getElementById("cboFirma"),
    telefonoMovil: document.getElementById("txtMovil"),
    telefonoFijo: document.getElementById("txtFijo"),
    email: document.getElementById("txtEmail"),
    formulario: document.getElementById("frmSolicitante"),
    actividadEconomica: document.getElementById("cboActividad"),
    configDocumentos: {
        id: 0,
        cnt: 0,
        documentosAdjuntos: {},
        init: function (tmpDatos) {
            return new Promise((resolve, reject) => {

            });
        },
        edit: function (id = 0) {},
        save: function () {

        },
        addRowTbl: function () {
            return new Promise((resolve, reject) => {

            });
        },
        delete: function () {

        }
    },
    configReferencias: {
        id: 0,
        cnt: 0,
        cboTipoReferencia: document.getElementById("cboTipoReferencia"),
        campoNombre: document.getElementById("txtNombreReferencia"),
        campoParentesco: document.getElementById("txtParentescoReferencia"),
        campoDireccion: document.getElementById("txtDireccionReferencia"),
        campoLugarTrabajo: document.getElementById("txtTrabajoReferencia"),
        campoTelefono: document.getElementById("txtTelefonoReferencia"),
        campoComentarios: document.getElementById("txtComentariosReferencia"),
        referencias: {},
        init: function (tmpDatos) {
            return new Promise((resolve, reject) => {

            });
        },
        edit: function (id = 0) {
            formActions.clean("mdlReferencia").then(() => {
                const leyendo = new Promise((resolve, reject) => {
                    try {
                        this.id = id;
                        if (this.id > 0) {
                            let tmp = {
                                ...this.referencias[this.id]
                            };
                            this.cboTipoReferencia.value = tmp.idTipoReferencia;
                            this.campoNombre.value = tmp.nombre;
                            this.campoParentesco.value = tmp.parentesco;
                            this.campoDireccion.value = tmp.direccion;
                            this.campoLugarTrabajo.value = tmp.lugarTrabajo;
                            this.campoTelefono.value = tmp.telefono;
                            this.campoComentarios.value = tmp.comentarios;
                            resolve();
                        } else {
                            resolve();
                        }
                    } catch (err) {
                        reject(err.message);
                    }
                });

                leyendo.then(() => {
                    $("#" + this.cboTipoReferencia.id).selectpicker("refresh");
                    $("#mdlReferencia").modal("show");
                }).catch(generalMostrarError);
            });
        },
        save: function () {
            formActions.validate('frmReferencia').then(({
                errores
            }) => {
                if (errores == 0) {
                    let tmp = {
                        id: this.id > 0 ? this.referencias[this.id].id : 0,
                        idTipoReferencia: this.cboTipoReferencia.value,
                        tipoReferencia: this.cboTipoReferencia.options[this.cboTipoReferencia.options.selectedIndex].text,
                        nombre: this.campoNombre.value,
                        parentesco: this.campoParentesco.value,
                        direccion: this.campoDireccion.value,
                        lugarTrabajo: this.campoLugarTrabajo.value,
                        telefono: this.campoTelefono.value,
                        comentarios: this.campoComentarios.value,
                    };

                    const guardando = new Promise((resolve, reject) => {
                        try {
                            if (this.id == 0) {
                                let agregando = this.addRowTbl({
                                    ...tmp
                                });
                                agregando.then(resolve).catch(reject);
                            } else {
                                $("#trReferencia" + this.id).find("td:eq(1)").html(tmp.tipoReferencia);
                                $("#trReferencia" + this.id).find("td:eq(2)").html(tmp.nombre);
                                $("#trReferencia" + this.id).find("td:eq(3)").html(tmp.direccion);
                                $("#trReferencia" + this.id).find("td:eq(4)").html(tmp.lugarTrabajo);
                                $("#trReferencia" + this.id).find("td:eq(5)").html(tmp.telefono);
                                $("#trReferencia" + this.id).find("td:eq(6)").html(tmp.parentesco);
                                $("#trReferencia" + this.id).find("td:eq(7)").html(tmp.comentarios);
                                resolve();
                            }
                        } catch (err) {
                            reject(err.message);
                        }
                    });

                    guardando.then(() => {
                        this.referencias[this.id] = {
                            ...tmp
                        };
                        tblReferenciasSolicitante.columns.adjust().draw();
                        $("#mdlReferencia").modal("hide");
                    }).catch(generalMostrarError);
                }
            }).catch(generalMostrarError)
        },
        addRowTbl: function ({
            tipoReferencia,
            nombre,
            direccion,
            lugarTrabajo,
            telefono,
            parentesco,
            comentarios
        }) {
            return new Promise((resolve, reject) => {
                try {
                    this.cnt++;
                    this.id = this.cnt;
                    let botones = "<div class='tblButtonContainer'>" +
                        "<span class='btnTbl' title='Editar' onclick='configDatosSolicitante.configReferencias.edit(" + this.cnt + ");'>" +
                        "<i class='fas fa-edit'></i>" +
                        "</span>" +
                        "<span class='btnTbl' title='Eliminar' onclick='configDatosSolicitante.configReferencias.delete(" + this.cnt + ");'>" +
                        "<i class='fas fa-trash'></i>" +
                        "</span>" +
                        "</div>";

                    botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                    tblReferenciasSolicitante.row.add([
                        this.cnt,
                        tipoReferencia,
                        nombre,
                        direccion,
                        lugarTrabajo,
                        telefono,
                        parentesco,
                        comentarios,
                        botones
                    ]).node().id = "trReferencia" + this.cnt;
                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            });
        },
        delete: function (id) {
            let nombre = this.referencias[id].nombre;
            Swal.fire({
                title: 'Eliminar referencia',
                icon: 'warning',
                html: '¿Seguro/a que quieres <b>eliminar</b> la referencia de ' + nombre + '?.',
                showCloseButton: false,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
                cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    tblReferenciasSolicitante.row('#trReferencia' + id).remove().draw();
                    delete this.referencias[id];
                }
            });
        }
    },
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                formActions.clean(this.formulario.id).then(() => {
                    this.codigoCliente.value = tmpDatos.codigoCliente;
                    this.tipoDocumento.value = tmpDatos.tipoDocumento;
                    $("#" + this.tipoDocumento.id).trigger("change");
                    this.numeroDocumento.value = tmpDatos.numeroDocumento;
                    this.numeroDocumento.readOnly = true;
                    this.nit.value = tmpDatos.nit;
                    this.nombres.value = tmpDatos.nombres;
                    this.apellidos.value = tmpDatos.apellidos;
                    this.conocidoPor.value = tmpDatos.conocidoPor;
                    this.pais.value = tmpDatos.pais;
                    $("#" + this.pais.id).trigger("change");
                    this.departamento.value = tmpDatos.departamento;
                    $("#" + this.departamento.id).trigger("change");
                    this.municipio.value = tmpDatos.municipio;
                    this.direccion.value = tmpDatos.direccion;
                    this.profesion.value = tmpDatos.profesion;
                    this.fechaNacimiento.value = tmpDatos.fechaNacimiento;
                    this.edad.value = tmpDatos.edad;
                    tmpDatos.sabeFirmar != null && (this.sabeFirmar.value = tmpDatos.sabeFirmar);
                    this.telefonoMovil.value = tmpDatos.telefonoMovil;
                    this.telefonoFijo.value = tmpDatos.telefonoFijo;
                    this.email.value = tmpDatos.email;
                    this.actividadEconomica.value = tmpDatos.actividadEconomica;
                    resolve();
                });
            } catch (err) {
                reject(err.message);
            }
        });
    }
}

const configDatosCredito = {
    fechaRecepcion: document.getElementById("txtFechaRecepcion"),
    montoSolicitado: document.getElementById("numMontoSolicitado"),
    montoSugerido: document.getElementById("numMontoSugerido"),
    lineaFinanciera: document.getElementById("cboLineaFinanciera"),
    destino: document.getElementById("txtDestino"),
    tasaInteres: document.getElementById("numTasaInteres"),
    alVencimiento: document.getElementById("cboVencimiento"),
    plazo: document.getElementById("numPlazo"),
    tipoPlazo: document.getElementById("cboPlazo"),
    periodicidad: document.getElementById("cboPeriodicidad"),
    fuenteFondos: document.getElementById("cboFuente"),
    agencia: document.getElementById("txtAgencia"),
    configCuota: {
        campoTotalCuota: document.getElementById("numMontoTotal"),
        capitalInteres: document.getElementById("numCapitalInteres"),
        configAditivos: {
            catalogo: {},
            aditivos: {},
            id: 0,
            cnt: 0,
            cboAditivo: document.getElementById("cboAditivo"),
            montoAditivo: document.getElementById("numMontoAditivo"),
            mdlTitle: document.querySelector("#mdlAditivo h5.modal-title"),
            init: function (tmpDatos) {
                return new Promise((resolve, reject) => {

                });
            },
            edit: function (id = 0) {
                formActions.clean("mdlAditivo").then(() => {
                    const leyendo = new Promise((resolve, reject) => {
                        try {
                            let opciones = [`<option selected disables value="">seleccione</option>`],
                                incluidos = [];
                            this.id = id;
                            for (let key in this.aditivos) {
                                if (!this.aditivos[id] || this.aditivos[key].idAditivo != this.aditivos[id].idAditivo) {
                                    incluidos.push(this.aditivos[key].idAditivo);
                                }
                            }
                            for (let key in this.catalogo) {
                                if (!incluidos.includes(this.catalogo[key].id)) {
                                    opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].aditivo}</option>`);
                                }
                            }

                            this.cboAditivo.innerHTML = opciones.join("");

                            if (id > 0) {
                                this.cboAditivo.value = this.aditivos[id].idAditivo;
                                this.montoAditivo.value = this.aditivos[id].monto;
                                resolve();
                            } else {
                                resolve();
                            }
                        } catch (err) {
                            reject(err.message);
                        }
                    });

                    leyendo.then(() => {
                        $("#" + this.cboAditivo.id).selectpicker("refresh");
                        $("#mdlAditivo").modal("show");
                    }).catch(generalMostrarError);
                });
            },
            save: function () {
                formActions.validate('frmAditivo').then(({
                    errores
                }) => {
                    if (errores == 0) {
                        let tmp = {
                            id: this.id > 0 ? this.aditivos[this.id].id : 0,
                            idAditivo: this.cboAditivo.value,
                            aditivo: this.cboAditivo.options[this.cboAditivo.options.selectedIndex].text,
                            monto: this.montoAditivo.value
                        }
                        let guardando = new Promise((resolve, reject) => {
                            if (this.id == 0) {
                                let agregar = this.addRowTbl({
                                    ...tmp
                                });
                                agregar.then(resolve).catch(reject);
                            } else {
                                $("#trAditivo" + this.id).find("td:eq(1)").html(this.catalogo[this.cboAditivo.value].aditivo);
                                $("#trAditivo" + this.id).find("td:eq(2)").html("$" + parseFloat(this.montoAditivo.value).toFixed(2));
                                resolve();
                            }
                        });
                        guardando.then(() => {
                            this.aditivos[this.id] = {
                                ...tmp
                            };
                            tblAditivos.columns.adjust().draw();
                            $("#mdlAditivo").modal("hide");
                            configDatosCredito.configCuota.calcularCuota();
                        }).catch(generalMostrarError);
                    }
                }).catch(generalMostrarError);
            },
            addRowTbl: function ({
                idAditivo,
                aditivo,
                monto
            }) {
                return new Promise((resolve, reject) => {
                    try {
                        this.cnt++;
                        this.id = this.cnt;
                        let botones = "<div class='tblButtonContainer'>" +
                            "<span class='btnTbl' title='Editar' onclick='configDatosCredito.configCuota.configAditivos.edit(" + this.cnt + ");'>" +
                            "<i class='fas fa-edit'></i>" +
                            "</span>" +
                            "<span class='btnTbl' title='Eliminar' onclick='configDatosCredito.configCuota.configAditivos.delete(" + this.cnt + ");'>" +
                            "<i class='fas fa-trash'></i>" +
                            "</span>" +
                            "</div>";
                        botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                        tblAditivos.row.add([
                            this.cnt,
                            aditivo,
                            "$" + parseFloat(monto).toFixed(2),
                            botones
                        ]).node().id = "trAditivo" + this.cnt;
                        resolve();
                    } catch (err) {
                        reject(err.message);
                    }
                });
            },
            delete: function (id) {
                let aditivoLabel = this.aditivos[id].aditivo;
                Swal.fire({
                    title: 'Eliminar aditivo',
                    icon: 'warning',
                    html: '¿Seguro/a que quieres <b>eliminar</b> el aditivo ' + aditivoLabel + '?.',
                    showCloseButton: false,
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
                    cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
                }).then((result) => {
                    if (result.isConfirmed) {
                        tblAditivos.row('#trAditivo' + id).remove().draw();
                        delete this.aditivos[id];
                        configDatosCredito.configCuota.calcularCuota();
                    }
                });
            }
        },
        calcularCuota: function () {
            let totalCuota = 0;
            this.capitalInteres.value && (totalCuota += parseFloat(this.capitalInteres.value));
            for (let key in this.configAditivos.aditivos) {
                totalCuota += parseFloat(this.configAditivos.aditivos[key].monto);
            }

            this.campoTotalCuota.value = totalCuota.toFixed(2);
        }
    },
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                // console.log(tmpDatos)
                tmpDatos.fechaRecepcion != null && (this.fechaRecepcion.value = tmpDatos.fechaRecepcion);
                this.montoSolicitado.value = tmpDatos.montoSolicitado;
                this.montoSugerido.value = tmpDatos.montoSugerido == null ? tmpDatos.montoSolicitado : tmpDatos.montoSugerido;
                tmpDatos.lineaFinanciera != null && (this.lineaFinanciera.value = tmpDatos.lineaFinanciera);
                this.destino.value = tmpDatos.destinoCredito;
                this.tasaInteres.value = tmpDatos.tasaInteres;
                tmpDatos.vencimiento != null && (this.alVencimiento.value = tmpDatos.vencimiento);
                this.plazo.value = tmpDatos.plazo;
                tmpDatos.tipoPlazo != null && (this.tipoPlazo.value = tmpDatos.tipoPlazo);
                tmpDatos.periodicidad != null && (this.periodicidad.value = tmpDatos.periodicidad);
                tmpDatos.fuenteFondos != null && (this.fuenteFondos.value = tmpDatos.fuenteFondos);
                this.agencia.value = tmpDatos.nombreAgencia;
                this.configCuota.capitalInteres.value = tmpDatos.capitalInteres;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    }
}

const configGarantia = {
    id: 0,
    cnt: 0,
    catalogo: {},
    garantias: {},
    cboTipoGarantia: document.getElementById("cboTipoGarantia"),
    init: function () {},
    edit: function (id = 0) {
        formActions.clean('mdlGarantia').then(() => {
            this.id = id;
            let incluidos = [],
                opciones = [`<option selected disabled value="">seleccione</option>`];
            for (let key in this.garantias) {
                incluidos.push(this.garantias[key].idTipoGarantia);
            }
            for (let key in this.catalogo) {
                if (!incluidos.includes(this.catalogo[key].id)) {
                    opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].tipoGarantia}</option>`);
                }
            }
            this.cboTipoGarantia.innerHTML = opciones.join("");
            $("#" + this.cboTipoGarantia.id).selectpicker("refresh");
            $("#mdlGarantia").modal("show");
        }).catch(generalMostrarError);
    },
    save: function () {
        formActions.validate().then(({
            errores
        }) => {
            if (errores == 0) {
                let currentDate = new Date(),
                    fechaActualizacion = ("0" + currentDate.getDate()).slice(-2) + "-" + ("0" + (currentDate.getMonth() + 1)).slice(-2) + "-" + currentDate.getFullYear() + " " + ("0" + currentDate.getHours()).slice(-2) + ":" + ("0" + currentDate.getMinutes()).slice(-2);
                let tmp = {
                    id: this.id > 0 ? this.garantias[this.id].id : 0,
                    idTipoGarantia: this.cboTipoGarantia.value,
                    tipoGarantia: this.cboTipoGarantia.options[this.cboTipoGarantia.options.selectedIndex].text,
                    fechaRegistro: this.id > 0 ? this.garantias[this.id].fechaRegistro : fechaActualizacion,
                    configurada: "NO"
                }
                let guardando = new Promise((resolve, reject) => {
                    try {
                        if (this.id > 0) {
                            $("#trGarantia" + this.id).find("td:eq(1)").html(tmp.tipoGarantia);
                            resolve();
                        } else {
                            let agregar = this.addRowTbl({
                                ...tmp
                            }).then(resolve).catch(reject);
                        }
                    } catch (err) {
                        reject(err.message);
                    }
                });

                guardando.then(() => {
                    this.garantias[this.id] = {
                        ...tmp
                    };
                    tblGarantias.columns.adjust().draw();
                    $("#mdlGarantia").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        tipoGarantia,
        fechaRegistro,
        configurada
    }) {
        return new Promise((resolve, reject) => {
            try {
                this.cnt++;
                this.id = this.cnt;
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Configurar' onclick='configGarantia.config(" + this.cnt + ");'>" +
                    "<i class='fas fa-cogs'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configGarantia.delete(" + this.cnt + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblGarantias.row.add([
                    this.cnt,
                    tipoGarantia,
                    fechaRegistro,
                    configurada,
                    botones
                ]).node().id = "trGarantia" + this.cnt;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id) {
        let labelTipoGarantia = this.garantias[id].tipoGarantia;
        Swal.fire({
            title: 'Eliminar garantía',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la garantia de <strong>' + labelTipoGarantia + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblGarantias.row('#trGarantia' + id).remove().draw();
                delete this.garantias[id];
            }
        });
    }
}