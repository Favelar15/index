let tblLineasFinancieras;

let botones = "<div class='tblButtonContainer'>" +
    "<span class='btnTbl' title='Eliminar' onclick='configuracionPendiente.delete(identificador);'>" +
    "<i class='fas fa-trash'></i>" +
    "</span>" +
    "</div>";

window.onload = () => {
    const gettingData = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "SIMAC",
                archivo: 'simacGetCats',
                solicitados: ['lineasFinancieras']
            }).then(({
                lineasFinancieras
            }) => {
                $(".preloader").fadeIn("fast");
                configLineaFinanciera.init([...lineasFinancieras]).then(resolve).catch(reject);
                resolve();
            }).catch(reject);
        });
    });

    gettingData.then(() => {
        $(".preloader").fadeOut("fast");
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 250;
            if ($("#tblLineasFinancieras").length) {
                tblLineasFinancieras = $("#tblLineasFinancieras").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    scrollY: alturaTabla,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblLineasFinancieras.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configLineaFinanciera = {
    id: 0,
    cnt: 0,
    catalogo: {},
    campoLineaFinanciera: document.getElementById("txtLineaFinanciera"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(linea => {
                    this.cnt++;
                    this.catalogo[this.cnt] = {
                        ...linea
                    };
                    this.addRowTbl({
                        ...linea,
                        idFila: this.cnt
                    });
                });
                tblLineasFinancieras.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('mdlLineaFinanciera').then(() => {
            const leyendo = new Promise((resolve, reject) => {
                this.id = id;
                resolve();
            });

            leyendo.then(() => {
                $("#mdlLineaFinanciera").modal("show");
            }).catch(generalMostrarError);
        });
    },
    save: function () {
        formActions.validate().then(({
            errores
        }) => {
            if (errores == 0) {
                let currentDate = new Date(),
                    fechaRegistro = ("0" + currentDate.getDate()).slice(-2) + "-" + ("0" + (currentDate.getMonth() + 1)).slice(-2) + "-" + currentDate.getFullYear() + " " + ("0" + currentDate.getHours()).slice(-2) + ":" + ("0" + currentDate.getMinutes()).slice(-2);
                let tmp = {
                    id: this.id > 0 ? this.catalogo[this.id].id : 0,
                    lineaFinanciera: this.campoLineaFinanciera.value,
                    fechaRegistro: this.id > 0 ? this.catalogo[this.id].fechaRegistro : fechaRegistro
                }
                const guardando = new Promise((resolve, reject) => {
                    fetchActions.set({modulo:"SIMAC",archivo:"simacGuardarLineaFinanciera",datos:{...tmp,idApartado,tipoApartado}}).then((datosRespuesta)=>{
                        if (datosRespuesta.respuesta) {
                            switch (datosRespuesta.respuesta.trim()) {
                                case "EXITO":
                                    if (this.id > 0) {
                                        $("#trLineaFinanciera" + this.id).find("td:eq(1)").html(tmp.lineaFinanciera);
                                        resolve();
                                    } else {
                                        tmp.id = datosRespuesta.id;
                                        this.cnt++;
                                        this.id = this.cnt;
                                        this.addRowTbl({
                                            ...tmp,
                                            idFila: this.cnt
                                        }).then(resolve).catch(reject);
                                    }
                                    break;
                                default:
                                    generalMostrarError(datosRespuesta);
                                    break;
                            }
                        } else {
                            generalMostrarError(datosRespuesta);
                        }
                    }).catch(generalMostrarError);
                });

                guardando.then(() => {
                    $(".preloader").fadeOut("fast");
                    tblLineasFinancieras.columns.adjust().draw();
                    this.catalogo[this.id] = {
                        ...tmp
                    };
                    $("#mdlLineaFinanciera").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        lineaFinanciera,
        fechaRegistro
    }) {
        return new Promise((resolve, reject) => {
            try {
                botones = tipoPermiso != "ESCRITURA" ? '' : botones;
                tblLineasFinancieras.row.add([
                    idFila,
                    lineaFinanciera,
                    fechaRegistro,
                    botones.replace('configuracionPendiente', 'configLineaFinanciera').replace('identificador', idFila)
                ]).node().id = "trLineaFinanciera" + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let label = this.catalogo[id].lineaFinanciera;
        Swal.fire({
            title: 'Eliminar línea financiera',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la línea financiera <strong>' + label + '</strong>?.<br />Este cambio es irreversible y afectará a todo el módulo SIMAC.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: "SIMAC",
                    archivo: "simacEliminarLineaFinanciera",
                    datos: {
                        id,
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                tblLineasFinancieras.row('#trLineaFinanciera' + id).remove().draw();
                                delete this.catalogo[id];
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        });
    },
}