let tblSolicitudesProceso,
    tblSolicitudesEvaluacion,
    tblSolicitudesDesembolso;

let catActividadesEconomicas = {};

window.onload = () => {
    $(".preloader").find("span").html("Obteniendo solicitudes");
    $(".preloader").fadeIn("fast");
    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "SIMAC",
                archivo: "simacGetCats",
                solicitados: ['actividadEconomica']
            }).then((datosRespuesta) => {
                if (datosRespuesta.actividadesEconomicas) {
                    datosRespuesta.actividadesEconomicas.forEach(actividad => {
                        catActividadesEconomicas[actividad.id] = {
                            ...actividad
                        };
                    });
                }
                resolve();
            }).catch(reject);
        });
    }).catch(generalMostrarError);

    const gettingSolicitudes = gettingCats.then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.get({
                modulo: "SIMAC",
                archivo: "simacGetSolicitudes",
                params: {
                    tipo: tipoApartado,
                    apartado: idApartado,
                    q: 'proceso@@@evaluacion@@@desembolso'
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.solicitudesProceso) {
                    configSolicitudesProceso.init([...datosRespuesta.solicitudesProceso]).then(() => {
                        tblSolicitudesProceso.columns.adjust().draw();
                    }).catch(generalMostrarError);
                }
                resolve();
            }).catch(reject);
        });
    }).catch(generalMostrarError)

    gettingSolicitudes.then(() => {
        $(".preloader").fadeOut("fast");
        $(".preloader").find("span").html("");
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 250;
            if ($("#tblSolicitudesProceso").length) {
                tblSolicitudesProceso = $("#tblSolicitudesProceso").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                            "targets": [0],
                            "orderable": false,
                        },
                        {
                            "targets": [1, 2, 3, 4, 5],
                            "className": "text-start"
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblSolicitudesProceso.columns.adjust().draw();
                resolve();
            }
        } catch (err) {
            console.error(err.message);
            reject();
        }
    });
}

const configSolicitudesProceso = {
    cnt: 0,
    solicitudes: {},
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(solicitud => {
                    this.addRowTbl({
                        ...solicitud
                    }).then(this.solicitudes[this.cnt] = {
                        ...solicitud
                    });
                });
                resolve();
            } catch (err) {
                reject(err.message)
            }
        });
    },
    addRowTbl: function ({
        nombres,
        apellidos,
        actividadEconomica,
        destino,
        montoSolicitado,
        montoSugerido,
        nombreUsuario = "undefined",
        agencia,
        fechaRecepcion,
        ultimaRevision,
        tiempoTrabajado = 'Sin registros',
        solicitudNueva
    }) {
        return new Promise((resolve, reject) => {
            try {
                this.cnt++;
                let icono = solicitudNueva == 'S' ? 'fa-download' : 'fa-cogs',
                    botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configSolicitudesProceso.edit(" + this.cnt + ");'>" +
                    "<i class='fas " + icono + "'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                let nombreCompleto = `${nombres} ${apellidos}`,
                    labelActividadEconomica = catActividadesEconomicas[actividadEconomica].actividadEconomica,
                    impMonto = montoSugerido.length == 0 ? montoSolicitado : montoSugerido;
                tblSolicitudesProceso.row.add([
                    this.cnt,
                    nombreCompleto,
                    labelActividadEconomica,
                    destino,
                    "$" + impMonto,
                    nombreUsuario,
                    agencia,
                    fechaRecepcion,
                    ultimaRevision,
                    tiempoTrabajado,
                    botones
                ]).node().id = "trSolicitudProceso" + this.cnt;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (index) {
        let {
            solicitudNueva
        } = this.solicitudes[index];
        let validando = new Promise((resolve, reject) => {
            if (solicitudNueva == 'S') {
                fetchActions.set({
                    modulo: "SIMAC",
                    archivo: 'simacDownloadSolicitud',
                    datos: {
                        solicitud: this.solicitudes[index],
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                resolve(datosRespuesta.id);
                                break;
                            default:
                                reject(datosRespuesta);
                                break;
                        }
                    } else {
                        reject(datosRespuesta);
                    }
                }).catch(reject);
            } else {
                resolve(this.solicitudes[index].id);
            }
        });
        validando.then((idDb) => {
            let tmpModulo = encodeURIComponent(btoa('SIMAC')),
                tmpId = encodeURIComponent(btoa(idDb));
            window.top.location.href = `./?page=simacSolicitud&mod=${tmpModulo}&id=${tmpId}`;
        }).catch(generalMostrarError);
    }
}
const configSolicitudesEvaluacion = {
    cnt: 0,
    solicitudes: {}
}
const configSolicitudesDesembolso = {
    cnt: 0,
    solicitudes: {}
}