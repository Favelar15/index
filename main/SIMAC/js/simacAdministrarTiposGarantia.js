let tblTiposGarantia,
    tblGrupos;
window.onload = () => {
    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "SIMAC",
                archivo: "simacGetCats",
                solicitados: ['tiposGarantia']
            }).then((datosRespuesta) => {
                $(".preloader").fadeIn("fast");
                configTipoGarantia.init([...datosRespuesta.tiposGarantia]).then(resolve).catch(reject);
            }).catch(reject);
        });
    });

    gettingCats.then(() => {
        $(".preloader").fadeOut("fast");
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 250;
            if ($("#tblTiposGarantia").length) {
                tblTiposGarantia = $("#tblTiposGarantia").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblTiposGarantia.columns.adjust().draw();
            }

            if ($("#tblGrupos").length) {
                tblGrupos = $("#tblGrupos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblGrupos.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configTipoGarantia = {
    id: 0,
    cnt: 0,
    tiposGarantia: {},
    tabTiposGarantia: document.getElementById("tiposGarantia-tab"),
    tabConfiguracion: document.getElementById("configuracion-tab"),
    campoTipoGarantia: document.getElementById("txtTipoGarantia"),
    cboConfigurable: document.getElementById("cboConfigurable"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(tipo => {
                    this.cnt++;
                    this.tiposGarantia[this.cnt] = {
                        ...tipo,
                        grupos: [...tipo.grupos]
                    };
                    this.addRowTbl({
                        ...tipo,
                        idFila: this.cnt
                    });
                });
                tblTiposGarantia.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmTipoGarantia').then(() => {
            const leyendo = new Promise((resolve, reject) => {
                this.id = id;
                tblGrupos.clear().draw();
                configGrupo.grupos = {};
                configGrupo.cnt = 0;
                this.campoTipoGarantia.readOnly = false;
                if (this.id > 0) {
                    this.campoTipoGarantia.value = this.tiposGarantia[this.id].tipoGarantia;
                    this.campoTipoGarantia.readOnly = true;
                    configGrupo.init([...this.tiposGarantia[this.id].grupos]).then(resolve).catch(reject);;
                } else {
                    resolve();
                }
            });

            leyendo.then(() => {
                this.tabConfiguracion.classList.remove("disabled");
                $("#" + this.tabConfiguracion.id).trigger("click");
                this.tabTiposGarantia.classList.add("disabled");
            });
        });
    },
    save: function () {
        formActions.validate('frmTipoGarantia').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.tiposGarantia[this.id].id : 0,
                    tipoGarantia: this.campoTipoGarantia.value,
                    configurable: this.cboConfigurable.value,
                    grupos: {
                        ...configGrupo.grupos
                    }
                }
                fetchActions.set({
                    modulo: "SIMAC",
                    archivo: 'simacGuardarTipoGarantia',
                    datos: {
                        ...tmp,
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                location.reload();
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        tipoGarantia,
        configurable,
        desarrollo,
        grupos,
        fechaRegistro,
        fechaActualizacion
    }) {
        return new Promise((resolve, reject) => {
            try {
                let cntGrupos = Object.keys(grupos).length > 0 ? "SI" : "NO";
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configTipoGarantia.edit(" + idFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configTipoGarantia.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblTiposGarantia.row.add([
                    idFila,
                    tipoGarantia,
                    configurable == 'S' ? "SI" : "NO",
                    cntGrupos,
                    desarrollo == 'S' ? "SI" : "NO",
                    fechaRegistro,
                    fechaActualizacion,
                    botones
                ]).node().id = "trTipoGarantia" + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let label = this.tiposGarantia[id].tipoGarantia;
        Swal.fire({
            title: 'Eliminar tipo de garantía',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el tipo de garantía <strong>' + label + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: "SIMAC",
                    archivo: "simacEliminarTipoGarantia",
                    datos: {
                        id,
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                tblTiposGarantia.row('#trTipoGarantia' + id).remove().draw();
                                delete this.tiposGarantia[id];
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                this.showPrincipal();
            }
        });
    },
    showPrincipal: function () {
        this.tabTiposGarantia.classList.remove("disabled");
        $("#" + this.tabTiposGarantia.id).trigger('click');
        this.tabConfiguracion.classList.add("disabled");
        return;
    }
}

const configGrupo = {
    id: 0,
    cnt: 0,
    grupos: {},
    campoGrupo: document.getElementById("txtGrupo"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(grupo => {
                    this.cnt++;
                    this.grupos[this.cnt] = {
                        ...grupo
                    };
                    this.addRowTbl({
                        ...grupo,
                        idFila: this.cnt
                    });
                });
                tblGrupos.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean("mdlGrupo").then(() => {
            const leyendo = new Promise((resolve, reject) => {
                try {
                    this.id = id;
                    if (this.id > 0) {
                        this.campoGrupo.value = this.grupos[this.id].nombreGrupo;
                    } else {
                        resolve();
                    }
                } catch (err) {
                    reject(err.message);
                }
            });

            leyendo.then(() => {
                $("#mdlGrupo").modal("show");
            }).catch(generalMostrarError);
        });
    },
    save: function () {
        formActions.validate('mdlGrupo').then(({
            errores
        }) => {
            if (errores == 0) {
                let currentDate = new Date(),
                    fechaActualizacion = ("0" + currentDate.getDate()).slice(-2) + "-" + ("0" + (currentDate.getMonth() + 1)).slice(-2) + "-" + currentDate.getFullYear() + " " + ("0" + currentDate.getHours()).slice(-2) + ":" + ("0" + currentDate.getMinutes()).slice(-2);
                let tmp = {
                    id: this.id > 0 ? this.grupos[this.id].id : 0,
                    nombreGrupo: this.campoGrupo.value,
                    fechaRegistro: this.id > 0 ? this.grupos[this.id].fechaRegistro : fechaActualizacion
                };
                const guardando = new Promise((resolve, reject) => {
                    if (this.id > 0) {
                        $("#trGrupo" + this.id).find("td:eq(1)").html(tmp.nombreGrupo);
                        resolve();
                    } else {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            idFila: this.cnt
                        }).then(resolve).catch(reject);
                    }
                });

                guardando.then(() => {
                    tblGrupos.columns.adjust().draw();
                    this.grupos[this.id] = {
                        ...tmp
                    };
                    $("#mdlGrupo").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        nombreGrupo,
        fechaRegistro
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configGrupo.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblGrupos.row.add([
                    idFila,
                    nombreGrupo,
                    fechaRegistro,
                    botones
                ]).node().id = 'trGrupo' + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let label = this.grupos[id].nombreGrupo;
        Swal.fire({
            title: 'Eliminar grupo',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el grupo <strong>' + label + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                tblGrupos.row('#trGrupo' + id).remove().draw();
                delete this.grupos[id];
            }
        });
    },
}