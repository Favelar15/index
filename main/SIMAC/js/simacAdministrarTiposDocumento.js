let tblCatalogo,
    tblPersonas,
    tblLineasFinancieras,
    tblTiposGarantia;

window.onload = () => {
    $(".fileUploader").on("change", function (e) {
        let file = e.target.files[0],
            $file = $(this),
            url = URL.createObjectURL(file),
            filename = $file.val().split('\\').pop();

        let label = $(this).parent().find("span.fileName"),
            idFrame = label.attr("frameTarget");
        label.html(filename);
        if (idFrame) {
            document.getElementById(idFrame).src = url;
        }
    });

    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "SIMAC",
                archivo: 'simacGetCats',
                solicitados: ['lineasFinancierasCbo', 'tiposGarantia', 'simacTiposDocumentos']
            }).then(({
                lineasFinancieras,
                tiposGarantia,
                simacTiposDocumentos
            }) => {
                $(".preloader").fadeIn("fast");
                lineasFinancieras.forEach(linea => {
                    configGrupoLineaFinanciera.catalogo[linea.id] = {
                        ...linea
                    };
                });

                tiposGarantia.forEach(tipo => {
                    configGrupoTipoGarantia.catalogo[tipo.id] = {
                        ...tipo
                    };
                });

                configTipoDocumento.init([...simacTiposDocumentos]);
                resolve();
            }).catch(reject);
        });
    });

    gettingCats.then(() => {
        $(".preloader").fadeOut("fast");
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 250;
            if ($("#tblCatalogo").length) {
                tblCatalogo = $("#tblCatalogo").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblCatalogo.columns.adjust().draw();
            }

            if ($("#tblPersonas").length) {
                tblPersonas = $("#tblPersonas").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblPersonas.columns.adjust().draw();
            }

            if ($("#tblLineasFinancieras").length) {
                tblLineasFinancieras = $("#tblLineasFinancieras").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblLineasFinancieras.columns.adjust().draw();
            }

            if ($("#tblTiposGarantia").length) {
                tblTiposGarantia = $("#tblTiposGarantia").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblTiposGarantia.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configTipoDocumento = {
    id: 0,
    cnt: 0,
    catalogo: {},
    tabCatalogo: document.getElementById("catalogo-tab"),
    tabConfiguracion: document.getElementById("configuracion-tab"),
    tabGeneral: document.getElementById("configGeneral-tab"),
    chksPermiteAdjuntos: document.querySelectorAll("input[type='checkbox'].chkPermiteAdjuntos"),
    tipoDocumento: document.getElementById("txtTipoDocumento"),
    chkDocumentoObligatorio: document.getElementById("chkObligatorio"),
    rbImpReferencia: document.getElementById("rbImpReferencia"),
    rbImpSistema: document.getElementById("rbImpSistema"),
    campoArchivo: document.getElementById("filePDF"),
    chkDocumentoConfigurable: document.getElementById("chkConfigurable"),
    chkPermiteAdjuntos: document.getElementById("chkAdjuntos"),
    chkAdjuntosObligatorios: document.getElementById("chkAdjuntosObligatorios"),
    urlArchivo: '',
    archivo: null,
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(tipoDocumento => {
                    this.cnt++;
                    this.catalogo[this.cnt] = {
                        ...tipoDocumento
                    };
                    this.addRowTbl({
                        ...tipoDocumento,
                        idFila: this.cnt
                    });
                });

                tblCatalogo.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('configuracion').then(() => {
            const leyendo = new Promise((resolve, reject) => {
                let nombreArchivo = this.campoArchivo.parentNode.querySelector("span.fileName"),
                    frame = document.getElementById("generalPDFViewer");
                this.id = id;
                configGrupoPersona.cnt = 0;
                configGrupoLineaFinanciera.cnt = 0;
                configGrupoTipoGarantia.cnt = 0;
                configGrupoPersona.grupos = {};
                configGrupoLineaFinanciera.grupos = {};
                configGrupoLineaFinanciera.grupos = {};
                tblPersonas.clear().draw();
                tblLineasFinancieras.clear().draw();
                tblTiposGarantia.clear().draw();

                $("#" + this.tabGeneral.id).trigger("click");
                configPrintMode.evalPrintMode('printModeGeneral', '');
                this.chkDocumentoObligatorio.checked = false;
                this.rbImpReferencia.checked = false;
                this.rbImpSistema.checked = false;
                frame.src = "";
                nombreArchivo.innerHTML = "No hay archivo seleccionado";
                this.chkDocumentoConfigurable.checked = false;
                this.chkPermiteAdjuntos.checked = false;
                this.chkAdjuntosObligatorios.checked = false;

                if (this.id > 0) {
                    let tmp = {
                        ...this.catalogo[this.id]
                    };
                    this.tipoDocumento.value = tmp.tipoDocumento;
                    tmp.paramGeneral.obligatorio == 'S' && (this.chkDocumentoObligatorio.checked = true);
                    if (Object.keys(tmp.paramGeneral.impReferencia).length > 0) {
                        this.rbImpReferencia.checked = true;
                        $("#" + this.rbImpReferencia.id).trigger("change");
                        nombreArchivo.innerHTML = tmp.paramGeneral.impReferencia.nombreArchivo;
                        frame.src = tmp.paramGeneral.impReferencia.urlArchivo;
                    }

                    if (Object.keys(tmp.paramGeneral.impSistema).length > 0) {
                        this.rbImpSistema.checked = true;
                        $("#" + this.rbImpSistema.id).trigger("change");
                        tmp.paramGeneral.impSistema.configurable == 'S' && (this.chkDocumentoConfigurable.checked = true);
                        tmp.paramGeneral.impSistema.permiteAdjuntos == 'S' && (this.chkPermiteAdjuntos.checked = true);
                        configPrintMode.evalPermiteAdjuntos(this.chkPermiteAdjuntos, this.chkAdjuntosObligatorios.id).then(() => {
                            tmp.paramGeneral.impSistema.adjuntosObligatorios == 'S' && (this.chkAdjuntosObligatorios.checked = true);
                        });
                    }

                    configGrupoPersona.init({
                        ...tmp.paramPersonas
                    });
                    configGrupoLineaFinanciera.init({
                        ...tmp.paramLineasFinancieras
                    });
                    configGrupoTipoGarantia.init({
                        ...tmp.paramTiposGarantia
                    });
                }
                // this.lgTitulo.innerHTML = "Nuevo tipo de documento";
                resolve();
            });

            leyendo.then(() => {
                this.chksPermiteAdjuntos.forEach(chk => {
                    $("#" + chk.id).trigger("change");
                });
                this.tabConfiguracion.classList.remove("disabled");
                $("#" + this.tabConfiguracion.id).trigger("click");
                this.tabCatalogo.classList.add("disabled");
            }).catch(generalMostrarError)
        });
    },
    save: function () {
        formActions.validate('frmTipoDocumento').then(({
            errores
        }) => {
            if (errores == 0) {
                let datosCompletos = new FormData();
                let impReferencia = {},
                    impSistema = {};

                if (this.rbImpSistema.checked) {
                    impSistema = {
                        configurable: this.chkDocumentoConfigurable.checked ? 'S' : 'N',
                        permiteAdjuntos: this.chkPermiteAdjuntos.checked ? 'S' : 'N',
                        adjuntosObligatorios: this.chkAdjuntosObligatorios.checked ? 'S' : 'N'
                    }
                }
                if (this.rbImpReferencia.checked) {
                    let nombreArchivo = this.campoArchivo.parentNode.querySelector("span.fileName").innerHTML
                    if (this.campoArchivo.files[0]) {
                        impReferencia = {
                            nombreArchivo,
                            archivo: this.campoArchivo.files[0],
                            urlArchivo: URL.createObjectURL(this.campoArchivo.files[0])
                        }
                    } else {
                        impReferencia = {
                            nombreArchivo,
                            urlArchivo: this.urlArchivo,
                            archivo: this.archivo
                        }
                    }
                }

                let gruposConfiguracion = {
                    general: {
                        obligatorio: this.chkDocumentoObligatorio.checked ? 'S' : 'N',
                        impReferencia,
                        impSistema,
                    },
                    personas: {
                        ...configGrupoPersona.grupos
                    },
                    lineasFinancieras: {
                        ...configGrupoLineaFinanciera.grupos
                    },
                    tiposGarantias: {
                        ...configGrupoTipoGarantia.grupos
                    }
                };

                datosCompletos.append("id", this.id);
                datosCompletos.append('tipoDocumento', this.tipoDocumento.value);

                if (this.campoArchivo.files[0]) {
                    datosCompletos.append("archivoGeneral", this.campoArchivo.files[0]);
                }

                if (Object.keys(gruposConfiguracion.personas).length > 0) {
                    for (let key in gruposConfiguracion.personas) {
                        if (gruposConfiguracion.personas[key].configuracion.impReferencia && Object.keys(gruposConfiguracion.personas[key].configuracion.impReferencia).length > 0) {
                            if (gruposConfiguracion.personas[key].configuracion.impReferencia.archivo != null) {
                                datosCompletos.append(`archivoPersona${gruposConfiguracion.personas[key].idPersona}`, gruposConfiguracion.personas[key].configuracion.impReferencia.archivo);
                            }
                            gruposConfiguracion.personas[key].configuracion.impReferencia.urlArchivo = `./main/SIMAC/docs/referencias/${gruposConfiguracion.personas[key].configuracion.impReferencia.nombreArchivo}`;
                        }
                    }
                }

                if (Object.keys(gruposConfiguracion.lineasFinancieras).length > 0) {
                    for (let key in gruposConfiguracion.lineasFinancieras) {
                        if (gruposConfiguracion.lineasFinancieras[key].configuracion.impReferencia && Object.keys(gruposConfiguracion.lineasFinancieras[key].configuracion.impReferencia).length > 0) {
                            if (gruposConfiguracion.lineasFinancieras[key].configuracion.impReferencia.archivo != null) {
                                datosCompletos.append(`archivoLinea${gruposConfiguracion.lineasFinancieras[key].idLineaFinanciera}`, gruposConfiguracion.lineasFinancieras[key].configuracion.impReferencia.archivo);
                            }
                            gruposConfiguracion.lineasFinancieras[key].configuracion.impReferencia.urlArchivo = `./main/SIMAC/docs/referencias/${gruposConfiguracion.lineasFinancieras[key].configuracion.impReferencia.nombreArchivo}`;
                        }
                    }
                }

                if (Object.keys(gruposConfiguracion.tiposGarantias).length > 0) {
                    for (let key in gruposConfiguracion.tiposGarantias) {
                        if (gruposConfiguracion.tiposGarantias[key].configuracion.impReferencia && Object.keys(gruposConfiguracion.tiposGarantias[key].configuracion.impReferencia).length > 0) {
                            if (gruposConfiguracion.tiposGarantias[key].configuracion.impReferencia.archivo != null) {
                                datosCompletos.append(`archivoGarantia${gruposConfiguracion.tiposGarantias[key].idTipoGarantia}_${gruposConfiguracion.tiposGarantias[key].idSubGrupo}`, gruposConfiguracion.tiposGarantias[key].configuracion.impReferencia.archivo);
                            }
                            gruposConfiguracion.tiposGarantias[key].configuracion.impReferencia.urlArchivo = `./main/SIMAC/docs/referencias/${gruposConfiguracion.tiposGarantias[key].configuracion.impReferencia.nombreArchivo}`;
                        }
                    }
                }

                datosCompletos.append('gruposConfiguracion', JSON.stringify(gruposConfiguracion));
                datosCompletos.append('tipoApartado', tipoApartado);
                datosCompletos.append('idApartado', idApartado);

                fetchActions.setWFiles({
                    modulo: "SIMAC",
                    archivo: 'simacGuardarTipoDocumento',
                    datos: datosCompletos
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                location.reload();
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        tipoDocumento,
        fechaRegistro,
        fechaActualizacion
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configTipoDocumento.edit(" + idFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configTipoDocumento.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblCatalogo.row.add([
                    idFila,
                    tipoDocumento,
                    'Pendiente de hacer',
                    fechaRegistro,
                    fechaActualizacion,
                    botones
                ]).node().id = "trCatalogo" + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let label = this.catalogo[id].tipoDocumento;
        Swal.fire({
            title: 'Eliminar tipo de documento',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el tipo de documento <strong>' + label + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: "SIMAC",
                    archivo: "simacEliminarTipoDocumento",
                    datos: {
                        id,
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                tblCatalogo.row('#trCatalogo' + id).remove().draw();
                                delete this.catalogo[id];
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                this.showPrincipal();
            }
        });
    },
    showPrincipal: function () {
        this.tabCatalogo.classList.remove("disabled");
        $("#" + this.tabCatalogo.id).trigger('click');
        this.tabConfiguracion.classList.add("disabled");
        return;
    }
}

const configGrupoPersona = {
    id: 0,
    cnt: 0,
    grupos: {},
    catalogo: {
        1: {
            id: 1,
            persona: 'Solicitante'
        },
        2: {
            id: 2,
            persona: 'Fiador'
        }
    },
    cboPersona: document.getElementById("cboPersona"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                for (let key in tmpDatos) {
                    this.cnt++;
                    this.grupos[this.cnt] = {
                        ...tmpDatos[key]
                    };

                    this.addRowTbl({
                        ...tmpDatos[key],
                        idFila: this.cnt
                    });
                }

                tblPersonas.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmPersona').then(() => {
            const leyendo = new Promise((resolve, reject) => {
                this.id = id;
                let incluidos = [],
                    opciones = [`<option selected disabled value="">seleccione</option>`];

                for (let key in this.grupos) {
                    incluidos.push(parseInt(this.grupos[key].idPersona));
                }

                for (let key in this.catalogo) {
                    if (!incluidos.includes(this.catalogo[key].id)) {
                        opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].persona}</option>`);
                    }
                }
                this.cboPersona.innerHTML = opciones.join("");
                resolve();
            });

            leyendo.then(() => {
                let allSelectpickerCbos = document.querySelectorAll("#mdlPersona select.selectpicker");
                allSelectpickerCbos.forEach(cbo => {
                    $("#" + cbo.id).selectpicker("refresh");
                });
                $("#mdlPersona").modal("show");
            });
        });
    },
    save: function () {
        formActions.validate('frmPersona').then(({
            errores
        }) => {
            if (errores == 0) {
                let currentDate = new Date(),
                    fechaRegistro = ("0" + currentDate.getDate()).slice(-2) + "-" + ("0" + (currentDate.getMonth() + 1)).slice(-2) + "-" + currentDate.getFullYear() + " " + ("0" + currentDate.getHours()).slice(-2) + ":" + ("0" + currentDate.getMinutes()).slice(-2);
                let tmp = {
                    id: this.id > 0 ? this.grupos[this.id].id : 0,
                    idPersona: this.cboPersona.value,
                    persona: this.cboPersona.options[this.cboPersona.options.selectedIndex].text,
                    configuracion: this.id > 0 ? this.grupos[this.id].configuracion : {},
                    fechaRegistro: this.id > 0 ? this.grupos[this.id].fechaRegistro : fechaRegistro
                }
                const guardando = new Promise((resolve, reject) => {
                    if (this.id > 0) {

                    } else {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            idFila: this.cnt
                        }).then(resolve).catch(reject);
                    }
                });

                guardando.then(() => {
                    this.grupos[this.id] = {
                        ...tmp
                    };
                    tblPersonas.columns.adjust().draw();
                    $("#mdlPersona").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        persona,
        fechaRegistro
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Configurar' onclick='configGrupoPersona.configParam(" + idFila + ");'>" +
                    "<i class='fas fa-cogs'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configGrupoPersona.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblPersonas.row.add([
                    idFila,
                    persona,
                    fechaRegistro,
                    botones
                ]).node().id = "trPersona" + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let label = this.grupos[id].persona;
        Swal.fire({
            title: 'Eliminar tipo de persona',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el tipo de persona <strong>' + label + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                tblPersonas.row('#trPersona' + id).remove().draw();
                delete this.grupos[id];
            }
        });
    },
    configParam: function (id = 0) {
        configParamDoc.grupo = 'Persona';
        configParamDoc.edit(id);
    }
}

const configGrupoLineaFinanciera = {
    id: 0,
    cnt: 0,
    grupos: {},
    catalogo: {},
    cboLineaFinanciera: document.getElementById("cboLineaFinanciera"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                for (let key in tmpDatos) {
                    this.cnt++;
                    this.grupos[this.cnt] = {
                        ...tmpDatos[key]
                    };

                    this.addRowTbl({
                        ...tmpDatos[key],
                        idFila: this.cnt
                    });
                }

                tblLineasFinancieras.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmLineaFinanciera').then(() => {
            const leyendo = new Promise((resolve, reject) => {
                this.id = id;
                let incluidos = [],
                    opciones = [`<option selected disabled value="">seleccione</option>`];

                for (let key in this.grupos) {
                    incluidos.push(this.grupos[key].idLineaFinanciera);
                }

                for (let key in this.catalogo) {
                    if (!incluidos.includes(this.catalogo[key].id)) {
                        opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].lineaFinanciera}</option>`);
                    }
                }
                this.cboLineaFinanciera.innerHTML = opciones.join("");
                resolve();
            });

            leyendo.then(() => {
                let allSelectpickerCbos = document.querySelectorAll("#mdlLineaFinanciera select.selectpicker");
                allSelectpickerCbos.forEach(cbo => {
                    $("#" + cbo.id).selectpicker("refresh");
                });
                $("#mdlLineaFinanciera").modal("show");
            });
        });
    },
    save: function () {
        formActions.validate('frmLineaFinanciera').then(({
            errores
        }) => {
            if (errores == 0) {
                let currentDate = new Date(),
                    fechaRegistro = ("0" + currentDate.getDate()).slice(-2) + "-" + ("0" + (currentDate.getMonth() + 1)).slice(-2) + "-" + currentDate.getFullYear() + " " + ("0" + currentDate.getHours()).slice(-2) + ":" + ("0" + currentDate.getMinutes()).slice(-2);
                let tmp = {
                    id: this.id > 0 ? this.grupos[this.id].id : 0,
                    idLineaFinanciera: this.cboLineaFinanciera.value,
                    lineaFinanciera: this.cboLineaFinanciera.options[this.cboLineaFinanciera.options.selectedIndex].text,
                    configuracion: this.id > 0 ? this.grupos[this.id].configuracion : {},
                    fechaRegistro: this.id > 0 ? this.grupos[this.id].fechaRegistro : fechaRegistro
                }
                const guardando = new Promise((resolve, reject) => {
                    if (this.id > 0) {

                    } else {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            idFila: this.cnt
                        }).then(resolve).catch(reject);
                    }
                });

                guardando.then(() => {
                    this.grupos[this.id] = {
                        ...tmp
                    };
                    tblLineasFinancieras.columns.adjust().draw();
                    $("#mdlLineaFinanciera").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        lineaFinanciera,
        fechaRegistro
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Configurar' onclick='configGrupoLineaFinanciera.configParam(" + idFila + ");'>" +
                    "<i class='fas fa-cogs'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configGrupoLineaFinanciera.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblLineasFinancieras.row.add([
                    idFila,
                    lineaFinanciera,
                    fechaRegistro,
                    botones
                ]).node().id = "trLineaFinanciera" + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let label = this.grupos[id].lineaFinanciera;
        Swal.fire({
            title: 'Eliminar línea financiera',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la línea financiera <strong>' + label + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                tblLineasFinancieras.row('#trLineaFinanciera' + id).remove().draw();
                delete this.grupos[id];
            }
        });
    },
    configParam: function (id = 0) {
        configParamDoc.grupo = 'LineaFinanciera';
        configParamDoc.edit(id);
    }
}

const configGrupoTipoGarantia = {
    id: 0,
    cnt: 0,
    grupos: {},
    catalogo: {},
    cboTipoGarantia: document.getElementById("cboTipoGarantia"),
    cboSubGrupo: document.getElementById("cboGrupoTipoGarantia"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                for (let key in tmpDatos) {
                    this.cnt++;
                    this.grupos[this.cnt] = {
                        ...tmpDatos[key]
                    };

                    this.addRowTbl({
                        ...tmpDatos[key],
                        idFila: this.cnt
                    });
                }

                tblTiposGarantia.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmTipoGarantia').then(() => {
            const leyendo = new Promise((resolve, reject) => {
                this.id = id;
                let opciones = [`<option selected disabled value="">seleccione</option>`];
                for (let key in this.catalogo) {
                    let incluir = true;

                    if (this.catalogo[key].grupos.length == 0) {
                        for (let key2 in this.grupos) {
                            if (this.grupos[key2].idTipoGarantia == this.catalogo[key].id) {
                                incluir = false;
                            }
                        }
                    } else {
                        let tmpCnt = 0;
                        for (let key2 in this.grupos) {
                            if (this.grupos[key2].idTipoGarantia == this.catalogo[key].id) {
                                tmpCnt++;
                            }
                        }

                        tmpCnt == this.catalogo[key].grupos.length && (incluir = false);
                    }

                    if (incluir) {
                        opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].tipoGarantia}</option>`);
                    }
                }
                this.cboTipoGarantia.innerHTML = opciones.join("");
                $("#" + this.cboTipoGarantia.id).trigger("change");
                resolve();
            });

            leyendo.then(() => {
                let allSelectpickerCbos = document.querySelectorAll("#mdlTipoGarantia select.selectpicker");
                allSelectpickerCbos.forEach(cbo => {
                    $("#" + cbo.id).selectpicker("refresh");
                });
                $("#mdlTipoGarantia").modal("show");
            });
        });
    },
    save: function () {
        formActions.validate('frmTipoGarantia').then(({
            errores
        }) => {
            if (errores == 0) {
                let currentDate = new Date(),
                    fechaRegistro = ("0" + currentDate.getDate()).slice(-2) + "-" + ("0" + (currentDate.getMonth() + 1)).slice(-2) + "-" + currentDate.getFullYear() + " " + ("0" + currentDate.getHours()).slice(-2) + ":" + ("0" + currentDate.getMinutes()).slice(-2);
                let tmp = {
                    id: this.id > 0 ? this.grupos[this.id].id : 0,
                    idTipoGarantia: this.cboTipoGarantia.value,
                    idSubGrupo: this.cboSubGrupo.value.length == 0 ? '0' : this.cboSubGrupo.value,
                    tipoGarantia: this.cboTipoGarantia.options[this.cboTipoGarantia.options.selectedIndex].text,
                    subGrupo: this.cboSubGrupo.options[this.cboSubGrupo.options.selectedIndex].text,
                    configuracion: this.id > 0 ? this.grupos[this.id].configuracion : {},
                    fechaRegistro: this.id > 0 ? this.grupos[this.id].fechaRegistro : fechaRegistro
                }
                const guardando = new Promise((resolve, reject) => {
                    if (this.id > 0) {

                    } else {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            idFila: this.cnt
                        }).then(resolve).catch(reject);
                    }
                });

                guardando.then(() => {
                    this.grupos[this.id] = {
                        ...tmp
                    };
                    tblTiposGarantia.columns.adjust().draw();
                    $("#mdlTipoGarantia").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        tipoGarantia,
        subGrupo,
        fechaRegistro
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Configurar' onclick='configGrupoTipoGarantia.configParam(" + idFila + ");'>" +
                    "<i class='fas fa-cogs'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configGrupoTipoGarantia.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblTiposGarantia.row.add([
                    idFila,
                    tipoGarantia,
                    subGrupo,
                    fechaRegistro,
                    botones
                ]).node().id = "trTipoGarantia" + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let label = this.grupos[id].tipoGarantia;
        Swal.fire({
            title: 'Eliminar tipo de garantía',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el tipo de garantía <strong>' + label + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                tblTiposGarantia.row('#trTipoGarantia' + id).remove().draw();
                delete this.grupos[id];
            }
        });
    },
    evalTipoGarantia: function (tipoGarantia) {
        let label = tipoGarantia == 0 ? "seleccione un tipo de garantía" : (this.catalogo[tipoGarantia].grupos.length > 0 ? "seleccione" : "No aplica");
        let opciones = [`<option selected disabled value="">${label}</option>`];

        this.cboSubGrupo.required = true;

        if (tipoGarantia > 0 && this.catalogo[tipoGarantia]) {
            let incluidos = [];
            for (let key in this.grupos) {
                if (this.grupos[key].idTipoGarantia == tipoGarantia) {
                    incluidos.push(this.grupos[key].idSubGrupo);
                }
            }
            this.catalogo[tipoGarantia].grupos.forEach(grupo => {
                if (!incluidos.includes(grupo.id)) {
                    opciones.push(`<option value="${grupo.id}">${grupo.nombreGrupo}</option>`);
                }
            });

            if (this.catalogo[tipoGarantia].grupos.length == 0) {
                this.cboSubGrupo.required = false;
            }
        }

        this.cboSubGrupo.innerHTML = opciones.join("");
        $("#" + this.cboSubGrupo.id).selectpicker("refresh");
    },
    configParam: function (id = 0) {
        configParamDoc.grupo = 'TipoGarantia';
        configParamDoc.edit(id);
    }
}

const configParamDoc = {
    id: 0,
    grupo: '',
    chkDocumentoObligatorio: document.getElementById("chkMdlObligatorio"),
    rbImpReferencia: document.getElementById("rbMdlImpReferencia"),
    rbImpSistema: document.getElementById("rbMdlImpSistema"),
    campoArchivo: document.getElementById("mdlfilePDF"),
    chkDocumentoConfigurable: document.getElementById("chkMdlConfigurable"),
    chkPermiteAdjuntos: document.getElementById("chkMdlAdjuntos"),
    chkAdjuntosObligatorios: document.getElementById("chkMdlAdjuntosObligatorios"),
    urlArchivo: '',
    archivo: null,
    edit: function (id = 0) {
        formActions.clean("mdlConfigDoc").then(() => {
            const leyendo = new Promise((resolve, reject) => {
                this.id = id;
                let tmp = {};
                configPrintMode.evalPrintMode('printModeGrupo', '');
                this.campoArchivo.parentNode.querySelector("span.fileName").innerHTML = 'No hay archivo seleccionado';
                document.getElementById('mdlPDFViewer').src = '';
                if (id > 0) {
                    switch (this.grupo.trim()) {
                        case "Persona":
                            tmp = {
                                ...configGrupoPersona.grupos[this.id].configuracion
                            };
                            break;
                        case "LineaFinanciera":
                            tmp = {
                                ...configGrupoLineaFinanciera.grupos[this.id].configuracion
                            };
                            break;
                        case "TipoGarantia":
                            tmp = {
                                ...configGrupoTipoGarantia.grupos[this.id].configuracion
                            };
                            break;
                    }

                    this.chkDocumentoObligatorio.checked = (tmp.obligatorio && tmp.obligatorio == 'S') ? true : false;
                    if (tmp.impReferencia && Object.keys(tmp.impReferencia).length > 0) {
                        this.rbImpReferencia.checked = true;
                        $("#" + this.rbImpReferencia.id).trigger("change");
                        this.campoArchivo.parentNode.querySelector("span.fileName").innerHTML = tmp.impReferencia.nombreArchivo;
                        document.getElementById('mdlPDFViewer').src = tmp.impReferencia.urlArchivo;
                    } else if (tmp.impSistema && Object.keys(tmp.impSistema).length > 0) {
                        this.rbImpSistema.checked = true;
                        $("#" + this.rbImpSistema.id).trigger("change");
                        tmp.impSistema.configurable == 'S' && (this.chkDocumentoConfigurable.checked = true);
                        tmp.impSistema.permiteAdjuntos == 'S' && (this.chkPermiteAdjuntos.checked = true);
                        configPrintMode.evalPermiteAdjuntos(this.chkPermiteAdjuntos, this.chkAdjuntosObligatorios.id).then(() => {
                            tmp.impSistema.adjuntosObligatorios == 'S' && (this.chkAdjuntosObligatorios.checked = true);
                        });
                    }
                }

                $("#" + this.chkPermiteAdjuntos.id).trigger("change");
                resolve();
            });

            leyendo.then(() => {
                $("#mdlConfigDoc").modal("show");
            }).catch(generalMostrarError);
        });
    },
    save: function () {
        let impReferencia = {},
            impSistema = {};

        if (this.rbImpReferencia.checked) {
            let nombreArchivo = this.campoArchivo.parentNode.querySelector("span.fileName").innerHTML
            if (this.campoArchivo.files[0]) {
                impReferencia = {
                    nombreArchivo,
                    archivo: this.campoArchivo.files[0],
                    urlArchivo: URL.createObjectURL(this.campoArchivo.files[0])
                }
            } else {
                impReferencia = {
                    nombreArchivo,
                    urlArchivo: this.urlArchivo,
                    archivo: this.archivo
                }
            }
        }

        if (this.rbImpSistema.checked) {
            impSistema = {
                configurable: this.chkDocumentoConfigurable.checked ? 'S' : 'N',
                permiteAdjuntos: this.chkPermiteAdjuntos.checked ? 'S' : 'N',
                adjuntosObligatorios: this.chkAdjuntosObligatorios.checked ? 'S' : 'N'
            }
        }

        let tmp = {
            obligatorio: this.chkDocumentoObligatorio.checked ? "S" : "N",
            impReferencia,
            impSistema
        }
        switch (this.grupo.trim()) {
            case "Persona":
                configGrupoPersona.grupos[this.id].configuracion = {
                    ...tmp
                };
                break;
            case "LineaFinanciera":
                configGrupoLineaFinanciera.grupos[this.id].configuracion = {
                    ...tmp
                };
                break;
            case "TipoGarantia":
                configGrupoTipoGarantia.grupos[this.id].configuracion = {
                    ...tmp
                };
                break;
        }
        $("#mdlConfigDoc").modal("hide");
    }
}

const configPrintMode = {
    evalPrintMode: function (idMainContainer, idContainer) {
        let containers = document.querySelectorAll(`#${idMainContainer} .printConfigContainer`);

        containers.forEach(container => {
            if (container.id && container.id != idContainer) {
                container.style.display = 'none';
            } else {
                if ($("#" + idContainer).is(":visible") == false) {
                    $("#" + idContainer).fadeIn('fast');
                }
            }
        });
    },
    evalPermiteAdjuntos: function (chk, idChkObligatorio) {
        return new Promise((resolve, reject) => {
            try {
                let chkObligatorio = document.getElementById(idChkObligatorio);
                chkObligatorio.checked = false;
                if (chk.checked) {
                    chkObligatorio.disabled = false;
                } else {
                    chkObligatorio.disabled = true;
                }
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    }
}