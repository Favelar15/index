<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET)) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/solicitudCredito.php';
        $idSolicitud = base64_decode(urldecode($_GET["q"]));

        $solicitud = new solicitudCredito();
        $solicitud->id = $idSolicitud;
        $datos = $solicitud->getSolicitud();

        $respuesta->{"respuesta"} = $datos["respuesta"];
        isset($datos["solicitud"]) && $respuesta->{"solicitud"} = $datos["solicitud"];
        isset($datos["datosSolicitante"]) && $respuesta->{"datosSolicitante"} = $datos["datosSolicitante"];
        isset($datos["datosCredito"]) && $respuesta->{"datosCredito"} = $datos["datosCredito"];

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
