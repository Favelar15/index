<?php
class tipoGarantia
{
    public $id;
    public $tipoGarantia;
    public $configurable;
    public $grupos;
    public $desarrollo;
    public $fechaRegistro;
    public $fechaActualizacion;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date("d-m-Y h:i a", strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date("d-m-Y h:i a", strtotime($this->fechaActualizacion));
        if (!empty($this->fechaRegistro)) {
            $grupos = new grupoTipoGarantia();
            $this->grupos = $grupos->getGrupos($this->id);
        }
    }

    public function getTiposGarantia()
    {
        $query = "SELECT * FROM simac_viewTiposGarantia;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

    public function getTiposGarantiaCbo()
    {
        $query = "SELECT id,tipoGarantia FROM simac_viewTiposGarantia;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            $datos = $result->fetchAll(PDO::FETCH_OBJ);
            foreach ($datos as $key => $tipo) {
                $grupos = new grupoTipoGarantia();
                $datos[$key]->grupos = $grupos->getGrupos($tipo->id);
            }
            return $datos;
        }

        $this->conexion = null;
        return [];
    }

    public function saveTipoGarantia(int $idUsuario, string $ipActual, string $tipoApartado, int $idApartado)
    {
        $response = null;
        $arrGrupos = [];

        foreach ($this->grupos as $grupo) {
            array_push($arrGrupos, $grupo["id"] . "%%%" . $grupo["nombreGrupo"]);
        }

        $gruposDb = count($arrGrupos) > 0 ? implode("@@@", $arrGrupos) : '';

        $query = "EXEC simac_spGuardarTipoGarantia :id,:tipoGarantia,:configurable,:grupos,:usuario,:ip,:tipoApartado,:idApartado,:response;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':tipoGarantia', $this->tipoGarantia, PDO::PARAM_STR);
        $result->bindParam(':configurable', $this->configurable, PDO::PARAM_STR);
        $result->bindParam(':grupos', $gruposDb, PDO::PARAM_STR);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return $response;
        }

        $this->conexion = null;
        return "Error con simac_spGuardarTipoGarantia";
    }

    public function eliminarTipoGarantia(int $idUsuario, string $ipActual, string $tipoApartado, int $idApartado)
    {
        $response = null;
        $query = "EXEC simac_spEliminarTipoGarantia :id,:usuario,:ip,:tipoApartado,:idApartado,:response;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            return $response;
        }

        return "Error en simac_spEliminarTipoGarantia";
    }
}

class grupoTipoGarantia
{
    public $id;
    public $nombreGrupo;
    public $fechaRegistro;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i', strtotime($this->fechaRegistro));
    }

    public function getGrupos(int $tipoGarantia)
    {
        $query = 'SELECT id,nombreGrupo,fechaRegistro from simac_viewGruposTiposGarantia where tipoGarantia = :id;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $tipoGarantia, PDO::PARAM_INT);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }
}
