<?php
class solicitudCredito
{
    public $id;
    public $agencia;
    public $nombreAgencia;
    public $usuario;
    public $fechaRegistro;
    public $ultimaRevision;
    public $fechaRecepcion;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date("d-m-Y h:i a", strtotime($this->fechaRegistro));
        !empty($this->ultimaRevision) && $this->ultimaRevision = date("d-m-Y h:i a", strtotime($this->ultimaRevision));
        !empty($this->fechaRecepcion) && $this->fechaRecepcion = date("d-m-Y h:i a", strtotime($this->fechaRecepcion));
    }

    public function getSolicitudesProceso($tipoPermiso, $idUsuario, $agencias)
    {
        $solicitudesNube = [];
        $solicitudesLocales = [];
        $permiso = $tipoPermiso == 'Administrativo' ? 'all' : 'single';

        $requestNube = curl_init();
        curl_setopt($requestNube, CURLOPT_URL, "https://solicitudes.acacypac.coop/main/code/api_solicitudesProceso.php?ag=" . implode('@@@', $agencias) . "&user=" . $idUsuario . "&p=" . $permiso);
        curl_setopt($requestNube, CURLOPT_RETURNTRANSFER, true);
        $solicitudesNube = json_decode(curl_exec($requestNube), true);
        $usuariosPendientes = [];

        foreach ($solicitudesNube as $key => $tmpSolicitud) {
            $solicitudesNube[$key] = array_merge($tmpSolicitud, ["solicitudNueva" => "S", "ultimaRevision" => "Sin registros", "montoSugerido" => $tmpSolicitud["montoSolicitado"], "fechaRecepcion" => date("d-m-Y h:i a", strtotime($tmpSolicitud["fechaRegistro"]))]);
            array_push($usuariosPendientes, $tmpSolicitud["usuario"]);
        }

        if (count($usuariosPendientes) > 0) {
            $queryUsuarios = "SELECT id,concat(nombres,' ',apellidos) as nombreUsuario from sistema_datosUsuarios where id in(" . implode(",", $usuariosPendientes) . ");";
            $resultUsuarios = $this->conexion->prepare($queryUsuarios, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultUsuarios->execute();
            $usuarios = $resultUsuarios->fetchAll(PDO::FETCH_OBJ);

            foreach ($solicitudesNube as $key => $tmpSolicitud) {
                $nombreUsuario = "";
                foreach ($usuarios as $user) {
                    if ($user->id == $tmpSolicitud["usuario"]) {
                        $nombreUsuario = $user->nombreUsuario;
                    }
                }
                $solicitudesNube[$key] = array_merge($tmpSolicitud, ["nombreUsuario" => $nombreUsuario]);
            }
        }

        $query = "SELECT * FROM simac_viewDatosMinimosSolicitudes;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();
        $solicitudesLocales = $result->fetchAll(PDO::FETCH_OBJ);
        foreach ($solicitudesLocales as $solicitud) {
            if (!empty($solicitud->fechaRecepcion) && !empty($solicitud->ultimaRevision)) {
                $tmpInicio = new DateTime($solicitud->fechaRecepcion);
                $tmpFin = new DateTime($solicitud->ultimaRevision);
                $tiempoTrabajado = '';
                $diferencia = $tmpInicio->diff($tmpFin);

                $tiempoTrabajado = $diferencia->y > 0 ? ($diferencia->y > 1 ? $tiempoTrabajado .= $diferencia->y . ' años ' : $tiempoTrabajado .= $diferencia->y . ' año ') : $tiempoTrabajado;
                $tiempoTrabajado = $diferencia->m > 0 ? ($diferencia->m > 1 ? $tiempoTrabajado .= $diferencia->m . ' meses ' : $tiempoTrabajado .= $diferencia->m . ' mes ') : $tiempoTrabajado;
                $tiempoTrabajado = $diferencia->d > 0 ? ($diferencia->d > 1 ? $tiempoTrabajado .= $diferencia->d . ' días ' : $tiempoTrabajado .= $diferencia->d . ' día ') : $tiempoTrabajado;
                $tiempoTrabajado = $diferencia->h > 0 ? ($diferencia->h > 1 ? $tiempoTrabajado .= $diferencia->h . ' horas ' : $tiempoTrabajado .= $diferencia->h . ' hora ') : $tiempoTrabajado;
                $tiempoTrabajado = $diferencia->i > 0 ? ($diferencia->i > 1 ? $tiempoTrabajado .= $diferencia->i . ' minutos ' : $tiempoTrabajado .= $diferencia->i . ' minutos ') : $tiempoTrabajado;
                $solicitud->tiempoTrabajado = $tiempoTrabajado;
            }
            !empty($solicitud->fechaRecepcion) && $solicitud->fechaRecepcion = date('d-m-Y h:i a', strtotime($solicitud->fechaRecepcion));
            !empty($solicitud->ultimaRevision) && $solicitud->ultimaRevision = date('d-m-Y h:i a', strtotime($solicitud->ultimaRevision));
            empty($solicitud->montoSugerido) && $solicitud->montoSugerido = $solicitud->montoSolicitado;
        }

        return array_merge($solicitudesNube, $solicitudesLocales);
    }

    public function downloadSolicitud($idUsuario, $idAgencia, $tipoApartado, $idApartado, $ipActual)
    {
        $response = null;
        $id = null;
        $fechaActualizacion = null;
        $queryLocal = "EXEC simac_spDescargarSolicitud :idNube,:tipoDocumento,:numeroDocumento,:nit,:nombres,:apellidos,:pais,:departamento,:municipio,:direccion,:telefonoMovil,:telefonoFijo,:email,:codigoEjecutivo,:actividadEconomica,:montoSolicitado,:destino,:usuarioAsignado,:agenciaAsignada,:fechaRecepcion,:usuario,:agencia,:ip,:tipoApartado,:idApartado,:response,:id,:fechaActualizacion;";

        $resultLocal = $this->conexion->prepare($queryLocal, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultLocal->bindParam(':idNube', $this->id, PDO::PARAM_INT);
        $resultLocal->bindParam(':tipoDocumento', $this->tipoDocumento, PDO::PARAM_INT);
        $resultLocal->bindParam(':numeroDocumento', $this->numeroDocumento, PDO::PARAM_STR);
        $resultLocal->bindParam(':nit', $this->nit, PDO::PARAM_STR);
        $resultLocal->bindParam(':nombres', $this->nombres, PDO::PARAM_STR);
        $resultLocal->bindParam(':apellidos', $this->apellidos, PDO::PARAM_STR);
        $resultLocal->bindParam(':pais', $this->pais, PDO::PARAM_INT);
        $resultLocal->bindParam(':departamento', $this->departamento, PDO::PARAM_INT);
        $resultLocal->bindParam(':municipio', $this->municipio, PDO::PARAM_INT);
        $resultLocal->bindParam(':direccion', $this->direccion, PDO::PARAM_STR);
        $resultLocal->bindParam(':telefonoMovil', $this->telefonoMovil, PDO::PARAM_STR);
        $resultLocal->bindParam(':telefonoFijo', $this->telefonoFijo, PDO::PARAM_STR);
        $resultLocal->bindParam(':email', $this->email, PDO::PARAM_STR);
        $resultLocal->bindParam(':codigoEjecutivo', $this->codigoEjecutivo, PDO::PARAM_STR);
        $resultLocal->bindParam(':actividadEconomica', $this->actividadEconomica, PDO::PARAM_INT);
        $resultLocal->bindParam(':montoSolicitado', $this->montoSolicitado, PDO::PARAM_STR);
        $resultLocal->bindParam(':destino', $this->destino, PDO::PARAM_STR);
        $resultLocal->bindParam(':usuarioAsignado', $this->usuario, PDO::PARAM_INT);
        $resultLocal->bindParam(':agenciaAsignada', $this->agencia, PDO::PARAM_INT);
        $resultLocal->bindParam(':fechaRecepcion', $this->fechaRecepcion, PDO::PARAM_STR);
        $resultLocal->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $resultLocal->bindParam(':agencia', $idAgencia, PDO::PARAM_INT);
        $resultLocal->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $resultLocal->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $resultLocal->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $resultLocal->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $resultLocal->bindParam(':id', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
        $resultLocal->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($resultLocal->execute()) {
            if ($response) {
                if ($response == 'EXITO') {
                    $requestNube = curl_init();
                    curl_setopt($requestNube, CURLOPT_URL, "https://solicitudes.acacypac.coop/main/code/api_descargaSolicitud.php?id=" . urlencode(base64_encode($this->id)) . "&fecha=" . urlencode($fechaActualizacion));
                    curl_setopt($requestNube, CURLOPT_RETURNTRANSFER, true);
                    $guardadoNube = json_decode(curl_exec($requestNube), true);
                }
                return ["respuesta" => $response, "id" => $id];
            }


            return ["respuesta" => "Error al ejecutar procedimiento almacenado", "id" => $id];
        }

        $this->conexion = null;
        return ["respuesta" => "Error en simac_spDescargarSolicitud"];
    }

    public function getSolicitud()
    {
        $query = "SELECT top(1) * FROM simac_viewDatosSolicitudes WHERE id = :id;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);

        if ($result->execute()) {
            if ($result->rowCount() > 0) {
                $solicitud = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__)[0];
                $datosSolicitante = new stdClass();
                $datosCredito = new stdClass();

                $querySolicitante = "SELECT top(1) * from simac_viewDatosSolicitantes where idSolicitud = :id;";
                $resultSolicitante = $this->conexion->prepare($querySolicitante, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultSolicitante->bindParam(':id', $this->id, PDO::PARAM_INT);
                if ($resultSolicitante->execute() && $resultSolicitante->rowCount() > 0) {
                    require_once 'simacPersona.php';
                    $datosSolicitante = $resultSolicitante->fetchAll(PDO::FETCH_CLASS, 'simacPersona')[0];
                }

                $queryCredito = "SELECT top(1) * from simac_viewDatosCreditoSolicitud where idSolicitud = :id;";
                $resultCredito = $this->conexion->prepare($queryCredito, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultCredito->bindParam(':id', $this->id, PDO::PARAM_INT);
                if ($resultCredito->execute() && $resultCredito->rowCount() > 0) {
                    require_once 'simacCredito.php';
                    $datosCredito = $resultCredito->fetchAll(PDO::FETCH_CLASS, 'simacCredito')[0];
                }

                return ["respuesta" => "EXITO", "solicitud" => $solicitud, "datosSolicitante" => $datosSolicitante, "datosCredito" => $datosCredito];
            }

            return ["respuesta" => "NoSolicitud"];
        }

        return ["respuesta" => "Error en simac_viewDatosSolicitudes", "datos" => $this->id];
    }
}
