<?php
class periodicidadPago{
    public $id;
    public $periodicidad;
    public $divisor;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function getPeriodicidadesCbo(){
        $query = "SELECT * FROM simac_viewPeriodicidadesCbo;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }
}