<?php
class simacPersona
{
    public $id;
    public $codigoCliente;
    public $tipoDocumento;
    public $numeroDocumento;
    public $nit;
    public $nombres;
    public $apellidos;
    public $conocidoPor;
    public $pais;
    public $departamento;
    public $municipio;
    public $direccion;
    public $profesion;
    public $telefonoFijo;
    public $telefonoMovil;
    public $email;
    public $fechaNacimiento;
    public $edad;
    public $actividadEconomica;
    public $sabeFirmar;
    public $fechaRegistro;
    public $fechaActualizacion;

    function __construct()
    {
        !empty($this->fechaNacimiento) && $this->fechaNacimiento = date('d-m-Y', strtotime($this->fechaNacimiento));
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
    }
}
