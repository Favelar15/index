<?php
class simacAditivo
{
    public $id;
    public $aditivo;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function getAditivosCbo()
    {
        $query = "SELECT * FROM simac_viewAditivosCbo;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }
}
