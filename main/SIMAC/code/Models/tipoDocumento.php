<?php
class simacTipoDocumento
{
    public $id;
    public $tipoDocumento;
    public $paramGeneral;
    public $paramPersonas;
    public $paramLineasFinancieras;
    public $paramTiposGarantia;
    public $fechaRegistro;
    public $fechaActualizacion;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
    }

    public function getTiposDocumentos()
    {
        $query = "SELECT * FROM simac_viewTiposDocumento";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $datos = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
            $archivoConfiguracion = __DIR__ . '/../../config/tiposDocumento.json';
            $configuracionCompleta = json_decode(file_get_contents($archivoConfiguracion));

            foreach ($datos as $key => $tipoDocumento) {
                if (isset($configuracionCompleta->{$tipoDocumento->id})) {
                    $datos[$key]->paramGeneral = $configuracionCompleta->{$tipoDocumento->id}->paramGeneral;
                    $datos[$key]->paramPersonas = $configuracionCompleta->{$tipoDocumento->id}->paramPersonas;
                    $datos[$key]->paramLineasFinancieras = $configuracionCompleta->{$tipoDocumento->id}->paramLineasFinancieras;
                    $datos[$key]->paramTiposGarantia = $configuracionCompleta->{$tipoDocumento->id}->paramTiposGarantia;
                }
            }
            return $datos;
        }

        return [];
    }

    public function saveTipoDocumento($idUsuario, $ipActual, $tipoApartado, $idApartado)
    {
        $id = null;
        $response = null;

        $query = "EXEC simac_spGuardarTipoDocumento :id,:tipoDocumento,:usuario,:ip,:tipoApartado,:idApartado,:response,:idDocumento;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':tipoDocumento', $this->tipoDocumento, PDO::PARAM_STR);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idDocumento', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            if ($response && $response == 'EXITO') {
                $archivoConfiguracion = __DIR__ . '/../../config/tiposDocumento.json';
                $configuracionCompleta = json_decode(file_get_contents($archivoConfiguracion), true);
                $configuracionActual = new stdClass();
                $configuracionActual->paramGeneral = $this->paramGeneral;
                $configuracionActual->paramPersonas = $this->paramPersonas;
                $configuracionActual->paramLineasFinancieras = $this->paramLineasFinancieras;
                $configuracionActual->paramTiposGarantia = $this->paramTiposGarantia;

                $configuracionCompleta[$id] = $configuracionActual;

                file_put_contents($archivoConfiguracion, json_encode($configuracionCompleta));
            }

            return ["respuesta" => $response, "id" => $id];
        }

        $this->conexion = null;
        return ["respuesta" => "Error en simac_spGuardarTipoDocumento"];
    }

    public function eliminarTipoDocumento($idUsuario, $ipActual, $tipoApartado, $idApartado)
    {
        return "EXITO";
    }
}
