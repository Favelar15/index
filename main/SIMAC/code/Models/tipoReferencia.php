<?php
class tipoReferencia{
    public $id;
    public $tipoReferencia;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;    
    }

    public function getTiposReferenciaCbo(){
        $query = "SELECT * FROM simac_viewTiposReferencias;";
        $result = $this->conexion->prepare($query,[PDO::ATTR_CURSOR=>PDO::CURSOR_SCROLL]);

        if($result->execute()){
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS,__CLASS__);
        }

        $this->conexion = null;
        return [];
    }
}
