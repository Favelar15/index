<?php
class lineaFinanciera
{
    public $id;
    public $lineaFinanciera;
    public $fechaRegistro;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i', strtotime($this->fechaRegistro));
    }

    public function getLineasFinancierasCbo()
    {
        $query = "SELECT id,lineaFinanciera FROM simac_viewLineasFinancieras;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

    public function getLineasFinancieras()
    {
        $query = "SELECT * FROM simac_viewLineasFinancieras;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

    public function saveLineaFinanciera(int $idUsuario, string $ipActual, string $tipoApartado, int $idApartado)
    {
        $response = null;
        $id = null;

        $query = "EXEC simac_spGuardarLineaFinanciera :id,:lineaFinanciera,:usuario,:ip,:tipoApartado,:idApartado,:response,:idLinea;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':lineaFinanciera', $this->lineaFinanciera, PDO::PARAM_STR);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idLinea', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return ["respuesta" => $response, "id" => $id];
        }

        $this->conexion = null;
        return ["respuesta" => "Error en simac_spGuardarLineaFinanciera"];
    }

    public function deleteLineaFinanciera(int $idUsuario, string $tipoApartado, int $idApartado)
    {
        $response = null;
        $query = "EXEC simac_spEliminarLineaFinanciera :id,:usuario,:tipoApartado,:idApartado,:response;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return $response;
        }

        $this->conexion = null;
        return "Error en simac_spEliminarLineaFinanciera";
    }
}
