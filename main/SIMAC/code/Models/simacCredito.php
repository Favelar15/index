<?php
class simacCredito
{
    public $id;
    public $lineaFinanciera;
    public $tasaInteres;
    public $capitalInteres;
    public $vencimiento;
    public $plazo;
    public $tipoPlazo;
    public $periodicidad;
    public $fuenteFondos;
    public $montoSolicitado;
    public $montoSugerido;
    public $destinoCredito;
    public $fechaRegistro;
    public $fechaActualizacion;

    function __construct()
    {
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
        empty($this->montoSugerido) && $this->montoSugerido = $this->montoSolicitado;
        empty($this->capitalInteres) && $this->capitalInteres = 0;
    }
}
