<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";

        $solicitud = explode("@@@", $_GET["q"]);

        if (in_array('actividadEconomica', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/actividadEconomica.php';
            $actividad = new simacActividadEconomica();
            $actividadesEconomicas = $actividad->getActividadesCbo();

            $respuesta->{"actividadesEconomicas"} = $actividadesEconomicas;
        }

        if (in_array('tiposDocumento', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../code/Models/tipoDocumento.php';
            $tipoDocumento = new tipoDocumento();
            $tiposDocumento = $tipoDocumento->obtenerTiposDocumento();

            $respuesta->{"tiposDocumento"} = $tiposDocumento;
        }

        if (in_array('geograficos', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../code/Models/datosGeograficos.php';
            $geografico = new geografico();
            $datosGeograficos = $geografico->obtenerGeograficos();

            $respuesta->{"datosGeograficos"} = $datosGeograficos;
        }

        if (in_array('lineasFinancierasCbo', $solicitud) || in_array('lineasFinancieras', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/lineaFinanciera.php';
            $linea = new lineaFinanciera();
            if (in_array('lineasFinancierasCbo', $solicitud) || in_array('all', $solicitud)) {
                $lineasFinancieras = $linea->getLineasFinancierasCbo();
            } else {
                $lineasFinancieras = $linea->getLineasFinancieras();
            }

            $respuesta->{"lineasFinancieras"} = $lineasFinancieras;
        }

        if (in_array('tiposPlazo', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/tipoPlazo.php';
            $tipo = new tipoPlazo();
            $tiposPlazo = $tipo->getTiposPlazoCbo();

            $respuesta->{"tiposPlazo"} = $tiposPlazo;
        }

        if (in_array('periodicidades', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/periodicidadPago.php';
            $periodicidad = new periodicidadPago();
            $periodicidades = $periodicidad->getPeriodicidadesCbo();

            $respuesta->{"periodicidades"} = $periodicidades;
        }

        if (in_array('fuentesFondo', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/fuenteFondos.php';
            $fuente = new fuenteFondos();
            $fuentesFondo = $fuente->getFuentesFondosCbo();

            $respuesta->{"fuentesFondo"} = $fuentesFondo;
        }

        if (in_array('aditivos', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/aditivo.php';
            $aditivo = new simacAditivo();
            $aditivos = $aditivo->getAditivosCbo();

            $respuesta->{"aditivos"} = $aditivos;
        }

        if (in_array('tiposReferencia', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/tipoReferencia.php';
            $tipo = new tipoReferencia();
            $tiposReferencia = $tipo->getTiposReferenciaCbo();

            $respuesta->{"tiposReferencia"} = $tiposReferencia;
        }

        if (in_array('tiposGarantia', $solicitud) || in_array('tiposGarantiaCbo', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/tipoGarantia.php';
            $tipo = new tipoGarantia();
            if (in_array('tiposGarantiaCbo', $solicitud) || in_array('all', $solicitud)) {
            } else {
                $tiposGarantia = $tipo->getTiposGarantia();
            }

            $respuesta->{"tiposGarantia"} = $tiposGarantia;
        }

        if (in_array('simacTiposDocumentos', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/tipoDocumento.php';
            $tipoDocumento = new simacTipoDocumento();
            $tiposDocumentos = $tipoDocumento->getTiposDocumentos();

            $respuesta->{"simacTiposDocumentos"} = $tiposDocumentos;
        }

        $conexion = null;
    }
}

echo json_encode($respuesta);
