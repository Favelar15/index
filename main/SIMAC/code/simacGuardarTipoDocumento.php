<?php
include "../../code/generalParameters.php";
$respuesta = (object)[];
session_start();

if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (!empty($_POST)) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/tipoDocumento.php';

        $idUsuario = $_SESSION["index"]->id;
        $tipoApartado = $_POST["tipoApartado"];
        $idApartado = base64_decode(urldecode($_POST["idApartado"]));
        $gruposConfiguracion = json_decode($_POST["gruposConfiguracion"]);
        $id = $_POST["id"];

        $tipoDocumento = new simacTipoDocumento();
        $tipoDocumento->id = $id;
        $tipoDocumento->tipoDocumento = $_POST["tipoDocumento"];
        $tipoDocumento->paramGeneral = $gruposConfiguracion->{"general"};
        $tipoDocumento->paramPersonas = $gruposConfiguracion->{"personas"};
        $tipoDocumento->paramLineasFinancieras = $gruposConfiguracion->{"lineasFinancieras"};
        $tipoDocumento->paramTiposGarantia = $gruposConfiguracion->{"tiposGarantias"};

        $guardar = $tipoDocumento->saveTipoDocumento($idUsuario, $ipActual, $tipoApartado, $idApartado);

        if (isset($guardar["respuesta"])) {
            $respuesta->{"respuesta"} = $guardar["respuesta"];
            if ($guardar["respuesta"] == "EXITO") {
                if (!empty($_FILES)) {
                    foreach ($_FILES as $key => $archivo) {
                        $nombreArchivo = $archivo["name"];
                        if (file_exists(__DIR__ . '/../docs/referencias/' . $nombreArchivo)) {
                            unlink(__DIR__ . '/../docs/referencias/' . $nombreArchivo);
                        }

                        move_uploaded_file($archivo['tmp_name'], __DIR__ . '/../docs/referencias/' . $nombreArchivo);
                    }
                }
            }

            $respuesta->{"respuesta"} = $guardar["respuesta"];
            isset($guardar["id"]) && $respuesta->{"id"} = $guardar["id"];
        } else {
            $respuesta->{"respuesta"} = "Error al guardar en la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
