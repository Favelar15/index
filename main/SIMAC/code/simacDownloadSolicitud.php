<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    include "../../code/connectionSqlServer.php";
    require_once './Models/solicitudCredito.php';

    $idUsuario = $_SESSION["index"]->id;
    $idAgencia = $_SESSION["index"]->agenciaActual->id;
    $tipoApartado = $input["tipoApartado"];
    $idApartado = base64_decode(urldecode($input["idApartado"]));

    $solicitud = new solicitudCredito();
    $solicitud->id = $input["solicitud"]["id"];
    $solicitud->tipoDocumento = $input["solicitud"]["tipoDocumento"];
    $solicitud->numeroDocumento = $input["solicitud"]["numeroDocumento"];
    $solicitud->nit = $input["solicitud"]["nit"];
    $solicitud->nombres = $input["solicitud"]["nombres"];
    $solicitud->apellidos = $input["solicitud"]["apellidos"];
    $solicitud->pais = $input["solicitud"]["pais"];
    $solicitud->departamento = $input["solicitud"]["departamento"];
    $solicitud->municipio = $input["solicitud"]["municipio"];
    $solicitud->direccion = $input["solicitud"]["direccion"];
    $solicitud->telefonoMovil = $input["solicitud"]["telefonoMovil"];
    $solicitud->telefonoFijo = $input["solicitud"]["telefonoFijo"];
    $solicitud->email = $input["solicitud"]["email"];
    $solicitud->codigoEjecutivo = $input["solicitud"]["codigoEjecutivo"];
    $solicitud->actividadEconomica = $input["solicitud"]["actividadEconomica"];
    $solicitud->montoSolicitado = $input["solicitud"]["montoSolicitado"];
    $solicitud->destino = $input["solicitud"]["destino"];
    $solicitud->usuario = $input["solicitud"]["usuario"];
    $solicitud->agencia = $input["solicitud"]["agencia"];
    $solicitud->fechaRecepcion = date("Y-m-d H:i:s", strtotime($input["solicitud"]["fechaRecepcion"]));

    $descargar = $solicitud->downloadSolicitud($idUsuario, $idAgencia, $tipoApartado, $idApartado, $ipActual);

    $respuesta->{"respuesta"} = $descargar["respuesta"];
    isset($descargar["id"]) && $respuesta->{"id"} = $descargar["id"];

    $conexion = null;
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
