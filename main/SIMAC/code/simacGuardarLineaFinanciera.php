<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/lineaFinanciera.php';

        $idUsuario = $_SESSION["index"]->id;
        $tipoApartado = $input["tipoApartado"];
        $idApartado = base64_decode(urldecode($input["idApartado"]));
        $id = $input["id"];

        $lineaFinanciera = new lineaFinanciera();
        $lineaFinanciera->id = $input["id"];
        $lineaFinanciera->lineaFinanciera = $input["lineaFinanciera"];

        $guardar = $lineaFinanciera->saveLineaFinanciera($idUsuario, $ipActual, $tipoApartado, $idApartado);

        if (isset($guardar["respuesta"])) {
            $respuesta->{"respuesta"} = $guardar["respuesta"];
            isset($guardar["id"]) && $respuesta->{"id"} = $guardar["id"];
        } else {
            $respuesta->{"respuesta"} = "Error al actualizar la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
