<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET)) {
        include "../../code/connectionSqlServer.php";

        require_once "./Models/solicitudCredito.php";

        $idUsuario = $_SESSION["index"]->id;
        $tipoApartado = $_GET["tipo"];
        $idApartado = base64_decode(urldecode($_GET["apartado"]));
        $solicitados = explode("@@@", $_GET["q"]);

        $vista = $_GET["tipo"] == 'Sub' ? 'general_viewRolesSubApartados' : 'general_viewRolesApartados';
        $queryRoles = "SELECT top(1) * from " . $vista . " WHERE id = " . $idApartado;
        $resultRoles = $conexion->prepare($queryRoles, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultRoles->execute();
        $dataRoles = $resultRoles->fetch(PDO::FETCH_ASSOC);
        $roles = explode('@@@', $dataRoles["roles"]);
        $agencias = [];

        $tipoPermiso = 'Local';

        foreach ($_SESSION["index"]->roles as $rol) {
            if (in_array($rol->id, $roles)) {
                if ($rol->tipo == 'Administrativo') {
                    $tipoPermiso = 'Administrativo';
                    break;
                }
            }
        }

        foreach ($_SESSION["index"]->agencias as $agencia) {
            array_push($agencias, $agencia->id);
        }

        $solicitud = new solicitudCredito();

        if (in_array('proceso', $solicitados) || in_array('all', $solicitados)) {
            $solicitudesProceso = $solicitud->getSolicitudesProceso($tipoPermiso, $idUsuario, $agencias);
            $respuesta->{"solicitudesProceso"} = $solicitudesProceso;
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
