<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = [];

session_start();
if (isset($_SESSION['indexAccess']) && $_SESSION['indexAccess']->sesionUnica == 'S') {
    $idUsuarioActual = $_SESSION['indexAccess']->identificador;
    $idAgenciaActual = $_SESSION['indexAccess']->identificadorAgenciaActual;
    if (count($input) > 0) {
        $input['usuarioActual'] = $idUsuarioActual;
        $input['agenciaActual'] = $idAgenciaActual;
        $input['ipOrdenador'] = $ipActual;

        $requestGuardado = curl_init();
        curl_setopt($requestGuardado, CURLOPT_URL, "http://localhost/acacypac-new/main/SIMAC/code/simacGuardarSolicitud.php");
        curl_setopt($requestGuardado, CURLOPT_POSTFIELDS, json_encode($input));
        curl_setopt($requestGuardado, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($requestGuardado, CURLOPT_RETURNTRANSFER, true);

        $response = json_decode(curl_exec($requestGuardado), true);

        $respuesta['respuesta'] = $response['respuesta'];
        if ($response['respuesta'] == 'EXITO') {
            $idSolicitud = $response['idSolicitud'];
            $idLog = $response['idLog'];
            $fechaActualizacion = $response['fechaActualizacion'];

            $respuesta["idSolicitud"] = $idSolicitud;
            $respuesta["idLog"] = $idLog;
            $respuesta["fechaActualizacion"] = $fechaActualizacion;

            $requestNube = curl_init();
            curl_setopt($requestNube, CURLOPT_URL, "https://solicitudes.acacypac.coop/main/code/api_descargaSolicitud.php?id=" . urlencode(base64_encode($input["id"])) . "&fecha=" . urlencode($fechaActualizacion));
            curl_setopt($requestNube, CURLOPT_RETURNTRANSFER, true);
            $guardadoNube = json_decode(curl_exec($requestNube), true);
        }
    }
} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);
