<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$response = [];

if (count($input)) {
    include "../../code/connectionSqlServer.php";
    include "../../code/generalParameters.php";

    $idSolicitud = $input['solicitudNueva'] == 'S' ? 0 : $input['id'];
    $tipoDocumentoPrimario = $input['tipoDocumento'];
    $numeroDocumentoPrimario = $input['numeroDocumento'];
    $tipoDocumentoSecundario = isset($input['tipoDocumentoSecundario']) ? $input['tipoDocumentoSecundario'] : 2;
    $numeroDocumentoSecundario = isset($input['numeroDocumentoSecundario']) ? $input['numeroDocumentoSecundario'] : $input['nit'];
    $nombres = $input['nombres'];
    $apellidos = $input['apellidos'];
    $conocido = isset($input['conocido']) ? $input['conocido'] : null;
    $pais = $input['pais'];
    $departamento = $input['departamento'];
    $municipio = $input['municipio'];
    $direccion = $input['direccion'];
    $telefonoMovil = $input['telefonoMovil'];
    $telefonoFijo = $input['telefonoFijo'];
    $email = $input['email'];
    $codigoEjecutivo = $input['codigoEjecutivo'];
    $actividadEconomica = $input['actividadEconomica'];
    $montoSolicitado = $input['montoSolicitado'];
    $montoSugerido = isset($input['montoSugerido']) ? $input['montoSugerido'] : $input['montoSolicitado'];
    $destino = $input['destino'];
    $usuario = $input['usuarioActual'];
    $agencia = $input['agenciaActual'];
    $estado = isset($input['estado']) ? $input['estado'] : 'PROCESO';
    $fechaRecepcion = date('Y-m-d H:i', strtotime($input['fechaRegistro']));
    $idNube = $input['solicitudNueva'] == 'S' ? $input['id'] : 0;
    $usuarioNube = isset($input['usuario']) ? $input['usuario'] : 0;
    $agenciaNube = isset($input['agencia']) ? $input['agencia'] : 0;
    $descripcionAccion = isset($input['accion']) ? $input['accion'] : 'Descarga de solicitud';
    $ipActual = $input['ipOrdenador'];

    $query = "EXEC simac_spGuardarSolicitud :id,:tipoDocumentoPrimario,:numeroDocumentoPrimario,:tipoDocumentoSecundario,:numeroDocumentoSecundario,:nombres,:apellidos,:conocido,:pais,:departamento,:municipio,:direccion,:telefonoMovil,:telefonoFijo,:email,:codigoEjecutivo,:actividadEconomica,:montoSolicitado,:montoSugerido,:destino,:usuario,:agencia,:estado,:fechaRecepcion,:idNube,:usuarioNube,:agenciaNube,:descripcion,:ip;";

    $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
    $result->bindParam(':id', $idSolicitud, PDO::PARAM_INT);
    $result->bindParam(':tipoDocumentoPrimario', $tipoDocumentoPrimario, PDO::PARAM_INT);
    $result->bindParam(':numeroDocumentoPrimario', $numeroDocumentoPrimario, PDO::PARAM_STR);
    $result->bindParam(':tipoDocumentoSecundario', $tipoDocumentoSecundario, PDO::PARAM_INT);
    $result->bindParam(':numeroDocumentoSecundario', $numeroDocumentoSecundario, PDO::PARAM_STR);
    $result->bindParam(':nombres', $nombres, PDO::PARAM_STR);
    $result->bindParam(':apellidos', $apellidos, PDO::PARAM_STR);
    $result->bindParam(':conocido', $conocido, PDO::PARAM_STR);
    $result->bindParam(':pais', $pais, PDO::PARAM_INT);
    $result->bindParam(':departamento', $departamento, PDO::PARAM_INT);
    $result->bindParam(':municipio', $municipio, PDO::PARAM_INT);
    $result->bindParam(':direccion', $direccion, PDO::PARAM_STR);
    $result->bindParam(':telefonoMovil', $telefonoMovil, PDO::PARAM_STR);
    $result->bindParam(':telefonoFijo', $telefonoFijo, PDO::PARAM_STR);
    $result->bindParam(':email', $email, PDO::PARAM_STR);
    $result->bindParam(':codigoEjecutivo', $codigoEjecutivo, PDO::PARAM_STR);
    $result->bindParam(':actividadEconomica', $actividadEconomica, PDO::PARAM_INT);
    $result->bindParam(':montoSolicitado', $montoSolicitado, PDO::PARAM_STR);
    $result->bindParam(':montoSugerido', $montoSugerido, PDO::PARAM_STR);
    $result->bindParam(':destino', $destino, PDO::PARAM_STR);
    $result->bindParam(':usuario', $usuario, PDO::PARAM_INT);
    $result->bindParam(':agencia', $agencia, PDO::PARAM_INT);
    $result->bindParam(':estado', $estado, PDO::PARAM_STR);
    $result->bindParam(':fechaRecepcion', $fechaRecepcion, PDO::PARAM_STR);
    $result->bindParam(':idNube', $idNube, PDO::PARAM_INT);
    $result->bindParam(':usuarioNube', $usuarioNube, PDO::PARAM_INT);
    $result->bindParam(':agenciaNube', $agenciaNube, PDO::PARAM_INT);
    $result->bindParam(':descripcion', $descripcionAccion, PDO::PARAM_STR);
    $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);

    $result->execute();

    $data = $result->fetch(PDO::FETCH_ASSOC);
    if (isset($data['respuesta'])) {
        $response['respuesta'] = $data['respuesta'];
        if ($data['respuesta'] == "EXITO") {
            $response['idSolicitud'] = isset($data['id']) ? $data['id'] : 0;
            $response['idLog'] = isset($data['idLog']) ? $data['idLog'] : 0;
            $response['fechaActualizacion'] = isset($data['fechaActualizacion']) ? $data['fechaActualizacion'] : 'Sin registros';
        }
    } else {
        $response['respuesta'] = implode("", $data);
    }

    $conexion = null;
} else {
    $response['respuesta'] = "NoDatos";
}

echo json_encode($response);
