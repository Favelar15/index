<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/tipoGarantia.php';

        $idUsuario = $_SESSION["index"]->id;
        $tipoApartado = $input["tipoApartado"];
        $idApartado = base64_decode(urldecode($input["idApartado"]));

        $tipoGarantia = new tipoGarantia();
        $tipoGarantia->id = $input["id"];
        $tipoGarantia->tipoGarantia = $input["tipoGarantia"];
        $tipoGarantia->configurable = $input["configurable"];
        $tipoGarantia->grupos = $input["grupos"];

        $guardar = $tipoGarantia->saveTipoGarantia($idUsuario, $ipActual, $tipoApartado, $idApartado);

        if ($guardar) {
            $respuesta->{"respuesta"} = $guardar;
        } else {
            $respuesta->{"respuesta"} = "Error al guardar en la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
