<div class="pagetitle">
    <h1>Administración de tipos de garantía</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Simac</li>
            <li class="breadcrumb-item">Administración</li>
            <li class="breadcrumb-item active">Tipos de garantía</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="tiposGarantia-tab" data-bs-toggle="tab" data-bs-target="#tiposGarantia" role="tab" aria-controls="tiposGarantia" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesPrincipal')">
                Tipos de garantía
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="configuracion-tab" data-bs-toggle="tab" data-bs-target="#configuracion" role="tab" aria-controls="configuracion" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesConfiguracion')">
                Configuración
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="botonesPrincipal">
                <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configTipoGarantia.edit();">
                    <i class="fas fa-plus-circle"></i> Tipo de garantía
                </button>
            </div>
            <div class="tabButtonContainer" id="botonesConfiguracion" style="display: none;">
                <button type="submit" class="btn btn-sm btn-outline-success ml-2" form="frmTipoGarantia">
                    <i class="fas fa-save"></i> Guardar
                </button>
                <button type="button" class="btn btn-sm btn-outline-danger ml-2" onclick="javascript:configTipoGarantia.cancel();">
                    <i class="fas fa-times"></i> Cancelar
                </button>
            </div>
        </li>
    </ul>
    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="tiposGarantia" role="tabpanel" aria-labelledby="tiposGarantia-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblTiposGarantia" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Tipo de garantía</th>
                                <th>¿Configurable?</th>
                                <th>¿Incluye grupos?</th>
                                <th>¿En desarrollo?</th>
                                <th>Fecha de registro</th>
                                <th>Última modificación</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="configuracion" role="tabpanel" aria-labelledby="configuracion-tab">
            <div class="row">
                <form id="frmTipoGarantia" class="needs-validation" novalidate name="frmTipoGarantia" method="POST" accept-charset="utf-8" action="javascript:configTipoGarantia.save();">
                    <div class="col-lg-12 col-xl-12">
                        <div class="row">
                            <div class="col-lg-9 col-xl-9 mayusculas">
                                <label class="form-label" for="txtTipoGarantia">Tipo de garantía <span class="requerido">*</span></label>
                                <input type="text" id="txtTipoGarantia" name="txtTipoGarantia" class="form-control" required placeholder="tipo de garantía">
                                <div class="invalid-feedback">
                                    Ingrese el tipo de garantía
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboConfigurable">¿Configurable? <span class="requerido">*</span></label>
                                <select id="cboConfigurable" name="cboConfigurable" class="form-select">
                                    <option selected value="N">NO</option>
                                    <option value="S">SI</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione una opción
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-lg-12 col-xl-12 mt-2">
                    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                        <li class="nav-item">
                            <button class="nav-link active" id="grupos-tab" data-bs-toggle="tab" data-bs-target="#grupos" role="tab" aria-controls="grupos" aria-selected="true">
                                Grupos
                            </button>
                        </li>
                        <li class="nav-item ms-auto">
                            <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configGrupo.edit();">
                                <i class="fas fa-plus-circle"></i> Grupo
                            </button>
                        </li>
                    </ul>
                    <div class="tab-content" style="margin-top: 20px;">
                        <div class="tab-pane fade show active" id="tiposGarantia" role="tabpanel" aria-labelledby="tiposGarantia-tab">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <table id="tblGrupos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Grupo</th>
                                                <th>Fecha de registro</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="mdlGrupo" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmGrupo" name="frmGrupo" class="needs-validation" novalidate accept-charset="utf-8" method="POST" action="javascript:configGrupo.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Grupo</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Nombre de grupo -->
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for='txtGrupo'>Nombre de grupo <span class="requerido">*</span></label>
                            <input type="text" id="txtGrupo" name="txtGrupo" placeholder="nombre de grupo" class="form-control" required>
                            <div class="invalid-feedback">
                                Ingrese el nombre del grupo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$_GET["js"] = ["simacAdministrarTiposGarantia"];
