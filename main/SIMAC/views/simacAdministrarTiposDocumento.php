<div class="pagetitle">
    <h1>Administración de tipos de documento</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Simac</li>
            <li class="breadcrumb-item">Administración</li>
            <li class="breadcrumb-item active">Tipos de documento</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="catalogo-tab" data-bs-toggle="tab" data-bs-target="#catalogo" role="tab" aria-controls="catalogo" aria-selected="true" onclick="generalShowHideButtonTab('principalTabButtonContainer','btnCatalogo');">
                Catálogo
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="configuracion-tab" data-bs-toggle="tab" data-bs-target="#configuracion" role="tab" aria-controls="configuracion" aria-selected="true" onclick="generalShowHideButtonTab('principalTabButtonContainer','btnConfiguracion');">
                Configuración
            </button>
        </li>
        <li class="nav-item ms-auto" id="principalTabButtonContainer">
            <div class="tabButtonContainer" id="btnCatalogo">
                <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configTipoDocumento.edit();">
                    <i class="fas fa-plus-circle"></i> Tipo de documento
                </button>
            </div>
            <div class="tabButtonContainer" id="btnConfiguracion" style="display: none;">
                <button type="submit" form="frmTipoDocumento" class="btn btn-sm btn-outline-success ml-2">
                    <i class="fas fa-save"></i> Guardar
                </button>
                <button type="button" class="btn btn-sm btn-outline-danger ml-2" onclick="javascript:configTipoDocumento.cancel();">
                    <i class="fas fa-times"></i> Cancelar
                </button>
            </div>
        </li>
    </ul>
    <div class="tab-content mt-2">
        <div class="tab-pane fade show active" id="catalogo" role="tabpanel" aria-labelledby="catalogo-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblCatalogo" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Tipo de documento</th>
                                <th>Asignaciones</th>
                                <th>Fecha de registro</th>
                                <th>Última modificación</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="configuracion" role="tabpanel" aria-labelledby="configuracion-tab">
            <div class="row">
                <form id="frmTipoDocumento" name="frmTipoDocumento" class="needs-validation" novalidate method="POST" accept-charset="utf-8" action="javascript:configTipoDocumento.save();">
                    <div class="col-lg-12 col-xl-12 mayusculas">
                        <label class="form-label" for="txtTipoDocumento">Tipo de documento <span class="requerido">*</span></label>
                        <input type="text" id="txtTipoDocumento" name="txtTipoDocumento" class="form-control" required placeholder="tipo de documento">
                        <div class="invalid-feedback">
                            Ingrese el tipo de documento
                        </div>
                    </div>
                </form>
                <div class="col-lg-12 col-xl-12">
                    <fieldset class="border p-1 mt-2">
                        <legend class="w-auto float-none">Grupos de configuración</legend>
                        <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                            <li class="nav-item">
                                <button class="nav-link active" id="configGeneral-tab" data-bs-toggle="tab" data-bs-target="#configGeneral" role="tab" aria-controls="configGeneral" aria-selected="true" onclick="generalShowHideButtonTab('configButtonsContainer','btnConfigGeneral');">
                                    General
                                </button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" id="configPersona-tab" data-bs-toggle="tab" data-bs-target="#configPersona" role="tab" aria-controls="configPersona" aria-selected="true" onclick="generalShowHideButtonTab('configButtonsContainer','btnConfigPersona');">
                                    Persona
                                </button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" id="configLineaFinanciera-tab" data-bs-toggle="tab" data-bs-target="#configLineaFinanciera" role="tab" aria-controls="configLineaFinanciera" aria-selected="true" onclick="generalShowHideButtonTab('configButtonsContainer','btnConfigLineaFinanciera');">
                                    Línea financiera
                                </button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" id="configTipoGarantia-tab" data-bs-toggle="tab" data-bs-target="#configTipoGarantia" role="tab" aria-controls="configTipoGarantia" aria-selected="true" onclick="generalShowHideButtonTab('configButtonsContainer','btnConfigTipoGarantia');">
                                    Tipo de garantía
                                </button>
                            </li>
                            <li class="nav-item ms-auto" id="configButtonsContainer">
                                <div id="btnConfigGeneral" class="tabButtonContainer"></div>
                                <div id="btnConfigPersona" class="tabButtonContainer" style="display: none;">
                                    <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="configGrupoPersona.edit();">
                                        <i class="fas fa-plus-circle"></i>
                                        Persona
                                    </button>
                                </div>
                                <div id="btnConfigLineaFinanciera" class="tabButtonContainer" style="display: none;">
                                    <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="configGrupoLineaFinanciera.edit();">
                                        <i class="fas fa-plus-circle"></i>
                                        Línea financiera
                                    </button>
                                </div>
                                <div id="btnConfigTipoGarantia" class="tabButtonContainer" style="display: none;">
                                    <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="configGrupoTipoGarantia.edit();">
                                        <i class="fas fa-plus-circle"></i>
                                        Tipo de garantía
                                    </button>
                                </div>
                            </li>
                        </ul>
                        <div class="tab-content mt-2">
                            <div class="tab-pane fade show active" id="configGeneral" role="tabpanel" aria-labelledby="configGeneral-tab">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="chkObligatorio">
                                            <label class="form-check-label" for="chkObligatorio">¿Documento obligatorio?</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-xl-4">
                                        <div class="form-check">
                                            <input type="radio" id="rbImpReferencia" name="rbTipoImpresion" onchange="configPrintMode.evalPrintMode('printModeGeneral','containerImpReferencia');" class="form-check-input">
                                            <label class="form-check-label" for="rbImpReferencia">Impresión de referencia</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-xl-4">
                                        <div class="form-check">
                                            <input type="radio" id="rbImpSistema" name="rbTipoImpresion" class="form-check-input" onchange="configPrintMode.evalPrintMode('printModeGeneral','containerImpSistema');">
                                            <label class="form-check-label" for="rbImpSistema">Impresión de sistema</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xl-12">
                                        <fieldset class="border p-1" style="padding-top: 0;">
                                            <legend class="w-auto float-none mb-0">Configuración de impresión</legend>
                                            <div class="row" id="printModeGeneral">
                                                <div class="col-lg-12 col-xl-12 printConfigContainer" id="containerImpReferencia" style="display:none;">
                                                    <div class="row">
                                                        <div class="col-lg-7 col-xl-7">
                                                            <iframe class="pdfContainer" id="generalPDFViewer"></iframe>
                                                        </div>
                                                        <div class="col-lg-5 col-xl-5" align="center">
                                                            <button class="btn btn-sm btn-outline-secondary" type="button" style="min-width: 200px;" onclick="generalActivarUpload('filePDF')">
                                                                <i class="flaticon-download"></i> Seleccionar archivo
                                                            </button><br /><span class="fileName" frameTarget="generalPDFViewer">No hay archivo seleccionado</span>
                                                            <input type="file" id="filePDF" class="fileUploader" accept="application/pdf" style="display: none;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-xl-12 printConfigContainer" style="display: none;" id="containerImpSistema">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-xl-4">
                                                            <div class="form-check">
                                                                <input type="checkbox" class="form-check-input" id="chkConfigurable">
                                                                <label class="form-check-label" for="chkConfigurable">¿Documento configurable?</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-xl-4">
                                                            <div class="form-check">
                                                                <input type="checkbox" class="form-check-input chkPermiteAdjuntos" id="chkAdjuntos" onchange="configPrintMode.evalPermiteAdjuntos(this,'chkAdjuntosObligatorios');">
                                                                <label class="form-check-label" for="chkAdjuntos">¿Permite documentos adjuntos?</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-xl-4">
                                                            <div class="form-check">
                                                                <input type="checkbox" class="form-check-input" id="chkAdjuntosObligatorios">
                                                                <label class="form-check-label" for="chkAdjuntosObligatorios">¿Adjuntos obligatorios?</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="configPersona" role="tabpanel" aria-labelledby="configPersona-tab">
                                <table id="tblPersonas" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Persona</th>
                                            <th>Fecha de registro</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="configLineaFinanciera" role="tabpanel" aria-labelledby="configLineaFinanciera-tab">
                                <table id="tblLineasFinancieras" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Línea financiera</th>
                                            <th>Fecha de registro</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="configTipoGarantia" role="tabpanel" aria-labelledby="configTipoGarantia-tab">
                                <table id="tblTiposGarantia" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Tipo de garantía</th>
                                            <th>Sub grupo</th>
                                            <th>Fecha de registro</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="mdlPersona" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmPersona" name="frmPersona" class="needs-validation" novalidate accept-charset="utf-8" method="POST" action="javascript:configGrupoPersona.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Grupo persona</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- persona -->
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for='cboPersona'>Persona <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboPersona" name="cboPersona" class="form-control selectpicker" data-live-search="true" required>
                                    <option selected disabled value="0">seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlLineaFinanciera" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmLineaFinanciera" class="needs-validation" novalidate name="frmLineaFinanciera" accept-charset="utf-8" method="POST" action="javascript:configGrupoLineaFinanciera.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Grupo línea financiera</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Línea financiera -->
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for='cboLineaFinanciera'>Línea Financiera <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboLineaFinanciera" name="cboLineaFinanciera" class="form-control selectpicker" data-live-search="true" required>
                                    <option selected disabled value="0">seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlTipoGarantia" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmTipoGarantia" class="needs-validation" novalidate name="frmTipoGarantia" accept-charset="utf-8" method="POST" action="javascript:configGrupoTipoGarantia.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Grupo tipo de garantía</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Tipo de garantía -->
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for='cboTipoGarantia'>Tipo de garantía <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboTipoGarantia" name="cboTipoGarantia" class="form-control selectpicker" required data-live-search="true" onchange="configGrupoTipoGarantia.evalTipoGarantia(this.value);">
                                    <option selected disabled value="0">seleccione</option>
                                </select>
                            </div>
                        </div>
                        <!-- Sub grupo -->
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for='cboGrupoTipoGarantia'>Sub grupo <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboGrupoTipoGarantia" name="cboGrupoTipoGarantia" class="form-control selectpicker" data-live-search="true">
                                    <option selected disabled value="0">seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlConfigDoc" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-xl">
        <form id="frmParametersDoc" class="needs-validation" novalidate name="frmParametersDoc" accept-charset="utf-8" method="POST" action="javascript:configParamDoc.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Parámetros de documento</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="chkMdlObligatorio">
                                <label class="form-check-label" for="chkMdlObligatorio">¿Documento obligatorio?</label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-check">
                                <input type="radio" id="rbMdlImpReferencia" name="rbMdlTipoImpresion" onchange="configPrintMode.evalPrintMode('printModeGrupo','containerMdlImpReferencia');" class="form-check-input">
                                <label class="form-check-label" for="rbMdlImpReferencia">Impresión de referencia</label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-check">
                                <input type="radio" id="rbMdlImpSistema" name="rbMdlTipoImpresion" class="form-check-input" onchange="configPrintMode.evalPrintMode('printModeGrupo','containerMdlImpSistema');">
                                <label class="form-check-label" for="rbMdlImpSistema">Impresión de sistema</label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-12">
                            <fieldset class="border p-1" style="padding-top: 0;">
                                <legend class="w-auto mb-0 float-none">Configuración de impresión</legend>
                                <div class="row" id="printModeGrupo">
                                    <div class="col-lg-12 col-xl-12 printConfigContainer" id="containerMdlImpReferencia" style="display:none;">
                                        <div class="row">
                                            <div class="col-lg-7 col-xl-7">
                                                <iframe class="pdfContainer" id="mdlPDFViewer"></iframe>
                                            </div>
                                            <div class="col-lg-5 col-xl-5" align="center">
                                                <button class="btn btn-sm btn-outline-secondary" type="button" style="min-width: 200px;" onclick="generalActivarUpload('mdlfilePDF')">
                                                    <i class="flaticon-download"></i> Seleccionar archivo
                                                </button><br /><span class="fileName" frameTarget="mdlPDFViewer">No hay archivo seleccionado</span>
                                                <input type="file" id="mdlfilePDF" class="fileUploader" accept="application/pdf" style="display: none;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xl-12 printConfigContainer" style="display: none;" id="containerMdlImpSistema">
                                        <div class="row">
                                            <div class="col-lg-4 col-xl-4">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="chkMdlConfigurable">
                                                    <label class="form-check-label" for="chkMdlConfigurable">¿Documento configurable?</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-xl-4">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input chkPermiteAdjuntos" id="chkMdlAdjuntos" onchange="configPrintMode.evalPermiteAdjuntos(this,'chkMdlAdjuntosObligatorios');">
                                                    <label class="form-check-label" for="chkMdlAdjuntos">¿Permite documentos adjuntos?</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-xl-4">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="chkMdlAdjuntosObligatorios">
                                                    <label class="form-check-label" for="chkMdlAdjuntosObligatorios">¿Adjuntos obligatorios?</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$_GET["js"] = ["simacAdministrarTiposDocumento"];
