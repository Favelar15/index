<div class="pagetitle">
    <h1>Catálogos generales de SIMAC</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Simac</li>
            <li class="breadcrumb-item">Administración</li>
            <li class="breadcrumb-item active">Catálogos generales</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-6 col-xl-6">
            <fieldset class="border p-1" style="padding-top: 0;">
                <legend class="w-auto float-none mb-0">Líneas financieras</legend>
                <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                    <li class="nav-item">
                        <button class="nav-link active" id="lineasFinancieras-tab" data-bs-toggle="tab" data-bs-target="#lineasFinancieras" role="tab" aria-controls="lineasFinancieras" aria-selected="true">
                            Catálogo actual
                        </button>
                    </li>
                    <li class="nav-item ms-auto">
                        <button type="button" onclick="configLineaFinanciera.edit();" class="btn btn-outline-primary btn-sm">
                            <i class="fa fa-plus-circle"></i> Línea financiera
                        </button>
                    </li>
                </ul>
                <div class="tab-content" style="margin-top: 10px;">
                    <div class="tab-pane fade show active" id="lineasFinancieras" role="tabpanel" aria-labelledby="lineasFinancieras-tab">
                        <table id="tblLineasFinancieras" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                            <thead>
                                <th>N°</th>
                                <th>Línea financiera</th>
                                <th>Fecha de registro</th>
                                <th>Acciones</th>
                            </thead>

                            <body>
                            </body>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-6 col-xl-6">
            <fieldset class="border p-1" style="padding-top: 0;">
                <legend class="w-auto float-none">Aditivos</legend>
            </fieldset>
        </div>
    </div>
</section>
<div class="modal fade" id="mdlLineaFinanciera" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmLineaFinanciera" name="frmLineaFinanciera" class="needs-validation" novalidate accept-charset="utf-8" method="POST" action="javascript:configLineaFinanciera.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Configuración de línea financiera</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Línea financiera -->
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label for='txtLineaFinanciera' class="form-label">Línea Financiera <span class="requerido">*</span></label>
                            <input type="text" id="txtLineaFinanciera" name="txtLineaFinanciera" class="form-control" required>
                            <div class="invalid-feedback">
                                Ingrese la línea financiera
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$_GET["js"] = ["simacAdministrarCatalogos"];
