<div class="pagetitle">
    <h1>Solicitudes de crédito</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Simac</li>
            <li class="breadcrumb-item active">Solicitudes</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="solicitudes-tab" data-bs-toggle="tab" data-bs-target="#solicitudesProceso" role="tab" aria-controls="solicitudes" aria-selected="true">
                En proceso
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="evaluacion-tab" data-bs-toggle="tab" data-bs-target="#solicitudesEvaluacion" role="tab" aria-controls="evaluacion" aria-selected="true">
                En evaluación
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="desembolso-tab" data-bs-toggle="tab" data-bs-target="#solicitudesDesembolso" role="tab" aria-controls="desembolso" aria-selected="false">
                A desembolso
            </button>
        </li>
    </ul>
    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="solicitudesProceso" role="tabpanel" aria-labelledby="solicitudes-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblSolicitudesProceso" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Nombre solicitante</th>
                                <th>Actividad económica</th>
                                <th>Destino</th>
                                <th>Monto</th>
                                <th>Ejecutivo</th>
                                <th>Agencia</th>
                                <th>Fecha de recepción</th>
                                <th>Fecha última revisión</th>
                                <th>Tiempo trabajado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="solicitudesEvaluacion" role="tabpanel" aria-labelledby="evaluacion-tab">
            <h1>Evaluación</h1>
        </div>
        <div class="tab-pane fade" id="solicitudesDesembolso" role="tabpanel" aria-labelledby="desembolso-tab">
            <h1>Desembolso</h1>
        </div>
    </div>
</section>
<?php
$_GET["js"] = ["simacSolicitudesEjecutivo"];
