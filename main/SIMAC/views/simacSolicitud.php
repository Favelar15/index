<?php
if (isset($_GET['id'])) {
?>
    <div class="pagetitle">
        <div class="row">
            <div class="col">
                <h1>Solicitud de crédito</h1>
            </div>
            <div class="col-4 topBtnContainer">
                <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configModulo.cancel();">
                    <i class="fas fa-times"></i> <span>Cancelar</span>
                </button>
                <button class="btn btn-sm btn-outline-success float-end" title="Guardar" type="submit" form="frmOID">
                    <i class="fas fa-save"></i> <span>Guardar</span>
                </button>
            </div>
        </div>
        <nav>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item">SIMAC</li>
                <li class="breadcrumb-item">Solicitudes</li>
                <li class="breadcrumb-item active">Configuración</li>
            </ol>
        </nav>
    </div>
    <hr class="mb-1 mt-1">
    <section class="section">
        <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
            <li class="nav-item">
                <button class="nav-link active" id="solicitante-tab" data-bs-toggle="tab" data-bs-target="#solicitante" role="tab" aria-controls="solicitante" aria-selected="true" onclick="generalShowHideButtonTab('tabPrimaryButtonContainer','btnsSolicitanteTab');">
                    Datos del solicitante
                </button>
            </li>
            <li class="nav-item">
                <button class="nav-link" id="credito-tab" data-bs-toggle="tab" data-bs-target="#credito" role="tab" aria-controls="credito" aria-selected="false" onclick="generalShowHideButtonTab('tabPrimaryButtonContainer','btnsCreditosTab');">
                    Datos del crédito
                </button>
            </li>
            <li class="nav-item">
                <button class="nav-link" id="garantias-tab" data-bs-toggle="tab" data-bs-target="#garantias" role="tab" aria-controls="garantias" aria-selected="false" onclick="generalShowHideButtonTab('tabPrimaryButtonContainer','btnsGarantiaTab');">
                    Garantías
                </button>
            </li>
            <li class="nav-item ms-auto" id="tabPrimaryButtonContainer">
                <div class="tabButtonContainer" id="btnsGarantiaTab" style="display: none;">
                    <button type="button" onclick="configGarantia.edit();" class="btn btn-outline-primary btn-sm">
                        <i class="fa fa-plus-circle"></i> Garantía
                    </button>
                </div>
            </li>
        </ul>
        <div class="tab-content" style="margin-top: 10px;">
            <div class="tab-pane fade show active" id="solicitante" role="tabpanel" aria-labelledby="solicitante-tab">
                <fieldset class="border p-1">
                    <legend class="w-auto float-none pb-0 mb-0">Datos personales</legend>
                    <form id="frmSolicitante" name="frmSolicitante" accept-charset="utf-8" method="POST" action="javascript:void(0)">
                        <div class="row">
                            <!-- código de cliente -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtCodigoCliente">Código de cliente</label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtCodigoCliente" name="txtCodigoCliente" placeholder="" title="" class="form-control" value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- tipo de documento -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboTipoDocumento'>Tipo de documento <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboTipoDocumento" name="cboTipoDocumento" class="selectpicker form-control cboTipoDocumento" required data-live-search="true" disabled onchange="generalMonitoreoTipoDocumento(this.value,'txtNumeroDocumento');">
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- número de documento -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtNumeroDocumento">Número de documento <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtNumeroDocumento" name="txtNumeroDocumento" placeholder="" title="" class="form-control" required value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- nit -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtNit">NIT <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtNit" name="txtNit" placeholder="" title="" class="form-control NIT" required value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- nombres solicitante -->
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label class="form-label" for="txtNombres">Nombres <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtNombres" name="txtNombres" placeholder="" title="" class="form-control" required value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- apellidos solicitante -->
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label class="form-label" for="txtApellidos">Apellidos <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtApellidos" name="txtApellidos" placeholder="" title="" class="form-control" required value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- conocido solicitante -->
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label class="form-label" for="txtConocido">Conocido por</label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtConocido" name="txtConocido" placeholder="" title="" class="form-control" value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- pais solicitante -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboPais'>País de residencia <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboPais" name="cboPais" class="selectpicker form-control cboPais" required data-live-search="true" disabled onchange="generalMonitoreoPais(this.value,'cboDepartamento');">
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- departamento solicitante -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboDepartamento'>Departamento de residencia <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboDepartamento" name="cboDepartamento" class="selectpicker form-control" required data-live-search="true" disabled onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');">
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- municipio solicitante -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboMunicipio'>Municipio de residencia <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboMunicipio" name="cboMunicipio" class="selectpicker form-control" required data-live-search="true" disabled>
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- dirección solicitante -->
                            <div class="col-lg-6 col-xl-6 mayusculas">
                                <label class="form-label" for="txtDireccion">Dirección <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtDireccion" name="txtDireccion" placeholder="" title="" class="form-control" required value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- profesión solicitante -->
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label class="form-label" for="txtProfesion">Profesión <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtProfesion" name="txtProfesion" placeholder="" title="" class="form-control" required value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- fecha de nacimiento solicitante -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtFechaNacimiento">Fecha de nacimiento <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtFechaNacimiento" name="txtFechaNacimiento" placeholder="" title="" class="form-control fechaMayorEdad campoFecha" required disabled value="" readonly onchange="simacCalcularEdad(this.value,'numEdad');">
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- edad solicitante -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="numEdad">Edad <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="numEdad" name="numEdad" placeholder="" title="" class="form-control" required value="" readonly>
                                </div>
                            </div>
                            <!-- ¿sabe firmar solicitante? -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboFirma'>¿Sabe firmar? <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboFirma" name="cboFirma" class="custom-select form-control" required data-live-search="true" disabled>
                                        <option selected value="0" disabled>seleccione</option>
                                        <option value="S">Si</option>
                                        <option value="S">No</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- teléfono móvil solicitante -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtMovil">Teléfono móvil <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtMovil" name="txtMovil" placeholder="" title="" class="form-control telefono" required value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- teléfono fijo solicitante -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtFijo">Teléfono fijo <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtFijo" name="txtFijo" placeholder="" title="" class="form-control telefono" value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- email solicitante -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtEmail">Correo electrónico <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="email" id="txtEmail" name="txtEmail" placeholder="" title="" class="form-control" value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- actividad económica -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboActividad'>Actividad económica <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboActividad" name="cboActividad" class="selectpicker form-control cboActividadEconomica" required data-live-search="true" disabled>
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </fieldset>
                <ul class="nav nav-tabs nav-tabs-bordered" id="tabsSolicitante" role="tablist" style="margin-top: 20px;">
                    <li class="nav-item">
                        <button class="nav-link active" id="documentosSolicitante-tab" data-bs-toggle="tab" data-bs-target="#documentosSolicitante" role="tab" aria-controls="documentosSolicitante" aria-selected="true" onclick="generalShowHideButtonTab('tabSecondaryButtonContainer','btnsDocumentosSolicitante');">
                            Documentos
                        </button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link" id="referenciasSolicitante-tab" data-bs-toggle="tab" data-bs-target="#referenciasSolicitante" role="tab" aria-controls="referenciasSolicitante" aria-selected="false" onclick="generalShowHideButtonTab('tabSecondaryButtonContainer','btnsReferenciasSolicitante');">
                            Referencias
                        </button>
                    </li>
                    <li class="nav-item ms-auto" id="tabSecondaryButtonContainer">
                        <div class="tabButtonContainer" id="btnsDocumentosSolicitante">
                            <button type="button" onclick="configDocumento.edit();" class="btn btn-outline-primary btn-sm">
                                <i class="fa fa-plus-circle"></i> Documento
                            </button>
                        </div>
                        <div class="tabButtonContainer" id="btnsReferenciasSolicitante" style="display: none;">
                            <button type="button" onclick="configDatosSolicitante.configReferencias.edit();" class="btn btn-outline-primary btn-sm">
                                <i class="fa fa-plus-circle"></i> Referencia
                            </button>
                        </div>
                    </li>
                </ul>
                <div class="tab-content" style="margin-top: 10px;">
                    <div class="tab-pane fade show active" id="documentosSolicitante" role="tabpanel" aria-labelledby="documentosSolicitante-tab">
                        <div class="row">
                            <div class="col-lg-12 col-xl-12">
                                <table id="tblDocumentosSolicitante" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                                    <thead>
                                        <th>N°</th>
                                        <th>Documento</th>
                                        <th>Comentarios</th>
                                        <th>Acciones</th>
                                    </thead>

                                    <body></body>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="referenciasSolicitante" role="tabpanel" aria-labelledby="referenciasSolicitante-tab">
                        <div class="row">
                            <div class="col-lg-12 col-xl-12">
                                <table id="tblReferenciasSolicitante" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                                    <thead>
                                        <th>N°</th>
                                        <th>Tipo</th>
                                        <th>Nombre</th>
                                        <th>Dirección</th>
                                        <th>Lugar de trabajo</th>
                                        <th>Teléfono</th>
                                        <th>Parentesco</th>
                                        <th>Comentarios</th>
                                        <th>Acciones</th>
                                    </thead>

                                    <body></body>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="credito" role="tabpanel" aria-labelledby="credito-tab">
                <fieldset class="border p-1" style="padding-top: 0;">
                    <legend class="w-auto float-none mb-0">Generales</legend>
                    <form id="frmCredito" name="frmCredito" accept-charset="utf-8" method="POST" action="javascript:void(0);">
                        <div class="row">
                            <!-- fecha de recepcion -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtFechaRecepcion">Fecha de recepción <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtFechaRecepcion" name="txtFechaRecepcion" placeholder="" title="" class="form-control" required value="" readonly>
                                </div>
                            </div>
                            <!-- monto solicitado -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="numMontoSolicitado">Monto solicitado <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="number" id="numMontoSolicitado" name="numMontoSolicitado" placeholder="" title="" class="form-control" required value="" readonly min="0" step="0.01" onkeypress="return generalSoloNumeros(event);">
                                </div>
                            </div>
                            <!-- monto sugerido -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="numMontoSugerido">Monto sugerido <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="number" id="numMontoSugerido" name="numMontoSugerido" placeholder="" title="" class="form-control" required value="" readonly min="0" step="0.01" onkeypress="return generalSoloNumeros(event);">
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--línea financiera-->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboLineaFinanciera'>Línea financiera <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboLineaFinanciera" name="cboLineaFinanciera" class="selectpicker form-control cboLineaFinanciera" required data-live-search="true" disabled>
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- destino -->
                            <div class="col-lg-6 col-xl-6 mayusculas">
                                <label class="form-label" for="txtDestino">Destino de crédito <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtDestino" name="txtDestino" placeholder="" title="" class="form-control" required value="" readonly>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- tasa de interés -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="numTasaInteres">Tasa de interés <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="number" id="numTasaInteres" name="numTasaInteres" placeholder="" title="" class="form-control" required value="" readonly min="0" step="0.01" onkeypress="return generalSoloNumeros(event);">
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- ¿crédito al vencimiento? -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboVencimiento'>¿Crédito al vencimiento? <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboVencimiento" name="cboVencimiento" class="custom-select form-control" required data-live-search="true" disabled>
                                        <option selected value="0" disabled>seleccione</option>
                                        <option value="N">No</option>
                                        <option value="S">Si</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- plazo de crédito -->
                            <div class="col-lg-3 col-xl-3" style="border:1px solid rgba(206, 206, 206, 0.96); border-radius:.3em;">
                                <label class="form-label" class="form-label">Plazo <span class="requerido">*</span></label>
                                <div class="row">
                                    <div class="col-sm-6" style="padding-right:.26rem;">
                                        <div class="input-group form-group">
                                            <input type="number" id="numPlazo" name="numPlazo" title="plazo" data-bs-toggle="tooltip" min="0" step="1" onkeypress="return generalSoloNumeros(event);" class="form-control" value="" required readonly>
                                            <div class="input-group-append">
                                                <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="padding-left: 0.26rem;">
                                        <div class="input-group form-group">
                                            <select id="cboPlazo" name="cboPlazo" class="custom-select form-control cboTipoPlazo" required data-live-search="true" disabled>
                                                <option selected value="0" disabled>seleccione</option>
                                            </select>
                                            <div class="input-group-append">
                                                <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- periodicidad de pago -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboPeriodicidad'>Periodicidad de pago <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboPeriodicidad" name="cboPeriodicidad" class="selectpicker form-control cboPeriodicidad" required data-live-search="true" disabled>
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- fuente de fondos -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboFuente'>Fuente de fondos <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboFuente" name="cboFuente" class="selectpicker form-control cboFuenteFondos" required data-live-search="true" disabled>
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="input-group-text btn btn-outline-secondary" type="button" onclick="generalHabilitarDeshabilitarEscritura(this);">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- nombre agencia -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtAgencia">Agencia <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtAgencia" name="txtAgencia" placeholder="" title="" class="form-control" required value="" readonly>
                                </div>
                            </div>
                        </div>
                    </form>
                </fieldset>
                <div class="row mt-3">
                    <div class="col-lg-12 col-xl-12">
                        <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                            <li class="nav-item">
                                <button class="nav-link active " id="construccionCuota-tab" data-bs-toggle="tab" data-bs-target="#construccionCuota" role="tab" aria-controls="construccionCuota" aria-selected="true">
                                    Construcción de cuota 
                                </button>
                            </li>
                            <li class="nav-item ms-auto">
                                <div class="tabButtonContainerThird" id="btnsCuota">
                                    <button type="button" onclick="configDatosCredito.configCuota.configAditivos.edit();" class="btn btn-outline-primary btn-sm">
                                        <i class="fa fa-plus-circle"></i> Aditivo
                                    </button>
                                </div>
                            </li>
                        </ul>
                        <div class="tab-content" style="margin-top: 20px;">
                            <div class="tab-pane fade show active" id="construccionCuota" role="tabpanel" aria-labelledby="construccionCuota-tab">
                                <div class="row">
                                    <div class="col-lg-12 col-xl-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-xl-6">
                                                <div class="input-group input-group-sm mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Monto capital e intereses: $</span>
                                                    </div>
                                                    <input type="number" id="numCapitalInteres" value="0.00" name="numCapitalInteres" class="form-control" min="0" step="0.01" onkeypress="return generalSoloNumeros(event);" onkeyup="configDatosCredito.configCuota.calcularCuota();">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-xl-6">
                                                <div class="input-group input-group-sm mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Monto total de cuota: $</span>
                                                    </div>
                                                    <input type="number" disabled id="numMontoTotal" value="0.00" name="numMontoTotal" class="form-control" min="0" step="0.01">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xl-12">
                                        <table id="tblAditivos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                            <thead>
                                                <th>N°</th>
                                                <th>Aditivo</th>
                                                <th>Monto</th>
                                                <th>Opciones</th>
                                            </thead>

                                            <body>
                                            </body>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="garantias" role="tabpanel" aria-labelledby="garantias-tab">
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <table id="tblGarantias" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                            <thead>
                                <th>N°</th>
                                <th>Tipo de garantía</th>
                                <th>Fecha de registro</th>
                                <th>¿Configurada?</th>
                                <th>Opciones</th>
                            </thead>

                            <body>
                            </body>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="mdlDocumento" data-bs-keyboard="false" data-bs-backdrop="static">
        <div class="modal-dialog modal-lg">
            <form id="frmDocumento" name="frmDocumento" class="needs-validation" novalidate accept-charset="utf-8" method="POST" action="javascript:configDocumentos.save();">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Agregar documento</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Documento -->
                            <div class="col-lg-12 col-xl-12">
                                <label class="form-label" for='cboDocumento'>Documento <span class="requerido">*</span></label>
                                <div class="input-group form-group">
                                    <select id="cboDocumento" name="cboDocumento" class="selectpicker form-control documentoSolicitud" required data-live-search="true">
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Comentarios -->
                            <div class="col-lg-12 col-xl-12">
                                <label class="form-label" for="txtComentariosDocumento">Comentarios</label>
                                <div class="input-group form-group">
                                    <input type="text" id="txtComentariosDocumento" name="txtComentariosDocumento" placeholder="" title="" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-primary">
                            <i class="fas fa-save"></i> Guardar
                        </button>
                        <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                            <i class="fas fa-times-circle"></i> Cancelar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="mdlReferencia" data-bs-keyboard="false" data-bs-backdrop="static">
        <div class="modal-dialog modal-lg">
            <form id="frmReferencia" name="frmReferencia" accept-charset="utf-8" method="POST" action="javascript:configDatosSolicitante.configReferencias.save();" class="needs-validation" novalidate>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Referencia</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Documento -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for='cboTipoReferencia'>Tipo de referencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboTipoReferencia" title="" name="cboTipoReferencia" class="selectpicker form-control cboTipoReferencia" required data-live-search="true">
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Nombre referencia -->
                            <div class="col-lg-6 col-xl-6 mayusculas">
                                <label class="form-label" for="txtNombreReferencia">Nombre <span class="requerido">*</span></label>
                                <input type="text" id="txtNombreReferencia" name="txtNombreReferencia" placeholder="" title="" class="form-control" required value="">
                                <div class="invalid-feedback">
                                    Ingrese el nombre
                                </div>
                            </div>
                            <!-- Lugar de trabajo referencia -->
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label class="form-label" for="txtParentescoReferencia">Parentesco <span class="requerido">*</span></label>
                                <input type="text" id="txtParentescoReferencia" name="txtParentescoReferencia" placeholder="" title="" class="form-control" required value="">
                                <div class="invalid-feedback">
                                    Ingrese el parentesco
                                </div>
                            </div>
                            <!-- direccion referencia -->
                            <div class="col-lg-6 col-xl-6 mayusculas">
                                <label class="form-label" for="txtDireccionReferencia">Dirección <span class="requerido">*</span></label>
                                <input type="text" id="txtDireccionReferencia" name="txtDireccionReferencia" placeholder="" title="" class="form-control" required value="">
                                <div class="invalid-feedback">
                                    Ingrese la dirección
                                </div>
                            </div>
                            <!-- Lugar de trabajo referencia -->
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label class="form-label" for="txtTrabajoReferencia">Lugar de trabajo <span class="requerido">*</span></label>
                                <input type="text" id="txtTrabajoReferencia" name="txtTrabajoReferencia" placeholder="" title="" class="form-control" required value="">
                                <div class="invalid-feedback">
                                    Ingrese el lugar de trabajo
                                </div>
                            </div>
                            <!-- teléfono referencia -->
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtTelefonoReferencia">Teléfono <span class="requerido">*</span></label>
                                <input type="text" id="txtTelefonoReferencia" name="txtTelefonoReferencia" placeholder="" title="" class="form-control telefono" required value="">
                                <div class="invalid-feedback">
                                    Ingrese el teléfono
                                </div>
                            </div>
                            <!-- Comentarios -->
                            <div class="col-lg-12 col-xl-12">
                                <label class="form-label" for="txtComentariosReferencia">Comentarios <span class="requerido">*</span></label>
                                <input type="text" id="txtComentariosReferencia" name="txtComentariosReferencia" placeholder="" title="" class="form-control " value="" required>
                                <div class="invalid-feedback">
                                    Ingrese los comentarios
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-primary">
                            Guardar
                        </button>
                        <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="mdlAditivo" data-bs-keyboard="false" data-bs-backdrop="static">
        <div class="modal-dialog">
            <form id="frmAditivo" name="frmAditivo" accept-charset="utf-8" method="POST" class="needs-validation" novalidate action="javascript:configDatosCredito.configCuota.configAditivos.save();">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Aditivo</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Aditivo -->
                            <div class="col-lg-12 col-xl-12">
                                <label class="form-label" for='cboAditivo'>Aditivo <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboAditivo" name="cboAditivo" class="selectpicker form-control cboAditivo" required data-live-search="true">
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Monto de aditivo -->
                            <div class="col-lg-12 col-xl-12 mayusculas">
                                <label class="form-label" for="numMontoAditivo">Monto <span class="requerido">*</span></label>
                                <input type="number" id="numMontoAditivo" name="numMontoAditivo" placeholder="" title="" class="form-control" min="0" step="0.01" required value="">
                                <div class="invalid-feedback">
                                    Ingrese el monto
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-primary">
                            Guardar
                        </button>
                        <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="mdlGarantia" data-bs-keyboard="false" data-bs-backdrop="static">
        <div class="modal-dialog">
            <form id="frmGarantia" name="frmGarantia" class="needs-validation" novalidate accept-charset="utf-8" method="POST" action="javascript:configGarantia.save();">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tipo de garantía</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- Tipo de garantía -->
                            <div class="col-lg-12 col-xl-12">
                                <label class="form-label" for='cboTipoGarantia'>Tipo de garantía <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboTipoGarantia" name="cboTipoGarantia" class="selectpicker form-control cboTipoGarantia" required data-live-search="true">
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-primary">
                            Guardar
                        </button>
                        <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                            Cancelar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php
    $_GET['js'] = ['simacSolicitud'];
} else {
?>
    <center>
        <h2>Sin solicitud</h2>
    </center>
<?php
}
?>