<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->sesionUnica = 'S')) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";

        $solicitud = explode("@@@", $_GET["q"]);

        if (in_array('tiposDocumento', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../code/Models/tipoDocumento.php';
            $tipoDocumento = new tipoDocumento();
            $tiposDocumento = $tipoDocumento->obtenerTiposDocumento('Adulto');
            $respuesta->{"tiposDocumento"} = $tiposDocumento;
        }

        if (in_array('tiposCredito', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/tipoCredito.php';
            $tCredito = new tipoCredito();
            $tiposCredito = $tCredito->getTiposCredito();
            $respuesta->{"tiposCredito"} = $tiposCredito;
        }

        if (in_array('tiposGarantia', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/tipoGarantia.php';
            $tGarantia = new tipoGarantia();
            $tiposGarantia = $tGarantia->getTiposGarantia();
            $respuesta->{"tiposGarantia"} = $tiposGarantia;
        }

        if (in_array('agenciasCbo', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../code/Models/agencia.php';
            $agencia = new agencia();
            $agencias = $agencia->getAgenciasCbo($_SESSION["index"]->agencias);

            $respuesta->{"agencias"} = $agencias;
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
