<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/OID.php';
        // echo json_encode($input);die;
        $OID = new OID();
        $idOID = isset($input["id"]) ? $input["id"] : 0;
        $idUsuario = $_SESSION["index"]->id;
        $tipoDocumento = $input["cboTipoDocumento"]["value"];
        $labelTipoDocumento = $input["cboTipoDocumento"]["labelOption"];
        $numeroDocumento = $input["txtDocumento"]["value"];
        $nombres = $input["txtNombres"]["value"];
        $apellidos = $input["txtApellidos"]["value"];
        $conocido = $input["txtConocido"]["value"];
        $salario = $input["numSalario"]["value"];
        $policia = $input["cboPolicia"]["value"];
        $ONI = $input["txtONI"]["value"];
        $agencia = $input["cboAgencia"]["value"];
        $ubicacionAgencia = $agencia == "709" ? "Apopa" : ($agencia == "710" ? "San Salvador" : $input["cboAgencia"]["labelOption"]);
        $nombreAgencia = $input["cboAgencia"]["labelOption"];
        $referenciaCredito = $input["txtNumeroReferencia"]["value"];
        $nombreEmpresa = $input["txtEmpresa"]["value"];
        $montoCredito = $input["numMontoCredito"]["value"];
        $valorCuota = $input["numCuota"]["value"];
        $tipoCredito = $input["cboTipoCredito"]["value"];
        $tipoGarantia = $input["cboTipoGarantia"]["value"];
        $labelTipoCredito = $input["cboTipoCredito"]["labelOption"];
        $labelTipoGarantia = $input["cboTipoGarantia"]["labelOption"];
        $cantidadCuotas = $input["numCantidadCuotas"]["value"];
        $inicioCobro = $input["txtInicioCobro"]["value"];
        $finCobro = $input["txtFinCobro"]["value"];
        $fechaGeneracion = $input["txtFechaGeneracion"]["value"];
        $montoCancelacion = $input["numMontoCancelaciones"]["value"];
        $observaciones = $input["txtObservaciones"]["value"];
        $cancelaciones = $input["cancelaciones"];

        $OID->id = $idOID;
        $OID->tipoDocumento = $tipoDocumento;
        $OID->numeroDocumento = $numeroDocumento;
        $OID->nombres = $nombres;
        $OID->apellidos = $apellidos;
        $OID->conocido = $conocido;
        $OID->salario = $salario;
        $OID->policia = $policia;
        $OID->ONI = $ONI;
        $OID->agencia = $agencia;
        $OID->numeroReferencia = $referenciaCredito;
        $OID->empresa = $nombreEmpresa;
        $OID->montoCredito = $montoCredito;
        $OID->montoCuota = $valorCuota;
        $OID->tipoCredito = $tipoCredito;
        $OID->tipoGarantia = $tipoGarantia;
        $OID->cantidadCuotas = $cantidadCuotas;
        $OID->inicioCobro = "01-" . $inicioCobro;
        $OID->finCobro = "01-" . $finCobro;
        $OID->observaciones = $observaciones;
        $OID->usuario = $idUsuario;
        $OID->cancelaciones = $cancelaciones;
        $tipoOID = $input["tipoOID"];

        $tipoApartado = $input["tipoApartado"];
        $idApartado = base64_decode(urldecode($input["idApartado"]));
        $response = $OID->guardarOID($tipoApartado, $idApartado, $ipActual, $tipoOID);
        $nombreArchivo = 'test.pdf';

        if (!empty($response) && $response["respuesta"] == "EXITO") {
            $referenciaDB = $response["referencia"];
            $nombreArchivo = "O" . $response["referencia"] . ".pdf";
            $jefeAgencia = $response["nombreJefe"];
            $emailAgencia = $response["emailAgencia"];
            include "fordesConstruccionOID.php";
            $respuesta->{"nombreArchivo"} = $nombreArchivo;
        }

        $respuesta->{"respuesta"} = $response['respuesta'];

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
