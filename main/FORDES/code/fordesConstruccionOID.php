<?php
require_once 'C:\xampp\htdocs\vendor\autoload.php';

if (!isset($nombreArchivo)) {
    $nombreArchivo = "prueba.pdf";
}

$fechaActual = date('Y-m-d H:i:s');

$arrMeses = array('', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');

$montoCreditoImp = "$" . number_format($montoCredito, 2, '.', ',');
$referenciaCreditoImp = $policia == "N" ? $referenciaCredito : $referenciaDB;
$partesFechaGeneracion = explode("-", $fechaGeneracion);
$lugarEmision = mb_strtoupper($ubicacionAgencia, "UTF-8") . ", " . $partesFechaGeneracion[0] . " DE " . mb_strtoupper($arrMeses[intval($partesFechaGeneracion[1])], "UTF-8") . " DE " . $partesFechaGeneracion[2];
$montoCuotaImp = "$" . number_format($valorCuota, 2, '.', ',');
$salarioImp = "$" . number_format($salario, 2, '.', ',');
$valoresInicioCobro = explode("-", $inicioCobro);
$valoresFinCobro = explode("-", $finCobro);
$inicioCobroImp = mb_strtoupper($arrMeses[intval($valoresInicioCobro[0])], "UTF-8") . " DE " . $valoresInicioCobro[1];
$finCobroImp = mb_strtoupper($arrMeses[intval($valoresFinCobro[0])], "UTF-8") . " DE " . $valoresFinCobro[1];
$tblONI = !empty($ONI) ? '<tr><td style="width:410px;"></td><td style="font-weight:bold;">ONI: ' . $ONI . '</td></tr>' : '';

$nombreCompletoDeudor = $nombres . " " . $apellidos;
$nombreCompletoDeudor = !empty($conocido) ? $nombreCompletoDeudor . " CONOCIDO POR " . $conocido : $nombreCompletoDeudor;

if (!isset($jefeAgencia)) {
    $jefeAgencia = "NO DEFINIDO";
    $emailAgencia = "sin correo";
}

$documentoDeudor = $labelTipoDocumento . ": " . $numeroDocumento;

$cancelacionesImp = [];

foreach ($cancelaciones as $cancelacion) {
    $parrafoCancelacion = 'SE CANCELARÁ CRÉDITO ' . mb_strtoupper($cancelacion["labelTipoCredito"], "UTF-8") . ' CON <strong>' . $cancelacion["nombreEmpresa"] . '</strong>, POR UN MONTO TOTAL DE <strong>$' . number_format($cancelacion["monto"], 2, '.', ',') . '</strong> CON NÚMERO DE REFERENCIA <strong>' . htmlspecialchars($cancelacion["numeroReferencia"]) . '</strong>';
    array_push($cancelacionesImp, $parrafoCancelacion);
}

class MYPDF extends TCPDF
{

    //Page header
    public function Header()
    {
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $img_file = '../../img/membrete.png';
        $this->Image('@' . file_get_contents($img_file), 0, 0, 220, 275, '', '', '', false, 300);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

    // Page footer
    public function Footer()
    {
        global $emailAgencia;
        // Position at 15 mm from bottom
        $this->SetY(-40);
        // Set font
        $this->SetFont('helvetica', 'B', 8);
        // Page number
        // $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Write(0, 'Favor: Al hacer el descuento depositar valor a nuestra cuenta # 009-401-00-009411-3 en Banco Cuscatlán ó en nuestra cuenta de ahorro Banco Agrícola # 300-089-247-3 y enviar comprobante al correo ' . $emailAgencia . '.', '', 0, 'C', true, 0, false, false, 0);
    }
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Departamento IT');
$pdf->SetTitle('OID');
$pdf->SetSubject('ACACYPAC');
$pdf->SetKeywords('TCPDF, PDF, FORDES');

// // Fuente de cabecera y pie de página
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// // Espacios por defecto
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// //Margenes
$pdf->SetMargins(16, 15, 13);
$pdf->SetHeaderMargin(7);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// // Configurar Auto salto de página
$pdf->SetAutoPageBreak(TRUE, 40);

// // Factor de escalado de imagen
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// // Configurando lenguaje
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// // Establecer fuente
$pdf->SetFont('dejavusans', '', 10);
$pdf->AddPage();

$tablaPresentacion = '<style>table{font-size:10px; line-height:15px;}</style><table cellspacing="0" cellpadding="0" border="0">
    <tr>
    <td rowspan="5" style="width:410px;"></td>
    <td>ORDEN IRREVOCABLE DE DESCUENTO</td>
    </tr>
    <tr>
    <td style="font-weight:bold; height:30px;">NIT: 0416-250471-001-8</td>
    </tr>
    <tr>
    <td>' . $lugarEmision . '</td>
    </tr>
    <tr>
    <td style="font-weight:bold;">Monto: ' . $montoCreditoImp . '</td>
    </tr>
    <tr>
    <td style="font-weight:bold;">REFERENCIA: ' . $referenciaCreditoImp . '</td>
    </tr>' . $tblONI . '
    </table>';
$pdf->writeHTML($tablaPresentacion, true, false, true, false, '');

$parrafoDescripcion = 'Señores (as):<br/>' . $nombreEmpresa . '<br/><br/>PRESENTE<br/><br/>Por medio de la presente  y de conformidad al artículo 136 del Código de Trabajo, publicado en el Diario Oficial del 31 de julio de 1972, y el artículo 65 de la Ley General de Asociaciones Cooperativas de acuerdo al decreto Legislativo No. 339, publicado en el Diario Oficial del 14 de mayo de 1986, Autorizo al Pagador o Tesorero a descontar de mi sueldo mensual de <span style="font-weight:bold;">' . $salarioImp . '</span> que devengo en la Institución como empleado (a) de la misma; un total de  <span style="font-weight:bold;">' . $cantidadCuotas . ' Cuotas Mensuales</span> de <span style="font-weight:bold;">' . $montoCuotaImp . '</span> cada una en concepto de pago de <span style="font-weight:bold;">Crédito ' . $labelTipoCredito . '</span> garantía <span style="font-weight:bold;">' . $labelTipoGarantia . '</span>, las que deberá pagar por mi cuenta a La Cooperativa Financiera "Progreso" ACACYPAC N.C. de R.L, Sucursal <span style="font-weight:bold;">' . $nombreAgencia . '</span>, a partir del mes de <span style="font-weight:bold;">' . $inicioCobroImp . ' A ' . $finCobroImp . '</span>, como abono a deuda contraída por mi persona con dicha institución, rogando dar su conformidad a La Cooperativa, de que procederá a efectuar lo que por medio de esta carta autorizo; también podrá ser efectiva en otro empleo que yo pueda tener en el futuro con cualquier empresa privada o estatal.';
$pdf->writeHTML('<style>p{line-height:16px; font-size: 11.3px; text-align:justify;}</style><p>' . $parrafoDescripcion . '</p>', true, false, true, false, '');

$tablaFirmas = '<br/><br/><br/><style>table{font-size:11px;}</style><table cellspacing="0" cellpadding="0" border="0">
    <tr>
    <td style="width:300px; text-align:center;">F___________________________________________</td>
    <td style="width:40px;"></td>
    <td style="width:300px; text-align:center;">F___________________________________________</td>
    </tr>
    <tr>
    <td style="text-align:center; font-weight:bold;">' . $nombreCompletoDeudor . '</td>
    <td></td>
    <td style="text-align:center; font-weight:bold;">' . $jefeAgencia . '</td>
    </tr>
    <tr>
    <td style="text-align:center; font-weight:bold;">' . $documentoDeudor . '</td>
    <td></td>
    <td style="text-align:center; font-weight:bold;">JEFE DE AGENCIA</td>
    </tr>
    </table>';
$pdf->writeHTML('<br><br>', true, false, true, false, '');
$pdf->writeHTML($tablaFirmas, true, false, true, false, '');

if (count($cancelacionesImp) > 0 || !empty($observaciones)) {
    $parrafoNota = '<span style="font-weight:bold;">NOTA: </span>';
    if (count($cancelacionesImp)) {
        $parrafoNota .= implode(". ", $cancelacionesImp) . ". ";
    }
    if (!empty($observaciones)) {
        $parrafoNota .= $observaciones;
    }

    $pdf->writeHTML('<style>p{line-height:14px; font-size: 10.2px; text-align:justify;}</style><p>' . $parrafoNota . '<br/></p>', true, false, true, false, '');
}

$parrafoEmpresa = 'SEÑORES: COOPERATIVA FINANCIERA PROGRESO, ACACYPAC N.C. DE R.L.<br>Presente<br><br>Estimados señores: Al tomar debida nota de la carta anterior, me comprometo a descontar del sueldo mensual del Sr (a) <span style="font-weight:bold;">' . $nombreCompletoDeudor . '</span> las cuotas que se refiere la autorización anterior y remitirlas a ACACYPAC N.C. DE R.L.';
$pdf->writeHTML('<style>p{line-height:14px; font-size: 10.2px; text-align:justify;}</style><p>' . $parrafoEmpresa . '</p>', true, false, true, false, '');

$tablaFirmaPagador = '<br/><br/><br/><br/><table cellspacing="0" cellpaddin="0" border="0"><tr><td style="width:450px;"></td><td>____________________________</td></tr><tr><td></td><td style="text-align:center; width:150px;">           Firma y Sello Pagador</td></tr></table>';
$pdf->writeHTML($tablaFirmaPagador, true, false, true, false, '');

$mypdf = __DIR__ . '\..\docs\/' . $nombreArchivo . '';
$pdf->Output($mypdf, 'F');
    // header('Content-type: application/pdf');
    // header('Content-Disposition: inline;filename=O' . $nombreArchivo);
    // readfile($mypdf);
