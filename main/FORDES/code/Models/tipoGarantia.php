<?php
class tipoGarantia
{
    public $id;
    public $tipoGarantia;
    public $fechaRegistro;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        if (!empty($this->fechaRegistro)) {
            $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        }
    }

    public function getTiposGarantia()
    {
        $query = "SELECT * FROM fordes_viewTiposGarantia;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'tipoGarantia');
        }

        $this->conexion = null;
        return false;
    }
}
