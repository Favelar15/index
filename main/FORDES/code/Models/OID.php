<?php
class OID
{
    public $id;
    public $tipoDocumento;
    public $numeroDocumento;
    public $nombres;
    public $apellidos;
    public $conocido;
    public $salario;
    public $policia;
    public $ONI;
    public $agencia;
    public $numeroReferencia;
    public $empresa;
    public $montoCredito;
    public $montoCuota;
    public $tipoCredito;
    public $tipoGarantia;
    public $cantidadCuotas;
    public $inicioCobro;
    public $finCobro;
    public $observaciones;
    public $usuario;
    public $cancelaciones;
    private $conexion;
    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    function guardarOID($tipoApartado, $idApartado, $ipActual, $tipoOID = 'Normal')
    {
        $response = null;
        $tmpId = null;
        $referenciaDb = null;
        $nombreJefe = null;
        $emailAgencia = null;
        $arrCancelaciones = [];

        foreach ($this->cancelaciones as $cancelacion) {
            array_push($arrCancelaciones, $cancelacion["nombreEmpresa"] . "%%%" . $cancelacion["numeroReferencia"] . "%%%" . $cancelacion["tipoCredito"] . "%%%" . $cancelacion["monto"]);
        }

        $tmpCancelaciones = count($arrCancelaciones) > 0 ? implode("@@@", $arrCancelaciones) : '';
        $query = "EXEC fordes_spGuardarOID :id,:tipoDocumento,:numeroDocumento,:nombres,:apellidos,:conocido,:salario,:policia,:oni,:agencia,:tipoReferencia,:numeroReferencia,:empresa,:montoCredito,:montoCuota,:tipoCredito,:tipoGarantia,:cantidadCuotas,:inicioCobro,:finCobro,:observaciones,:cancelaciones,:usuario,:ip,:tipoApartado,:idApartado,:response,:idSalida,:referencia,:nombreJefe,:emailAgencia;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':tipoDocumento', $this->tipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumento', $this->numeroDocumento, PDO::PARAM_STR);
        $result->bindParam(':nombres', $this->nombres, PDO::PARAM_STR);
        $result->bindParam(':apellidos', $this->apellidos, PDO::PARAM_STR);
        $result->bindParam(':conocido', $this->conocido, PDO::PARAM_STR);
        $result->bindParam(':salario', $this->salario, PDO::PARAM_STR);
        $result->bindParam(':policia', $this->policia, PDO::PARAM_STR);
        $result->bindParam(':oni', $this->ONI, PDO::PARAM_STR);
        $result->bindParam(':agencia', $this->agencia, PDO::PARAM_INT);
        $result->bindParam(':tipoReferencia', $tipoOID, PDO::PARAM_STR);
        $result->bindParam(':numeroReferencia', $this->numeroReferencia, PDO::PARAM_STR);
        $result->bindParam(':empresa', $this->empresa, PDO::PARAM_STR);
        $result->bindParam(':montoCredito', $this->montoCredito, PDO::PARAM_STR);
        $result->bindParam(':montoCuota', $this->montoCuota, PDO::PARAM_STR);
        $result->bindParam(':tipoCredito', $this->tipoCredito, PDO::PARAM_INT);
        $result->bindParam(':tipoGarantia', $this->tipoGarantia, PDO::PARAM_INT);
        $result->bindParam(':cantidadCuotas', $this->cantidadCuotas, PDO::PARAM_STR);
        $result->bindParam(':inicioCobro', $this->inicioCobro, PDO::PARAM_STR);
        $result->bindParam(':finCobro', $this->finCobro, PDO::PARAM_STR);
        $result->bindParam(':observaciones', $this->observaciones, PDO::PARAM_STR);
        $result->bindParam(':cancelaciones', $tmpCancelaciones, PDO::PARAM_STR);
        $result->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idSalida', $tmpId, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':referencia', $referenciaDb, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':nombreJefe', $nombreJefe, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':emailAgencia', $emailAgencia, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            if ($response) {
                return ["respuesta" => $response, "id" => $tmpId, "referencia" => $referenciaDb, "nombreJefe" => $nombreJefe, "emailAgencia" => $emailAgencia];
            } else {
                return ["respuesta" => "Error en fordes_spGuardarOID"];
            }
        }

        $this->conexion = null;
        return false;
    }
}
