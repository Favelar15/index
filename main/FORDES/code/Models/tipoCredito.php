<?php
class tipoCredito
{
    public $id;
    public $tipoCredito;
    public $fechaRegistro;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        if (!empty($this->fechaRegistro)) {
            $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        }
    }

    public function getTiposCredito()
    {
        $query = "SELECT * FROM fordes_viewTiposCredito;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'tipoCredito');
        }

        $this->conexion = null;
        return false;
    }
}
