<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Generación de orden de descuento</h1>
        </div>
        <div class="col-4 topBtnContainer">
            <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configOID.cancel();">
                <i class="fas fa-times"></i> <span>Cancelar</span>
            </button>
            <button class="btn btn-sm btn-outline-success float-end" title="Guardar" type="submit" form="frmOID">
                <i class="fas fa-save"></i> <span>Generar documentos</span>
            </button>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">FORDES</li>
            <li class="breadcrumb-item active">Generar OID</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <form id="frmOID" name="frmOID" accept-charset="utf-8" method="POST" class="needs-validation" novalidate action="javascript:configOID.save();">
        <!-- Datos personales -->
        <fieldset class="border p-1" style="padding-top: 0;">
            <legend class="w-auto float-none mb-0 pb-0">Datos personales</legend>
            <div class="row">
                <div class='col-lg-3 col-xl-3'>
                    <label for='cboTipoDocumento' class="form-label">Tipo de documento <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select id="cboTipoDocumento" name="cboTipoDocumento" title="Tipo de documento" class="form-select cboTipoDocumento tipoPrimario" data-live-search="true" onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumento');" required>
                            <option selected value="" disabled>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el tipo de documento
                        </div>
                    </div>
                </div>
                <div class='col-lg-3 col-xl-3'>
                    <label for='txtDocumento' class="form-label">Número de documento <span class="requerido">*</span></label>
                    <input type='text' class='form-control numeroDocumentoPrimario' id='txtDocumento' name='txtDocumento' readonly required value="">
                    <div class="invalid-feedback">
                        Ingrese el número de documento
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtNombres" class="form-label">Nombres de deudor/a <span class="requerido">*</span> </label>
                    <input type="text" id="txtNombres" name="txtNombres" placeholder="" title="nombres" class="form-control" value="" required>
                    <div class="invalid-feedback">
                        Ingrese los nombres
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtApellidos" class="form-label">Apellidos de deudor/a <span class="requerido">*</span> </label>
                    <input type="text" id="txtApellidos" name="txtApellidos" placeholder="" title="apellidos" class="form-control" value="" required>
                    <div class="invalid-feedback">
                        Ingrese los apellidos
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtConocido" class="form-label">Conocido por </label>
                    <input type="text" id="txtConocido" name="txtConocido" placeholder="" title="conocido por" class="form-control" value="">
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="numSalario" class="form-label">Salario <span class="requerido">*</span></label>
                    <input type="number" id="numSalario" name="numSalario" class="form-control" placeholder="" title="salario" min="0" step="0.01" onkeypress="return generalSoloNumeros(event);" value="" required>
                    <div class="invalid-feedback">
                        Ingrese el salario
                    </div>
                </div>
                <div class='col-lg-3 col-xl-3'>
                    <label for='cboPolicia' class="form-label">¿Policia? <span class="requerido">*</span></label>
                    <select id="cboPolicia" name="cboPolicia" class="form-select" data-live-search="true" required onchange="configOID.evalPolicia(this.value);" title="¿es policia?">
                        <option selected value="" disabled>seleccione</option>
                        <option value="N">NO</option>
                        <option value="S">SI</option>
                    </select>
                    <div class="invalid-feedback">
                        Seleccione una opción
                    </div>
                </div>
                <div class='col-lg-3 col-xl-3'>
                    <label for='txtONI' class="form-label">ONI</label>
                    <input type='text' class='form-control' id='txtONI' name='txtONI' value="" readonly>
                    <div class="invalid-feedback">
                        Ingrese el número ONI
                    </div>
                </div>
            </div>
        </fieldset>
        <!-- Datos personales -->
        <fieldset class="border p-1 mt-1" style="padding-top: 0;">
            <legend class="w-auto float-none mb-0 pb-0">Datos del crédito</legend>
            <div class="row">
                <div class="col-lg-3 col-xl-3">
                    <label for='cboAgencia' class="form-label">Agencia <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select id="cboAgencia" name="cboAgencia" class="selectpicker form-control cboAgencia" required data-live-search="true">
                            <option selected value="" disabled>seleccione</option>
                        </select>
                    </div>
                </div>
                <div class='col-lg-3 col-xl-3'>
                    <label for='txtNumeroReferencia' class="form-label">Número de referencia <span class="requerido">*</span></label>
                    <input type='text' class='form-control' required id='txtNumeroReferencia' name='txtNumeroReferencia' value="">
                    <div class="invalid-feedback">
                        Ingrese el número de referencia
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtEmpresa" class="form-label">Empresa <span class="requerido">*</span></label>
                    <input type="text" id="txtEmpresa" name="txtEmpresa" required class="form-control" placeholder="" title="empresa" value="">
                    <div class="invalid-feedback">
                        Ingrese el nombre de la empresa
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="numMontoCredito" class="form-label">Monto <span class="requerido">*</span></label>
                    <input type="number" id="numMontoCredito" name="numMontoCredito" class="form-control" placeholder="" title="monto" min="0" step="0.01" onkeypress="return generalSoloNumeros(event);" onkeyup="configCancelacion.evalMonto();" required>
                    <div class="invalid-feedback">
                        Ingrese el monto del crédito
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="numCuota" class="form-label">Valor de cuota <span class="requerido">*</span></label>
                    <input type="number" id="numCuota" name="numCuota" class="form-control" required placeholder="" title="cuota" min="0" step="0.01" onkeypress="return generalSoloNumeros(event);" value="">
                    <div class="invalid-feedback">
                        Ingrese el valor de la cuota
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for='cboTipoCredito' class="form-label">Tipo de crédito <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select id="cboTipoCredito" name="cboTipoCredito" class="selectpicker form-control cboTipoCredito" required data-live-search="true">
                            <option selected value="" disabled>seleccione</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for='cboTipoGarantia' class="form-label">Tipo de garantía <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select id="cboTipoGarantia" name="cboTipoGarantia" class="form-control selectpicker cboTipoGarantia" data-live-search="true" required>
                            <option selected value="" disabled>seleccione</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="numCantidadCuotas" class="form-label">Número de cuotas <span class="requerido">*</span></label>
                    <input type="number" id="numCantidadCuotas" name="numCantidadCuotas" class="form-control" placeholder="" title="número de cuotas" min="0" step="1" onkeypress="return generalSoloNumeros(event);" onkeyup="configOID.calcFinCobro();" value="" required>
                    <div class="invalid-feedback">
                        Ingrese el número de cuotas
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="txtInicioCobro" class="form-label">Inicio de cobro <span class="requerido">*</span></label>
                    <input type="text" id="txtInicioCobro" name="txtInicioCobro" class="form-control fechaMesInicioFinLimitado" placeholder="MM-YYYY" title="inicio de cobro" value="" readonly required onchange="configOID.calcFinCobro(this.value);">
                    <div class="invalid-feedback">
                        Selecciona el mes de inicio de cobro
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="txtFinCobro" class="form-label">Fin de cobro </label>
                    <input type="text" id="txtFinCobro" name="txtFinCobro" class="form-control" placeholder="MM-YYYY" title="fin de cobro" value="" readonly required>
                    <div class="invalid-feedback">
                        No seleccionó el mes de inicio de cobro
                    </div>
                </div>
                <div class='col-lg-3 col-xl-3'>
                    <label for='txtFechaGeneracion' class="form-label">Fecha de generación <span class="requerido">*</span></label>
                    <input type='text' class='form-control fechaInicioLimitado' id='txtFechaGeneracion' name='txtFechaGeneracion' value="" readonly required>
                    <div class="invalid-feedback">
                        Seleccione la fecha de generación
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="numMontoCancelaciones" class="form-label">Monto de cancelaciones </label>
                    <input type="number" id="numMontoCancelaciones" name="numMontoCancelaciones" class="form-control" placeholder="" title="monto de cancelaciones" min="0" step="0.01" onkeypress="return generalSoloNumeros(event);" value="0" readonly style="color: green; font-weight:bold;">
                </div>
                <div class='col-lg-12 col-xl-12'>
                    <label for='txtObservaciones' class="form-label">Observaciones</label>
                    <input type='text' class='form-control' id='txtObservaciones' name='txtObservaciones' value="">
                </div>
            </div>
        </fieldset>
    </form>
    <!-- Cancelaciones -->
    <ul class="nav nav-tabs nav-tabs-bordered mt-2" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="cancelaciones-tab" data-bs-toggle="tab" data-bs-target="#cancelaciones" role="tab" aria-controls="cancelaciones" aria-selected="true">
                Cancelaciones
            </button>
        </li>
        <li class="nav-item ms-auto">
            <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="configCancelacion.edit();">
                <i class="fas fa-plus-circle"></i> Cancelación
            </button>
        </li>
    </ul>
    <div class="tab-content" style="margin-top: 10px;">
        <div class="tab-pane fade show active" id="cancelaciones" role="tabpanel" aria-labelledby="cancelaciones-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblCancelaciones" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Empresa</th>
                                <th>N° de referencia</th>
                                <th>Tipo de crédito</th>
                                <th>Monto a cancelar</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="mdlCancelacion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="frmCancelacion" name="frmCancelacion" method="POST" class="needs-validation" novalidate accept-charset="utf-8" action="javascript:configCancelacion.save();">
                <div class="modal-header">
                    <h5 class="modal-title">Detalles de cancelación</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6 mayusculas">
                            <label for="txtEmpresaCancelacion" class="form-label">Empresa <span class="requerido">*</span></label>
                            <input type="text" id="txtEmpresaCancelacion" name="txtEmpresaCancelacion" required class="form-control" placeholder="" title="empresa" value="">
                            <div class="invalid-feedback">
                                Ingrese el nombre de la empresa
                            </div>
                        </div>
                        <div class='col-lg-6 col-xl-6'>
                            <label for='txtNumeroReferenciaCancelacion' class="form-label">Número de referencia <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtNumeroReferenciaCancelacion' required name='txtNumeroReferenciaCancelacion' value="">
                            <div class="invalid-feedback">
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <label for='cboTipoCreditoCancelacion' class="form-label">Tipo de crédito <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboTipoCreditoCancelacion" name="cboTipoCreditoCancelacion" required class="selectpicker form-control cboTipoCredito" data-live-search="true">
                                    <option selected value="0" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <label for="numMontoCancelacion" class="form-label">Monto a cancelar <span class="requerido">*</span></label>
                            <input type="number" id="numMontoCancelacion" name="numMontoCancelacion" class="form-control" placeholder="" title="monto cancelación" min="0" step="0.01" required onkeypress="return generalSoloNumeros(event);" value="">
                            <div class="invalid-feedback">
                                Ingrese el monto a cancelar
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Guardar</button>
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="fas fa-times-circle"></i> Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlGeneracion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Generación de documentos</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <div class="buttonContainer spacedAround">

                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12" align="center">
                        <span class="">Intentos restantes de generación: <strong id="strIntentos"></strong></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<?php
$_GET["js"] = ['fordesOID'];
