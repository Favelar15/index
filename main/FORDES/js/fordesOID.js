let tblCancelaciones;

let tipoOID = 'Normal';

let idOID = 0,
    idCancelacion = 0,
    cntCancelaciones = 0,
    intentos = 5;

let tiposCredito = {},
    tiposGarantia = {},
    cancelaciones = {},
    datosFormulario = {};

window.onload = () => {
    document.getElementById('frmOID').reset();
    const gettingCat = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "FORDES",
                archivo: 'fordesGetCats',
                solicitados: ['all'],
            }).then((datosRespuesta) => {
                const opciones = [`<option selected disabled value="">seleccione</option>`];

                if (datosRespuesta.tiposDocumento) {
                    let tmpOpciones = [...opciones],
                        cbos = document.querySelectorAll("select.cboTipoDocumento");
                    datosRespuesta.tiposDocumento.forEach(tipo => {
                        tiposDocumento[tipo.id] = tipo;
                        tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
                    });

                    cbos.forEach(cbo => {
                        cbo.innerHTML = tmpOpciones.join("");
                    });
                }

                if (datosRespuesta.agencias) {
                    let tmpOpciones = [...opciones],
                        cbos = document.querySelectorAll("select.cboAgencia");
                    datosRespuesta.agencias.forEach(agencia => {
                        tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`);
                    });

                    cbos.forEach(cbo => {
                        cbo.innerHTML = tmpOpciones.join("");
                    });
                }

                if (datosRespuesta.tiposCredito) {
                    let tmpOpciones = [...opciones],
                        cbos = document.querySelectorAll("select.cboTipoCredito");
                    datosRespuesta.tiposCredito.forEach(tipo => {
                        tiposCredito[tipo.id] = tipo;
                        tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoCredito}</option>`);
                    });

                    cbos.forEach(cbo => {
                        cbo.innerHTML = tmpOpciones.join("");
                    });
                }

                if (datosRespuesta.tiposGarantia) {
                    let tmpOpciones = [...opciones],
                        cbos = document.querySelectorAll("select.cboTipoGarantia");
                    datosRespuesta.tiposGarantia.forEach(tipo => {
                        tiposGarantia[tipo.id] = tipo;
                        tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoGarantia}</option>`);
                    });

                    cbos.forEach(cbo => {
                        cbo.innerHTML = tmpOpciones.join("");
                    });
                }
                resolve();
            }).catch(reject);
        });
    });

    const allCboSelectpicker = document.querySelectorAll("select.selectpicker");

    gettingCat.then(() => {
        allCboSelectpicker.forEach(cbo => {
            $("#" + cbo.id).selectpicker("refresh");
        });
        $(".preloader").fadeOut("fast");
        $(".preloader").find("span").html("");
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 350;
            if ($("#tblCancelaciones").length) {
                tblCancelaciones = $("#tblCancelaciones").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, "asc"],
                    columnDefs: [{
                            "targets": [0],
                            "orderable": true,
                        },
                        {
                            "targets": [2, 3, 4],
                            "className": "text-start"
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblCancelaciones.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            console.error(err.message);
            reject();
        }
    });
}

const configOID = {
    cboPolicia: document.getElementById("cboPolicia"),
    campoONI: document.getElementById("txtONI"),
    campoInicioCobro: document.getElementById("txtInicioCobro"),
    campoFinCobro: document.getElementById("txtFinCobro"),
    campoFechaGeneracion: document.getElementById("txtFechaGeneracion"),
    campoCuotas: document.getElementById("numCantidadCuotas"),
    contenedorBotonesModal: document.querySelector("#mdlGeneracion .buttonContainer"),
    spnIntentos: document.getElementById("strIntentos"),
    save: function () {
        formActions.validate('frmOID').then(({
            errores
        }) => {
            if (errores == 0) {
                if (!this.campoInicioCobro.value || !this.campoFechaGeneracion.value) {
                    errores++;
                    toastr.warning("Hay campos de fecha que deben ser completados");
                }
            }
            if (errores == 0) {
                let botones = `<button class="btn btn-outline-dark btn-sm" type="button" onclick="configOID.saveDb();"><i class="fas fa-file-pdf"></i> Generar OID</button>`;
                let datosCompletos = formActions.getJSON('frmOID');
                datosCompletos.cancelaciones = {
                    ...cancelaciones
                };

                datosCompletos.id = idOID;

                datosFormulario = {
                    ...datosCompletos
                };

                if (this.cboPolicia.value == 'S') {
                    botones = `<button class="btn btn-outline-dark btn-sm" type="button" onclick="configOID.saveDb('PP');"><i class="fas fa-file-pdf"></i> Generar OID (Referencia PP)</button><button class="btn btn-outline-dark btn-sm" type="button" onclick="configOID.saveDb('PH');"><i class="fas fa-file-pdf"></i> Generar OID (Referencia PH)</button>`;
                }

                this.contenedorBotonesModal.innerHTML = botones;
                this.spnIntentos.innerHTML = intentos;


                $("#mdlGeneracion").modal("show");
            }
        }).catch(generalMostrarError);
    },
    saveDb: function (tipoOID = 'Normal') {
        fetchActions.set({
            modulo: "FORDES",
            archivo: 'fordesGuardarOID',
            datos: {
                ...datosFormulario,
                tipoOID,
                idApartado,
                tipoApartado
            }
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta.trim()) {
                    case "EXITO":
                        intentos--;
                        this.spnIntentos.innerHTML = intentos;
                        userActions.abrirArchivo('FORDES', datosRespuesta.nombreArchivo);
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    evalPolicia: function (opcion) {
        this.campoONI.value = "";
        if (opcion == 'S') {
            this.campoONI.readOnly = false;
            this.campoONI.required = true;
            this.campoONI.classList.add("campoRequerido");
        } else {
            this.campoONI.readOnly = true;
            this.campoONI.required = false;
            this.campoONI.classList.remove("campoRequerido");
        }
    },
    calcFinCobro: function () {
        let fechaFin = "";
        let fechaInicio = this.campoInicioCobro.value;
        if (fechaInicio.length > 0) {
            let tmpCuotas = this.campoCuotas.value,
                cantidadCuotas = tmpCuotas.length > 0 ? parseInt(tmpCuotas) : 0,
                tmpFecha = fechaInicio.split("-"),
                tmpFechaInicio = tmpFecha[1] + "-" + tmpFecha[0] + "-15",
                fechaInicioCobro = new Date(tmpFechaInicio);

            if (cantidadCuotas > 0) {
                fechaInicioCobro.setMonth(fechaInicioCobro.getMonth() + cantidadCuotas);
                fechaFin = ("0" + (fechaInicioCobro.getMonth())).slice(-2) + "-" + fechaInicioCobro.getFullYear();
                if (fechaInicioCobro.getMonth() == 0) {
                    fechaFin = ("0" + (12)).slice(-2) + "-" + (fechaInicioCobro.getFullYear() - 1);
                }
            }
        }
        this.campoFinCobro.value = fechaFin;
    },
    cancel:function(){
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Salir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.reload();
            }
        });
    }
}

const configCancelacion = {
    campoMontoCredito: document.getElementById("numMontoCredito"),
    campoEmpresa: document.getElementById("txtEmpresaCancelacion"),
    campoReferencia: document.getElementById("txtNumeroReferenciaCancelacion"),
    cboTipoCredito: document.getElementById("cboTipoCreditoCancelacion"),
    campoMonto: document.getElementById("numMontoCancelacion"),
    campoMontoCancelaciones: document.getElementById("numMontoCancelaciones"),
    init: function () {
        return new Promise((resolve, reject) => {

        });
    },
    edit: function (id = 0) {
        formActions.clean('frmCancelacion').then(() => {
            let verificando = new Promise((resolve, reject) => {
                idCancelacion = 0;

                if (id > 0) {
                    idCancelacion = id;
                    this.campoEmpresa.value = cancelaciones[id].nombreEmpresa;
                    this.campoReferencia.value = cancelaciones[id].numeroReferencia;
                    this.cboTipoCredito.value = cancelaciones[id].tipoCredito;
                    this.campoMonto.value = cancelaciones[id].monto;

                    if (this.cboTipoCredito.className.includes("selectpicker")) {
                        $("#" + this.cboTipoCredito.id).selectpicker("refresh");
                        $("#" + this.cboTipoCredito.id).selectpicker("val", cancelaciones[id].tipoCredito);
                    }
                }
                resolve();
            });

            verificando.then((resolve, reject) => {
                $("#mdlCancelacion").modal("show");
            });
        });
    },
    save: function () {
        formActions.validate('frmCancelacion').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmpDatos = {
                    id: idCancelacion > 0 ? cancelaciones[idCancelacion].id : 0,
                    nombreEmpresa: this.campoEmpresa.value,
                    numeroReferencia: this.campoReferencia.value,
                    tipoCredito: this.cboTipoCredito.value,
                    labelTipoCredito: this.cboTipoCredito.options[this.cboTipoCredito.options.selectedIndex].text,
                    monto: this.campoMonto.value
                };

                let guardando = new Promise((resolve, reject) => {
                    if (idCancelacion == 0) {
                        cntCancelaciones++;
                        cancelaciones[cntCancelaciones] = {
                            ...tmpDatos
                        };
                        let agregando = this.addRowTbl({
                            ...tmpDatos,
                            idFila: cntCancelaciones
                        });
                        agregando.then(() => {
                            resolve();
                        }).catch(reject);
                    } else {
                        cancelaciones[idCancelacion] = {
                            ...tmpDatos
                        };
                        let tipoCreditoLabel = tiposCredito[tmpDatos.tipoCredito].tipoCredito;
                        $("#trCancelacion" + idCancelacion).find("td:eq(1)").html(this.campoEmpresa.value);
                        $("#trCancelacion" + idCancelacion).find("td:eq(2)").html(this.campoReferencia.value);
                        $("#trCancelacion" + idCancelacion).find("td:eq(3)").html(tipoCreditoLabel);
                        $("#trCancelacion" + idCancelacion).find("td:eq(4)").html("$" + parseFloat(this.campoMonto.value).toFixed(2));
                        resolve();
                    }
                });

                guardando.then(() => {
                    $("#mdlCancelacion").modal("hide");
                    this.evalMonto();
                });
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        nombreEmpresa,
        numeroReferencia,
        tipoCredito,
        monto
    }) {
        return new Promise((resolve, reject) => {
            try {
                let tipoCreditoLabel = tiposCredito[tipoCredito].tipoCredito;
                tblCancelaciones.row.add([
                    idFila,
                    nombreEmpresa,
                    numeroReferencia,
                    tipoCreditoLabel,
                    "$" + parseFloat(monto).toFixed(2),
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configCancelacion.edit(" + idFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configCancelacion.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = 'trCancelacion' + idFila;
                tblCancelaciones.draw(false);
                tblCancelaciones.columns.adjust().draw();
                resolve();
            } catch (err) {
                console.error(err.message);
                reject();
            }
        });
    },
    delete: function (id = 0) {
        let empresa = cancelaciones[id].nombreEmpresa,
            referencia = cancelaciones[id].numeroReferencia;
        Swal.fire({
            title: 'Eliminar cancelación',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la cancelación a la empresa ' + empresa + ' con número de referencia: ' + referencia + '?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblCancelaciones.row('#trCancelacion' + id).remove().draw();
                tblCancelaciones.columns.adjust().draw();
                delete cancelaciones[id];
            }
        });
    },
    evalMonto: function () {
        let montoCredito = this.campoMontoCredito.value.length > 0 ? parseFloat(this.campoMontoCredito.value) : 0,
            montoCancelaciones = 0;

        for (let key in cancelaciones) {
            let monto = parseFloat(cancelaciones[key].monto);
            montoCancelaciones += monto;
        }

        this.campoMontoCancelaciones.value = montoCancelaciones.toFixed(2);
        if (montoCancelaciones > montoCredito) {
            this.campoMontoCancelaciones.style.color = "red";
        } else {
            this.campoMontoCancelaciones.style.color = "green";
        }
    }
}