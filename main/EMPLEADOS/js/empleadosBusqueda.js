let tblEmpleados;

window.onload = () => {
    document.getElementById("frmReporte").reset();
    $(".preloader").fadeOut("fast");
    initTables();
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblEmpleados").length) {
                tblEmpleados = $("#tblEmpleados").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2, 3, 4, 5, 6],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de contrato';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblEmpleados.columns.adjust().draw();
                resolve();
            }
        } catch (err) {
            console.error(err.message);
            reject();
        }
    });
}

const configEmpleados = {
    id: 0,
    cnt: 0,
    datos: {},
    cboTipo: document.getElementById('cboTipoBusqueda'),
    campoDato: document.getElementById("txtDato"),
    evalTipoBusqueda: function (tipo) {
        this.campoDato.value = '';
        $("#" + this.campoDato.id).unmask();

        tipo == 'DUI' && ($("#" + this.campoDato.id).mask('00000000-0'));
    },
    search: function () {
        formActions.validate('frmReporte').then(({
            errores
        }) => {
            if (errores == 0) {
                fetchActions.get({
                    modulo: "EMPLEADOS",
                    archivo: "empleadosBuscar",
                    params: {
                        tipo: encodeURIComponent(this.cboTipo.value),
                        dato: encodeURIComponent(this.campoDato.value)
                    }
                }).then(this.init).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    init: function (tmpDatos) {
        tblEmpleados.clear().draw();
        configEmpleados.datos = {};
        configEmpleados.datos = {
            ...tmpDatos
        };
        for (let key in tmpDatos) {
            configEmpleados.cnt++;
            configEmpleados.addRowTbl({
                ...tmpDatos[key],
                nFila: configEmpleados.cnt
            });
        }
        tblEmpleados.columns.adjust().draw();
    },
    addRowTbl: function ({
        nFila,
        indice,
        DUI: dui,
        NIT: nit,
        nombreCompleto,
        direccionEmpleado,
        registros
    }) {
        return new Promise((resolve, reject) => {
            try {
                try {
                    let ultimaCotizacion = "Sin registros";
                    registros.length > 0 && (ultimaCotizacion = registros[0].periodoRegistro);
                    tblEmpleados.row.add([
                        nFila,
                        dui,
                        nit,
                        nombreCompleto,
                        direccionEmpleado,
                        ultimaCotizacion,
                        "<div class='tblButtonContainer'>" +
                        "<span class='btnTbl' title='Generar reporte' onclick='configEmpleados.print(" + indice + ");'>" +
                        "<i class='fas fa-print'></i>" +
                        "</span>" +
                        "</div>"
                    ]);
                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            } catch (err) {
                reject(err.message);
            }
        });
    },
    print: function (id) {
        fetchActions.set({
            modulo: "EMPLEADOS",
            archivo: 'empleadosConstruirPDF',
            datos: this.datos[id]
        }).then((datosRespuesta) => {
            if (datosRespuesta.nombreArchivo) {
                window.open(`./main/EMPLEADOS/docs/${datosRespuesta.nombreArchivo}`);
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    }
}