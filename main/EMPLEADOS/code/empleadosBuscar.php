<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

if (!empty($_GET)) {
    include "../../code/connectionSqlServer.php";
    include "../../code/generalParameters.php";

    $tipoBusqueda = urldecode($_GET["tipo"]);
    $datoBuscar = urldecode($_GET["dato"]);

    $condicion = $tipoBusqueda == "DUI" ? "numeroDUI='{$datoBuscar}'" : "nombreCompleto like '%{$datoBuscar}%'";

    $query = "SELECT * from empleados_datosFormales WHERE " . $condicion . " AND habilitado = 'S' ORDER BY mesReportado DESC";

    $result = $conexion->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
    $result->execute();

    if ($result->rowCount() > 0) {
        while ($dato = $result->fetch(PDO::FETCH_ASSOC)) {
            $indice = $dato["idEmpleado"];
            $numeroDUI = $dato["numeroDUI"];
            $numeroNIT = $dato["numeroNIT"];
            $nombreCompleto = $dato["nombreCompleto"];
            $direccionEmpleado = $dato["direccionEmpleado"];
            $telefonoEmpleado = $dato["telefono"];
            $registros = [];
            if (!isset($respuesta->$indice)) {
                $respuesta->$indice = new tmpResultado($indice, $numeroDUI, $numeroNIT, $nombreCompleto, $direccionEmpleado, $telefonoEmpleado, $registros);
            }

            $identificador = $dato["id"];
            $identificadorPatrono = $dato["idEmpresa"];
            $nombreEmpresa = $dato["nombreEmpresa"];
            $direccionEmpresa = $dato["direccionEmpresa"];
            $telefonoEmpresa = $dato["telefonoEmpresa"];
            $periodoCotizado = date("m-Y", strtotime($dato["mesReportado"]));
            $salario = !empty($dato["salario"]) ? "$" . number_format($dato["salario"], 2, '.', ',') : "Sin registro";

            $respuesta->$indice->registros[] = new tmpRegistro($identificador, $identificadorPatrono, $nombreEmpresa, $direccionEmpresa, $telefonoEmpresa, $periodoCotizado, $salario);
        }
    }

    $conexion = null;
}

class tmpResultado
{
    public $indice;
    public $DUI;
    public $NIT;
    public $nombreCompleto;
    public $direccionEmpleado;
    public $telefonoEmpleado;
    public $registros;

    function __construct($indice, $DUI, $NIT, $nombreCompleto, $direccionEmpleado, $telefonoEmpleado, $registros)
    {
        $this->indice = $indice;
        $this->DUI = $DUI;
        $this->NIT = $NIT;
        $this->nombreCompleto = $nombreCompleto;
        $this->direccionEmpleado = $direccionEmpleado;
        $this->telefonoEmpleado = $telefonoEmpleado;
        $this->registros = $registros;
    }
}

class tmpRegistro
{
    public $id;
    public $idEmpresa;
    public $nombreEmpresa;
    public $direccionEmpresa;
    public $telefonoEmpresa;
    public $periodoRegistro;
    public $salario;

    function __construct($id, $idEmpresa, $nombreEmpresa, $direccionEmpresa, $telefonoEmpresa, $periodoRegistro, $salario)
    {
        $this->id = $id;
        $this->idEmpresa = $idEmpresa;
        $this->nombreEmpresa = $nombreEmpresa;
        $this->direccionEmpresa = $direccionEmpresa;
        $this->telefonoEmpresa = $telefonoEmpresa;
        $this->periodoRegistro = $periodoRegistro;
        $this->salario = $salario;
    }
}

echo json_encode($respuesta);
