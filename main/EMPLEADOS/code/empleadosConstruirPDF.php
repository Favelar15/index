<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
} else {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';
}

require_once '../../code/generalParameters.php';

if (count($input) > 0) {
    class construccionPDF extends TCPDF
    {
        function configuracionDocumento()
        {
            $this->SetCreator(PDF_CREATOR);
            $this->SetAuthor('Departamento IT');
            $this->SetTitle('Reporte de empleado formal');
            $this->SetSubject('ACACYPAC');
            $this->SetKeywords('TCPDF, PDF, Reporte de empleado formal');

            $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        }

        function setMargin()
        {
            $this->SetMargins(15, 15, 15, 15);
            $this->SetHeaderMargin(4);
            $this->SetFooterMargin(PDF_MARGIN_FOOTER);
        }

        function Header()
        {
            $this->setFont('dejavusans', 'B', 10);
            // $this->Write(0, 'N° de cliente: ' . $this->codigoCliente, '', 0, 'R', true, 0, false, false, 0);
        }

        function Footer()
        {
        }

        public function setDatosPersonales($input)
        {
            $this->SetFillColor(0, 95, 85);
            $this->SetTextColor(255);
            $this->SetDrawColor(0, 95, 85);
            $this->SetLineWidth(0.3);
            $this->SetFont('', 'B');
            // $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 193, 7)));

            $this->Cell(249, 7, 'DATOS PERSONALES', 1, 0, 'C', 1);
            $this->ln();
            $this->SetFont('', '');
            $this->SetTextColor(255);
            $this->SetFillColor(17, 142, 129);
            $this->Cell(50, 6, 'Nombre completo:', 1, 0, 'L', 1);
            $this->SetTextColor(0);
            $this->Cell(199, 6, $input["nombreCompleto"], 1, 0, 'L', 0);
            $this->ln();
            $this->SetTextColor(255);
            $this->Cell(50, 6, 'DUI:', 1, 0, 'L', 1);
            $this->SetTextColor(0);
            $this->Cell(50, 6, $input["DUI"], 1, 0, 'L', 0);
            $this->SetTextColor(255);
            $this->Cell(50, 6, 'NIT:', 1, 0, 'L', 1);
            $this->SetTextColor(0);
            $this->Cell(99, 6, $input["NIT"], 1, 0, 'L', 0);
            $this->ln();
            $this->SetTextColor(255);
            $this->Cell(50, 6, 'Dirección:', 1, 0, 'L', 1);
            $this->SetTextColor(0);
            $this->Cell(100, 6, $input["direccionEmpleado"], 1, 0, 'L', 0);
            $this->SetTextColor(255);
            $this->Cell(50, 6, 'Fecha de generación:', 1, 0, 'L', 1);
            $this->SetTextColor(0);
            $this->Cell(49, 6, date('d-m-Y h:i a'), 1, 0, 'L', 0);
            $this->ln();
        }

        public function setRegistros($registros)
        {
            $tbl = '<style>table th{background-color:rgb(0, 95, 85); color:white; border:1px solid black; font-weight:bold;} table td{border:1px solid black;}</style><table cellpadding="3" cellspacing="0" style="text-align:center;" nobr="true">' .
                '<thead>' .
                '<tr>' .
                '<th style="width: 40px; text-align:center; font-size:12px;">N°</th>' .
                '<th style="width: 291px; text-align:center; font-size:12px;">Empresa</th>' .
                '<th style="width: 301px; text-align:center; font-size:12px;">Dirección</th>' .
                '<th style="width: 80px; text-align:center; font-size:12px;">Teléfono</th>' .
                '<th style="width: 80px; text-align:center; font-size:12px;">Mes</th>' .
                '<th style="width: 90px; text-align:center; font-size:12px;">Salario</th>' .
                '</tr>' .
                '</thead>' .
                '<tbody>';

            $cnt = 0;

            foreach ($registros as $key => $registro) {
                if ($cnt <= 12) {
                    $cnt++;
                    $tbl .= '<tr>' .
                        '<td style="width: 40px;">' . $cnt . '</td>' .
                        '<td style="width: 291px;">' . $registro["nombreEmpresa"] . '</td>' .
                        '<td style="width: 301px;">' . $registro["direccionEmpresa"] . '</td>' .
                        '<td style="width: 80px;">' . $registro["telefonoEmpresa"] . '</td>' .
                        '<td style="width: 80px;">' . $registro["periodoRegistro"] . '</td>' .
                        '<td style="width: 90px;">' . $registro["salario"] . '</td>' .
                        '</tr>';
                }
            }

            $tbl .= '</tbody>' .
                '</table>';
            $this->SetFont('dejavusans', '', 7.8);
            // $this->SetTextColor(0);
            $this->writeHTML($tbl, true, false, true, false, '');
            $this->ln();

            $this->SetFillColor(224, 235, 255);
            $this->SetTextColor(0);
            $this->SetFont('');
        }
    }

    $path = __DIR__ . '\..\docs\/';

    $nombreArchivo = 'EmpleadoFormal' . $input["indice"] . ".pdf";

    $pdf = new construccionPDF('L', PDF_UNIT, 'LETTER', true, 'UTF-8', false);
    // $pdf->codigoCliente = $solicitud->codigoCliente;
    $pdf->configuracionDocumento();
    $pdf->setMargin();

    $pdf->SetAutoPageBreak(TRUE, 35);

    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    $pdf->AddPage();

    $pdf->setFont('dejavusans', 'B', 12);
    $pdf->Write(0, 'REPORTE DE EMPLEADO FORMAL', '', 0, 'C', true, 0, false, false, 0);
    $pdf->ln(3);

    $pdf->setFont('dejavusans', '', 10);
    $pdf->setDatosPersonales($input);

    $pdf->ln(10);

    $pdf->setRegistros($input["registros"]);

    $pdf->Output($path . $nombreArchivo, 'F');

    $respuesta->{"nombreArchivo"} = $nombreArchivo;
}

echo json_encode($respuesta);
