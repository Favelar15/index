<div class="pagetitle">
    <h1>Empleados formales - Búsqueda</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Empleados formales</li>
            <li class="breadcrumb-item active">Búsqueda</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <form action="javascript:configEmpleados.search();" id="frmReporte" name="frmReporte" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-1 col-xl-1"></div>
                    <div class="col-lg-10 col-xl-10">
                        <div class="row">
                            <div class="col-lg-4 col-xl-4">
                                <label for="cboTipoBusqueda" class="form-label">Tipo de búsqueda <span class="requerido">*</span></label>
                                <select id="cboTipoBusqueda" name="cboTipoBusqueda" class="form-select" required onchange="configEmpleados.evalTipoBusqueda(this.value);">
                                    <option selected value="DUI">DUI</option>
                                    <option value="Nombre">Nombre</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione una opción
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4 mayusculas">
                                <label for="txtDato" class="form-label">Dato a buscar <span class="requerido">*</span></label>
                                <input type="text" id="txtDato" name="txtDato" class="form-control DUI" required>
                                <div class="invalid-feedback">
                                    Ingrese un valor
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <label class="form-label" style="color:white;">.</label>
                                <div class="d-grid">
                                    <button type="submit" class="btn btn-outline-primary">
                                        <i class="fas fa-search"></i> Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-xl-1"></div>
                </div>
            </form>
            <hr class="mb-0 mt-2">
        </div>
        <div class="col-lg-12 col-xl-12 mt-3">
            <table id="tblEmpleados" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>DUI</th>
                        <th>NIT</th>
                        <th>Nombre completo</th>
                        <th>Dirección</th>
                        <th>Último registro</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php
$_GET["js"] = ["empleadosBusqueda"];
