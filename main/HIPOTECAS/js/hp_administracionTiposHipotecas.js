let tblAdministracionTiposHipotecas;

window.onload = () => {
    hp_initializeDatatable();
    fetchActions.get({
        modulo: "HIPOTECAS",
        archivo: "hp_procesarTipoHipoteca",
        params: {
            'accion': 'obtenertipoHipotecaGeneral'
        }
    }).then((arregloHipotecas) => {
        configTipoHipoteca.init(arregloHipotecas);
    }).catch(generalMostrarError);

}

const configTipoHipoteca = {
    id: 0,
    cnt: 0,
    tiposHipoteca: {},
    campoTipoHipoteca: document.getElementById("txtTipoHipoteca"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            tmpDatos.forEach(tipo => {
                this.cnt++;
                this.tiposHipoteca[this.cnt] = {
                    ...tipo
                };
                this.addRowTbl({
                    ...tipo,
                    nFila: this.cnt
                });
            });
            tblAdministracionTiposHipotecas.columns.adjust().draw();
            resolve();
        });
    },
    edit: function (id = 0) {
        formActions.clean('mdlAdmTipoHipoteca').then(() => {
            this.id = id;
            if (this.id > 0) {
                this.campoTipoHipoteca.value = this.tiposHipoteca[id].tipoHipoteca;
            }

            $("#mdlAdmTipoHipoteca").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmTipoHipoteca').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.tiposHipoteca[this.id].id : 0,
                    tipoHipoteca: this.campoTipoHipoteca.value
                };

                let logsCambios = '';

                let guardar = new Promise((resolve, reject) => {
                    try {
                        fetchActions.set({
                            modulo: "HIPOTECAS",
                            archivo: "hp_procesarTipoHipoteca",
                            datos: {
                                ...tmp,
                                idApartado,
                                tipoApartado,
                                accion: 'guardarTipoHipoteca',
                                logsCambios
                            }
                        }).then(hp_generalmostrarAlertas).catch(reject);
                    } catch (err) {
                        reject(err.message);
                    }
                });

                guardar.then(() => {
                    this.tiposHipoteca[this.id] = {
                        ...tmp
                    };
                    tblAdministracionTiposHipotecas.columns.adjust().draw();
                    $("#mdlAdmTipoHipoteca").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        tipoHipoteca
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblAdministracionTiposHipotecas.row.add([
                    nFila,
                    tipoHipoteca,
                    "<div class='tblButtonContainer' align='center'>" +
                    "<span class='btnTbl' title='Editar tipo hipoteca' onclick='configTipoHipoteca.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar tipo hipoteca' onclick='configTipoHipoteca.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"

                ]).node().id = "trTipoHipoteca" + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: "HIPOTECAS",
                    archivo: "hp_procesarTipoHipoteca",
                    datos: {
                        id: this.tiposHipoteca[id].id,
                        accion: 'eliminarTipoHipoteca',
                        idApartado,
                        tipoApartado
                    }
                }).then((respuesta) => {
                    hp_generalmostrarAlertas(respuesta);
                });
            }
        });
    },
};

function hp_initializeDatatable() {
    let alturaTabla = 350;
    if ($("#tblAdministacionTipoHipotecas").length) {
        tblAdministracionTiposHipotecas = $("#tblAdministacionTipoHipotecas").DataTable({
            dateFormat: 'uk',
            order: [0, "asc"],
        });
        tblAdministracionTiposHipotecas.columns.adjust().draw();
    }

    return;
}