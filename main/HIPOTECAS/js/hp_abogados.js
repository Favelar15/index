//PENDIENTES
//Al guardar debería de regresar a la pantalla de administración de abogados

let tblAdministracionAbogados,
    tblAsignacionAgenciaAbogados;

window.onload = () => {
    document.getElementById("frmAbogado").reset();

    const cboPais = document.querySelectorAll('select.cboPais');
    const cboEstadoAbogado = document.querySelectorAll("select.cboEstadoAbogado");
    const cbosTipoDocumento = document.querySelectorAll('select.cboTipoDocumento');

    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo,
                archivo: 'hp_getCats',
                solicitados: ['agenciasCbo', 'tiposDocumento', 'geograficos', 'estadosAbogadoCbo']
            }).then(({
                tiposDocumento: tmpTipoDocumento = [],
                agencias = [],
                datosGeograficos: tmpDatosGeograficos = {},
                estadosAbogado = []
            }) => {
                $(".preloader").fadeIn("fast");
                const opciones = [`<option selected disabled value="">seleccione</option>`];
                let tmpOpciones = [...opciones];
                tmpTipoDocumento.forEach(tipo => {
                    tiposDocumento[tipo.id] = tipo;
                    tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
                });

                cbosTipoDocumento.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                agencias.forEach(agencia => configAgencia.catalogo[agencia.id] = {
                    ...agencia
                });

                tmpOpciones = [...opciones];

                for (let key in tmpDatosGeograficos) {
                    datosGeograficos[tmpDatosGeograficos[key].id] = {
                        ...tmpDatosGeograficos[key]
                    }
                    tmpOpciones.push(`<option value="${tmpDatosGeograficos[key].id}">${tmpDatosGeograficos[key].nombrePais}</option>`);
                }

                cboPais.forEach(cbo => cbo.innerHTML = tmpOpciones.join(''));

                tmpOpciones = [...opciones];
                estadosAbogado.forEach(estado => tmpOpciones.push(`<option value="${estado.id}">${estado.estadoAbogado}</option>`));

                cboEstadoAbogado.forEach(cbo => cbo.innerHTML = tmpOpciones.join(''));

                resolve();
            }).catch(reject);
        });
    });

    gettingCats.then(() => {
        let allCbosSelectpicker = document.querySelectorAll("select.selectpicker");
        allCbosSelectpicker.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
        let id = getParameterByName('id');
        if (id.length > 0) {
            configAbogado.init(id);
        } else {
            $(".preloader").fadeOut("fast");
        }
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 300;
            if ($("#tblAsignacionAgenciaAbogados").length) {
                tblAsignacionAgenciaAbogados = $("#tblAsignacionAgenciaAbogados").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    sortable: true,
                });
                tblAsignacionAgenciaAbogados.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configAbogado = {
    id: 0,
    agencias: {},
    init: function (id) {
        this.id = atob(decodeURIComponent(id));

        fetchActions.get({
            modulo: "HIPOTECAS",
            archivo: "hp_procesarAbogado",
            params: {
                'idAbogado': id,
                'accion': 'obtenerAbogadoEspecifico'
            }
        }).then(([datosAbogado]) => {
            $(".preloader").fadeIn("fast");
// console.log(datosAbogado)
            document.getElementById('cboTipoDocumentoAbogado').value = datosAbogado['idTipoDocumentoPrimario'];
            $("#cboTipoDocumentoAbogado").trigger("change");
            document.getElementById('txtDocumentoPrimarioAbogado').value = datosAbogado['numeroDocumentoPrimario'];
            document.getElementById('txtNitAbogado').value = datosAbogado['numeroNIT'];
            document.getElementById('txtNombresAbogado').value = datosAbogado['nombresAbogado'];
            document.getElementById('txtApellidosAbogado').value = datosAbogado['apellidosAbogado'];
            document.getElementById('txtNumeroAbogado').value = datosAbogado['numeroAbogado'];
            document.getElementById('txtTelefonoCelular').value = datosAbogado['numeroTelefono'];
            document.getElementById('cboEstadoAbogado').value = datosAbogado['idEstadoAbogado'];
            document.getElementById('dtpInicioContrato').value = datosAbogado['inicioContrato'];
            $("#dtpInicioContrato").datepicker("update",datosAbogado["inicioContrato"]);
            document.getElementById('dtpFinContrato').value = datosAbogado['finContrato'];
            $("#dtpFinContrato").datepicker("update",datosAbogado["finContrato"]);

            if (datosAbogado["idPais"] != null) {
                document.getElementById("cboPaisAbogado").value = datosAbogado["idPais"];
                $("#cboPaisAbogado").trigger("change");
                document.getElementById("cboDepartamentoAbogado").value = datosAbogado["idDepartamento"];
                $("#cboDepartamentoAbogado").trigger("change");
                document.getElementById("cboMunicipioAbogado").value = datosAbogado["idMunicipios"];
            }

            let todosCampos = document.querySelectorAll("#frmAbogado .form-control"),
                agencias = datosAbogado['idAgenciasAsignadas'].split('@@@');

            todosCampos = [...todosCampos, ...document.querySelectorAll("#frmAbogado .form-select")];
            configAgencia.init([...agencias]);

            todosCampos.forEach(campo => {
                if (campo.id) {
                    if (campo.type == 'select-one') {
                        $("#" + campo.id).data("valorInicial", campo.options[campo.options.selectedIndex].text);
                    } else {
                        $("#" + campo.id).data("valorInicial", campo.value);
                    }

                    if (campo.className.includes("selectpicker")) {
                        $("#" + campo.id).selectpicker("refresh");
                    }
                }
            });
            $(".preloader").fadeOut("fast");
        }).catch(generalMostrarError);
    },
    save: function () {
        formActions.validate('frmAbogado').then(({
            errores
        }) => {
            if (errores == 0) {
                if (Object.keys(this.agencias).length == 0) {
                    errores++;
                    Swal.fire({
                        title: "Atención",
                        icon: "warning",
                        html: "Debe asignar al menos una agencia"
                    });
                }
            }
            if (errores == 0) {
                let datosAbogado = formActions.getJSON('frmAbogado'),
                    camposRequeridos = document.querySelectorAll("#frmAbogado .form-control");

                camposRequeridos = [...camposRequeridos, ...document.querySelectorAll('#frmAbogado .form-select')];

                const {
                    contadorEditados,
                    camposEditados
                } = hp_generarStringCambios(camposRequeridos);
                if (contadorEditados > 0) {
                    datosAbogado["identificadorAbogado"] = this.id;
                    datosAbogado["datosAgencias"] = this.agencias;
                    datosAbogado["datosCambiosAbogado"] = camposEditados;
                    datosAbogado['accion'] = 'guardarAbogado';
                    datosAbogado['idApartado'] = idApartado;
                    datosAbogado['tipoApartado'] = tipoApartado;

                    fetchActions.set({
                        modulo: "HIPOTECAS",
                        archivo: "hp_procesarAbogado",
                        datos: datosAbogado
                    }).then(hp_generalmostrarAlertas).catch(generalMostrarError);
                } else {
                    toastr.warning("Actualemnte no se han detectado cambios", "Atención");
                }
            }
        }).catch(generalMostrarError);
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Salir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                let urlModulo = encodeURIComponent(btoa('HIPOTECAS'));
                window.location.href = `?page=hp_administrarAbogados&mod=${urlModulo}`;
            }
        });
    }
};

const configAgencia = {
    id: 0,
    cnt: 0,
    catalogo: {},
    cboAgencia: document.getElementById('mdl_cboAgenciaAsignada'),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                this.cnt = 0;
                tmpDatos.forEach(agencia => {
                    this.cnt++;
                    let tmp = {
                        id: agencia,
                        nombreAgencia: this.catalogo[agencia].nombreAgencia
                    };

                    configAbogado.agencias[this.cnt] = {
                        ...tmp
                    };
                    this.addRowTbl({
                        ...tmp,
                        nFila: this.cnt
                    });
                });

                tblAsignacionAgenciaAbogados.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmAgencia').then(() => {
            this.id = id;

            let incluidos = [],
                opciones = [`<option selected disabled value="">seleccione</option>`];

            for (let key in configAbogado.agencias) {
                if (configAbogado.agencias[key].id != this.id) {
                    incluidos.push(configAbogado.agencias[key].id);
                }
            }

            for (let key in this.catalogo) {
                if (!incluidos.includes(this.catalogo[key].id)) {
                    opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].nombreAgencia}</option>`);
                }
            }

            this.cboAgencia.innerHTML = opciones.join("");

            if (this.id > 0) {
                this.cboAgencia.value = configAbogado.agencias[this.id].id;
            }

            $("#mdlConfigAgenciaAbogado").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmAgencia').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.cboAgencia.value,
                    nombreAgencia: this.cboAgencia.options[this.cboAgencia.options.selectedIndex].text
                };

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trAgencia" + this.id).find("td:eq(1)").html(tmp.nombreAgencia);
                        resolve();
                    }
                });

                guardar.then(() => {
                    configAbogado.agencias[this.id] = {
                        ...tmp
                    };
                    tblAsignacionAgenciaAbogados.columns.adjust().draw();
                    $("#mdlConfigAgenciaAbogado").modal("hide");
                });
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        nombreAgencia
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblAsignacionAgenciaAbogados.row.add([
                    nFila,
                    nombreAgencia,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configAgencia.delete(" + nFila + ")'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trAgencia" + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información ingresada.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO'
        }).then((result) => {
            if (result.isConfirmed) {
                tblAsignacionAgenciaAbogados.row('#trAgencia' + id).remove().draw();
                delete configAbogado.agencias[id];
            }
        });
    }
};

function hp_requerirFechaFin(objCboEstado) {
    if (objCboEstado.value == 1 || objCboEstado.value == 0) {
        document.getElementById("dtpFinContrato").required = false;
    } else {
        document.getElementById("dtpFinContrato").required = true;
    }
}

function hp_mostrarMensajeIntenteNuevamente() {
    Swal.fire({
        title: '¡Upss!',
        text: "Al parecer ocurrió un error inesperado, favor intentar nuevamente.",
        icon: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'OK'
    }).then((result) => {})
}