
let tblModificaciones,
    tblMatriculasHipoteca,
    tblGarantiasHipoteca,
    tblObservacionesHipoteca,
    tblPrendas;

let tmpLogCambiosHipoteca = [],
    logsCambiosHipoteca = null,
    contadorModificaciones = 0,
    numeroFila = 1,
    numeroFilaGarantia = 1,
    numeroFilaMatricula = 1,
    numeroFilaObservacion = 1,
    montoHipotecaModificaciones = 0,
    plazoHipotecaModficiaciones = 0;

window.onload = () => {
    localStorage.removeItem('logCambiosHipoteca');
    localStorage.removeItem('logCambiosGarantias');
    localStorage.removeItem('logCambiosMatriculas');
    localStorage.removeItem('logCambiosObservaciones');

    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "HIPOTECAS",
                archivo: "hp_getCats",
                solicitados: ['agenciasCbo', 'estadosHipoteca', 'tiposHipotecaGeneral', 'plazosHipoteca', 'garantiasHipoteca', 'ubicacionesHipoteca', 'tiposGarantias']
            }).then(({
                         agencias,
                         estadosHipoteca,
                         tiposHipoteca,
                         plazosHipoteca,
                         garantiasHipoteca,
                         ubicacionesHipoteca,
                         tiposGarantias
                     }) => {
                $(".preloader").fadeIn("fast");
                const opciones = [`<option selected disabled value="">seleccione</option>`];

                let tmpOpciones = [...opciones];
                agencias.forEach(agencia => {
                    configHipoteca.agencias[agencia.id] = {
                        ...agencia
                    };
                    tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`);
                });

                let cbosAgencias = document.querySelectorAll("select.cboAgencia");
                cbosAgencias.forEach(cbo => {
                    cbo.innerHTML = tmpOpciones.join("");
                });

                tmpOpciones = [...opciones];
                estadosHipoteca.forEach(estado => {
                    configHipoteca.estados[estado.id] = {
                        ...estado
                    };
                    tmpOpciones.push(`<option value="${estado.id}">${estado.estadoHipoteca}</option>`);
                });

                let cbosEstados = document.querySelectorAll("select.cboEstadoHipoteca");
                cbosEstados.forEach(cbo => {
                    cbo.innerHTML = tmpOpciones.join("");
                });

                tmpOpciones = [...opciones];
                tiposHipoteca.forEach(tipo => {
                    configHipoteca.tipos[tipo.id] = {
                        ...tipo
                    };
                    tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoHipoteca}</option>`);
                });

                let cbosTipos = document.querySelectorAll("select.cboTipoHipoteca");
                cbosTipos.forEach(cbo => {
                    cbo.innerHTML = tmpOpciones.join("");
                });

                tmpOpciones = [...opciones];
                plazosHipoteca.forEach(plazo => {
                    configHipoteca.plazos[plazo.id] = {
                        ...plazo
                    };
                    tmpOpciones.push(`<option value="${plazo.id}">${plazo.nombrePlazo}</option>`);
                });

                let cbosPlazo = document.querySelectorAll("select.cboPlazoHipoteca");
                cbosPlazo.forEach(cbo => {
                    cbo.innerHTML = tmpOpciones.join("");
                });

                tmpOpciones = [...opciones];
                garantiasHipoteca.forEach(garantia => {
                    configHipoteca.garantias[garantia.id] = {
                        ...garantia
                    };
                    tmpOpciones.push(`<option value="${garantia.id}">${garantia.tipoGarantia}</option>`);
                });

                let cbosGarantia = document.querySelectorAll("select.cboGarantiaHipoteca");
                cbosGarantia.forEach(cbo => {
                    cbo.innerHTML = tmpOpciones.join("");
                });

                tmpOpciones = [...opciones];
                ubicacionesHipoteca.forEach(ubicacion => {
                    configHipoteca.ubicacionesHipoteca[ubicacion.id] = {
                        ...ubicacionesHipoteca
                    };
                    tmpOpciones.push(`<option value="${ubicacion.id}">${ubicacion.ubicacion}</option>`);
                });
                let cboUbicacion = document.querySelectorAll("select.cboUbicacionHipoteca");

                cboUbicacion.forEach(cbo => {
                    cbo.innerHTML = tmpOpciones.join("");
                });

                tmpOpciones = [...opciones];
                tiposGarantias.forEach(garantia => {
                    configHipoteca.tipoGarantias[garantia.id] = {
                        ...garantia
                    };
                    tmpOpciones.push(`<option value="${garantia.id}">${garantia.nombre}</option>`);
                });
                let cboTipoGarantia = document.querySelectorAll("select.cboTipoGarantia");
                console.log(tmpOpciones);
                cboTipoGarantia.forEach(cbo => {
                    cbo.innerHTML = tmpOpciones.join("");
                });

                resolve();
            }).catch(reject);
        });
    });

    gettingCats.then(() => {
        $(".preloader").fadeOut("fast");
        let allSelectpickerCbos = document.querySelectorAll('select.selectpicker');
        allSelectpickerCbos.forEach(cbo => {
            $("#" + cbo.id).selectpicker("refresh");
        });
        document.getElementById('numMontoHipotecaCancelada').disabled =  true;
        document.getElementById('dtpFechaCancelacion').disabled =true;
        let id = getParameterByName('id');
        if (id.length > 0) {
            configHipoteca.init(id);
        } else {
            $(".preloader").fadeOut("fast");
        }
    });
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 350;
            if ($("#tblAdministacionHipotecas").length) {
                administracionHipotecas = $("#tblAdministacionHipotecas").DataTable({
                    dateFormat: 'uk',
                    scrollY: alturaTabla,
                    order: [0, "asc"],
                });
                administracionHipotecas.columns.adjust().draw();
            }
            if ($("#tblModificaciones").length) {
                tblModificaciones = $("#tblModificaciones").DataTable({
                    dateFormat: 'uk',
                    order: [0, "asc"],
                });
                tblModificaciones.columns.adjust().draw();
            }
            if ($("#tblGarantiasHipoteca").length) {
                tblGarantiasHipoteca = $("#tblGarantiasHipoteca").DataTable({
                    dateFormat: 'uk',
                    order: [0, "asc"],
                });
                tblGarantiasHipoteca.columns.adjust().draw();
            }
            if ($("#tblMatricula").length) {
                tblMatriculasHipoteca = $("#tblMatricula").DataTable({
                    dateFormat: 'uk',
                    order: [0, "asc"],
                });
                tblMatriculasHipoteca.columns.adjust().draw();
            }
            if ($("#tblObservaciones").length) {
                tblObservacionesHipoteca = $("#tblObservaciones").DataTable({
                    dateFormat: 'uk',
                    order: [0, "asc"],
                });
                tblObservacionesHipoteca.columns.adjust().draw();
            }
            if ($("#tblPrendas").length) {
                tblPrendas = $("#tblPrendas").DataTable({
                    dateFormat: 'uk',
                    order: [0, "asc"],
                });
                tblPrendas.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configHipoteca = {
    id: 0,
    cnt: 0,
    datos: {},
    agencias: {},
    estados: {},
    tipos: {},
    plazos: {},
    garantias: {},
    ubicacionesHipoteca: {},
    tipoGarantias : {},
    prendas: {},
    campoMonto: document.getElementById('numMontoHipoteca'),
    campoPlazo: document.getElementById('numPlazoHipoteca'),
    campoPlazoModificaciones: document.getElementById('numPlazoModificaciones'),
    campoMontoModificaciones: document.getElementById('numMontoModificaciones'),
    btnBuscarDatosMutuoHipotecario:  document.getElementById('btnBuscarDatosMutuoHipotecario'),
    txtNumeroCredito:  document.getElementById('txtNumeroCredito'),
    cboTipoGarantia: document.getElementById('cboTipoGarantia'),
    init: function (id) {
        this.id = atob(decodeURIComponent(id));
        fetchActions.get({
            modulo: 'HIPOTECAS',
            archivo: 'hp_procesarHipoteca',
            params:{
                'idHipoteca': this.id,
                'accion': 'obtenerHipotecaEspecifica'
            }
        }).then(([datosHipoteca])=>{
           document.getElementById('cboAgencia').value =  datosHipoteca['idAgencia'];
           this.evalAgencia(datosHipoteca['idAgencia']).then(()=>{
               return new Promise((resolve, reject) => {
                   document.getElementById('cboAbogado').value =  datosHipoteca['idAbogado'];
                   document.getElementById('cboEjecutivo').value =  datosHipoteca['idEjecutivo'];
                   resolve();
               }).then(()=>{
                   document.getElementById('txtCodigoCliente').value =  datosHipoteca['codigoCliente'];
                   document.getElementById('txtNumeroCredito').value =  datosHipoteca['numeroCredito'];
                   document.getElementById('txtNombreDeudor').value =  datosHipoteca['nombreCliente'];
                   document.getElementById('txtApellidosDeudor').value =  datosHipoteca['apellidosCliente'];
                   document.getElementById('cboEstadoHipoteca').value =  datosHipoteca['idEstadoHipoteca'];
                   document.getElementById('cboUbicacionHipoteca').value =  datosHipoteca['idUbicacionHipoteca'];
                   document.getElementById('cboTipoHipoteca').value =  datosHipoteca['idTipoHipoteca'];
                   document.getElementById('numPlazoHipoteca').value =  datosHipoteca['plazo'];
                   document.getElementById('numMontoHipoteca').value =  datosHipoteca['montoHipoteca'];
                   document.getElementById('numMontoCredito').value =  datosHipoteca['montoCredito'];
                   document.getElementById('numMontoHipotecaCancelada').value =  datosHipoteca['montoCancelado'];
                   document.getElementById('numPlazoModificaciones').value =  datosHipoteca['plazoModificaciones'];
                   document.getElementById('numMontoModificaciones').value =  datosHipoteca['montoModificaciones'];
                   document.getElementById('txtSyric').value =  datosHipoteca['syric'];
                   document.getElementById('dtpFechaOtorgamiento').value =  datosHipoteca['fechaOtorgamiento'];
                   document.getElementById('dtpFechaInscripcion').value =  datosHipoteca['fechaInscripcion'];
                   document.getElementById('dtpFechaCancelacion').value =  datosHipoteca['fechaCancelacion'];
                   configModificacion.init(datosHipoteca['modificaciones']);
                   configGarantia.init(datosHipoteca['garantias']);
                   configMatricula.init(datosHipoteca['matriculas']);
                   configObservacion.init(datosHipoteca['observaciones']);
                   let todosCampos = document.querySelectorAll("#frmDatosGarantia .form-control");
                   todosCampos = [...todosCampos, ...document.querySelectorAll("#frmDatosGarantia .form-select")];

                   todosCampos.forEach(campo => {
                       if (campo.id) {
                           if (campo.type == 'select-one') {
                               $("#" + campo.id).data("valorInicial", campo.options[campo.options.selectedIndex].text);
                           } else {
                               $("#" + campo.id).data("valorInicial", campo.value);
                           }

                           if (campo.className.includes("selectpicker")) {
                               $("#" + campo.id).selectpicker("refresh");
                           }
                       }
                   });
               })

           });

        }).catch(generalMostrarError);

    },
    save: function () {

        let respuesta = [], validador = 0;
        if (formActions.manualValidate('frmDatosGarantia')) {
            switch (parseInt(this.cboTipoGarantia.value)){
                case 1:
                    if (Object.keys(configGarantia.garantias).length != 0) {
                        if (Object.keys(configMatricula.matriculas).length == 0) {
                            $("#matriculas-tab").trigger("click");
                            toastr.warning("Favor ingresar al menos una matrícula");
                            validador = 1;
                        }
                    } else {
                        $("#garantia-tab").trigger("click");
                        toastr.warning("Favor Ingresar al menos garantía", "Atención");
                        validador = 1;
                    }
                    break;
                case 2 :
                    if (Object.keys(configPrenda.prendas).length != 0){
                        $("#prendas-tab").trigger("click");
                        toastr.warning("Favor Ingresar al menos una prenda", "Atención");
                        validador = 1;
                    }
                    break;
                case 3:
                    if (Object.keys(configGarantia.garantias).length != 0) {
                        if (Object.keys(configMatricula.matriculas).length == 0) {
                            $("#matriculas-tab").trigger("click");
                            toastr.warning("Favor ingresar al menos una matrícula");
                            validador = 1;
                        }else {
                            $("#prendas-tab").trigger("click");
                            toastr.warning("Favor Ingresar al menos una prenda", "Atención");
                            validador = 1;
                        }
                    } else {
                        $("#garantia-tab").trigger("click");
                        toastr.warning("Favor Ingresar al menos garantía", "Atención");
                        validador = 1;
                    }
                    break;
                default:
                    break;
            }


            if (validador == 0){
                let datosHipoteca = formActions.getJSON('frmDatosGarantia'),
                    camposRequeridos = document.querySelectorAll('#frmDatosGarantia .form-control');
                camposRequeridos = [...camposRequeridos, ...document.querySelectorAll("#frmDatosGarantia .form-select")];

                const {contadorEditados, camposEditados} = hp_generarStringCambios(camposRequeridos);

                let datos = {
                    idHipoteca : this.id,
                    datosHipoteca: datosHipoteca,
                    datosModificaciones: {...configModificacion.modificaciones},
                    datosGarantias: {...configGarantia.garantias},
                    datosMatriculas: {...configMatricula.matriculas},
                    datosObservaciones: {...configObservacion.observaciones},
                    accion: 'guardarHipoteca',
                    idApartado,
                    tipoApartado,
                    camposEditados: camposEditados
                };
                fetchActions.set({
                    modulo: 'HIPOTECAS',
                    archivo: 'hp_procesarHipoteca',
                    datos
                }).then((resultado) => {
                    hp_generalmostrarAlertas(resultado);
                }).catch(hp_generalmostrarAlertas);

            }else {
                $("#datosHipoteca-tab").trigger("click");
                console.log(Object.keys(configGarantia.garantias).length);
                toastr.warning("Favor Complete los datos de la hipoteca", "Atención");
            }

        } else {
            $("#datosHipoteca-tab").trigger("click");
            console.log(Object.keys(configGarantia.garantias).length);
            toastr.warning("Favor Complete los datos de la hipoteca", "Atención");
        }
    },
    buscarDatosMutuoHipotecario: function () {
        let numeroCredito = document.getElementById('txtNumeroCredito').value;
        if (numeroCredito.length != 0) {
            fetchActions.get({
                modulo: 'HIPOTECAS',
                archivo: 'hp_obtenerDatosMutuos',
                params: {
                    numeroCredito: numeroCredito
                }
            }).then((resultado) => {
                if (resultado.respuesta == 'NO_DATOS_SGA' || resultado.respuesta == 'SESION') {
                    hp_generalmostrarAlertas(resultado);
                } else {
                    this.btnBuscarDatosMutuoHipotecario.disabled = true;
                    this.txtNumeroCredito.disabled = true;
                    document.getElementById('txtCodigoCliente').value  = resultado.codigoCliente;
                    document.getElementById('txtNombreDeudor').value =  resultado.nombres;
                    document.getElementById('txtApellidosDeudor').value =  resultado.apellidos;
                    document.getElementById('cboAgencia').value = resultado.idAgencia;
                    $("#cboAgencia").trigger('onchange');
                    document.getElementById('numPlazoHipoteca').value =  resultado.plazoHipoteca;
                    document.getElementById('numMontoCredito').value =  resultado.montoCredito;
                    document.getElementById('cboTipoGarantia').value = 1; // asigamos la garantia hipotecaria
                    $("#cboTipoGarantia").trigger('onchange'); // forzamos para que nos habilite las pestañas

                    console.log(resultado);

                    let tmp = {
                        id: 1,
                        observacion: resultado.descripcion,
                        usuarioObservo: resultado.nombreObservo
                    }
                    configObservacion.observaciones[1] = {
                        ...tmp
                    };
                    configObservacion.addRowTbl({nFila: 1, ...tmp}).then(() => {
                        tblObservacionesHipoteca.columns.adjust().draw();
                    })

                }
            }).catch(generalMostrarError);

        } else {
            toastr.warning("Ingrese un número de crédito para realizar la búsqueda", "Atención");
        }
    },
    evalAgencia: function (idAgencia) {
        return new Promise((resolve,reject)=>{
            fetchActions.get({
                modulo: "HIPOTECAS",
                archivo: 'hp_procesarAbogado',
                params: {
                    'accion': 'obtenerAbogadoAgenciaAsignada',
                    'idAgencia': idAgencia
                }
            }).then((abogados) => {
                console.log("abogados", abogados);
                let label = abogados.length > 0 ? 'seleccione' : 'seleccione una agencia',
                    opciones = [`<option selected value="" disabled>${label}</option>`],
                    cbos = document.querySelectorAll("select.cboAbogado");

                abogados.forEach(abogado => opciones.push(`<option value="${abogado.id}">${abogado.nombresAbogado}</option>`));

                cbos.forEach(cbo => {
                    cbo.innerHTML = opciones.join("");
                    if (cbo.className.includes("selectpicker")) {
                        $("#" + cbo.id).selectpicker("refresh");
                    }
                });
                fetchActions.get({
                    modulo: "HIPOTECAS",
                    archivo: 'hp_procesarEjecutivo',
                    params: {
                        'accion': 'obtenerAgenciaAsignada',
                        'idAgencia': idAgencia
                    }
                }).then((ejecutivos) => {
                    console.log("eject",ejecutivos);
                    let label = Object.keys(ejecutivos).length > 0 ? 'seleccione' : 'seleccione un agencia',
                        opciones = [`<option value="" selected disabled>${label}</option>`],
                        cbos = document.querySelectorAll("select.cboEjecutivo");

                    for (let key in ejecutivos) {
                        opciones.push(`<option value="${ejecutivos[key].id}">${ejecutivos[key].nombres} ${ejecutivos[key].apellidos}</option>`);
                    }

                    cbos.forEach(cbo => {
                        cbo.innerHTML = opciones.join("");
                        if (cbo.className.includes("selectpicker")) {
                            $("#" + cbo.id).selectpicker("refresh");
                        }
                    });
                    resolve();
                }).catch(generalMostrarError);
            }).catch(reject);
        });
    },
    evalEstado: function (idEstado) {
        console.log(idEstado);
        switch (idEstado) {
            case '1':
                document.getElementById('numMontoHipotecaCancelada').value = "";
                document.getElementById('dtpFechaCancelacion').value = "";
                document.getElementById('numMontoHipotecaCancelada').disabled = true;
                document.getElementById('dtpFechaCancelacion').readonly= true;
                document.getElementById('dtpFechaInscripcion').required =  false;
                document.getElementById('dtpFechaInscripcion').disabled =  true;
                document.getElementById('dtpFechaCancelacion').required = false
                document.getElementById('dtpFechaCancelacion').disabled = true;
                break;
            case '2':
                document.getElementById('numMontoHipotecaCancelada').value = "";
                document.getElementById('dtpFechaCancelacion').value = "";
                document.getElementById('numMontoHipotecaCancelada').disabled = true;
                document.getElementById('dtpFechaInscripcion').required = true;
                document.getElementById('dtpFechaInscripcion').disabled =  false;
                document.getElementById('dtpFechaCancelacion').required = false;
                document.getElementById('dtpFechaCancelacion').disabled = true;

                break;
            case '3':
                document.getElementById('numMontoHipotecaCancelada').value = "";
                document.getElementById('dtpFechaCancelacion').value = "";
                document.getElementById('dtpFechaInscripcion').required = false;
                document.getElementById('dtpFechaInscripcion').disabled =  true;
                document.getElementById('dtpFechaCancelacion').required =  true;
                document.getElementById('dtpFechaCancelacion').disabled =  false;
                document.getElementById('numMontoHipotecaCancelada').disabled = false;
                break;
        }
    },
    evalTipoHipoteca : function (idTipo){
        switch (idTipo) {
            case '1':
                document.getElementById('numMontoCredito').value = "";
                document.getElementById('numMontoCredito').disabled = true;
                $("#numMontoCredito").removeClass("required");

                break;
            case '2':
                document.getElementById('numMontoCredito').value = "";
                document.getElementById('numMontoCredito').disabled = false;
                $("#numMontoCredito").addClass("required");
                break;
            case '3':
                break;
        }
    },
    evalTipoGarantia : function (idTipo){
        switch (parseInt(idTipo)){
            case 1:
                document.getElementById('modificaciones-tab').classList.remove("disabled");
                document.getElementById('matriculas-tab').classList.remove('disabled');
                document.getElementById('cboTipoHipoteca').disabled = false;
                document.getElementById('cboTipoHipoteca').required =  true;
                document.getElementById('prendas-tab').classList.add("disabled");

                break;
            case 2:
                document.getElementById('prendas-tab').classList.remove("disabled");
                document.getElementById('modificaciones-tab').classList.add("disabled");
                document.getElementById('matriculas-tab').classList.add('disabled');
                document.getElementById('cboTipoHipoteca').disabled = true;
                document.getElementById('cboTipoHipoteca').required =  false;
                break;
            case 3:
                document.getElementById('prendas-tab').classList.remove("disabled");
                document.getElementById('modificaciones-tab').classList.remove("disabled");
                document.getElementById('matriculas-tab').classList.remove('disabled');
                document.getElementById('cboTipoHipoteca').disabled = false;
                document.getElementById('cboTipoHipoteca').required =  true;
                break;
            default:
                console.log("llego al default perro");
                break;
        }
    },
    evalEstadoGarantia: function (idEstado){

    },
    evalPlazo: function () {
        let plazoTotal = 0;

        this.campoPlazo.value && (plazoTotal += parseFloat(this.campoPlazo.value));

        for (let key in configModificacion.modificaciones) {
            configModificacion.modificaciones[key].ampliacionPlazo.length > 0 && (plazoTotal += parseFloat(configModificacion.modificaciones[key].ampliacionPlazo));
        }

        this.campoPlazoModificaciones.value = plazoTotal;
    },
    evalMonto: function () {
        let montoTotal = 0;

        this.campoMonto.value && (montoTotal += parseFloat(this.campoMonto.value));

        for (let key in configModificacion.modificaciones) {
            configModificacion.modificaciones[key].ampliacionMonto.length > 0 && (montoTotal += parseFloat(configModificacion.modificaciones[key].ampliacionMonto));
        }

        this.campoMontoModificaciones.value = montoTotal.toFixed(2);
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Salir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                let urlModulo = encodeURIComponent(btoa('HIPOTECAS'));
                window.location.href = `?page=hp_administrarHipotecas&mod=${urlModulo}`;
            }
        });
    },
    getHipotecasAdministracion: function (){
        return new Promise( (resolve, reject) => {
            $(".preloader").fadeIn("fast");
            fetchActions.get({
                modulo:'HIPOTECAS',
                archivo: 'hp_procesarHipoteca',
                params: {
                    'accion': 'obtenerHipotecasAdministracion',
                }
            }).then((resultado)=>{
                this.datatableAdmHipoteca(resultado).then(resolve);
            }).catch(reject);
        });


    },
    datatableAdmHipoteca : function (datosHipotecas){
        return new Promise((resolve, reject) => {
            let nFila = 1;
            datosHipotecas.forEach(datos =>{
                administracionHipotecas.row.add([
                    nFila,
                    datos['nombreCliente'],
                    datos['nombreAgencia'],
                    datos['nombreUsuario'],
                    datos['nombresAbogado'],
                    datos['numeroCredito'],
                    datos['fechaOtorgamiento'],
                    datos['estadoHipoteca'],
                    datos['matriculas'],
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configHipoteca.redirEditHipoteca(" + datos['idHipoteca'] + ") ' >" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configHipoteca.delete(" + datos['idHipoteca'] + ","+nFila+")'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trAdministracionHipoteca" + nFila;
                nFila ++;

            });
            administracionHipotecas.columns.adjust().draw();
            resolve();
        });
    },
    delete: function (id, nfila){

        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información ingresada.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO'
        }).then((result) => {
            if (result.isConfirmed) {
                let datos = {
                    accion: 'eliminarHipoteca',
                    idHipoteca: id,
                    idApartado,
                    tipoApartado,
                };
                fetchActions.set({
                    modulo: 'HIPOTECAS',
                    archivo: 'hp_procesarHipoteca',
                    datos
                }).then((respuesta)=>{
                  if (respuesta.respuesta = 'EXITO'){
                      toastr.success("Registro eliminado exitosamente","Proceso Completado");
                      administracionHipotecas.row('#trAdministracionHipoteca' + nfila).remove().draw();
                  }else {
                      toastr.warning("Intente Nuevamente", "Ocurrió un error");
                      console.warn(respuesta);
                  }


                }).catch((respuesta)=>{
                    generalMostrarError(respuesta);
                });


            }
        });
    },
    redirEditHipoteca :function (id){
        let encript = encodeURIComponent(btoa(id));
        window.location.href = "?page=hp_procesarGarantia&mod=SElQT1RFQ0FT&id=" + encript+"&acc=A";
    }

}

const configModificacion = {
    id: 0,
    cnt: 0,
    modificaciones: {},
    campoFechaHora: document.getElementById("dtpFechaHoraModificacion"),
    campoAbogado: document.getElementById("txtNombreAbogadoModificacion"),
    campoAsiento: document.getElementById("txtAsientoRegistro"),
    campoModificacionMonto: document.getElementById("txtAmpliacionMonto"),
    campoModificacionPlazo: document.getElementById("txtAmpliacionPlazo"),
    init: function (datos) {
        if (datos != null){
            let modificaciones = datos.split("%%%");
            modificaciones.forEach(algo => {
                let detalles = algo.split("@@@");
                this.id = detalles[0];
                let tmp = {
                    id : detalles[0],
                    fechaHora: detalles[1],
                    nombreAbogado : detalles[2],
                    asientoRegistro : detalles[3],
                    ampliacionMonto : detalles[4],
                    ampliacionPlazo : detalles[5]
                };
                this.modificaciones[this.id] = {
                    ...tmp
                };
                this.addRowTbl({ ...tmp,  nFila: this.id});
                tblModificaciones.columns.adjust().draw();

                configHipoteca.evalMonto();
                configHipoteca.evalPlazo();
            })
        }
    },
    edit: function (id = 0) {
        formActions.clean('frmModificacion').then(() => {
            this.id = id;

            if (id > 0) {
                let tmp = {
                    ...this.modificaciones[id]
                };
                this.campoFechaHora.value = tmp.fechaHora;
                $("#" + this.campoFechaHora.id).datetimepicker("update", tmp.fechaHora);
                this.campoAbogado.value = tmp.nombreAbogado;
                this.campoAsiento.value = tmp.asientoRegistro;
                this.campoModificacionMonto.value = tmp.ampliacionMonto;
                this.campoModificacionPlazo.value = tmp.ampliacionPlazo;
            }

            $("#mdlModificacionHipoteca").modal("show");
        })
    },
    save: function () {
        formActions.validate('frmModificacion').then(({
                                                          errores
                                                      }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? (this.modificaciones[this.id] ? this.modificaciones[this.id].id : 0) : 0,
                    fechaHora: this.campoFechaHora.value,
                    nombreAbogado: this.campoAbogado.value,
                    asientoRegistro: this.campoAsiento.value,
                    ampliacionMonto: this.campoModificacionMonto.value,
                    ampliacionPlazo: this.campoModificacionPlazo.value
                };

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        let numberAmpliacion = tmp.ampliacionMonto.length > 0 ? '$' + parseFloat(tmp.ampliacionMonto).toFixed(2) : 'Sin ampliación';
                        $("#trModificacion" + this.id).find("td:eq(1)").html(tmp.fechaHora);
                        $("#trModificacion" + this.id).find("td:eq(2)").html(tmp.nombreAbogado);
                        $("#trModificacion" + this.id).find("td:eq(3)").html(tmp.asientoRegistro);
                        $("#trModificacion" + this.id).find("td:eq(4)").html(numberAmpliacion);
                        $("#trModificacion" + this.id).find("td:eq(5)").html(tmp.ampliacionPlazo);
                        resolve();
                    }
                });

                guardar.then(() => {
                    tblModificaciones.columns.adjust().draw();
                    this.modificaciones[this.id] = {
                        ...tmp
                    };
                    configHipoteca.evalMonto();
                    configHipoteca.evalPlazo();
                    $("#mdlModificacionHipoteca").modal("hide");
                });
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
                             nFila,
                             fechaHora,
                             nombreAbogado,
                             asientoRegistro,
                             ampliacionMonto,
                             ampliacionPlazo
                         }) {
        return new Promise((resolve, reject) => {
            try {
                let numberAmpliacion = ampliacionMonto.length > 0 ? '$' + parseFloat(ampliacionMonto).toFixed(2) : 'Sin ampliación';
                tblModificaciones.row.add([
                    nFila,
                    fechaHora,
                    nombreAbogado,
                    asientoRegistro,
                    numberAmpliacion,
                    ampliacionPlazo,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' data-toggle='tooltip' onclick='configModificacion.edit(" + nFila + ")'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' data-toggle='tooltip' onclick='configModificacion.delete(" + nFila + ")'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trModificacion" + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información ingresada.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO'
        }).then((result) => {
            if (result.isConfirmed) {
                tblModificaciones.row('#trModificacion' + id).remove().draw();
                delete this.modificaciones[id];
                configHipoteca.evalMonto();
                configHipoteca.evalPlazo();
            }
        });
    },
}

const configGarantia = {
    id: 0,
    cnt: 0,
    garantias: {},
    cboGarantia: document.getElementById('cboGarantiaHipoteca'),
    init: function (datosGatantias) {
        if (datosGatantias != null){
            let garantias = datosGatantias.split("%%%");
            garantias.forEach(garantia => {
                let detalles = garantia.split("@@@");
                this.id = detalles[0];
                let tmp = {
                    id: detalles[0],
                    idGarantiaHipoteca: detalles[1],
                    nombreGarantiaHipoteca: detalles[2]
                };
                this.garantias[this.id] = {
                    ...tmp
                };
                this.addRowTbl({
                    ...tmp,
                    nFila:  this.id
                }).then(()=>{

                    tblGarantiasHipoteca.columns.adjust().draw();
                }).catch(generalMostrarError);
            })
        }
    },
    edit: function (id = 0) {
        formActions.clean('frmGarantiaHipoteca').then(() => {
            this.id = 0;

            if (this.id > 0) {
                this.cboGarantia.value = this.garantias[this.id].idGarantiaHipoteca;
            }

            $("#mdlGarantiaHipoteca").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmGarantiaHipoteca').then(({
                                                              errores
                                                          }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? (this.garantias[this.id] ? this.garantias[this.id].id : 0) : 0,
                    idGarantiaHipoteca: this.cboGarantia.value,
                    nombreGarantiaHipoteca: this.cboGarantia.options[this.cboGarantia.selectedIndex].text
                }
                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trGarantiaHipoteca" + this.id).find("td:eq(1)").html(tmp.nombreGarantiaHipoteca);
                        resolve();
                    }
                });

                guardar.then(() => {
                    this.garantias[this.id] = {
                        ...tmp
                    };
                    tblGarantiasHipoteca.columns.adjust().draw();
                    $("#mdlGarantiaHipoteca").modal('hide');
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
                             nFila,
                             nombreGarantiaHipoteca
                         }) {
        return new Promise((resolve, reject) => {
            try {
                tblGarantiasHipoteca.row.add([
                    nFila,
                    nombreGarantiaHipoteca,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Eliminar' data-toggle='tooltip' onclick='configGarantia.delete(" + nFila + ")'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trGarantiaHipoteca" + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información ingresada.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO'
        }).then((result) => {
            if (result.isConfirmed) {
                tblGarantiasHipoteca.row('#trGarantiaHipoteca' + id).remove().draw();
                delete this.garantias[id];
            }
        });
    },
}

const configMatricula = {
    id: 0,
    cnt: 0,
    matriculas: {},
    campoMatricula: document.getElementById("txtNumeroMatricula"),
    init: function (datosMatricula) {
        if (datosMatricula != null){
            let matriculas = datosMatricula.split("%%%");
            matriculas.forEach(matricula =>{
               let detalle =  matricula.split("@@@");
               this.id =  detalle[0];
               let tmp = {
                   id : detalle[0],
                   numeroMatricula: detalle[1]
               };
                this.matriculas[this.id] = {
                    ...tmp
                };
                this.addRowTbl({
                    ...tmp,
                    nFila: this.id
                }).then(()=>{
                    tblMatriculasHipoteca.columns.adjust().draw();
                }).catch(generalMostrarError);
            });
        }
    },
    edit: function (id = 0) {
        formActions.clean('frmMatriculaHipoteca').then(() => {
            this.id = id;

            if (this.id > 0) {
                this.campoMatricula.value = this.matriculas[this.id].numeroMatricula;
            }

            $("#mdlMatriculasHipoteca").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmMatriculaHipoteca').then(({
                                                               errores
                                                           }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? (this.matriculas[this.id] ? this.matriculas[this.id].id : 0) : 0,
                    numeroMatricula: this.campoMatricula.value
                }

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trMatriculaHipoteca" + this.id).find("td:eq(1)").html(tmp.numeroMatricula);
                        resolve();
                    }
                });

                guardar.then(() => {
                    this.matriculas[this.id] = {
                        ...tmp
                    };
                    tblMatriculasHipoteca.columns.adjust().draw();
                    $("#mdlMatriculasHipoteca").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
                             nFila,
                             numeroMatricula
                         }) {
        return new Promise((resolve, reject) => {
            try {
                tblMatriculasHipoteca.row.add([
                    nFila,
                    numeroMatricula,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configMatricula.edit(" + nFila + ")'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configMatricula.delete(" + nFila + ")'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trMatriculaHipoteca" + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información ingresada.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO'
        }).then((result) => {
            if (result.isConfirmed) {
                tblMatriculasHipoteca.row('#trMatriculaHipoteca' + id).remove().draw();
                delete this.matriculas[id];
            }
        });
    },
    validarFormatoMatricula: function (matricula) {

        if (matricula.length == 9) {
            matricula += "-";
            document.getElementById('txtNumeroMatricula').value = matricula;
        }
    }
}

const configPrenda = {
    id: 0,
    cnt : 0,
    prendas: {},
    txtDetallePrenda: document.getElementById('txtDetallePrenda'),

    init : function (datosPrendas){

    },
    edit: function (id = 0){
        formActions.clean('frmDetallePrenda').then(() => {
            this.id = id;

            if (this.id > 0) {
                this.txtDetallePrenda.value = this.prendas[this.id].detallePrenda;
            }

            $("#mdlPrenda").modal("show");
        });
    },
    save : function (){
        formActions.validate('frmDetallePrenda').then(({errores}) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? (this.prendas[this.id] ? this.prendas[this.id].id : 0) : 0,
                    detallePrenda: this.txtDetallePrenda.value
                }

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trPrenda" + this.id).find("td:eq(1)").html(tmp.detallePrenda);
                        resolve();
                    }
                });

                guardar.then(() => {
                    this.prendas[this.id] = {
                        ...tmp
                    };
                    tblPrendas.columns.adjust().draw();
                    $("#mdlPrenda").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({nFila, detallePrenda}) {
        return new Promise((resolve, reject) => {
            try {
                tblPrendas.row.add([
                    nFila,
                    detallePrenda,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configPrenda.edit(" + nFila + ")'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configPrenda.delete(" + nFila + ")'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trPrenda" + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete : function (id = 0) {
        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información ingresada.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO'
        }).then((result) => {
            if (result.isConfirmed) {
                tblPrendas.row('#trPrenda' + id).remove().draw();
                delete this.matriculas[id];
            }
        });
    }

}

const configObservacion = {
    id: 0,
    cnt: 0,
    observaciones: {},
    campoObservacion: document.getElementById("txtObservacionHipoteca"),
    init: function (datosObservaciones) {
        if (datosObservaciones != null) {
            let observaciones = datosObservaciones.split("%%%");

            observaciones.forEach(observacion => {
                let detalles = observacion.split("@@@");
                this.id = detalles[0];
                console.log(observacion);
                let tmp = {
                    id: detalles[0],
                    observacion: detalles[1],
                    usuarioObservo: detalles[2]
                }
                this.observaciones[this.id] = {
                    ...tmp
                };
                this.addRowTbl({nFila: this.id, ...tmp}).then(() => {
                    tblObservacionesHipoteca.columns.adjust().draw();
                })
            });
        }


    },
    edit: function (id = 0) {
        formActions.clean('frmObservacion').then(() => {
            this.id = id;

            if (this.id > 0) {
                this.campoObservacion.value = this.observaciones[this.id].observacion;
            }

            $("#mdlObservacionesHipoteca").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmObservacion').then(({
                                                         errores
                                                     }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? (this.observaciones[this.id] ? this.observaciones[this.id].id : 0) : 0,
                    observacion: this.campoObservacion.value,
                    usuarioObservo: document.getElementById('spnNombreUsuario').innerHTML
                }

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trObservacion" + this.id).find("td:eq(1)").html(tmp.observacion);
                        resolve();
                    }
                });

                guardar.then(() => {
                    this.observaciones[this.id] = {
                        ...tmp
                    };
                    tblObservacionesHipoteca.columns.adjust().draw();
                    $("#mdlObservacionesHipoteca").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
                             nFila,
                             observacion,
                             usuarioObservo
                         }) {
        return new Promise((resolve, reject) => {
            try {
                tblObservacionesHipoteca.row.add([
                    nFila,
                    observacion,
                    usuarioObservo,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configObservacion.edit(" + nFila + ")'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configObservacion.delete(" + nFila + ")'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trObservacion" + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información ingresada.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO'
        }).then((result) => {
            if (result.isConfirmed) {
                tblObservacionesHipoteca.row('#trObservacion' + id).remove().draw();
                delete this.observaciones[id];
            }
        });
    },
}
