let tblAdministracionAbogados,
    tblAsignacionAgenciaAbogados;
let hp_catalogoAgencias,
    hp_cantidadAgencias = 0,
    FechaFin = false;
var arregloAbogado = new FormData();


window.onload = () => {
    hp_initializeDatatable();
    fetchActions.get({
        modulo: "HIPOTECAS",
        archivo: 'hp_procesarAbogado',
        params: {
            'accion': 'obtenerAbogadoGeneral'
        }
    }).then(hp_construirDatatableAbogados).catch(generalMostrarError);
}

function hp_alertEliminarAbogado(pIdentificadorAbogado) {
    Swal.fire({
        title: "¿Estás seguro/a?",
        html: "No se podrá recuperar la información.",
        icon: "warning",
        showCloseButton: false,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
        cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            let datos = {
                identificadorAbogado: pIdentificadorAbogado,
                idApartado: localStorage.getItem('idApartado'),
                tipoApartado: localStorage.getItem('tipoApartado'),
                accion: 'eliminarAbogado'
            };

            fetchActions.set({
                modulo: "HIPOTECAS",
                archivo: 'hp_procesarAbogado',
                datos
            }).then(hp_generalmostrarAlertas).catch(generalMostrarError);
        }
    });
}

function hp_modalConfigurarAbogados(identificador) {
    (async () => {
        $(".preloader").fadeIn("fast");
        try {
            let datos = {
                identificador: identificador
            };
            let init = {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(datos)
            };
            let response = await fetch('./main/HIPOTECAS/code/obtenerInfoAbogado.php', init);
            if (response.ok) {
                let result = await response.json();
                switch (result) {
                    case 'EXITO':
                        const cargar = hp_llenarCboAgenciasAbogado();
                        document.getElementById("spnNombreAbogado").innerHTML = "Jaun Perez";
                        tblAsignacionAgenciaAbogados.columns.adjust().draw();
                        $("#mdlConfigurarAbogado").modal("show");
                        $(".preloader").fadeOut("fast")
                        break;
                    case 'ERROR':
                        hp_mostrarMensajeIntenteNuevamente();
                        break;
                    case 'SESION':
                        console.info("Se cerró tu sesion papu");
                        break;
                    default:
                        generalMostrarError(result);
                        break;
                }

            } else {
                throw new Error("Ta juerte el asunto");
            }
        } catch (error) {
            generalMostrarError("pperro");
        }
    })($(".preloader").fadeOut("fast"));

}

function hp_construirDatatableAbogados(datos) {
    tblAdministracionAbogados.clear().draw();
    let cantidadAbogados = 1;

    datos.forEach(abogado => {
        let fechaFinContrato = (abogado.finContrato == '01-01-2000' || abogado.finContrato == null) ? "N/A" : abogado.finContrato;
        tblAdministracionAbogados.row.add([
            cantidadAbogados,
            abogado.nombresAbogado + " " + abogado.apellidosAbogado,
            abogado.labelAgenciasAsignadas,
            abogado.estadoAbogado,
            abogado.inicioContrato,
            fechaFinContrato,
            "<div class='tblButtonContainer' align='center'>" +
            "<span class='btnTbl' title='Editar Datos' onclick=' hp_redirEditAbogado(" + abogado.idAbogado + ");'>" +
            "<i class='fas fa-user-edit'></i>" +
            "</span>" +
            "<span class='btnTbl' title='Eliminar Abogado' onclick=' hp_alertEliminarAbogado(" + abogado.idAbogado + ");'>" +
            "<i class='fas fa-trash'></i>" +
            "</div>"
        ]).node().id = "filaAdmAbogados" + cantidadAbogados;
        tblAdministracionAbogados.draw(false);
        tblAdministracionAbogados.page('last').draw('page');
        tblAdministracionAbogados.columns.adjust().draw();
        cantidadAbogados++;
        $("[data-toggle='tooltip']").tooltip();
        $(".preloader").fadeOut("fast");
    });
}

function hp_redirEditAbogado(identificador) {
    let encript = encodeURIComponent(btoa(identificador));
    window.location.href = "?page=hp_abogado&mod=SElQT1RFQ0FT&id=" + encript+"&acc=A";
}

function hp_initializeDatatable() {
    let alturaTabla = 300;
    if ($("#tblAdministracionAbogados").length) {
        tblAdministracionAbogados = $("#tblAdministracionAbogados").DataTable({
            drawCallBack: function () {
                $("[data-toggle='tooltip']").tooltip("dispose");
                $("[data-toggle='tooltip']").tooltip();
            },
            dateFormat: 'uk',
            order: [0, "asc"],
            columnDefs: [{
                    "targets": [0],
                    "orderable": false,
                },
                {
                    "targets": [1,2,3, 4],
                    "className": "text-start"
                }
            ],
            sortable: true,
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Datos de abogados';
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });

        tblAdministracionAbogados.columns.adjust().draw();
    }
    if ($("#tblAsignacionAgenciaAbogados").length) {
        tblAsignacionAgenciaAbogados = $("#tblAsignacionAgenciaAbogados").DataTable({
            drawCallBack: function () {
                $("[data-toggle='tooltip']").tooltip("dispose");
                $("[data-toggle='tooltip']").tooltip();
            },
            dateFormat: 'uk',
            scrollY: 200,
            sortable: true,
        });
        tblAsignacionAgenciaAbogados.columns.adjust().draw();
    }
    return;
}

function hp_estadoAbogado() {
    let datos = [{
        identificador: 1,
        estado: "En función"
    }, {
        identificador: 2,
        estado: "Contrato finalizado"
    }];

    return datos;
}