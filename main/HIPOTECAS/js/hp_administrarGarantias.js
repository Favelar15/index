let hp_tblAdministracionGarantias;

window.onload = () => {
    hp_initializeDatatable();
    fetchActions.get({
        modulo: "HIPOTECAS",
        archivo: "hp_procesarGarantia",
        params: {
            accion: 'obtenerGarantiasGeneral'
        }
    }).then((datosRespuesta) => {
        configTipoGarantia.init(datosRespuesta);
    }).catch(generalMostrarError);
};

const configTipoGarantia = {
    id: 0,
    cnt: 0,
    tiposGarantia: {},
    campoTipoGarantia: document.getElementById("txtNombreGarantia"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(tipo => {
                    this.cnt++;
                    this.tiposGarantia[this.cnt] = {
                        ...tipo
                    };
                    this.addRowTbl({
                        ...tipo,
                        nFila: this.cnt
                    });
                });
                hp_tblAdministracionGarantias.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmTipoGarantia').then(() => {
            this.id = id;

            if (this.id > 0) {
                this.campoTipoGarantia.value = this.tiposGarantia[id].tipoGarantia;
            }

            $("#mdlAdmGarantia").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmTipoGarantia').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.tiposGarantia[this.id].id : 0,
                    tipoGarantia: this.campoTipoGarantia.value
                };
                let logsCambios = '';

                let guardar = new Promise((resolve, reject) => {
                    fetchActions.set({
                        modulo: "HIPOTECAS",
                        archivo: "hp_procesarGarantia",
                        datos: {
                            ...tmp,
                            idApartado,
                            tipoApartado,
                            accion: 'guardarGarantia',
                            logsCambios
                        }
                    }).then(hp_generalmostrarAlertas).catch(reject);
                });

                guardar.then(() => {
                    this.tiposGarantia[this.id] = {
                        ...tmp
                    };
                    hp_tblAdministracionGarantias.columns.adjust().draw();
                    $("#mdlAdmGarantia").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        tipoGarantia
    }) {
        return new Promise((resolve, reject) => {
            try {
                hp_tblAdministracionGarantias.row.add([
                    nFila,
                    tipoGarantia,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar tipo garantia' onclick='configTipoGarantia.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>&nbsp;&nbsp;" +
                    "<span class='btnTbl' title='Eliminar tipo garantía' onclick='configTipoGarantia.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trTipoGarantia" + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO'
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: 'HIPOTECAS',
                    archivo: "hp_procesarGarantia",
                    datos: {
                        id: this.tiposGarantia[id].id,
                        accion: 'eliminarTipoGarantia',
                        idApartado,
                        tipoApartado
                    }
                }).then(hp_generalmostrarAlertas).catch(generalMostrarError);
            }
        });
    },
}

function hp_initializeDatatable() {
    let alturaTabla = 350;
    if ($("#tblAdminstracionGarantias").length) {
        hp_tblAdministracionGarantias = $("#tblAdminstracionGarantias").DataTable({
            dateFormat: 'uk',
            order: [0, "asc"],
        });
        hp_tblAdministracionGarantias.columns.adjust().draw();
    }

    return;
}