var tblAdmReporteGeneralHipotecas = "";
window.onload = () => {
    $(".preloader").fadeOut("fast");

    const getCats = configReporteGeneralHipoteca.prepareDatatable().then(() => {
        fetchActions.getCats({
            modulo: "HIPOTECAS",
            archivo: "hp_getCats",
            solicitados: ['agenciasCbo', 'estadosHipoteca', 'ubicacionesHipoteca']
        }).then(({
                     agencias,
                     estadosHipoteca,
                     ubicacionesHipoteca
                 }) => {
            const opciones = [`<option selected value="all">Todos</option>`];
            let tmpOpciones = [...opciones];
            agencias.forEach(agencia => {
                tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`);
            });

            let cbosAgencias = document.querySelectorAll("select.cboAgencia");
            cbosAgencias.forEach(cbo => {
                cbo.innerHTML = tmpOpciones.join("");
            });

            tmpOpciones = [...opciones];
            estadosHipoteca.forEach(estado => {
                tmpOpciones.push(`<option value="${estado.id}">${estado.estadoHipoteca}</option>`);
            });

            let cbosEstados = document.querySelectorAll("select.cboEstadoHipoteca");
            cbosEstados.forEach(cbo => {
                cbo.innerHTML = tmpOpciones.join("");
            });

            tmpOpciones = [...opciones];
            ubicacionesHipoteca.forEach(ubicacion =>{
                tmpOpciones.push(`<option value="${ubicacion.id}">${ubicacion.ubicacion}</option>`)
            });
            let cboUbicaciones =  document.querySelectorAll('select.cboUbicacionHipoteca');
            cboUbicaciones.forEach(cbo=>{
                cbo.innerHTML =  tmpOpciones.join("");
            });

        });
    })
}

const configReporteGeneralHipoteca = {
    id: 0,
    cnt: 0,
    datos: {},
    cboAgencia: document.getElementById('cboAgencia'),
    cboAbogado: document.getElementById('cboAbogado'),
    cboEjecutivo: document.getElementById('cboEjecutivo'),
    cboEstadohipoteca: document.getElementById('cboEstadoHipoteca'),
    cboUbicacionHipoteca: document.getElementById('cboUbicacionHipoteca'),
    txtFechaInicio: document.getElementById('txtInicio'),
    txtFechaFin: document.getElementById('txtFin'),
    btnExcel: document.getElementById('btnExcel'),

    init: function (datosHipotecas) {
        console.log(datosHipotecas);
        return new Promise((resolve, reject) => {
            try {
                if (datosHipotecas.respuesta != 'NO_DATOS') {
                    configReporteGeneralHipoteca.datos = {};

                    tblAdmReporteGeneralHipotecas.clear().draw();
                    datosHipotecas.forEach(hipoteca => {
                        configReporteGeneralHipoteca.cnt++;
                        configReporteGeneralHipoteca.datos[configReporteGeneralHipoteca.cnt] = {
                            ...hipoteca
                        };
                        configReporteGeneralHipoteca.addRowTbl({
                            ...hipoteca,
                            nfila: configReporteGeneralHipoteca.cnt
                        })
                    });
                    tblAdmReporteGeneralHipotecas.columns.adjust().draw();
                    this.btnExcel.disabled = false;
                    resolve();
                } else {
                    toastr.info("No se encontraron datos para mostrar", "Información")
                    resolve();
                }

            } catch (error) {
                reject(error.message);
            }

        });

    },
    search: function () {
        formActions.validate('frmReporteGeneralHipoteca').then(({errores}) => {
            if (errores == 0) {
                fetchActions.get({
                    modulo: 'HIPOTECAS',
                    archivo: 'hp_procesarHipoteca',
                    params: {
                        'accion': 'obtenerHipotecasReporteGeneral',
                        idAgencia: this.cboAgencia.value,
                        idAbogado: this.cboAbogado.value,
                        idEjecutivo: this.cboEjecutivo.value,
                        idEstadoHipoteca: this.cboEstadohipoteca.value,
                        idUbicacionHipoteca : this.cboUbicacionHipoteca.value,
                        fechaInicio: this.txtFechaInicio.value,
                        fechaFin: this.txtFechaFin.value
                    }
                }).then(this.init).catch(generalMostrarError);
            }

        }).catch(generalMostrarError);
    },
    evalAgencia: function (idAgencia) {
        return new Promise((resolve, reject) => {
            fetchActions.get({
                modulo: "HIPOTECAS",
                archivo: 'hp_procesarAbogado',
                params: {
                    'accion': 'obtenerAbogadoAgenciaAsignada',
                    'idAgencia': idAgencia
                }
            }).then((abogados) => {

                let label = abogados.length > 0 ? 'Todos' : 'seleccione una agencia',
                    opciones = [`<option selected value="all" >${label}</option>`],
                    cbos = document.querySelectorAll("select.cboAbogado");

                abogados.forEach(abogado => opciones.push(`<option value="${abogado.id}">${abogado.nombresAbogado}</option>`));

                cbos.forEach(cbo => {
                    cbo.innerHTML = opciones.join("");
                    if (cbo.className.includes("selectpicker")) {
                        $("#" + cbo.id).selectpicker("refresh");
                    }
                });
                fetchActions.get({
                    modulo: "HIPOTECAS",
                    archivo: 'hp_procesarEjecutivo',
                    params: {
                        'accion': 'obtenerAgenciaAsignada',
                        'idAgencia': idAgencia
                    }
                }).then((ejecutivos) => {
                    let label = Object.keys(ejecutivos).length > 0 ? 'Todos' : 'seleccione una agencia',
                        opciones = [`<option value="all" selected >${label}</option>`],
                        cbos = document.querySelectorAll("select.cboEjecutivo");

                    for (let key in ejecutivos) {
                        opciones.push(`<option value="${ejecutivos[key].id}">${ejecutivos[key].nombres} ${ejecutivos[key].apellidos}</option>`);
                    }

                    cbos.forEach(cbo => {
                        cbo.innerHTML = opciones.join("");
                        if (cbo.className.includes("selectpicker")) {
                            $("#" + cbo.id).selectpicker("refresh");
                        }
                    });
                    resolve();
                }).catch(generalMostrarError);
            }).catch(reject);
        });

    },
    prepareDatatable: function () {
        return new Promise((resolve, reject) => {
            if ($("#tblReporteHipotecas").length) {
                tblAdmReporteGeneralHipotecas = $("#tblReporteHipotecas").DataTable({
                    dateFormat: 'uk',
                    order: [0, "asc"],
                });
                tblAdmReporteGeneralHipotecas.columns.adjust().draw();
            }
            resolve();
        });

    },
    generarExcel: function () {
        if (Object.keys(this.datos).length > 0) {
            fetchActions.set({
                modulo: 'HIPOTECAS',
                archivo: 'hp_generarReporteGeneralExcel',
                datos: {
                    datos: {
                        ...this.datos
                    },
                    fechaInicio: this.txtFechaInicio.value,
                    fechaFin: this.txtFechaFin.value
                }
            }).then((datosRespuesta) => {
                console.log(datosRespuesta);
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta.trim()) {
                        case 'EXITO':
                            window.open('./main/HIPOTECAS/docs/reportes/' + datosRespuesta.nombreArchivo, '_blank');
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            })
        } else {
            toastr.warning("No hay datos para Exportar", "Atención");
        }

    },
    addRowTbl: function ({
                             nfila,
                             nombreCliente,
                             nombreAgencia,
                             nombreUsuario,
                             nombresAbogado,
                             ubicacionHipoteca,
                             estadoHipoteca,
                             numeroCredito,
                             montoHipoteca,
                             matriculas
                         }) {
        return new Promise((resolve, reject) => {
            try {
                tblAdmReporteGeneralHipotecas.row.add([
                    nfila,
                    nombreCliente,
                    nombreAgencia,
                    nombreUsuario,
                    nombresAbogado,
                    ubicacionHipoteca,
                    estadoHipoteca,
                    numeroCredito,
                    montoHipoteca,
                    matriculas
                ]).node().id = "trReporteGeneralHipoteca" + nfila;
                resolve();
            } catch (error) {
                reject(error.message);
            }
        });

    }
}