var administracionHipotecas = '';
window.onload = () => {
    administrarHipoteca.init();

}

const administrarHipoteca = {
 init: function () {
     this.initializeDatatable();
     configHipoteca.getHipotecasAdministracion().then(()=>{
         $(".preloader").fadeOut("fast");
         }).catch(generalMostrarError);


 },
 initializeDatatable : function () {
     if ($("#tblAdministacionHipotecas").length) {
         administracionHipotecas = $("#tblAdministacionHipotecas").DataTable({
             dateFormat: 'uk',
             order: [0, "asc"],
         });
         administracionHipotecas.columns.adjust().draw();
     }
 }
};
