<link rel="stylesheet" href="./main/css/hipotecas.css">
<link rel="stylesheet" href="./main/css/hipotecas.css">
<div class="pagetitle">
    <h1>Administración de abogados</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Hipotecas</li>
            <li class="breadcrumb-item">Administración</li>
            <li class="breadcrumb-item active">Abogados</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="deudor-tab" data-bs-toggle="tab" data-bs-target="#divAdministracionAbogados" role="tab" aria-controls="deudor" aria-selected="true">
                Administración
            </button>
        </li>
        <li class="nav-item ms-auto">
            <button type="button" class="btn btn-outline-dark btn-sm" onclick="javascript: window.location.href = '?page=hp_abogado&mod=<?php echo base64_encode('HIPOTECAS'); ?>&acc=N'">
                <i class="fas fa-gavel"></i> Nuevo Abogado
            </button>
        </li>
    </ul>
    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="divAdministracionAbogados" role="tabpanel" aria-labelledby="fiadores-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblAdministracionAbogados" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Nombre Abogado</th>
                                <th>Agencias asignadas</th>
                                <th>Estado</th>
                                <th>Inicio de Contrato</th>
                                <th>Fin de contrato</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$_GET["js"] = ['hp_generales', 'hp_administracionAbogados'];
