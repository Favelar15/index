<?php $identificador = (isset($_GET['id']) && !empty($_GET['id'])) ? $_GET['id'] : 0;
$accion = $_GET['acc'] == 'N' ? 'Registrar' : 'Actualizacion de' ?>
<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1><?php echo $accion; ?> garantía</h1>
        </div>
        <div class="col-4 topBtnContainer">
            <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configHipoteca.cancel();">
                <i class="fas fa-times"></i> <span>Cancelar</span>
            </button>
            <button class="btn btn-sm btn-outline-success float-end" title="Guardar" type="button" onclick="configHipoteca.save()">
                <i class="fas fa-save"></i> <span>Guardar</span>
            </button>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Garantias</li>
            <li class="breadcrumb-item">Control de garantías</li>
            <li class="breadcrumb-item active">Configuración</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="datosGarantia-tab" data-bs-toggle="tab" data-bs-target="#datosGarantia" role="tab" aria-controls="hipoteca" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','mainButtons');">
                Datos Garantía
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="modificaciones-tab" data-bs-toggle="tab" data-bs-target="#modificaciones" role="tab" aria-controls="user" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','modificacionesButtons');">
                Modificaciones
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="productos-tab" data-bs-toggle="tab" data-bs-target="#productos" role="tab" aria-controls="user" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','productosButtons');">
                Productos
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="matriculas-tab" data-bs-toggle="tab" data-bs-target="#matriculas" role="tab" aria-controls="user" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','matriculasButttons');">
                Matriculas
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="prendas-tab" data-bs-toggle="tab" data-bs-target="#prendas" role="tab" aria-controls="user" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','prendasButttons');">
                Detalle de Prendas
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="observaciones-tab" data-bs-toggle="tab" data-bs-target="#observaciones" role="tab" aria-controls="user" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','observacionesButtons');">
                Observaciones
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="mainButtons"></div>
            <div class="tabButtonContainer" id="modificacionesButtons" style="display: none;">
                <button type="button" class="btn btn-outline-primary btn-sm" onclick="configModificacion.edit()">
                    <i class="fas fa-plus "></i> Agregar modificación
                </button>
            </div>
            <div class="tabButtonContainer" id="productosButtons" style="display: none;">
                <button type="button" class="btn btn-outline-primary btn-sm" onclick="configGarantia.edit();">
                    <i class="fas fa-plus "></i> Agregar Producto
                </button>
            </div>
            <div class="tabButtonContainer" id="matriculasButttons" style="display: none;">
                <button type="button" class="btn btn-outline-primary btn-sm" onclick="configMatricula.edit()">
                    <i class="fas fa-plus "></i> Agregar matricula
                </button>
            </div>
            <div class="tabButtonContainer" id="prendasButttons" style="display: none;">
                <button type="button" class="btn btn-outline-primary btn-sm" onclick="configPrenda.edit()">
                    <i class="fas fa-plus "></i> Agregar prenda
                </button>
            </div>
            <div class="tabButtonContainer" id="observacionesButtons" style="display: none;">
                <button type="button" class="btn btn-outline-primary btn-sm" onclick="configObservacion.edit()">
                    <i class="fas fa-plus "></i> Agregar observaciones
                </button>
            </div>
        </li>
    </ul>
    <div class="tab-content mt-2" id="contenedorGeneral">
        <div class="tab-pane fade show active" id="datosGarantia" role="tabpanel" aria-labelledby="datosGarantia-tab">
            <form id="frmDatosGarantia" name="frmDatosGarantia" method="POST" accept-charset="utf-8" class="needs-validation" novalidate action="javascript:void(0);">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="row">
                            <div class='col-lg-3 col-xl-3 cuenta-acacypac'>
                                <label for='txtNumeroCredito' class="form-label">Número de crédito <span class="requerido">*</span></label>
                                <div class="input-group has-validation">
                                    <input type='text' class='form-control cuentaAcacypac' id='txtNumeroCredito' name='txtNumeroCredito' value="" required onkeypress="return generalSoloNumeros(event);">
                                    <div class="input-group-append">
                                        <button type="button" title="Buscar datos" id="btnBuscarDatosMutuoHipotecario" onclick="configHipoteca.buscarDatosMutuoHipotecario()" class="input-group-text btn btn-outline-dark"><i class="fas fa-search"></i> </button>
                                    </div>
                                    <div class='invalid-feedback'>
                                        Ingrese el número de crédito
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtCodigoCliente">Código de cliente <span class="requerido">*</span>
                                </label>
                                <input type="text" id="txtCodigoCliente" name="txtCodigoCliente" onkeypress="return generalSoloNumeros(event)" class="form-control" maxlength="15" required value="">
                                <div class="invalid-feedback">
                                    Ingrese el código de cliente
                                </div>
                            </div>

                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtNombreDeudor">Nombres deudor <span class="requerido">*</span></label>
                                <input type="text" id="txtNombreDeudor" name="txtNombreDeudor" onkeypress="return generalSoloLetras(event)" class="form-control" required maxlength="150">
                                <div class="invalid-feedback">
                                    Ingrese los nombres
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtApellidosDeudor">Apellidos deudor <span class="requerido">*</span></label>
                                <input type="text" id="txtApellidosDeudor" name="txtApellidosDeudor" onkeypress="generalSoloLetras(event)" class="form-control" maxlength="150" required>
                                <div class="invalid-feedback">
                                    Ingrese los apellidos
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboAgencias">Agencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboAgencia" required id="cboAgencia" onchange="configHipoteca.evalAgencia(this.value)">
                                        <option value="">Seleccione</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione una agencia
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboAbogado">Abogado <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboAbogado" required id="cboAbogado" name="cboAbogado">
                                        <option value="" selected disabled>Seleccione Agencia</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione un abogado
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboEjecutivo">Ejecutivo Responsable <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboEjecutivo" required id="cboEjecutivo" name="cboEjecutivo">
                                        <option value="" selected disabled>Seleccione</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione a un ejecutivo
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboTipoGarantia">Tipo de Garantia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboTipoGarantia" required id="cboTipoGarantia" name="cboTipoGarantia" onchange="configHipoteca.evalTipoGarantia(this.value)">
                                        <option value="0">Seleccione</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione una opción
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboEstadoHipoteca">Estado de Garantía <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboEstadoHipoteca" required id="cboEstadoHipoteca" name="cboEstadoHipoteca" onchange="configHipoteca.evalEstado(this.value)">
                                        <option value="0">Seleccione</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione una opción
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboUbicacionHipoteca">Ubicación de Garantía <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboUbicacionHipoteca" required id="cboUbicacionHipoteca" name="cboUbicacionHipoteca">
                                        <option value="0">Seleccione</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione una opción
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboTipoHipoteca">Tipo de Hipoteca <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboTipoHipoteca"  id="cboTipoHipoteca" name="cboTipoHipoteca" onchange="configHipoteca.evalTipoHipoteca(this.value)" disabled>
                                        <option value="">Seleccione</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione una opción
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="numPlazoHipoteca">Plazo Garantía <span class="requerido">*</span></label>
                                <input type="text" id="numPlazoHipoteca" name="numPlazoHipoteca" class="form-control" onkeypress="return generalSoloNumeros(event)" onkeyup="configHipoteca.evalPlazo()" required maxlength="5">
                                <div class="invalid-feedback">
                                    Ingrese el plazo
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="numMontoHipoteca">Monto de garantia <span class="requerido">*</span></label>
                                <input type="text" id="numMontoHipoteca" name="numMontoHipoteca" class="form-control" onkeypress="return generalSoloNumeros(event)" onkeyup="configHipoteca.evalMonto()" maxlength="15" placeholder="USD" required>
                                <div class="invalid-feedback">
                                    Ingrese el monto de la garantia
                                </div>
                            </div>

                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="numMontoCredito">Monto de Crédito <span class="requerido">*</span></label>
                                <input type="text" id="numMontoCredito" name="numMontoCredito" class="form-control" onkeypress="return generalSoloNumeros(event)" maxlength="15" placeholder="USD" required>
                                <div class="invalid-feedback">
                                    Ingrese el monto del crédito
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="numMontoHipotecaCancelada">Monto cancelado <span class="requerido">*</span></label>
                                <input type="text" id="numMontoHipotecaCancelada" name="numMontoHipotecaCancelada" class="form-control" onkeypress="return generalSoloNumeros(event)" maxlength="15" placeholder="USD" required>
                                <div class="invalid-feedback">
                                    Ingrese el monto cancelado
                                </div>
                                <!-- <span class="spnError" id="spnHipotecaCancelada">*Campo requerido</span> -->
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="numPlazoModificaciones">Plazo con modificaciones </label>
                                <input type="text" id="numPlazoModificaciones" name="numPlazoModificaciones" class="form-control " readonly>
                                <div class="invalid-feedback">
                                    Ingrese el plazo en modificaciones
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="numMontoModificaciones">Monto hipoteca con modificaciones </label>
                                <input type="text" id="numMontoModificaciones" name="numMontoModificaciones" class="form-control" readonly>
                                <div class="invalid-feedback">
                                    Ingrese el monto de las hipotecas
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="txtSyric">SYRIC (CNR)</label>
                                <input type="text" id="txtSyric" name="txtSyric" class="form-control" onkeypress="return generaLetrasNumeros(event)" maxlength="250">
                                <div class="invalid-feedback">
                                    Ingrese el SYRIC
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="dtpFechaOtorgamiento">Fecha Otorgamiento</label>
                                <div class="input-group form-group has-validation">
                                    <input type="text" id="dtpFechaOtorgamiento" name="dtpFechaOtorgamiento" class="form-control fechaAbierta readonly"  placeholder="dd/mm/yyyy">
                                    <div class="invalid-feedback">
                                        Seleccione la fecha de otorgamiento
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="dtpFechaInscripcion">Fecha Inscripción</label>
                                <input type="text" id="dtpFechaInscripcion" name="dtpFechaInscripcion" class="form-control fechaAbierta readonly" placeholder="dd/mm/yyyy">
                                <div class="invalid-feedback">
                                    Seleccione la fecha de inscripción
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="dtpFechaCancelacion">Fecha Cancelación</label>
                                <input type="text" id="dtpFechaCancelacion" name="dtpFechaCancelacion" class="form-control fechaAbierta readonly"  placeholder="dd/mm/yyyy" required>
                                <div class="invalid-feedback">
                                    Seleccione la fecha de cancelación
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <div class="tab-pane fade" id="modificaciones" role="tabpanel" aria-labelledby="modificaciones-tab">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <table id="tblModificaciones" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Fecha Modificación</th>
                                <th>Abogado</th>
                                <th>Asiento</th>
                                <th>Ampliacion en monto</th>
                                <th>Ampliacion en plazo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php include 'modals/hp_modalModificaciones.php' ?>
        </div>
        <div class="tab-pane fade" id="productos" role="tabpanel" aria-labelledby="garantias-tab">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <table id="tblGarantiasHipoteca" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Producto</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php include 'modals/hp_modalGarantiaHipoteca.php' ?>
        </div>
        <div class="tab-pane fade" id="matriculas" role="tabpanel" aria-labelledby="matriculas-tab">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <table id="tblMatricula" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Matrícula</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <?php include 'modals/hp_modalMatriculasHipoteca.php' ?>
            </div>
        </div>
        <div class="tab-pane fade" id="prendas" role="tabpanel" aria-labelledby="prendas-tab">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <table id="tblPrendas" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Descripción de prenda</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <?php include 'modals/hp_modalPrenda.php' ?>
            </div>
        </div>
        <div class="tab-pane fade" id="observaciones" role="tabpanel" aria-labelledby="observaciones-tab">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <table id="tblObservaciones" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Observaciones</th>
                                <th>Usuario</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <?php include 'modals/hp_modalObservacionesHipoteca.php' ?>
            </div>
        </div>
    </div>
</section>
<?php
$_GET["js"] = ['hp_generales', 'hp_garantias'];
