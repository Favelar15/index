<link rel="stylesheet" href="./main/css/hipotecas.css">
<div class="pagetitle">
    <h1>Administración de tipos de hipotecas</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Hipotecas</li>
            <li class="breadcrumb-item">Administración</li>
            <li class="breadcrumb-item active">Tipos de hipoteca</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="tipos-tab" data-toggle="tab" data-bs-target="#divAdministracionTiposHipotecas" role="tab" aria-controls="tipos" aria-selected="true">
                Administración
            </button>
        </li>
        <li class="nav-item ms-auto">
            <button type="button" class="btn btn-outline-dark btn-sm" onclick="configTipoHipoteca.edit();">
                <i class="fas fa-plus-circle"></i> Nuevo tipo Hipoteca
            </button>
        </li>
    </ul>
    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="divAdministacionTipoHipotecas" role="tabpanel" aria-labelledby="tipos-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblAdministacionTipoHipotecas" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Tipo de hipoteca</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "modals/hp_modalConfigTipoHipoteca.php" ?>
<?php
$_GET["js"] = ['hp_generales', 'hp_administracionTiposHipotecas'];
