<div class="pagetitle">
    <h1>Reporte general de registro de Hipotecas</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">HIPOTECAS</li>
            <li class="breadcrumb-item">Reportería</li>
            <li class="breadcrumb-item active">General</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <form action="javascript:configReporteGeneralHipoteca.search();" id="frmReporteGeneralHipoteca" name="frmReporteGeneralHipoteca" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <div class="row">
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboAgencias">Agencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboAgencia" required id="cboAgencia" onchange="configReporteGeneralHipoteca.evalAgencia(this.value)">
                                        <option value="">Seleccione</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione una agencia
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboAbogado">Abogado <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboAbogado" required id="cboAbogado" name="cboAbogado">
                                        <option value="all" selected disabled>Todos</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione un abogado
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboEjecutivo">Ejecutivo Responsable <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboEjecutivo" required id="cboEjecutivo" name="cboEjecutivo">
                                        <option value="all" selected disabled>Todos</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione a un ejecutivo
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboEstadoHipoteca">Estado de Hipoteca <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboEstadoHipoteca" required id="cboEstadoHipoteca" name="cboEstadoHipoteca">
                                        <option value="all">Todos</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione una opción
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label class="form-label" for="cboEstadoHipoteca">Ubicación Hipoteca <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select class="form-select cboUbicacionHipoteca" required id="cboUbicacionHipoteca" name="cboUbicacionHipoteca">
                                        <option value="all">Todos</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Seleccione una opción
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for="txtInicio" class="form-label">Inicio <span class="requerido">*</span></label>
                                <input type="text" id="txtInicio" name="txtInicio" class="form-control fechaFinLimitado readonly" required>
                                <div class="invalid-feedback">
                                    Ingrese la fecha de inicio de búsqueda
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for="txtFin" class="form-label">Fin <span class="requerido">*</span></label>
                                <input type="text" id="txtFin" name="txtFin" class="form-control fechaFinLimitado readonly" required>
                                <div class="invalid-feedback">
                                    Ingrese la fecha de fin de búsqueda
                                </div>
                            </div>
                        </div>
                        <div class="col-lg- col-xl- mt-3" align="center">
                            <button type="submit" class="btn btn-sm btn-outline-primary">
                                <i class="fas fa-search"></i> Buscar
                            </button>
                            <button type="button" class="btn btn-sm btn-outline-success" id="btnExcel" disabled onclick="configReporteGeneralHipoteca.generarExcel();">
                                <i class="fas fa-file-excel"></i> Excel
                            </button>
                        </div>
                    </div>


                </div>
            </form>
            <hr class="mb-0 mt-2">
        </div>
        <div class="col-lg-12 col-xl-12 mt-3">
            <table id="tblReporteHipotecas" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                <thead>
                <tr>
                    <th>N°</th>
                    <th>Nombre cliente</th>
                    <th>Agencia</th>
                    <th>Usuario</th>
                    <th>Notario</th>
                    <th>Ubicación</th>
                    <th>Estado</th>
                    <th>N° Credito</th>
                    <th>Monto Hipoteca</th>
                    <th>Matrículas</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php
$_GET["js"] = ['hp_reportesGeneral'];
