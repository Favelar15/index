<link rel="stylesheet" href="./main/css/hipotecas.css">
<div class="pagetitle">
    <h1>Control de garantías</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Garantías</li>
            <li class="breadcrumb-item active">Control de Garantías</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">


<section class="section">
    <form id="frmAdministracionHipotecas" name="frmAdministracionHipotecas" method="POST" accept-charset="utf-8" action="javascript:void(0);">
        <div class="bloques" id="bloquePrincipal">
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="deudor-tab" data-bs-toggle="tab" data-bs-target="#divfrmAdministracionHipotecas" role="tab" aria-controls="deudor" aria-selected="true">
                        Administración
                    </button>
                </li>
                <li class="nav-item ms-auto">
                    <button type="button" class="btn  btn-outline-dark btn-sm " onclick="javascript: window.location.href = '?page=hp_procesarGarantia&mod=<?php echo base64_encode('HIPOTECAS'); ?>&acc=N'">
                        <i class="fas fa-plus-circle"></i> Nueva Garantía
                    </button>
                </li>
            </ul>
            <div class="tab-content" style="margin-top: 20px;">
                <div class="tab-pane fade show active" id="divfrmAdministracionHipotecas" role="tabpanel" aria-labelledby="fiadores-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblAdministacionHipotecas" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Nombre Cliente</th>
                                        <th>Agencia</th>
                                        <th>Usuario</th>
                                        <th>Notario</th>
                                        <th>N° Crédito</th>
                                        <th>Fecha otorgamiento</th>
                                        <th>Estado Hipoteca</th>
                                        <th>Matricula's</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "modals/hp_modalConfigTipoHipoteca.php" ?>
    </form>
</section>
<?php
$_GET["js"] = ['hp_generales', 'hp_garantias', 'hp_administracionGarantias'];
