<?php
?>
<div class="modal fade " tabindex="-1" id="mdlConfigAgenciaAbogado" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mdlTitulo">Agencias</span></h5>
            </div>
            <div class="modal-body">
                <form id="frmAgencia" name="frmAgencia" method="post" action="javascript:configAgencia.save();" accept-charset="utf-8" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-label" for="mdl_cboAgenciaAsignada">Seleccione una agencia <span class="requerido">*</span></label>
                            <select class="form-select" id="mdl_cboAgenciaAsignada" name="mdl_cboAgenciaAsignada" required>
                                <option value="0">Seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una agencia
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnGuardar" class="btn btn-sm btn-primary" form="frmAgencia">
                    Agregar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>