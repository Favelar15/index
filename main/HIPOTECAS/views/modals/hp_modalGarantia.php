<?php
?>
<div class="modal fade " tabindex="-1" id="mdlAdmGarantia" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span id="spnTitulo">Garantía</span></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="frmTipoGarantia" name="frmTipoGarantia" accept-charset="utf-8" method="POST" action="javascript:configTipoGarantia.save();" class="needs-validation" novalidate>
                        <div class="col-md-12 mayusculas">
                            <label class="form-label">Nombre Garantía <span class="requerido">*</span></label>
                            <input class="form-control" type="text" id="txtNombreGarantia" name="txtNombreGarantia" onkeypress="return generaLetrasNumeros(event)" maxlength="150" required>
                            <div class="invalid-feedback">
                                Ingrese el tipo de garantía
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-primary" form="frmTipoGarantia">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>