<?php
?>
<div class="modal fade " tabindex="-1" id="mdlAdmTipoHipoteca" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mdlTitulo"><span id="spnTitulo">Tipo de hipoteca </span></h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configTipoHipoteca.save()" id="frmTipoHipoteca" name="frmTipoHipoteca" accept-charset="utf-8" method="post" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-label" for="txtTipoHipoteca">Tipo de hipoteca</label>
                            <input class="form-control" type="text" id="txtTipoHipoteca" name="txtTipoHipoteca" onkeypress="return generalSoloLetras(event)" required>
                            <div class="invalid-feedback">
                                Ingrese el tipo de hipoteca
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmTipoHipoteca" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>