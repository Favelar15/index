<div class="modal fade " tabindex="-1" data-bs-keyboard="false" data-bs-backdrop="static" id="mdlGarantiaHipoteca">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span id="spnTitulo">Garantía Hipoteca</span></h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configGarantia.save();" id="frmGarantiaHipoteca" name="frmGarantiaHipoteca" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-label">Garantía <span class="requerido">*</span></label>
                            <select class="form-select cboGarantiaHipoteca" id="cboGarantiaHipoteca" name="cboGarantiaHipoteca" required>
                                <option value="">Seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un tipo de garantía
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnGuardar" class="btn btn-sm btn-primary" form="frmGarantiaHipoteca">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>