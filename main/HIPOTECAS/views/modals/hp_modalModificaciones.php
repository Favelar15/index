<?php
?>
<div class="modal fade" tabindex="-1" data-bs-keyboard="false" data-bs-backdrop="static" id="mdlModificacionHipoteca">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mdlTitulo"><span id="spnTitulo">Modificaciones hipoteca</span></h5>
            </div>
            <div class="modal-body">
                <form id="frmModificacion" name="frmModificacion" class="needs-validation" novalidate action="javascript:configModificacion.save();">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="hdnAccionModificacion" name="hdnAccionModificacion" value="N">
                            <input type="hidden" id="hdnIdModificacionHipoteca" name="hdnIdModificacionHipoteca" value="0">
                            <div class="row" id="divModalModificacionesHipoteca">
                                <div class="col-sm-4">
                                    <label class="form-label" for="dtpFechaHoraModificacion">Fecha y hora de modificación <span class="requerido">*</span></label>
                                    <input class="form-control fechaHoraAbierta" type="text" id="dtpFechaHoraModificacion" name="dtpFechaHoraModificacion" readonly>
                                    <div class="invalid-feedback">
                                        Seleccione la fecha
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="form-label" for="txtNombreAbogadoModificacion">Nombre abogado <span class="requerido">*</span></label>
                                    <input type="text" class="form-control" id="txtNombreAbogadoModificacion" name="txtNombreAbogadoModificacion" maxlength="250" required onkeypress="return generalSoloLetras(event)">
                                    <div class="invalid-feedback">
                                        Ingrese el nombre del abogado
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="form-label" for="txtAsientoRegistro">Asiento registro <span class="requerido">*</span></label>
                                    <input type="text" class="form-control" id="txtAsientoRegistro" name="txtAsientoRegistro" onkeypress="return generalSoloNumeros(event)" maxlength="50" required>
                                    <div class="invalid-feedback">
                                        Ingrese el número de asiento
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="form-label" for="txtAmpliacionMonto">Ampliación en monto <span class="requerido">*</span></label>
                                    <input type="text" class="form-control" id="txtAmpliacionMonto" name="txtAmpliacionMonto" onkeypress="return generalSoloNumeros(event)" maxlength="50" required>
                                    <div class="invalid-feedback">
                                        Ingrese el monto de ampliación
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="form-label" for="">Ampliación plazo <span class="requerido">*</span></label>
                                    <input type="text" class="form-control" id="txtAmpliacionPlazo" name="txtAmpliacionPlazo" onkeypress="return generalSoloNumeros(event)" maxlength="50" required>
                                    <div class="invalid-feedback">
                                        Ingrese el plazo ampliado
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnGuardar" class="btn btn-sm btn-primary" form="frmModificacion">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar
                </button>
            </div>
        </div>
    </div>
</div>