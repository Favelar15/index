<?php ?>
<div class="modal fade" data-bs-keyboard="false" data-bs-backdrop="static" tabindex="-1" id="mdlObservacionesHipoteca">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mdlTitulo"><span id="spnTitulo">Observación Hipoteca</span></h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configObservacion.save();" id="frmObservacion" name="frmObservacion" accept-charset="utf-8" method="post" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-label">Escriba la observación<span class="requerido">*</span></label>
                            <textarea type="text" class="form-control" id="txtObservacionHipoteca" required name="txtObservacionHipoteca" style="height: 80px !important;" maxlength="7000" rows="5"></textarea>
                            <div class="invalid-feedback">
                                Ingrese la observación
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnGuardar" class="btn btn-sm btn-primary" form="frmObservacion">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>