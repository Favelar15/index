<?php ?>
<div class="modal fade" data-bs-keyboard="false" data-bs-backdrop="static" tabindex="-1" id="mdlMatriculasHipoteca">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span id="spnTitulo">Matícula Hipoteca</span></h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configMatricula.save();" id="frmMatriculaHipoteca" name="frmMatriculaHipoteca" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-label">Número de Matrícula <span class="requerido">*</span></label>
                            <input type="text" class="form-control" id="txtNumeroMatricula" name="txtNumeroMatricula" maxlength="14" onkeypress="return generaLetrasNumeros(event), configMatricula.validarFormatoMatricula(this.value)" required>
                            <div class="invalid-feedback">
                                Ingrese el número de matrícula
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmMatriculaHipoteca" id="btnGuardar" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>