<?php 
$accionLabel = (isset($_GET['acc']) && $_GET['acc'] == 'N') ? 'Nuevo' : 'Actualizacion de';

?>
<link rel="stylesheet" data-bs-target="./main/css/hipotecas.css">
<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1><?php echo $accionLabel; ?> abogado</h1>
        </div>
        <div class="col-4 topBtnContainer">
            <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configAbogado.cancel();">
                <i class="fas fa-times"></i> <span>Cancelar</span>
            </button>
            <button class="btn btn-sm btn-outline-success float-end" title="Guardar" type="submit" form="frmAbogado">
                <i class="fas fa-save"></i> <span>Guardar</span>
            </button>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Hipotecas</li>
            <li class="breadcrumb-item">Administración</li>
            <li class="breadcrumb-item">Abogado</li>
            <li class="breadcrumb-item active">Configuración</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <fieldset class="border p-1" style="padding-top: 0;">
        <legend class="w-auto float-none mb-0 pb-0">Datos personales</legend>
        <form action="javascript:configAbogado.save()" id="frmAbogado" name="frmAbogado" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
            <div class="row">
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label" for="cboTipoDocumentoAbogado">Tipo de documento: <span class="requerido">*</span></label>
                    <select class="form-select cboTipoDocumento tipoPrimario" id="cboTipoDocumentoAbogado" onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumentoPrimarioAbogado');" required>
                        <option selected disabled value=''>seleccione</option>
                    </select>
                    <div class="invalid-feedback">
                        Seleccione un tipo de documento
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label" for="txtDocumentoPrimarioAbogado">Numero de documento: <span class="requerido">*</span></label>
                    <input type="text" class="form-control numeroDocumentoPrimario" id="txtDocumentoPrimarioAbogado" name="txtDocumentoPrimarioAbogado" placeholder="Ingrese el número de documento" readonly required>
                    <div class="invalid-feedback">
                        Ingrese el número de documento
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label" for="txtNitAbogado">NIT: <span class="requerido">*</span></label>
                    <input type="text" class="form-control NIT numeroDocumentoSecundario" id="txtNitAbogado" name="txtNitAbogado" placeholder="ingrese el numero de NIT" required>
                    <div class="invalid-feedback">
                        Ingrese el número de NIT
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label" for="txtNumeroAbogado">Número abogado: <span class="requerido">*</span></label>
                    <input type="text" class="form-control " id="txtNumeroAbogado" name="txtNumeroAbogado" maxlength="15" onkeypress="return generalSoloNumeros(event)" placeholder="Número de Abogado" required>
                    <div class="invalid-feedback">
                        Ingrese el número de abogado
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label class="form-label">Nombres <span class="requerido">*</span></label>
                    <input class="form-control" type="text" id="txtNombresAbogado" name="txtNombresAbogado" onkeypress="return generalSoloLetras(event)" placeholder="Nombres Abogado" required>
                    <div class="invalid-feedback">
                        Ingrese los nombres
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label class="form-label">Apellidos <span class="requerido">*</span></label>
                    <input class="form-control" type="text" id="txtApellidosAbogado" name="txtApellidosAbogado" onkeypress="return generalSoloLetras(event)" placeholder="Apellidos Abogado" required>
                    <div class="invalid-feedback">
                        Ingrese los apellidos
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label">Teléfono <span class="requerido">*</span></label>
                    <input class="form-control telefono" type="text" id="txtTelefonoCelular" name="txtTelefonoCelular" required>
                    <div class="invalid-feedback">
                        Ingrese el número de teléfono
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label">Correo Electrónico</label>
                    <input class="form-control" type="email" id="txtCorreoElectronico" name="txtCorreoElectronico">
                    <div class="invalid-feedback">
                        Ingrese el correo electrónico
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label" for="cboPaisAbogado">Pais: <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select class="form-control selectpicker cboPais" title="País" data-live-search="true" id="cboPaisAbogado" name="cboPaisAbogado" onchange="generalMonitoreoPais(this.value,'cboDepartamentoAbogado');" required>
                            <option value="" selected disabled>Seleccione</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label" for="cboDepartamentoAbogado">Departamento: <span class="requerido">*</span> </label>
                    <div class="form-group has-validation">
                        <select class="form-control selectpicker" title="Departamento" data-live-search="true" id="cboDepartamentoAbogado" name="cboDepartamentoAbogado" onchange="generalMonitoreoDepartamento('cboPaisAbogado',this.value,'cboMunicipioAbogado');" required>
                            <option value="" selected disabled>seleccione</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label" for="cboMunicipioAbogado">Municipio: <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select class="form-control selectpicker" title="Municipio" data-live-search="true" id="cboMunicipioAbogado" name="cboMunicipioAbogado" required>
                            <option selected value="" disabled>seleccione</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label" for="txtDireccion">Dirección</label>
                    <input type="text" class="form-control" id="txtDireccion" name="txtDireccion" placeholder="Ingrese la dirección">
                    <div class="invalid-feedback">
                        Ingrese la dirección
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label">Estado abogado <span class="requerido">*</span></label>
                    <select class="form-select cboEstadoAbogado" id="cboEstadoAbogado" name="cboEstadoAbogado" onchange="hp_requerirFechaFin(this)" required>
                        <option value="">Seleccione</option>
                    </select>
                    <div class="invalid-feedback">
                        Seleccione el estado del abogado
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label class="form-label">Fecha inició contrato <span class="requerido">*</span></label>
                    <input type="text" class="form-control fechaAbierta readonly" autocomplete="off" id="dtpInicioContrato" name="dtpInicioContrato" placeholder="dd-mm-yyyy" required>
                    <div class="invalid-feedback">
                        Seleccione una fecha
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 fecha_abierta">
                    <label class="form-label">Fecha finalizó contrato</label>
                    <input type="text" class="form-control fechaAbierta readonly" autocomplete="off" id="dtpFinContrato" name="dtpFinContrato" placeholder="dd-mm-yyyy">
                    <div class="invalid-feedback">
                        Seleccione una fecha
                    </div>
                </div>
            </div>
        </form>
    </fieldset>
    <div class="row mt-2">
        <div class="col d-flex align-items-end">
            <label class="form-label" class="form-label">Agencias asignadas</label>
        </div>
        <div class="col">
            <button type="button" class="btn btn-sm btn-outline-primary float-end" onclick="configAgencia.edit()">
                <i class="fas fa-plus"></i> Agencia
            </button>
        </div>
    </div>
    <hr class="mt-0">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <table id="tblAsignacionAgenciaAbogados" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Agencia</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php include "modals/hp_modalConfigAgenciaAbogado.php" ?>
<?php $_GET["js"] = ['hp_generales', 'hp_abogados']; ?>