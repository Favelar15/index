<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";

session_start();
$respuesta['respuesta'] = [];

if (isset($_SESSION["index"])) {
    date_default_timezone_set('America/El_Salvador');
    include "../../code/connectionSqlServer.php";

    $query = "select * from hp_catPlazosHipotecas";
    $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
    $result->execute();
    if ($result->rowCount() > 0){
       $respuesta = $result->fetchAll(PDO::FETCH_ASSOC);
    }else{
     $respuesta['respuesta'] = "NO_EXCE";
    }
} else {
    $respuesta['respuesta'] = "SESSION";
}
echo json_encode($respuesta);
