<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    $filename  = "rpt-" . $input['fechaInicio'] . "-hasta-" . $input['fechaFin'] . ".xlsx";

    $respuesta->{"nombreArchivo"} = $filename;

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'N°');
    $sheet->setCellValue('B1', 'Código de cliente');
    $sheet->setCellValue('C1', 'Número Crédito');
    $sheet->setCellValue('D1', 'Nombre Cliente');
    $sheet->setCellValue('E1', 'Agencia');
    $sheet->setCellValue('F1', 'Nombre Abogado');
    $sheet->setCellValue('G1', 'Ejecutivo');
    $sheet->setCellValue('H1', 'Estado Hipoteca');
    $sheet->setCellValue('I1', 'Ubicación de Hipoteca');
    $sheet->setCellValue('J1', 'Tipo Hipoteca');
    $sheet->setCellValue('K1', 'Plazo Hipoteca');
    $sheet->setCellValue('L1', 'Monto Hipoteca');
    $sheet->setCellValue('M1', 'Monto credito');
    $sheet->setCellValue('N1', 'Monto cancelado');
    $sheet->setCellValue('O1', 'Plazo con modificaciones');
    $sheet->setCellValue('P1', 'Monto con modificaciones');
    $sheet->setCellValue('Q1', 'SIRYC');
    $sheet->setCellValue('R1', 'Modificaciones');
    $sheet->setCellValue('S1', 'Garantías');
    $sheet->setCellValue('T1', 'Matrículas');
    $sheet->setCellValue('U1', 'Observaciones');
    $sheet->setCellValue('V1', 'Usuario Registró');
    $sheet->setCellValue('W1', 'Fecha Otorgamiento');
    $sheet->setCellValue('X1', 'Fecha Inscripción');
    $sheet->setCellValue('Y1', 'Fecha Cancelación');
    $sheet->setCellValue('Z1', 'Fecha Registro en Sistema');
    $spreadsheet->getActiveSheet()->getStyle('A1:Z1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $spreadsheet->getActiveSheet()->getStyle('A1:Z1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

    $nFila = 1;
    foreach ($input["datos"] as $contrato) {
        $nFila++;
        $sheet->setCellValue('A' . $nFila, ($nFila - 1));
        $sheet->setCellValue('B' . $nFila, $contrato["codigoCliente"]);
        $sheet->setCellValue('C' . $nFila, $contrato["numeroCredito"]);
        $sheet->setCellValue('D' . $nFila, $contrato["nombreCliente"]);
        $sheet->setCellValue('E' . $nFila, $contrato["nombreAgencia"]);
        $sheet->setCellValue('F' . $nFila, $contrato["nombresAbogado"]);
        $sheet->setCellValue('G' . $nFila, $contrato["nombreEjecutivo"]);
        $sheet->setCellValue('H' . $nFila, $contrato["estadoHipoteca"]);
        $sheet->setCellValue('I' . $nFila, $contrato["ubicacionHipoteca"]);
        $sheet->setCellValue('J' . $nFila, $contrato["tipoHipoteca"]);
        $sheet->setCellValue('K' . $nFila, $contrato["plazo"]);
        $sheet->setCellValue('L' . $nFila, $contrato["montoHipoteca"]);
        $sheet->setCellValue('M' . $nFila, $contrato["montoCredito"]);
        $sheet->setCellValue('N' . $nFila, $contrato["montoCancelado"]);
        $sheet->setCellValue('O' . $nFila, $contrato["plazoModificaciones"]);
        $sheet->setCellValue('P' . $nFila, $contrato["montoModificaciones"]);
        $sheet->setCellValue('Q' . $nFila, $contrato["syric"]);
        $sheet->setCellValue('R' . $nFila, $contrato["modificaciones"]);
        $sheet->setCellValue('S' . $nFila, $contrato["garantias"]);
        $sheet->setCellValue('T' . $nFila, $contrato["matriculas"]);
        $sheet->setCellValue('U' . $nFila, $contrato["observaciones"]);
        $sheet->setCellValue('V' . $nFila, $contrato["nombreUsuario"]);
        $sheet->setCellValue('W' . $nFila, $contrato["fechaOtorgamiento"]);
        $sheet->setCellValue('X' . $nFila, $contrato["fechaInscripcion"]);
        $sheet->setCellValue('Y' . $nFila, $contrato["fechaCancelacion"]);
        $sheet->setCellValue('Z' . $nFila, $contrato["fechaRegistro"]);
    }
    $writer = new Xlsx($spreadsheet);
    $writer->save(__DIR__ . '/../docs/Reportes/' . $filename);
    $respuesta->{"respuesta"} = "EXITO";
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
