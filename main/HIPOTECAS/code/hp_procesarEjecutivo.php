<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";

session_start();
$respuesta['respuesta'] = "NO_ACCION";
$accion = "";


if ($_SESSION['index']) {
    if (!empty($_GET)) {
        $accion = isset($_GET['accion']) ? $_GET['accion'] : "";
    } else if (isset($input['accion'])) {
        $accion = isset($input['accion']) ? $input['accion'] : "prueba";
    }
    include "../../code/connectionSqlServer.php";
    $ejecutivo = new hp_procesarEjecutivo();

    switch ($accion) {
        case 'obtenerAgenciaAsignada':
            $ejecutivo->idAgenciaAsignada = $_GET['idAgencia'];
            $respuesta = $ejecutivo->obtenerAgenciaAsignada();
            break;
    }
} else {
    $respuesta['respuesta'] = "SESION";
}
echo json_encode($respuesta);

class hp_procesarEjecutivo
{
    protected $conection;
    public $id,
        $nombres,
        $apellidos,
        $idAgenciaAsignada;

    public function __construct()
    {
        global $conexion;
        $this->conection = $conexion;
    }

    function obtenerAgenciaAsignada()
    {
        $return['respuesta'] = 'NO_DATOS';

        $query = "select * from hp_viewObtenerEjecutivosAgenciasaCbo";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            $return = $result->fetchAll(PDO::FETCH_ASSOC);

            foreach ($return as $key => $ejecutivo) {
                $agencias = explode(",", $ejecutivo["agencias"]);
                $roles = explode(",", $ejecutivo["roles"]);

                if (!in_array($this->idAgenciaAsignada, $agencias) || (!in_array(37, $roles) && !in_array(19,$roles) && !in_array(33,$roles) && !in_array(34,$roles) && !in_array(36,$roles))) {
                    unset($return[$key]);
                }
            }
        }
        $this->conection = null;
        return $return;
    }
}
