<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";

session_start();
$respuesta['RESPUESTA'] = "NO_ACCION";
$accion = "";
if (isset($_SESSION['index'])) {
    if (!empty($_GET)) {
        $accion = isset($_GET['accion']) ? $_GET['accion'] : "";
    } else if (isset($input['accion'])) {
        $accion = isset($input['accion']) ? $input['accion'] : "prueba";
    }
    include "../../code/connectionSqlServer.php";
    $hipoteca = new hp_procesarHipoteca();

    switch ($accion) {
        case 'guardarHipoteca':

            $hipoteca->idHipoteca = ((isset($input['idHipoteca']) && $input['idHipoteca'] != 0)) ? $input['idHipoteca'] : 0;
            $hipoteca->codigoCliente = $input['datosHipoteca']['txtCodigoCliente']['value'];
            $hipoteca->numeroCredito = $input['datosHipoteca']['txtNumeroCredito']['value'];
            $hipoteca->nombresDeudor = $input['datosHipoteca']['txtNombreDeudor']['value'];
            $hipoteca->apellidosDeudor = $input['datosHipoteca']['txtApellidosDeudor']['value'];
            $hipoteca->idAgencia = $input['datosHipoteca']['cboAgencia']['value'];
            $hipoteca->idAbogado = $input['datosHipoteca']['cboAbogado']['value'];
            $hipoteca->idEjecutivo = $input['datosHipoteca']['cboEjecutivo']['value'];
            $hipoteca->idEstadoHipoteca = $input['datosHipoteca']['cboEstadoHipoteca']['value'];
            $hipoteca->idUbicacionHipoteca = $input['datosHipoteca']['cboUbicacionHipoteca']['value'];
            $hipoteca->idTipoHipoteca = $input['datosHipoteca']['cboTipoHipoteca']['value'];
            $hipoteca->plazoHipoteca = $input['datosHipoteca']['numPlazoHipoteca']['value'];
            $hipoteca->montoHipoteca = $input['datosHipoteca']['numMontoHipoteca']['value'];
            $hipoteca->montoCredito = $input['datosHipoteca']['numMontoCredito']['value'];
            $hipoteca->montoCancelado = $input['datosHipoteca']['numMontoHipotecaCancelada']['value'];
            $hipoteca->plazoModificaciones = $input['datosHipoteca']['numPlazoModificaciones']['value'];
            $hipoteca->montoHipotecaModificaciones = $input['datosHipoteca']['numMontoModificaciones']['value'];
            $hipoteca->syric = $input['datosHipoteca']['txtSyric']['value'];
            $hipoteca->fechaOtorgamiento = ($input['datosHipoteca']['dtpFechaOtorgamiento']['value'] != null) ? date('Y-m-d', strtotime($input['datosHipoteca']['dtpFechaOtorgamiento']['value'])) : null;
            $hipoteca->fechaInscripcion = ($input['datosHipoteca']['dtpFechaInscripcion']['value'] != null) ? date('Y-m-d', strtotime($input['datosHipoteca']['dtpFechaInscripcion']['value'])) : null;
            $hipoteca->fechaCancelacion = ($input['datosHipoteca']['dtpFechaCancelacion']['value'] != null) ? date('Y-m-d', strtotime($input['datosHipoteca']['dtpFechaCancelacion']['value'])) : null;

            $datosModificaciones = $input['datosModificaciones'];
            $datosGarantias = $input['datosGarantias'];
            $datosMatriculas = $input['datosMatriculas'];
            $datosObservaciones = $input['datosObservaciones'];

            $arrayModificaciones = [];
            $arrayGarantias = [];
            $arrayMatriculas = [];
            $arrayObservaciones = [];

            foreach ($datosModificaciones as $modificaciones) {
                array_push($arrayModificaciones, $modificaciones['id'] . "$$$" . $modificaciones['ampliacionMonto'] . "$$$" . $modificaciones['ampliacionPlazo'] . "$$$" . $modificaciones['asientoRegistro'] . "$$$" . date('Y-m-d H:i', strtotime($modificaciones['fechaHora'])) . "$$$" . $modificaciones['nombreAbogado']);
            }

            $modificacionesString = count($arrayModificaciones) > 0 ? implode('%%%', $arrayModificaciones) : "null";

            foreach ($datosGarantias as $garantia) {
                array_push($arrayGarantias, $garantia['idGarantiaHipoteca']);
            }
            $garantiasString = count($arrayGarantias) > 0 ? implode("%%%", $arrayGarantias) : 'null';

            foreach ($datosMatriculas as $datosMatricula) {
                array_push($arrayMatriculas, $datosMatricula['id'] . "$$$" . $datosMatricula['numeroMatricula']);
            }
            $matriculasString = count($arrayMatriculas) > 0 ? implode("%%%", $arrayMatriculas) : "null";

            foreach ($datosObservaciones as $datosObservacione) {
                array_push($arrayObservaciones, $datosObservacione['id'] . "$$$" . $datosObservacione['observacion']);
            }
            $observacionesString = count($arrayObservaciones) > 0 ? implode("%%%", $arrayObservaciones) : "null";

            $hipoteca->stringModificaciones = $modificacionesString;
            $hipoteca->stringGarantias = $garantiasString;
            $hipoteca->stringMatriculas = $matriculasString;
            $hipoteca->stringObservaciones = $observacionesString;
            $hipoteca->stringLogsCambiosHipoteca = $input['camposEditados'];
            $hipoteca->idApartado = base64_decode(urldecode($input['idApartado']));
            $hipoteca->tipoApartado = $input['tipoApartado'];
            $hipoteca->idUsuario = $_SESSION['index']->id;
            $hipoteca->ipActual = generalObtenerIp();
            $hipoteca->idAgenciaActual = $_SESSION['index']->agenciaActual->id;
            $respuesta = $hipoteca->guardarHipoteca();

            break;
        case  'obtenerHipotecasAdministracion':
            $respuesta = $hipoteca->obtenerHipotecaAdm();
            break;
        case 'obtenerHipotecaEspecifica':
            $hipoteca->idHipoteca = $_GET['idHipoteca'];
            $respuesta = $hipoteca->obtenerHipotecaEspecifica();
            break;

        case 'obtenerHipotecasReporteGeneral':
            $hipoteca->idAgencia = $_GET['idAgencia'];
            $hipoteca->idAbogado = $_GET['idAbogado'];
            $hipoteca->idEjecutivo = $_GET['idEjecutivo'];
            $hipoteca->idEstadoHipoteca = $_GET['idEstadoHipoteca'];
            $hipoteca->fechaInicio = date('Y-m-d', strtotime( $_GET['fechaInicio']));
            $hipoteca->fechaFin = date('Y-m-d', strtotime( $_GET['fechaFin']));
            $hipoteca->idUbicacionHipoteca = $_GET['idUbicacionHipoteca'];
            $respuesta = $hipoteca->obtenerReporteGeneralHipoteca();
            break;

        case 'eliminarHipoteca':
            $hipoteca->idHipoteca = $input['idHipoteca'];
            $hipoteca->stringLogsCambiosHipoteca = "Estado%%%Habilitado%%%Eliminado";
            $hipoteca->idApartado = base64_decode(urldecode($input['idApartado']));
            $hipoteca->tipoApartado = $input['tipoApartado'];
            $hipoteca->idUsuario = $_SESSION['index']->id;
            $hipoteca->idAgenciaActual = $_SESSION['index']->agenciaActual->id;
            $hipoteca->ipActual = generalObtenerIp();
            $respuesta = $hipoteca->eliminarHipoteca();
            break;
        default:
            $respuesta = $accion;
            break;
    }


} else {
    $respuesta['respuesta'] = "SESION";
}


echo json_encode($respuesta);

class hp_procesarHipoteca
{
    protected $conection;
    public $idHipoteca,
        $codigoCliente,
        $numeroCredito,
        $nombresDeudor,
        $apellidosDeudor,
        $idAgencia,
        $idAbogado,
        $idEjecutivo,
        $idEstadoHipoteca,
        $idUbicacionHipoteca,
        $idTipoHipoteca,
        $plazoHipoteca,
        $montoHipoteca,
        $montoCredito,
        $montoCancelado,
        $plazoModificaciones,
        $montoHipotecaModificaciones,
        $syric,
        $fechaOtorgamiento,
        $fechaInscripcion,
        $fechaCancelacion,
        $stringModificaciones,
        $stringGarantias,
        $stringMatriculas,
        $stringObservaciones,
        $stringLogsCambiosHipoteca,
        $idUsuario,
        $idApartado,
        $tipoApartado,
        $ipActual,
        $idAgenciaActual,
        $fechaInicio,
        $fechaFin;

    public function __construct()
    {
        global $conexion;
        $this->conection = $conexion;
    }

    function obtenerHipotecaAdm()
    {
        $arrayAgencias = [];
        foreach ($_SESSION['index']->agencias as $agencia) {
            array_push($arrayAgencias, $agencia->id);
        }
        $strinAgencias = count($arrayAgencias) > 0 ? implode(",", $arrayAgencias) : $_SESSION['index']->agenciaActual;

        $query = "select * from view_hpObtenerHipotecasAdm where idAgencia in ($strinAgencias)";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            if ($result->rowCount() > 0) {
                $retorno = $result->fetchAll(PDO::FETCH_ASSOC);
            }
        } else {
            $retorno[] = "NO_EXCE";
        }
        $this->conection = null;
        return $retorno;
    }

    function obtenerHipotecaEspecifica()
    {
        $return[] = "NO_DATOS";
        $response = null;
        $query = "select * from hp_viewObtenerHipotecaEspecifica where id  = $this->idHipoteca";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            if ($result->rowCount() > 0) {
                $retorno = $result->fetchAll(PDO::FETCH_ASSOC);
            }
        } else {
            $retorno[] = "NO_EXCE";
        }
        $this->conection = null;
        return $retorno;
    }

    function obtenerReporteGeneralHipoteca(){
        $return[] = "NO_DATOS";
        $response = null;

        $condicion =  "where fechaOtorgamiento between '$this->fechaInicio' and '$this->fechaFin' ";
        $condicion.= ($this->idAgencia == 'all') ? " " :  " and idAgencia =  $this->idAgencia " ;
        $condicion.= ($this->idAbogado == 'all') ? " " : " and idAbogado =  $this->idAbogado ";
        $condicion.= ($this->idEjecutivo == 'all') ? " " : " and idEjecutivo = $this->idEjecutivo ";
        $condicion.= ($this->idEstadoHipoteca == 'all' )? " " : " and idEstadoHipoteca =  $this->idEstadoHipoteca ";
        $condicion.= ($this->idUbicacionHipoteca == 'all' )? " " : " and idUbicacionHipoteca =  $this->idUbicacionHipoteca ";

        $query = "select * from view_hpObtenerHipotecasReporteGeneral {$condicion}";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            if ($result->rowCount() > 0) {
                $retorno = $result->fetchAll(PDO::FETCH_ASSOC);
            }else{
                $retorno["respuesta"] = "NO_DATOS";
            }
        } else {
            $retorno["respuesta"] = "NO_EXCE";
        }
        $this->conection = null;
        return $retorno;
    }

    function guardarHipoteca()
    {


        $return[] = "NO_DATOS";
        $response = null;
        $query = 'EXEC hp_spProcesarHipoteca :idHipoteca, :codigoCliente, :numeroCredito, :nombresDeudor, :apellidosDeudor, :idAgencia, :idAbogado, :idEjecutivo, :idEstadoHipoteca, :idTipoHipoteca, :idUbicacionHipoteca, :plazoHipoteca, :montoHipoteca, :montoCredito, :montoCancelado, :plazoModificaciones, :montoModificaciones, :siryc, :fechaOtorgamiento, :fechaInscripcion, :fechaCancelacion, :stringModificaciones, :stringGarantias, :stringMatriculas, :stringObservaciones, :stringLogsCambios, :idApartado, :tipoApartado, :idUsuario, :ipActual, :idAgenciaActual, :respuesta';
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);


        $result->bindParam(':idHipoteca', $this->idHipoteca, PDO::PARAM_INT);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':numeroCredito', $this->numeroCredito, PDO::PARAM_STR);
        $result->bindParam(':nombresDeudor', $this->nombresDeudor, PDO::PARAM_STR);
        $result->bindParam(':apellidosDeudor', $this->apellidosDeudor, PDO::PARAM_STR);
        $result->bindParam(':idAgencia', $this->idAgencia, PDO::PARAM_INT);
        $result->bindParam(':idAbogado', $this->idAbogado, PDO::PARAM_INT);
        $result->bindParam(':idEjecutivo', $this->idEjecutivo, PDO::PARAM_INT);
        $result->bindParam(':idEstadoHipoteca', $this->idEstadoHipoteca, PDO::PARAM_INT);
        $result->bindParam(':idTipoHipoteca', $this->idTipoHipoteca, PDO::PARAM_INT);
        $result->bindParam(':idUbicacionHipoteca', $this->idUbicacionHipoteca, PDO::PARAM_INT);
        $result->bindParam(':plazoHipoteca', $this->plazoHipoteca, PDO::PARAM_INT);
        $result->bindParam(':montoHipoteca', $this->montoHipoteca, PDO::PARAM_STR);
        $result->bindParam(':montoCredito', $this->montoCredito, PDO::PARAM_STR);
        $result->bindParam(':montoCancelado', $this->montoCancelado, PDO::PARAM_STR);
        $result->bindParam(':plazoModificaciones', $this->plazoModificaciones, PDO::PARAM_INT);
        $result->bindParam(':montoModificaciones', $this->montoHipotecaModificaciones, PDO::PARAM_STR);
        $result->bindParam(':siryc', $this->syric, PDO::PARAM_STR);
        $result->bindParam(':fechaOtorgamiento', $this->fechaOtorgamiento, PDO::PARAM_STR);
        $result->bindParam(':fechaInscripcion', $this->fechaInscripcion, PDO::PARAM_STR);
        $result->bindParam(':fechaCancelacion', $this->fechaCancelacion, PDO::PARAM_STR);
        $result->bindParam(':stringModificaciones', $this->stringModificaciones, PDO::PARAM_STR);
        $result->bindParam(':stringGarantias', $this->stringGarantias, PDO::PARAM_STR);
        $result->bindParam(':stringMatriculas', $this->stringMatriculas, PDO::PARAM_STR);
        $result->bindParam(':stringObservaciones', $this->stringObservaciones, PDO::PARAM_STR);
        $result->bindParam(':stringLogsCambios', $this->stringLogsCambiosHipoteca, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipActual', $this->ipActual, PDO::PARAM_STR);
        $result->bindParam(':idAgenciaActual', $this->idAgenciaActual, PDO::PARAM_INT);
        $result->bindParam(':respuesta', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $return = ["respuesta" => $response];
        } else {
            $return = "NO_EXCE";
        }
        $this->conection = null;
        return $return;
    }

    function eliminarHipoteca()
    {
        $return[] = "NO_DATOS";
        $response = null;
        $query = 'EXEC hp_spEliminarHipoteca  :idHipoteca, :stringLogs, :idApartado, :tipoApartado, :idUsuario, :ipActual, :idAgenciaActual, :respuesta';
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idHipoteca', $this->idHipoteca, PDO::PARAM_INT);
        $result->bindParam(':stringLogs', $this->stringLogsCambiosHipoteca, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipActual', $this->ipActual, PDO::PARAM_STR);
        $result->bindParam(':idAgenciaActual', $this->idAgenciaActual, PDO::PARAM_INT);
        $result->bindParam(':respuesta', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $return = ["respuesta" => $response];
        } else {
            $return = "NO_EXCE";
        }
        $this->conection = null;
        return $return;
    }


}