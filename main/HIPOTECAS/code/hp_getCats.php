<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";

        $solicitados = explode("@@@", $_GET["q"]);

        if (in_array('tiposDocumento', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/tipoDocumento.php';
            $tipoDocumento = new tipoDocumento();
            $tiposDocumento = $tipoDocumento->obtenerTiposDocumento();

            $respuesta->{"tiposDocumento"} = $tiposDocumento;
        }

        if (in_array('agenciasCbo', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/agencia.php';
            $agencia = new agencia();
            $agencias = $agencia->getAgenciasCbo($_SESSION["index"]->agencias);

            $respuesta->{"agencias"} = $agencias;
        }

        if (in_array('estadosHipoteca', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/estadoHipoteca.php';
            $estadoHipoteca = new estadoHipoteca();
            $estadosHipoteca = $estadoHipoteca->obtenerEstadosHipoteca();

            $respuesta->{"estadosHipoteca"} = $estadosHipoteca;
        }

        if (in_array('tiposHipotecaGeneral', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/tipoHipoteca.php';
            $tipoHipoteca = new hp_tipoHipoteca();
            $tiposHipoteca = $tipoHipoteca->obtenerTipoHipotecaGeneral();

            $respuesta->{"tiposHipoteca"} = $tiposHipoteca;
        }

        if (in_array('plazosHipoteca', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/plazoHipoteca.php';
            $plazoHipoteca = new plazoHipoteca();
            $plazosHipoteca = $plazoHipoteca->obtenerPlazosHipoteca();

            $respuesta->{"plazosHipoteca"} = $plazosHipoteca;
        }

        if (in_array('garantiasHipoteca',  $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/garantiaHipoteca.php';
            $garantiaHipotecas  =  new hp_garantiasHipoteca();
            $garantiasHipotecas =  $garantiaHipotecas->obtenerGarantiasHipoteca();

            $respuesta->{"garantiasHipoteca"} =  $garantiasHipotecas;
        }

        if (in_array('geograficos', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/datosGeograficos.php';
            $geografico = new geografico();
            $datosGeograficos = $geografico->obtenerGeograficos();

            $respuesta->{"datosGeograficos"} = $datosGeograficos;
        }

        if (in_array('estadosAbogadoCbo', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/estadoAbogado.php';
            $estado = new estadoAbogado();
            $estadosAbogado = $estado->getEstadosAbogadoCbo();
            $respuesta->{"estadosAbogado"} = $estadosAbogado;
        }

        if (in_array('ubicacionesHipoteca', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/ubicacionesHipoteca.php';
            $estado = new ubicacionesHipoteca();
            $estadosAbogado = $estado->obtenerUbicacionHipoteca();
            $respuesta->{"ubicacionesHipoteca"} = $estadosAbogado;
        }

        if (in_array('tiposGarantias', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/tiposGarantias.php';
            $estado = new tiposGarantias();
            $tiposGarantias = $estado->obtenerTiposGarantias();
            $respuesta->{"tiposGarantias"} = $tiposGarantias;
        }

        $conexion = null;
    }
}

echo json_encode($respuesta);
