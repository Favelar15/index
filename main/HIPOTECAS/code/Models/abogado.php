<?php
class hp_abogados
{
    protected $conection;
    public
        $idAbogado,
        $idTipoDocumentoPrimario,
        $numeroDocumentoPrimario,
        $numeroNIT,
        $numeroAbogado,
        $nombres,
        $apellidos,
        $telefono,
        $correoElectronico,
        $idPais,
        $idDepartamento,
        $idMuncipio,
        $direccion,
        $idEstadoAbogado,
        $fechaInicio,
        $fechaFin,
        $stringLogCambios,
        $fechaActual,
        $agenciasAsignadas,
        $idAgenciaActual,
        $idUsuario,
        $ipOrdenador,
        $idApartado,
        $tipoApartado,
        $idAgenciaAsignada;

    public function __construct()
    {
        global $conexion;
        $this->conection = $conexion;
    }

    function obtenerAbogadosTbl()
    {
        $return[] = 'NO_DATOS';

        $query = "select * from hp_viewDatosAbogado ";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            $datos = $result->fetchAll(PDO::FETCH_ASSOC);
            foreach($datos as $key=>$abogado){
                !empty($abogado["inicioContrato"]) && $datos[$key]["inicioContrato"] = date('d-m-Y',strtotime($abogado["inicioContrato"]));
                !empty($abogado["finContrato"]) && $datos[$key]["finContrato"] = date('d-m-Y',strtotime($abogado["finContrato"]));
            }
            $return = $datos;
        }

        $this->conection = null;
        return $return;
    }

    function obtenerAbogadoEspecifico()
    {
        $return[] = 'NO_DATOS';
        $query = "select * from hp_viewDatosAbogado where idAbogado = $this->idAbogado";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            $datos = $result->fetchAll(PDO::FETCH_ASSOC);
            !empty($datos[0]["inicioContrato"]) && $datos[0]["inicioContrato"] = date('d-m-Y',strtotime($datos[0]["inicioContrato"]));
            !empty($datos[0]["finContrato"]) && $datos[0]["finContrato"] = date('d-m-Y',strtotime($datos[0]["finContrato"]));
            $return = $datos;
        }
        $this->conection = null;
        return $return;
    }

    function obtenerAbogadoAgenciaAsignada()
    {
        $return['respuesta'] = 'NO_DATOS';
        $this->idAgenciaAsignada;

        $query = "select * from hp_viewAbogadosAgenciasAsignadasCbo where idAgencia = $this->idAgenciaAsignada";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
        $this->conection = null;
        return [];
    }

    function obtenerAbogadosAgenciasAsignadasCbo()
    {
        $return[] = "NO_DATOS";
        $query = "select * from hp_view_cbo_abogadoxAgenciaAsignada where idAgencia = $this->idAbogado";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
        $this->conection = null;
        return [];
    }

    function guardarAbogado()
    {
        $return[] = "NO_DATOS";
        $response = null;

        $query = 'EXEC hp_spGuardarAbogado :identificadorAbogado, :idtipoDocumentoPrimario, :numeroDocumentoPrimario, :numeroNitAbogado, :numeroAbogado, :nombresAbogado, :ApellidosAbogado, :numeroTelefono, :correoElectronico,:idPais, :idDepartamento, :idMuncipio,  :direccion, :identificadorEstadoAbogado, :fechaInicio, :fechaFin, :identificadorUsuarioRegistra, :agenciaActual, :ipActual, :agenciasAsignadas, :stringLogsCambio, :idApartado, :tipoApartado, :response';
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':identificadorAbogado', $this->idAbogado, PDO::PARAM_INT);
        $result->bindParam(':idtipoDocumentoPrimario', $this->idTipoDocumentoPrimario, PDO::PARAM_STR);
        $result->bindParam(':numeroDocumentoPrimario', $this->numeroDocumentoPrimario, PDO::PARAM_STR);
        $result->bindParam(':numeroNitAbogado', $this->numeroNIT, PDO::PARAM_STR);
        $result->bindParam(':numeroAbogado', $this->numeroAbogado, PDO::PARAM_STR);
        $result->bindParam(':nombresAbogado', $this->nombres, PDO::PARAM_STR);
        $result->bindParam(':ApellidosAbogado', $this->apellidos, PDO::PARAM_STR);
        $result->bindParam(':numeroTelefono', $this->telefono, PDO::PARAM_STR);
        $result->bindParam(':correoElectronico', $this->correoElectronico, PDO::PARAM_STR);
        $result->bindParam(':idPais', $this->idPais, PDO::PARAM_STR);
        $result->bindParam(':idDepartamento', $this->idDepartamento, PDO::PARAM_INT);
        $result->bindParam(':idMuncipio', $this->idMuncipio, PDO::PARAM_INT);
        $result->bindParam(':direccion', $this->direccion, PDO::PARAM_STR);
        $result->bindParam(':identificadorEstadoAbogado', $this->idEstadoAbogado, PDO::PARAM_INT);
        $result->bindParam(':fechaInicio', $this->fechaInicio, PDO::PARAM_STR);
        $result->bindParam(':fechaFin', $this->fechaFin, PDO::PARAM_STR);
        $result->bindParam(':identificadorUsuarioRegistra', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':agenciaActual', $this->idAgenciaActual, PDO::PARAM_STR);
        $result->bindParam(':ipActual', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':agenciasAsignadas', $this->agenciasAsignadas, PDO::PARAM_STR);
        $result->bindParam(':stringLogsCambio', $this->stringLogCambios, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $return = ["respuesta" => $response];
        }
        $this->conection = null;
        return $return;
    }

    function eliminarAbogado()
    {
        $return[] = "NO_DATOS";
        $response = null;

        $query = 'EXEC hp_spEliminarAbogado :idAbogado, :idUsuario, :logsCambios, :ipActual, :idApartado, :tipoApartado, :response';
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idAbogado', $this->idAbogado, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':logsCambios', $this->stringLogCambios, PDO::PARAM_STR);
        $result->bindParam(':ipActual', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        if ($result->execute()) {
            $return = ["respuesta" => $response];
        }
        $this->conection = null;
        return $return;
    }
}