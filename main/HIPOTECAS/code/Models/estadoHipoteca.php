<?php
class estadoHipoteca
{
    public $id;
    public $estadoHipoteca;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function obtenerEstadosHipoteca()
    {
        $query = " select * from hp_viewEstadosHipoteca ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'estadoHipoteca');
        }

        $this->conexion = null;
        return [];
    }
}