<?php
class plazoHipoteca
{
    public $id;
    public $nombrePlazo;
    public $divisor;
    public $habilitado;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function obtenerPlazosHipoteca()
    {
        $query = " select * from hp_catPlazosHipotecas ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'plazoHipoteca');
        }

        $this->conexion = null;
        return [];
    }
}