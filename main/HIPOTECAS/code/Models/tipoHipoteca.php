<?php
class hp_tipoHipoteca
{
    protected $conection;
    public $id,
        $tipoHipoteca,
        $idUsuario,
        $idAgenciaActual,
        $ipOrdenador,
        $logCambios,
        $idApartado,
        $tipoApartado;

    public function __construct()
    {
        global $conexion;
        $this->conection = $conexion;
    }

    function obtenerTipoHipotecaGeneral()
    {

        $retorno[] = "NO_DATOS";
        $query = "select * from hp_viewTiposHipoteca ";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            if ($result->rowCount() > 0) {
                $retorno = $result->fetchAll(PDO::FETCH_ASSOC);
            }
        } else {
            $retorno[] = "NO_EXCE";
        }
        $this->conection = null;
        return $retorno;
    }

    function obtenerTipoHipotecaEspecifica()
    {
        $return[] = "NO_DATOS";
        $query = "select * from hp_viewTiposHipoteca where id = $this->id ";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            if ($result->rowCount() > 0) {
                $return = $result->fetchAll(PDO::FETCH_ASSOC);
            }
        } else {
            $return = "NO_EXCE";
        }
        $this->conection = null;
        return $return;
    }

    function guardarTipoHipoteca()
    {
        $return[] = "NO_DATOS";
        $response = null;
        $query = 'EXEC hp_spProcesarTipoHipoteca :id, :tipoHipoteca, :idUsuario, :logCambios, :ipOrdenador, :idApartado, :tipoApartado, :response';
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':tipoHipoteca', $this->tipoHipoteca, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_STR);
        $result->bindParam(':logCambios', $this->logCambios, PDO::PARAM_STR);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        if ($result->execute()) {
            $return = ["respuesta" => $response];
        }
        $this->conection = null;
        return $return;
    }

    function eliminarTipoHipoteca()
    {

        $return[] = "NO_DATOS";
        $response = null;
        $query = 'EXEC hp_spEliminarTipoHipoteca :id, :idUsuario, :stringLogs, :ipOrdenador, :idApartado, :tipoApartado, :response';
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':stringLogs', $this->logCambios, PDO::PARAM_STR);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        if ($result->execute()) {
            $return = ["respuesta" => $response];
        }
        $this->conection = null;
        return $return;
    }
}