<?php
class hp_garantiasHipoteca
{
    private $conexion;
    public $id,
        $nombre;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function obtenerGarantiasHipoteca()
    {
        $query = "select * from hp_catTiposGarantias where habilitado =  'S'";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            return $result->fetchAll(PDO::FETCH_ASSOC);
            $this->conexion = null;
        }
    }
}