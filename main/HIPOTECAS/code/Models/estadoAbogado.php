<?php
class estadoAbogado
{
    public $id;
    public $estadoAbogado;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function getEstadosAbogadoCbo()
    {
        $query = "SELECT id,estadoAbogado FROM hp_catEstadosAbogados where habilitado = 'S';";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_OBJ);
        }

        $this->conexion = null;
        return [];
    }
}
