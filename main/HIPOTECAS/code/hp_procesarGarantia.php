<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";

session_start();
$respuesta['RESPUESTA'] = "NO_ACCION";
$accion = "";

if (isset($_SESSION['index'])) {
    if (!empty($_GET)) {
        $accion = isset($_GET['accion']) ? $_GET['accion'] : "";
    } else if (isset($input['accion'])) {
        $accion = isset($input['accion']) ? $input['accion'] : "prueba";
    }
    include "../../code/connectionSqlServer.php";
    $garantia = new hp_procesarGarantia();
    switch ($accion) {
        case 'obtenerGarantiasGeneral':
            $respuesta = $garantia->obtenerGarantiasGeneral();
            break;
        case  'obtenerGarantiaEspecifica':
            $garantia->id = $_GET['id'];
            $respuesta = $garantia->obtenerGarantiaEspecifica();
            break;

        case 'guardarGarantia':
            $garantia->id = $input['id'];
            $garantia->tipoGarantia = $input['tipoGarantia'];
            $garantia->idUsuario = $_SESSION['index']->id;
            $garantia->ipOrdenador = generalObtenerIp();
            $garantia->logCambios = $input['logsCambios'];
            $garantia->idApartado = base64_decode(urldecode($input['idApartado']));
            $garantia->tipoApartado = $input['tipoApartado'];
            $respuesta = $garantia->guardarGarantia();
            break;

        case 'eliminarTipoGarantia':
            $garantia->id = $input['id'];
            $garantia->idUsuario = $_SESSION['index']->id;
            $garantia->ipOrdenador = generalObtenerIp();
            $garantia->logCambios =  "Estado%%%Habilitado%%%Deshabilitado";;
            $garantia->idApartado = base64_decode(urldecode($input['idApartado']));
            $garantia->tipoApartado = $input['tipoApartado'];
            $respuesta = $garantia->eliminarGarantia();
            break;

        default:
            break;
    }
} else {
    $respuesta['respuesta'] = "SESSION";
}
echo json_encode($respuesta);

class hp_procesarGarantia
{
    protected $conection;
    public $id,
        $tipoGarantia,
        $idUsuario,
        $agenciaActual,
        $ipOrdenador,
        $logCambios,
        $idApartado,
        $tipoApartado;

    public function __construct()
    {
        global $conexion;
        $this->conection = $conexion;
    }

    function obtenerGarantiasGeneral()
    {
        $return[] = "NO_DATOS";
        $query = "select * from hp_viewTiposGarantias";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            if ($result->rowCount() > 0) {
                $return = $result->fetchAll(PDO::FETCH_ASSOC);
            }
        } else {
            $return[] = "NO_EXCE";
        }
        $this->conection = null;

        return $return;
    }

    function obtenerGarantiaEspecifica()
    {
        $return[] = "NO_DATOS";
        $query = "select * from hp_viewTiposGarantias where id = $this->id";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            if ($result->rowCount() > 0) {
                $return = $result->fetchAll(PDO::FETCH_ASSOC);
            }
        } else {
            $return[] = "NO_EXCE";
        }
        $this->conection = null;

        return $return;
    }

    function guardarGarantia()
    {

        $return[] = "NO_DATOS";
        $response = null;
        $query = 'EXEC hp_spProcesarGarantia :id, :tipoGarantia, :idUsuario, :logCambios, :ipOrdenador, :idApartado, :tipoApartado, :response';
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':tipoGarantia', $this->tipoGarantia, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_STR);
        $result->bindParam(':logCambios', $this->logCambios, PDO::PARAM_STR);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $return = ["respuesta" => $response];
        } else {
            $return = "NO_EXCE";
        }
        $this->conection = null;
        return $return;
    }

    function eliminarGarantia()
    {
        $return[] = "NO_DATOS";
        $response = null;
        $query = "EXEC hp_spEliminarTipoGarantia :idGarantia, :idUsuario, :logCambios, :ipOrdenador, :idApartado, :tipoApartado, :response";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idGarantia', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':logCambios', $this->logCambios, PDO::PARAM_STR);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        if ($result->execute()) {
            $return = ["respuesta" => $response];
        }
        $this->conection = null;
        return $return;
    }
}
