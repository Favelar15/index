<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";

session_start();
$respuesta[] = "NO_ACCION";
$accion = "";


if (isset($_SESSION['index'])) {
    if (!empty($_GET)) {
        $accion = isset($_GET['accion']) ? $_GET['accion'] : "";
    } else if (isset($input['accion'])) {
        $accion = isset($input['accion']) ? $input['accion'] : "prueba";
    }

    include "../../code/connectionSqlServer.php";
    require_once './Models/tipoHipoteca.php';
    $tipoHipoteca = new hp_tipoHipoteca();
    switch ($accion) {
        case 'obtenerTipoHipotecaEspecifica':
            $tipoHipoteca->id = $_GET['id'];
            $respuesta = $tipoHipoteca->obtenerTipoHipotecaEspecifica();

            break;
        case 'obtenertipoHipotecaGeneral':
            $respuesta = $tipoHipoteca->obtenerTipoHipotecaGeneral();
            break;

        case 'guardarTipoHipoteca':
            $tipoHipoteca->id = $input['id'];
            $tipoHipoteca->tipoHipoteca = $input['tipoHipoteca'];
            $tipoHipoteca->idUsuario = $_SESSION['index']->id;
            $tipoHipoteca->ipOrdenador = generalObtenerIp();
            $tipoHipoteca->logCambios = $input['logsCambios'];
            $tipoHipoteca->idApartado = base64_decode(urldecode($input['idApartado']));
            $tipoHipoteca->tipoApartado = $input['tipoApartado'];

            $respuesta = $tipoHipoteca->guardarTipoHipoteca();
            break;

        case 'eliminarTipoHipoteca':

            $tipoHipoteca->id = $input['id'];
            $tipoHipoteca->idUsuario = $_SESSION['index']->id;
            $tipoHipoteca->logCambios = "Accion%%%Habilitado%%%Eliminado";
            $tipoHipoteca->ipOrdenador = generalObtenerIp();
            $tipoHipoteca->idApartado = base64_decode(urldecode($input['idApartado']));
            $tipoHipoteca->tipoApartado = $input['tipoApartado'];

            $respuesta = $tipoHipoteca->eliminarTipoHipoteca();
            break;
        default:
            $respuesta['respuesta'] = "DEFAULT";
            break;
    }
} else {
    $respuesta['respuesta'] = "SESSION";
}

echo json_encode($respuesta);
