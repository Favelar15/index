<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";

session_start();
$respuesta = [];
$accion = "";

if (isset($_SESSION['index'])) {
    if (!empty($_GET)) {
        $accion = isset($_GET['accion']) ? $_GET['accion'] : "";
    } else if (isset($input['accion'])) {
        $accion = isset($input['accion']) ? $input['accion'] : "prueba";
    }

    include "../../code/connectionSqlServer.php";

    require_once './Models/abogado.php';
    $abogado = new  hp_abogados();

    switch ($accion) {
        case 'obtenerAbogadoEspecifico':
            $abogado->idAbogado = base64_decode(urldecode($_GET['idAbogado']));
            $respuesta = $abogado->obtenerAbogadoEspecifico();
            break;

        case 'obtenerAbogadoGeneral':
            $respuesta = $abogado->obtenerAbogadosTbl();
            break;

        case 'obtenerAbogadoAgenciaAsignada':
            $abogado->idAgenciaAsignada = $_GET['idAgencia'];
            $respuesta = $abogado->obtenerAbogadoAgenciaAsignada();
            break;

        case 'guardarAbogado':

            $agencias = [];
            $abogado->idAbogado = $input['identificadorAbogado'];
            $abogado->idUsuario = $_SESSION['index']->id;
            $abogado->idTipoDocumentoPrimario = $input['cboTipoDocumentoAbogado']['value'];
            $abogado->numeroDocumentoPrimario = $input['txtDocumentoPrimarioAbogado']['value'];
            $abogado->numeroNIT = $input['txtNitAbogado']['value'];
            $abogado->numeroAbogado = $input['txtNumeroAbogado']['value'];
            $abogado->nombres = $input['txtNombresAbogado']['value'];
            $abogado->apellidos = $input['txtApellidosAbogado']['value'];
            $abogado->telefono = $input['txtTelefonoCelular']['value'];
            $abogado->correoElectronico = $input['txtCorreoElectronico']['value'];
            $abogado->idPais = $input['cboPaisAbogado']['value'];
            $abogado->idDepartamento = $input['cboDepartamentoAbogado']['value'];
            $abogado->idMuncipio = $input['cboMunicipioAbogado']['value'];
            $abogado->direccion = $input['txtDireccion']['value'];
            $abogado->idEstadoAbogado = $input['cboEstadoAbogado']['value'];
            $abogado->fechaInicio = date('Y-m-d', strtotime($input['dtpInicioContrato']['value']));
            $abogado->fechaFin = ($input['cboEstadoAbogado']['value'] == 1) ? null : date('Y-m-d', strtotime($input['datosAgencias']['value']));
            $abogado->stringLogCambios = $input['datosCambiosAbogado'];
            $abogado->fechaActual = date('Y-m-d');
            $abogado->idAgenciaActual = $_SESSION['index']->agenciaActual->id;
            $abogado->ipOrdenador = generalObtenerIp();
            $abogado->idApartado = base64_decode(urldecode($input['idApartado']));
            $abogado->tipoApartado = $input['tipoApartado'];
            foreach ($input['datosAgencias'] as $agencia) {
                array_push($agencias, $agencia['id']);
            }
            $datosAgenciasString = count($agencias) > 0 ? implode("@@@", $agencias) : "";
            $abogado->agenciasAsignadas = $datosAgenciasString;

            $respuesta = $abogado->guardarAbogado();

            break;

        case 'eliminarAbogado':
            $abogado->idAbogado = $input['identificadorAbogado'];
            $abogado->idUsuario = $_SESSION['index']->id;
            $abogado->stringLogCambios = "Accion%%%Habilitado%%%Eliminado";
            $abogado->ipOrdenador = generalObtenerIp();
            $abogado->idApartado = base64_decode(urldecode($input['idApartado']));
            $abogado->tipoApartado = $input['tipoApartado'];
            $respuesta = $abogado->eliminarAbogado();
            break;

        default:
            $respuesta['respuesta'] = "SIN_ACCION";
            break;
    }
} else {
    $respuesta['respuesta'] = "SESION";
}

echo json_encode($respuesta);
