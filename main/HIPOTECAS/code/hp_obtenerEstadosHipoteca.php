<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = [];

if (count($input) > 0) {
    include "../../code/connectionSqlServer.php";


    $query = " select * from hp_viewEstadosHipoteca ";
    $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
    $result->execute();

    if ($result->rowCount() > 0) {
        while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {
            $respuesta[] = new hp_tmpObtenerEstadoHipoteca($rows['id'], $rows['estadoHipoteca']);
        }
    } else {
        $respuesta[] = "NO_DATOS";
    }
} else {
    $respuesta[] = "NO_POST";
}

echo json_encode($respuesta);
$conexion = null;

class hp_tmpObtenerEstadoHipoteca
{
    var $identificador, $estadoHipoteca;

    function __construct($identificador, $estadoHipoteca)
    {
        $this->identificador = $identificador;
        $this->estadoHipoteca = $estadoHipoteca;
    }

}