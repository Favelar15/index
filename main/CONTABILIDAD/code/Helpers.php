<?php

class Helpers
{

    public static function formatGetLogs( $logs )
    {
        $logsDB = '';

        if (count($logs) > 0) {
            $logTemp = [];

            foreach ($logs as $log) {
                $campo = $log['campo'];
                $valorAntiguo = $log['valorAnterior'];
                $valorNuevo = $log['valorNuevo'];
                $temp = $campo . '%%%' . $valorAntiguo . '%%%' . $valorNuevo;
                array_push($logTemp, $temp);
            }
            $logsDB = implode("@@@", $logTemp);
        }

        return $logsDB;
    }

}