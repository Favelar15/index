<?php
header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
$input = json_decode(file_get_contents("php://input"), true);

session_start();

$respuesta = ['respuesta' => (Object)[] ];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    require_once '../../../code/connectionSqlServer.php';
    require_once '../../../code/Permiso.php';
    require_once 'Models/SujetoExcluido.php';

    $sujeto = new SujetoExcluido();
    $sujeto->idResolucion = (int) $input['idResolucion'];
    $sujeto->correlativo = (int) $input['correlativo'];
    $sujeto->id = (int) $input['idSujeto'];
    $respuesta['respuesta'] = $sujeto->comprobarCorrelativo();

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);