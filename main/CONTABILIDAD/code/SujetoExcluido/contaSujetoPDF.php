<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = [];

if ( isset($_SESSION['index'])  && $_SESSION['index']->locked )
{
    if ( isset( $_GET['id'] ))
    {
        require_once '../../../code/connectionSqlServer.php';
        require_once '../../../code/Permiso.php';
        require_once 'Models/SujetoExcluido.php';

        $sujeto = new SujetoExcluido();
        $sujeto->id = (int) $_GET['id'];
        $response = $sujeto->generarPDF();

        $respuesta['respuesta'] = $response->respuesta;
        $respuesta['nombreArchivo'] = $response->nombreArchivo;

    } else {
        $respuesta['respuesta'] = 'La solicitud no existe';
        $respuesta['nombreArchivo'] = null;
    }
} else
{
    $respuesta['respuesta'] = 'SESION';
}
echo json_encode( $respuesta  );
