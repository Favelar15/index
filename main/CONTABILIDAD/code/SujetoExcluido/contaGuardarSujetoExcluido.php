<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
$input = json_decode(file_get_contents("php://input"), true);

session_start();

$respuesta = [ 'respuesta' => (Object) [] ];

if ( isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../../../code/Permiso.php';
    require_once 'Models/SujetoExcluido.php';

    $sujeto = new SujetoExcluido();

    $proveedor = $input['proveedor'];

    $sujeto->idTipoDocumento = (int) $proveedor['cboTipoDocumento']['value'];
    $sujeto->numeroDocumento = $proveedor['txtDocumentoPrimario']['value'];
    $sujeto->codigoCliente = !empty($proveedor['txtCodigoCliente']['value']) ? $proveedor['txtCodigoCliente']['value'] : null ;
    $sujeto->nombres = $proveedor['txtNombres']['value'];
    $sujeto->apellidos = $proveedor['txtApellidos']['value'];
    $sujeto->nit = $proveedor['txtNit']['value'];
    $sujeto->telefonoFijo = $proveedor['txtTelefonoFijo']['value'];
    $sujeto->telefonoMovil = $proveedor['txtTelefonoMovil']['value'];
    $sujeto->email = $proveedor['txtEmail']['value'];
    $sujeto->idPais = (int) $proveedor['cboPais']['value'];
    $sujeto->idDepartamento = (int) $proveedor['cboDepartamento']['value'];
    $sujeto->idMunicipio = (int) $proveedor['cboMunicipio']['value'];
    $sujeto->direccion = $proveedor['txtDireccion']['value'];

    $sujetoExcluido = $input['sujetosExcluidos'];
    $sujeto->id = $sujetoExcluido['idSujeto'];
    $sujeto->idResolucion = (int) $sujetoExcluido['cboResolucion']['value'];
    $sujeto->correlativo = (int) $sujetoExcluido['txtCorrelativo']['value'];
    $sujeto->fechaEmision = date('Y-m-d', strtotime( $sujetoExcluido['txtFechaEmision']['value'] ) ) ;
    $sujeto->idTipoServicio = (int) $sujetoExcluido['cboTipoServicio']['value'];
    $sujeto->iva = $sujetoExcluido['cboIva']['value'];

    $sujeto->renta = $sujetoExcluido['cboRenta']['value'];
    $sujeto->seAsumeRenta = !empty($sujetoExcluido['cboAsumimosRenta']['value']) ? $sujetoExcluido['cboAsumimosRenta']['value'] : null;

    $calcularRenta = new Calcular();

    $detalle = $input['detalle'];
    $dbDetalle = '';

    if ( count( $detalle ) > 0 )
    {
        $detalleTemp = [];

        $precioRenta = 0;
        $montoRenta = 0;
        $sumaPrecioConRenta = 0; $renta = 0;

        foreach ( $detalle as $det )
        {
            $dbLogs = 'NULL';

            if ( count( $det['logs'] ) > 0  )
            {
                $logsTemporal = [];
                foreach ( $det['logs'] as $log )
                {
                    array_push( $logsTemporal, $log['campo'].'###'.$log['valorAnterior'].'###'.$log['valorNuevo']);
                }
                $dbLogs = implode('???', $logsTemporal );
            }

            if ( $det['formulario']['habilitado'] == 'S')
            {
                if ( $sujeto->seAsumeRenta == 'S')
                {
                    $precioRenta = (int) $det['formulario']['cantidad'] * $calcularRenta->calcularRenta( floatval( $det['formulario']['precio']));
                    $montoRenta += $precioRenta - ( $det['formulario']['cantidad'] * floatval( $det['formulario']['precio'] ) );
                } else
                {
                    $precioRenta = (int) $det['formulario']['cantidad'] * floatval($det['formulario']['precio']);
                }

                $sumaPrecioConRenta += $precioRenta;
            }

            array_push($detalleTemp, $det['formulario']['id'].'%%%'.$det['formulario']['descripcion'].'%%%'.$det['formulario']['cantidad'].'%%%'.$det['formulario']['precio'].'%%%'.$det['formulario']['habilitado'].'%%%'.$dbLogs);
        }
        $dbDetalle = implode( '@@@', $detalleTemp);
    }

    //echo json_encode( $dbDetalle ); return;

    $sujeto->montoRenta = $sujeto->renta == 'S' && $sujeto->seAsumeRenta == 'S'? sprintf('%0.2f', $montoRenta) : sprintf('%0.2f', $sumaPrecioConRenta * 0.10);
    $sujeto->detalleSujeto = $dbDetalle;
    $sujeto->montoIva = $sujetoExcluido['cboIva']['value'] == 'S' ?  sprintf('%0.2f', $calcularRenta->calcularIva( $sumaPrecioConRenta ) )  : null;

    $logsProveedor = $input['logsProveedor'];
    $dbLogsProveedor = '';

    if ( count( $logsProveedor ) > 0 )
    {
        $provedorTemp = [];

        foreach ( $logsProveedor as $log )
        {
            array_push( $provedorTemp, $log['campo'].'%%%'.$log['valorAnterior'].'%%%'.$log['valorNuevo']);
        }

        $dbLogsProveedor = implode('@@@', $provedorTemp);
    }

    $logsSujeto = $input['logsSujeto'];
    $dbLogsSujetoExcluido = '';

    if ( count($logsSujeto) > 0 )
    {
        $sujetoTemp = [];
        foreach ($logsSujeto as $log )
        {
            array_push( $sujetoTemp, $log['campo'].'%%%'.$log['valorAnterior'].'%%%'.$log['valorNuevo']);
        }
        $dbLogsSujetoExcluido = implode('@@@', $sujetoTemp);
    }


    $sujeto->idUsuario = $_SESSION['index']->id;
    $sujeto->ipOrdenador = generalObtenerIp();
    $sujeto->idAgencia = $_SESSION['index']->agenciaActual->id;

    $sujeto->setIdApartado((int)base64_decode(urldecode($input['idApartado'])));
    $sujeto->setTipoApartado($input['tipoApartado']);

    $respuesta = $sujeto->guardar( $dbLogsProveedor, $dbLogsSujetoExcluido );


    //$respuesta['Total'] = sprintf('%0.2f', $sumaPrecioConRenta);
    //$respuesta['IVA'] = sprintf('%0.2f', $sujeto->montoIva);
    //$respuesta['RENTA'] =  $sujeto->renta == 'S' && $sujeto->seAsumeRenta == 'S'? sprintf('%0.2f', $montoRenta) : sprintf('%0.2f', $sumaPrecioConRenta * 0.10);


} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );


class Calcular
{
    private $iva = 0.13;

    function calcularRenta ( $sujeto )
    {
        return floatval($sujeto)  + ( floatval($sujeto) / 9 );
    }

    function calcularIva( $monto )
    {
        return floatval($monto) * $this->iva;
    }
}
