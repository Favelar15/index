<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
//$input = json_decode(file_get_contents("php://input"), true);

session_start();

$respuesta = [ 'sujetos' => [] ];

if ( isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../../../code/Permiso.php';
    require_once 'Models/SujetoExcluido.php';

    $sujeto = new SujetoExcluido();

    if ( !empty( $_GET['id'] ) )
    {
        $sujeto->id = (int) $_GET['id'];
        $respuesta['sujetos'] = $sujeto->obtenerSujeto();
    } else
    {
        $respuesta['sujetos'] = $sujeto->obtenerSujetos();
    }
}

echo json_encode( $respuesta );