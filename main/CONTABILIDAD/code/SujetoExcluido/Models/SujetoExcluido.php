<?php

class Proveedor extends Permiso
{
    private $conexion;

    public $idProveedor, $codigoCliente,
        $numeroDocumento,
        $idTipoDocumento,
        $nombres, $apellidos, $nit, $telefonoFijo, $telefonoMovil, $email,
        $idPais, $idDepartamento, $idMunicipio, $direccion, $idUsuario, $ipOrdenador, $fechaRegistro;

    function __construct()
    {
        parent::__construct();

        global $conexion;
        $this->conexion = $conexion;
    }

    private function buscarAsociadoPorDUI()
    {
        $query = "SELECT * FROM general_viewDatosAsociados WHERE tipoDocumentoPrimario = :tipoDocumento AND numeroDocumentoPrimario = :numeroDocumento AND habilitado = 'S';";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':tipoDocumento', $this->idTipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumento', $this->numeroDocumento, PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetch(PDO::FETCH_OBJ);
        }
        return false;
    }

    function buscarProveedor()
    {
        $query = "SELECT * FROM conta_viewDatosProveedores WHERE idTipoDocumento = :idTipoDocumento AND numeroDocumento = :numeroDocumento";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idTipoDocumento', $this->idTipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumento', $this->numeroDocumento, PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() > 0) {
            $proveedor = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__)[0];

            return (object)[
                'Persona' => $proveedor,
                'existe' => true
            ];

        } else
        {
            $proveedor = $this->buscarAsociadoPorDUI();

            if ($proveedor) {
                return (object)[
                    'Persona' => $proveedor,
                    'existe' => true
                ];
            }
        }

        $this->conexion = null;
        return (object)[
            'Persona' => (object)[],
            'existe' => false,
            'uno' => 'okokok'
        ];
    }
}


class SujetoExcluido extends Proveedor
{
    public $id, $idResolucion, $correlativo, $fechaEmision, $idTipoServicio,
        $renta, $montoRenta, $seAsumeRenta, $iva, $montoIva,
    $anuladoFisico, $idAgencia, $motivoAnulacion;

    private $conexion;
    private $path =  __DIR__ . '\..\..\..\docs\SujetosExcluidos\/';
    private $agenciasAsignadas = [];

    public $detalleSujeto;


    public function __construct()
    {
        parent::__construct();

        global $conexion;
        $this->conexion = $conexion;

        !(empty($this->fechaEmision)) && $this->fechaEmision = date('d-m-Y', strtotime($this->fechaEmision));

    }

    function comprobarCorrelativo()
    {
        $isValido = false;
        $respuesta = null;

        $query = 'EXEC conta_spComprobarCorrelativo :idResolucion, :correlativo, :idSujeto, :respuesta, :isValido';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idResolucion', $this->idResolucion, PDO::PARAM_INT);
        $result->bindParam(':idSujeto', $this->id, PDO::PARAM_INT);
        $result->bindParam(':correlativo', $this->correlativo, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':isValido', $isValido, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        return (Object)[
            'respuesta' => $respuesta,
            'valido' => boolval($isValido)
        ];
    }

    function guardar( $logsPersona, $logsSujeto )
    {
        $respuesta = null; $nombreArchivo =  null; $idDb = null;

        $query = 'EXEC conta_spGuardarSujetoExcluido :tipoDocumento, :numeroDocumento, :codigoCliente, :nombres, :apellidos, :nit, :telefonoFijo, :telefonoMovil,
                :email, :idPais, :idDepartamento, :idMunicipio, :direccion, :idSujeto, :idResolucion, :correlativo, :fechaEmision, :idTipoServicio, :renta,
                :seAsumeRenta, :montoRenta, :asumimosIva, :montoIva, :detalleSujeto, :ipOrdenador, :idUsuario, :idAgencia, :idApartado, :tipoApartado, :logsPersona, 
                :logsSujeto, :respuesta, :nombreArchivo, :idDb';

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':tipoDocumento', $this->idTipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumento', $this->numeroDocumento, PDO::PARAM_STR);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':nombres', $this->nombres, PDO::PARAM_STR);
        $result->bindParam(':apellidos', $this->apellidos, PDO::PARAM_STR);
        $result->bindParam(':nit', $this->nit, PDO::PARAM_STR);
        $result->bindParam(':telefonoFijo', $this->telefonoFijo, PDO::PARAM_STR);
        $result->bindParam(':telefonoMovil', $this->telefonoMovil, PDO::PARAM_STR);
        $result->bindParam(':email', $this->email, PDO::PARAM_STR);
        $result->bindParam(':idPais', $this->idPais, PDO::PARAM_INT);
        $result->bindParam(':idDepartamento', $this->idDepartamento, PDO::PARAM_INT);
        $result->bindParam(':idMunicipio', $this->idMunicipio, PDO::PARAM_INT);
        $result->bindParam(':direccion', $this->direccion, PDO::PARAM_STR);
        $result->bindParam(':idSujeto', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idResolucion', $this->idResolucion, PDO::PARAM_INT);
        $result->bindParam(':correlativo', $this->correlativo, PDO::PARAM_INT);
        $result->bindParam(':fechaEmision', $this->fechaEmision, PDO::PARAM_STR);
        $result->bindParam(':idTipoServicio', $this->idTipoServicio, PDO::PARAM_INT);
        $result->bindParam(':renta', $this->renta, PDO::PARAM_STR);
        $result->bindParam(':seAsumeRenta', $this->seAsumeRenta, PDO::PARAM_STR);
        $result->bindParam(':montoRenta', $this->montoRenta, PDO::PARAM_INT);
        $result->bindParam(':asumimosIva', $this->iva, PDO::PARAM_STR);
        $result->bindParam(':montoIva', $this->montoIva, PDO::PARAM_INT);
        $result->bindParam(':detalleSujeto', $this->detalleSujeto, PDO::PARAM_STR);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':idAgencia', $this->idAgencia, PDO::PARAM_INT);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':logsPersona', $logsPersona, PDO::PARAM_STR);
        $result->bindParam(':logsSujeto', $logsSujeto, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':nombreArchivo', $nombreArchivo, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idDb', $idDb, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ($respuesta == 'EXITO') {
            return (object)['respuesta' => $respuesta, 'nombreArchivo' => $nombreArchivo, 'idDb' => $idDb];
        }
        return (object)['respuesta' => 'ERROR', 'mensaje' => $respuesta, 'nombreArchivo' => null, 'idDb' => null ];
    }

    function obtenerSujeto()
    {
        $query = "SELECT * FROM conta_viewDatosSujetos WHERE id = :id";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->execute();

        if ($result->rowCount() > 0) {
            $sujeto = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__)[0];
            $sujeto->detalle = $this->obtenerDetalles();
            return $sujeto;
        }

        return [];
    }

    function obtenerSujetos()
    {
        $query = "SELECT * FROM conta_viewListSujetosExcluidos ORDER BY id DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    private function obtenerDetalles()
    {
        $query = "SELECT * FROM conta_viewDetDetalleSujeto WHERE idSujeto = :id";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_OBJ);
        }
        $this->conexion = null;
        return [];
    }

    function setIdApartado( $idApartado )
    {
        $this->idApartado = $idApartado;
    }

    function setTipoApartado( $tipoApartado )
    {
        $this->tipoApartado = $tipoApartado;
    }

    function generarPDF()
    {
        $pdf = new SujetoPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', true);

        $sujeto = (Object)[];

        $sujeto = $this->obtenerSujeto();
        $sujeto->detalle = $this->obtenerDetalles();

        $pdf->setSujeto( $sujeto );
        $pdf->configuracionDocumento();
        $pdf->setMargin();

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $pdf->SetAutoPageBreak(TRUE, 35);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();

        $pdf->encabezados();
        $pdf->sujetoExcluido();
        $pdf->detSujeto();
        $pdf->setTotales();

        //$pdf->Output('MyPDF_File.pdf', 'I');

        $pdf->Output($this->path . $sujeto->nombreArchivo, 'F');
        $this->conexion = null;

        if (file_exists($this->path . $sujeto->nombreArchivo)) {
            return (object)["respuesta" => "EXITO", "nombreArchivo" => $sujeto->nombreArchivo];
        }
        return (object)["respuesta" => "Error al construir archivo", "nombreArchivo" => null];
    }

    function anularSujeto()
    {
        $respuesta = null;

        $query = 'EXEC conta_spAnularSujetoExcluido :id, :anuladoFisico, :motivo, :ipOrdenador, :idUsuario, :idApartado, :tipoApartado, :respuesta';

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':anuladoFisico', $this->anuladoFisico, PDO::PARAM_STR);
        $result->bindParam(':motivo', $this->motivoAnulacion, PDO::PARAM_STR);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        return (object)['respuesta' => $respuesta];
    }

    function rptSujetosExcluidos()
    {
        $query = "SELECT * FROM conta_viewRptDatosExcluidos ORDER BY id DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function setAgencias( $agencias )
    {
        $this->agenciasAsignadas = $agencias;
    }

}

class SujetoPDF extends TCPDF
{
    private $sujeto;
    private $sumaSujetos = 0;
    private $total = 0;

    function setSujeto( $sujeto )
    {
        $this->sujeto = $sujeto;
    }

    function configuracionDocumento()
    {
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('Departamento IT');
        $this->SetTitle('Solicitud de activación de cuenta');
        $this->SetSubject('ACACYPAC');
        $this->SetKeywords('TCPDF, PDF, Solicitud de activación de cuenta');

        $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    }

    function setMargin()
    {
        $this->SetMargins(15, 15, 15, 15);
        $this->SetHeaderMargin(4);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);
    }

    function encabezados()
    {
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);

        //20

        $this->Cell(185, 7, '', 0, 0, 'L', 0);
        $this->ln();

        $this->Cell(150, 5, '', 0, 0, 'L', 0);
        $this->Cell(15, 5, str_pad($this->sujeto->correlativo, 5, '0', STR_PAD_LEFT), 0, 0, 'L', 0);
        $this->ln();

        $this->Cell(185, 7, '', 0, 0, 'L', 0);
        $this->ln();
    }

    function sujetoExcluido()
    {
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);


        $this->SetFont('dejavusans', '', 7);
        $this->Cell(30, 5, '', 0, 0, 0, 0);
        $this->Cell(145, 5, $this->sujeto->nombres, 0, 0, 'L', 0);
        $this->ln();

        $this->Cell(30, 5, '', 0, 0, 0, 0);
        $this->writeHTMLCell(145, 4, '', '', '<p style="text-align: justify">' . $this->sujeto->direccion . ' ' . $this->sujeto->municipio . ' ' . $this->sujeto->departamento . '</p>', 0, 0, 0, true, 'L', 0);
        $this->ln();

        $this->Cell(30, 5, '', 0, 0, 0, 0);
        $this->Cell(145, 5, $this->sujeto->telefonoFijo.' / '.$this->sujeto->telefonoMovil, 0, 0, 'L', 0);
        $this->ln();

        $this->Cell(30, 5, '', 0, 0, 0, 0);
        $this->Cell(30, 5, $this->sujeto->nit, 0, 0, 'L', 0);
        $this->Cell(35, 5, $this->sujeto->tipoDocumento.': '.$this->sujeto->nit, 0, 0, 'L', 0);
        $this->Cell(35, 5, 'Correo: '.$this->sujeto->email, 0, 0, 'L', 0);
        $this->ln();


        $this->Cell(30, 5, '', 0, 0, 0, 0);
        $this->Cell(145, 5, $this->sujeto->fechaEmision, 0, 0, 'L', 0);
        $this->ln();
    }

    function detSujeto()
    {
        $this->SetFont('dejavusans', '', 7);
        $this->ln(5);

        foreach ( $this->sujeto->detalle as $detalle )
        {
            $nuevoPrecio = $detalle->precio;

            if ($this->sujeto->seAsumeRenta == 'S' )
            {
                $nuevoPrecio = floatval($nuevoPrecio)  + ( floatval($nuevoPrecio) / 9 );
            }

            $this->sumaSujetos += (int) $detalle->cantidad * floatval($nuevoPrecio);

            $this->Cell(30, 5, '', 0, 0, 0, 0);
            $this->Cell(10, 5, $detalle->cantidad, 0, 0, 'C', 0);
            $this->Cell(90, 5, $detalle->descripcion, 0, 0, 'C', 0);
            $this->Cell(30, 5, number_format($nuevoPrecio, 2, '.', ',') , 0, 0, 'C', 0);
            $this->Cell(30, 5, number_format((int) $detalle->cantidad * floatval($nuevoPrecio), 2, '.', ',') , 0, 0, 'C', 0);
            $this->ln();
        }
    }

    function setTotales()
    {
        $this->total = floatval( $this->sumaSujetos) - floatval( $this->sujeto->montoRenta);

        $this->setY(100);
        $this->SetFont('dejavusans', '', 7);

        if ($this->sujeto->tipoFormato === 'L')
        {
            $this->Cell(30, 5, '', 0, 0, 0, 0);
            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(25, 5, 'RECIBÍ LA CANTIDAD DE:', 0, 0, 'L', 0);
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(100, 5, conversionDineroLetras($this->total, 'S'), 0, 0, 'L', 0);
        } else
        {
            $this->Cell(50, 5, '', 0, 0, 0, 0);
            $this->Cell(135, 5, conversionDineroLetras($this->total, 'S'), 0, 0, 'L', 0);
        }
        $this->ln();

        $this->SetXY(131, 102);

        if ($this->sujeto->tipoFormato === 'L')
        {
            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(45, 5, 'SUMAS', 0, 0, 'R', 0);
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(25, 5, number_format($this->sumaSujetos, 2, '.', ',') , 0, 0, 'L', 0);
        } else
        {
            $this->Cell(45, 5, '', 0, 0, 0, 0);
            $this->Cell(135, 5, number_format($this->sumaSujetos, 2, '.', ',') , 0, 0, 'L', 0);
        }
        $this->ln();


        $this->SetXY(131, 107);

        if ($this->sujeto->tipoFormato === 'L')
        {
            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(45, 5, '(-) RENTA RETENIDO', 0, 0, 'R', 0);
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(135, 5, number_format($this->sujeto->montoRenta, 2, '.', ','), 0, 0, 'L', 0);
        } else
        {
            if ( $this->sujeto->tipoFormato === 'N' )
            {
                $this->Cell(45, 5, '', 0, 0, 0, 0);
                $this->Cell(135, 5, number_format($this->sujeto->montoRenta, 2, '.', ','), 0, 0, 'L', 0);
            } else
            {
                $this->Cell(45, 5, '', 0, 0, 0, 0);
                $this->Cell(135, 5, number_format($this->sujeto->montoIva, 2, '.', ','), 0, 0, 'L', 0);
            }
        }
        $this->ln();

        $this->SetXY(131, 112);

        if ( $this->sujeto->tipoFormato === 'L')
        {
            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(45, 5, 'TOTAL', 0, 0, 'R', 0);
            $this->SetFont('dejavusans', '', 7);
        } else
        {
            $this->Cell(45, 5, '', 0, 0, 0, 0);
        }
        $this->Cell(135, 5, number_format($this->total, 2, '.', ','), 0, 0, 'L', 0);
        $this->ln();
    }
}


