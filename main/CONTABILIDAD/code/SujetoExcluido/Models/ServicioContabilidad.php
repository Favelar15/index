<?php

class ServicioContabilidad
{
    public $id, $servicio;

    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    function obtenerServicios()
    {
        $query = "SELECT * FROM conta_viewCatServicios;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        return [];
    }
}