<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();

$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {

    $nombreArchivo = 'rpt-SujetosExcluidos-' . $input['txtInicio']['value'] . '-' . $input['txtFin']['value'] . '.xlsx';
    $idAgencias = [];

    if ( empty( $input['cboAgencia']['value'] ) )
    {
        foreach ( $_SESSION['index']->agencias as $agencia) {
            array_push($idAgencias, (int)$agencia->id);
        }
    } else {
        $nombreArchivo = 'rpt-SujetosExcluidos-' . $input['cboAgencia']['value'] . '-' . $input['txtInicio']['value'] . '-' . $input['txtFin']['value'] . '.xlsx';
        array_push($idAgencias, (int)$input['cboAgencia']['value']);
    }

    if ($input['accion'] == 'EXPORTAR') {

//        $exportar = new ExcelCreditos();
//        $exportar->agencias = $idAgencias;
//        $exportar->nombreArchivo = $nombreArchivo;
//        $exportar->creditos = $input['creditos'];
//        $respuesta = $exportar->crearReporte();

    } else {

        $desde = date('Y-m-d', strtotime($input['txtInicio']['value']));
        $hasta = date('Y-m-d', strtotime($input['txtFin']['value']));

        require_once '../../../code/connectionSqlServer.php';
        require_once '../../../code/Permiso.php';
        require_once 'Models/SujetoExcluido.php';

        $sujeto = new SujetoExcluido();
        $sujeto->setAgencias( $idAgencias );
        $respuesta['sujetos'] = $sujeto->rptSujetosExcluidos();
    }
}

echo json_encode($respuesta);


class ExcelSujetos
{
    public $nombreArchivo;
    public $creditos = [];
    public $agencias = [];

    function crearReporte()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $agencias = implode(',', $this->agencias);

        $sheet->setCellValue('A1', "Reporte de créditos que nos cancelan de agencias ($agencias)");
        $spreadsheet->getActiveSheet()->mergeCells("A1:I1");
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A2', 'N°');
        $sheet->setCellValue('B2', 'Fecha de registro');
        $sheet->setCellValue('C2', 'Agencia');
        $sheet->setCellValue('D2', 'Numero de préstamo');
        $sheet->setCellValue('E2', 'Asociado');
        $sheet->setCellValue('F2', 'Tipo de préstamo');
        $sheet->setCellValue('G2', 'Empresa');
        $sheet->setCellValue('H2', 'monto');
        $sheet->setCellValue('I2', 'Usuario');

        $spreadsheet->getActiveSheet()->getStyle('A1:I1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $spreadsheet->getActiveSheet()->getStyle('A2:I2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:I2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');


        $contador = 2;

        foreach ($this->creditos as $credito) {
            $contador++;
            $sheet->setCellValue("A$contador", ($contador - 2));
            $sheet->setCellValue("B$contador", $credito['fechaRegistro']);
            $sheet->setCellValue("C$contador", $credito['Agencia']['agencia']);
            $sheet->setCellValue("D$contador", $credito['numeroDePrestamo']);
            $sheet->setCellValue("E$contador", $credito['nombresAsociado'] . ' ' . $credito['apellidosAsociado']);
            $sheet->setCellValue("F$contador", $credito['Prestamo']['prestamo']);
            $sheet->setCellValue("G$contador", $credito['Empresa']['empresa']);
            $sheet->setCellValue("H$contador", $credito['monto']);
            $sheet->setCellValue("I$contador", $credito['usuario']);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '../../../docs/creditos/Excel/' . $this->nombreArchivo);

        return (object)[
            'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];
    }
}