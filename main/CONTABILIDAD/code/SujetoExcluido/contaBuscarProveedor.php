<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
$input = json_decode(file_get_contents("php://input"), true);

session_start();

$respuesta = [ 'proveedor' => (Object) [] ];

if ( isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../../../code/Permiso.php';
    require_once 'Models/SujetoExcluido.php';

    $proveedor = new Proveedor();
    $proveedor->idTipoDocumento = (int) $input['idTipoDocumento'];
    $proveedor->numeroDocumento = $input['numeroDocumento'];

    $respuesta['proveedor'] = $proveedor->buscarProveedor();
}

echo json_encode( $respuesta );




