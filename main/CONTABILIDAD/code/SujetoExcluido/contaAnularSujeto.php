<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
$input = json_decode(file_get_contents("php://input"), true);

session_start();

$respuesta = [ 'respuesta' => (Object) [] ];

if ( isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../../../code/Permiso.php';
    require_once 'Models/SujetoExcluido.php';

    $sujeto = new SujetoExcluido();
    $sujeto->id = $input['id'];
    $sujeto->anuladoFisico = $input['cboDañadoFisicamente']['value'];
    $sujeto->motivoAnulacion = $input['txtMotivo']['value'];
    $sujeto->setIdApartado((int)base64_decode(urldecode($input['idApartado'])));
    $sujeto->setTipoApartado($input['tipoApartado']);
    $sujeto->idUsuario = $_SESSION['index']->id;
    $sujeto->ipOrdenador = generalObtenerIp();

    $respuesta = $sujeto->anularSujeto();

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );



