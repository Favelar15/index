<?php

class Resolucion extends Permiso
{
    public $id, $numeroResolucion, $TipoDocumentoHacienda, $Agencia, $fechaAprobacion, $correlativoInicial,
    $correlativoFinal, $tipoFormato, $idUsuario, $ipOrdenador, $fechaRegistro, $disponible;
    public $idAgencia, $agencia;

    private $idTipoDocumentoHacienda, $tipoDocumento;
    private $conexion;
    private $agencias = [];


    function __construct()
    {
        parent::__construct();

        global $conexion;
        $this->conexion = $conexion;

        if ( !empty( $this->idTipoDocumentoHacienda ))
        {
            $this->TipoDocumentoHacienda = new TipoDocumento();
            $this->TipoDocumentoHacienda->id = $this->idTipoDocumentoHacienda;
            $this->TipoDocumentoHacienda->tipoDocumento = $this->tipoDocumento;
        }

        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime( $this->fechaRegistro ));
        !empty($this->fechaAprobacion) && $this->fechaAprobacion = date('d-m-Y', strtotime( $this->fechaAprobacion ));
    }

    function guardar( $logs )
    {
        $query = 'EXEC conta_spGuardarResolucion :id, :numeroDeResolucion, :idTipoDocumento, :idAgencia, :fechaAprobacion, 
                    :correlativoInicio, :correlativoFin, :disponibles, :tipoFormato, :idUsuario, :ipOrdenador, :logsResolucion, 
                    :idApartado, :tipoApartado, :respuesta, :idDb';

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':numeroDeResolucion', $this->numeroResolucion, PDO::PARAM_STR);
        $result->bindParam(':idTipoDocumento', $this->TipoDocumentoHacienda->id, PDO::PARAM_INT);
        $result->bindParam(':idAgencia', $this->idAgencia, PDO::PARAM_INT);
        $result->bindParam(':fechaAprobacion', $this->fechaAprobacion, PDO::PARAM_STR);
        $result->bindParam(':correlativoInicio', $this->correlativoInicial, PDO::PARAM_STR);
        $result->bindParam(':correlativoFin', $this->correlativoFinal, PDO::PARAM_STR);
        $result->bindParam(':disponibles', $this->disponible, PDO::PARAM_INT);
        $result->bindParam(':tipoFormato', $this->tipoFormato, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':logsResolucion', $logs, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idDb', $idDb, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ($respuesta == 'EXITO') {
            return (object)['respuesta' => $respuesta, 'idResolucion' => $idDb, 'fechaRegistro' => date('d-m-Y H:s a') ];
        }
        return (object)['respuesta' => $respuesta, 'idResolucion' => null, 'fechaRegistro' => null ];
    }

    function eliminar()
    {
        $query = 'EXEC conta_spEliminarResolucion :id, :idUsuario, :ipOrdenador, :idApartado, :tipoApartado, :respuesta';

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        $result->execute();

        return (object)['respuesta' => $respuesta];
    }

    function obtenerResoluciones()
    {
        $query = "SELECT * FROM conta_viewDatosResolucionesHacienda ORDER BY fechaRegistro ASC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function obtenerCatResoluciones()
    {
        $query = "SELECT * FROM conta_viewCatResoluciones";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            return $result->fetchAll(PDO::FETCH_OBJ);
        }
        $this->conexion = null;
        return [];
    }

    function setIdApartado( $idApartado )
    {
        $this->idApartado = $idApartado;
    }

    function setTipoApartado( $tipoApartado )
    {
        $this->tipoApartado = $tipoApartado;
    }

    function setAgencias( $agencia )
    {
        $this->agencias = $agencia;
    }

}