<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if ( isset( $_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../../../code/Permiso.php';

    require_once 'Models/TipoDocumento.php';
    require_once 'Models/Resolucion.php';
    require_once '../Helpers.php';

    $resolucion = new Resolucion();
    $resolucion->id = (int) $input['id'];
    $resolucion->numeroResolucion = $input['txtNumeroResolucion']['value'];
    $resolucion->TipoDocumentoHacienda = new stdClass();
    $resolucion->TipoDocumentoHacienda->id = $input['cboTipoDocumentoHacienda']['value'];
    $resolucion->idAgencia = $input['cboAgencia']['value'];
    $resolucion->fechaAprobacion = date('Y-m-d', strtotime($input['txtFechaAprobacion']['value'])) ;
    $resolucion->correlativoInicial = $input['txtInicio']['value'];
    $resolucion->correlativoFinal = $input['txtFin']['value'];
    $resolucion->correlativoFinal = $input['txtFin']['value'];
    $resolucion->disponible = (int) $input['txtDisponibles']['value'];
    $resolucion->tipoFormato = $input['cboTipoFormato']['value'];
    $resolucion->idUsuario = (int) $_SESSION['index']->id;
    $resolucion->ipOrdenador = generalObtenerIp();
    $resolucion->setIdApartado( (int) base64_decode(urldecode($input['idApartado'])));
    $resolucion->setTipoApartado( $input['tipoApartado'] );

    $respuesta = $resolucion->guardar( Helpers::formatGetLogs( $input['logs'] ) );

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );