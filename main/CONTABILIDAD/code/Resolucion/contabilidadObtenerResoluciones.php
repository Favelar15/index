<?php
header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['resoluciones' => [] ];

if (isset($_SESSION['index']) && $_SESSION["index"]->locked)
{

    require_once '../../../code/connectionSqlServer.php';
    require_once '../../../code/Permiso.php';

    require_once 'Models/TipoDocumento.php';
    require_once 'Models/Resolucion.php';

    $resolucion = new Resolucion();
    $resolucion->id = !empty( $_GET['id']) ? (int) $_GET['id'] : 0;
    $respuesta['resoluciones'] = $resolucion->obtenerResoluciones();

} else
{
    $respuesta['respuesta'] = 'SESION';
}


echo json_encode( $respuesta );
