<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if ( isset( $_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../../../code/Permiso.php';

    require_once 'Models/TipoDocumento.php';
    require_once 'Models/Resolucion.php';

    $resolucion = new Resolucion();
    $resolucion->id = $input['id'];
    $resolucion->idUsuario = $_SESSION['index']->id;
    $resolucion->ipOrdenador = generalObtenerIp();
    $resolucion->setIdApartado( base64_decode(urldecode($input['idApartado'])));
    $resolucion->setTipoApartado( $input['tipoApartado'] );
    $respuesta = $resolucion->eliminar();

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );
