<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";
        include "../../code/Permiso.php";

        $solicitados = explode("@@@", $_GET["q"]);

        if (in_array('agencias', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/agencia.php';
            $agencia = new agencia();
            $agencias = $agencia->getAgenciasCbo();

            $respuesta->{"agencias"} = $agencias;
        }

        if ( in_array('agenciasAsignadas', $solicitados) || in_array('all', $solicitados))
        {
            require_once '../../code/Models/agencia.php';
            $agencia = new agencia();
            $agencias = $agencia->getAgenciasCbo();

            $idsAgencia = [];

            foreach ($_SESSION['index']->agencias as $agencia )
            {
                array_push($idsAgencia, $agencia->id );
            }

            foreach ( $agencias as $key => $agencia )
            {
                if ( !in_array( $agencia->id, $idsAgencia ))
                {
                    unset( $agencias[$key]);
                }
            }
            $respuesta->{'agenciasAsignadas'} = $agencias;
        }

        if ( in_array('tiposDocumentosResolucion', $solicitados) || in_array('all', $solicitados) )
        {
            require_once "Resolucion/Models/TipoDocumento.php";
            $documentos = new TipoDocumento();
            $respuesta->{'tiposDocumentosResolucion'} = $documentos->obtenerTiposDocumentos();
        }

        if (in_array('tiposDocumento', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/tipoDocumento.php';
            $tipoDocumento = new tipoDocumento();
            $tiposDocumento = $tipoDocumento->obtenerTiposDocumento();

            $respuesta->{"tiposDocumento"} = $tiposDocumento;
        }

        if (in_array('geograficos', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/datosGeograficos.php';
            $geografico = new geografico();
            $datosGeograficos = $geografico->obtenerGeograficos();

            $respuesta->{"datosGeograficos"} = $datosGeograficos;
        }

        if (in_array('resolucionesHacienda', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Resolucion/Models/Resolucion.php';
            $resolucion = new Resolucion();
            $respuesta->{"resolucionesHacienda"} = $resolucion->obtenerCatResoluciones();
        }

        if (in_array('serviciosContabilidad', $solicitados) || in_array('all', $solicitados)) {
            require_once 'SujetoExcluido/Models/ServicioContabilidad.php';
            $servicio = new ServicioContabilidad();
            $respuesta->{"serviciosContabilidad"} = $servicio->obtenerServicios();
        }



        $conexion = null;
    }
}

echo json_encode($respuesta);