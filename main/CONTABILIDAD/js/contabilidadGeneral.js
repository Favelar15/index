
const gestionLogs = {

    set( id )
    {
        let campos = document.querySelectorAll(`form#${id} input.form-control`)
        campos = [ ...campos, ...document.querySelectorAll(`form#${id} select.selectpicker`) ]

        campos.forEach( campo => {
            if (campo.id) {
                if (campo.type == 'select-one') {
                    const value = campo.value ? campo.options[campo.selectedIndex].text : ''
                    $(`#${campo.id}`).data("valorInicial", value);
                } else {
                    $(`#${campo.id}`).data("valorInicial", campo.value);
                }
            }
        })

    },
    get( id )
    {
        let camposEditados = [];
        let campos = document.querySelectorAll(`form#${id} input.form-control`);

        campos = [...campos, ...document.querySelectorAll(`form#${id} select.form-control`)];

        campos.forEach(campo => {
            if (campo.id) {
                let valorActual = campo.type == 'select-one' ? campo.options[campo.selectedIndex].text : campo.value;

                if ($(`#${campo.id}`).data("valorInicial") || $(`#${campo.id}`).data("valorInicial") == '') {
                    let valorAnterior = $(`#${campo.id}`).data("valorInicial");

                    if (valorActual != valorAnterior) {
                        let label = $(`#${campo.id}`).parent().find("label").length > 0 ? $(`#${campo.id}`).parent().find("label:eq(0)").text() : ($(`#${campo.id}`).parent().parent().parent().find("label").length > 0 ? $(`#${campo.id}`).parent().parent().parent().find("label:eq(0)").text() : 'undefined');
                        label = (label.replace("*", "")).trim();
                        camposEditados.push({
                            campo: label,
                            valorAnterior,
                            valorNuevo: valorActual
                        })
                    }
                }
            }
        });

        return camposEditados
    },
    delete( id )
    {
        return new Promise( ( resolve, reject ) => {
            try {
                let campos = document.querySelectorAll(`form#${id} input.form-control`)
                campos = [ ...campos, ...document.querySelectorAll(`form#${id} select.selectpicker`) ]

                campos.forEach( campo => {
                    if (campo.id) {
                        $(`#${campo.id}`).removeData("valorInicial");
                    }
                })
                resolve()

            } catch (e) {
                reject(e.message)
            }
        })

    }

}

const habilitarFormulario = (idFormulario, estado = true, ignore = [] ) => {

    let campos = document.querySelectorAll(`form#${idFormulario} .form-control`)

    campos.forEach(campo => {
        if (campo.id) {
            if (campo.type == 'select-one') {
                if (!estado && !ignore.includes(campo.id) )
                {
                    campo.disabled = estado
                }

                if (campo.className.includes('selectpicker')) {
                    $(`#${campo.id}`).selectpicker('refresh')
                }

            } else {
                if (campo.className.includes('readonly'))
                {
                    campo.disabled = estado
                } else {
                    if ( !ignore.includes(campo.id) ) {
                        campo.readOnly = estado
                    }
                }
            }
        }
    });
}

const construirSujeto = ( id ) => {

    fetchActions.get({
        modulo, archivo: 'SujetoExcluido/contaSujetoPDF', params : { id }
    }).then( response => {

        switch (response.respuesta.trim()) {
            case 'EXITO':
                userActions.abrirArchivo(modulo, response.nombreArchivo, 'SujetosExcluidos', 'S');
                break;
            default:
                generalMostrarError(response);
                break;
        }
    }).catch( generalMostrarError)

}