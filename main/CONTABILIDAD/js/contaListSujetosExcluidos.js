
const modulo = 'CONTABILIDAD'
let tblSujetosExluidos

window.onload = () => {

    const inicializando = initDataTable().then( () => {
        return new Promise( ( resolve, reject ) => {

            try {

                fetchActions.get({
                    modulo, archivo: 'SujetoExcluido/contaObtenerSujetosExcluidos', params: []
                }).then( ( { sujetos = [] }  ) => {

                    sujetos.forEach( sujeto => {
                        configSujetos.contador++
                        configSujetos.sujetos[configSujetos.contador] = { ...sujeto }
                        configSujetos.agregarFila({
                            contador: configSujetos.contador, ...sujeto
                        })
                    })
                    tblSujetosExluidos.columns.adjust().draw()

                    resolve()

                }).catch(reject)

            } catch (e) {
                reject(e.message)
            }
        })
    })

    inicializando.then( () => {
        $('.preloader').fadeOut('fast')
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {

            if ($('#tblSujetosExluidos').length > 0) {
                tblSujetosExluidos = $('#tblSujetosExluidos').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0, 6],
                            "orderable": true,
                            className: 'text-center',
                            width: '8%'
                        },
                        {
                            targets: [2, 4, 5],
                            className: 'text-center',
                            width: '12%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSujetosExluidos.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}


const configSujetos = {
    contador: 0,
    idFila: 0,
    sujetos: {},
    id: 0,
    nuevoSujeto( id = 0 )
    {
        if ( tipoPermiso === 'ESCRITURA')
        {
            if ( id == 0 )
            {
                window.location = `?page=contaSujetosExcluidos&mod=${btoa(modulo)}`
            } else
            {
                window.location = `?page=contaSujetosExcluidos&mod=${btoa(modulo)}&id=${btoa(id)}`
            }
        } else
        {
            toastr.warning(`¡Modo solo lectura! No tienes permiso para crear sujetos excluidos`)
        }
    },
    agregarFila( { contador, numeroResolucion, correlativo, nombres, apellidos, agencia, fechaEmision } )
    {
        let btnEditar =`<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`;
        let btnEliminar = ``
        let btnPDF = `<button type="button" class="btnTbl" onclick="construirSujeto(${configSujetos.sujetos[contador].id})" title="Generar sujeto"><i class="fas fa-print"></i></button>`

        if ( tipoPermiso == 'ESCRITURA' )
        {
            btnEditar = `<button type="button" class="btnTbl" onclick="configSujetos.nuevoSujeto( ${configSujetos.sujetos[contador].id })" title="Editar sujeto"><i class="fas fa-edit"></i></button>`
            btnEliminar = `<button type="button" class="btnTbl" onclick="configSujetos.shoModalEliminar(${contador})" title="Anular sujeto"><i class="fas fa-trash-alt"></i></button>`
        }

        tblSujetosExluidos.row.add([
            contador, numeroResolucion, correlativo, `${nombres} ${apellidos}`, agencia, fechaEmision,
            `<div class="tblButtonContainer"> ${btnEditar} ${btnPDF} ${btnEliminar}</div>`
        ]).node().id = `trSujetos${contador}`
    },
    shoModalEliminar( contador )
    {
        formActions.clean('formAnularSujeto')
            .then( () => {

                this.id = this.sujetos[contador].id
                document.getElementById('title-eliminar').innerHTML = `Anular sujeto | ${this.sujetos[contador].agencia}`
                document.getElementById('txtFechaEmision').value = this.sujetos[contador].fechaEmision
                document.getElementById('txtNumeroResolucion').value = this.sujetos[contador].numeroResolucion
                document.getElementById('txtCorrelativo').value = this.sujetos[contador].correlativo
                document.getElementById('txtProveedor').value = `${this.sujetos[contador].nombres} ${this.sujetos[contador].apellidos}`

                $(`#modal-eliminar`).modal('show')

            }).catch( generalMostrarError )

    },
    eliminar()
    {
        formActions.validate('formAnularSujeto')
            .then( ( { errores } ) => {

                if ( errores == 0 )
                {
                    fetchActions.set({
                        modulo, archivo: 'SujetoExcluido/contaAnularSujeto', datos: {
                            ...formActions.getJSON('formAnularSujeto'),
                            idApartado, tipoApartado, id: this.id
                        }
                    }).then( response => {
                        switch (response.respuesta.trim()) {
                            case 'EXITO':

                                $(`#modal-eliminar`).modal('hide')

                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: '¡Bien hecho!',
                                    text: 'Sujeto excluido anulado con exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then( () => {
                                    delete this.sujetos[this.id];
                                    tblSujetosExluidos.row(`#trSujetos${this.id}`).remove().draw();
                                });
                                break;
                            default:
                                generalMostrarError(response);
                                break;
                        }
                    }).catch( generalMostrarError )
                }
            }).catch( generalMostrarError )
    }
}