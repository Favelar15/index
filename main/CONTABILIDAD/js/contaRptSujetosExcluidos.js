let tblSujetosExluidos
const modulo = 'CONTABILIDAD'

window.onload = () => {

    const cboAgencia = document.getElementById('cboAgencia')

    const inicializando = initDataTable().then(() => {
        return new Promise( ( resolve, reject) => {
            try {
                fetchActions.getCats({
                    modulo, archivo: 'contabilidadGetCats', solicitados: ['agenciasAsignadas']
                }).then( ( { agenciasAsignadas = {} } ) => {

                    let opciones = [`<option value="" selected>Todas la agencias</option>`]

                    if ( cboAgencia )
                    {
                        for( const key in agenciasAsignadas )
                        {
                            opciones.push(`<option value="${agenciasAsignadas[key].id}">${agenciasAsignadas[key].nombreAgencia}</option>`)
                        }
                        cboAgencia.innerHTML = opciones.join('')
                        $(`#${cboAgencia.id}`).selectpicker('refresh')
                    }
                    resolve()
                }).catch( error => {
                   throw new Error(error)
                })
            } catch(e)
            {
                reject(e.message)
            }
        })
    })

    inicializando.then( () => {
        $('.preloader').fadeOut('fast')
    }).catch( generalMostrarError )
}


const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {

            if ($('#tblSujetosExluidos').length > 0) {
                tblSujetosExluidos = $('#tblSujetosExluidos').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0, 7],
                            "orderable": true,
                            className: 'text-center',
                            width: '8%'
                        },
                        {
                            targets: [2, 4, 5],
                            className: 'text-center',
                            width: '12%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSujetosExluidos.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const configRptSujetosExcluidos = {
    contador: 0,
    sujetos: {},
    cboAgencia: document.getElementById('cboAgencia'),
    txtInicio: document.getElementById('txtInicio'),
    txtFin: document.getElementById('txtFin'),
    btnExcel: document.getElementById('btnExcel'),
    generar()
    {
        formActions.validate('formRptSujetos')
            .then( ( { errores } ) => {

                if ( errores == 0 )
                {
                    fetchActions.set({
                        modulo, archivo: 'SujetoExcluido/contaRptSujetosExcluidos', datos: {
                            ...formActions.getJSON('formRptSujetos'),
                            accion: 'GENERAR'
                        }
                    }).then( ( { sujetos = [] } ) => {

                        this.contador = 0
                        tblSujetosExluidos.clear()

                        if ( sujetos.length > 0 )
                        {
                            this.cboAgencia.disabled = true
                            $(`#${this.cboAgencia.id}`).selectpicker('refresh')
                            this.txtInicio.disabled = true
                            this.txtFin.disabled = true
                            this.btnExcel.classList.remove('disabled')
                        }

                        sujetos.forEach( sujeto => {
                            this.contador++
                            this.sujetos[this.contador] = { ...sujeto }
                            this.agregarFila({
                                contador: this.contador, ...sujeto
                            })
                        })
                        tblSujetosExluidos.columns.adjust().draw()
                    }).catch( generalMostrarError )
                }
            }).catch( generalMostrarError )
    },
    exportar()
    {

    },
    agregarFila( { contador, numeroResolucion, correlativo, nombres, apellidos, agencias, fechaEmision, usuario } )
    {
        const btnDetalle = `<button type="button" class="btnTbl" onclick="void(0)"><i class="fas fa-eye"></i></button>`

        tblSujetosExluidos.row.add([
            contador, numeroResolucion, correlativo, `${nombres} ${apellidos}`, fechaEmision, agencias, usuario,
            `<div class="tblButtonContainer">${btnDetalle}</div>`
        ]).node().id = `trSujetoExcluidos${contador}`
    },
    changeFechaInicio()
    {

    },
    limpiar()
    {
        formActions.clean('formRptSujetos')
            .then( () => {

                this.cboAgencia.disabled = false
                $(`#${this.cboAgencia.id}`).selectpicker('refresh')

                this.txtInicio.disabled = false
                this.txtFin.disabled = false

                this.btnExcel.classList.add('disabled')

                tblSujetosExluidos.clear().draw()
                this.sujetos = {}
                this.contador = 0

            }).catch( generalMostrarError )
    }
}
