let tblDetalle
const modulo = 'CONTABILIDAD'

window.onload = () => {

    const cboTipoDocumento = document.getElementById('cboTipoDocumento')
    const cboPais = document.getElementById('cboPais')
    const cboServicios = document.getElementById('cboTipoServicio')

    const cboResoluciones = document.getElementById('cboResolucion')

    document.getElementById('formProveedor').reset()
    document.getElementById('formSujeto').reset()


    const inicializando = initDataTable().then(() => {
        return new Promise((resolve, reject) => {
            try {
                fetchActions.getCats({
                    modulo,
                    archivo: 'contabilidadGetCats',
                    solicitados: ['tiposDocumento', 'geograficos', 'resolucionesHacienda', 'serviciosContabilidad']
                }).then(({
                             tiposDocumento: documentos,
                             datosGeograficos: geograficos,
                             resolucionesHacienda = [],
                             serviciosContabilidad = []
                         }) => {

                    if (cboTipoDocumento) {
                        let opciones = [`<option value="" selected disabled>Seleccione</option>`]

                        documentos.forEach(documento => {
                            tiposDocumento[documento.id] = {
                                ...documento
                            }

                            if (documento.tipoDocumento == 'DUI' || documento.tipoDocumento == 'Pasaporte') {
                                opciones.push(` <option value="${documento.id}">${documento.tipoDocumento}</option> `)
                            }
                        });

                        cboTipoDocumento.innerHTML = opciones.join('');
                        $(`#${cboTipoDocumento.id}`).selectpicker('refresh');
                    }

                    if (cboResoluciones) {
                        let opciones = [`<option value="" selected disabled>Seleccione</option>`]

                        resolucionesHacienda.forEach(resolucion => {
                            opciones.push(`<option value="${resolucion.id}">${resolucion.numeroResolucion} | ${resolucion.correlativoInicial} - ${resolucion.correlativoFinal}</option>`)
                        })

                        cboResoluciones.innerHTML = opciones.join('')
                        $(`#${cboResoluciones.id}`).selectpicker('refresh')
                    }

                    if (cboPais) {
                        datosGeograficos = {
                            ...geograficos
                        };

                        let opciones = [`<option value="" selected disabled>Seleccione</option>`]

                        for (let key in geograficos) {
                            opciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
                        }

                        cboPais.innerHTML = opciones.join('');
                        $(`#${cboPais.id}`).selectpicker('refresh');
                    }

                    if (cboServicios) {
                        let opciones = [`<option value="" selected disabled>Seleccione</option>`]

                        serviciosContabilidad.forEach(servicio => {
                            opciones.push(`<option value="${servicio.id}">${servicio.servicio}</option>`)
                        })

                        cboServicios.innerHTML = opciones.join('');
                        $(`#${cboServicios.id}`).selectpicker('refresh');
                    }


                }).catch(generalMostrarError)

                resolve()
            } catch (e) {
                reject(e.message)
            }

        })
    })

    inicializando.then(editar)
}

const editar = () => {
    return new Promise( ( resolve, reject ) => {
        try {
            const id = atob(getParameterByName('id'))

            if ( id && parseInt(id) > 0 )
            {
                fetchActions.get({
                    modulo, archivo: 'SujetoExcluido/contaObtenerSujetosExcluidos', params: {id}
                }).then( ( { sujetos = {} } ) => {

                    this.cboTipoDocumento.disabled = true
                    this.txtDocumentoPrimario.readOnly = true
                    this.btnBuscarProveedor.classList.add('disabled')
                    this.btnAgregarSujeto.classList.remove('disabled')

                    configSujeto.id = parseInt(sujetos.id)

                    document.getElementById('formProveedor').action = 'javascript:configSujeto.guardar();';

                    document.getElementById('txtCodigoCliente').value = sujetos.codigoCliente
                    document.getElementById('cboTipoDocumento').value = sujetos.idTipoDocumento
                    document.getElementById('txtDocumentoPrimario').value = sujetos.numeroDocumento

                    document.getElementById('txtNombres').value = sujetos.nombres
                    document.getElementById('txtApellidos').value = sujetos.apellidos

                    document.getElementById('txtNit').value = sujetos.nit
                    document.getElementById('txtEmail').value = sujetos.email
                    document.getElementById('txtTelefonoFijo').value = sujetos.telefonoFijo
                    document.getElementById('txtTelefonoMovil').value = sujetos.telefonoMovil

                    document.getElementById('cboPais').value = sujetos.idPais

                    $("#cboPais").trigger("change");
                    document.getElementById('cboDepartamento').value = sujetos.idDepartamento
                    $("#cboDepartamento").trigger("change");
                    document.getElementById('cboMunicipio').value = sujetos.idMunicipio
                    document.getElementById('txtDireccion').value = sujetos.direccion

                    habilitarFormulario('formProveedor', false, ['txtDocumentoPrimario', 'cboTipoDocumento', 'txtCodigoCliente'])

                    document.getElementById('txtFechaEmision').value = sujetos.fechaEmision
                    $(`#txtFechaEmision`).datepicker('update')

                    document.getElementById('cboResolucion').value = sujetos.idResolucion
                    document.getElementById('txtCorrelativo').value = sujetos.correlativo
                    document.getElementById('cboTipoServicio').value = sujetos.idTipoServicio

                    document.getElementById('cboRenta').value = sujetos.renta

                    const cboTipoServicio = document.getElementById('cboTipoServicio')

                    $('#cboTipoServicio').selectpicker('refresh')

                    cboTipoServicio.value = cboTipoServicio.value ?? '';

                    if ( cboTipoServicio.options[cboTipoServicio.selectedIndex].innerHTML == 'OTROS')
                    {
                        console.log(cboTipoServicio.options[cboTipoServicio.selectedIndex].innerHTML)
                        console.log('change')
                        $(`#cboTipoServicio`).trigger('change')
                    } else {
                        document.getElementById('cboIva').value = sujetos.iva
                    }

                    const cboRenta = document.getElementById('cboRenta')
                    const cboAsumimosRenta = document.getElementById('cboAsumimosRenta')

                    if ( cboRenta.options[cboRenta.selectedIndex].innerHTML == 'SI')
                    {
                        cboAsumimosRenta.disabled = false
                        cboAsumimosRenta.value = sujetos.seAsumeRenta
                    }
                    habilitarFormulario('formSujeto', false, ['cboResolucion', 'cboAsumimosRenta', 'txtCorrelativo'])
                    gestionLogs.set('formProveedor')
                    gestionLogs.set('formSujeto')

                    sujetos.detalle.forEach( detalle => {

                        configDetalleSujeto.contador++

                        const subTotal = (parseInt(detalle.cantidad) * Number(detalle.precio)).toFixed(2)
                        detalle.precio = parseFloat(detalle.precio).toFixed(2)

                        configDetalleSujeto.detalles[configDetalleSujeto.contador] = {
                            formulario: {...detalle, subTotal},
                            db: { ...detalle, subTotal},
                            logs: []
                        }

                        configDetalleSujeto.agregarFila({
                            contador: configDetalleSujeto.contador,
                            ...detalle, subTotal
                        })
                    })

                    tblDetalle.columns.adjust().draw()

                }).catch( generalMostrarError )
                resolve()
            }
        } catch (e) {
            reject(e.message)
        }
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {

            if ($('#tblDetalle').length > 0) {
                tblDetalle = $('#tblDetalle').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0, 5],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [2, 3, 4],
                            className: 'text-center',
                            width: '15%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblDetalle.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const buscarProveedor = {
    btnBuscarProveedor: document.getElementById('btnBuscarProveedor'),
    cboTipoDocumento: document.getElementById('cboTipoDocumento'),
    txtDocumentoPrimario: document.getElementById('txtDocumentoPrimario'),
    btnAgregarSujeto: document.getElementById('btnAgregarSujeto'),
    buscar() {
        const {
            cboTipoDocumento: { labelOption: tipoDocumento,  value: idTipoDocumento}, txtDocumentoPrimario: {value: numeroDocumento}
        } = formActions.getJSON('formProveedor')

        if (idTipoDocumento && numeroDocumento) {
            fetchActions.set({
                modulo, archivo: 'SujetoExcluido/contaBuscarProveedor', datos: {
                    idTipoDocumento, numeroDocumento
                }
            }).then( ( {proveedor: {Persona, existe} } ) => {

                this.cboTipoDocumento.disabled = true
                this.txtDocumentoPrimario.readOnly = true
                this.btnBuscarProveedor.classList.add('disabled')
                this.btnAgregarSujeto.classList.remove('disabled')

                document.getElementById('formProveedor').action = 'javascript:configSujeto.guardar();';

                if (existe) {
                    document.getElementById('txtCodigoCliente').value = Persona.codigoCliente
                    document.getElementById('cboTipoDocumento').value = Persona.idTipoDocumento ? Persona.idTipoDocumento : Persona.tipoDocumentoPrimario
                    document.getElementById('txtDocumentoPrimario').value = Persona.numeroDocumento ? Persona.numeroDocumento : Persona.numeroDocumentoPrimario

                    document.getElementById('txtNombres').value = Persona.nombres ? Persona.nombres : Persona.nombresAsociado;
                    document.getElementById('txtApellidos').value = Persona.apellidos ? Persona.apellidos : Persona.apellidosAsociado;

                    document.getElementById('txtNit').value = Persona.nit ? Persona.nit : Persona.numeroDocumentoSecundario;
                    document.getElementById('txtEmail').value = Persona.email;
                    document.getElementById('txtTelefonoFijo').value = Persona.telefonoFijo;
                    document.getElementById('txtTelefonoMovil').value = Persona.telefonoMovil;

                    document.getElementById('cboPais').value = Persona.pais ? Persona.pais : Persona.idPais;
                    $("#cboPais").trigger("change");
                    document.getElementById('cboDepartamento').value = Persona.departamento ? Persona.departamento : Persona.idDepartamento;
                    $("#cboDepartamento").trigger("change");
                    document.getElementById('cboMunicipio').value = Persona.municipio ? Persona.municipio : Persona.idMunicipio;
                    document.getElementById('txtDireccion').value = Persona.direccionCompleta ? Persona.direccionCompleta : Persona.direccion;

                    habilitarFormulario('formProveedor', false, ['txtDocumentoPrimario', 'cboTipoDocumento', 'txtCodigoCliente'])
                    habilitarFormulario('formSujeto', false, ['cboIva', 'cboAsumimosRenta', 'txtCorrelativo'])
                    gestionLogs.set('formProveedor')
                } else
                {
                    Swal.fire({
                        icon: 'warning',
                        title: '¡Información!',
                        allowOutsideClick: false,
                        html: `No se encontro registros del proveedor con <strong>${tipoDocumento} : ${numeroDocumento}</strong>, ¡Completa los datos del proveedor!`,
                    }).then( result => {

                        if ( result.isConfirmed )
                        {
                            habilitarFormulario('formProveedor', false, ['txtDocumentoPrimario', 'cboTipoDocumento', 'txtCodigoCliente'])
                            habilitarFormulario('formSujeto', false, ['cboIva', 'cboAsumimosRenta', 'txtCorrelativo'])
                        }
                    })
                }

            }).catch(generalMostrarError)

        } else {
            toastr.warning(`Tipo de documento ó numero de documento no son valido`)
        }
    },
    selectTipoDocumento(tipoDocumento) {
        this.cboTipoDocumento.disabled = true
        this.btnBuscarProveedor.classList.remove('disabled')
        generalMonitoreoTipoDocumento(tipoDocumento, 'txtDocumentoPrimario')

        $(`#cboTipoDocumento`).selectpicker('refresh')
    }
}

const configSujeto = {
    id: 0,
    guardar() {
        let formValido = true;

        if (formActions.manualValidate('formProveedor') && formActions.manualValidate('formSujeto'))
        {
            if (!Object.keys(configDetalleSujeto.detalles).length > 0) {

                toastr.warning('Agrege al menos un detalle para el sujeto')
                formValido = false;
            }
        } else {
            formValido = false
        }

        if ( formValido )
        {
            fetchActions.set({
                modulo, archivo: 'SujetoExcluido/contaGuardarSujetoExcluido', datos: {
                    proveedor: {...formActions.getJSON('formProveedor')},
                    sujetosExcluidos: { idSujeto: this.id, ...formActions.getJSON('formSujeto')},
                    detalle: {...configDetalleSujeto.detalles},
                    logsProveedor: gestionLogs.get('formProveedor'),
                    logsSujeto: gestionLogs.get('formSujeto'),
                    idApartado, tipoApartado
                }
            }).then( response => {
                switch (response.respuesta.trim()) {
                    case 'EXITO':

                        this.id = response.idDb

                        Swal.fire({
                            title: 'INFORMACIÓN',
                            html: `El sujeto excluido se genero con exito ¿Seleccione una opción?`,
                            icon: 'success',
                            allowOutsideClick: false,
                            showCloseButton: true,
                            showCancelButton: true,
                            showDenyButton: true,
                            denyButtonText: `<i class="fas fa-sign-in-alt"></i> Salir`,
                            confirmButtonColor: '#46b1c3',
                            confirmButtonText: `<i class="fas fa-print"></i> Generar Sujeto`,
                            cancelButtonText: `<i class="fas fa-file"></i> Seguir editando`
                        }).then( result => {
                            if ( result.isConfirmed )
                            {
                                construirSujeto(this.id)
                            } else if ( result.isDenied )
                            {
                                window.location = `?page=contaListSujetosExcluidos&mod=${btoa(modulo)}`
                            } else {
                                window.location = `?page=contaSujetosExcluidos&mod=${btoa(modulo)}&id=${btoa(this.id)}`
                            }
                        })
                        break;
                    case 'ERROR':
                        Swal.fire({
                            icon: 'error',
                            title: '¡Ups. Algo salio mal!',
                            text: response.mensaje,
                        })
                        break;
                    default:
                        generalMostrarError(response.respuesta);
                        break;
                }
            }).catch( generalMostrarError )
        }
    },
    cancelar()
    {
        Swal.fire({
            title: 'IMPORTANTE',
            html: `¿Deseas salir del formulario? Los datos ingresados se perderán`,
            icon: 'warning',
            allowOutsideClick: false,
            showCloseButton: true,
            showCancelButton: true,
            showDenyButton: true,
            denyButtonText: `<i class=" fas fa-trash-alt"></i> Limpiar`,
            confirmButtonColor: '#46b1c3',
            confirmButtonText: `<i class="fas fa-thumbs-up"></i> Sí`,
            cancelButtonText: `<i class="fas fa-thumbs-down"></i> No`
        }).then( result => {
            if ( result.isConfirmed )
            {
                window.location = `?page=contaListSujetosExcluidos&mod=${btoa(modulo)}`

            } else if ( result.isDenied )
            {
                window.location = `?page=contaSujetosExcluidos&mod=${btoa(modulo)}`
            }
        })
    }
}

const configDetalleSujeto = {
    contador: 0,
    idFila: 0,
    detalles: {},
    mostrarModal(idFila = 0) {
        this.idFila = idFila

        formActions.clean('formDetalleSujeto')
            .then(() => {

                gestionLogs.delete('formDetalleSujeto').then( () => {

                    if (this.idFila > 0) {

                        document.getElementById('txtDescripcion').value = this.detalles[this.idFila].formulario.descripcion
                        document.getElementById('txtCantidad').value = this.detalles[this.idFila].formulario.cantidad
                        document.getElementById('txtPrecio').value = this.detalles[this.idFila].formulario.precio

                        if ( Object.keys(this.detalles[this.idFila].db).length > 0 )
                        {
                            $(`#txtDescripcion`).data("valorInicial", this.detalles[this.idFila].db.descripcion);
                            $(`#txtCantidad`).data("valorInicial", this.detalles[this.idFila].db.cantidad);
                            $(`#txtPrecio`).data("valorInicial", this.detalles[this.idFila].db.precio);
                        }
                    }

                    $(`#modal-detalleSujeto`).modal('show')
                })
            }).catch(generalMostrarError)

    },
    guardar() {
        formActions.validate('formDetalleSujeto')
            .then(({errores}) => {

                if (errores == 0) {
                    let {
                        txtDescripcion: {value: descripcion},
                        txtCantidad: {value: cantidad},
                        txtPrecio: {value: precio}
                    } = formActions.getJSON('formDetalleSujeto')

                    const subTotal = (parseInt(cantidad) * Number(precio)).toFixed(2)
                    precio = parseFloat(precio).toFixed(2)

                    if (this.idFila == 0) {
                        this.contador++
                        this.detalles[this.contador] = {
                            formulario: { id: 0, descripcion, cantidad, precio, subTotal, habilitado: 'S'},
                            db: { },
                            logs: []
                        }

                        this.agregarFila({
                            contador: this.contador, descripcion, cantidad, precio, subTotal
                        })

                        tblDetalle.columns.adjust().draw()
                    } else {
                        this.detalles[this.idFila].formulario = {... this.detalles[this.idFila].formulario, descripcion, cantidad, precio, subTotal }
                        this.detalles[this.idFila].logs = [...gestionLogs.get('formDetalleSujeto') ]

                        this.editarFila(this.idFila, {
                            descripcion, cantidad, precio, subTotal
                        })
                    }
                }

                $(`#modal-detalleSujeto`).modal('hide')

            }).catch(generalMostrarError)
    },
    agregarFila({contador, descripcion, cantidad, precio, subTotal}) {
        const btnEditar = `<button class="btnTbl" onclick="configDetalleSujeto.mostrarModal(${contador})"><i class="fas fa-edit"></i></button>`
        const btnEliminar = `<button class="btnTbl" onclick="configDetalleSujeto.eliminar(${contador})"><i class="fas fa-trash-alt"></i></button>`

        tblDetalle.row.add([
            contador, descripcion, cantidad, `$${precio}`, `$${subTotal}`,
            `<div class="tblButtonContainer"> ${btnEditar} ${btnEliminar} </div>`
        ]).node().id = `trDetalle${contador}`
    },
    editarFila(idFila, {descripcion, cantidad, precio, subTotal}) {
        $(`#trDetalle${idFila}`).find("td:eq(1)").html(descripcion);
        $(`#trDetalle${idFila}`).find("td:eq(2)").html(cantidad);
        $(`#trDetalle${idFila}`).find("td:eq(3)").html(`$${precio}`);
        $(`#trDetalle${idFila}`).find("td:eq(4)").html(`$${subTotal}`);
    },
    eliminar(idFila) {
        Swal.fire({
            title: 'IMPORTANTE',
            html: `¿Deseas eliminar el detalle del sujeto?`,
            icon: 'warning',
            allowOutsideClick: false,
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            confirmButtonText: `<i class="fas fa-thumbs-up"></i> Eliminar`,
            cancelButtonText: `<i class="fas fa-thumbs-down"></i> Cancelar`
        }).then(result => {

            if (result.isConfirmed) {
                tblDetalle.row(`#trDetalle${idFila}`).remove().draw()
                this.detalles[idFila].formulario.habilitado = 'N'
            }
        })
    }
}

const configTipoServicio = {
    cboIva: document.getElementById('cboIva'),
    cboTipoServicio: document.getElementById('cboTipoServicio'),
    change() {
        this.cboIva.value = ''

        if (this.cboTipoServicio.options[this.cboTipoServicio.selectedIndex].text === 'Transporte y alquileres') {
            this.cboIva.value = ''
            this.cboIva.disabled = false

        } else if (this.cboTipoServicio.options[this.cboTipoServicio.selectedIndex].text === 'Otros') {
            this.cboIva.value = 'N'
            this.cboIva.disabled = true
        }

        this.cboIva.disabled = this.cboTipoServicio.options[this.cboTipoServicio.selectedIndex].text === 'Transporte y alquileres' ? false : true
        $(`#${this.cboIva.id}`).selectpicker('refresh')
    }
}

const configRenta = {
    cboRenta: document.getElementById('cboRenta'),
    cboAsumimosRenta: document.getElementById('cboAsumimosRenta'),
    cboIva: document.getElementById('cboIva'),
    change() {
        this.cboAsumimosRenta.value = ''

        if ( this.cboRenta.value == 'S')
        {
            this.cboAsumimosRenta.disabled = false;

        } else if (this.cboRenta.value == 'N')
        {
            this.cboAsumimosRenta.value = 'N'
        }

        $(`#${this.cboAsumimosRenta.id}`).selectpicker('refresh')
    },
    calcular() {
        let suma = 0, renta = 0, iva = 0;

        if (Object.keys(configDetalleSujeto.detalles).length > 0) {
            for (const key in configDetalleSujeto.detalles) {
                suma += Number(configDetalleSujeto.detalles[key].subTotal)
            }
        }

        if (this.cboRenta.value == 'S' && this.cboAsumimosRenta.value == 'S') {
            renta = Number(suma + (Number(suma) / 9) - suma).toFixed(2)

        } else if (this.cboRenta.value == 'S' && this.cboAsumimosRenta.value == 'N') {
            renta = Number(suma * 0.10).toFixed(2)
        } else {
            renta = renta.toFixed(2)
        }

        let sumaMasRenta = (Number(suma) + Number(renta)).toFixed(2)
        document.getElementById('txtRentaRetenido').value = renta

        if (this.cboIva.value && this.cboIva.value === 'S') {
            iva = Number(sumaMasRenta * 0.13).toFixed(2)
        }

        document.getElementById('txtSuma').value = sumaMasRenta
        document.getElementById('txtIva').value = iva
        document.getElementById('txtTotal').value = Number(suma).toFixed(2)
    }
}

const configCorrelativo = {
    txtCorrelativo: document.getElementById('txtCorrelativo'),
    cboResolucion: document.getElementById('cboResolucion'),
    btnComprobarCorrelativo: document.getElementById('btnComprobarCorrelativo'),
    comprobar() {
        if ( this.txtCorrelativo.value )
        {
            fetchActions.set({
                modulo, archivo: 'SujetoExcluido/contaComprobarCorrelativo', datos: {
                    idResolucion: this.cboResolucion.value,
                    correlativo: this.txtCorrelativo.value,
                    idSujeto: configSujeto.id
                }
            }).then( ( { respuesta: {  respuesta, valido } } ) => {

                if ( respuesta == 'EXITO' && valido )
                {
                    toastr.success(`Correlativo Valido`)
                } else
                {
                    toastr.warning(respuesta)
                }

            }).catch( generalMostrarError )
        } else
        {
            toastr.warning(`Ingrese el correlativo del sujeto`)
        }
    },
    onChangeResolucion()
    {
        if ( this.cboResolucion.value )
        {
            this.txtCorrelativo.value = ''

            if (this.txtCorrelativo.readOnly )
            {
                this.txtCorrelativo.readOnly = false
            }

            if ( this.btnComprobarCorrelativo.className.includes('disabled'))
            {
                this.btnComprobarCorrelativo.classList.remove('disabled')
            }
        }
    }
}
