
let tblResoluciones
const modulo = 'CONTABILIDAD'

window.onload = () => {

    const cboAgencia = document.getElementById('cboAgencia')
    const cboTipoDocumentoHacienda = document.getElementById('cboTipoDocumentoHacienda')

    const inicializando = initDataTable().then( () => {
        return new Promise( ( resolve, reject) => {

            fetchActions.getCats({
                modulo, archivo: 'contabilidadGetCats', solicitados: ['agencias', 'tiposDocumentosResolucion']
            }).then( ( { agencias = [], tiposDocumentosResolucion = [] } ) => {

                let opciones = [`<option value="" disabled selected>Seleccione</option>`]

                agencias.forEach( agencia => {
                    opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                })

                cboAgencia.innerHTML = opciones.join()
                $(`#${cboAgencia.id}`).selectpicker('refresh')

                opciones = [`<option value="" disabled selected>Seleccione</option>`]

                tiposDocumentosResolucion.forEach( documento => {
                    opciones.push(`<option value="${documento.id}">${documento.tipoDocumento}</option>`)
                })

                cboTipoDocumentoHacienda.innerHTML = opciones.join()
                $(`#${cboTipoDocumentoHacienda.id}`).selectpicker('refresh')

                resolve()

            }).catch( reject )
        })
    })

    inicializando.then( () => {
        configResoluciones.cargarResoluciones()
        $('.preloader').fadeOut('fast')
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblResoluciones').length > 0) {
                tblResoluciones = $('#tblResoluciones').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            className: 'text-center',
                            width: '8%'
                        },
                        {
                            "targets": [1],
                            width: '15%'
                        },
                        {
                            "targets": [4],
                            width: '9%',
                            className: 'text-center'
                        },
                        {
                            "targets": [5],
                            width: '25%'
                        },
                        {
                            targets: [6],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblResoluciones.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const configResoluciones = {
    resoluciones: {},
    contador: 0,
    idFila: 0,
    tabFormulario: document.getElementById('formulario-tab'),
    tabResoluciones: document.getElementById('resoluciones-tab'),
    cargarResoluciones()
    {
        this.contador = 0
        this.resoluciones = {}

        fetchActions.get({
            modulo, archivo: 'Resolucion/contabilidadObtenerResoluciones', params: {}
        }).then( ( { resoluciones = [] } ) => {

            resoluciones.forEach( resolucion => {
                this.contador++
                this.resoluciones[this.contador] = { ...resolucion }
                let progreso = configCorrelativos.calcularProgreso({ ...resolucion } )
                this.agregarFila( {
                    contador: this.contador, ...resolucion, progreso
                })
            })
            tblResoluciones.columns.adjust().draw()

        }).catch( generalMostrarError )
    },
    mostrarModal( idFila = 0 ) {

        this.idFila = idFila

        formActions.clean('formResolución')
            .then ( () => {

                document.getElementById('txtNumeroResolucion').readOnly = false
                document.getElementById('txtInicio').readOnly = false
                document.getElementById('txtFin').readOnly = false

                if ( idFila > 0 )
                {
                    document.getElementById('txtNumeroResolucion').value = this.resoluciones[this.idFila].numeroResolucion
                    document.getElementById('cboTipoDocumentoHacienda').value = this.resoluciones[this.idFila].TipoDocumentoHacienda.id
                    document.getElementById('cboAgencia').value = this.resoluciones[this.idFila].idAgencia
                    document.getElementById('txtFechaAprobacion').value = this.resoluciones[this.idFila].fechaAprobacion
                    $(`#txtFechaAprobacion`).datepicker('update')

                    document.getElementById('txtInicio').value = this.resoluciones[this.idFila].correlativoInicial
                    document.getElementById('txtFin').value = this.resoluciones[this.idFila].correlativoFinal
                    document.getElementById('txtDisponibles').value = this.resoluciones[this.idFila].disponible
                    document.getElementById('cboTipoFormato').value = this.resoluciones[this.idFila].tipoFormato

                    const selects = document.querySelectorAll('form#formResolución select.selectpicker')
                    selects.forEach( select => {
                        $(`#${select.id}`).selectpicker('refresh')
                    })

                    document.getElementById('txtNumeroResolucion').readOnly = true
                    document.getElementById('txtInicio').readOnly = true
                    document.getElementById('txtFin').readOnly = true

                    gestionLogs.set('formResolución')
                }

                $('#resolucion-modal').modal('show')
            })
    },
    guardar()
    {
        const id = this.idFila == 0 ? 0 : this.resoluciones[this.idFila].id

        formActions.validate('formResolución')
            .then( ( { errores } ) => {

                const {
                    txtNumeroResolucion : { value: numeroResolucion },
                    txtInicio: { value: correlativoInicial },
                    txtFin: { value: correlativoFinal }
                } = formActions.getJSON('formResolución')

                let existe = false

                if ( Object.keys( this.resoluciones).length > 0  )
                {
                    for ( const key in this.resoluciones )
                    {
                        if ( this.resoluciones[key].numeroResolucion == numeroResolucion && this.idFila != key )
                        {
                            existe = true
                            break
                        }
                    }
                }

                if ( ! existe )
                {
                    if ( validadCorrelativos( correlativoInicial ) && validadCorrelativos( correlativoFinal )
                        && validadCorrelativos( correlativoInicial ) < validadCorrelativos( correlativoFinal ) )
                    {
                        if ( errores == 0 )
                        {
                            fetchActions.set({
                                modulo, archivo: 'Resolucion/contabilidadGuardarResolucion', datos: {
                                    idApartado, tipoApartado,
                                    id: id,
                                    ...formActions.getJSON('formResolución'), logs: [...gestionLogs.get('formResolución') ]
                                }
                            }).then( response => {

                                switch (response.respuesta.trim()) {
                                    case 'EXITO':

                                        $(`#resolucion-modal`).modal('hide')

                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: '¡Bien hecho!',
                                            text: 'Los datos de la resolución fueron registrados exitosamente',
                                            showConfirmButton: false,
                                            timer: 1500
                                        }).then(() => {

                                            const {
                                                txtNumeroResolucion: { value: numeroResolucion },
                                                cboTipoDocumentoHacienda: { value: idTipoDocumento, labelOption: tipoDocumento },
                                                cboAgencia: { value: idAgencia, labelOption: agencia },
                                                txtFechaAprobacion: { value: fechaAprobacion },
                                                txtInicio: { value : correlativoInicial },
                                                txtFin: { value: correlativoFinal },
                                                txtDisponibles: { value: disponible },
                                                cboTipoFormato: { value: tipoFormato}
                                            } = formActions.getJSON('formResolución')

                                            if ( this.idFila == 0 )
                                            {
                                                this.contador++
                                            } else
                                            {
                                                this.contador = this.idFila
                                            }

                                            const datos  = {
                                                numeroResolucion,
                                                TipoDocumentoHacienda: { id: idTipoDocumento, tipoDocumento },
                                                agencia, idAgencia, fechaAprobacion,
                                                correlativoInicial, correlativoFinal, disponible,
                                                tipoFormato,
                                            }


                                            if ( this.idFila == 0 )
                                            {
                                                datos.id =  response.id
                                                datos.fechaRegistro = response.fechaRegistro
                                                this.resoluciones[this.contador] = { ...datos }
                                                let progreso = configCorrelativos.calcularProgreso({ ...datos } )
                                                this.agregarFila( {
                                                    contador: this.contador, ...this.resoluciones[this.contador], progreso
                                                })

                                                tblResoluciones.columns.adjust().draw()

                                            } else
                                            {
                                                this.resoluciones[this.contador] = {...this.resoluciones[this.contador], ...datos }
                                                let progreso = configCorrelativos.calcularProgreso({ ...datos } )
                                                this.editarFila(this.idFila , {
                                                    ...this.resoluciones[this.contador], progreso
                                                })
                                            }
                                        })
                                        break;
                                    default:
                                        generalMostrarError(response);
                                        break;
                                }

                            }).catch( generalMostrarError )
                        }
                    } else
                    {
                        toastr.warning('Error al procesar los valores de los correlativos')
                    }
                } else
                {
                    document.getElementById('txtNumeroResolucion').focus()
                    toastr.warning(`El número de la resolución de hacienda ya existe`)
                }

            })
    },
    agregarFila( { contador, numeroResolucion, TipoDocumentoHacienda: { tipoDocumento }, agencia, fechaAprobacion, fechaRegistro, progreso} )
    {
        let btnEditar =`<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`;
        let btnEliminar = ``

        if ( tipoPermiso == 'ESCRITURA')
        {
            btnEditar = `<button type="button" class="btnTbl" onclick="configResoluciones.mostrarModal(${contador})"><i class="fas fa-edit"></i></button>`
            btnEliminar = `<button type="button" class="btnTbl" onclick="configResoluciones.eliminar(${contador})"><i class="fas fa-times-circle"></i></button>`
        }

        tblResoluciones.row.add([
            contador, numeroResolucion, tipoDocumento, agencia, fechaAprobacion, progreso,
            `<div class="tblButtonContainer"> ${btnEditar} ${btnEliminar}</div>`
        ]).node().id = `trResoluciones${contador}`
    },
    editarFila( contador, { numeroResolucion, TipoDocumentoHacienda: { tipoDocumento }, agencia, fechaAprobacion, progreso } )
    {
        $(`#trResoluciones${contador}`).find("td:eq(1)").html(numeroResolucion);
        $(`#trResoluciones${contador}`).find("td:eq(2)").html(tipoDocumento);
        $(`#trResoluciones${contador}`).find("td:eq(3)").html(agencia);
        $(`#trResoluciones${contador}`).find("td:eq(4)").html(fechaAprobacion);
        $(`#trResoluciones${contador}`).find("td:eq(5)").html(progreso);
    },
    salir() {

        Swal.fire({
            title: 'IMPORTANTE',
            text: `¿Quieres salir del formulario de resolución de hacienda?`,
            icon: 'warning',
            allowOutsideClick: false,
            showCloseButton: true,
            showCancelButton: true,
            showDenyButton: true,
            confirmButtonColor: '#46b1c3',
            denyButtonText: `<i class="fa fa-trash"></i> Limpiar`,
            confirmButtonText: `<i class="fas fa-check-circle"></i> Salir`,
            cancelButtonText: `<i class="fas fa-times-circle"></i> Cancelar`
        }).then( result  => {

            if ( result.isConfirmed)
            {
                this.tabFormulario.classList.add('disabled')
                this.tabResoluciones.classList.remove('disabled')
                $(`#resoluciones-tab`).trigger('click')

            } else if (result.isDenied)
            {
               formActions.getJSON('formResolución')
                   .then()
            }
        })
    },
    eliminar( idFila )
    {
        const numeroHacienda = this.resoluciones[idFila].numeroResolucion
        const id = this.resoluciones[idFila].id

        Swal.fire({
            title: 'IMPORTANTE',
            html: `¿Quieres anular/eliminar la resolución de hacienda con número <strong>${numeroHacienda}</strong>?`,
            icon: 'warning',
            allowOutsideClick: false,
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            confirmButtonText: `<i class="fas fa-thumbs-up"></i> Eliminar`,
            cancelButtonText: `<i class="fas fa-thumbs-down"></i> Cancelar`
        }).then( result => {

            if ( result.isConfirmed)
            {
                fetchActions.set({
                    modulo, archivo: 'Resolucion/contabilidadEliminarResolucion', datos: {
                        id, idApartado, tipoApartado
                    }
                }).then( response => {

                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'La resolución de hacienda fue eliminada con exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then( () => {
                                delete this.resoluciones[idFila]
                                tblSujetosExluidos.row(`#trResoluciones${idFila}`).remove().draw()
                            })
                            break;
                            default:
                                generalMostrarError(response);
                            break;
                    }
                }).catch( generalMostrarError )
            }
        })
    }
}

const configCorrelativos = {
    txtInicio: document.getElementById('txtInicio'),
    txtFin: document.getElementById('txtFin'),
    txtDisponibles: document.getElementById('txtDisponibles'),
    calcularSujetos()
    {
        this.txtDisponibles.value = ''
        this.txtDisponibles.value = 0

        if ( this.txtInicio.value && this.txtFin.value )
        {
            this.txtDisponibles.value = calcularSujetos( this.txtInicio.value, this.txtFin.value )
        }
    },
    calcularProgreso(  { correlativoInicial, correlativoFinal, disponible } )
    {
        let totalCorrelativos = 0

        if ( validadCorrelativos( correlativoInicial) && validadCorrelativos(correlativoFinal) )
        {
            totalCorrelativos = Number(validadCorrelativos(correlativoFinal)) - Number( validadCorrelativos( correlativoInicial) ) + 1
        }

        const porcentajeDisponible = parseFloat( ( Number(disponible) * 100 ) / Number(totalCorrelativos) ).toFixed(2)

        let progress = `
            <div class="progress">        
                <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: ${100 - porcentajeDisponible}%" aria-valuenow="${ 100 - porcentajeDisponible}" aria-valuemin="0" aria-valuemax="100">${100 - porcentajeDisponible}%</div>
                <div class="progress-bar bg-dark" role="progressbar" style="width: ${porcentajeDisponible}%" aria-valuenow="${porcentajeDisponible}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>`

        return progress
    }
}

const validadCorrelativos = ( correlativo ) => {
    if ( !isNaN( correlativo.substring( (correlativo.length - 4), correlativo.length ) ) )
    {
        return Number(correlativo.substring( (correlativo.length - 4), correlativo.length ))
    }
    return false
}

const calcularSujetos = ( correlativoinicial, correlativoFinal ) => {

    if ( validadCorrelativos( correlativoinicial ) && validadCorrelativos( correlativoFinal ) &&
        validadCorrelativos( correlativoinicial ) < validadCorrelativos( correlativoFinal )
    )
    {
        return Number( validadCorrelativos( correlativoFinal ) - validadCorrelativos( correlativoinicial ) + 1)
    }

    return 0
}