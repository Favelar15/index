<div class="pagetitle">
    <h1>Reporte de sujetos excluidos</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Contabilidad</li>
            <li class="breadcrumb-item">Reportes</li>
            <li class="breadcrumb-item active">Sujetos excluidos</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <form action="javascript:configRptSujetosExcluidos.generar()" id="formRptSujetos" name="formRptSujetos"
          accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
        <div class="row">
            <div class="col-lg-4 col-xl-4">
                <label class="form-label" for="cboAgencia">Agencia</label>
                <div class="form-group has-validation">
                    <select class="selectpicker form-control cboAgencia"
                            id="cboAgencia" name="cboAgencia">
                        <option selected disabled value="">Todas las agencias</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-xl-4">
                <label class="form-label" for="txtInicio">Desde <span class="requerido">*</span> </label>
                <input type="text" class="form-control fechaFinLimitado readonly" onchange="void(0)" id="txtInicio" name="txtInicio" placeholder="Fecha de inicio" required>
                <div class="invalid-feedback">
                    Seleccione la fecha de inicio
                </div>
            </div>
            <div class="col-lg-4 col-xl-4">
                <label class="form-label" for="txtFin">Hasta <span class="requerido">*</span></label>
                <input type="text" class="form-control fechaFinLimitado readonly" id="txtFin" name="txtFin" placeholder="Fecha final" required>
                <div class="invalid-feedback">
                    Seleccione la fecha final
                </div>
            </div>
            <div class="col-lg- col-xl- mt-3" align="center">
                <button type="submit" class="btn btn-sm btn-outline-primary">
                    <i class="fas fa-search"></i> Generar reporte
                </button>
                <button type="button" class="disabled btn btn-sm btn-outline-success" id="btnExcel" onclick="void(0)">
                    <i class="fas fa-file-excel"></i> Exportar a excel
                </button>
                <button type="button"  class="btn btn-sm btn-outline-danger" id="btnCancelarG" onclick="configRptSujetosExcluidos.limpiar()">
                    <i class="fas fa-times-circle"></i> Limpiar
                </button>
            </div>
        </div>
    </form>

    <hr class="mb-1 mt-3">
    <table class="table table-striped table-bordered" id="tblSujetosExluidos">
        <thead>
        <th>N°</th>
        <th>N° de resolución</th>
        <th>Correlativo</th>
        <th>Proveedor</th>
        <th>Fecha emisión</th>
        <th>Agencia</th>
        <th>Usuario</th>
        <th>Acciones</th>
        </thead>
        <body></body>
    </table>
</section>
<?php
$_GET['js'] = ['contaRptSujetosExcluidos'];