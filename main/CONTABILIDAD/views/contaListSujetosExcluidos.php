<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Sujetos excluidos</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">CONTABILIDAD</li>
            <li class="breadcrumb-item active">Sujetos excluidos</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">

            <button class="nav-link active" id="resoluciones-tab" data-bs-toggle="tab"
                    data-bs-target="#resoluciones-panel" role="tab"
                    aria-controls="Resoluciones" aria-selected="true">
                Sujetos excluidos
            </button>
        </li>

        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="btnResoluciones">
                <button type="button" class="btn btn-sm btn-outline-dark buttonModal" onclick="configSujetos.nuevoSujeto()">
                    <i class="fa fa-plus-circle"></i> Formulario
                </button>
            </div>
        </li>
        
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="resoluciones-panel" role="tabpanel" aria-labelledby="resoluciones-panel">

            <table class="table table-striped table-bordered" id="tblSujetosExluidos">
                <thead>
                <th>N°</th>
                <th>Resolución</th>
                <th>Correlativo</th>
                <th>Proveedor</th>
                <th>Agencia</th>
                <th>Fecha emisión</th>
                <th>Acciones</th>
                </thead>
            </table>
            
        </div>
    </div>

</section>


<div class="modal fade" id="modal-eliminar" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title-eliminar">Anular sujeto</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="javascript:configSujetos.eliminar()" class="needs-validation" novalidate id="formAnularSujeto"
                      name="formAnularSujeto"
                      accept-charset="utf-8" method="post">
                    <div class="row">

                        <div class="col-lg-4 col-xl-4 mayusculas">
                            <label for="txtFechaEmision" class="form-label">Fecha de emisión:</label>
                            <input type="text" id="txtFechaEmision" name="txtFechaEmision" class="form-control" placeholder="dd/mm/YYYY"
                                   required readonly>
                        </div>

                        <div class="col-lg-8 col-xl-8 mayusculas">
                            <label for="txtNumeroResolucion" class="form-label">Número de resolución:</label>
                            <input type="text" id="txtNumeroResolucion" name="txtNumeroResolucion" class="form-control" placeholder="Número de resolución"
                                   required readonly>
                        </div>

                        <div class="col-lg-4 col-xl-4">
                            <label for="txtCorrelativo" class="form-label">Correlativo:</label>
                            <input type="text" id="txtCorrelativo" name="txtCorrelativo" class="form-control" placeholder="Correlativo"
                                   required readonly>
                        </div>

                        <div class="col-lg-8 col-xl-8 mayusculas">
                            <label for="txtProveedor" class="form-label">Proveedor:</label>
                            <input type="text" id="txtProveedor" name="txtProveedor" class="form-control" placeholder="Datos del proveedor"
                                   required readonly>
                        </div>

                        <div class="col-lg-4 col-xl-4">
                            <label for="cboDañadoFisicamente" class="form-label">¿Dañado fisícamente?: <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select class="form-control selectpicker"
                                        id="cboDañadoFisicamente" name="cboDañadoFisicamente" required>
                                    <option selected value="" disabled>Seleccione</option>
                                    <option value="S">Sí</option>
                                    <option value="N">No</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-8 col-xl-8 mayusculas">
                            <label for="txtMotivo" class="form-label">Detalle el motivo de anulación del sujeto: <span class="requerido">*</span></label>
                            <input type="text" id="txtMotivo" name="txtMotivo" class="form-control" placeholder="Motivo"
                                   required>
                            <div class="invalid-feedback">
                                * Campo obligatorio
                            </div>
                        </div>

                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" form="formAnularSujeto" class="btn btn-sm btn-outline-primary"><i class="fas fa-trash-alt"></i> Eliminar</button>
                <button type="button" class="btn btn-sm btn-outline-secondary" data-bs-dismiss="modal"><i class="fas fa-times-circle"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>


<?php
$_GET['js'] = ['contabilidadGeneral', 'contaListSujetosExcluidos'];
