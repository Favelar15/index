<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Resoluciones</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">CONTABILIDAD</li>
            <li class="breadcrumb-item">Resoluciones de hacienda (Papeleria)</li>
            <li class="breadcrumb-item active">Resolución</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">

            <button class="nav-link active" id="resoluciones-tab" data-bs-toggle="tab"
            data-bs-target="#resoluciones-panel" role="tab"
            aria-controls="Resoluciones" aria-selected="true"
            onclick="generalShowHideButtonTab('mainButtonContainer','btnResoluciones');">
                Resoluciones
            </button>

        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="btnResoluciones">
                <button type="button" class="btn btn-sm btn-outline-dark buttonModal" onclick="configResoluciones.mostrarModal()">
                    <i class="fa fa-plus"></i> Nueva resolución
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="resoluciones-panel" role="tabpanel" aria-labelledby="resoluciones-panel">

            <table class="table table-bordered table-striped" id="tblResoluciones">
                <thead>
                <th>N°</th>
                <th>Número de resolución</th>
                <th>Tipo de documento</th>
                <th>Agencia</th>
                <th>Fecha de aprobación</th>
                <th>Correlativos utilizados</th>
                <th>Acciones</th>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

</section>


<div class="modal fade" id="resolucion-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Gestión de resoluciones de hacienda</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="javascript:configResoluciones.guardar()" id="formResolución"
                      name="formResolución" accept-charset="utf-8" method="post" class="needs-validation"
                      novalidate>

                    <div class="row">

                        <div class="col-lg-4 col-xl-4 mayusculas">
                            <label class="form-label" for="txtNumeroResolucion">Número de resolución: <span
                                        class="requerido">*</span> </label>
                            <input type="text" class="form-control" id="txtNumeroResolucion"
                                   name="txtNumeroResolucion"
                                   placeholder="Ingrese el número de la resolución" required/>
                            <div class="invalid-feedback">
                                Ingrese el número de la resolución
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-4">
                            <label class="form-label" for="cboTipoDocumentoHacienda">Tipo de documento: <span
                                        class="requerido">*</span></label>
                            <select class="selectpicker form-control" id="cboTipoDocumentoHacienda" name="cboTipoDocumentoHacienda" required>
                                <option selected disabled value=''>Seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione el tipo de documento
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-4">
                            <label class="form-label" for="cboAgencia">Agencia: <span
                                        class="requerido">*</span></label>
                            <select class="selectpicker form-control" id="cboAgencia" required>
                                <option selected disabled value=''>Seleccione</option>
                            </select>
                            <div class="invalid-feedback">Seleccione la agencia</div>
                        </div>

                        <div class="col-lg-4 col-xl-4 mayusculas">
                            <label class="form-label" for="txtInicio">Inicio del correlativo: <span
                                        class="requerido">*</span> </label>
                            <input type="text" class="form-control" id="txtInicio"
                                   name="txtInicio" onkeyup="configCorrelativos.calcularSujetos()"
                                   placeholder="Inicio del correlativo" required/>
                            <div class="invalid-feedback">
                                Indique el correlativo de inicio de la resolución
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-4 mayusculas">
                            <label class="form-label" for="txtFin">Fin del correlativo: <span
                                        class="requerido">*</span> </label>
                            <input type="text" class="form-control" id="txtFin"
                                   name="txtFin" onkeyup="configCorrelativos.calcularSujetos()"
                                   placeholder="Fin del correlativo" required/>
                            <div class="invalid-feedback">
                                Indique el correlativo de fin de la resolución
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-4">
                            <label class="form-label" for="txtDisponibles">Sujetos disponibles: <span
                                        class="requerido">*</span> </label>
                            <input type="text" class="form-control" id="txtDisponibles"
                                   name="txtDisponibles" readonly
                                   placeholder="0" required/>
                            <div class="invalid-feedback">
                                Indique el correlativo de fin de la resolución
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-4">
                            <label class="form-label" for="txtFechaAprobacion">Fecha de aprobación <span class="requerido">*</span> </label>
                            <input type="text" class="form-control fechaFinLimitado readonly" id="txtFechaAprobacion"
                                   name="txtFechaAprobacion" placeholder="Fecha de inicio" required>
                            <div class="invalid-feedback">
                                Seleccione la fecha de aprobación de la resolución
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-4">
                            <label class="form-label" for="cboTipoFormato">Tipo de formato: <span
                                        class="requerido">*</span></label>
                            <select class="selectpicker form-control" id="cboTipoFormato" required>
                                <option selected disabled value=''>Seleccione</option>
                                <option  value='N'>Nuevo formato</option>
                                <option  value='A'>Antiguo formato</option>
                                <option  value='L'>Formato 2022</option>
                            </select>
                            <div class="invalid-feedback">Seleccione</div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">

                <button type="submit" form="formResolución" class="btn btn-outline-primary btn-sm"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal"><i class="fa fa-times-circle"></i> Cancelar</button>

            </div>
        </div>
    </div>
</div>



<?php
$_GET['js'] = [ 'contabilidadGeneral', 'contabilidadResolucionHacienda'];