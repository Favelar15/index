<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Formulario sujetos excluidos</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">CONTABILIDAD</li>
            <li class="breadcrumb-item active">Sujetos excluidos</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">

            <button class="nav-link active" id="resoluciones-tab" data-bs-toggle="tab"
                    data-bs-target="#resoluciones-panel" role="tab"
                    aria-controls="Resoluciones" aria-selected="true">
                Datos del proveedor
            </button>
        </li>

        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="btnResoluciones">
                <button type="button" class="btn btn-sm btn-outline-primary buttonModal" onclick="configSujeto.guardar()">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button type="button" class="btn btn-sm btn-outline-danger buttonModal" onclick="configSujeto.cancelar()">
                    <i class="fa fa-times-circle"></i> Cancelar
                </button>
            </div>
        </li>

    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="resoluciones-panel" role="tabpanel" aria-labelledby="resoluciones-panel">

            <form action="javascript:void(0)" class="needs-validation" novalidate id="formProveedor"
                  name="formProveedor"
                  accept-charset="utf-8" method="post">

                <div class="row">

                    <div class="col-lg-3 col-xl-3">
                        <label for="cboTipoDocumento" class="form-label">Tipo de documento: <span
                                    class="requerido">*</span></label>
                        <select onchange="buscarProveedor.selectTipoDocumento(this.value)"
                                class="form-control selectpicker cboTipoDocumento tipoPrimario" id="cboTipoDocumento" required>
                            <option selected value=''>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el tipo de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtDocumentoPrimario" class="form-label">Número de documento <span
                                    class="requerido">*</span></label>
                        <div class="input-group has-validation">
                            <input onkeypress="return generalSoloNumeros(event);" type="text" class="form-control"
                                   id="txtDocumentoPrimario" name="txtDocumentoPrimario" placeholder="Número de identificación"
                                   required readonly>
                            <div class="input-group-append">
                                <button class="input-group-text btn btn-outline-dark disabled" onclick="buscarProveedor.buscar()"
                                        type="button"
                                        id="btnBuscarProveedor"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtCodigoCliente" class="form-label">Código de cliente</label>
                        <input type="text" placeholder="Código de cliente" id="txtCodigoCliente" name="txtCodigoCliente"
                               class="form-control" readonly>
                    </div>
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label for="txtNombres" class="form-label">Nombres: <span class="requerido">*</span></label>
                        <input type="text" id="txtNombres" name="txtNombres" class="form-control" placeholder="Nombres"
                               required readonly>
                        <div class="invalid-feedback">
                            Ingrese los nombres
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label for="txtApellidos" class="form-label">Apellidos: <span class="requerido">*</span></label>
                        <input type="text" id="txtApellidos" name="txtApellidos" class="form-control" placeholder="Apellidos"
                               required readonly>
                        <div class="invalid-feedback">
                            Ingrese los apellidos
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtNit" class="form-label">NIT:</label>
                        <input type="text" placeholder="0000-000000-000-0" id="txtNit" name="txtNit"
                               class="form-control" readonly required>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtTelefonoFijo" class="form-label">Teléfono fijo:</label>
                        <input type="text" placeholder="0000-0000" id="txtTelefonoFijo" name="txtTelefonoFijo"
                               class="form-control telefono" readonly>
                        <div class="invalid-feedback">
                            Ingrese el teléfono fijo
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtTelefonoMovil" class="form-label">Teléfono movil:</label>
                        <input type="text" placeholder="0000-0000" id="txtTelefonoMovil" name="txtTelefonoMovil"
                               class="form-control telefono" required readonly>
                        <div class="invalid-feedback">
                            Ingrese el teléfono móvil
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label for="txtEmail" class="form-label">Email:</label>
                        <input type="text" placeholder="Ingrese el correo electronico" id="txtEmail" name="txtEmail"
                               class="form-control" required readonly>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="cboPais" class="form-label">País: <span class="requerido">*</span></label>
                        <select disabled title="Seleccione" class="form-control selectpicker cboPais" data-live-search="true"
                                id="cboPais" name="cboPais" onchange="generalMonitoreoPais(this.value,'cboDepartamento');"
                                required>
                            <option value="" selected disabled>Seleccione</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="cboDepartamento" class="form-label">Departamento de residencia: <span
                                    class="requerido">*</span> </label>

                        <select disabled title="Seleccione" class="form-control selectpicker" data-live-search="true"
                                id="cboDepartamento" name="cboDepartamento"
                                onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');" required>
                            <option value="" selected disabled>seleccione un país</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="cboMunicipio" class="form-label">Municipio de residencia: <span
                                    class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select disabled title="Seleccione" class="form-control selectpicker" data-live-search="true"
                                    id="cboMunicipio" name="cboMunicipio" required>
                                <option selected value="" disabled>seleccione un departamento</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-12 col-xl-12 mayusculas">
                        <label for="txtDireccion" class="form-label">Dirección completa: <span class="requerido">*</span></label>
                        <input readonly type="text" class="form-control" id="txtDireccion" name="txtDireccion"
                               placeholder="Detalle la dirección" required/>
                        <div class="invalid-feedback">
                            Ingrese la dirección completa
                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>

    <br>
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">

            <button class="nav-link active" id="resoluciones-tab" data-bs-toggle="tab"
                    data-bs-target="#resoluciones-panel" role="tab"
                    aria-controls="Resoluciones" aria-selected="true">
                Datos del sujeto excluido
            </button>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="resoluciones-panel" role="tabpanel" aria-labelledby="resoluciones-panel">

            <form action="javascript:configSujeto.guardar()" class="needs-validation" novalidate id="formSujeto"
                  name="formSujeto"
                  accept-charset="utf-8" method="post">
                <div class="row">

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtFechaEmision">Fecha de emisión <span class="requerido">*</span> </label>
                        <input type="text" disabled class="form-control fechaFinLimitado readonly" id="txtFechaEmision"
                               name="txtFechaEmision" placeholder="dd/mm/YYYY" required>
                        <div class="invalid-feedback">
                            Seleccione la fecha de emisión del sujeto
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-6">
                        <label for="cboResolucion" class="form-label">Resolución de hacienda: <span
                                    class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select disabled class="form-control selectpicker" onchange="configCorrelativo.onChangeResolucion()"
                                    id="cboResolucion" name="cboResolucion" data-live-search="true"  required>
                                <option selected value=''>Seleccione</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label for="txtCorrelativo" class="form-label">Correlativo <span
                                    class="requerido">*</span></label>
                        <div class="input-group has-validation">
                            <input onkeypress="return generalSoloNumeros(event);" type="text" class="form-control"
                                   id="txtCorrelativo" name="txtCorrelativo" placeholder="Ingrese el correlativo"
                                   required readonly>
                            <div class="input-group-append">
                                <button class="input-group-text btn btn-outline-dark disabled" onclick="configCorrelativo.comprobar()"
                                        type="button" title="Validar correlativo"
                                        id="btnComprobarCorrelativo"><i class="fas fa-check-square"></i></button>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label for="cboResolucion" class="form-label">Tipo de servicio: <span
                                    class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select disabled onchange="configTipoServicio.change()"
                                    class="form-control selectpicker" id="cboTipoServicio" name="cboTipoServicio" required>
                                <option selected value=''>Seleccione</option>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            Seleccione el tipo de servicio
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label for="cboResolucion" class="form-label">¿Asumimos IVA?: <span
                                    class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select disabled class="form-control selectpicker" id="cboIva" name="cboIva" required>
                                <option selected disabled value=''>Seleccione</option>
                                <option value='S'>SI</option>
                                <option value='N'>NO</option>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            Seleccione si se asume IVA
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label for="cboResolucion" class="form-label">¿Aplica RENTA?: <span
                                    class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select onchange="configRenta.change()"
                                    disabled class="form-control selectpicker" id="cboRenta" name="cboRenta" required>
                                <option selected disabled value=''>Seleccione</option>
                                <option value='S'>SI</option>
                                <option value='N'>NO</option>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            Seleccione la aplica RENTA
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label for="cboResolucion" class="form-label">¿Asumimos RENTA?: <span
                                    class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select disabled class="form-control selectpicker" id="cboAsumimosRenta" name="cboAsumimosRenta" required>
                                <option selected disabled value=''>Seleccione</option>
                                <option value='S'>SI</option>
                                <option value='N'>NO</option>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            Seleccione si se asume RENTA
                        </div>
                    </div>
                </div>
            </form>
            <br>
            <div class="row">
                <div class="col-8">
                    <h6>Detalle del sujeto</h6>
                </div>
                <div class="col-4 topBtnContainer">
                    <button class="btn btn-sm btn-outline-dark float-end disabled" title="Guardar" id="btnAgregarSujeto" type="button" onclick="configDetalleSujeto.mostrarModal()">
                        <i class="fas fa-plus-circle"></i> <span>Agregar</span>
                    </button>
                </div>
                <br>
                <div class="col-12">
                    <table class="table table-bordered table-striped" id="tblDetalle">
                        <thead>
                        <th>N°</th>
                        <th>Descripción</th>
                        <th>Cantidad</th>
                        <th>Precio unitario</th>
                        <th>SubTotal</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>

</section>

<div class="modal fade" id="modal-detalleSujeto" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Detalle del sujeto excluido</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="javascript:configDetalleSujeto.guardar()" class="needs-validation" novalidate id="formDetalleSujeto"
                      name="formDetalleSujeto"
                      accept-charset="utf-8" method="post">
                    <div class="row">

                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label for="txtDescripcion" class="form-label">Descripción: <span class="requerido">*</span>
                            </label>
                            <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion"
                                   placeholder="Ingrese el detalle" required/>
                            <div class="invalid-feedback">
                                Ingrese el detalle
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-6 mayusculas">
                            <label for="txtCantidad" class="form-label">Cantidad: <span class="requerido">*</span>
                            </label>
                            <input type="text" class="form-control" id="txtCantidad" name="txtCantidad"
                                   placeholder="Ingrese la cantidad" required/>
                            <div class="invalid-feedback">
                                Ingrese la cantidad
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-6 mayusculas">
                            <label for="txtPrecio" class="form-label">Precio: <span class="requerido">*</span>
                            </label>
                            <input type="text" class="form-control" id="txtPrecio" name="txtPrecio"
                                   placeholder="USD" required/>
                            <div class="invalid-feedback">
                                Ingrese el precio unitario
                            </div>
                        </div>

                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" form="formDetalleSujeto" class="btn btn-sm btn-outline-primary"><i class="fas fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-sm btn-outline-secondary" data-bs-dismiss="modal"><i class="fas fa-times-circle"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>


<?php
$_GET['js'] = ['contabilidadGeneral', 'contaSujetosExcluidos'];