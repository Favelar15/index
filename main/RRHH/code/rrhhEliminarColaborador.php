<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    if (count($input) > 0) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/colaborador.php';

        $id = $input['id'];
        $idUsuario = $_SESSION['index']->id;
        $tipoApartado = $input['tipoApartado'];
        $idApartado = base64_decode(urldecode($input['idApartado']));

        $colaborador = new colaborador();
        $colaborador->id = $id;

        $respuesta = $colaborador->deleteColaborador($idUsuario, $tipoApartado, $idApartado);

        $conexion = null;
    }
} else {
    $respuesta->{'respuesta'} = 'SESION';
}


echo json_encode($respuesta);
