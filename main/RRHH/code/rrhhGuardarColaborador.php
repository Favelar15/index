<?php
require_once '../../code/generalParameters.php';
$respuesta = (object)[];
session_start();

if (!empty($_POST)) {
    if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
        include '../../code/connectionSqlServer.php';
        require_once './Models/colaborador.php';

        $idUsuario = $_SESSION['index']->id;
        $tipoApartado = $_POST['tipoApartado'];
        $idApartado = base64_decode(urldecode($_POST['idApartado']));

        $colaborador = new colaborador();
        $colaborador->id = $_POST['id'];
        $colaborador->nombres = $_POST['nombres'];
        $colaborador->apellidos = $_POST['apellidos'];
        $colaborador->agencia = $_POST['agencia'];
        $colaborador->gerencia = $_POST['gerencia'];
        $colaborador->cargo = $_POST['cargo'];

        $detArchivo = isset($_FILES['archivoFoto']) ? pathinfo($_FILES['archivoFoto']['name']) : pathinfo('avatar.jpg');
        $extension = $detArchivo['extension'];
        $nombreFoto = $_POST['foto'] != 'avatar.jpg' ? $_POST['foto'] : 'avatar.jpg';
        $colaborador->foto = $nombreFoto;

        $guardar = $colaborador->saveColaborador($idUsuario, $tipoApartado, $idApartado, $extension);

        if (isset($guardar["respuesta"])) {
            $respuesta->{"respuesta"} = $guardar["respuesta"];
            $respuesta->{"id"} = $guardar["id"];
            $respuesta->{"nombreArchivo"} = $guardar["nombreArchivo"];
            $respuesta->{"fechaActualizacion"} = date('d-m-Y h:i a', strtotime($guardar["fechaActualizacion"]));
            if ($guardar["respuesta"] == "EXITO" && isset($_FILES["archivoFoto"])) {
                $nombreArchivo = $guardar["nombreArchivo"];
                if (file_exists('../../img/colaboradores/' . $nombreArchivo)) {
                    unlink('../../img/colaboradores/' . $nombreArchivo);
                }

                if (!move_uploaded_file($_FILES['archivoFoto']['tmp_name'], '../../img/colaboradores/' . $nombreArchivo)) {
                    $respuesta->{'respuesta'} = 'ERROR_UPLOAD';
                }
            }
        } else {
            $respuesta->{"respuesta"} = "Error al conectar con la base de datos";
        }

        $conexion = null;
    } else {
        $respuesta->{"respuesta"} = "SESION";
    }
}

echo json_encode($respuesta);
