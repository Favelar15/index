<?php
class cargo
{
    public $id;
    public $cargo;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function getCargos()
    {
        $query = 'SELECT * FROM rrhh_viewCargos;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }
}
