<?php
class colaborador
{
    public $id;
    public $dui;
    public $nit;
    public $isss;
    public $nombres;
    public $apellidos;
    public $agencia;
    public $gerencia;
    public $cargo;
    public $foto;
    public $salario;
    public $codigoOficialia;
    public $fechaContratacion;
    public $fechaActualizacion;
    public $fechaRegistro;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
    }

    public function saveColaborador($idUsuario, $tipoApartado, $idApartado, $extension)
    {
        $response = null;
        $id = null;
        $nombreArchivo = null;
        $fechaActualizacion = null;
        $query = 'EXEC rrhh_spGuardarColaborador :id,:nombres,:apellidos,:agencia,:gerencia,:cargo,:foto,:extension,:tipoApartado,:idApartado,:usuario,:response,:idColaborador,:nombreArchivo,:fechaActualizacion;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':nombres', $this->nombres, PDO::PARAM_STR);
        $result->bindParam(':apellidos', $this->apellidos, PDO::PARAM_STR);
        $result->bindParam(':agencia', $this->agencia, PDO::PARAM_INT);
        $result->bindParam(':gerencia', $this->gerencia, PDO::PARAM_INT);
        $result->bindParam(':cargo', $this->cargo, PDO::PARAM_INT);
        $result->bindParam(':foto', $this->foto, PDO::PARAM_STR);
        $result->bindParam(':extension', $extension, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idColaborador', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':nombreArchivo', $nombreArchivo, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return ['respuesta' => $response, 'id' => $id, 'nombreArchivo' => $nombreArchivo, 'fechaActualizacion' => $fechaActualizacion];
        }

        $this->conexion = null;
        return [];
    }

    public function getColaboradores()
    {
        $query = 'SELECT * FROM rrhh_viewDatosColaboradores;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

    public function getColaboradoresCbo()
    {
        $query = 'SELECT id,nombres,apellidos,codigoOficialia FROM rrhh_viewDatosColaboradores;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_OBJ);
        }

        $this->conexion = null;
        return [];
    }

    public function deleteColaborador($idUsuario, $tipoApartado, $idApartado)
    {
        $response = null;
        $query = 'EXEC rrhh_spEliminarColaborador :id,:usuario,:tipoApartado,:idApartado,:response;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;

            return ['respuesta' => $response];
        }

        $this->conexion = null;
        return ['respuesta' => 'Error en rrhh_spEliminarColaborador'];
    }

    public function actualizarCodigoOficialia($idUsuario, $tipoApartado, $idApartado)
    {
        $response = null;
        $query = 'EXEC oficialia_spActualizarColaborador :id,:codigo,:usuario,:tipoApartado,:idApartado,:response';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':codigo', $this->codigoOficialia, PDO::PARAM_STR);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return ['respuesta' => $response];
        }
        $this->conexion = null;
        return ['respuesta' => 'Error en oficialia_spActualizarColaborador'];
    }
}
