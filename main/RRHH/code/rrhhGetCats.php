<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";

        $solicitados = explode("@@@", $_GET["q"]);

        if (in_array('agencias', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/agencia.php';
            $agencia = new agencia();
            $agencias = $agencia->getAgenciasCbo();

            $respuesta->{"agencias"} = $agencias;
        }

        if (in_array('gerencias', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/gerencia.php';
            $gerencia = new gerencia();
            $gerencias = $gerencia->getGerencias();

            $respuesta->{"gerencias"} = $gerencias;
        }

        if (in_array('cargos', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/cargo.php';
            $cargo = new cargo();
            $cargos = $cargo->getCargos();

            $respuesta->{"cargos"} = $cargos;
        }

        $conexion = null;
    }
}

echo json_encode($respuesta);
