<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET)) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/colaborador.php';

        $colaborador = new colaborador();

        $respuesta->{'colaboradores'} = $colaborador->getColaboradores();

        $conexion = null;
    }
}
else{
    $respuest->{'respuesta'} = 'SESION';
}

echo json_encode($respuesta);