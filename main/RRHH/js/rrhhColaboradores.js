let tblColaboradores = {};

window.onload = () => {
    initTables().then(() => {
        fetchActions.getCats({
            modulo: 'RRHH',
            archivo: 'rrhhGetCats',
            solicitados: ['all']
        }).then(({
            agencias,
            cargos,
            gerencias
        }) => {
            const opciones = [`<option selected disabled value="">seleccione</option>`];
            let cbosAgencia = document.querySelectorAll('select.cboAgencia'),
                cbosGerencia = document.querySelectorAll('select.cboGerencia'),
                cbosCargo = document.querySelectorAll('select.cboCargo'),
                allSelectpickerCbos = document.querySelectorAll('select.selectpicker'),
                tmpOpciones = [...opciones];

            agencias.forEach(agencia => {
                tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`);
                configColaborador.agencias[agencia.id] = agencia;
            });
            cbosAgencia.forEach(cbo => cbo.innerHTML = tmpOpciones.join(''));

            tmpOpciones = [...opciones];
            gerencias.forEach(gerencia => {
                tmpOpciones.push(`<option value="${gerencia.id}">${gerencia.gerencia}</option>`);
                configColaborador.gerencias[gerencia.id] = gerencia;
            });
            cbosGerencia.forEach(cbo => cbo.innerHTML = tmpOpciones.join(''));

            tmpOpciones = [...opciones];
            cargos.forEach(cargo => {
                tmpOpciones.push(`<option value="${cargo.id}">${cargo.cargo}</option>`);
                configColaborador.cargos[cargo.id] = cargo;
            });
            cbosCargo.forEach(cbo => cbo.innerHTML = tmpOpciones.join(''));

            allSelectpickerCbos.forEach(cbo => $('#' + cbo.id).selectpicker('refresh'));

            fetchActions.get({
                modulo: 'RRHH',
                archivo: 'rrhhGetColaboradores',
                params: {
                    'Solicitados': 'Todos'
                }
            }).then(({
                colaboradores
            }) => {
                configColaborador.init(colaboradores);
            }).catch(generalMostrarError);
        }).catch(generalMostrarError);
    });
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblColaboradores").length) {
                tblColaboradores = $("#tblColaboradores").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [2, 4, 5, 6],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblColaboradores.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configColaborador = {
    id: 0,
    cnt: 0,
    agencias: {},
    gerencias: {},
    cargos: {},
    colaboradores: {},
    campoImagen: document.getElementById('userImage'),
    campoNombres: document.getElementById('txtNombres'),
    campoApellidos: document.getElementById('txtApellidos'),
    cboAgencia: document.getElementById('cboAgencia'),
    cboGerencia: document.getElementById('cboGerencia'),
    cboCargo: document.getElementById('cboCargo'),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(colaborador => {
                    this.cnt++;
                    this.colaboradores[this.cnt] = {
                        ...colaborador,
                        labelAgencia: this.agencias[colaborador.agencia].nombreAgencia,
                        labelGerencia: this.gerencias[colaborador.gerencia].gerencia,
                        labelCargo: this.cargos[colaborador.cargo].cargo,
                    };
                    this.addRowTbl({
                        ...this.colaboradores[this.cnt],
                        nFila: this.cnt
                    });
                });
                tblColaboradores.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmColaborador').then(() => {
            this.id = id;
            $("#labelUserImage").css("background-image", "url('./main/img/colaboradores/avatar.jpg')");

            if (this.id > 0) {
                let tmp = this.colaboradores[this.id];
                this.campoNombres.value = tmp.nombres;
                this.campoApellidos.value = tmp.apellidos;
                this.cboAgencia.value = tmp.agencia;
                this.cboGerencia.value = tmp.gerencia;
                this.cboCargo.value = tmp.cargo;
                $("#labelUserImage").css('background-image', "url('./main/img/colaboradores/" + tmp.foto + "')");

                document.querySelectorAll('#mdlColaborador select.selectpicker').forEach(cbo => $('#' + cbo.id).selectpicker('refresh'));
            }

            $("#mdlColaborador").modal('show');
        });
    },
    save: function () {
        formActions.validate('frmColaborador').then(({
            errores
        }) => {
            if (errores == 0) {
                let nombreImagen = this.campoImagen.files[0] ? $("#" + this.campoImagen.id).val().split('\\').pop() : (this.colaboradores[this.id] ? this.colaboradores[this.id].foto : "avatar.jpg");
                let tmp = {
                    id: this.id > 0 ? this.colaboradores[this.id].id : 0,
                    nombres: this.campoNombres.value,
                    apellidos: this.campoApellidos.value,
                    agencia: this.cboAgencia.value,
                    labelAgencia: this.cboAgencia.options[this.cboAgencia.options.selectedIndex].text,
                    gerencia: this.cboGerencia.value,
                    labelGerencia: this.cboGerencia.options[this.cboGerencia.options.selectedIndex].text,
                    cargo: this.cboCargo.value,
                    labelCargo: this.cboCargo.options[this.cboCargo.options.selectedIndex].text,
                    foto: nombreImagen
                };

                let datosCompletos = new FormData();
                datosCompletos.append('id', tmp.id);
                datosCompletos.append('nombres', tmp.nombres);
                datosCompletos.append('apellidos', tmp.apellidos);
                datosCompletos.append('agencia', tmp.agencia);
                datosCompletos.append('gerencia', tmp.gerencia);
                datosCompletos.append('cargo', tmp.cargo);
                datosCompletos.append('foto', tmp.foto);
                datosCompletos.append('tipoApartado', tipoApartado);
                datosCompletos.append('idApartado', idApartado);

                if (this.campoImagen.files[0]) {
                    datosCompletos.append("archivoFoto", this.campoImagen.files[0]);
                }

                this.saveDB(datosCompletos).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta) {
                            case 'EXITO':
                                tmp.id = datosRespuesta.id;
                                tmp.fechaActualizacion = datosRespuesta.fechaActualizacion;
                                tmp.foto = datosRespuesta.nombreArchivo;
                                let guardarLocal = new Promise((resolve, reject) => {
                                    if (this.id == 0) {
                                        this.cnt++;
                                        this.id = this.cnt;
                                        this.addRowTbl({
                                            ...tmp,
                                            nFila: this.cnt
                                        }).then(resolve).catch(reject);
                                    } else {
                                        $('#trColaborador' + this.id).find('td:eq(1)').find('img').attr('src', `./main/img/colaboradores/${tmp.foto}`);
                                        $('#trColaborador' + this.id).find('td:eq(2)').html(`${tmp.nombres} ${tmp.apellidos}`);
                                        $('#trColaborador' + this.id).find('td:eq(3)').html(tmp.agencia);
                                        $('#trColaborador' + this.id).find('td:eq(4)').html(tmp.labelAgencia);
                                        $('#trColaborador' + this.id).find('td:eq(5)').html(tmp.labelGerencia);
                                        $('#trColaborador' + this.id).find('td:eq(6)').html(tmp.labelCargo);
                                        $('#trColaborador' + this.id).find('td:eq(8)').html(tmp.fechaActualizacion);
                                        resolve();
                                    }
                                });

                                guardarLocal.then(() => {
                                    this.colaboradores[this.id] = {
                                        ...tmp
                                    };
                                    tblColaboradores.columns.adjust().draw();
                                    $('#mdlColaborador').modal('hide');
                                }).catch(generalMostrarError);
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    saveDB: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            fetchActions.setWFiles({
                modulo: 'RRHH',
                archivo: 'rrhhGuardarColaborador',
                datos: tmpDatos
            }).then(resolve).catch(reject);
        });
    },
    addRowTbl: function ({
        nFila,
        foto,
        nombres,
        apellidos,
        agencia,
        labelAgencia,
        labelGerencia,
        labelCargo,
        fechaActualizacion
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblColaboradores.row.add([
                    nFila,
                    "<div class='contenedorImgTbl'>" +
                    "<img src='./main/img/colaboradores/" + foto + "' />" +
                    "</div>",
                    `${nombres} ${apellidos}`,
                    agencia,
                    labelAgencia,
                    labelGerencia,
                    labelCargo,
                    '',
                    fechaActualizacion,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configColaborador.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configColaborador.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = 'trColaborador' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let label = `${this.colaboradores[id].nombres} ${this.colaboradores[id].apellidos}`;
        Swal.fire({
            title: `Eliminar colaborador`,
            icon: 'warning',
            html: `¿Seguro/a que quieres <b>eliminar</b> al colaborador <strong>${label}</strong>?.`,
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: "RRHH",
                    archivo: "rrhhEliminarColaborador",
                    datos: {
                        id: this.colaboradores[id].id,
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta) {
                            case 'EXITO':
                                tblColaboradores.row('#trColaborador' + id).remove().draw();
                                delete this.colaboradores[id];
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    cancel: function () {},
}