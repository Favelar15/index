<div class="pagetitle">
    <h1>Administración de colaboradores</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">RRHH</li>
            <li class="breadcrumb-item">Administración</li>
            <li class="breadcrumb-item active">Colaboradores</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="users-tab" data-bs-toggle="tab" data-bs-target="#users" type="button" role="tab" aria-controls="users" aria-selected="true">
                Colaboradores
            </button>
        </li>
        <li class="nav-item ms-auto" id="userButtonsTabs">
            <div class="tabButtonContainer" id="botonesPrincipal">
                <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configColaborador.edit();">
                    <i class="fas fa-plus-circle"></i> Colaborador
                </button>
            </div>
        </li>
    </ul>
    <div class="tab-content pt-2">
        <div class="tab-pane fade show active" id="users" role="tabpanel" aria-labelledby="users-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblColaboradores" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Foto</th>
                                <th>Nombre completo</th>
                                <th>Código de agencia</th>
                                <th>Oficina</th>
                                <th>Gerencia</th>
                                <th>Cargo</th>
                                <th>Fecha de contratación</th>
                                <th>Última actualización</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="user" role="tabpanel" aria-labelledby="user-tab">
        </div>
    </div>
</section>
<div class="modal fade" id="mdlColaborador" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Datos de colaborador</h5>
            </div>
            <form id="frmColaborador" class="needs-validation" name="frmColaborador" method="POST" novalidate action="javascript:configColaborador.save();">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <form id="frmUsuario" name="frmUsuario" enctype="multipart/form-data" method="POST" class="needs-validation" novalidate accept-charset="utf-8" action="javascript:configColaborador.save();">
                                <div class="row">
                                    <div class="col-lg-3 col-xl-3" style="padding-left: 40px;">
                                        <div class="wrap-custom-file">
                                            <input type="file" name="userImage" id="userImage" accept=".gif, .jpg, .png .jpeg" />
                                            <label for="userImage" style="background-size: 100% 100%; background-image:url('./main/img/colaboradores/avatar.jpg');" id="labelUserImage">
                                                <span><i class="fas fa-download"></i> Subir foto </span>
                                            </label>
                                        </div>
                                        <p style="font-size: 12px;">*mínimo 260px x 260px</p>
                                    </div>
                                    <div class="col-lg-9 col-xl-9">
                                        <div class="row">
                                            <div class="col-lg-4 col-xl-4">
                                                <label for="txtNombres" class="form-label">Nombres <span class="requerido">*</span></label>
                                                <input type="text" id="txtNombres" name="txtNombres" placeholder="" title="" class="form-control" required value="">
                                                <div class="invalid-feedback">
                                                    Ingrese los nombres.
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-xl-4">
                                                <label for="txtApellidos" class="form-label">Apellidos <span class="requerido">*</span></label>
                                                <input type="text" id="txtApellidos" name="txtApellidos" placeholder="" title="" required class="form-control" value="">
                                                <div class="invalid-feedback">
                                                    Ingrese los apellidos
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-xl-4">
                                                <label for="cboAgencia" class="form-label">Agencia <span class="requerido">*</span></label>
                                                <div class="form-group has-validation">
                                                    <select id="cboAgencia" name="cboAgencia" title="" class="selectpicker form-control cboAgencia" required data-live-search="true">
                                                        <option selected disabled value="0">seleccione</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col-xl-4">
                                                <label for="cboGerencia" class="form-label">Gerencia <span class="requerido">*</span></label>
                                                <div class="form-group has-validation">
                                                    <select id="cboGerencia" name="cboGerencia" title="" class="selectpicker form-control cboGerencia" required data-live-search="true">
                                                        <option selected disabled value="0">seleccione</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-xl-4">
                                                <label for="cboCargo" class="form-label">Cargo <span class="requerido">*</span></label>
                                                <div class="form-group has-validation">
                                                    <select id="cboCargo" name="cboCargo" title="" class="selectpicker form-control cboCargo" required data-live-search="true">
                                                        <option selected disabled value="0">seleccione</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="submit" form="frmColaborador" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<?php
$_GET["js"] = ['rrhhColaboradores'];
?>