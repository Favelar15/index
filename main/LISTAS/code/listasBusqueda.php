<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();

if (isset($_SESSION["index"]) && $_SESSION['index']->locked) {
    include "../../code/connectionSqlServer.php";

    $rolesSinRestriccion = ['Gerencia de Oficialía','Analista de cumplimiento'];
    $rolesActuales = $_SESSION["index"]->roles;

    require_once './Models/registro.php';

    $arrCondicion = [];

    !empty($input["documento"]) && array_push($arrCondicion, "numeroDocumento='" . $input["documento"] . "'");
    !empty($input["nombre"]) && array_push($arrCondicion, "nombreCompleto LIKE '%" . $input["nombre"] . "%'");

    $condicion = implode(" OR ",$arrCondicion);

    $registro = new registro();
    $restringido = true;

    foreach($rolesActuales as $rol){
        if(in_array($rol->label,$rolesActuales)){
            $registro = false;
        }
    }

    $resultados =[];

    if($restringido){
        $resultados = $registro->obtenerRegistrosRestringidos($condicion);
    }
    else{
        $resultados = $registro->obtenerRegistros($condicion);
    }

    $respuesta->{"respuesta"} = 'EXITO';
    $respuesta->{"restringido"} = $restringido;
    $respuesta->{"resultados"} = $resultados;

    $conexion = null;
} else {
    $respuesta->{"respuesta"} = "SESION";
}


echo json_encode($respuesta);
