<?php
class registro
{
    public $id;
    public $numeroDocumento;
    public $nombreCompleto;
    public $nit;
    public $edad;
    public $fechaNacimiento;
    public $fechaIngreso;
    public $motivo;
    public $fuente;
    public $unidad;
    public $delito;
    public $calidad;
    public $fechaInicioProcesoJudicial;
    public $medidasDebidaDiligenciaRequerida;
    public $fechaRegistro;
    public $usuario;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        !empty($this->fechaNacimiento) && $this->fechaNacimiento = date('d-m-Y', strtotime($this->fechaNacimiento));
        !empty($this->fechaIngreso) && $this->fechaIngreso = date('d-m-Y', strtotime($this->fechaIngreso));
        !empty($this->fechaInicioProcesoJudicial) && $this->fechaInicioProcesoJudicial = date('d-m-Y', strtotime($this->fechaInicioProcesoJudicial));
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y', strtotime($this->fechaRegistro));
    }

    public function obtenerRegistros($condicion)
    {
        $query = "SELECT * FROM listas_viewsDatosRegistros where " . $condicion;
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

    public function obtenerRegistrosRestringidos($condicion)
    {
        $query = "SELECT id,numeroDocumento,nombreCompleto,nit,edad,fechaNacimiento,fechaIngreso FROM listas_viewsDatosRegistros where " . $condicion;
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }
}
