<?php
require_once "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
} else {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';
}

session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include "../../code/connectionSqlServer.php";

        class construccionPDF extends TCPDF
        {
            function configuracionDocumento()
            {
                $this->SetCreator(PDF_CREATOR);
                $this->SetAuthor('Departamento IT');
                $this->SetTitle('Resultados de búsqueda en listas internas');
                $this->SetSubject('ACACYPAC');
                $this->SetKeywords('TCPDF, PDF, Resultados de búsqueda en listas internas');

                $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

                $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            }

            function setMargin()
            {
                $this->SetMargins(15, 15, 15, 15);
                $this->SetHeaderMargin(4);
                $this->SetFooterMargin(PDF_MARGIN_FOOTER);
            }

            function Header()
            {
                $this->setFont('dejavusans', 'B', 10);
                // $this->Write(0, 'N° de cliente: ' . $this->codigoCliente, '', 0, 'R', true, 0, false, false, 0);
            }

            function Footer()
            {
            }

            public function setDatosGenerales($input, $usuario, $agenciaActual)
            {
                $this->Cell(50, 15, '', 1, 0, 'L', 0);
                $this->Image("../../img/logoAcacypac.png", 33, 15, 15, 0, 'PNG', '', '');
                $this->SetTextColor(0);
                $this->SetFillColor(255, 255, 255);
                $this->Multicell(199, 15, "ASOCIACIÓN COOPERATIVA DE AHORRO, CRÉDITO Y PRODUCCIÓN \nAGROPECUARIA COMUNAL DE NUEVA CONCEPCIÓN DE RESPONSABILIDAD LIMITADA", 1, 'C', 1, 0, '', '', true, 0, false, true, 15, 'M');
                $this->ln(17);

                $this->setFont('dejavusans', 'B', 12);
                $this->Write(0, 'RESULTADO DE BÚSQUEDA EN LISTAS DE VIGILANCIA INTERNA', '', 0, 'C', true, 0, false, false, 0);
                $this->ln(2);

                $this->SetFillColor(0, 95, 85);
                $this->SetTextColor(255);
                $this->SetDrawColor(0, 95, 85);
                $this->SetLineWidth(0.3);
                $this->SetFont('', 'B');
                // $this->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 193, 7)));

                $this->Cell(249, 7, 'DATOS GENERALES DE LA BÚSQUEDA', 1, 0, 'C', 1);
                $this->ln();
                $this->SetFont('', '');
                $this->SetTextColor(255);
                $this->SetFillColor(17, 142, 129);
                $this->Cell(50, 6, 'Usuario responsable:', 1, 0, 'L', 1);
                $this->SetTextColor(0);
                $this->Cell(199, 6, $usuario, 1, 0, 'L', 0);
                $this->ln();
                $this->SetTextColor(255);
                $this->Cell(50, 6, 'Documento buscado:', 1, 0, 'L', 1);
                $this->SetTextColor(0);
                $this->Cell(50, 6, $input["documentoBuscado"], 1, 0, 'L', 0);
                $this->SetTextColor(255);
                $this->Cell(50, 6, 'Nombre buscado:', 1, 0, 'L', 1);
                $this->SetTextColor(0);
                $this->Cell(99, 6, $input["nombreBuscado"], 1, 0, 'L', 0);
                $this->ln();
                $this->SetTextColor(255);
                $this->Cell(50, 6, 'Agencia:', 1, 0, 'L', 1);
                $this->SetTextColor(0);
                $this->Cell(100, 6, $agenciaActual, 1, 0, 'L', 0);
                $this->SetTextColor(255);
                $this->Cell(50, 6, 'Fecha de búsqueda:', 1, 0, 'L', 1);
                $this->SetTextColor(0);
                $this->Cell(49, 6, date('d-m-Y h:i a'), 1, 0, 'L', 0);
                $this->ln();
            }

            public function setRegistros($registros)
            {
                $tbl = '<style>table th{background-color:rgb(0, 95, 85); color:white; border:1px solid black; font-weight:bold;} table td{border:1px solid black;}</style><table cellpadding="3" cellspacing="0" style="text-align:center;" nobr="true">' .
                    '<thead>' .
                    '<tr>' .
                    '<th style="width: 883px; text-align:center; font-size:13px;" colspan="7">Coincidencias encontradas</th>' .
                    '</tr>' .
                    '<tr>' .
                    '<th style="width: 40px; text-align:center; font-size:12px;">N°</th>' .
                    '<th style="width: 120px; text-align:center; font-size:12px;">Documento</th>' .
                    '<th style="width: 301px; text-align:center; font-size:12px;">Nombre completo</th>' .
                    '<th style="width: 135px; text-align:center; font-size:12px;">NIT</th>' .
                    '<th style="width: 60px; text-align:center; font-size:12px;">Edad</th>' .
                    '<th style="width: 117px; text-align:center; font-size:10px;">Fecha de nacimiento</th>' .
                    '<th style="width: 110px; text-align:center; font-size:10px;">Fecha de ingreso</th>' .
                    '</tr>' .
                    '</thead>' .
                    '<tbody>';

                $cnt = 0;

                foreach ($registros as $key => $registro) {
                    if ($cnt <= 12) {
                        $cnt++;
                        $tbl .= '<tr>' .
                            '<td style="width: 40px;">' . $cnt . '</td>' .
                            '<td style="width: 120px;">' . $registro["numeroDocumento"] . '</td>' .
                            '<td style="width: 301px;">' . $registro["nombreCompleto"] . '</td>' .
                            '<td style="width: 135px;">' . $registro["nit"] . '</td>' .
                            '<td style="width: 60px;">' . $registro["edad"] . '</td>' .
                            '<td style="width: 117px;">' . $registro["fechaNacimiento"] . '</td>' .
                            '<td style="width: 110px;">' . $registro["fechaIngreso"] . '</td>' .
                            '</tr>';
                    }
                }

                $tbl .= '</tbody>' .
                    '</table>';
                $this->SetFont('dejavusans', '', 7.8);
                // $this->SetTextColor(0);
                $this->writeHTML($tbl, true, false, true, false, '');
                $this->ln();

                $this->SetFillColor(224, 235, 255);
                $this->SetTextColor(0);
                $this->SetFont('');
            }
        }

        $path = __DIR__ . '\..\docs\/';

        $usuario = $_SESSION['index']->nombres . ' ' . $_SESSION['index']->apellidos;
        $agenciaActual = $_SESSION['index']->agenciaActual->nombreAgencia;

        $nombreArchivo = date('d-m-Y H-i').".pdf";

        $pdf = new construccionPDF('L', PDF_UNIT, 'LETTER', true, 'UTF-8', false);
        // $pdf->codigoCliente = $solicitud->codigoCliente;
        $pdf->configuracionDocumento();
        $pdf->setMargin();

        $pdf->SetAutoPageBreak(TRUE, 35);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->AddPage();

        $pdf->setFont('dejavusans', '', 10);
        $pdf->setDatosGenerales($input, $usuario, $agenciaActual);

        $pdf->ln(10);

        $pdf->setRegistros($input["resultados"]);

        $pdf->Output($path . $nombreArchivo, 'F');


        $respuesta->{'respuesta'} = 'EXITO';
        $respuesta->{"nombreArchivo"} = $nombreArchivo;

        $conexion = null;
    }
} else {
    $respuesta->{'respuesta'} = 'SESION';
}

echo json_encode($respuesta);
