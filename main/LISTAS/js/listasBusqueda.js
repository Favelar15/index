let tblResultados;

window.onload = () => {
    $('.preloader').fadeOut('fast');
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblResultados").length) {
                tblResultados = $("#tblResultados").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2, 3, 4],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblResultados.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configBusqueda = {
    tblHeader: `<table id="tblResultados" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;"><thead>`,
    tblFooter: `</tbody></table>`,
    cnt: 0,
    resultados: {},
    campoDocumento: document.getElementById('txtDocumento'),
    campoNombre: document.getElementById('txtNombre'),
    containerMensaje: document.getElementById('containerMessage'),
    mainContainerResultados: document.getElementById('mainContainerResultados'),
    containerResultados: document.getElementById('containerResultados'),
    stBusqueda: document.getElementById('stBusqueda'),
    documentoBuscado: null,
    nombreBuscado: null,
    restringido: true,
    search: function () {
        formActions.validate('frmBusqueda').then(({
            errores
        }) => {
            if (errores == 0) {
                this.cnt = 0;
                this.resultados = {};
                this.restringido = true;
                if (this.campoDocumento.value || this.campoNombre.value) {
                    fetchActions.set({
                        archivo: 'listasBusqueda',
                        modulo: 'LISTAS',
                        datos: {
                            documento: this.campoDocumento.value,
                            nombre: this.campoNombre.value
                        }
                    }).then((datosRespuesta) => {
                        if (datosRespuesta.respuesta) {
                            switch (datosRespuesta.respuesta) {
                                case 'EXITO':
                                    let tmpTabla = this.tblHeader;
                                    let arrBuscados = [];
                                    this.campoDocumento.value && (arrBuscados.push(`Documento: <strong>${this.campoDocumento.value}</strong>`));
                                    this.documentoBuscado = this.campoDocumento.value ? this.campoDocumento.value : null;
                                    this.campoNombre.value && (arrBuscados.push(`Nombre: <strong>${this.campoNombre.value}</strong>`));
                                    this.nombreBuscado = this.campoNombre.value ? this.campoNombre.value : null;

                                    this.stBusqueda.innerHTML = arrBuscados.join(', ');
                                    this.restringido = datosRespuesta.restringido;

                                    if (datosRespuesta.restringido) {
                                        tmpTabla += `<tr>
                                        <th>N°</th>
                                        <th>Documento</th>
                                        <th>Nombre completo</th>
                                        <th>NIT u otro documento</th>
                                        <th>Edad</th>
                                        <th>Fecha de nacimiento</th>
                                        <th>Fecha de ingreso a la lista</th>
                                        </tr></thead><tbody>`;
                                    } else {
                                        tmpTabla += `<tr>
                                        <th>N°</th>
                                        <th>Documento</th>
                                        <th>Nombre completo</th>
                                        <th>NIT u otro documento</th>
                                        <th>Edad</th>
                                        <th>Fecha de nacimiento</th>
                                        <th>Fecha de ingreso a la lista</th>
                                        <th>Motivo</th>
                                        <th>Fuente</th>
                                        <th>Unidad</th>
                                        <th>Delito</th>
                                        <th>Calidad</th>
                                        <th>Fecha de inicio de proceso judicial</th>
                                        <th>Medidas de debida diligencia</th>
                                        <th>Fecha de registro en sistema</th>
                                        <th>Usuario</th>
                                        </tr></thead><tbody>`;
                                    }

                                    datosRespuesta.resultados.forEach(registro => {
                                        this.cnt++;
                                        this.resultados[this.cnt] = {
                                            ...registro
                                        };
                                        for (let key in registro) {
                                            if (registro[key] == null) {
                                                registro[key] = '';
                                            }
                                        }
                                        if (datosRespuesta.restringido) {
                                            tmpTabla += `<tr>
                                            <td>${this.cnt}</td>
                                            <td>${registro.numeroDocumento}</td>
                                            <td>${registro.nombreCompleto}</td>
                                            <td>${registro.nit}</td>
                                            <td>${registro.edad}</td>
                                            <td>${registro.fechaNacimiento}</td>
                                            <td>${registro.fechaIngreso}</td>
                                            </tr>`;
                                        } else {
                                            tmpTabla += `<tr>
                                            <td>${this.cnt}</td>
                                            <td>${registro.numeroDocumento}</td>
                                            <td>${registro.nombreCompleto}</td>
                                            <td>${registro.nit}</td>
                                            <td>${registro.edad}</td>
                                            <td>${registro.fechaNacimiento}</td>
                                            <td>${registro.fechaIngreso}</td>
                                            <td>${registro.motivo}</td>
                                            <td>${registro.fuente}</td>
                                            <td>${registro.unidad}</td>
                                            <td>${registro.delito}</td>
                                            <td>${registro.calidad}</td>
                                            <td>${registro.fechaInicioProcesoJudicial}</td>
                                            <td>${registro.medidasDebidaDiligenciaRequerida}</td>
                                            <td>${registro.fechaRegistro}</td>
                                            <td>${registro.usuario}</td>
                                            </tr>`;
                                        }
                                    });

                                    tmpTabla += this.tblFooter;
                                    this.containerMensaje.style.display = 'none';
                                    $("#" + this.mainContainerResultados.id).fadeIn("fast", function () {
                                        configBusqueda.containerResultados.innerHTML = tmpTabla;
                                        initTables().then(() => {
                                            // tblResultados.columns.adjust().draw();
                                        });
                                    });
                                    break;
                                default:
                                    generalMostrarError(datosRespuesta);
                                    break;
                            }
                        } else {
                            generalMostrarError(datosRespuesta);
                        }
                        // console.log(datosRespuesta);
                    }).catch(generalMostrarError);
                } else {
                    Swal.fire({
                        title: "¡Atención!",
                        html: "No hay datos para buscar",
                        icon: "warning"
                    });
                }
            }
        }).catch(generalMostrarError);
    },
    print: function () {
        if (this.documentoBuscado != null || this.nombreBuscado != null) {
            fetchActions.set({
                archivo: 'listasConstruirPDF',
                modulo: 'LISTAS',
                datos: {
                    resultados: this.resultados,
                    documentoBuscado: this.documentoBuscado,
                    nombreBuscado: this.nombreBuscado,
                    restringido: this.restringido
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta) {
                        case "EXITO":
                            window.open(`./main/LISTAS/docs/${datosRespuesta.nombreArchivo}`);
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            Swal.fire({
                title: "",
                html: "Debe realizar una búsqueda para imprimir los resultados.",
                icon: "info"
            });
        }
    }
}