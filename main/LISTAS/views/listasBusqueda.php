<div class="pagetitle">
    <h1>Buscador en listas de vigilancia interna</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Listas</li>
            <li class="breadcrumb-item">Internas</li>
            <li class="breadcrumb-item active">Buscar</li>
        </ol>
    </nav>
</div>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <form action="javascript:configBusqueda.search();" id="frmBusqueda" name="frmBusqueda" accept-charset="utf-8" method="POST">
                <div class="row">
                    <div class="col-lg-1 col-xl-1"></div>
                    <div class="col-lg-10 col-xl-10">
                        <div class="row">
                            <div class="col-lg-4 col-xl-4">
                                <label for="txtDocumento" class="form-label">Documento</label>
                                <input type="text" id="txtDocumento" class="form-control" name="txtDocumento">
                            </div>
                            <div class="col-lg-4 col-xl-4 mayusculas">
                                <label for="txtNombre" class="form-label">Nombre</label>
                                <input type="text" id="txtNombre" class="form-control" name="txtNombre">
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <div class="row">
                                    <div class="col-lg-6 col-xl-6">
                                        <div class="d-grid gap-2" style="margin-top: 28px;">
                                            <button type="submit" class="btn btn-block btn-outline-primary">
                                                <i class="fas fa-search"></i> Buscar
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xl-6">
                                        <div class="d-grid gap-2" style="margin-top: 28px;">
                                            <button type="button" class="btn btn-block btn-outline-secondary" onclick="configBusqueda.print();">
                                                <i class="fas fa-print"></i> Imprimir
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-xl-1"></div>
                </div>
            </form>
            <hr class="mb-3 mt-3">
        </div>
        <div class="col-lg-12 col-xl-12" id="containerMessage">
            <h4>ACACYPAC N.C. DE R.L.</h4>
            <h5>Sistema de consulta en listas internas de cautela</h5>
            <hr class="bg-success">
            <p class="text-success">Por favor ingresa los datos que deseas consultar</p>
        </div>
        <div class="col-lg-12 col-xl-12" id="mainContainerResultados" style="display: none;">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <h4>Resultados de la búsqueda</h4>
                    <h5>Valores buscados - <strong id="stBusqueda"></strong></h5>
                </div>
                <div class="col-lg-12 col-xl-12" id="containerResultados">

                </div>
            </div>
        </div>
    </div>
</section>
<?php
// echo json_encode($_SESSION['index']);
$_GET['js'] = ['listasBusqueda'];
