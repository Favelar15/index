var tipoApartado = localStorage.getItem('tipoApartado') != null ? localStorage.getItem('tipoApartado') : '',
    idApartado = localStorage.getItem('idApartado') != null ? localStorage.getItem('idApartado') : '',
    tipoPermiso = localStorage.getItem('tipoPermiso') != null ? localStorage.getItem('tipoPermiso') : '';

let tiposDocumento = {},
    datosGeograficos = {};

$(function () {
    // $(".preloader").fadeOut("fast");
    $(".mayusculas input[type='text']").css("text-transform", "uppercase");
    $(".mayusculas input[type='text']").keyup(function () {
        this.value = this.value.toUpperCase();
    });

    $('button[data-bs-toggle="tab"]').on('shown.bs.tab', function (e) {
        $.fn.dataTable.tables({
            visible: true,
            api: true
        }).columns.adjust();

        $("[data-toggle='tooltip']").tooltip("dispose");
        $("[data-toggle='tooltip']").tooltip();
    });

    const camposDUI = document.querySelectorAll('input.DUI');
    const camposNIT = document.querySelectorAll('input.NIT');
    const camposTelefono = document.querySelectorAll('input.telefono');
    const camposCuentaAcacypac = document.querySelectorAll('input.cuentaAcacypac');
    const camposMatriculaHipoteca = document.querySelectorAll('input.matriculaHipoteca');
    const camposNumPrestamos = document.querySelectorAll('input.prestamo')

    if (camposDUI.length > 0) {
        camposDUI.forEach(campo => {
            $("#" + campo.id).mask('00000000-0');
        });
    }

    if (camposNIT.length > 0) {
        camposNIT.forEach(campo => {
            $("#" + campo.id).mask('0000-000000-000-0');
        });
    }

    if (camposTelefono.length > 0) {
        camposTelefono.forEach(campo => {
            $("#" + campo.id).mask('0000-0000');
        });
    }

    if (camposNumPrestamos.length > 0) {
        camposNumPrestamos.forEach(campo => {
            $("#" + campo.id).mask('000-000-00000000000');
        });
    }

    if (camposCuentaAcacypac.length > 0) {
        camposCuentaAcacypac.forEach(campo => {
            $("#" + campo.id).mask('000-000-00000000000000000');
        });
    }

    if (camposMatriculaHipoteca.length > 0) {
        camposMatriculaHipoteca.forEach(campo => $("#" + campo.id).mask('00000000-00000000000'));
    }


    if ($("#reloadCaptcha").length) {
        $("#reloadCaptcha").click(function () {
            var captchaImage = $('#captcha').attr('src');
            captchaImage = captchaImage.substring(0, captchaImage.lastIndexOf("?"));
            captchaImage = captchaImage + "?rand=" + Math.random() * 1000;
            $('#captcha').attr('src', captchaImage);
        });
    }

    $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function (e) {
        $.fn.dataTable.tables({
            visible: true,
            api: true
        }).columns.adjust();

        $("[data-toggle='tooltip']").tooltip("dispose");
        $("[data-toggle='tooltip']").tooltip();
    });

    $('.modal').on('shown.bs.modal', function (e) {
        $.fn.dataTable.tables({
            visible: true,
            api: true
        }).columns.adjust();
    });

    //Para dar formato a campos de fecha -->
    $(".fechaAbierta").each(function () {
        $(this).datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
        });
    });

    $(".fechaHoraAbierta").each(function () {
        $(this).datetimepicker({
            format: "dd-mm-yyyy hh:ii",
            todayHighlight: true,
            autoclose: true,
            minuteStep: 1
        });
    });

    $(".fechaFinLimitado").each(function () {
        $(this).datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            endDate: 'now'
        });
    });

    $(".fechaInicioLimitado").each(function () {
        $(this).datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: '-1m'
        });
    });

    $('.fechaMesInicioFinLimitado').each(function () {
        $(this).datepicker({
            format: 'mm-yyyy',
            todayHighlight: true,
            autoclose: true,
            viewMode: "months",
            minViewMode: "months",
            startDate: "-2m",
            endDate: "+12m"
        });
    });
    //<--Para dar formato a campos de fecha

    $(".readonly").on('keydown change paste focus blur', function (e) {
        if (e.keyCode != 9) { // ignore tab
            e.preventDefault();
        }
    });

    if (localStorage.getItem("logged")) {
        const gettingMenu = new Promise((resolve, reject) => {
            let menuGuardado = (localStorage.getItem('menu') != null && localStorage.getItem('menu') != 'undefined') ? JSON.parse(localStorage.getItem('menu')) : [];
            // let menuGuardado = [];

            if (menuGuardado.length > 0) {
                resolve(menuGuardado);
            } else {
                fetchActions.getCats({
                    modulo: '',
                    archivo: 'getMenu',
                    solicitados: ['all']
                }).then(({
                    menu
                }) => {
                    localStorage.setItem('menu', JSON.stringify(menu));
                    // console.log(menu)
                    resolve(menu);
                }).catch(reject);
            }
        });

        gettingMenu.then(userActions.constructMenu).then(() => {
            let page = getParameterByName('page'),
                globalPages = ['userProfile', 'directorio', 'noticias', 'listasBusqueda'];
            if (page.length && !globalPages.includes(page)) {
                let accessGranted = false;

                if (localStorage.getItem('menu') && localStorage.getItem('tipoApartado') && localStorage.getItem('idApartado')) {
                    let menu = JSON.parse(localStorage.getItem("menu")),
                        tmpIdApartado = atob(decodeURIComponent(localStorage.getItem('idApartado'))),
                        tmpTipo = localStorage.getItem("tipoApartado"),
                        tmpPermiso = {},
                        usuario = localStorage.getItem('idUser'),
                        roles = JSON.parse(localStorage.getItem('roles'));

                    menu.forEach(modulo => {
                        modulo.apartados.forEach(apartado => {
                            if (tmpTipo == 'Apartado') {
                                if (apartado.id == tmpIdApartado) {
                                    tmpPermiso = {
                                        ...apartado.permisos
                                    };
                                }
                            } else {
                                for (let key in apartado.subApartados) {
                                    if (apartado.subApartados[key].id == tmpIdApartado) {
                                        tmpPermiso = {
                                            ...apartado.subApartados[key].permisos
                                        };
                                    }
                                }
                            }
                        });
                    });

                    for (let key in tmpPermiso) {
                        if (tmpPermiso[key].grupo == 'USER' && tmpPermiso[key].asignacion == usuario) {
                            accessGranted = true;
                        } else if (roles.includes(tmpPermiso[key].asignacion)) {
                            accessGranted = true;
                        }
                    }
                }

                if (!accessGranted) {
                    Swal.fire({
                        title: "¿Perdido/a?",
                        html: "Ups, parece que no puedes estar aquí.<br />Te ayudaremos a salir.",
                        icon: "warning"
                    }).then(() => {
                        window.top.location.href = './';
                    });
                } else {
                    idApartado = localStorage.getItem('idApartado');
                    tipoApartado = localStorage.getItem('tipoApartado');
                    tipoPermiso = localStorage.getItem('tipoPermiso');
                }
            }
        }).catch(generalMostrarError);
    }
})

const formActions = {
    clean: function (idForm) {
        return new Promise((resolve, reject) => {
            try {
                if (idForm) {
                    let inputs = document.querySelectorAll(`#${idForm} input`),
                        cbos = document.querySelectorAll(`#${idForm} select`),
                        frames = document.querySelectorAll(`#${idForm} iframe`),
                        textArea = document.querySelectorAll(`#${idForm} textarea`),
                        camposValidados = document.querySelectorAll(".was-validated");

                    // msjError.forEach(msj => {
                    //     msj.style.display = "none";
                    // });

                    camposValidados.forEach(campo => {
                        campo.classList.remove("was-validated");
                    });

                    inputs.forEach(input => {
                        input.value = "";
                        input.checked = false;
                        if (input.className.includes("fechaAbierta") || input.className.includes("fechaInicioLimitado") || input.className.includes('fechaFinLimitado')) {
                            $("#" + input.id).datepicker("update", "");
                            // $("#" + input.id).datepicker("update", new Date());
                        } else if (input.className.includes("fechaHoraAbierta")) {
                            if ($("#" + input.id).data("datetimepicker") == undefined) {
                                // $("#" + input.id).datetimepicker({
                                //     format: "dd-mm-yyyy hh:ii",
                                //     todayHighlight: true,
                                //     autoclose: true,
                                //     minuteStep: 1
                                // });
                                $("#" + input.id).datetimepicker("update", "");
                            } else {
                                $("#" + input.id).datetimepicker("update", "");
                            }
                        }
                    });

                    cbos.forEach(cbo => {
                        if (cbo.id) {
                            cbo.value = cbo.options[0].value;
                            $("#" + cbo.id).trigger("onchange");
                            if (cbo.className.includes("selectpicker")) {
                                $("#" + cbo.id).selectpicker("refresh");
                                $("#" + cbo.id).selectpicker("val", cbo.options[0].value);
                            }
                        }
                    });

                    frames.forEach(frame => {
                        frame.src = "";
                    });

                    textArea.forEach(text => {
                        text.value = "";
                    });
                }
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    validate: function (idForm) {
        return new Promise((resolve, reject) => {
            let response = {
                errores: 0,
                camposError: []
            };

            try {
                if (idForm) {
                    let camposValidados = document.querySelectorAll(".was-validated");

                    camposValidados.forEach(campo => {
                        campo.classList.remove("was-validated");
                    });
                }
                resolve(response);
            } catch (err) {
                console.log(err, 'Este es el error');
                reject(err.message);
            }
        });
    },
    getJSON: function (idForm) {
        let datosCompletos = {},
            inputs = document.querySelectorAll(`#${idForm} input.form-control`),
            cbos = document.querySelectorAll(`#${idForm} select.form-control`),
            othersCbos = document.querySelectorAll(`#${idForm} select.form-select`),
            textarea = document.querySelectorAll(`#${idForm} textarea.form-control`);

        inputs.forEach(input => {
            if (input.id) {
                let label = $("#" + input.id).parent().parent().find("label").length > 0 ? $("#" + input.id).parent().parent().find("label:eq(0)").text() : ($("#" + input.id).parent().parent().parent().find("label").length > 0 ? $("#" + input.id).parent().parent().parent().find("label:eq(0)").text() : "No definido");
                label = label.replace(':', '').replace('*', '').trim();
                datosCompletos[input.id] = {
                    value: input.value,
                    id: input.id,
                    label,
                    labelOption: 'undefined'
                };
            }
        });

        textarea.forEach(input => {
            if (input.id) {
                let label = $("#" + input.id).parent().parent().find("label").length > 0 ? $("#" + input.id).parent().parent().find("label:eq(0)").text() : ($("#" + input.id).parent().parent().parent().find("label").length > 0 ? $("#" + input.id).parent().parent().parent().find("label:eq(0)").text() : "No definido");
                label = label.replace(':', '').replace('*', '').trim();
                datosCompletos[input.id] = {
                    value: input.value,
                    id: input.id,
                    label,
                    labelOption: 'undefined'
                };
            }
        });

        cbos.forEach(cbo => {
            if (cbo.id) {
                let label = $("#" + cbo.id).parent().parent().find("label").length > 0 ? $("#" + cbo.id).parent().parent().find("label:eq(0)").text() : ($("#" + cbo.id).parent().parent().parent().find("label").length > 0 ? $("#" + cbo.id).parent().parent().parent().find("label:eq(0)").text() : "No definido"),
                    labelOption = cbo.options[cbo.options.selectedIndex].text;

                label = label.replace(':', '').replace('*', '').trim();
                datosCompletos[cbo.id] = {
                    value: cbo.value,
                    id: cbo.id,
                    label,
                    labelOption
                };
            }
        });

        othersCbos.forEach(cbo => {
            if (cbo.id) {
                let label = $("#" + cbo.id).parent().parent().find("label").length > 0 ? $("#" + cbo.id).parent().parent().find("label:eq(0)").text() : ($("#" + cbo.id).parent().parent().parent().find("label").length > 0 ? $("#" + cbo.id).parent().parent().parent().find("label:eq(0)").text() : "No definido"),
                    labelOption = cbo.options[cbo.options.selectedIndex].text;

                label = label.replace(':', '').replace('*', '').trim();
                datosCompletos[cbo.id] = {
                    value: cbo.value,
                    id: cbo.id,
                    label,
                    labelOption
                };
            }
        });

        return datosCompletos;
    },
    manualValidate: function (idContainer) {
        document.getElementById(idContainer).classList.add('was-validated')

        return $("#" + idContainer)[0].checkValidity();
    }
}

const fetchActions = {
    set: function ({
        modulo = '',
        archivo = '',
        datos = {}
    }) {
        return new Promise((resolve, reject) => {
            $(".preloader").fadeIn("fast");
            (async () => {
                try {
                    let tmpModulo = modulo.length > 0 ? `${modulo}/` : '';
                    let init = {
                            method: "POST",
                            headers: {
                                "Content-type": "application/json",
                            },
                            body: JSON.stringify(datos)
                        },
                        response = await fetch(`./main/${tmpModulo}code/${archivo}.php`, init);
                    if (response.ok) {
                        $(".preloader").fadeOut("fast");
                        let datosRespuesta = await response.json();
                        resolve(datosRespuesta);
                    } else {
                        reject(response.statusText);
                    }
                } catch (err) {
                    reject(err.message);
                }
            })();
        });
    },
    getCats: function ({
        modulo = null,
        archivo = null,
        solicitados = []
    }) {
        return new Promise((resolve, reject) => {
            (async () => {
                try {
                    $(".preloader").fadeIn("fast");
                    let init = {
                            method: "GET",
                            headers: {
                                "Content-type": "application/json"
                            }
                        },
                        response = await fetch(`./main/${modulo}/code/${archivo}.php?q=${solicitados.join("@@@")}`, init);
                    if (response.ok) {
                        $(".preloader").fadeOut("fast");
                        let datosRespuesta = await response.json();
                        resolve(datosRespuesta);
                    } else {
                        reject(response.statusText);
                    }
                } catch (err) {
                    reject(err.message);
                }
            })();
        });
    },
    setWFiles: function ({
        modulo = '',
        archivo = '',
        datos = new FormData()
    }) {
        return new Promise((resolve, reject) => {
            $(".preloader").fadeIn("fast");
            (async () => {
                try {
                    let tmpModulo = modulo.length > 0 ? `${modulo}/` : '';
                    let init = {
                            method: "POST",
                            headers: {},
                            body: datos
                        },
                        response = await fetch(`./main/${tmpModulo}code/${archivo}.php`, init);
                    if (response.ok) {
                        $(".preloader").fadeOut("fast");
                        let datosRespuesta = await response.json();
                        resolve(datosRespuesta);
                    } else {
                        reject(response.statusText);
                    }
                } catch (err) {
                    reject(err.message);
                }
            })();
        });
    },
    get: function ({
        modulo = null,
        archivo = null,
        params = {}
    }) {
        return new Promise((resolve, reject) => {
            (async () => {
                try {
                    $(".preloader").fadeIn("fast");
                    let urlParams = [];
                    for (let key in params) {
                        urlParams.push(`${key}=${params[key]}`);
                    }
                    let init = {
                            method: "GET",
                            headers: {
                                "Content-type": "application/json"
                            }
                        },
                        response = await fetch(`./main/${modulo}/code/${archivo}.php?${urlParams.join("&")}`, init);
                    if (response.ok) {
                        $(".preloader").fadeOut("fast");
                        let datosRespuesta = await response.json();
                        resolve(datosRespuesta);
                    } else {
                        reject(response.statusText);
                    }
                } catch (err) {
                    reject(err.message);
                }
            })();
        });
    },
};

const userActions = {
    logOut: function () {
        let username = localStorage.getItem('username') == null ? '' : localStorage.getItem('username');
        fetchActions.set({
            modulo: '',
            archivo: 'logOut',
            datos: {
                username
            }
        }).then((datosRespuesta) => {
            switch (datosRespuesta.respuesta.trim()) {
                case "EXITO":
                    localStorage.removeItem('username');
                    localStorage.removeItem('tipoApartado');
                    localStorage.removeItem('tipoPermiso');
                    localStorage.removeItem('idApartado');
                    localStorage.removeItem('menu');
                    localStorage.removeItem('logged');
                    window.top.location.href = "./";
                    break;
                default:
                    generalMostrarError(datosRespuesta);
                    break;
            }
        }).catch(generalMostrarError);
    },
    constructMenu: function (menu) {
        return new Promise((resolve, reject) => {
            let menuContainer = $("#sidebar-nav"),
                htmlMenu = [];
            // console.log(menu)

            if (menu) {
                menu.forEach(modulo => {
                    let urlModulo = encodeURIComponent(btoa(modulo.contenedor)),
                        contenedor = modulo.contenedor;
                    if (modulo.apartados.length > 0) {
                        htmlMenu.push(`<li class="nav-heading">${modulo.titulo}</li>`);
                        let tmpTipoApartado = localStorage.getItem('tipoApartado'),
                            tmpIdApartado = atob(decodeURIComponent(localStorage.getItem('idApartado')));
                        modulo.apartados.forEach(apartado => {
                            if (Object.keys(apartado.subApartados).length > 0) {
                                let showSelected = 'collapsed',
                                    ulShow = '';
                                if (tmpTipoApartado != null && tmpTipoApartado == 'Sub') {
                                    for (let key in apartado.subApartados) {
                                        if (apartado.subApartados[key].id == tmpIdApartado) {
                                            showSelected = '';
                                            ulShow = 'show';
                                        }
                                    }
                                }
                                htmlMenu.push(`<li class="nav-item">
                                <a class="nav-link ${showSelected}" data-bs-target="#${apartado.titulo.trim().replace(/ /g,"")+apartado.id}-nav" data-bs-toggle="collapse" href="#">
                                    <i class="${apartado.claseIcono}"></i><span>${apartado.titulo}</span><i class="bi bi-chevron-down ms-auto"></i>
                                </a>
                                <ul id="${apartado.titulo.trim().replace(/ /g,"")+apartado.id}-nav" class="nav-content collapse ${ulShow}" data-bs-parent="#sidebar-nav">`);
                                let tmpSubApartados = Object.values(apartado.subApartados).sort(function (a, b) {
                                    return a.titulo > b.titulo ? 1 : b.titulo > a.titulo ? -1 : 0
                                });

                                tmpSubApartados.forEach(subApartado => {
                                    let idSubApartado = subApartado.id,
                                        nombreArchivo = subApartado.nombreArchivo,
                                        tipoSubApartado = subApartado.tipo,
                                        accionClick = `userActions.abrirApartado('Sub','${encodeURIComponent(btoa(idSubApartado))}','${urlModulo}','${nombreArchivo}');`,
                                        showSelectedSub = '';

                                    if (tipoSubApartado == 'M') {
                                        accionClick = `userActions.abrirArchivo('${contenedor}','${nombreArchivo}','','S')`;
                                    }

                                    if ((tmpTipoApartado != null && tmpTipoApartado == 'Sub') && tmpIdApartado == idSubApartado) {
                                        showSelectedSub = 'class="active"';
                                    }
                                    htmlMenu.push(`<li>
                                    <a href="#" ${showSelectedSub} onclick="${accionClick}">
                                        <i class="bi bi-circle"></i><span>${subApartado.titulo}</span>
                                    </a>
                                </li>`);
                                });

                                htmlMenu.push(`</ul>
                                </li>`);
                            } else {
                                let idApartado = apartado.id,
                                    nombreArchivo = apartado.nombreArchivo,
                                    tipoApartado = apartado.tipo,
                                    accionClick = `userActions.abrirApartado('Apartado','${encodeURIComponent(btoa(idApartado))}','${urlModulo}','${nombreArchivo}');`,
                                    showSelected = 'collapsed';

                                if (tipoApartado == 'M') {
                                    accionClick = `userActions.abrirArchivo('${contenedor}','${nombreArchivo}','','S')`;
                                }

                                if ((tmpTipoApartado != null && tmpTipoApartado == 'Apartado') && tmpIdApartado == apartado.id) {
                                    showSelected = '';
                                }
                                htmlMenu.push(`<li class="nav-item">
                                <a class="nav-link ${showSelected}" href="#" onclick="${accionClick}">
                                    <i class="${apartado.claseIcono}"></i>
                                    <span>${apartado.titulo}</span>
                                </a>
                            </li>`);
                            }
                        });
                    }
                });

                menuContainer.append(htmlMenu.join(""));
                resolve();
            } else {
                resolve();
            }
        });
    },
    abrirApartado: function (tipo, id, contenedor, nombreArchivo) {
        let archivo = nombreArchivo.replace(".php", "");
        fetchActions.getCats({
            modulo: "",
            archivo: "validarPermiso",
            solicitados: [tipo, id],
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                // console.log(datosRespuesta);
                switch (datosRespuesta.respuesta.trim()) {
                    case "EXITO":
                        localStorage.setItem('tipoApartado', tipo);
                        localStorage.setItem('idApartado', id);
                        localStorage.setItem('tipoPermiso', datosRespuesta.tipoPermiso);
                        window.top.location.href = `?page=${archivo}&mod=${contenedor}`;
                        if (!nombreArchivo.includes(".pdf")) {} else {
                            archivo = nombreArchivo;
                            console.log(archivo)
                        }
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    abrirArchivo: function (contenedor, nombreArchivo, subContenedor = '', permisoCopia = 'S') {
        let tipoVista = permisoCopia == 'S' ? 'viewer_print.html' : 'viewer.html',
            subCarpeta = subContenedor.length > 0 ? `/${subContenedor}` : '';
        url = `./main/pdfjs/web/${tipoVista}?file=../../${contenedor}/docs${subCarpeta}/${nombreArchivo}`;

        window.open(url);
    },
    updateProfile: function () {
        let imagen = document.getElementById('previewImgUser'),
            accion = imagen.src.includes('avatar.jpg') ? 'delete' : 'update',
            campoImagen = document.getElementById('userImg'),
            datosActualizar = new FormData();

        campoImagen.files[0] && (datosActualizar.append("imagen", campoImagen.files[0]));
        datosActualizar.append('accion', accion);
        if (accion == 'delete' && !campoImagen.files[0]) {
            accion = 'update';
        }

        fetchActions.setWFiles({
            modulo: '',
            archivo: 'updateProfile',
            datos: datosActualizar
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta.trim()) {
                    case "EXITO":
                        Swal.fire({
                            title: "¡Bien hecho!",
                            html: "Datos actualizados exitosamente",
                            icon: "success"
                        }).then(() => {
                            window.location.reload();
                        });
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    changePassword: function () {
        formActions.validate('frmContrasenia').then(({
            errores
        }) => {
            if (errores == 0) {
                let campoPassActual = document.getElementById('currentPassword'),
                    campoPassNueva = document.getElementById("newPassword"),
                    campoConfirmationPass = document.getElementById("renewPassword");
                errores += generalValidarFormatoContrasenia(campoPassNueva.value);

                if (campoPassNueva.value != campoConfirmationPass.value && errores == 0) {
                    errores++;
                    toastr.warning("La nueva contraseña y su confirmación no son iguales.");
                }
                if (errores == 0) {
                    let datos = {
                        passActual: campoPassActual.value,
                        newPass: campoPassNueva.value
                    }

                    fetchActions.set({
                        modulo: '',
                        archivo: 'updatePassword',
                        datos
                    }).then((datosRespuesta) => {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                Swal.fire({
                                    title: "¡Bien hecho!",
                                    html: "Datos actualizados exitosamente",
                                    icon: "success"
                                }).then(() => {
                                    location.reload();
                                });
                                break;
                            case "PassRepetida":
                                Swal.fire({
                                    title: "¡Atención!",
                                    html: "Al parecer ya utilizaste esta contraseña en los últimos cinco cambios, la contraseña debe ser diferente.",
                                    icon: "warning"
                                });
                                break;
                            case "PassActual":
                                Swal.fire({
                                    title: "¡Cuidado!",
                                    html: "Tu confirmación de la contraseña actual no coincide.",
                                    icon: "warning"
                                });
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    }).catch(generalMostrarError);
                }
            }
        }).catch(generalMostrarError);
    },
    recoverySession: function () {
        formActions.validate('frmRecoverySession').then(({
            errores
        }) => {
            if (errores == 0) {
                let username = localStorage.getItem("username"),
                    password = document.getElementById("txtPasswordRecoverySession"),
                    intentosFallidos = 0;
                fetchActions.set({
                    modulo: '',
                    archivo: 'validateLogin',
                    datos: {
                        username: username,
                        password: password.value,
                        newPassword: '',
                        passwordChange: false,
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                localStorage.setItem('username', username);
                                localStorage.setItem('idUser', datosRespuesta.id);
                                localStorage.setItem('roles', JSON.stringify(datosRespuesta.roles));
                                localStorage.setItem("logged", "true");
                                $("#mdlRecoverySesion").modal("hide");
                                break;
                            case "Bloqueado":
                                Swal.fire({
                                    title: "¡Atención!",
                                    text: "Usuario bloqueado",
                                    icon: "warning"
                                }).then(() => {
                                    userActions.cancelRecoverySession();
                                });
                                break;
                            case "Contrasenia":
                                toastr.warning('Contraseña incorrecta');
                                intentosFallidos++;
                                if (intentosFallidos == 2) {
                                    userActions.cancelRecoverySession();
                                }
                                break;
                            case "BloqueoIntentos":
                                Swal.fire({
                                    title: "¡Bloqueado!",
                                    html: "Este usuario ha alcanzado el máximo de intentos fallidos, intenta recuperar la contraseña.",
                                    icon: "error"
                                }).then(() => {
                                    userActions.cancelRecoverySession();
                                });
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    cancelRecoverySession: function () {
        localStorage.removeItem('username');
        localStorage.removeItem('tipoApartado');
        localStorage.removeItem('tipoPermiso');
        localStorage.removeItem('idApartado');
        localStorage.removeItem('menu');
        localStorage.removeItem('logged');
        window.top.location.href = "./";
    }
}

function generalValidarFormatoContrasenia(contrasenia) {
    var errores = 0,
        mayus = false,
        minus = false,
        num = false,
        especial = false;

    if (contrasenia.length >= 6) {
        for (var a = 0; a < contrasenia.length; a++) {
            if (contrasenia.charCodeAt(a) >= 65 && contrasenia.charCodeAt(a) <= 90) {
                mayus = true;
            } else if (contrasenia.charCodeAt(a) >= 97 && contrasenia.charCodeAt(a) <= 122) {
                minus = true;
            } else if (contrasenia.charCodeAt(a) >= 48 && contrasenia.charCodeAt(a) <= 57) {
                num = true;
            } else {
                especial = true;
            }
        }
        if ((mayus == false) || (minus == false) || (num == false)) {
            errores++;
            if (mayus == false) {
                toastr.error("La nueva contraseña debe contener al menos una mayúscula", "ERROR");
            } else if (minus == false) {
                toastr.error("La nueva contraseña debe contener al menos una minúscula", "ERROR");
            } else if (num == false) {
                toastr.error("La nueva contraseña debe contener al menos un número", "ERROR");
            }
        }
    } else {
        errores++;
        toastr.error("La contraseña debe tener al menos 6 carácteres", "ERROR");
    }

    return errores;
}

function generalMostrarError(error = {}) {
    $(".preloader").fadeOut("fast");
    console.error(error);
    if (typeof error == 'object') {
        if (error.respuesta) {
            switch (error.respuesta.trim()) {
                case "SESION":
                    Swal.fire({
                        title: "Sesión caducada",
                        icon: "warning",
                        html: "Parece que tu sesión caducó o se ha iniciado en otro equipo. <br />¿Deseas intentar recuperarla?",
                        showCloseButton: false,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText: '<i class="fa fa-thumbs-up"></i> Recuperar',
                        cancelButtonText: '<i class="fa fa-thumbs-down"></i> Salir',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $("#mdlRecoverySesion").modal("show");
                        } else {
                            userActions.cancelRecoverySession();
                        }
                    });
                    break;
                default:
                    Swal.fire({
                        title: "¡Error!",
                        html: "Ha ocurrido un error catálogado. " + error.respuesta,
                        icon: "error"
                    });
                    break;
            }
        } else {
            Swal.fire({
                title: "¡Error!",
                html: "Ha ocurrido un error no catálogado",
                icon: "error"
            });
        }
    } else {
        Swal.fire({
            title: "¡Error!",
            html: "Ha ocurrido un error no controlado",
            icon: "error"
        });
    }
}

function generalShowHideButtonTab(idParentContainer, idButtonContainer) {
    let containers = document.querySelectorAll(`#${idParentContainer} .tabButtonContainer`);
    if (containers.length) {
        containers.forEach(buttonContainer => {
            if (buttonContainer.id && buttonContainer.id != idButtonContainer) {
                buttonContainer.style.display = 'none';
            } else {
                if ($("#" + idButtonContainer).is(":visible") == false) {
                    $("#" + idButtonContainer).fadeIn('fast');
                }
            }
        });
    }

    return;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function generalSoloNumeros(e) {
    tecla = e.which || e.keyCode; //obtenemos el código ascii de la tecla que se presiona
    patron = /\d/; //definimos el patrón de búsqueda de la tecla presionada
    te = String.fromCharCode(tecla); //convertimos a un formato legible el ascii de la tecla presionada
    return ((patron.test(te)) || (tecla == 9) || (tecla == 8) || (tecla == 46) || (tecla == 37) || (tecla == 39) || (tecla == 13)); //si la tecla presionada es un número, la tecla enter, la tecla para borrar ó la tecla suprimir, devuelve el valor o la función de la tecla, caso contrario no hace nada.
}

function generalSoloLetras(e) {
    if (e.which != 13) {
        tecla = (document.all) ? e.keyCode : e.which;

        if (tecla == 8) {
            return true;
        }
        patron = /[A-Za-zÁÉÍÓÚáéíóúñÑ. ]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final.toUpperCase());
    }

}

function generaLetrasNumeros(e) {
    if (e.which != 13) {
        tecla = (document.all) ? e.keyCode : e.which;

        if (tecla == 8) {
            return true;
        }
        patron = /[A-Za-zÁÉÍÓÚáéíóúñÑ.123456780 ]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final.toUpperCase());
    }

}

function generalValidateEmail(email) {
    var caract = new RegExp(/^([a-zA-Z0-9.+_])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/);
    if (caract.test(email) == false) {
        return false;
    } else {
        return true;
    }
}

function generalMonitoreoTipoDocumento(tipo, idCampo) {
    $("#" + idCampo).val("");
    if (tipo != '') {
        document.getElementById(idCampo).readOnly = false;
        if (tiposDocumento[tipo] && tiposDocumento[tipo].mascara && tiposDocumento[tipo].mascara != null) {
            $("#" + idCampo).mask(tiposDocumento[tipo].mascara);
        } else {
            $("#" + idCampo).unmask();
        }
    } else {
        document.getElementById(idCampo).readOnly = true;
    }

    return;
}

function generalMonitoreoPais(identificadorPais, campoDepartamento) {
    let cboDepartamento = document.getElementById(campoDepartamento),
        opciones = datosGeograficos[identificadorPais] ? ["<option selected disabled value=''>seleccione</option>"] : ["<option selected disabled value=''>seleccione un país</option>"];

    if (datosGeograficos[identificadorPais]) {
        datosGeograficos[identificadorPais]["departamentos"].forEach(departamento => {
            opciones.push(`<option value='${departamento.id}'>${departamento.nombreDepartamento}</option>`);
        });
    }

    cboDepartamento.innerHTML = opciones.join('');
    if (cboDepartamento.className.includes("selectpicker")) {
        $("#" + cboDepartamento.id).selectpicker("refresh");
        $("#" + cboDepartamento.id).selectpicker("val", cboDepartamento.options[0].value);
    }

    $("#" + cboDepartamento.id).trigger("change");

    return;
}

function generalMonitoreoDepartamento(campoPais, identificadorDepartamento, campoMunicipio) {
    let cboMunicipio = document.getElementById(campoMunicipio),
        cboPais = document.getElementById(campoPais),
        identificadorPais = cboPais.value,
        opciones = identificadorDepartamento != '0' ? ["<option selected disabled value=''>seleccione</option>"] : ["<option selected disabled value='0'>seleccione un departamento</option>"];

    if (datosGeograficos[identificadorPais] && datosGeograficos[identificadorPais]["departamentos"]) {
        datosGeograficos[identificadorPais]["departamentos"].forEach(departamento => {
            if (departamento["id"] == identificadorDepartamento) {
                departamento["municipios"].forEach(municipio => {
                    opciones.push(`<option value='${municipio.id}'>${municipio.nombreMunicipio}</option>`);
                });
            }
        });
    }

    cboMunicipio.innerHTML = opciones.join('');
    if (cboMunicipio.className.includes("selectpicker")) {
        $("#" + cboMunicipio.id).selectpicker("refresh");
        $("#" + cboMunicipio.id).selectpicker("val", cboMunicipio.options[0].value);
    }

    return;
}

// Funcion verifica si existe el codigo de cliente en la tabla
function generalVerificarCodigoCliente(codigoCliente) {
    return new Promise((resolve, reject) => {
        fetchActions.getCats({
            modulo: "",
            archivo: 'validarCodigoCliente',
            solicitados: [codigoCliente]
        }).then((datosRespuesta) => {
            resolve(datosRespuesta);
        }).catch(reject);
    });
}

function generalCalcularEdad(fecha) {
    let edad = '';
    if (fecha.length > 0) {
        let hoy = new Date(),
            partes = fecha.split('-'),
            tmpFecha = partes[2] + '-' + partes[1] + '-' + partes[0],
            cumpleanos = new Date(tmpFecha),
            m = hoy.getMonth() - cumpleanos.getMonth();

        edad = hoy.getFullYear() - cumpleanos.getFullYear();

        if (m < 0 || (m === 0 && hoy.getDate() < (cumpleanos.getDate()))) {
            edad--;
        }
    }

    return edad;
}

function generalHabilitarDeshabilitarEscritura(btn) {
    let campoSelect = btn.parentNode.parentNode.querySelector('select'),
        campoIcono = btn.querySelector('i'),
        nuevoIcono = 'fas fa-edit';
    if (campoSelect) {
        let nuevoEstado = !campoSelect.disabled;
        campoSelect.disabled = nuevoEstado;
        if (nuevoEstado) {
            campoSelect.classList.remove('campoWarning');
        } else {
            campoSelect.classList.add('campoWarning');
            nuevoIcono = 'fas fa-lock';
        }

        if (campoSelect.className.includes('selectpicker')) {
            $("#" + campoSelect.id).selectpicker("refresh");
            let especial = campoSelect.parentNode.querySelector("button.dropdown-toggle");
            if (nuevoEstado) {
                especial.classList.remove('campoWarning');
            } else {
                especial.classList.add('campoWarning');
            }
        }
        campoIcono.className = nuevoIcono;
    } else {
        let campoInput = btn.parentNode.parentNode.querySelector('input');
        let nuevoEstado = !campoInput.readOnly;
        let nuevoEstadoDis = !campoInput.disabled;
        if (campoInput.className.includes('campoFecha') || campoInput.className.includes('campoFechaHora')) {
            campoInput.disabled = nuevoEstadoDis;
            if (nuevoEstadoDis) {
                campoInput.classList.remove('campoWarning');
            } else {
                campoInput.classList.add('campoWarning');
                nuevoIcono = 'fas fa-lock';
            }
        } else {
            campoInput.readOnly = nuevoEstado;
            if (nuevoEstado) {
                campoInput.classList.remove('campoWarning');
            } else {
                campoInput.classList.add('campoWarning');
                nuevoIcono = 'fas fa-lock';
            }
        }
        campoIcono.className = nuevoIcono;
    }
}

function generalActivarUpload(idCampo) {
    if (idCampo) {
        $("#" + idCampo).trigger("click");
    }

    return;
}

function generalAsignarEdad(fecha, idCampoEdad) {
    let edad = '';
    if (fecha.length > 0) {
        let hoy = new Date(),
            partes = fecha.split('-'),
            tmpFecha = partes[2] + '-' + partes[1] + '-' + partes[0],
            cumpleanos = new Date(tmpFecha),
            m = hoy.getMonth() - cumpleanos.getMonth();

        edad = hoy.getFullYear() - cumpleanos.getFullYear();

        if (m < 0 || (m === 0 && hoy.getDate() < (cumpleanos.getDate()))) {
            edad--;
        }
    }

    document.getElementById(idCampoEdad).value = edad;
}

class usuario{
    get actualUserName(){
        return document.getElementById('spnNombreUsuario').innerHTML;
    }
}