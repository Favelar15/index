let tblContactos;

window.onload = () => {
    const getting = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "SOPORTE",
                archivo: "soporteGetCats",
                solicitados: ['agenciasCbo', 'cargosDirectorio', "contactosDirectorio"]
            }).then((datosRespuesta) => {
                if (datosRespuesta.agencias) {
                    datosRespuesta.agencias.forEach(agencia => {
                        configContacto.agencias[agencia.id] = {
                            ...agencia
                        };
                    });
                }

                if (datosRespuesta.cargos) {
                    datosRespuesta.cargos.forEach(cargo => {
                        configContacto.cargos[cargo.id] = {
                            ...cargo
                        };
                    });
                }

                if (datosRespuesta.contactosDirectorio) {
                    configContacto.init([
                        ...datosRespuesta.contactosDirectorio
                    ]);
                }
                resolve();
            }).catch(reject);
        });
    });

    getting.then(() => {
        $(".preloader").fadeOut("fast");
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 250;
            if ($("#tblContactos").length) {
                tblContactos = $("#tblContactos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2, 3, 4],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblContactos.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configContacto = {
    id: 0,
    cnt: 0,
    agencias: {},
    cargos: {},
    contactos: {},
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(contacto => {
                    this.cnt++;
                    this.contactos[this.cnt] = {
                        ...contacto
                    };
                    this.addRowTbl({
                        ...contacto,
                        nombreAgencia: this.agencias[contacto.idAgencia].nombreAgencia,
                        nombreCargo: this.cargos[contacto.idCargo].cargo,
                        idFila: this.cnt
                    });
                });
                tblContactos.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    addRowTbl: function ({
        idFila,
        nombres,
        apellidos,
        nombreAgencia,
        nombreCargo,
        discord,
        telefonos,
        emails,
    }) {
        return new Promise((resolve, reject) => {
            try {
                let tmpTelefonos = [],
                    tmpEmails = [];

                if (telefonos != null) {
                    telefonos.forEach(telefono => {
                        tmpTelefonos.push(telefono.telefono);
                    });
                }

                if (emails != null) {
                    emails.forEach(email => {
                        tmpEmails.push(email.email);
                    });
                }

                tblContactos.row.add([
                    idFila,
                    `${nombres} ${apellidos}`,
                    nombreAgencia,
                    nombreCargo,
                    discord,
                    tmpTelefonos.join('<br/>'),
                    tmpEmails.join('<br />'),
                ]).node().id = 'trContacto' + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    }
}