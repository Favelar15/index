window.onload = ()=>{
    $(".preloader").fadeOut("fast");
}

const configUser = {
    campoImagen:document.getElementById('userImg'),
    imagen:document.getElementById('previewImgUser'),
    evalImage:function(){
        if(this.campoImagen.files[0]){
            let url = URL.createObjectURL(this.campoImagen.files[0]);
            this.imagen.src = url;
        }
    },
    deleteImage:function(){
        this.campoImagen.value = '';
        this.imagen.src = './main/img/users/avatar.jpg';
        if(this.campoImagen.files[0]){
            console.log('Aún hay imagen')
        }
    }
};