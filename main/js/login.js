let imagenes = ['workteam.png', 'hero-3.png', 'hero-2.png', 'pc_.png'];

window.onload = () => {
    $(".preloader").fadeOut("fast");
    document.getElementById("mainContainer").reset();
    document.getElementById("frmRecuperacion").reset();
    document.getElementById("frmCodeRestore").reset();
    const inputs = document.querySelectorAll(".input");


    function addcl() {
        let parent = this.parentNode.parentNode;
        parent.classList.add("focus");
    }

    function remcl() {
        let parent = this.parentNode.parentNode;
        if (this.value == "") {
            parent.classList.remove("focus");
        }
    }


    inputs.forEach(input => {
        input.addEventListener("focus", addcl);
        input.addEventListener("change", function () {
            if (!this.parentNode.parentNode.className.includes('focus')) {
                this.parentNode.parentNode.classList.add("focus");
            }
        });
        input.addEventListener("blur", remcl);
    });

    let posicion = aleatorio();
    $("#formImg").attr("src", './main/img/' + imagenes[posicion]);

}

function aleatorio() {
    let minimo = 0,
        maximo = imagenes.length - 1;
    return Math.floor(Math.random() * ((maximo + 1) - minimo) + minimo);
}

const login = {
    passwordChange: false,
    username: document.querySelector("#txtUsername"),
    password: document.querySelector("#pwdContrasenia"),
    newPassword: document.querySelector("#pwdContraseniaNueva"),
    passwordConfirm: document.querySelector("#pwdContraseniaConfirmacion"),
    idRestore: 0,
    signIn: function () {
        formActions.validate('frmLogin').then(({
            errores
        }) => {
            if (errores == 0) {
                if (this.passwordChange) {
                    errores = generalValidarFormatoContrasenia(this.newPassword.value);
                    if (errores == 0 && (this.password.value == this.newPassword.value)) {
                        errores++;
                        Swal.fire({
                            title: "¡Atención!",
                            text: "La nueva contraseña no puede ser igual a la anterior",
                            icon: "warning"
                        });
                    } else if (errores == 0 && (this.newPassword.value != this.passwordConfirm.value)) {
                        errores++;
                        toastr.error("Las contraseñas no son iguales");
                    }
                }

                if (errores == 0) {
                    fetchActions.set({
                        modulo: '',
                        archivo: 'validateLogin',
                        datos: {
                            username: this.username.value,
                            password: this.password.value,
                            newPassword: this.newPassword.value,
                            passwordChange: this.passwordChange,
                        }
                    }).then(this.evalResponse).catch(generalMostrarError);
                }
            }
        });
    },
    evalResponse: function (datos) {
        if (datos.respuesta) {
            switch (datos.respuesta.trim()) {
                case "EXITO":
                    localStorage.setItem('username', login.username.value);
                    localStorage.setItem('idUser', datos.id);
                    localStorage.setItem('roles', JSON.stringify(datos.roles));
                    localStorage.removeItem('tipoApartado');
                    localStorage.removeItem('tipoPermiso');
                    localStorage.removeItem('idApartado');
                    localStorage.removeItem('menu');
                    localStorage.setItem("logged", "true");
                    window.top.location.reload();
                    break;
                case "PrimerIngreso":
                    Swal.fire({
                        title: '¡Te damos la bienvenida a INDEX DE APLICACIONES!',
                        icon: 'info',
                        html: 'Para poder ingresar al sistema, deberás cambiar la contraseña a una personalizada.',
                        showCloseButton: false,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText: '<i class="fa fa-thumbs-up"></i> De acuerdo',
                        cancelButtonText: '<i class="fa fa-thumbs-down"></i> No quiero',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            login.passwordChange = true;
                            loginMostrarCambioContrasenia();
                        }
                    });
                    break;
                case "Bloqueado":
                    Swal.fire({
                        title: "¡Atención!",
                        text: "Usuario bloqueado",
                        icon: "warning"
                    });
                    break;
                case "PassRepetida":
                    Swal.fire({
                        title: "¡Atención!",
                        html: "Al parecer ya utilizaste esta contraseña en los últimos cinco cambios, la contraseña debe ser diferente.",
                        icon: "warning"
                    });
                    break;
                case "PassCaducada":
                    Swal.fire({
                        title: '¡Atención!',
                        icon: 'warning',
                        html: 'La contraseña ha caducado, para poder ingresar al sistema, deberás cambiarla.',
                        showCloseButton: false,
                        showCancelButton: true,
                        focusConfirm: false,
                        confirmButtonText: '<i class="fa fa-thumbs-up"></i> De acuerdo',
                        cancelButtonText: '<i class="fa fa-thumbs-down"></i> No quiero',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            login.passwordChange = true;
                            loginMostrarCambioContrasenia();
                        }
                    });
                    break;
                case "Contrasenia":
                    Swal.fire({
                        title: "Ups!",
                        html: "Contraseña incorrecta<br/>Intentos restantes: <strong>" + datos.intentos + "</strong>",
                        icon: "error"
                    });
                    break;
                case "BloqueadoIntentos":
                    Swal.fire({
                        title: "¡Bloqueado!",
                        html: "Este usuario ha alcanzado el máximo de intentos fallidos, intenta recuperar la contraseña.",
                        icon: "error"
                    });
                    break;
                case "NoUsuario":
                    Swal.fire({
                        title: "Ups!",
                        html: "Parece que el usuario no existe",
                        icon: "warning"
                    });
                    break;
                default:
                    generalMostrarError(datos);
                    break;
            }
        } else {
            generalMostrarError(datos);
        }
    },
    evalShowForm: function (idForm) {
        let forms = document.querySelectorAll(".login-content form");

        forms.forEach(form => {
            form.style.display = 'none';
        });

        $("#" + idForm).fadeIn("fast");
        $("#reloadCaptcha").trigger('click');
    },
    requestPassRestore: function () {
        formActions.validate('frmRecuperacion').then(({
            errores
        }) => {
            if (errores == 0) {
                let username = document.getElementById("txtUsernameRecovery").value,
                    codigoVerificacion = document.getElementById("txtCodigo").value;

                let datos = {
                    username,
                    codigoVerificacion
                }

                fetchActions.set({
                    modulo: "",
                    archivo: 'requestPassRestore',
                    datos
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                login.idRestore = datosRespuesta.id;
                                document.getElementById("mdlEmail").innerHTML = datosRespuesta.email;
                                $("#mdlTemporal").modal("show");
                                break;
                            case "Timeout":
                                Swal.fire({
                                    title: "¡Atención!",
                                    html: "El código en pantalla ha caducado.",
                                    icon: "warning"
                                }, () => {
                                    $("#reloadCaptcha").trigger("click");
                                    document.getElementById("txtCodigo").value = "";
                                });
                                break;
                            case "Incorrecto":
                                toastr.error("Código de verificación incorrecto.");
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }

                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    confirmCodeRestore: function () {
        formActions.validate('frmCodeRestore').then(({
            errores
        }) => {
            if (errores == 0) {
                fetchActions.set({
                    modulo: '',
                    archivo: 'confirmCodeRestore',
                    datos: {
                        identificador: login.idRestore,
                        contraseniaTemporal: document.getElementById("pwdTemporal").value
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                let contraseniaAnterior = datosRespuesta.contrasenia,
                                    username = document.getElementById("txtUsernameRecovery").value;
                                document.getElementById("txtUsername").value = username;
                                document.getElementById("pwdContrasenia").value = contraseniaAnterior;
                                $("#mdlTemporal").modal("hide");
                                $("#txtUsername").parent().parent().addClass("focus");
                                $("#txtUsername").trigger("blur");
                                login.passwordChange = true;
                                loginMostrarCambioContrasenia();
                                this.evalShowForm("mainContainer");
                                break;
                            case "Incorrecto":
                                toastr.warning("La contraseña no coincide con la enviada por correo.");
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    }
}

function loginMostrarCambioContrasenia() {
    let campoUsername = document.getElementById("txtUsername"),
        campoNuevaContrasenia = document.getElementById("pwdContraseniaNueva"),
        campoConfirmacion = document.getElementById("pwdContraseniaConfirmacion");
    campoUsername.readOnly = true;
    $("#" + campoNuevaContrasenia.id).addClass('campoRequerido');
    campoNuevaContrasenia.required = true;
    $("#" + campoConfirmacion.id).addClass('campoRequerido');
    campoConfirmacion.required = true;
    $("#mainPass").hide("slide", {
        direction: "right"
    }, 250, function () {
        $("#newPass").fadeIn("fast");
    });

    return;
}