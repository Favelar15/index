<div class="pagetitle">
    <h1>¡Que no está el archivo!</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item">Forms</li>
            <li class="breadcrumb-item active">Validation</li>
        </ol>
    </nav>
</div><!-- End Page Title -->

<script type="text/javascript">
    window.onload = () => {
        $('.preloader').fadeOut('fast');
    }
</script>

<!-- <section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <h2>Test</h2>
        </div>
    </div>
</section> -->

<?php
