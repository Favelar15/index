<div class="pagetitle">
    <h1>Directorio interno</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Generales</li>
            <li class="breadcrumb-item active">Directorio</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <table id="tblContactos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Nombre completo</th>
                        <th>Oficina</th>
                        <th>Cargo</th>
                        <th>Discord</th>
                        <th>Teléfonos</th>
                        <th>Correos</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php
$_GET["js"] = ['directorio'];