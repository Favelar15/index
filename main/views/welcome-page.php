<?php
// echo json_encode($_SESSION["index"]);
?>

<section class="section">
    <div class="centrado">
        <h1>Bienvenido/a</h1>
        <hr>
        <h3>Este es el nuevo INDEX DE APLICACIONES.</h3>
        <p>Como habrás notado, aún me falta asimilar unos módulos desde mi primera versión, el equipo de desarrollo se está encargando de actualizarlos, en el momento que se vayan terminando podrás encontrarlos en esta nueva y genial versión. <br>
            No sólo cambiamos visualmente, a continuación el detalle de algunas mejoras significativas que ahora tenemos para tí:
        <ul>
            <li>
                Si olvidas tu contraseña, ahora cuentas con un método de recuperación vinculado a tu correo institucional (Para el caso de los cajeros, es el correo del jefe inmediato).
            </li>
            <li>
                Parámetros de seguridad en la identificación de usuarios mejorada.
            </li>
            <li>
                Personalización de perfil, ahora puedes cambiar tu foto de perfil <i class="fas fa-hand-peace"></i>.
            </li>
            <li>
                Mejoras en la construcción de todos los diferentes documentos generados por el sistema.
            </li>
            <li>
                Incorporación de directorio interno.
            </li>
            <li>
                Validación de formularios mejorada, ahora podrás identificar facilmente si estás llenando correctamente la información de los diferentes formularios dentro del sistema.
            </li>
            <li>
                <b>Recuperación de sesión:</b> Si tu sesión caduca mientras estás en un proceso, ahora podrás recuperarla ingresando nuevamente la contraseña y tu avance no se perderá. Sólo se podrá reanudad la sesión antes vigente y sólo tendrás dos intentos de recuperación.
            </li>
        </ul>
        </p><br />

        <h3>Próximamente</h3>
        <p>Me alegra informarte que además de las actualizaciones pendientes, hay nuevos proyectos próximos a incorporarse. Te doy un pequeño adelanto de las maravillas que conocerás pronto:
        <ul>
            <li>
                <b>CONTROLES: </b>Módulo para el registro y seguimiento de las diferentes actividades del área de operaciones, su objetivo es llevar un control mejorado y ágil de los diferentes reportes de actividades entregados al Departamento de Operaciones.
            </li>
            <li>
                <b>HIPOTECAS: </b>Módulo para la gestión de control de garantías internas.
            </li>
            <li>
                <b>DIETACOOP: </b>Módulo totalmente mejorado para la gestión, control y pago de directivos y miembros de comités de apoyo.
            </li>
            <li>
                <b>CHATBOT - TÉCNICO: </b>Su objetivo será facilitar el reporte de inconvenientes o fallas técnicas con el Departamento de Tecnología
            </li>
            <li>
                <b>LISTAS INTERNAS: </b>Búscador personalizado y sujeto a todo el sistema para la prevención de actividad irregular dentro de La Cooperativa.
            </li>
            <li>
                <b>SIMAC: </b>Módulo de mapeo automático de créditos, desde la creación de la solicitud, hasta el desembolso, será el encargado de llevar el control de todos los créditos de La Cooperativa.
            </li>
        </ul>
        </p>
    </div>
</section>

<?php
$_GET["js"] = ['welcome'];
