<style>
    html,
    body {
        background-color: rgba(100%, 100%, 100%, 0) !important;
    }
</style>
<img class="loginWave" src="./main/img/loginBackground.png">
<div class="loginContainer">
    <div class="loginImg">
        <img id="formImg" src="./main/img/hero-3.png">
    </div>
    <div class="login-content">
        <form method="post" id="mainContainer" accept-charset="utf-8" action="javascript:login.signIn();">
            <img src="./main/img/logoAcacypac.png">
            <h2 class="title">Index de aplicaciones</h2>
            <div class="input-div one">
                <div class="i">
                    <i class="fas fa-user"></i>
                </div>
                <div class="div">
                    <h5>Usuario</h5>
                    <input type="text" class="input campoRequerido" id="txtUsername" required>
                </div>
            </div>
            <div id="mainPass">
                <div class="input-div pass">
                    <div class="i">
                        <i class="fas fa-lock"></i>
                    </div>
                    <div class="div">
                        <h5>Contraseña</h5>
                        <input type="password" class="input campoRequerido" id="pwdContrasenia" required>
                    </div>
                </div>
                <a href="#" onclick="login.evalShowForm('frmRecuperacion');">¿Olvidó su contraseña?</a>
            </div>
            <div id="newPass" style="display: none;">
                <div class="input-div pass">
                    <div class="i">
                        <i class="fas fa-lock"></i>
                    </div>
                    <div class="div">
                        <h5>Nueva contraseña</h5>
                        <input type="password" class="input" id="pwdContraseniaNueva">
                    </div>
                </div>
                <div class="input-div pass">
                    <div class="i">
                        <i class="fas fa-lock"></i>
                    </div>
                    <div class="div">
                        <h5>Confirme contraseña</h5>
                        <input type="password" class="input" id="pwdContraseniaConfirmacion">
                    </div>
                </div>
            </div>
            <input type="submit" id="loginBtn" class="btn" value="Ingresar">
            <input type="button" id="loginOLD" class="btn btn mt-2" value="Ir al viejo INDEX" onclick="window.top.location.href='http://172.16.15.82/ACACYPAC-OLD';">
        </form>
        <form method="post" accept-charset="utf-8" action="javascript:login.requestPassRestore();" id="frmRecuperacion" style="display: none;">
            <img src="./main/img/logoAcacypac.png">
            <h2 class="title">Recuperación de contraseña</h2>
            <div class="input-div one">
                <div class="i">
                    <i class="fas fa-user"></i>
                </div>
                <div class="div">
                    <h5>Usuario</h5>
                    <input type="text" class="input campoRequerido" id="txtUsernameRecovery" required>
                </div>
            </div>
            <div class="input-div pass">
                <div class="i">
                    <i class="fas fa-question-circle"></i>
                </div>
                <div class="div mayusculas">
                    <h5>Código de verificación</h5>
                    <input type="text" class="input campoRequerido" id="txtCodigo" required>
                </div>
            </div>
            <div class="row captchaContainer">
                <div class="col-md-12">
                    <img src="./main/code/getCaptcha.php?rand=<?php echo rand(); ?>" id='captcha' class="captchaImage" />
                </div>
                <div class="col-md-12">
                    <center><a href="javascript:void(0)" id="reloadCaptcha" class="captcha-reload"><i class="fas fa-sync-alt"></i> Recargar código</a></center>
                </div>
            </div>

            <input type="button" onclick="login.evalShowForm('mainContainer');" id="backBtn" class="btnB" value="Regresar">
            <input type="submit" id="recoveryBtn" class="btnR" value="Solicitar">
        </form>
    </div>
</div>
<div class="modal fade" id="mdlTemporal" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Recuperación de contraseña</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:login.confirmCodeRestore();" id="frmCodeRestore" name="frmCodeRestore" method="POST" accept-charset="utf-8" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <p>Se ha enviado una contraseña temporal al correo: <strong id="mdlEmail">francisco.avelar@acacypac.coop</strong><br />Esta contraseña sólo podrá ser utilizada una vez.</p>
                        </div>
                        <div class="col-lg-12 col-xl-12">
                            <label>Contraseña temporal</label>
                            <input type="text" class="form-control" id="pwdTemporal" required>
                            <div class="invalid-feedback">
                                Ingrese la contraseña temporal
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmCodeRestore" class="btn btn-sm btn-primary">
                    <i class="fas fa-send"></i> Enviar
                </button>
            </div>
        </div>
    </div>
</div>
<?php
$_GET['js'] = ['login'];
