<!DOCTYPE html>
<html lang="en" translate="no">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>INDEX DE APLICACIONES</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="main/img/white_acacypac.png" rel="icon">

  <!-- Google Fonts -->
  <!-- <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"> -->

  <!-- Vendor CSS Files -->
  <link href="main/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="main/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="main/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="main/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="main/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="main/vendor/remixicon/remixicon.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="main/css/fontawesome/css/all.css" rel="stylesheet">
  <!-- <link rel="stylesheet" href="main/css/sweetalert/sweetalert2.min.css"> -->
  <link rel="stylesheet" href="./main/vendor/sweetalert/css/sweetalert2.css">
  <link rel="stylesheet" href="./main/css/toastr/toastr.min.css">
  <link rel="stylesheet" href="./main/vendor/datatable/datatables.css">
  <link rel="stylesheet" href="./main/vendor/datatable/DataTables-1.11.3/css/dataTables.bootstrap5.css">
  <link rel="stylesheet" href="./main/vendor/datatable/Responsive-2.2.9/css/responsive.bootstrap5.css">
  <link rel="stylesheet" href="./main/vendor/datatable/Responsive-2.2.9/css/responsive.dataTables.css">
  <link rel="stylesheet" href="./main/vendor/datatable/FixedColumns-4.0.1/css/fixedColumns.bootstrap5.min.css">
  <link rel="stylesheet" href="./main/vendor/selectpicker/css/bootstrap-select.css">
  <link rel="stylesheet" href="./main/vendor/datepicker/css/bootstrap-datepicker.standalone.css">
  <link rel="stylesheet" href="./main/vendor/datetimepicker/css/bootstrap-datetimepicker.css">
  <link href="main/css/style.css" rel="stylesheet">
  <link href="main/css/index.css?v=1" rel="stylesheet">

</head>

<body>
  <div class="preloader"></div>