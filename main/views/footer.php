<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="main/vendor/apexcharts/apexcharts.min.js"></script>
<script src="main/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="main/vendor/chart.js/chart.min.js"></script>
<script src="main/vendor/echarts/echarts.min.js"></script>
<script src="main/vendor/quill/quill.min.js"></script>
<script src="main/vendor/tinymce/tinymce.min.js"></script>
<script src="main/vendor/php-email-form/validate.js"></script>
<script src="main/js/jquery-3.6.0.js"></script>
<script src="main/js/jquery-ui.js"></script>
<script src="main/js/fontawesome/fontawesome.js"></script>
<script type="text/javascript" src="./main/js/toastr/toastr.min.js"></script>
<!-- <script type="text/javascript" src="./main/js/sweetalert/sweetalert2@11.js"></script> -->
<script type="text/javascript" src="./main/vendor/sweetalert/js/sweetalert2.js"></script>
<script type="text/javascript" src="./main/vendor/datatable/datatables.js"></script>
<script type="text/javascript" src="./main/vendor/datatable/DataTables-1.11.3/js/dataTables.bootstrap5.js"></script>
<script type="text/javascript" src="./main/vendor/datatable/Responsive-2.2.9/js/dataTables.responsive.js"></script>
<script type="text/javascript" src="./main/vendor/datatable/Responsive-2.2.9/js/responsive.bootstrap5.js"></script>
<script type="text/javascript" src="./main/vendor/datatable/FixedColumns-4.0.1/js/fixedColumns.bootstrap5.min.js"></script>
<script type="text/javascript" src="./main/vendor/selectpicker/js/bootstrap-select.js"></script>
<script type="text/javascript" src="./main/vendor/jqueryMask/js/jquery.mask.js"></script>
<script type="text/javascript" src="./main/vendor/datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="./main/vendor/datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="./main/vendor/moment/moment.js"></script>
<!-- <script src="//code.tidio.co/ac0gyz7zqzjjnhjr5qmvo2eqiwgsyiz3.js" async></script> -->

<script type="text/javascript" src="main/js/smartuploader.js"></script>
<script type="text/javascript" src="main/js/general.js?v=1"></script>

<script src="main/js/main.js"></script>
<?php
if (isset($_GET["js"])) {
  $jsContainer = isset($_GET['mod']) ? (!isset($_SESSION["index"]) ? '' : '/' . base64_decode(urldecode($_GET["mod"]))) : '';
  foreach ($_GET["js"] as $archivo) {
?>
    <script type="text/javascript" src="./main<?php echo $jsContainer; ?>/js/<?php echo $archivo; ?>.js?v=7"></script>
<?php
  }
}
?>
</body>

</html>