<style>
    .card {
        box-shadow: 0px 0 30px rgba(1, 41, 112, 0.19);
    }
</style>
<div class="pagetitle">
    <h1>Mi perfil</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item active">Perfil</li>
        </ol>
    </nav>
</div><!-- End Page Title -->

<section class="section profile">
    <div class="row">
        <div class="col-xl-4">

            <div class="card">
                <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">

                    <img src="./main/img/users/<?php echo $_SESSION["index"]->foto; ?>" alt="Profile" class="rounded-circle">
                    <h2 align="center"><?php echo $_SESSION["index"]->nombres . ' ' . $_SESSION["index"]->apellidos; ?></h2>
                </div>
            </div>

        </div>

        <div class="col-xl-8">
            <div class="card">
                <div class="card-body pt-3">
                    <ul class="nav nav-tabs nav-tabs-bordered">
                        <li class="nav-item">
                            <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-edit">Editar perfil</button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-change-password">Cambiar contraseña</button>
                        </li>
                    </ul>
                    <div class="tab-content pt-2">
                        <div class="tab-pane fade show active profile-edit pt-3" id="profile-edit">
                            <form id="frmUser" name="frmUser" accept-charset="utf-8" method="POST" action="javascript:userActions.updateProfile();" class="needs-validation" novalidate>
                                <div class="row mb-3">
                                    <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Foto de perfil</label>
                                    <div class="col-md-8 col-lg-9">
                                        <img src="./main/img/users/<?php echo $_SESSION["index"]->foto; ?>" alt="Foto de perfil" id="previewImgUser">
                                        <div class="pt-2 has-validation">
                                            <input type="file" id="userImg" name="userImg" onchange="configUser.evalImage();" style="display: none;" accept=".gif, .jpg, .png .jpeg">
                                            <button type="button" class="btn btn-primary btn-sm" title="Cargar nueva imagen" onclick="$('#userImg').trigger('click');">
                                                <i class="bi bi-upload"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-sm" title="Eliminar mi foto de perfil" onclick="configUser.deleteImage();">
                                                <i class="bi bi-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Nombre completo</label>
                                    <div class="col-md-8 col-lg-9">
                                        <input name="fullName" type="text" class="form-control" id="fullName" value="<?php echo $_SESSION["index"]->nombres . ' ' . $_SESSION["index"]->apellidos; ?>" readonly required>
                                        <div class="invalid-feedback">
                                            Ingrese el nombre completo
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="email" class="col-md-4 col-lg-3 col-form-label">Email</label>
                                    <div class="col-md-8 col-lg-9">
                                        <input name="email" type="email" class="form-control" id="email" value="<?php echo $_SESSION["index"]->email; ?>" readonly required>
                                        <div class="invalid-feedback">
                                            Ingrese la dirección de correo electrónico
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="email" class="col-md-4 col-lg-3 col-form-label">Agencias asignadas</label>
                                    <div class="col-md-8 col-lg-9">
                                        <?php
                                        foreach ($_SESSION["index"]->agencias as $agencia) {
                                            $clase = $agencia->principal == 'S' ? 'bg-warning text-dark' : 'bg-secondary text-light';
                                            $icono = $agencia->principal == 'S' ? '<i class="fas fa-star"></i>' : '';
                                        ?>
                                            <span class="badge <?php echo $clase; ?>" style="font-size: 12px;"><?php echo $icono . ' ' . $agencia->id; ?></span>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="email" class="col-md-4 col-lg-3 col-form-label">Roles asignados</label>
                                    <div class="col-md-8 col-lg-9">
                                        <?php
                                        foreach ($_SESSION["index"]->roles as $rol) {
                                        ?>
                                            <span class="badge bg-secondary text-light" style="font-size: 12px;"><?php echo $rol->label; ?></span>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Actualizar cambios</button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade profile-edit pt-3" id="profile-change-password">
                            <form id="frmContrasenia" name="frmContrasenia" accept-charset="utf-8" method="POST" action="javascript:userActions.changePassword();" class="needs-validation" novalidate>
                                <div class="row mb-3">
                                    <label for="currentPassword" class="col-md-4 col-lg-3 col-form-label">Contraseña actual</label>
                                    <div class="col-md-8 col-lg-9">
                                        <input name="password" type="password" class="form-control" id="currentPassword" required>
                                        <div class="invalid-feedback">
                                            Ingrese la contraseña actual
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">Nueva contraseña</label>
                                    <div class="col-md-8 col-lg-9">
                                        <input name="newpassword" type="password" class="form-control" id="newPassword" required>
                                        <div class="invalid-feedback">
                                            Ingrese la nueva contraseña
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">Confirmar contraseña</label>
                                    <div class="col-md-8 col-lg-9">
                                        <input name="renewpassword" type="password" class="form-control" id="renewPassword" required>
                                        <div class="invalid-feedback">
                                            Confirme la nueva contraseña
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Cambiar contraseña</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<?php
$_GET["js"] = ['userProfile'];
