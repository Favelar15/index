<?php
$nombres = explode(' ', $_SESSION["index"]->nombres);
$apellidos = explode(' ', $_SESSION["index"]->apellidos);
$nombreCompleto = $nombres[0] . ' ' . $apellidos[0];
$rol = 'Usuario';
foreach ($_SESSION["index"]->roles as $tmpRol) {
    if ($tmpRol->label == 'ROOT') {
        $rol = 'ROOT';
    }
}
?>
<!-- ======= Header ======= -->
<header id="header" class="header fixed-top d-flex align-items-center">

    <div class="d-flex align-items-center justify-content-between">
        <a href="#" onclick="window.top.location.href='./';" class="logo d-flex align-items-center">
            <img src="main/img/logoAcacypac.png" alt="">
            <span class="d-none d-lg-block">ACACYPAC</span>
        </a>
        <i class="bi bi-list toggle-sidebar-btn" style="color: white;"></i>
    </div>

    <nav class="header-nav ms-auto">
        <ul class="d-flex align-items-center">

            <li class="nav-item dropdown">

                <a class="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
                    <i class="bi bi-bell" style="color: white;"></i>
                    <span class="badge bg-primary badge-number">1</span>
                </a><!-- End Notification Icon -->

                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">
                    <li class="dropdown-header">
                        Tienes 1 nuevas notificaciones
                        <a href="#"><span class="badge rounded-pill bg-primary p-2 ms-2">Ver todo</span></a>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li class="notification-item">
                        <i class="bi bi-exclamation-circle text-warning"></i>
                        <div>
                            <h4>Nueva versión disponible</h4>
                            <p>Temporalmente se tendrán dos versiones de INDEX en producción.</p>
                            <p>Hace 30 min.</p>
                        </div>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li class="dropdown-footer">
                        <a href="#">Ver todas las notificaciones</a>
                    </li>

                </ul><!-- End Notification Dropdown Items -->

            </li><!-- End Notification Nav -->

            <li class="nav-item dropdown">

                <a class="nav-link nav-icon" href="?page=directorio">
                    <i class="fas fa-address-book" style="color: white;"></i>
                </a>

            </li><!-- End Messages Nav -->

            <li class="nav-item dropdown pe-3">

                <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                    <img src="main/img/users/<?php echo $_SESSION["index"]->foto; ?>" alt="Profile" class="rounded-circle">
                    <span class="d-none d-md-block dropdown-toggle ps-2" style="color: white;" id="spnNombreUsuario"><?php echo $nombreCompleto; ?></span>
                </a><!-- End Profile Image Icon -->

                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                    <li class="dropdown-header">
                        <h6><?php echo $nombreCompleto; ?></h6>
                        <span><?php echo $rol; ?></span>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li>
                        <a class="dropdown-item d-flex align-items-center" href="?page=userProfile">
                            <i class="bi bi-gear"></i>
                            <span>Configuración de cuenta</span>
                        </a>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li>
                        <a class="dropdown-item d-flex align-items-center" href="#" onclick="javascript:userActions.logOut();">
                            <i class="bi bi-box-arrow-right"></i>
                            <span>Salir</span>
                        </a>
                    </li>

                </ul><!-- End Profile Dropdown Items -->
            </li><!-- End Profile Nav -->

        </ul>
    </nav><!-- End Icons Navigation -->

</header><!-- End Header -->

<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <!-- <a class="nav-link collapsed " href="?page=dashboard"> -->
            <a class="nav-link collapsed " href="#">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="nav-heading">LISTAS</li>
        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#ListasE-nav" data-bs-toggle="collapse" href="#">
                <i class="fas fa-search"></i><span>Externas</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="ListasE-nav" class="nav-content collapse" data-bs-parent="#sidebar-nav">
                <li>
                    <a href="http://sdnsearch.ofac.treas.gov/Default.aspx" target="_blank">
                        <i class="bi bi-circle"></i><span>OFAC</span>
                    </a>
                </li>
                <li>
                    <a href="https://www.interpol.int/es/Como-trabajamos/Notificaciones/Ver-las-notificaciones-rojas" target="_blank">
                        <i class="bi bi-circle"></i><span>Interpol</span>
                    </a>
                </li>
            </ul>
            <a class="nav-link collapsed" data-bs-target="#ListasI-nav" data-bs-toggle="collapse" href="#">
                <i class="fas fa-search"></i><span>Internas</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="ListasI-nav" class="nav-content collapse" data-bs-parent="#sidebar-nav">
                <li>
                    <a href="?page=listasBusqueda&mod=TElTVEFT">
                        <i class="bi bi-circle"></i><span>Buscar</span>
                    </a>
                </li>
            </ul>
        <li class="nav-item">
            <a class="nav-link collapsed" href="./main/LISTAS/docs/Circulares/Circular_003-2022.pdf" target="_blank" onclick="">
                <i class="fas fa-file-signature"></i>
                <span>Circular_003-2022</span>
            </a>
        </li>
        </li>
    </ul>

</aside><!-- End Sidebar-->
<main id="main" class="main">
    <?php
    $mod = isset($_GET["mod"]) ? '/' . base64_decode(urldecode($_GET["mod"])) : '';
    $page = isset($_GET["page"]) ? $_GET["page"] : "welcome-page";
    $completeURL = './main/' . $mod . '/views/' . $page . '.php';
    // echo urlencode(base64_encode('LISTAS'));

    !file_exists($completeURL) && $completeURL = './main/views/404-not-found.php';

    require_once $completeURL;
    ?>
</main>
<!-- ======= Footer ======= -->
<footer id="footer" class="footer">
    <div class="copyright">
        &copy; Copyright <strong><span>ACACYPAC</span></strong>. All Rights Reserved
    </div>
</footer><!-- End Footer -->

<div class="modal fade" id="mdlRecoverySesion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Recuperación de sesión</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:userActions.recoverySession();" id="frmRecoverySession" name="frmRecoverySession" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-12 col-xl-12 d-flex justify-content-center mt-3">
                            <img class="imgRecoverySesion" src="./main/img/users/<?php echo $_SESSION["index"]->foto; ?>">
                        </div>
                        <div class="col-lg-12 col-xl-12">
                            <label for='txtPasswordRecoverySession' class="form-label">Contraseña <span class="requerido">*</span></label>
                            <input type='password' class='form-control' id='txtPasswordRecoverySession' required name='txtPasswordRecoverySession' value="">
                            <div class='invalid-feedback'>
                                Ingrese la contraseña
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-12">
                            <p style="text-align: justify; text-justify: inter-word;"><strong>IMPORTANTE: </strong>Sólo tendrás dos intentos de recuperación.<br /><strong style="color:red;">NOTA: </strong>Si cancelas, se perderá toda la información que hayas ingresado y serás redireccionado a la pantalla de identificación.</p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-primary" form="frmRecoverySession">
                    Recuperar sesión
                </button>
                <button type="button" onclick="userActions.cancelRecoverySession();" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>