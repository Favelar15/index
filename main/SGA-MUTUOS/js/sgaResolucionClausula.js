let tblAditivos,
    tblGarantias,
    tblDesembolsos,
    tblFiadores,
    tblHipotecas;

window.onload = () => {
    document.getElementById('frmDeudor').reset();
    document.getElementById('frmCredito').reset();
    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "SGA-MUTUOS",
                archivo: "sgaGetCats",
                solicitados: ['all']
            }).then((datosRespuesta) => {
                $(".preloader").fadeIn("fast");
                configDocumento.initCats(datosRespuesta).then(resolve).catch(reject);
            }).catch(reject);
        });
    });

    gettingCats.then(() => {
        let idEdicion = getParameterByName('id');
        let allSelectpickerCbos = document.querySelectorAll('#credito select');
        allSelectpickerCbos.forEach(cbo => {
            cbo.disabled = true;
            cbo.className.includes('selectpicker') && ($("#" + cbo.id).selectpicker("refresh"));
        });
        configDatosCredito.campoFechaAprobacion.disabled = true;
        configDatosCredito.campoFechaGeneracion.disabled = true;
        configDatosCredito.campoNotario.readOnly = true;
        configDatosCredito.btnAditivos.disabled = true;
        configDocumento.buttonGeneracion.disabled = true;
        configDocumento.btnGarantias.disabled = true;
        if (idEdicion.length > 0) {
            idEdicion = atob(decodeURIComponent(idEdicion));
            fetchActions.get({
                modulo: "SGA-MUTUOS",
                archivo: 'sgaGetRC',
                params: {
                    id: idEdicion
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta.trim()) {
                        case "EXITO":
                            configDocumento.id = idEdicion;
                            let dependienteMutuo = false;
                            (datosRespuesta.datosCredito.idMutuo != null && datosRespuesta.datosCredito.idMutuo.length > 0) && (dependienteMutuo = true);
                            (datosRespuesta.datosCredito.idMutuo != null && datosRespuesta.datosCredito.idMutuo.length > 0) && (configDocumento.idMutuo = datosRespuesta.datosCredito.idMutuo);
                            $(".preloader").fadeIn('fast');
                            configDatosCredito.init({
                                ...datosRespuesta.datosCredito,
                                dependienteMutuo
                            }).then(() => {
                                configDatosCredito.datosOriginales = JSON.parse(JSON.stringify(configDatosCredito.getJSON()));
                                configDatosDeudor.init({
                                    ...datosRespuesta.datosDeudor,
                                    dependienteMutuo
                                }).then(() => {
                                    configDatosDeudor.datosOriginales = JSON.parse(JSON.stringify(configDatosDeudor.getJSON()));
                                    configGarantia.init({
                                        datosGarantias: [...datosRespuesta.datosGarantias],
                                        dependienteMutuo
                                    }).then(() => {
                                        configGarantia.datosOriginales = JSON.parse(JSON.stringify(configGarantia.garantias));
                                        $(".preloader").fadeOut("fast");
                                        configDatosCredito.campoNumeroCredito.readOnly = true;
                                        configDatosCredito.campoNumeroCredito.parentNode.querySelector('.input-group-append').remove();
                                        configDatosCredito.campoFechaAprobacion.disabled = false;
                                        configDatosCredito.campoFechaGeneracion.disabled = false;
                                        configDatosCredito.cboComite.disabled = false;
                                        $("#" + configDatosCredito.cboComite.id).selectpicker("refresh");
                                        configDatosCredito.campoNotario.readOnly = false;
                                        configDatosDeudor.campoDireccion.readOnly = false;
                                        configDocumento.buttonGeneracion.disabled = false;
                                        if(!dependienteMutuo){
                                            configDatosCredito.habilitarEdicion().then(() => {
                                                configDatosCredito.campoNumeroCredito.readOnly = true;
                                                configDatosDeudor.habilitarEdicion().then(() => {
                                                    configDatosDeudor.campoNumeroDocumento.value = datosRespuesta.datosDeudor.numeroDocumento;
                                                    this.btnGarantias.disabled = false;
                                                }).catch(generalMostrarError);
                                            }).catch(generalMostrarError);
                                        }
                                    });
                                });
                            });
                            // console.log(datosRespuesta);
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            $(".preloader").fadeOut("fast");
        }
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblAditivos").length) {
                tblAditivos = $("#tblAditivos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de aditivo';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblAditivos.columns.adjust().draw();
            }

            if ($("#tblGarantias").length) {
                tblGarantias = $("#tblGarantias").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de garantía';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblGarantias.columns.adjust().draw();
            }

            if ($("#tblDesembolsos").length) {
                tblDesembolsos = $("#tblDesembolsos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de desembolso';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblDesembolsos.columns.adjust().draw();
            }

            if ($("#tblFiadores").length) {
                tblFiadores = $("#tblFiadores").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblFiadores.columns.adjust().draw();
            }

            if ($("#tblHipotecas").length) {
                tblHipotecas = $("#tblHipotecas").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de hipoteca';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblHipotecas.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configDocumento = {
    id: 0,
    idMutuo: null,
    intentos: 5,
    datosGuardar: {},
    tabDeudor: document.getElementById('deudor-tab'),
    tabCredito: document.getElementById('credito-tab'),
    tabGarantias: document.getElementById('garantia-tab'),
    tabParciales: document.getElementById('parcial-tab'),
    formDeudor: document.getElementById('frmDeudor'),
    formCredito: document.getElementById("frmCredito"),
    buttonGeneracion: document.getElementById('btnGenerarDocumentos'),
    btnGarantias: document.getElementById('btnGarantias'),
    incluyeParciales: false,
    datosGuardar: {},
    initCats: function ({
        tiposDocumento: tiposDoc,
        agencias,
        datosGeograficos: geograficos,
        lineasFinancieras,
        periodicidades,
        fuentesFondo,
        aditivos,
        tiposGarantia,
        tiposPlazo,
        zonasHipoteca,
        comites
    }) {
        return new Promise((resolve, reject) => {
            try {
                let cbosTipoDocumento = document.querySelectorAll("select.cboTipoDocumento"),
                    cbosPais = document.querySelectorAll("select.cboPais"),
                    cbosAgencia = document.querySelectorAll("select.cboAgencia"),
                    cbosLineaFinanciera = document.querySelectorAll("select.cboLineaFinanciera"),
                    cbosPeriodicidad = document.querySelectorAll("select.cboPeriodicidad"),
                    cbosPlazo = document.querySelectorAll("select.cboPlazo"),
                    cbosFuenteFondos = document.querySelectorAll("select.cboFuenteFondos"),
                    cbosZonasHipoteca = document.querySelectorAll("select.cboZonaHipoteca"),
                    cbosComite = document.querySelectorAll("select.cboComite"),
                    allSelectpickerCbos = document.querySelectorAll("select.selectpicker");
                datosGeograficos = {
                    ...geograficos
                };

                const opciones = [`<option selected value="" disabled>seleccione</option>`];

                let tmpOpciones = [...opciones];
                tiposDoc.forEach(tipo => {
                    tiposDocumento[tipo.id] = {
                        ...tipo
                    };
                    tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
                });

                cbosTipoDocumento.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                for (let key in geograficos) {
                    tmpOpciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
                }
                cbosPais.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                agencias.forEach(agencia => tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`));
                cbosAgencia.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                lineasFinancieras.forEach(linea => tmpOpciones.push(`<option value="${linea.id}">${linea.lineaFinanciera}</option>`));
                cbosLineaFinanciera.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                periodicidades.forEach(periodicidad => tmpOpciones.push(`<option value="${periodicidad.id}">${periodicidad.periodicidad}</option>`));
                cbosPeriodicidad.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                tiposPlazo.forEach(tipo => tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoPlazo}</option>`));
                cbosPlazo.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                fuentesFondo.forEach(fuente => tmpOpciones.push(`<option value="${fuente.id}">${fuente.fuenteFondos}</option>`));
                cbosFuenteFondos.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                aditivos.forEach(aditivo => configDatosCredito.configCuota.configAditivos.catalogo[aditivo.id] = {
                    ...aditivo
                });

                tmpOpciones = [...opciones];
                tiposGarantia.forEach(tipo => configGarantia.catalogo[tipo.id] = {
                    ...tipo
                });

                tmpOpciones = [...opciones];
                comites.forEach(comite => tmpOpciones.push(`<option value="${comite.id}">${comite.comite}</option>`));
                cbosComite.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];

                for (let key in zonasHipoteca) {
                    tmpOpciones.push(`<option value="${zonasHipoteca[key].id}">${zonasHipoteca[key].zona}</option>`);
                }

                cbosZonasHipoteca.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));


                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    resultados: 0,
    search: function () {
        formActions.validate('frmCredito').then(({
            errores
        }) => {
            if (errores == 0) {
                fetchActions.get({
                    modulo: "SGA-MUTUOS",
                    archivo: "sgaSearchMutuo",
                    params: {
                        numeroCredito: encodeURIComponent(configDatosCredito.campoNumeroCredito.value)
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                switch (datosRespuesta.datosMutuo.respuesta.trim()) {
                                    case "EXITO":
                                        console.log(datosRespuesta);
                                        $(".preloader").fadeIn("fast");
                                        configDatosCredito.init({
                                            ...datosRespuesta.datosMutuo.datosCredito,
                                            dependienteMutuo: true
                                        }).then(() => {
                                            configDocumento.idMutuo = datosRespuesta.datosMutuo.datosCredito.id;
                                            configDatosDeudor.init({
                                                ...datosRespuesta.datosMutuo.datosDeudor,
                                                dependienteMutuo: true
                                            }).then(() => {
                                                configGarantia.init({
                                                    datosGarantias: [...datosRespuesta.datosMutuo.datosGarantias],
                                                    dependienteMutuo: true
                                                }).then(() => {
                                                    $(".preloader").fadeOut("fast");
                                                    configDatosCredito.campoNumeroCredito.readOnly = true;
                                                    configDatosCredito.campoNumeroCredito.parentNode.querySelector('.input-group-append').remove();
                                                    configDatosCredito.campoFechaAprobacion.disabled = false;
                                                    configDatosCredito.campoFechaGeneracion.disabled = false;
                                                    configDatosCredito.cboComite.disabled = false;
                                                    $("#" + configDatosCredito.cboComite.id).selectpicker("refresh");
                                                    configDatosCredito.campoNotario.readOnly = false;
                                                    configDatosDeudor.campoDireccion.readOnly = false;
                                                    configDocumento.buttonGeneracion.disabled = false;
                                                }).catch(generalMostrarError);
                                            }).catch(generalMostrarError);
                                        }).catch(generalMostrarError);
                                        break;
                                    case "SinRegistros":
                                        configDatosCredito.habilitarEdicion().then(() => {
                                            configDatosCredito.campoNumeroCredito.readOnly = true;
                                            configDatosCredito.campoNumeroCredito.parentNode.querySelector('.input-group-append').remove();
                                            configDatosDeudor.habilitarEdicion().then(() => {
                                                this.btnGarantias.disabled = false;
                                                configDocumento.buttonGeneracion.disabled = false;
                                            }).catch(generalMostrarError);
                                        }).catch(generalMostrarError);
                                        break;
                                    default:
                                        generalMostrarError(datosRespuesta);
                                        break;
                                }
                                break;
                            case "Generado":
                                Swal.fire({
                                    title: "¡Atención!",
                                    html: "Parece que los documentos para este número de crédito ya fueron generados, puedes optar por anularlos o editarlos desde el administrador de documentos.",
                                    icon: "warning"
                                });
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    cancel: function () {
        let tmpModulo = btoa('SGA-MUTUOS');
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Salir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                window.top.location.href = './?page=sgaResolucionClausula&mod=' + encodeURIComponent(tmpModulo);
            }
        });
    },
    evalParciales: function (opcion) {
        if (opcion == 'S') {
            this.incluyeParciales = true;
            this.tabParciales.classList.remove("disabled");
        } else {
            this.incluyeParciales = false;
            this.tabParciales.classList.add("disabled");
        }
    },
    save: function () {
        if (formActions.manualValidate(this.formDeudor.id)) {
            if (formActions.manualValidate(this.formCredito.id)) {
                if (Object.keys(configGarantia.garantias).length > 0) {
                    let garantiasConfiguradas = true,
                        garantiasPendientes = [],
                        excepcionConfiguracion = ['1', '4', '7', '8'];

                    for (let key in configGarantia.garantias) {
                        if (!excepcionConfiguracion.includes(configGarantia.garantias[key].idTipoGarantia) && Object.keys(configGarantia.garantias[key].configuracion).length == 0) {
                            garantiasConfiguradas = false;
                            garantiasPendientes.push(`- ${configGarantia.garantias[key].tipoGarantia}`);
                        }
                    }

                    if (garantiasConfiguradas) {
                        let formularioCompleto = true;

                        if (this.incluyeParciales && Object.keys(configDesembolsos.desembolsos).length == 0) {
                            formularioCompleto = false;
                            toastr.warning("Debe especificar el detalle de los desembolsos");
                            $("#" + this.tabParciales.id).trigger("click");
                        } else if (this.incluyeParciales) {
                            let montoCredito = parseFloat(configDatosCredito.campoMonto.value),
                                montoDesembolsos = 0;

                            for (let key in configDesembolsos.desembolsos) {
                                montoDesembolsos += parseFloat(configDesembolsos.desembolsos[key].monto);
                            }

                            if (montoCredito != montoDesembolsos) {
                                formularioCompleto = false;
                                $("#" + this.tabParciales.id).trigger("click");
                                Swal.fire({
                                    title: "¡Atención!",
                                    html: "El total de los montos de desembolsos no coíncide con el monto del crédito",
                                    icon: "warning"
                                });
                            }
                        }

                        if (formularioCompleto) {
                            document.getElementById("strIntentos").innerHTML = this.intentos;
                            $("#mdlGeneracion").modal("show");
                        }
                    } else {
                        $("#" + this.tabGarantias.id).trigger("click");
                        Swal.fire({
                            title: "¡Atención!",
                            html: `Las siguientes garantías requieren configuración: <br />${garantiasPendientes.join("<br />")}`,
                            icon: "warning"
                        });
                    }
                } else {
                    toastr.warning("Debe configurar al menos una garantía");
                    $("#" + this.tabGarantias.id).trigger("click");
                }
            } else {
                $("#" + this.tabCredito.id).trigger("click");
                toastr.warning("Datos de crédito incompletos");
            }
        } else {
            $("#" + this.tabDeudor.id).trigger("click");
            toastr.warning("Datos de deudor incompletos");
        }
    },
    generarDocumentos: function () {
        let existenCambios = this.evalCambios();
        fetchActions.set({
            modulo: "SGA-MUTUOS",
            archivo: 'sgaGuardarRC',
            datos: {
                ...this.datosGuardar,
                incluyeParciales: this.incluyeParciales,
                tipoApartado,
                idApartado
            }
        }).then((datosRespuesta) => {
            // console.log(datosRespuesta);
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta.trim()) {
                    case "EXITO":
                        this.id = datosRespuesta.id;
                        this.intentos--;
                        let tmpModulo = btoa('SGA-MUTUOS');

                        for (let key in configDatosCredito.configCuota.configAditivos.aditivos) {
                            configDatosCredito.configCuota.configAditivos.aditivos[key].id = datosRespuesta.idsAditivos[key];
                        }
                        for (let key in configDesembolsos.desembolsos) {
                            configDesembolsos.desembolsos[key].id = datosRespuesta.idsDesembolsos[key];
                        }
                        configDatosCredito.datosOriginales = JSON.parse(JSON.stringify(configDatosCredito.getJSON()));
                        configDatosDeudor.id = datosRespuesta.idDeudor;
                        configDatosDeudor.datosOriginales = {
                            ...configDatosDeudor.getJSON()
                        };

                        if (this.intentos == 0) {
                            window.top.location.href = './?page=sgaResolucionClausula&mod=' + tmpModulo;
                        }

                        for (let key in configGarantia.garantias) {
                            configGarantia.garantias[key].id = datosRespuesta.idsGarantias[key].id;
                            let configuracionUnica = ['2', '3'];
                            if (configuracionUnica.includes(configGarantia.garantias[key].idTipoGarantia)) {
                                configGarantia.garantias[key].configuracion.id = datosRespuesta.idsGarantias[key].configuracion.id;
                            } else {
                                for (let key2 in configGarantia.garantias[key].configuracion) {
                                    configGarantia.garantias[key].configuracion[key2].id = datosRespuesta.idsGarantias[key].configuracion[key2].id;
                                }
                            }
                        }

                        configGarantia.datosOriginales = JSON.parse(JSON.stringify(configGarantia.garantias));
                        document.getElementById("strIntentos").innerHTML = this.intentos;
                        if (datosRespuesta.Errores.length > 0 || Object.keys(datosRespuesta.Errores).length > 0) {
                            let arrErrores = [];
                            if (datosRespuesta.Errores.length > 0) {
                                arrErrores = [...datosRespuesta.Errores];
                            } else {
                                for (let key in datosRespuesta.Errores) {
                                    arrErrores.push(`${key}: ${datosRespuesta.Errores[key]}`);
                                }
                            }
                            Swal.fire({
                                title: "¡Atención!",
                                icon: "warning",
                                html: `Parece que sólo se guardó parte de la información y ocurrió un error con la base de datos, los errores ocurridos son: <br> -${arrErrores.join("<br />-")}`
                            });
                        } else {
                            // Swal.fire({
                            //     title: "¡Bien hecho!",
                            //     icon: "success",
                            //     html: "Parece que todo va bien, algún día el sistema generará el documento"
                            // });
                            window.open('./main/SGA-MUTUOS/docs/Resolucion/' + datosRespuesta.nombreResolucion);
                            window.open('./main/SGA-MUTUOS/docs/Clausula/' + datosRespuesta.nombreClausula);
                        }
                        break;
                    case "Generado":
                        Swal.fire({
                            title: "¡Atención!",
                            html: "Parece que los documentos para este número de crédito ya fueron generados, puedes optar por anularlos o editarlos desde el administrador de documentos.",
                            icon: "warning"
                        });
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    prepareData: function () {
        let datos = {
            id: this.id,
            datosDeudor: {
                ...configDatosDeudor.getJSON()
            },
            datosCredito: {
                ...configDatosCredito.getJSON()
            },
            garantias: JSON.parse(JSON.stringify(configGarantia.garantias)),
        };

        return {
            ...datos
        };
    },
    evalCambios: function () {
        let data = this.prepareData();
        this.datosGuardar = {
            ...data
        };
        let existenCambios = false;

        if (Object.keys(configDatosCredito.datosOriginales).length == 0) {
            this.datosGuardar.datosCredito.logs = {
                descripcion: "Creación de Resolución y clausula"
            };
            if (Object.keys(this.datosGuardar.datosCredito.aditivos).length > 0) {
                for (let key in this.datosGuardar.datosCredito.aditivos) {
                    this.datosGuardar.datosCredito.aditivos[key].logs = {
                        accion: "Guardar",
                        descripcion: "Creación de aditivo"
                    }
                }
            }
            if (Object.keys(this.datosGuardar.datosCredito.detalleDesembolsos).length > 0) {
                for (let key in this.datosGuardar.datosCredito.detalleDesembolsos) {
                    this.datosGuardar.datosCredito.detalleDesembolsos[key].logs = {
                        accion: "Guardar",
                        descripcion: "Creación de desembolso"
                    }
                }
            }
            existenCambios = true;
        } else {
            //Validar cambios
            let detalles = {},
                cnt = 0;

            for (let key in this.datosGuardar.datosCredito) {
                if (key != 'aditivos' && key != 'detalleDesembolsos' && key != 'totalCuota' && !key.includes('label')) {
                    let nuevoValor = this.datosGuardar.datosCredito[key],
                        valorAnterior = configDatosCredito.datosOriginales[key];
                    (nuevoValor != null && nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                    (valorAnterior != null && valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                    if (nuevoValor != valorAnterior) {
                        cnt++;
                        detalles[cnt] = {
                            campo: key,
                            valorAnterior,
                            nuevoValor
                        }
                    }
                } else if (key == 'aditivos') {
                    let tmpAditivos = configDatosCredito.datosOriginales.aditivos,
                        aditivosRecurrentes = [];
                    for (let key in tmpAditivos) {
                        for (let key2 in this.datosGuardar.datosCredito.aditivos) {
                            if ((this.datosGuardar.datosCredito.aditivos[key2].idAditivo == tmpAditivos[key].idAditivo) && this.datosGuardar.datosCredito.aditivos[key2].id != 0) {
                                aditivosRecurrentes.push(tmpAditivos[key].idAditivo);
                                let nuevoValor = this.datosGuardar.datosCredito.aditivos[key2].monto,
                                    valorAnterior = tmpAditivos[key].monto;
                                (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                                (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                                if (nuevoValor != valorAnterior) {
                                    this.datosGuardar.datosCredito.aditivos[key2].logs = {
                                        accion: 'Guardar',
                                        descripcion: 'Edición de monto de aditivo',
                                        detalles: {
                                            campo: 'monto',
                                            valorAnterior,
                                            nuevoValor
                                        }
                                    }
                                    existenCambios = true;
                                }
                            }
                        }
                    }

                    for (let key in tmpAditivos) {
                        if (!aditivosRecurrentes.includes(tmpAditivos[key].idAditivo)) {
                            configDatosCredito.configCuota.configAditivos.cnt++;
                            this.datosGuardar.datosCredito.aditivos[configDatosCredito.configCuota.configAditivos.cnt] = {
                                idAditivo: tmpAditivos[key].idAditivo,
                                id: tmpAditivos[key].id,
                                logs: {
                                    accion: "Eliminar",
                                    descripcion: 'Eliminación de aditivo',
                                    detalles: {}
                                }
                            }
                            existenCambios = true;
                        }
                    }

                    for (let key in this.datosGuardar.datosCredito.aditivos) {
                        if (this.datosGuardar.datosCredito.aditivos[key].id == 0 && !this.datosGuardar.datosCredito.aditivos[key].logs) {
                            this.datosGuardar.datosCredito.aditivos[key].logs = {
                                accion: "Guardar",
                                descripcion: "Creación de aditivo"
                            }
                            existenCambios = true;
                        }
                    }
                } else if (key == 'detalleDesembolsos') {
                    let tmpDesembolsos = configDatosCredito.datosOriginales.detalleDesembolsos,
                        desembolsosRecurrentes = [];

                    for (let key in tmpDesembolsos) {
                        for (let key2 in this.datosGuardar.datosCredito.detalleDesembolsos) {
                            let detalles = {},
                                cnt = 0;
                            if (tmpDesembolsos[key].id == this.datosGuardar.datosCredito.detalleDesembolsos[key2].id) {
                                desembolsosRecurrentes.push(tmpDesembolsos[key].id);
                                for (let key3 in this.datosGuardar.datosCredito.detalleDesembolsos[key2]) {
                                    let nuevoValor = this.datosGuardar.datosCredito.detalleDesembolsos[key][key3],
                                        valorAnterior = tmpDesembolsos[key][key3];
                                    (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                                    (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                                    if (valorAnterior != nuevoValor) {
                                        cnt++;
                                        detalles[cnt] = {
                                            campo: key3,
                                            valorAnterior,
                                            nuevoValor
                                        }
                                    }
                                }
                            }

                            if (Object.keys(detalles).length > 0) {
                                this.datosGuardar.datosCredito.detalleDesembolsos[key2].logs = {
                                    accion: 'Guardar',
                                    descripcion: "Edición de desembolso parcial",
                                    detalles
                                }
                                existenCambios = true;
                            }
                        }
                    }

                    for (let key in tmpDesembolsos) {
                        if (!desembolsosRecurrentes.includes(tmpDesembolsos[key].id)) {
                            configDesembolsos.cnt++;
                            this.datosGuardar.datosCredito.detalleDesembolsos[configDesembolsos.cnt] = {
                                id: tmpDesembolsos[key].id,
                                logs: {
                                    accion: 'Eliminar',
                                    descripcion: 'Eliminación de desembolso parcial'
                                }
                            }
                            existenCambios = true;
                        }
                    }

                    for (let key in this.datosGuardar.datosCredito.detalleDesembolsos) {
                        if (this.datosGuardar.datosCredito.detalleDesembolsos[key].id == 0 && !this.datosGuardar.datosCredito.detalleDesembolsos[key].logs) {
                            this.datosGuardar.datosCredito.detalleDesembolsos[key].logs = {
                                accion: "Guardar",
                                descripcion: "Creación de desembolso"
                            }
                            existenCambios = true;
                        }
                    }
                }
            }

            if (Object.keys(detalles).length > 0) {
                this.datosGuardar.datosCredito.logs = {
                    descripcion: 'Edición de valores de crédito',
                    detalles
                };
                existenCambios = true;
            }
        }

        if (Object.keys(configDatosDeudor.datosOriginales).length == 0) {
            this.datosGuardar.datosDeudor.logs = {
                descripcion: 'Creación de deudor/a'
            }
            existenCambios = true;
        } else {
            //Validar cambios
            let detalles = {},
                cnt = 0;

            for (let key in this.datosGuardar.datosDeudor) {
                if (!key.includes('label')) {
                    let nuevoValor = this.datosGuardar.datosDeudor[key],
                        valorAnterior = configDatosDeudor.datosOriginales[key];
                    (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                    (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                    if (valorAnterior != nuevoValor) {
                        cnt++;
                        detalles[cnt] = {
                            campo: key,
                            valorAnterior,
                            nuevoValor
                        };
                    }
                }
            }

            if (Object.keys(detalles).length > 0) {
                this.datosGuardar.datosDeudor.logs = {
                    descripcion: 'Edición de datos de deudor/a',
                    detalles
                };
                existenCambios = true;
            }
        }

        if (Object.keys(configGarantia.datosOriginales).length == 0) {
            let configuracionUnica = ['2', '3'];
            for (let key in this.datosGuardar.garantias) {
                this.datosGuardar.garantias[key].logs = {
                    accion: "Guardar",
                    descripcion: "Creación de garantía"
                }
                if (Object.keys(this.datosGuardar.garantias[key].configuracion).length > 0 && configuracionUnica.includes(this.datosGuardar.garantias[key].idTipoGarantia)) {
                    this.datosGuardar.garantias[key].configuracion.logs = {
                        accion: "Guardar",
                        descripcion: 'Creación de datos'
                    };
                } else {
                    for (let key2 in this.datosGuardar.garantias[key].configuracion) {
                        this.datosGuardar.garantias[key].configuracion[key2].logs = {
                            accion: "Guardar",
                            descripcion: 'Creación de registro'
                        }
                    }
                }
            }
            existenCambios = true;
        } else {
            //Validar cambios
            let tmpGarantias = configGarantia.datosOriginales,
                garantiasRecurrentes = [],
                configuracionUnica = ['2', '3'];
            for (let key in tmpGarantias) {
                for (let key2 in this.datosGuardar.garantias) {
                    if ((this.datosGuardar.garantias[key2].idTipoGarantia == tmpGarantias[key].idTipoGarantia) && this.datosGuardar.garantias[key2].id != 0) {
                        garantiasRecurrentes.push(tmpGarantias[key].idTipoGarantia);
                        if (Object.keys(this.datosGuardar.garantias[key2].configuracion).length > 0 && configuracionUnica.includes(this.datosGuardar.garantias[key2].idTipoGarantia)) {
                            let detalles = {},
                                cnt = 0;
                            for (let key3 in tmpGarantias[key].configuracion) {
                                let nuevoValor = this.datosGuardar.garantias[key2].configuracion[key3],
                                    valorAnterior = tmpGarantias[key].configuracion[key3];

                                (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                                (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                                if (valorAnterior != nuevoValor) {
                                    cnt++;
                                    detalles[cnt] = {
                                        campo: key3,
                                        valorAnterior,
                                        nuevoValor
                                    };
                                }
                            }
                            if (Object.keys(detalles).length > 0) {
                                this.datosGuardar.garantias[key2].configuracion.logs = {
                                    accion: "Guardar",
                                    descripcion: "Edición de configuración de garantía",
                                    detalles
                                }
                                existenCambios = true;
                            }
                        } else {
                            let configuracionRecurrente = [],
                                tmpConfiguracionActual = this.datosGuardar.garantias[key2].configuracion;
                            for (let key3 in tmpGarantias[key].configuracion) {
                                for (let key4 in tmpConfiguracionActual) {
                                    if (tmpGarantias[key].configuracion[key3].id == tmpConfiguracionActual[key4].id) {
                                        configuracionRecurrente.push(tmpGarantias[key].configuracion[key3].id);
                                        let detalles = {},
                                            cnt = 0;
                                        for (let key5 in tmpGarantias[key].configuracion[key3]) {
                                            if (!key5.includes('label') && key5 != 'matriculas' && key5 != 'modificaciones') {
                                                let valorAnterior = tmpGarantias[key].configuracion[key3][key5],
                                                    nuevoValor = tmpConfiguracionActual[key4][key5];
                                                (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                                                (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                                                if (valorAnterior != nuevoValor) {
                                                    cnt++;
                                                    detalles[cnt] = {
                                                        campo: key5,
                                                        valorAnterior,
                                                        nuevoValor
                                                    }
                                                }
                                            }
                                        }
                                        if (Object.keys(detalles).length > 0) {
                                            let descripcion = 'Edición de parámetros de garantía';
                                            switch (this.datosGuardar.garantias[key2].idTipoGarantia) {
                                                case "5":
                                                    descripcion = 'Edición de datos de fiador';
                                                    break;
                                                case "6":
                                                    descripcion = 'Edición de datos de hipoteca';
                                                    break;
                                            }
                                            this.datosGuardar.garantias[key2].configuracion[key4].logs = {
                                                accion: "Guardar",
                                                descripcion,
                                                detalles
                                            }
                                            existenCambios = true;
                                        }
                                    }
                                }
                            }

                            let indicesActuales = Object.keys(this.datosGuardar.garantias[key2].configuracion),
                                tmpCnt = 0;
                            indicesActuales.sort(function (a, b) {
                                return a - b;
                            });

                            tmpCnt = indicesActuales[indicesActuales.length - 1];

                            for (let key3 in tmpGarantias[key].configuracion) {
                                if (!configuracionRecurrente.includes(tmpGarantias[key].configuracion[key3].id)) {
                                    let descripcion = 'Eliminación de parámetros de garantía';
                                    switch (this.datosGuardar.garantias[key2].idTipoGarantia) {
                                        case "5":
                                            descripcion = 'Eliminación de datos de fiador';
                                            break;
                                        case "6":
                                            descripcion = 'Eliminación de datos de hipoteca';
                                            break;
                                    }
                                    tmpCnt++;
                                    this.datosGuardar.garantias[key2].configuracion[tmpCnt] = {
                                        id: tmpGarantias[key].configuracion[key3].id,
                                        logs: {
                                            accion: "Eliminar",
                                            descripcion,
                                        }
                                    }
                                    existenCambios = true;
                                }
                            }

                            for (let key3 in tmpConfiguracionActual) {
                                if (tmpConfiguracionActual[key3].id == 0 && !tmpConfiguracionActual[key3].logs) {
                                    this.datosGuardar.garantias[key2].configuracion[key3].logs = {
                                        accion: "Guardar",
                                        descripcion: "Creación de datos"
                                    }
                                    existenCambios = true;
                                }
                            }
                        }
                    }
                }
            }

            for (let key in tmpGarantias) {
                if (!garantiasRecurrentes.includes(tmpGarantias[key].idTipoGarantia)) {
                    configGarantia.cnt++;
                    this.datosGuardar.garantias[configGarantia.cnt] = {
                        id: tmpGarantias[key].id,
                        logs: {
                            accion: "Eliminar",
                            descripcion: "Eliminar tipo de garantía"
                        }
                    }
                    existenCambios = true;
                }
            }

            for (let key in this.datosGuardar.garantias) {
                if (this.datosGuardar.garantias[key].id == 0 && !this.datosGuardar.garantias[key].logs) {
                    this.datosGuardar.garantias[key].logs = {
                        accion: "Guardar",
                        descripcion: "Creación de garantía"
                    }
                    existenCambios = true;
                }
            }
        }

        return existenCambios;
    }
}

const configDatosCredito = {
    datosOriginales: {},
    cboAgencia: document.getElementById("cboAgencia"),
    campoNumeroCredito: document.getElementById("txtNumeroCredito"),
    campoMonto: document.getElementById("numMontoCredito"),
    campoTasaInteres: document.getElementById("numTasaInteres"),
    cboLineaFinanciera: document.getElementById("cboLineaFinanciera"),
    campoDestino: document.getElementById("txtDestinoCredito"),
    cboVencimiento: document.getElementById("cboVencimiento"),
    cboPeriodicidad: document.getElementById("cboPeriodicidad"),
    campoPlazo: document.getElementById("numPlazo"),
    cboPlazo: document.getElementById("cboPlazo"),
    cboFuenteFondos: document.getElementById("cboFuenteFondos"),
    cboDesembolsoParcial: document.getElementById("cboDesembolsoParcial"),
    campoFechaAprobacion: document.getElementById("txtFechaAprobacion"),
    campoFechaGeneracion: document.getElementById('txtFechaGeneracion'),
    cboComite: document.getElementById('cboComite'),
    campoNotario: document.getElementById('txtNotario'),
    btnAditivos: document.getElementById('btnAditivos'),
    init: function (tmpDatos = {}) {
        return new Promise((resolve, reject) => {
            try {
                if (Object.keys(tmpDatos).length > 0) {
                    let allSelectpickerCbos = document.querySelectorAll('#credito select.selectpicker'),
                        aditivoEditable = !tmpDatos.dependienteMutuo;
                    this.cboAgencia.value = tmpDatos.agencia;
                    this.campoNumeroCredito.value = tmpDatos.numeroCredito;
                    this.campoMonto.value = tmpDatos.monto;
                    this.campoTasaInteres.value = tmpDatos.tasaInteres;
                    this.cboLineaFinanciera.value = tmpDatos.lineaFinanciera;
                    this.campoDestino.value = tmpDatos.destino;
                    this.cboVencimiento.value = tmpDatos.alVencimiento;
                    this.cboPeriodicidad.value = tmpDatos.periodicidad;
                    this.campoPlazo.value = tmpDatos.plazo;
                    this.cboPlazo.value = tmpDatos.tipoPlazo;
                    this.cboFuenteFondos.value = tmpDatos.fuenteFondos;
                    this.cboDesembolsoParcial.value = tmpDatos.desembolsoParcial;
                    $("#" + this.cboDesembolsoParcial.id).trigger("change");
                    this.configCuota.capitalInteres.value = tmpDatos.capitalInteres;
                    this.campoFechaAprobacion.value = tmpDatos.fechaAprobacion ? tmpDatos.fechaAprobacion : '';
                    this.campoFechaGeneracion.value = tmpDatos.fechaGeneracion ? tmpDatos.fechaGeneracion : '';
                    this.campoFechaAprobacion.value.length > 0 && ($("#" + this.campoFechaAprobacion.id).datepicker("update", tmpDatos.fechaAprobacion));
                    this.campoFechaGeneracion.value.length > 0 && ($("#" + this.campoFechaGeneracion.id).datepicker("update", tmpDatos.fechaGeneracion));
                    if (tmpDatos.comite) {
                        this.cboComite.value = tmpDatos.comite;
                    }
                    this.campoNotario.value = tmpDatos.notario ? tmpDatos.notario : '';

                    tmpDatos.aditivos.forEach(aditivo => {
                        this.configCuota.configAditivos.cnt++;
                        this.configCuota.configAditivos.aditivos[this.configCuota.configAditivos.cnt] = {
                            id: aditivo.id,
                            idAditivo: aditivo.idAditivo,
                            aditivo: this.configCuota.configAditivos.catalogo[aditivo.idAditivo].aditivo,
                            monto: aditivo.monto,
                            editable: aditivoEditable
                        };
                        this.configCuota.configAditivos.addRowTbl({
                            ...this.configCuota.configAditivos.aditivos[this.configCuota.configAditivos.cnt],
                            nFila: this.configCuota.configAditivos.cnt
                        });
                    });
                    tblAditivos.columns.adjust().draw();
                    this.configCuota.calcularCuota();

                    if (tmpDatos.desembolsoParcial == 'S') {
                        if (tmpDatos.parciales) {
                            configDesembolsos.init(tmpDatos.parciales);
                        }
                    }

                    allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
                    !tmpDatos.dependienteMutuo && (this.datosOriginales = JSON.parse(JSON.stringify(this.getJSON())));
                } else {

                }
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    habilitarEdicion: function () {
        return new Promise((resolve, reject) => {
            try {
                let allSelectpickerCbos = document.querySelectorAll('#credito select.selectpicker')
                this.cboAgencia.disabled = false;
                this.campoMonto.readOnly = false;
                this.campoTasaInteres.readOnly = false;
                this.cboLineaFinanciera.disabled = false;
                this.campoDestino.readOnly = false;
                this.cboVencimiento.disabled = false;
                this.cboPeriodicidad.disabled = false;
                this.campoPlazo.readOnly = false;
                this.cboPlazo.disabled = false;
                this.campoFechaAprobacion.disabled = false;
                this.cboFuenteFondos.disabled = false;
                this.cboDesembolsoParcial.disabled = false;
                this.campoFechaGeneracion.disabled = false;
                this.cboComite.disabled = false;
                this.campoNotario.readOnly = false;
                this.configCuota.capitalInteres.readOnly = false;
                this.btnAditivos.disabled = false;

                allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    getJSON: function () {
        let datos = {
            idMutuo: configDocumento.idMutuo,
            agencia: this.cboAgencia.value,
            labelAgencia: this.cboAgencia.options[this.cboAgencia.options.selectedIndex].text,
            numeroCredito: this.campoNumeroCredito.value,
            monto: this.campoMonto.value,
            tasaInteres: this.campoTasaInteres.value,
            capitalInteres: this.configCuota.capitalInteres.value,
            aditivos: JSON.parse(JSON.stringify(this.configCuota.configAditivos.aditivos)),
            totalCuota: this.configCuota.campoTotalCuota.value,
            lineaFinanciera: this.cboLineaFinanciera.value,
            labelLineaFinanciera: this.cboLineaFinanciera.options[this.cboLineaFinanciera.options.selectedIndex].text,
            destino: this.campoDestino.value,
            vencimiento: this.cboVencimiento.value,
            periodicidad: this.cboPeriodicidad.value,
            labelPeriodicidad: this.cboPeriodicidad.options[this.cboPeriodicidad.options.selectedIndex].text,
            plazo: this.campoPlazo.value,
            tipoPlazo: this.cboPlazo.value,
            labelTipoPlazo: this.cboPlazo.options[this.cboPlazo.options.selectedIndex].text,
            fuenteFondos: this.cboFuenteFondos.value,
            labelFuenteFondos: this.cboFuenteFondos.options[this.cboFuenteFondos.options.selectedIndex].text,
            desembolsoParcial: this.cboDesembolsoParcial.value,
            fechaGeneracion: this.campoFechaGeneracion.value,
            fechaAprobacion: this.campoFechaAprobacion.value,
            comite: this.cboComite.value,
            labelComite: this.cboComite.options[this.cboComite.options.selectedIndex].text,
            notario: this.campoNotario.value,
            detalleDesembolsos: JSON.parse(JSON.stringify(configDesembolsos.desembolsos)),
        };

        return datos;
    },
    configCuota: {
        campoTotalCuota: document.getElementById("numMontoTotal"),
        capitalInteres: document.getElementById("numCapitalInteres"),
        configAditivos: {
            catalogo: {},
            aditivos: {},
            id: 0,
            cnt: 0,
            cboAditivo: document.getElementById("cboAditivo"),
            montoAditivo: document.getElementById("numMontoAditivo"),
            mdlTitle: document.querySelector("#mdlAditivo h5.modal-title"),
            init: function (tmpDatos) {
                return new Promise((resolve, reject) => {

                });
            },
            edit: function (id = 0) {
                formActions.clean("mdlAditivo").then(() => {
                    const leyendo = new Promise((resolve, reject) => {
                        try {
                            let opciones = [`<option selected disables value="">seleccione</option>`],
                                incluidos = [];
                            this.id = id;
                            for (let key in this.aditivos) {
                                if (!this.aditivos[id] || this.aditivos[key].idAditivo != this.aditivos[id].idAditivo) {
                                    incluidos.push(this.aditivos[key].idAditivo);
                                }
                            }
                            for (let key in this.catalogo) {
                                if (!incluidos.includes(this.catalogo[key].id)) {
                                    opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].aditivo}</option>`);
                                }
                            }

                            this.cboAditivo.innerHTML = opciones.join("");
                            this.cboAditivo.disabled = false;

                            if (id > 0) {
                                this.cboAditivo.value = this.aditivos[id].idAditivo;
                                this.montoAditivo.value = this.aditivos[id].monto;
                                this.cboAditivo.disabled = true;
                                resolve();
                            } else {
                                resolve();
                            }
                        } catch (err) {
                            reject(err.message);
                        }
                    });

                    leyendo.then(() => {
                        $("#" + this.cboAditivo.id).selectpicker("refresh");
                        $("#mdlAditivo").modal("show");
                    }).catch(generalMostrarError);
                });
            },
            save: function () {
                formActions.validate('frmAditivo').then(({
                    errores
                }) => {
                    if (errores == 0) {
                        let tmp = {
                            id: this.id > 0 ? this.aditivos[this.id].id : 0,
                            idAditivo: this.cboAditivo.value,
                            aditivo: this.cboAditivo.options[this.cboAditivo.options.selectedIndex].text,
                            monto: this.montoAditivo.value
                        }
                        let guardando = new Promise((resolve, reject) => {
                            if (this.id == 0) {
                                this.cnt++;
                                this.id = this.cnt;
                                this.addRowTbl({
                                    ...tmp,
                                    nFila: this.cnt
                                }).then(resolve).catch(reject);
                            } else {
                                $("#trAditivo" + this.id).find("td:eq(1)").html(this.catalogo[this.cboAditivo.value].aditivo);
                                $("#trAditivo" + this.id).find("td:eq(2)").html("$" + parseFloat(this.montoAditivo.value).toFixed(2));
                                resolve();
                            }
                        });
                        guardando.then(() => {
                            this.aditivos[this.id] = {
                                ...tmp
                            };
                            tblAditivos.columns.adjust().draw();
                            $("#mdlAditivo").modal("hide");
                            configDatosCredito.configCuota.calcularCuota();
                        }).catch(generalMostrarError);
                    }
                }).catch(generalMostrarError);
            },
            addRowTbl: function ({
                nFila,
                idAditivo,
                aditivo,
                monto,
                editable = true
            }) {
                return new Promise((resolve, reject) => {
                    try {
                        let botones = "<div class='tblButtonContainer'>" +
                            "<span class='btnTbl' title='Editar' onclick='configDatosCredito.configCuota.configAditivos.edit(" + nFila + ");'>" +
                            "<i class='fas fa-edit'></i>" +
                            "</span>" +
                            "<span class='btnTbl' title='Eliminar' onclick='configDatosCredito.configCuota.configAditivos.delete(" + nFila + ");'>" +
                            "<i class='fas fa-trash'></i>" +
                            "</span>" +
                            "</div>";
                        botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                        !editable && (botones = 'Datos configurados en mutuo');
                        tblAditivos.row.add([
                            nFila,
                            aditivo,
                            "$" + parseFloat(monto).toFixed(2),
                            botones
                        ]).node().id = "trAditivo" + nFila;
                        resolve();
                    } catch (err) {
                        reject(err.message);
                    }
                });
            },
            delete: function (id) {
                let aditivoLabel = this.aditivos[id].aditivo;
                Swal.fire({
                    title: 'Eliminar aditivo',
                    icon: 'warning',
                    html: '¿Seguro/a que quieres <b>eliminar</b> el aditivo ' + aditivoLabel + '?.',
                    showCloseButton: false,
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
                    cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
                }).then((result) => {
                    if (result.isConfirmed) {
                        tblAditivos.row('#trAditivo' + id).remove().draw();
                        delete this.aditivos[id];
                        configDatosCredito.configCuota.calcularCuota();
                    }
                });
            }
        },
        calcularCuota: function () {
            let totalCuota = 0;
            this.capitalInteres.value && (totalCuota += parseFloat(this.capitalInteres.value));
            for (let key in this.configAditivos.aditivos) {
                totalCuota += parseFloat(this.configAditivos.aditivos[key].monto);
            }

            this.campoTotalCuota.value = totalCuota.toFixed(2);
        }
    },
    evalRotativa: function (opcion) {
        this.campoFechaVencimiento.value = '';
        $("#" + this.campoFechaVencimiento.id).datepicker("update", "");
        if (opcion == 'S') {
            this.campoFechaVencimiento.disabled = false;
        } else {
            this.campoFechaVencimiento.disabled = true;
        }
    }
}

const configDatosDeudor = {
    id: 0,
    datosOriginales: {},
    campoCodigoCliente: document.getElementById("txtCodigoCliente"),
    cboTipoDocumento: document.getElementById("cboTipoDocumentoDeudor"),
    campoNumeroDocumento: document.getElementById("txtNumeroDocumentoDeudor"),
    campoNIT: document.getElementById("txtNITDeudor"),
    campoNombres: document.getElementById("txtNombresDeudor"),
    campoApellidos: document.getElementById("txtApellidosDeudor"),
    campoConocido: document.getElementById("txtConocidoDeudor"),
    cboGenero: document.getElementById("cboGeneroDeudor"),
    cboPais: document.getElementById("cboPaisDeudor"),
    cboDepartamento: document.getElementById("cboDepartamentoDeudor"),
    cboMunicipio: document.getElementById("cboMunicipioDeudor"),
    campoDireccion: document.getElementById("txtDireccionDeudor"),
    init: function (tmpDatos = {}) {
        return new Promise((resolve, reject) => {
            try {
                if (Object.keys(tmpDatos).length > 0) {
                    let allSelectpickerCbos = document.querySelectorAll('#deudor select.selectpicker');
                    this.campoCodigoCliente.value = tmpDatos.codigoCliente;
                    this.cboTipoDocumento.value = tmpDatos.tipoDocumento;
                    $("#" + this.cboTipoDocumento.id).trigger("change");
                    this.campoNumeroDocumento.value = tmpDatos.numeroDocumento;
                    tmpDatos.dependienteMutuo && (this.campoNumeroDocumento.readOnly = true);
                    this.campoNIT.value = tmpDatos.nit;
                    this.campoNombres.value = tmpDatos.nombres;
                    this.campoApellidos.value = tmpDatos.apellidos;
                    this.campoConocido.value = tmpDatos.conocido;
                    this.cboGenero.value = tmpDatos.genero;
                    this.cboPais.value = tmpDatos.pais;
                    $("#" + this.cboPais.id).trigger("change");
                    this.cboDepartamento.value = tmpDatos.departamento;
                    $("#" + this.cboDepartamento.id).trigger("change");
                    this.cboMunicipio.value = tmpDatos.municipio;
                    this.campoDireccion.value = tmpDatos.direccion ? tmpDatos.direccion : '';

                    allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
                    this.id = tmpDatos.id;
                    if (!tmpDatos.dependienteMutuo) {
                        this.datosOriginales = {
                            ...this.getJSON()
                        };
                    }
                }
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    getJSON: function () {
        let datos = {
            id: this.id,
            codigoCliente: this.campoCodigoCliente.value,
            tipoDocumento: this.cboTipoDocumento.value,
            labelTipoDocumento: this.cboTipoDocumento.options[this.cboTipoDocumento.options.selectedIndex].text,
            numeroDocumento: this.campoNumeroDocumento.value,
            nit: this.campoNIT.value,
            nombres: this.campoNombres.value,
            apellidos: this.campoApellidos.value,
            conocido: this.campoConocido.value,
            genero: this.cboGenero.value,
            labelGenero: this.cboGenero.options[this.cboGenero.options.selectedIndex].text,
            pais: this.cboPais.value,
            labelPais: this.cboPais.options[this.cboPais.options.selectedIndex].text,
            departamento: this.cboDepartamento.value,
            labelDepartamento: this.cboDepartamento.options[this.cboDepartamento.options.selectedIndex].text,
            municipio: this.cboMunicipio.value,
            labelMunicipio: this.cboMunicipio.options[this.cboMunicipio.options.selectedIndex].text,
            direccion: this.campoDireccion.value,
        };

        return datos;
    },
    habilitarEdicion: function () {
        return new Promise((resolve, reject) => {
            try {
                let allSelectpickerCbos = document.querySelectorAll('#deudor select.selectpicker')
                this.campoCodigoCliente.readOnly = false;
                this.cboTipoDocumento.disabled = false;
                $('#' + this.cboTipoDocumento.id).trigger('change');
                this.campoNIT.readOnly = false;
                this.campoNombres.readOnly = false;
                this.campoApellidos.readOnly = false;
                this.campoConocido.readOnly = false;
                this.cboGenero.disabled = false;
                this.cboPais.disabled = false;
                this.cboDepartamento.disabled = false;
                this.cboMunicipio.disabled = false;
                this.campoDireccion.readOnly = false;
                allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    }
}

const configGarantia = {
    id: 0,
    cnt: 0,
    catalogo: {},
    garantias: {},
    datosOriginales: {},
    cboTipoGarantia: document.getElementById("cboTipoGarantia"),
    init: function (tmpDatosGarantias) {
        return new Promise((resolve, reject) => {
            try {
                if (Object.keys(tmpDatosGarantias).length > 0) {
                    let tmpDatos = tmpDatosGarantias.datosGarantias;
                    tmpDatos.forEach(garantia => {
                        this.cnt++;
                        let configuracion = {};

                        switch (garantia.idTipoGarantia) {
                            case '2':
                            case '3':
                                if (garantia.configuracion.descripcion) {
                                    configuracion = {
                                        id: garantia.configuracion.id,
                                        descripcion: garantia.configuracion.descripcion
                                    }
                                }
                                break;
                            case '5':
                            case '6':
                                garantia.configuracion.forEach(registro => {
                                    if (registro.descripcion) {
                                        if (garantia.idTipoGarantia == '5') {
                                            configFiduciaria.cnt++;
                                            configuracion[configFiduciaria.cnt] = {
                                                id: registro.id,
                                                descripcion: registro.descripcion
                                            }
                                            configFiduciaria.addRowTbl({
                                                ...configuracion[configFiduciaria.cnt],
                                                nFila: configFiduciaria.cnt
                                            });
                                        } else if (garantia.idTipoGarantia == '6') {
                                            configHipotecaria.cnt++;
                                            configuracion[configHipotecaria.cnt] = {
                                                id: registro.id,
                                                descripcion: registro.descripcion
                                            }
                                            configHipotecaria.addRowTbl({
                                                ...configuracion[configHipotecaria.cnt],
                                                nFila: configHipotecaria.cnt
                                            });
                                        }
                                    }
                                });
                                tblFiadores.columns.adjust().draw();
                                tblHipotecas.columns.adjust().draw();
                                break;
                        }

                        this.garantias[this.cnt] = {
                            id: garantia.id,
                            idTipoGarantia: garantia.idTipoGarantia,
                            tipoGarantia: this.catalogo[garantia.idTipoGarantia].tipoGarantia,
                            configurada: 'SI',
                            configuracion,
                            dependienteMutuo: tmpDatosGarantias.dependienteMutuo
                        };

                        configGarantia.id = this.cnt;
                        if (!tmpDatosGarantias.dependienteMutuo) {
                            configGarantia.datosOriginales[this.cnt] = JSON.parse(JSON.stringify({
                                id: garantia.id,
                                idTipoGarantia: garantia.idTipoGarantia,
                                tipoGarantia: this.catalogo[garantia.idTipoGarantia].tipoGarantia,
                                configurada: Object.keys(configuracion).length > 0 ? 'SI' : 'NO',
                                configuracion: {
                                    ...configuracion
                                }
                            }));
                        }
                        let eliminacionGarantia = !tmpDatosGarantias.dependienteMutuo;
                        this.addRowTbl({
                            ...this.garantias[this.cnt],
                            nFila: this.cnt,
                            eliminacion: eliminacionGarantia
                        });
                    });

                    tblGarantias.columns.adjust().draw();
                } else {

                }
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('mdlGarantia').then(() => {
            this.id = id;
            let incluidos = [],
                opciones = [`<option selected disabled value="">seleccione</option>`];
            for (let key in this.garantias) {
                incluidos.push(this.garantias[key].idTipoGarantia);
            }
            for (let key in this.catalogo) {
                if (!incluidos.includes(this.catalogo[key].id)) {
                    opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].tipoGarantia}</option>`);
                }
            }
            this.cboTipoGarantia.innerHTML = opciones.join("");
            $("#" + this.cboTipoGarantia.id).selectpicker("refresh");
            $("#mdlGarantia").modal("show");
        }).catch(generalMostrarError);
    },
    save: function () {
        formActions.validate('frmGarantia').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.garantias[this.id].id : 0,
                    idTipoGarantia: this.cboTipoGarantia.value,
                    tipoGarantia: this.cboTipoGarantia.options[this.cboTipoGarantia.options.selectedIndex].text,
                    configurada: "NO",
                    configuracion: this.id > 0 ? this.garantias[this.id].configuracion : {},
                    dependienteMutuo: this.id > 0 ? (this.garantias[this.id].dependienteMutuo ? true : false) : false,
                    eliminacion: this.id > 0 ? (this.garantias[this.id].dependienteMutuo ? false : true) : true
                }
                let guardando = new Promise((resolve, reject) => {
                    try {
                        if (this.id > 0) {
                            $("#trGarantia" + this.id).find("td:eq(1)").html(tmp.tipoGarantia);
                            resolve();
                        } else {
                            this.cnt++;
                            this.id = this.cnt;
                            this.addRowTbl({
                                ...tmp,
                                nFila: this.cnt
                            }).then(resolve).catch(reject);
                        }
                    } catch (err) {
                        reject(err.message);
                    }
                });

                guardando.then(() => {
                    this.garantias[this.id] = {
                        ...tmp
                    };
                    tblGarantias.columns.adjust().draw();
                    $("#mdlGarantia").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        tipoGarantia,
        configurada,
        eliminacion,
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Configurar' onclick='configGarantia.config(" + nFila + ");'>" +
                    "<i class='fas fa-cogs'></i>" +
                    "</span>";
                if (eliminacion) {
                    botones += "<span class='btnTbl' title='Eliminar' onclick='configGarantia.delete(" + nFila + ");'>" +
                        "<i class='fas fa-trash'></i>" +
                        "</span>" +
                        "</div>";
                }
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblGarantias.row.add([
                    nFila,
                    tipoGarantia,
                    '',
                    configurada,
                    botones
                ]).node().id = "trGarantia" + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id) {
        let labelTipoGarantia = this.garantias[id].tipoGarantia,
            tmpTipo = this.garantias[id].idTipoGarantia;;
        Swal.fire({
            title: 'Eliminar garantía',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la garantia de <strong>' + labelTipoGarantia + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblGarantias.row('#trGarantia' + id).remove().draw();
                delete this.garantias[id];
                switch (tmpTipo) {
                    case "5":
                        tblFiadores.clear().draw();
                        configFiduciaria.evalFiadoresHipoteca();
                        break;
                    case "6":
                        tblHipotecas.clear().draw();
                        break;
                }
            }
        });
    },
    config: function (id = 0) {
        let idTipoGarantia = this.garantias[id].idTipoGarantia;
        this.id = id;

        switch (idTipoGarantia) {
            case "1":
                configSinFiador.edit();
                break;
            case "2":
                configAportaciones.edit();
                break;
            case "3":
                configCDP.edit();
                break;
            case "4":
                configOID.edit();
                break;
            case "5":
                configFiduciaria.showScreen();
                break;
            case "6":
                configHipotecaria.showScreen();
                break;
            case "7":
                configFSG.edit();
                break;
            case "8":
                configProgara.edit();
                break;
            default:
                Swal.fire({
                    title: "Error",
                    html: "Ups! Parece que no se pudo identificar el tipo de garantía",
                    icon: "warning"
                });
                break;
        }
    }
}

const configSinFiador = {
    cnt: 0,
    id: 0,
    init: function () {},
    edit: function (id = 0) {
        this.id = id;
        Swal.fire({
            title: "",
            html: "Este tipo de garantía no requiere configuración",
            icon: "info"
        });
    }
}

const configFiduciaria = {
    cnt: 0,
    id: 0,
    datosOriginales: {},
    campoDescripcion: document.getElementById("txtDescripcionFiador"),
    showScreen: function () {
        sgaShowScreen('principalContainer', 'fiadoresContainer').then(() => {
            tblFiadores.columns.adjust().draw();
        });
    },
    init: function () {},
    edit: function (id = 0) {
        formActions.clean('frmFiador').then(() => {
            this.id = id;
            let allSelectpickerCbos = document.querySelectorAll("#frmFiador select.selectpicker");
            if (this.id > 0) {
                let tmp = configGarantia.garantias[configGarantia.id].configuracion[this.id];

                this.campoDescripcion.value = tmp.descripcion;
            }

            allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));

            $("#mdlFiador").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmFiador').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? configGarantia.garantias[configGarantia.id].configuracion[this.id].id : 0,
                    descripcion: this.campoDescripcion.value
                };

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trFiador" + this.id).find("td:eq(1)").html(tmp.descripcion);
                        resolve();
                    }
                });

                guardar.then(() => {
                    configGarantia.garantias[configGarantia.id].configuracion[this.id] = {
                        ...tmp
                    };
                    tblFiadores.columns.adjust().draw();
                    tblFiadores.responsive.recalc();
                    $("#mdlFiador").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        descripcion
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configFiduciaria.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configFiduciaria.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblFiadores.row.add([
                    nFila,
                    descripcion,
                    botones
                ]).node().id = 'trFiador' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: 'Eliminar fiador',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la descripción del fiador?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblFiadores.row('#trFiador' + id).remove().draw();
                delete configGarantia.garantias[configGarantia.id].configuracion[id];
            }
        });
    },
}

const configHipotecaria = {
    id: 0,
    cnt: 0,
    datosOriginales: {},
    tabHipotecas: document.getElementById("hipotecas-tab"),
    tabConfiguracion: document.getElementById("configuracionHipoteca-tab"),
    campoDescripcion: document.getElementById('txtDescripcionHipoteca'),
    zonas: {},
    showScreen: function () {
        sgaShowScreen('principalContainer', 'hipotecasContainer').then(() => {
            tblHipotecas.columns.adjust().draw();
        });
    },
    init: function () {},
    edit: function (id = 0) {
        formActions.clean('frmHipoteca').then(() => {
            this.id = id;
            if (this.id > 0) {
                let tmp = configGarantia.garantias[configGarantia.id].configuracion[this.id];

                this.campoDescripcion.value = tmp.descripcion;
            }

            $('#mdlHipoteca').modal('show');
        });
    },
    save: function () {
        formActions.validate('frmHipoteca').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? configGarantia.garantias[configGarantia.id].configuracion[this.id].id : 0,
                    descripcion: this.campoDescripcion.value
                }

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trHipoteca" + this.id).find("td:eq(1)").html(tmp.descripcion);
                        resolve();
                    }
                });

                guardar.then(() => {
                    configGarantia.garantias[configGarantia.id].configuracion[this.id] = {
                        ...tmp
                    };
                    tblHipotecas.columns.adjust().draw();
                    $('#mdlHipoteca').modal('hide');
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        descripcion
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configHipotecaria.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configHipotecaria.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblHipotecas.row.add([
                    nFila,
                    descripcion,
                    botones
                ]).node().id = 'trHipoteca' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: 'Eliminar hipoteca',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la hipoteca?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblHipotecas.row('#trHipoteca' + id).remove().draw();
                delete configGarantia.garantias[configGarantia.id].configuracion[id];
            }
        });
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar hipoteca',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Salir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                configHipotecaria.showPrincipal();
            }
        });
    },
    showPrincipal: function () {
        configHipotecaria.tabHipotecas.classList.remove("disabled");
        $("#" + configHipotecaria.tabHipotecas.id).trigger("click");
        configHipotecaria.tabConfiguracion.classList.add("disabled");
    },
}

const configAportaciones = {
    cnt: 0,
    id: 0,
    datosOriginales: {},
    campoDescripcion: document.getElementById("txtDescripcionAportaciones"),
    init: function (tmpDatos) {

    },
    edit: function (id = 0) {
        formActions.clean('frmAportaciones').then(() => {
            this.id = id;
            if (Object.keys(configGarantia.garantias[configGarantia.id].configuracion).length > 0) {
                let tmp = configGarantia.garantias[configGarantia.id].configuracion;
                this.campoDescripcion.value = tmp.descripcion;
            }
            $("#mdlAportaciones").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmAportaciones').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: Object.keys(configGarantia.garantias[configGarantia.id].configuracion).length > 0 ? configGarantia.garantias[configGarantia.id].configuracion.id : 0,
                    descripcion: this.campoDescripcion.value
                }

                configGarantia.garantias[configGarantia.id].configuracion = {
                    ...tmp
                };

                $("#mdlAportaciones").modal("hide");
            }
        }).catch(generalMostrarError);
    }
}

const configCDP = {
    cnt: 0,
    id: 0,
    datosOriginales: {},
    campoDescripcion: document.getElementById('txtDescripcionCDP'),
    init: function () {},
    edit: function (id = 0) {
        formActions.clean('frmCDP').then(() => {
            this.id = id;
            if (Object.keys(configGarantia.garantias[configGarantia.id].configuracion).length > 0) {
                let tmp = configGarantia.garantias[configGarantia.id].configuracion;
                this.campoDescripcion.value = tmp.descripcion;
            }
            $("#mdlCDP").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmCDP').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: Object.keys(configGarantia.garantias[configGarantia.id].configuracion).length > 0 ? configGarantia.garantias[configGarantia.id].configuracion.id : 0,
                    descripcion: this.campoDescripcion.value
                }

                configGarantia.garantias[configGarantia.id].configuracion = {
                    ...tmp
                };

                $("#mdlCDP").modal("hide");
            }
        }).catch(generalMostrarError);
    }
}

const configOID = {
    cnt: 0,
    id: 0,
    init: function () {},
    edit: function (id = 0) {
        this.id = id;
        Swal.fire({
            title: "",
            html: "Este tipo de garantía no requiere configuración",
            icon: "info"
        });
    }
}

const configFSG = {
    cnt: 0,
    id: 0,
    init: function () {},
    edit: function (id = 0) {
        this.id = id;
        Swal.fire({
            title: "",
            html: "Este tipo de garantía no requiere configuración",
            icon: "info"
        });
    }
}

const configProgara = {
    cnt: 0,
    id: 0,
    init: function () {},
    edit: function (id = 0) {
        this.id = id;
        Swal.fire({
            title: "",
            html: "Este tipo de garantía no requiere configuración",
            icon: "info"
        });
    }
}

const configDesembolsos = {
    id: 0,
    cnt: 0,
    desembolsos: {},
    campoFecha: document.getElementById("txtFechaDesembolso"),
    campoMonto: document.getElementById("numMontoDesembolso"),
    init: function (tmpDatos) {
        tmpDatos.forEach(parcial => {
            this.cnt++;
            this.desembolsos[this.cnt] = {
                id: parcial.id,
                fecha: parcial.fecha,
                monto: parcial.monto
            };

            this.addRowTbl({
                ...this.desembolsos[this.cnt],
                nFila: this.cnt
            });
        });
        tblDesembolsos.columns.adjust().draw();
    },
    edit: function (id = 0) {
        formActions.clean('frmParcial').then(() => {
            this.id = id;

            if (this.id > 0) {
                let tmp = this.desembolsos[this.id];
                this.campoFecha.value = tmp.fecha;
                $("#" + this.campoFecha.id).datepicker("update", tmp.fecha);
                this.campoMonto.value = tmp.monto;
            }

            $("#mdlDesembolsoParcial").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmParcial').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.desembolsos[this.id].id : 0,
                    fecha: this.campoFecha.value,
                    monto: this.campoMonto.value
                }

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trDesembolso" + this.id).find("td:eq(1)").html(tmp.fecha);
                        $("#trDesembolso" + this.id).find("td:eq(2)").html("$" + new Intl.NumberFormat().format(parseFloat(tmp.monto).toFixed(2)));
                        resolve();
                    }
                });

                guardar.then(() => {
                    this.desembolsos[this.id] = {
                        ...tmp
                    };
                    tblDesembolsos.columns.adjust().draw();
                    $("#mdlDesembolsoParcial").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        fecha,
        monto
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configDesembolsos.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configDesembolsos.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblDesembolsos.row.add([
                    nFila,
                    fecha,
                    "$" + new Intl.NumberFormat().format(parseFloat(monto).toFixed(2)),
                    botones
                ]).node().id = 'trDesembolso' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: 'Eliminar desembolso',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el desembolso?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblDesembolsos.row('#trDesembolso' + id).remove().draw();
                delete this.desembolsos[id];
            }
        });
    }
}

function sgaShowScreen(parentContainerId, containerId) {
    return new Promise((resolve, reject) => {
        let containers = document.querySelectorAll(`#${parentContainerId} .sgaContainer`);

        containers.forEach(container => {
            if (container.id && container.id != containerId) {
                container.style.display = 'none';
            } else {
                if ($("#" + containerId).is(":visible") == false) {
                    $("#" + containerId).fadeIn('fast', function () {
                        tblGarantias.columns.adjust().draw();
                        resolve();
                    });
                }
            }
        });
    });
}