let tblMutuos,
    tblPagares;

window.onload = () => {
    initTables();
    fetchActions.getCats({
        archivo: 'sgaGetCats',
        modulo: 'SGA-MUTUOS',
        solicitados: ['agencias']
    }).then(({
        agencias
    }) => {
        document.getElementById('frmReporte').reset();
        let cbosAgencia = document.querySelectorAll('select.cboAgencia'),
            opciones = [`<option selected value='0'>Todas las agencias</option>`];

        agencias.forEach(agencia => {
            agencia.id > 700 && (opciones.push(`<option value='${agencia.id}'>${agencia.nombreAgencia}</option>`));
        });

        cbosAgencia.forEach(cbo => {
            cbo.innerHTML = opciones.join('');
            cbo.className.includes('selectpicker') && ($('#' + cbo.id).selectpicker('refresh'));
        });
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblMutuos").length) {
                tblMutuos = $("#tblMutuos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [2, 3, 7, 8, 9, 10],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de aditivo';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblMutuos.columns.adjust().draw();
            }

            if ($("#tblPagares").length) {
                tblPagares = $("#tblPagares").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de garantía';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblPagares.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configBusqueda = {
    cboAgencia: document.getElementById('cboAgencia'),
    campoInicio: document.getElementById('txtInicio'),
    campoFin: document.getElementById('txtFin'),
    btnExcel: document.getElementById('btnExcel'),
    datosMutuos: {},
    datosPagares: {},
    cntMutuos: 0,
    cntPagares: 0,
    search: function () {
        formActions.validate('frmReporte').then(({
            errores
        }) => {
            if (errores == 0) {
                this.btnExcel.disabled = true;
                tblMutuos.clear().draw();
                tblPagares.clear().draw();
                this.datosMutuos = {};
                this.datosPagares = {};
                this.cntMutuos = 0;
                this.cntPagares = 0;
                fetchActions.get({
                    archivo: 'sgaSearchReporteDetallado',
                    modulo: 'SGA-MUTUOS',
                    params: {
                        agencia: encodeURIComponent(this.cboAgencia.value),
                        inicio: encodeURIComponent(this.campoInicio.value),
                        fin: encodeURIComponent(this.campoFin.value)
                    }
                }).then((datosRespuesta) => {
                    console.log(datosRespuesta);
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case 'EXITO':
                                if (datosRespuesta.datosMutuos.length > 0 || datosRespuesta.datosMutuos > 0) {
                                    this.btnExcel.disabled = false;
                                    this.datosMutuos = datosRespuesta.datosMutuos;
                                    this.datosPagares = datosRespuesta.datosPagares;
                                    datosRespuesta.datosMutuos.forEach(registro => {
                                        configBusqueda.cntMutuos++;
                                        this.addRowTbl({
                                            ...registro,
                                            nFila: configBusqueda.cntMutuos
                                        });
                                    });
                                    tblMutuos.columns.adjust().draw();

                                    datosRespuesta.datosPagares.forEach(registro => {
                                        configBusqueda.cntPagares++;
                                        this.addRowTblPagares({
                                            ...registro,
                                            nFila: configBusqueda.cntPagares
                                        });
                                    });
                                    tblPagares.columns.adjust().draw();
                                } else {
                                    toastr.warning('No hay registros');
                                }
                                break;
                            case 'SinRegistros':
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        agencia,
        codigoCliente,
        nombreDeudor,
        monto,
        numeroCredito,
        fechaDocumento,
        garantias,
        matriculas,
        presentadaPor,
        nombreUsuario,
        lineaRotativa,
        fechaVencimiento
    }) {
        return new Promise((resolve, reject) => {
            try {
                let impGarantias = garantias.split('@@@'),
                    impMatriculas = matriculas != null ? matriculas.split('@@@') : [],
                    impPresenta = presentadaPor != null ? presentadaPor.split('@@@') : [],
                    impRotativa = lineaRotativa == 'S' ? 'SI' : 'NO';
                tblMutuos.row.add([
                    nFila,
                    agencia,
                    codigoCliente,
                    nombreDeudor,
                    monto,
                    numeroCredito,
                    fechaDocumento,
                    impGarantias.join('\n'),
                    impMatriculas.join('\n'),
                    impPresenta,
                    nombreUsuario,
                    impRotativa,
                    fechaVencimiento
                ]);
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    addRowTblPagares: function ({
        nFila,
        agencia,
        codigoCliente,
        tipoDocumento,
        numeroDocumento,
        nit,
        nombreDeudor,
        pais,
        departamento,
        municipio,
        direccion,
        edad,
        numeroCredito,
        monto,
        tasaInteres,
        fechaVencimiento,
        fechaGeneracion,
        nombreUsuario,
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblPagares.row.add([
                    nFila,
                    agencia,
                    codigoCliente,
                    tipoDocumento,
                    numeroDocumento,
                    nit,
                    nombreDeudor,
                    pais,
                    departamento,
                    municipio,
                    direccion,
                    edad,
                    numeroCredito,
                    monto,
                    tasaInteres,
                    fechaVencimiento,
                    fechaGeneracion,
                    nombreUsuario,
                    ''
                ]);
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    generarExcel: function () {
        if (Object.keys(this.datosMutuos).length > 0 || Object.keys(this.datosPagares).length > 0) {
            fetchActions.set({
                archivo: 'sgaConstruirExcelReporteDetallado',
                modulo: 'SGA-MUTUOS',
                datos: {
                    datosMutuos: this.datosMutuos,
                    datosPagares: this.datosPagares,
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta) {
                        case 'EXITO':
                            window.open('./main/SGA-MUTUOS/docs/Reportes/' + datosRespuesta.nombreArchivo, '_blank');
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            Swal.fire({
                title: '¡Atención!',
                html: 'Parece que no hay datos para exportar a excel.',
                icon: 'warning'
            });
        }
    }
}