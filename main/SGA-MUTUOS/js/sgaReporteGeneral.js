let tblMutuos,
    tblPagares;

window.onload = () => {
    $('.preloader').fadeOut('fast');
    document.getElementById('frmReporte').reset();
    initTblMutuos();
    initTblPagares();
}

function initTblMutuos() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblMutuos").length) {
                tblMutuos = $("#tblMutuos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollX: true,
                    order: [0, "asc"],
                    ordering: true,
                    paging: false,
                    columnDefs: [{
                        orderable: false,
                        targets: "no-sort"
                    }],
                    searching: false,
                    fixedColumns: {
                        leftColumns: 1,
                        rightColumns: 2
                    }
                });

                tblMutuos.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

function initTblPagares() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblPagares").length) {
                tblPagares = $("#tblPagares").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollX: true,
                    order: [0, "asc"],
                    ordering: true,
                    paging: false,
                    columnDefs: [{
                        orderable: false,
                        targets: "no-sort"
                    }],
                    searching: false,
                    fixedColumns: {
                        leftColumns: 1,
                        rightColumns: 2
                    }
                });

                tblPagares.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configBusqueda = {
    campoTipoBusqueda: document.getElementById('cboTipoBusqueda'),
    campoInicio: document.getElementById('txtInicio'),
    campoFin: document.getElementById('txtFin'),
    contenedorMutuos: document.getElementById('divContenedorMutuos'),
    contenedorPagares: document.getElementById('divContenedorPagares'),
    btnExcel: document.getElementById('btnExcel'),
    datosMutuos: {},
    datosPagares: {},
    search: function () {
        formActions.validate('frmReporte').then(({
            errores
        }) => {
            if (errores == 0) {
                // this.contenedorMutuos.innerHTML = '';
                // this.contenedorPagares.innerHTML = '';
                this.btnExcel.disabled = true;
                tblMutuos.clear().draw();
                tblPagares.clear().draw();
                this.footerData({});
                this.footerDataPagares({});
                this.datosMutuos = {};
                this.datosPagares = {};
                fetchActions.get({
                    archivo: 'sgaSearchReporteGeneral',
                    modulo: 'SGA-MUTUOS',
                    params: {
                        tipoBusqueda: encodeURIComponent(this.campoTipoBusqueda.value),
                        inicio: encodeURIComponent(this.campoInicio.value),
                        fin: encodeURIComponent(this.campoFin.value)
                    }
                }).then((datosRespuesta) => {
                    // console.log(datosRespuesta);
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case 'EXITO':
                                if (Object.keys(datosRespuesta.datosMutuos.fechas).length > 0) {
                                    for (let key in datosRespuesta.datosMutuos.fechas) {
                                        this.addRowTbl({
                                            fecha: key,
                                            datos: datosRespuesta.datosMutuos.fechas[key]
                                        });
                                    }
                                    this.footerData(datosRespuesta.datosMutuos.totales).then(() => {
                                        tblMutuos.columns.adjust().draw();
                                    }).catch(generalMostrarError);
                                }

                                if (Object.keys(datosRespuesta.datosPagares.fechas).length > 0) {
                                    for (let key in datosRespuesta.datosPagares.fechas) {
                                        this.addRowTblPagares({
                                            fecha: key,
                                            datos: datosRespuesta.datosPagares.fechas[key]
                                        });
                                    }
                                    this.footerDataPagares(datosRespuesta.datosPagares.totales).then(() => {
                                        tblPagares.columns.adjust().draw();
                                    }).catch(generalMostrarError);
                                }

                                if (Object.keys(datosRespuesta.datosPagares.fechas).length > 0 || Object.keys(datosRespuesta.datosMutuos.fechas).length > 0) {
                                    this.datosMutuos = datosRespuesta.datosMutuos;
                                    this.datosPagares = datosRespuesta.datosPagares;
                                    this.btnExcel.disabled = false;
                                }
                                // this.contenedorMutuos.innerHTML = datosRespuesta.tablaMutuos;
                                // this.tblHtmlMutuos = datosRespuesta.tablaMutuos;
                                // this.contenedorPagares.innerHTML = datosRespuesta.tablaPagares;
                                // this.tblHtmlPagares = datosRespuesta.tablaPagares;
                                // initTblMutuos();
                                // initTblPagares();
                                // document.getElementById("spnExcel").innerHTML = "(Desde " + fechaInicio + " hasta " + fechaFin + ")";
                                break;
                            case 'SinRegistros':
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        fecha,
        datos: {
            A701,
            A702,
            A703,
            A704,
            A705,
            A706,
            A707,
            A708,
            A709,
            A710,
            totales,
        }
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblMutuos.row.add([
                    fecha,
                    A701.cantidad,
                    A701.monto,
                    A702.cantidad,
                    A702.monto,
                    A703.cantidad,
                    A703.monto,
                    A704.cantidad,
                    A704.monto,
                    A705.cantidad,
                    A705.monto,
                    A706.cantidad,
                    A706.monto,
                    A707.cantidad,
                    A707.monto,
                    A708.cantidad,
                    A708.monto,
                    A709.cantidad,
                    A709.monto,
                    A710.cantidad,
                    A710.monto,
                    totales.cantidad,
                    totales.monto,
                ]);
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    footerData: function ({
        A701 = {
            monto: '$0.00',
            cantidad: 0
        },
        A702 = {
            monto: '$0.00',
            cantidad: 0
        },
        A703 = {
            monto: '$0.00',
            cantidad: 0
        },
        A704 = {
            monto: '$0.00',
            cantidad: 0
        },
        A705 = {
            monto: '$0.00',
            cantidad: 0
        },
        A706 = {
            monto: '$0.00',
            cantidad: 0
        },
        A707 = {
            monto: '$0.00',
            cantidad: 0
        },
        A708 = {
            monto: '$0.00',
            cantidad: 0
        },
        A709 = {
            monto: '$0.00',
            cantidad: 0
        },
        A710 = {
            monto: '$0.00',
            cantidad: 0
        },
        totales = {
            monto: '$0.00',
            cantidad: 0
        }
    }) {
        return new Promise((resolve, reject) => {
            try {
                $(tblMutuos.column(1).footer()).html(A701.cantidad);
                $(tblMutuos.column(2).footer()).html(A701.monto);
                $(tblMutuos.column(3).footer()).html(A702.cantidad);
                $(tblMutuos.column(4).footer()).html(A702.monto);
                $(tblMutuos.column(5).footer()).html(A703.cantidad);
                $(tblMutuos.column(6).footer()).html(A703.monto);
                $(tblMutuos.column(7).footer()).html(A704.cantidad);
                $(tblMutuos.column(8).footer()).html(A704.monto);
                $(tblMutuos.column(9).footer()).html(A705.cantidad);
                $(tblMutuos.column(10).footer()).html(A705.monto);
                $(tblMutuos.column(11).footer()).html(A706.cantidad);
                $(tblMutuos.column(12).footer()).html(A706.monto);
                $(tblMutuos.column(13).footer()).html(A707.cantidad);
                $(tblMutuos.column(14).footer()).html(A707.monto);
                $(tblMutuos.column(15).footer()).html(A708.cantidad);
                $(tblMutuos.column(16).footer()).html(A708.monto);
                $(tblMutuos.column(17).footer()).html(A709.cantidad);
                $(tblMutuos.column(18).footer()).html(A709.monto);
                $(tblMutuos.column(19).footer()).html(A710.cantidad);
                $(tblMutuos.column(20).footer()).html(A710.monto);
                $(tblMutuos.column(21).footer()).html(totales.cantidad);
                $(tblMutuos.column(22).footer()).html(totales.monto);
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    addRowTblPagares: function ({
        fecha,
        datos: {
            A701,
            A702,
            A703,
            A704,
            A705,
            A706,
            A707,
            A708,
            A709,
            A710,
            totales,
        }
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblPagares.row.add([
                    fecha,
                    A701.cantidad,
                    A701.monto,
                    A702.cantidad,
                    A702.monto,
                    A703.cantidad,
                    A703.monto,
                    A704.cantidad,
                    A704.monto,
                    A705.cantidad,
                    A705.monto,
                    A706.cantidad,
                    A706.monto,
                    A707.cantidad,
                    A707.monto,
                    A708.cantidad,
                    A708.monto,
                    A709.cantidad,
                    A709.monto,
                    A710.cantidad,
                    A710.monto,
                    totales.cantidad,
                    totales.monto,
                ]);
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    footerDataPagares: function ({
        A701 = {
            monto: '$0.00',
            cantidad: 0
        },
        A702 = {
            monto: '$0.00',
            cantidad: 0
        },
        A703 = {
            monto: '$0.00',
            cantidad: 0
        },
        A704 = {
            monto: '$0.00',
            cantidad: 0
        },
        A705 = {
            monto: '$0.00',
            cantidad: 0
        },
        A706 = {
            monto: '$0.00',
            cantidad: 0
        },
        A707 = {
            monto: '$0.00',
            cantidad: 0
        },
        A708 = {
            monto: '$0.00',
            cantidad: 0
        },
        A709 = {
            monto: '$0.00',
            cantidad: 0
        },
        A710 = {
            monto: '$0.00',
            cantidad: 0
        },
        totales = {
            monto: '$0.00',
            cantidad: 0
        }
    }) {
        return new Promise((resolve, reject) => {
            try {
                $(tblPagares.column(1).footer()).html(A701.cantidad);
                $(tblPagares.column(2).footer()).html(A701.monto);
                $(tblPagares.column(3).footer()).html(A702.cantidad);
                $(tblPagares.column(4).footer()).html(A702.monto);
                $(tblPagares.column(5).footer()).html(A703.cantidad);
                $(tblPagares.column(6).footer()).html(A703.monto);
                $(tblPagares.column(7).footer()).html(A704.cantidad);
                $(tblPagares.column(8).footer()).html(A704.monto);
                $(tblPagares.column(9).footer()).html(A705.cantidad);
                $(tblPagares.column(10).footer()).html(A705.monto);
                $(tblPagares.column(11).footer()).html(A706.cantidad);
                $(tblPagares.column(12).footer()).html(A706.monto);
                $(tblPagares.column(13).footer()).html(A707.cantidad);
                $(tblPagares.column(14).footer()).html(A707.monto);
                $(tblPagares.column(15).footer()).html(A708.cantidad);
                $(tblPagares.column(16).footer()).html(A708.monto);
                $(tblPagares.column(17).footer()).html(A709.cantidad);
                $(tblPagares.column(18).footer()).html(A709.monto);
                $(tblPagares.column(19).footer()).html(A710.cantidad);
                $(tblPagares.column(20).footer()).html(A710.monto);
                $(tblPagares.column(21).footer()).html(totales.cantidad);
                $(tblPagares.column(22).footer()).html(totales.monto);
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    generarExcel: function () {
        if (Object.keys(this.datosMutuos).length > 0 || Object.keys(this.datosPagares).length > 0) {
            fetchActions.set({
                archivo: 'sgaConstruirExcelReporteGeneral',
                modulo: 'SGA-MUTUOS',
                datos: {
                    datosMutuos: this.datosMutuos,
                    datosPagares: this.datosPagares,
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta) {
                        case 'EXITO':
                            window.open('./main/SGA-MUTUOS/docs/Reportes/' + datosRespuesta.nombreArchivo, '_blank');
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            Swal.fire({
                title: '¡Atención!',
                html: 'Parece que no hay datos para exportar a excel.',
                icon: 'warning'
            });
        }
    }
}