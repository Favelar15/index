let tblFiadores;

window.onload = () => {
    document.getElementById('frmDeudor').reset();
    document.getElementById('frmCredito').reset();
    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "SGA-MUTUOS",
                archivo: "sgaGetCats",
                solicitados: ['agencias', 'tiposDocumento', 'geograficos']
            }).then((datosRespuesta) => {
                $(".preloader").fadeIn("fast");
                configDocumento.initCats(datosRespuesta).then(resolve).catch(reject);
            }).catch(reject);
        });
    });

    gettingCats.then(() => {
        let idEdicion = getParameterByName('id');
        let allSelectpickerCbos = document.querySelectorAll('#credito select');
        allSelectpickerCbos.forEach(cbo => {
            cbo.disabled = true;
            cbo.className.includes('selectpicker') && ($("#" + cbo.id).selectpicker("refresh"));
        });
        configDatosCredito.campoFechaVencimiento.disabled = true;
        configDatosCredito.campoFechaGeneracion.disabled = true;
        configDocumento.btnFiadores.disabled = true;
        configDocumento.buttonGeneracion.disabled = true;
        if (idEdicion.length > 0) {
            idEdicion = atob(decodeURIComponent(idEdicion));
            fetchActions.get({
                modulo: "SGA-MUTUOS",
                archivo: 'sgaGetPagare',
                params: {
                    id: idEdicion
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    console.log(datosRespuesta);
                    switch (datosRespuesta.respuesta.trim()) {
                        case "EXITO":
                            configDocumento.id = idEdicion;
                            let dependienteMutuo = false;
                            (datosRespuesta.datosCredito.idMutuo != null && datosRespuesta.datosCredito.idMutuo.length > 0) && (dependienteMutuo = true);
                            (datosRespuesta.datosCredito.idMutuo != null && datosRespuesta.datosCredito.idMutuo.length > 0) && (configDocumento.idMutuo = datosRespuesta.datosCredito.idMutuo);
                            $(".preloader").fadeIn('fast');
                            configDatosCredito.init({
                                ...datosRespuesta.datosCredito,
                                dependienteMutuo
                            }).then(() => {
                                configDatosCredito.datosOriginales = JSON.parse(JSON.stringify(configDatosCredito.getJSON()));
                                configDatosDeudor.init({
                                    ...datosRespuesta.datosDeudor,
                                    dependienteMutuo
                                }).then(() => {
                                    configDatosDeudor.datosOriginales = JSON.parse(JSON.stringify(configDatosDeudor.getJSON()));
                                    if (datosRespuesta.datosFiadores.length > 0) {
                                        datosRespuesta.datosFiadores.forEach(fiador => {
                                            let labelTipoDocumento = tiposDocumento[fiador.tipoDocumento].tipoDocumento,
                                                labelPais = datosGeograficos[fiador.pais].nombrePais,
                                                labelDepartamento = '',
                                                labelMunicipio = '',
                                                direccion = '';

                                            datosGeograficos[fiador.pais].departamentos.forEach(departamento => {
                                                if (departamento.id == fiador.departamento) {
                                                    labelDepartamento = departamento.nombreDepartamento;
                                                    departamento.municipios.forEach(municipio => {
                                                        if (municipio.id == fiador.municipio) {
                                                            labelMunicipio = municipio.nombreMunicipio;
                                                        }
                                                    });
                                                }
                                            });
                                            configFiduciaria.cnt++;
                                            fiador.direccion && (direccion = fiador.direccion);
                                            configFiduciaria.fiadores[configFiduciaria.cnt] = {
                                                ...fiador,
                                                labelPais,
                                                labelDepartamento,
                                                labelMunicipio,
                                                labelTipoDocumento,
                                                direccion,
                                                dependienteMutuo
                                            };
                                            configFiduciaria.addRowTbl({
                                                ...configFiduciaria.fiadores[configFiduciaria.cnt],
                                                nFila: configFiduciaria.cnt
                                            });
                                        });
                                        tblFiadores.columns.adjust().draw();
                                        configFiduciaria.datosOriginales = JSON.parse(JSON.stringify(configFiduciaria.fiadores));
                                    }
                                    $(".preloader").fadeOut("fast");
                                    configDatosCredito.campoNumeroCredito.readOnly = true;
                                    configDatosCredito.campoNumeroCredito.parentNode.querySelector('.input-group-append').remove();
                                    configDatosCredito.campoFechaVencimiento.disabled = false;
                                    configDatosCredito.campoFechaGeneracion.disabled = false;
                                    configDatosDeudor.campoDireccion.readOnly = false;
                                    configDocumento.buttonGeneracion.disabled = false;
                                    if (!dependienteMutuo) {
                                        configDatosCredito.habilitarEdicion().then(() => {
                                            configDatosCredito.campoNumeroCredito.readOnly = true;
                                            configDatosDeudor.habilitarEdicion().then(() => {
                                                configDatosDeudor.campoNumeroDocumento.value = datosRespuesta.datosDeudor.numeroDocumento;
                                                this.btnFiadores.disabled = false;
                                            }).catch(generalMostrarError);
                                        }).catch(generalMostrarError);
                                    }
                                });
                            });
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            $(".preloader").fadeOut("fast");
        }
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblFiadores").length) {
                tblFiadores = $("#tblFiadores").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblFiadores.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configDocumento = {
    id: 0,
    idMutuo: null,
    intentos: 50,
    datosGuardar: {},
    tabDeudor: document.getElementById('deudor-tab'),
    tabCredito: document.getElementById('credito-tab'),
    tabFiadores: document.getElementById('fiadores-tab'),
    formDeudor: document.getElementById('frmDeudor'),
    formCredito: document.getElementById("frmCredito"),
    buttonGeneracion: document.getElementById('btnGenerarDocumentos'),
    btnFiadores: document.getElementById('btnFiadores'),
    datosGuardar: {},
    initCats: function ({
        tiposDocumento: tiposDoc,
        agencias,
        datosGeograficos: geograficos,
    }) {
        return new Promise((resolve, reject) => {
            try {
                let cbosTipoDocumento = document.querySelectorAll("select.cboTipoDocumento"),
                    cbosPais = document.querySelectorAll("select.cboPais"),
                    cbosAgencia = document.querySelectorAll("select.cboAgencia"),
                    allSelectpickerCbos = document.querySelectorAll("select.selectpicker");
                datosGeograficos = {
                    ...geograficos
                };

                const opciones = [`<option selected value="" disabled>seleccione</option>`];

                let tmpOpciones = [...opciones];
                tiposDoc.forEach(tipo => {
                    tiposDocumento[tipo.id] = {
                        ...tipo
                    };
                    tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
                });

                cbosTipoDocumento.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                for (let key in geograficos) {
                    tmpOpciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
                }
                cbosPais.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                agencias.forEach(agencia => tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`));
                cbosAgencia.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));

                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    resultados: 0,
    search: function () {
        formActions.validate('frmCredito').then(({
            errores
        }) => {
            if (errores == 0) {
                fetchActions.get({
                    modulo: "SGA-MUTUOS",
                    archivo: "sgaSearchMutuo",
                    params: {
                        numeroCredito: encodeURIComponent(configDatosCredito.campoNumeroCredito.value),
                        pagare: 'S'
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                switch (datosRespuesta.datosMutuo.respuesta.trim()) {
                                    case "EXITO":
                                        // console.log(datosRespuesta);
                                        $(".preloader").fadeIn("fast");
                                        configDatosCredito.init({
                                            ...datosRespuesta.datosMutuo.datosCredito,
                                            dependienteMutuo: true
                                        }).then(() => {
                                            configDocumento.idMutuo = datosRespuesta.datosMutuo.datosCredito.id;
                                            configDatosDeudor.init({
                                                ...datosRespuesta.datosMutuo.datosDeudor,
                                                dependienteMutuo: true
                                            }).then(() => {
                                                let validarFiadores = new Promise((resolve, reject) => {
                                                    try {
                                                        datosRespuesta.datosMutuo.datosGarantias.forEach(garantia => {
                                                            if (garantia.idTipoGarantia == '5') {
                                                                garantia.configuracion.forEach(fiador => {
                                                                    let labelTipoDocumento = tiposDocumento[fiador.tipoDocumento].tipoDocumento,
                                                                        labelPais = datosGeograficos[fiador.pais].nombrePais,
                                                                        labelDepartamento = '',
                                                                        labelMunicipio = '',
                                                                        direccion = '';

                                                                    datosGeograficos[fiador.pais].departamentos.forEach(departamento => {
                                                                        if (departamento.id == fiador.departamento) {
                                                                            labelDepartamento = departamento.nombreDepartamento;
                                                                            departamento.municipios.forEach(municipio => {
                                                                                if (municipio.id == fiador.municipio) {
                                                                                    labelMunicipio = municipio.nombreMunicipio;
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                    configFiduciaria.cnt++;
                                                                    fiador.direccion && (direccion = fiador.direccion);
                                                                    configFiduciaria.fiadores[configFiduciaria.cnt] = {
                                                                        ...fiador,
                                                                        id: 0,
                                                                        labelPais,
                                                                        labelDepartamento,
                                                                        labelMunicipio,
                                                                        labelTipoDocumento,
                                                                        direccion,
                                                                        dependienteMutuo: true
                                                                    };
                                                                    configFiduciaria.addRowTbl({
                                                                        ...configFiduciaria.fiadores[configFiduciaria.cnt],
                                                                        nFila: configFiduciaria.cnt
                                                                    });
                                                                });
                                                                tblFiadores.columns.adjust().draw();
                                                            }
                                                        });
                                                        resolve();
                                                    } catch (err) {
                                                        reject(err.message);
                                                    }
                                                });

                                                validarFiadores.then(() => {
                                                    $(".preloader").fadeOut("fast");
                                                    configDatosCredito.campoNumeroCredito.readOnly = true;
                                                    configDatosCredito.campoNumeroCredito.parentNode.querySelector('.input-group-append').remove();
                                                    configDatosCredito.campoNumeroCreditoPagare.readOnly = false;
                                                    configDatosCredito.campoFechaVencimiento.disabled = false;
                                                    configDatosCredito.campoFechaGeneracion.disabled = false;
                                                    configDatosDeudor.campoDireccion.readOnly = false;
                                                    configDatosDeudor.campoTelefono.readOnly = false;
                                                    configDocumento.buttonGeneracion.disabled = false;
                                                }).catch(generalMostrarError);
                                            }).catch(generalMostrarError);
                                        }).catch(generalMostrarError);
                                        break;
                                    case "SinRegistros":
                                        configDatosCredito.habilitarEdicion().then(() => {
                                            configDatosCredito.campoNumeroCredito.readOnly = true;
                                            configDatosCredito.campoNumeroCredito.parentNode.querySelector('.input-group-append').remove();
                                            configDatosDeudor.habilitarEdicion().then(() => {
                                                this.btnFiadores.disabled = false;
                                                configDocumento.buttonGeneracion.disabled = false;
                                                configDatosCredito.campoNumeroCreditoPagare.readOnly = false;
                                                configDatosDeudor.campoFechaNacimiento.disabled = false;
                                                configDatosDeudor.campoTelefono.readOnly = false;
                                            }).catch(generalMostrarError);
                                        }).catch(generalMostrarError);
                                        break;
                                    default:
                                        generalMostrarError(datosRespuesta);
                                        break;
                                }
                                break;
                            case "Generado":
                                Swal.fire({
                                    title: "¡Atención!",
                                    html: "Parece que los documentos para este número de crédito ya fueron generados, puedes optar por anularlos o editarlos desde el administrador de documentos.",
                                    icon: "warning"
                                });
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    cancel: function () {
        let tmpModulo = btoa('SGA-MUTUOS');
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Salir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                window.top.location.href = './?page=sgaPagare&mod=' + encodeURIComponent(tmpModulo);
            }
        });
    },
    save: function () {
        if (formActions.manualValidate(this.formDeudor.id)) {
            if (formActions.manualValidate(this.formCredito.id)) {
                let granted = true;

                if (Object.keys(configFiduciaria.fiadores).length > 0) {
                    for (let key in configFiduciaria.fiadores) {
                        if (configFiduciaria.fiadores[key].direccion == null || configFiduciaria.fiadores[key].direccion.length == 0) {
                            granted = false;
                        }
                    }
                }

                if (granted) {
                    document.getElementById("strIntentos").innerHTML = this.intentos;
                    $("#mdlGeneracion").modal("show");
                } else {
                    Swal.fire({
                        title: '¡Atención!',
                        html: "Parece que uno o más fiadores tienen pendiente la configuración de dirección.",
                        icon: 'warning'
                    }).then(() => {
                        $('#' + configDocumento.tabFiadores.id).trigger('click')
                    });
                }
            } else {
                $("#" + this.tabCredito.id).trigger("click");
                toastr.warning("Datos de crédito incompletos");
            }
        } else {
            $("#" + this.tabDeudor.id).trigger("click");
            toastr.warning("Datos de deudor incompletos");
        }
    },
    generarDocumentos: function () {
        let existenCambios = this.evalCambios();
        fetchActions.set({
            modulo: "SGA-MUTUOS",
            archivo: 'sgaGuardarPagare',
            datos: {
                ...this.datosGuardar,
                tipoApartado,
                idApartado
            }
        }).then((datosRespuesta) => {
            console.log(datosRespuesta);
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta.trim()) {
                    case "EXITO":
                        this.id = datosRespuesta.id;
                        this.intentos--;
                        let tmpModulo = btoa('SGA-MUTUOS');
                        configDatosCredito.datosOriginales = JSON.parse(JSON.stringify(configDatosCredito.getJSON()));
                        configDatosDeudor.id = datosRespuesta.idDeudor;
                        configDatosDeudor.datosOriginales = {
                            ...configDatosDeudor.getJSON()
                        };

                        if (this.intentos == 0) {
                            window.top.location.href = './?page=sgaResolucionClausula&mod=' + tmpModulo;
                        }

                        if (Object.keys(configFiduciaria.fiadores).length > 0) {
                            for (let key in datosRespuesta.idsFiadores) {
                                configFiduciaria.fiadores[key].id = datosRespuesta.idsFiadores[key].id;
                            }
                            configFiduciaria.datosOriginales = JSON.parse(JSON.stringify(configFiduciaria.fiadores));
                        } else {
                            configFiduciaria.datosOriginales = {};
                        }

                        document.getElementById("strIntentos").innerHTML = this.intentos;
                        if (datosRespuesta.Errores.length > 0 || Object.keys(datosRespuesta.Errores).length > 0) {
                            let arrErrores = [];
                            if (datosRespuesta.Errores.length > 0) {
                                arrErrores = [...datosRespuesta.Errores];
                            } else {
                                for (let key in datosRespuesta.Errores) {
                                    arrErrores.push(`${key}: ${datosRespuesta.Errores[key]}`);
                                }
                            }
                            Swal.fire({
                                title: "¡Atención!",
                                icon: "warning",
                                html: `Parece que sólo se guardó parte de la información y ocurrió un error con la base de datos, los errores ocurridos son: <br> -${arrErrores.join("<br />-")}`
                            });
                        } else {
                            // Swal.fire({
                            //     title: "¡Bien hecho!",
                            //     icon: "success",
                            //     html: "Parece que todo va bien, algún día el sistema generará el documento."
                            // });
                            window.open('./main/SGA-MUTUOS/docs/Pagare/' + datosRespuesta.nombrePagare);
                        }
                        break;
                    case "Generado":
                        Swal.fire({
                            title: "¡Atención!",
                            html: "Parece que los documentos para este número de crédito ya fueron generados, puedes optar por anularlos o editarlos desde el administrador de documentos.",
                            icon: "warning"
                        });
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    prepareData: function () {
        let datos = {
            id: this.id,
            datosDeudor: {
                ...configDatosDeudor.getJSON()
            },
            datosCredito: {
                ...configDatosCredito.getJSON()
            },
            fiadores: JSON.parse(JSON.stringify(configFiduciaria.fiadores)),
        };

        return {
            ...datos
        };
    },
    evalCambios: function () {
        let data = this.prepareData();
        this.datosGuardar = {
            ...data
        };
        let existenCambios = false;

        if (Object.keys(configDatosCredito.datosOriginales).length == 0) {
            this.datosGuardar.datosCredito.logs = {
                descripcion: "Creación de Pagaré"
            };
            existenCambios = true;
        } else {
            //Validar cambios
            let detalles = {},
                cnt = 0;

            for (let key in this.datosGuardar.datosCredito) {
                if (key != 'aditivos' && key != 'detalleDesembolsos' && key != 'totalCuota' && !key.includes('label')) {
                    let nuevoValor = this.datosGuardar.datosCredito[key],
                        valorAnterior = configDatosCredito.datosOriginales[key];
                    (nuevoValor != null && nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                    (valorAnterior != null && valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                    if (nuevoValor != valorAnterior) {
                        cnt++;
                        detalles[cnt] = {
                            campo: key,
                            valorAnterior,
                            nuevoValor
                        }
                    }
                }
            }

            if (Object.keys(detalles).length > 0) {
                this.datosGuardar.datosCredito.logs = {
                    descripcion: 'Edición de valores de crédito',
                    detalles
                };
                existenCambios = true;
            }
        }

        if (Object.keys(configDatosDeudor.datosOriginales).length == 0) {
            this.datosGuardar.datosDeudor.logs = {
                descripcion: 'Creación de deudor/a'
            }
            existenCambios = true;
        } else {
            //Validar cambios
            let detalles = {},
                cnt = 0;

            for (let key in this.datosGuardar.datosDeudor) {
                if (!key.includes('label')) {
                    let nuevoValor = this.datosGuardar.datosDeudor[key],
                        valorAnterior = configDatosDeudor.datosOriginales[key];
                    (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                    (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                    if (valorAnterior != nuevoValor) {
                        cnt++;
                        detalles[cnt] = {
                            campo: key,
                            valorAnterior,
                            nuevoValor
                        };
                    }
                }
            }

            if (Object.keys(detalles).length > 0) {
                this.datosGuardar.datosDeudor.logs = {
                    descripcion: 'Edición de datos de deudor/a',
                    detalles
                };
                existenCambios = true;
            }
        }

        if (Object.keys(configFiduciaria.datosOriginales).length == 0) {
            for (let key in this.datosGuardar.fiadores) {
                this.datosGuardar.fiadores[key].logs = {
                    accion: "Guardar",
                    descripcion: "Creación de fiador"
                }
            }
            existenCambios = true;
        } else {
            //Validar cambios
            let tmpFiadores = configFiduciaria.datosOriginales,
                fiadoresRecurrentes = [];
            for (let key in tmpFiadores) {
                for (let key2 in this.datosGuardar.fiadores) {
                    if (tmpFiadores[key].id == this.datosGuardar.fiadores[key2].id) {
                        let detalles = {},
                            cnt = 0;
                        fiadoresRecurrentes.push(tmpFiadores[key].id);
                        for (let key3 in tmpFiadores[key]) {
                            let nuevoValor = this.datosGuardar.fiadores[key2][key3],
                                valorAnterior = tmpFiadores[key][key3];
                            (nuevoValor != null && nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                            (valorAnterior != null && valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                            if (nuevoValor != valorAnterior) {
                                cnt++;
                                detalles[cnt] = {
                                    campo: key3,
                                    valorAnterior,
                                    nuevoValor
                                }
                            }
                        }

                        if (Object.keys(detalles).length > 0) {
                            this.datosGuardar.fiadores[key].logs = {
                                accion: 'Guardar',
                                descripcion: 'Edición de datos de fiador',
                                detalles
                            };
                            existenCambios = true;
                        }
                    }
                }
            }

            for (let key in tmpFiadores) {
                if (!fiadoresRecurrentes.includes(tmpFiadores[key].id)) {
                    configFiduciaria.cnt++;
                    this.datosGuardar.fiadores[configFiduciaria.cnt] = {
                        id: tmpFiadores[key].id,
                        logs: {
                            accion: "Eliminar",
                            descripcion: "Eliminar fiador"
                        }
                    }
                    existenCambios = true;
                }
            }

            for (let key in this.datosGuardar.fiadores) {
                if (this.datosGuardar.fiadores[key].id == 0 && !this.datosGuardar.fiadores[key].logs) {
                    this.datosGuardar.fiadores[key].logs = {
                        accion: "Guardar",
                        descripcion: "Creación de fiador"
                    }
                    existenCambios = true;
                }
            }
        }

        return existenCambios;
    }
}

const configDatosCredito = {
    datosOriginales: {},
    cboAgencia: document.getElementById("cboAgencia"),
    campoNumeroCredito: document.getElementById("txtNumeroCredito"),
    campoNumeroCreditoPagare: document.getElementById('txtNumeroCreditoPagare'),
    campoMonto: document.getElementById("numMontoCredito"),
    campoTasaInteres: document.getElementById("numTasaInteres"),
    campoFechaVencimiento: document.getElementById("txtFechaVencimiento"),
    campoFechaGeneracion: document.getElementById('txtFechaGeneracion'),
    init: function (tmpDatos = {}) {
        return new Promise((resolve, reject) => {
            try {
                if (Object.keys(tmpDatos).length > 0) {
                    let allSelectpickerCbos = document.querySelectorAll('#credito select.selectpicker');
                    this.cboAgencia.value = tmpDatos.agencia;
                    this.campoNumeroCredito.value = tmpDatos.numeroCredito;
                    tmpDatos.numeroCreditoPagare && (this.campoNumeroCreditoPagare.value = tmpDatos.numeroCreditoPagare);
                    this.campoMonto.value = tmpDatos.monto;
                    this.campoTasaInteres.value = tmpDatos.tasaInteres;
                    this.campoFechaVencimiento.value = tmpDatos.fechaVencimiento ? tmpDatos.fechaVencimiento : '';
                    this.campoFechaGeneracion.value = tmpDatos.fechaGeneracion ? tmpDatos.fechaGeneracion : '';
                    this.campoFechaVencimiento.value.length > 0 && ($("#" + this.campoFechaVencimiento.id).datepicker("update", tmpDatos.fechaVencimiento));
                    this.campoFechaGeneracion.value.length > 0 && ($("#" + this.campoFechaGeneracion.id).datepicker("update", tmpDatos.fechaGeneracion));

                    allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
                    !tmpDatos.dependienteMutuo && (this.datosOriginales = JSON.parse(JSON.stringify(this.getJSON())));
                } else {

                }
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    habilitarEdicion: function () {
        return new Promise((resolve, reject) => {
            try {
                let allSelectpickerCbos = document.querySelectorAll('#credito select.selectpicker')
                this.cboAgencia.disabled = false;
                this.campoMonto.readOnly = false;
                this.campoTasaInteres.readOnly = false;
                this.campoFechaVencimiento.disabled = false;
                this.campoFechaGeneracion.disabled = false;

                allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    getJSON: function () {
        let datos = {
            idMutuo: configDocumento.idMutuo,
            agencia: this.cboAgencia.value,
            labelAgencia: this.cboAgencia.options[this.cboAgencia.options.selectedIndex].text,
            numeroCredito: this.campoNumeroCredito.value,
            numeroCreditoPagare: this.campoNumeroCreditoPagare.value,
            monto: this.campoMonto.value,
            tasaInteres: this.campoTasaInteres.value,
            fechaGeneracion: this.campoFechaGeneracion.value,
            fechaVencimiento: this.campoFechaVencimiento.value,
        };

        return datos;
    },
}

const configDatosDeudor = {
    id: 0,
    datosOriginales: {},
    campoCodigoCliente: document.getElementById("txtCodigoCliente"),
    cboTipoDocumento: document.getElementById("cboTipoDocumentoDeudor"),
    campoNumeroDocumento: document.getElementById("txtNumeroDocumentoDeudor"),
    campoNIT: document.getElementById("txtNITDeudor"),
    campoNombres: document.getElementById("txtNombresDeudor"),
    campoApellidos: document.getElementById("txtApellidosDeudor"),
    campoConocido: document.getElementById("txtConocidoDeudor"),
    cboGenero: document.getElementById("cboGeneroDeudor"),
    cboPais: document.getElementById("cboPaisDeudor"),
    cboDepartamento: document.getElementById("cboDepartamentoDeudor"),
    cboMunicipio: document.getElementById("cboMunicipioDeudor"),
    campoDireccion: document.getElementById("txtDireccionDeudor"),
    campoFechaNacimiento: document.getElementById('txtFechaNacimientoDeudor'),
    campoEdad: document.getElementById('numEdadDeudor'),
    campoTelefono: document.getElementById('txtTelefono'),
    init: function (tmpDatos = {}) {
        return new Promise((resolve, reject) => {
            try {
                if (Object.keys(tmpDatos).length > 0) {
                    let allSelectpickerCbos = document.querySelectorAll('#deudor select.selectpicker');
                    this.campoCodigoCliente.value = tmpDatos.codigoCliente;
                    this.cboTipoDocumento.value = tmpDatos.tipoDocumento;
                    $("#" + this.cboTipoDocumento.id).trigger("change");
                    this.campoNumeroDocumento.value = tmpDatos.numeroDocumento;
                    tmpDatos.dependienteMutuo && (this.campoNumeroDocumento.readOnly = true);
                    this.campoNIT.value = tmpDatos.nit;
                    this.campoNombres.value = tmpDatos.nombres;
                    this.campoApellidos.value = tmpDatos.apellidos;
                    this.campoConocido.value = tmpDatos.conocido;
                    this.cboGenero.value = tmpDatos.genero;
                    this.cboPais.value = tmpDatos.pais;
                    $("#" + this.cboPais.id).trigger("change");
                    this.cboDepartamento.value = tmpDatos.departamento;
                    $("#" + this.cboDepartamento.id).trigger("change");
                    this.cboMunicipio.value = tmpDatos.municipio;
                    this.campoDireccion.value = tmpDatos.direccion ? tmpDatos.direccion : '';
                    this.campoFechaNacimiento.value = tmpDatos.fechaNacimiento;
                    $('#' + this.campoFechaNacimiento.id).trigger('change');

                    tmpDatos.telefono && (this.campoTelefono.value = tmpDatos.telefono);

                    allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
                    this.id = tmpDatos.id;
                    if (!tmpDatos.dependienteMutuo) {
                        this.datosOriginales = {
                            ...this.getJSON()
                        };
                    }
                }
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    getJSON: function () {
        let datos = {
            id: this.id,
            codigoCliente: this.campoCodigoCliente.value,
            tipoDocumento: this.cboTipoDocumento.value,
            labelTipoDocumento: this.cboTipoDocumento.options[this.cboTipoDocumento.options.selectedIndex].text,
            numeroDocumento: this.campoNumeroDocumento.value,
            nit: this.campoNIT.value,
            nombres: this.campoNombres.value,
            apellidos: this.campoApellidos.value,
            conocido: this.campoConocido.value,
            genero: this.cboGenero.value,
            labelGenero: this.cboGenero.options[this.cboGenero.options.selectedIndex].text,
            pais: this.cboPais.value,
            labelPais: this.cboPais.options[this.cboPais.options.selectedIndex].text,
            departamento: this.cboDepartamento.value,
            labelDepartamento: this.cboDepartamento.options[this.cboDepartamento.options.selectedIndex].text,
            municipio: this.cboMunicipio.value,
            labelMunicipio: this.cboMunicipio.options[this.cboMunicipio.options.selectedIndex].text,
            direccion: this.campoDireccion.value,
            fechaNacimiento: this.campoFechaNacimiento.value,
            edad: this.campoEdad.value,
            telefono: this.campoTelefono.value
        };

        return datos;
    },
    habilitarEdicion: function () {
        return new Promise((resolve, reject) => {
            try {
                let allSelectpickerCbos = document.querySelectorAll('#deudor select.selectpicker')
                this.campoCodigoCliente.readOnly = false;
                this.cboTipoDocumento.disabled = false;
                $('#' + this.cboTipoDocumento.id).trigger('change');
                this.campoNIT.readOnly = false;
                this.campoNombres.readOnly = false;
                this.campoApellidos.readOnly = false;
                this.campoConocido.readOnly = false;
                this.cboGenero.disabled = false;
                this.cboPais.disabled = false;
                this.cboDepartamento.disabled = false;
                this.cboMunicipio.disabled = false;
                this.campoDireccion.readOnly = false;
                allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    }
}

const configFiduciaria = {
    cnt: 0,
    id: 0,
    datosOriginales: {},
    fiadores: {},
    cboTipoDocumento: document.getElementById("cboTipoDocumentoFiador"),
    campoDocumento: document.getElementById("txtNumeroDocumentoFiador"),
    campoNIT: document.getElementById("txtNITFiador"),
    campoNombres: document.getElementById("txtNombresFiador"),
    campoApellidos: document.getElementById("txtApellidosFiador"),
    campoConocido: document.getElementById("txtConocidoFiador"),
    cboPais: document.getElementById("cboPaisFiador"),
    cboDepartamento: document.getElementById("cboDepartamentoFiador"),
    cboMunicipio: document.getElementById("cboMunicipioFiador"),
    campoDireccion: document.getElementById("txtDireccionFiador"),
    campoFechaNacimiento: document.getElementById("txtFechaNacimientoFiador"),
    campoEdad: document.getElementById("numEdadFiador"),
    init: function () {
        return new Promise((resolve, reject) => {
            try {
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmFiador').then(() => {
            this.id = id;
            let allSelectpickerCbos = document.querySelectorAll("#frmFiador select.selectpicker");
            if (this.id > 0) {
                let tmp = this.fiadores[this.id];

                this.cboTipoDocumento.value = tmp.tipoDocumento;
                this.campoDocumento.value = tmp.numeroDocumento;
                this.campoNIT.value = tmp.nit;
                this.campoNombres.value = tmp.nombres;
                this.campoApellidos.value = tmp.apellidos;
                this.campoConocido.value = tmp.conocido;
                this.cboPais.value = tmp.pais;
                $("#" + this.cboPais.id).trigger("change");
                this.cboDepartamento.value = tmp.departamento;
                $("#" + this.cboDepartamento.id).trigger("change");
                this.cboMunicipio.value = tmp.municipio;
                this.campoDireccion.value = tmp.direccion;
                this.campoFechaNacimiento.value = tmp.fechaNacimiento;
                $("#" + this.campoFechaNacimiento.id).datepicker('update', tmp.fechaNacimiento)
                this.campoEdad.value = tmp.edad;
            }

            allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));

            $("#mdlFiador").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmFiador').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.fiadores[this.id].id : 0,
                    tipoDocumento: this.cboTipoDocumento.value,
                    labelTipoDocumento: this.cboTipoDocumento.options[this.cboTipoDocumento.options.selectedIndex].text,
                    numeroDocumento: this.campoDocumento.value,
                    nit: this.campoNIT.value,
                    nombres: this.campoNombres.value,
                    apellidos: this.campoApellidos.value,
                    conocido: this.campoConocido.value,
                    pais: this.cboPais.value,
                    labelPais: this.cboPais.options[this.cboPais.options.selectedIndex].text,
                    departamento: this.cboDepartamento.value,
                    labelDepartamento: this.cboDepartamento.options[this.cboDepartamento.options.selectedIndex].text,
                    municipio: this.cboMunicipio.value,
                    labelMunicipio: this.cboMunicipio.options[this.cboMunicipio.options.selectedIndex].text,
                    direccion: this.campoDireccion.value,
                    fechaNacimiento: this.campoFechaNacimiento.value,
                    edad: this.campoEdad.value,
                    dependienteMutuo: this.id > 0 ? this.fiadores[this.id].dependienteMutuo : false
                };

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trFiador" + this.id).find("td:eq(1)").html(tmp.numeroDocumento);
                        $("#trFiador" + this.id).find("td:eq(2)").html(tmp.nit);
                        $("#trFiador" + this.id).find("td:eq(3)").html(tmp.nombres + ' ' + tmp.apellidos);
                        $("#trFiador" + this.id).find("td:eq(4)").html(tmp.edad);
                        $("#trFiador" + this.id).find("td:eq(5)").html(tmp.direccion);
                        $("#trFiador" + this.id).find("td:eq(6)").html(tmp.labelPais);
                        $("#trFiador" + this.id).find("td:eq(7)").html(tmp.labelDepartamento);
                        $("#trFiador" + this.id).find("td:eq(8)").html(tmp.labelMunicipio);
                        resolve();
                    }
                });

                guardar.then(() => {
                    configFiduciaria.fiadores[this.id] = {
                        ...tmp
                    };
                    tblFiadores.columns.adjust().draw();
                    tblFiadores.responsive.recalc();
                    $("#mdlFiador").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        numeroDocumento,
        nit,
        nombres,
        apellidos,
        edad,
        direccion = '',
        labelPais,
        labelDepartamento,
        labelMunicipio,
        dependienteMutuo
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configFiduciaria.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>";

                !dependienteMutuo && (botones += "<span class='btnTbl' title='Eliminar' onclick='configFiduciaria.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>");
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblFiadores.row.add([
                    nFila,
                    numeroDocumento,
                    nit,
                    `${nombres} ${apellidos}`,
                    edad,
                    direccion,
                    labelPais,
                    labelDepartamento,
                    labelMunicipio,
                    botones
                ]).node().id = 'trFiador' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let tmp = this.fiadores[id],
            label = tmp.nombres + ' ' + tmp.apellidos;
        Swal.fire({
            title: 'Eliminar fiador',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> los datos del fiador <strong>' + label + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblFiadores.row('#trFiador' + id).remove().draw();
                delete configFiduciaria.fiadores[id];
            }
        });
    },
}