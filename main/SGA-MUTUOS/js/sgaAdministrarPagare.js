let tblPagares;

window.onload = () => {
    document.getElementById('frmBusqueda').reset();
    initTables().then(() => {
        fetchActions.getCats({
            modulo: "SGA-MUTUOS",
            archivo: 'sgaGetCats',
            solicitados: ['agencias']
        }).then(({
            agencias
        }) => {
            let opciones = [`<option selected disabled value=''>seleccione</option>`],
                cbosAgencia = document.querySelectorAll('select.cboAgencia');
            agencias.forEach(agencia => opciones.push(`<option value='${agencia.id}'>${agencia.nombreAgencia}</option>`));

            cbosAgencia.forEach(cbo => {
                cbo.innerHTML = opciones.join("");
                cbo.className.includes('selectpicker') && ($("#" + cbo.id).selectpicker("refresh"));
            });
        }).catch(generalMostrarError);
    });
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblPagares").length) {
                tblPagares = $("#tblPagares").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de resolución y clausula';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblPagares.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configBusqueda = {
    id: 0,
    cboAgencia: document.getElementById('cboAgencia'),
    campoInicio: document.getElementById('txtInicio'),
    campoFin: document.getElementById('txtFin'),
    cnt: 0,
    resultados: {},
    search: function () {
        formActions.validate('frmBusqueda').then(({
            errores
        }) => {
            if (errores == 0) {
                tblPagares.clear().draw();
                this.cnt = 0;
                this.resultados = {};
                fetchActions.get({
                    modulo: "SGA-MUTUOS",
                    archivo: 'sgaGetPagareAdministracion',
                    params: {
                        agencia: this.cboAgencia.value,
                        inicio: encodeURIComponent(this.campoInicio.value),
                        fin: encodeURIComponent(this.campoFin.value),
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                datosRespuesta.resultados.forEach(RC => {
                                    this.cnt++;
                                    this.resultados[this.cnt] = {
                                        ...RC
                                    };
                                    this.addRowTbl({
                                        ...RC,
                                        nFila: this.cnt
                                    });
                                });
                                tblPagares.columns.adjust().draw();
                                break
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        numeroCredito,
        nombres,
        apellidos,
        monto,
        fechaVencimiento,
        fechaRegistro,
        nombreAgencia,
        usuario
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configBusqueda.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Reimprimir' onclick='configBusqueda.print(" + nFila + ");'>" +
                    "<i class='fas fa-print'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configBusqueda.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblPagares.row.add([
                    nFila,
                    numeroCredito,
                    `${nombres} ${apellidos}`,
                    '$' + new Intl.NumberFormat().format(parseFloat(monto).toFixed(2)),
                    fechaVencimiento,
                    fechaRegistro,
                    nombreAgencia,
                    usuario,
                    botones
                ]).node().id = 'trRC' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        let tipo = 'Sub',
            tmpIdApartado = 'MjQ%3D',
            contenedor = 'U0dBLU1VVFVPUw%3D%3D',
            archivo = 'sgaPagare',
            tmpId = encodeURIComponent(btoa(this.resultados[id].id));
        fetchActions.getCats({
            modulo: "",
            archivo: "validarPermiso",
            solicitados: [tipo, tmpIdApartado],
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta.trim()) {
                    case "EXITO":
                        localStorage.setItem('tipoApartado', tipo);
                        localStorage.setItem('idApartado', tmpIdApartado);
                        localStorage.setItem('tipoPermiso', datosRespuesta.tipoPermiso);
                        window.top.location.href = `?page=${archivo}&mod=${contenedor}&id=${tmpId}`;
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    print: function (id = 0) {
        this.id = id;
        $("#mdlGeneracion").modal("show");
    },
    delete: function (id = 0) {
        let tmpId = this.resultados[id].id,
            label = this.resultados[id].numeroCredito;
        Swal.fire({
            title: 'Eliminar documentos',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la resolución y clausula con número de crédito <b>' + label + '</b>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: 'SGA-MUTUOS',
                    archivo: 'sgaEliminarPagare',
                    datos: {
                        id: tmpId,
                        idApartado,
                        tipoApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta) {
                            case "EXITO":
                                tblPagares.row('#trRC' + id).remove().draw();
                                delete this.resultados[id];
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    printPagare() {
        window.open('./main/SGA-MUTUOS/docs/Pagare/P' + this.resultados[this.id].numeroCredito + '.pdf');
    },
}