let tblAditivos,
    tblGarantias,
    tblDesembolsos,
    tblFiadores,
    tblHipotecas,
    tblMatriculas,
    tblModificacionesHipoteca;
window.onload = () => {
    document.getElementById('frmDeudor').reset();
    document.getElementById('frmCredito').reset();
    document.getElementById('frmFirmante').reset();
    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "SGA-MUTUOS",
                archivo: "sgaGetCats",
                solicitados: ['all']
            }).then((datosRespuesta) => {
                $(".preloader").fadeIn("fast");
                configMutuo.initCats(datosRespuesta).then(resolve).catch(reject);
            }).catch(reject);
        });
    });

    gettingCats.then(() => {
        let idEdicion = getParameterByName('id');
        if (idEdicion.length > 0) {
            idEdicion = atob(decodeURIComponent(idEdicion));
            fetchActions.get({
                modulo: "SGA-MUTUOS",
                archivo: 'sgaGetMutuo',
                params: {
                    id: idEdicion
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta.trim()) {
                        case "EXITO":
                            configMutuo.id = idEdicion;
                            configDatosDeudor.init(datosRespuesta.datosDeudor);
                            configDatosCredito.init(datosRespuesta.datosCredito);
                            configGarantia.init(datosRespuesta.datosGarantias);
                            if (Object.keys(datosRespuesta.datosFirmante).length > 0) {
                                configFirmante.init(datosRespuesta.datosFirmante);
                            }
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            $(".preloader").fadeOut("fast");
        }
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblAditivos").length) {
                tblAditivos = $("#tblAditivos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de aditivo';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblAditivos.columns.adjust().draw();
            }

            if ($("#tblGarantias").length) {
                tblGarantias = $("#tblGarantias").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de garantía';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblGarantias.columns.adjust().draw();
            }

            if ($("#tblDesembolsos").length) {
                tblDesembolsos = $("#tblDesembolsos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de desembolso';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblDesembolsos.columns.adjust().draw();
            }

            if ($("#tblFiadores").length) {
                tblFiadores = $("#tblFiadores").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblFiadores.columns.adjust().draw();
            }

            if ($("#tblHipotecas").length) {
                tblHipotecas = $("#tblHipotecas").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de hipoteca';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblHipotecas.columns.adjust().draw();
            }

            if ($("#tblMatriculas").length) {
                tblMatriculas = $("#tblMatriculas").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de matrícula';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblMatriculas.columns.adjust().draw();
            }

            if ($("#tblModificacionesHipoteca").length) {
                tblModificacionesHipoteca = $("#tblModificacionesHipoteca").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de modificación';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblModificacionesHipoteca.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configMutuo = {
    id: 0,
    revisionGenerada: false,
    mutuoGenerado: false,
    intentos: 5,
    datosGuardar: {},
    tabDeudor: document.getElementById('deudor-tab'),
    tabCredito: document.getElementById('credito-tab'),
    tabGarantias: document.getElementById('garantia-tab'),
    tabFirmante: document.getElementById('ruego-tab'),
    tabParciales: document.getElementById('parcial-tab'),
    formDeudor: document.getElementById('frmDeudor'),
    formCredito: document.getElementById("frmCredito"),
    formFirmante: document.getElementById("frmFirmante"),
    cboFirmaDeudor: document.getElementById('cboFirmaDeudor'),
    incluyeFirmante: false,
    incluyeParciales: false,
    init: function () {},
    initCats: function ({
        tiposDocumento: tiposDoc,
        agencias,
        datosGeograficos: geograficos,
        lineasFinancieras,
        periodicidades,
        fuentesFondo,
        aditivos,
        tiposGarantia,
        tiposPlazo,
        zonasHipoteca
    }) {
        return new Promise((resolve, reject) => {
            try {
                let cbosTipoDocumento = document.querySelectorAll("select.cboTipoDocumento"),
                    cbosPais = document.querySelectorAll("select.cboPais"),
                    cbosAgencia = document.querySelectorAll("select.cboAgencia"),
                    cbosLineaFinanciera = document.querySelectorAll("select.cboLineaFinanciera"),
                    cbosPeriodicidad = document.querySelectorAll("select.cboPeriodicidad"),
                    cbosPlazo = document.querySelectorAll("select.cboPlazo"),
                    cbosFuenteFondos = document.querySelectorAll("select.cboFuenteFondos"),
                    cbosZonasHipoteca = document.querySelectorAll("select.cboZonaHipoteca"),
                    allSelectpickerCbos = document.querySelectorAll("select.selectpicker");
                datosGeograficos = {
                    ...geograficos
                };

                const opciones = [`<option selected value="" disabled>seleccione</option>`];

                let tmpOpciones = [...opciones];
                tiposDoc.forEach(tipo => {
                    tiposDocumento[tipo.id] = {
                        ...tipo
                    };
                    tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
                });

                cbosTipoDocumento.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                for (let key in geograficos) {
                    tmpOpciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
                }
                cbosPais.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                agencias.forEach(agencia => tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`));
                cbosAgencia.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                lineasFinancieras.forEach(linea => tmpOpciones.push(`<option value="${linea.id}">${linea.lineaFinanciera}</option>`));
                cbosLineaFinanciera.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                periodicidades.forEach(periodicidad => tmpOpciones.push(`<option value="${periodicidad.id}">${periodicidad.periodicidad}</option>`));
                cbosPeriodicidad.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                tiposPlazo.forEach(tipo => tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoPlazo}</option>`));
                cbosPlazo.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                fuentesFondo.forEach(fuente => tmpOpciones.push(`<option value="${fuente.id}">${fuente.fuenteFondos}</option>`));
                cbosFuenteFondos.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                tmpOpciones = [...opciones];
                aditivos.forEach(aditivo => configDatosCredito.configCuota.configAditivos.catalogo[aditivo.id] = {
                    ...aditivo
                });

                tmpOpciones = [...opciones];
                tiposGarantia.forEach(tipo => configGarantia.catalogo[tipo.id] = {
                    ...tipo
                });

                tmpOpciones = [...opciones];

                for (let key in zonasHipoteca) {
                    tmpOpciones.push(`<option value="${zonasHipoteca[key].id}">${zonasHipoteca[key].zona}</option>`);
                }

                cbosZonasHipoteca.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

                allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));

                configHipotecaria.zonas = {
                    ...zonasHipoteca
                };


                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    save: function () {
        if (formActions.manualValidate(this.formDeudor.id)) {
            if (formActions.manualValidate(this.formCredito.id)) {
                if (Object.keys(configGarantia.garantias).length > 0) {
                    let garantiasConfiguradas = true,
                        garantiasPendientes = [],
                        excepcionConfiguracion = ['1', '4', '7', '8'];

                    for (let key in configGarantia.garantias) {
                        if (!excepcionConfiguracion.includes(configGarantia.garantias[key].idTipoGarantia) && Object.keys(configGarantia.garantias[key].configuracion).length == 0) {
                            garantiasConfiguradas = false;
                            garantiasPendientes.push(`- ${configGarantia.garantias[key].tipoGarantia}`);
                        }
                    }

                    if (garantiasConfiguradas) {
                        let formularioCompleto = true;

                        if (this.incluyeFirmante && !formActions.manualValidate(this.formFirmante.id)) {
                            formularioCompleto = false;
                            $("#" + this.tabFirmante.id).trigger("click");
                            toastr.warning("Datos de firmante a ruego incompletos");
                        }

                        if (formularioCompleto) {
                            if (this.incluyeParciales && Object.keys(configDesembolsos.desembolsos).length == 0) {
                                formularioCompleto = false;
                                toastr.warning("Debe especificar el detalle de los desembolsos");
                                $("#" + this.tabParciales.id).trigger("click");
                            } else if (this.incluyeParciales) {
                                let montoCredito = parseFloat(configDatosCredito.campoMonto.value),
                                    montoDesembolsos = 0;

                                for (let key in configDesembolsos.desembolsos) {
                                    montoDesembolsos += parseFloat(configDesembolsos.desembolsos[key].monto);
                                }

                                if (montoCredito != montoDesembolsos) {
                                    formularioCompleto = false;
                                    $("#" + this.tabParciales.id).trigger("click");
                                    Swal.fire({
                                        title: "¡Atención!",
                                        html: "El total de los montos de desembolsos no coíncide con el monto del crédito",
                                        icon: "warning"
                                    });
                                }
                            }
                        }

                        if (formularioCompleto) {
                            document.getElementById("strIntentos").innerHTML = this.intentos;
                            $("#mdlGeneracion").modal("show");
                        }
                    } else {
                        $("#" + this.tabGarantias.id).trigger("click");
                        Swal.fire({
                            title: "¡Atención!",
                            html: `Las siguientes garantías requieren configuración: <br />${garantiasPendientes.join("<br />")}`,
                            icon: "warning"
                        });
                    }
                } else {
                    toastr.warning("Debe configurar al menos una garantía");
                    $("#" + this.tabGarantias.id).trigger("click");
                }
            } else {
                $("#" + this.tabCredito.id).trigger("click");
                toastr.warning("Datos de crédito incompletos");
            }
        } else {
            $("#" + this.tabDeudor.id).trigger("click");
            toastr.warning("Datos de deudor incompletos");
        }
    },
    prepareData: function () {
        let datos = {
            id: this.id,
            datosDeudor: {
                ...configDatosDeudor.getJSON()
            },
            datosCredito: {
                ...configDatosCredito.getJSON()
            },
            datosFirmante: {
                ...configFirmante.getJSON()
            },
            garantias: JSON.parse(JSON.stringify(configGarantia.garantias)),
        };

        return {
            ...datos
        };
    },
    generarRevision: function () {
        let aplica = configMutuo.evalCambios();
        // if (aplica) {

        // } else {
        //     Swal.fire({
        //         title: "¡Atención!",
        //         html: "No hay cambios en el formulario.",
        //         icon: "info"
        //     });
        // }
        fetchActions.set({
            modulo: "SGA-MUTUOS",
            archivo: "sgaConstruirHojaRevision",
            datos: {
                ...this.prepareData(),
                incluyeFirmante: this.incluyeFirmante,
                incluyeParciales: this.incluyeParciales
            }
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta.trim()) {
                    case "EXITO":
                        this.revisionGenerada = true;
                        this.mutuoGenerado = false;
                        window.open("./main/SGA-MUTUOS/docs/HojaRevision/" + datosRespuesta.nombreArchivo);
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    generarMutuo: function () {
        let modificaciones = false;
        if (this.id > 0) {
            if (this.mutuoGenerado == true) {
                if (this.evalCambios()) {
                    this.revisionGenerada = false;
                    modificaciones = true;
                }
            }
        }
        if (this.revisionGenerada) {
            fetchActions.set({
                modulo: "SGA-MUTUOS",
                archivo: 'sgaGuardarMutuo',
                datos: {
                    ...this.datosGuardar,
                    incluyeFirmante: this.incluyeFirmante,
                    incluyeParciales: this.incluyeParciales,
                    tipoApartado,
                    idApartado
                }
            }).then((datosRespuesta) => {
                console.log(datosRespuesta);
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta.trim()) {
                        case "EXITO":
                            this.id = datosRespuesta.id;
                            this.mutuoGenerado = true;
                            this.intentos--;
                            let tmpModulo = btoa('SGA-MUTUOS');

                            for (let key in configDatosCredito.configCuota.configAditivos.aditivos) {
                                configDatosCredito.configCuota.configAditivos.aditivos[key].id = datosRespuesta.idsAditivos[key];
                            }
                            for (let key in configDesembolsos.desembolsos) {
                                configDesembolsos.desembolsos[key].id = datosRespuesta.idsDesembolsos[key];
                            }
                            configDatosCredito.datosOriginales = JSON.parse(JSON.stringify(configDatosCredito.getJSON()));
                            configDatosDeudor.id = datosRespuesta.idDeudor;
                            configDatosDeudor.datosOriginales = {
                                ...configDatosDeudor.getJSON()
                            };

                            if (this.intentos == 0) {
                                window.top.location.href = './?page=sgaMutuo&mod=' + tmpModulo;
                            }
                            // console.log(configGarantia.garantias,datosRespuesta.idsGarantias);
                            for (let key in configGarantia.garantias) {
                                configGarantia.garantias[key].id = datosRespuesta.idsGarantias[key].id;
                                let configuracionUnica = ['2', '3'];
                                if (configuracionUnica.includes(configGarantia.garantias[key].idTipoGarantia)) {
                                    configGarantia.garantias[key].configuracion.id = datosRespuesta.idsGarantias[key].configuracion.id;
                                } else {
                                    for (let key2 in configGarantia.garantias[key].configuracion) {
                                        configGarantia.garantias[key].configuracion[key2].id = datosRespuesta.idsGarantias[key].configuracion[key2].id;
                                        if (datosRespuesta.idsGarantias[key].configuracion[key2].matriculas) {
                                            for (let key3 in configGarantia.garantias[key].configuracion[key2].matriculas) {
                                                configGarantia.garantias[key].configuracion[key2].matriculas[key3].id = datosRespuesta.idsGarantias[key].configuracion[key2].matriculas[key3].id;
                                            }
                                        }

                                        if (datosRespuesta.idsGarantias[key].configuracion[key2].modificaciones) {
                                            for (let key3 in configGarantia.garantias[key].configuracion[key2].modificaciones) {
                                                configGarantia.garantias[key].configuracion[key2].modificaciones[key3].id = datosRespuesta.idsGarantias[key].configuracion[key2].modificaciones[key3].id;
                                            }
                                        }
                                    }
                                }
                            }
                            let idFirmante = datosRespuesta.idFirmante;

                            idFirmante == null && (idFirmante = 0);

                            configFirmante.id = idFirmante;
                            if (this.incluyeFirmante) {
                                configFirmante.datosOriginales = JSON.parse(JSON.stringify(configFirmante.getJSON()));
                            } else {
                                configFirmante.datosOriginales = {};
                            }

                            configGarantia.datosOriginales = JSON.parse(JSON.stringify(configGarantia.garantias));
                            document.getElementById("strIntentos").innerHTML = this.intentos;
                            if (datosRespuesta.Errores.length > 0 || Object.keys(datosRespuesta.Errores).length > 0) {
                                let arrErrores = [];
                                if (datosRespuesta.Errores.length > 0) {
                                    arrErrores = [...datosRespuesta.Errores];
                                } else {
                                    for (let key in datosRespuesta.Errores) {
                                        arrErrores.push(`${key}: ${datosRespuesta.Errores[key]}`);
                                    }
                                }
                                Swal.fire({
                                    title: "¡Atención!",
                                    icon: "warning",
                                    html: `Parece que sólo se guardó parte de la información y ocurrió un error con la base de datos, los errores ocurridos son: <br> -${arrErrores.join("<br />-")}`
                                });
                            } else {
                                // Swal.fire({
                                //     title: "¡Bien hecho!",
                                //     icon: "success",
                                //     html: "Parece que todo va bien, algún día el sistema generará el documento"
                                // });
                                window.open('./main/SGA-MUTUOS/docs/Mutuo/' + datosRespuesta.nombreArchivo);
                            }
                            break;
                        case "Generado":
                            Swal.fire({
                                title: "¡Atención!",
                                html: "Parece que el mutuo para este número de crédito ya fue generado, puedes optar por anularlo o editarlo desde el administrador de documentos.",
                                icon: "warning"
                            });
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            if (!modificaciones) {
                Swal.fire({
                    title: "¡Atención!",
                    html: "Debes de generar un formato de revisión para poder generar el mutuo.",
                    icon: "warning"
                });
            } else {
                Swal.fire({
                    title: "¡Atención!",
                    html: "Parece que has modificado el formulario, es necesario generar otra hoja de revisión.",
                    icon: "warning"
                });
            }
        }
    },
    cancel: function () {
        let tmpModulo = btoa('SGA-MUTUOS');
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Salir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                window.top.location.href = './?page=sgaMutuo&mod=' + encodeURIComponent(tmpModulo);
            }
        });
    },
    evalFirma: function () {
        let firmante = false,
            tmp = {};

        this.cboFirmaDeudor.value == 'N' && (firmante = true);

        for (let key in configGarantia.garantias) {
            if (configGarantia.garantias[key].idTipoGarantia == '5') {
                tmp = {
                    ...configGarantia.garantias[key].configuracion
                };
            }
        }

        for (let key in tmp) {
            if (tmp[key].firma == 'N') {
                firmante = true;
            }
        }

        if (firmante) {
            this.tabFirmante.classList.remove("disabled");
        } else {
            this.tabFirmante.classList.add("disabled");
        }

        this.incluyeFirmante = firmante;
    },
    evalParciales: function (opcion) {
        if (opcion == 'S') {
            this.incluyeParciales = true;
            this.tabParciales.classList.remove("disabled");
        } else {
            this.incluyeParciales = false;
            this.tabParciales.classList.add("disabled");
        }
    },
    evalCambios: function () {
        let data = this.prepareData();
        this.datosGuardar = {
            ...data
        };
        let existenCambios = false;

        if (Object.keys(configDatosCredito.datosOriginales).length == 0) {
            this.datosGuardar.datosCredito.logs = {
                descripcion: "Creación de mutuo"
            };
            if (Object.keys(this.datosGuardar.datosCredito.aditivos).length > 0) {
                for (let key in this.datosGuardar.datosCredito.aditivos) {
                    this.datosGuardar.datosCredito.aditivos[key].logs = {
                        accion: "Guardar",
                        descripcion: "Creación de aditivo"
                    }
                }
            }
            if (Object.keys(this.datosGuardar.datosCredito.detalleDesembolsos).length > 0) {
                for (let key in this.datosGuardar.datosCredito.detalleDesembolsos) {
                    this.datosGuardar.datosCredito.detalleDesembolsos[key].logs = {
                        accion: "Guardar",
                        descripcion: "Creación de desembolso"
                    }
                }
            }
            existenCambios = true;
        } else {
            //Validar cambios
            let detalles = {},
                cnt = 0;

            for (let key in this.datosGuardar.datosCredito) {
                if (key != 'aditivos' && key != 'detalleDesembolsos' && key != 'totalCuota' && !key.includes('label')) {
                    let nuevoValor = this.datosGuardar.datosCredito[key],
                        valorAnterior = configDatosCredito.datosOriginales[key];
                    (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                    (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                    if (nuevoValor != valorAnterior) {
                        cnt++;
                        detalles[cnt] = {
                            campo: key,
                            valorAnterior,
                            nuevoValor
                        }
                    }
                } else if (key == 'aditivos') {
                    let tmpAditivos = configDatosCredito.datosOriginales.aditivos,
                        aditivosRecurrentes = [];
                    for (let key in tmpAditivos) {
                        for (let key2 in this.datosGuardar.datosCredito.aditivos) {
                            if ((this.datosGuardar.datosCredito.aditivos[key2].idAditivo == tmpAditivos[key].idAditivo) && this.datosGuardar.datosCredito.aditivos[key2].id != 0) {
                                aditivosRecurrentes.push(tmpAditivos[key].idAditivo);
                                let nuevoValor = this.datosGuardar.datosCredito.aditivos[key2].monto,
                                    valorAnterior = tmpAditivos[key].monto;
                                (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                                (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                                if (nuevoValor != valorAnterior) {
                                    this.datosGuardar.datosCredito.aditivos[key2].logs = {
                                        accion: 'Guardar',
                                        descripcion: 'Edición de monto de aditivo',
                                        detalles: {
                                            campo: 'monto',
                                            valorAnterior,
                                            nuevoValor
                                        }
                                    }
                                    existenCambios = true;
                                }
                            }
                        }
                    }

                    for (let key in tmpAditivos) {
                        if (!aditivosRecurrentes.includes(tmpAditivos[key].idAditivo)) {
                            configDatosCredito.configCuota.configAditivos.cnt++;
                            this.datosGuardar.datosCredito.aditivos[configDatosCredito.configCuota.configAditivos.cnt] = {
                                idAditivo: tmpAditivos[key].idAditivo,
                                id: tmpAditivos[key].id,
                                logs: {
                                    accion: "Eliminar",
                                    descripcion: 'Eliminación de aditivo',
                                    detalles: {}
                                }
                            }
                            existenCambios = true;
                        }
                    }

                    for (let key in this.datosGuardar.datosCredito.aditivos) {
                        if (this.datosGuardar.datosCredito.aditivos[key].id == 0 && !this.datosGuardar.datosCredito.aditivos[key].logs) {
                            this.datosGuardar.datosCredito.aditivos[key].logs = {
                                accion: "Guardar",
                                descripcion: "Creación de aditivo"
                            }
                            existenCambios = true;
                        }
                    }
                } else if (key == 'detalleDesembolsos') {
                    let tmpDesembolsos = configDatosCredito.datosOriginales.detalleDesembolsos,
                        desembolsosRecurrentes = [];

                    for (let key in tmpDesembolsos) {
                        for (let key2 in this.datosGuardar.datosCredito.detalleDesembolsos) {
                            let detalles = {},
                                cnt = 0;
                            if (tmpDesembolsos[key].id == this.datosGuardar.datosCredito.detalleDesembolsos[key2].id) {
                                desembolsosRecurrentes.push(tmpDesembolsos[key].id);
                                for (let key3 in this.datosGuardar.datosCredito.detalleDesembolsos[key2]) {
                                    let nuevoValor = this.datosGuardar.datosCredito.detalleDesembolsos[key][key3],
                                        valorAnterior = tmpDesembolsos[key][key3];
                                    (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                                    (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                                    if (valorAnterior != nuevoValor) {
                                        cnt++;
                                        detalles[cnt] = {
                                            campo: key3,
                                            valorAnterior,
                                            nuevoValor
                                        }
                                    }
                                }
                            }

                            if (Object.keys(detalles).length > 0) {
                                this.datosGuardar.datosCredito.detalleDesembolsos[key2].logs = {
                                    accion: 'Guardar',
                                    descripcion: "Edición de desembolso parcial",
                                    detalles
                                }
                                existenCambios = true;
                            }
                        }
                    }

                    for (let key in tmpDesembolsos) {
                        if (!desembolsosRecurrentes.includes(tmpDesembolsos[key].id)) {
                            configDesembolsos.cnt++;
                            this.datosGuardar.datosCredito.detalleDesembolsos[configDesembolsos.cnt] = {
                                id: tmpDesembolsos[key].id,
                                logs: {
                                    accion: 'Eliminar',
                                    descripcion: 'Eliminación de desembolso parcial'
                                }
                            }
                            existenCambios = true;
                        }
                    }

                    for (let key in this.datosGuardar.datosCredito.detalleDesembolsos) {
                        if (this.datosGuardar.datosCredito.detalleDesembolsos[key].id == 0 && !this.datosGuardar.datosCredito.detalleDesembolsos[key].logs) {
                            this.datosGuardar.datosCredito.detalleDesembolsos[key].logs = {
                                accion: "Guardar",
                                descripcion: "Creación de desembolso"
                            }
                            existenCambios = true;
                        }
                    }
                }
            }

            if (Object.keys(detalles).length > 0) {
                this.datosGuardar.datosCredito.logs = {
                    descripcion: 'Edición de valores de crédito',
                    detalles
                };
                existenCambios = true;
            }
        }

        if (Object.keys(configDatosDeudor.datosOriginales).length == 0) {
            this.datosGuardar.datosDeudor.logs = {
                descripcion: 'Creación de deudor/a'
            }
            existenCambios = true;
        } else {
            //Validar cambios
            let detalles = {},
                cnt = 0;

            for (let key in this.datosGuardar.datosDeudor) {
                if (!key.includes('label')) {
                    let nuevoValor = this.datosGuardar.datosDeudor[key],
                        valorAnterior = configDatosDeudor.datosOriginales[key];
                    (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                    (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                    if (valorAnterior != nuevoValor) {
                        cnt++;
                        detalles[cnt] = {
                            campo: key,
                            valorAnterior,
                            nuevoValor
                        };
                    }
                }
            }

            if (Object.keys(detalles).length > 0) {
                this.datosGuardar.datosDeudor.logs = {
                    descripcion: 'Edición de datos de deudor/a',
                    detalles
                };
                existenCambios = true;
            }
        }

        if (this.incluyeFirmante && Object.keys(configFirmante.datosOriginales).length == 0) {
            this.datosGuardar.datosFirmante.logs = {
                accion: 'Guardar',
                descripcion: 'Creación de firmante a ruego'
            }
            existenCambios = true;
        } else if (this.incluyeFirmante) {
            //Validar cambios
            let detalles = {},
                cnt = 0;

            if (Object.keys(configFirmante.datosOriginales).length > 0) {
                for (let key in this.datosGuardar.datosFirmante) {
                    let nuevoValor = this.datosGuardar.datosFirmante[key],
                        valorAnterior = configFirmante.datosOriginales[key];
                    (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                    (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                    if (valorAnterior != nuevoValor) {
                        cnt++;
                        detalles[cnt] = {
                            campo: key,
                            valorAnterior,
                            nuevoValor
                        }
                    }
                }

                if (Object.keys(detalles).length > 0) {
                    this.datosGuardar.datosFirmante.logs = {
                        accion: "Guardar",
                        descripcion: "Edición de datos de firmante a ruego",
                        detalles
                    }
                    existenCambios = true;
                }
            } else {
                this.datosGuardar.datosFirmante.logs = {
                    accion: 'Guardar',
                    descripcion: 'Creación de firmante a ruego'
                }
                existenCambios = true;
            }
        } else if (Object.keys(configFirmante.datosOriginales).length > 0 && !this.incluyeFirmante) {
            this.datosGuardar.datosFirmante.logs = {
                accion: 'Eliminar',
                descripcion: 'Eliminación de firmante a ruego',
                detalles: {
                    id: configFirmante.datosOriginales.id
                }
            }
            existenCambios = true;
        }

        if (Object.keys(configGarantia.datosOriginales).length == 0) {
            let configuracionUnica = ['2', '3'];
            for (let key in this.datosGuardar.garantias) {
                this.datosGuardar.garantias[key].logs = {
                    accion: "Guardar",
                    descripcion: "Creación de garantía"
                }
                if (Object.keys(this.datosGuardar.garantias[key].configuracion).length > 0 && configuracionUnica.includes(this.datosGuardar.garantias[key].idTipoGarantia)) {
                    this.datosGuardar.garantias[key].configuracion.logs = {
                        accion: "Guardar",
                        descripcion: 'Creación de datos'
                    };
                } else {
                    for (let key2 in this.datosGuardar.garantias[key].configuracion) {
                        this.datosGuardar.garantias[key].configuracion[key2].logs = {
                            accion: "Guardar",
                            descripcion: 'Creación de registro'
                        }

                        if (this.datosGuardar.garantias[key].configuracion[key2].matriculas) {
                            let tmpMatriculas = this.datosGuardar.garantias[key].configuracion[key2].matriculas;
                            if (Object.keys(tmpMatriculas).length > 0) {
                                for (let key3 in tmpMatriculas) {
                                    this.datosGuardar.garantias[key].configuracion[key2].matriculas[key3].logs = {
                                        accion: "Guardar",
                                        descripcion: "Creación de matrícula"
                                    };
                                }
                            }
                        }

                        if (this.datosGuardar.garantias[key].configuracion[key2].modificaciones) {
                            let tmpModificaciones = this.datosGuardar.garantias[key].configuracion[key2].modificaciones;
                            if (Object.keys(tmpModificaciones).length > 0) {
                                for (let key3 in tmpModificaciones) {
                                    this.datosGuardar.garantias[key].configuracion[key2].modificaciones[key3].logs = {
                                        accion: "Guardar",
                                        descripcion: "Creación de modificación"
                                    };
                                }
                            }
                        }
                    }
                }
            }
            existenCambios = true;
        } else {
            //Validar cambios
            let tmpGarantias = configGarantia.datosOriginales,
                garantiasRecurrentes = [],
                configuracionUnica = ['2', '3'];
            for (let key in tmpGarantias) {
                for (let key2 in this.datosGuardar.garantias) {
                    if ((this.datosGuardar.garantias[key2].idTipoGarantia == tmpGarantias[key].idTipoGarantia) && this.datosGuardar.garantias[key2].id != 0) {
                        garantiasRecurrentes.push(tmpGarantias[key].idTipoGarantia);
                        if (Object.keys(this.datosGuardar.garantias[key2].configuracion).length > 0 && configuracionUnica.includes(this.datosGuardar.garantias[key2].idTipoGarantia)) {
                            let detalles = {},
                                cnt = 0;
                            for (let key3 in tmpGarantias[key].configuracion) {
                                let nuevoValor = this.datosGuardar.garantias[key2].configuracion[key3],
                                    valorAnterior = tmpGarantias[key].configuracion[key3];

                                (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                                (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                                if (valorAnterior != nuevoValor) {
                                    cnt++;
                                    detalles[cnt] = {
                                        campo: key3,
                                        valorAnterior,
                                        nuevoValor
                                    };
                                }
                            }
                            if (Object.keys(detalles).length > 0) {
                                this.datosGuardar.garantias[key2].configuracion.logs = {
                                    accion: "Guardar",
                                    descripcion: "Edición de configuración de garantía",
                                    detalles
                                }
                                existenCambios = true;
                            }
                        } else {
                            let configuracionRecurrente = [],
                                tmpConfiguracionActual = this.datosGuardar.garantias[key2].configuracion;
                            for (let key3 in tmpGarantias[key].configuracion) {
                                for (let key4 in tmpConfiguracionActual) {
                                    if (tmpGarantias[key].configuracion[key3].id == tmpConfiguracionActual[key4].id) {
                                        configuracionRecurrente.push(tmpGarantias[key].configuracion[key3].id);
                                        let detalles = {},
                                            cnt = 0;
                                        for (let key5 in tmpGarantias[key].configuracion[key3]) {
                                            if (!key5.includes('label') && key5 != 'matriculas' && key5 != 'modificaciones') {
                                                let valorAnterior = tmpGarantias[key].configuracion[key3][key5],
                                                    nuevoValor = tmpConfiguracionActual[key4][key5];
                                                (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                                                (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                                                if (valorAnterior != nuevoValor) {
                                                    cnt++;
                                                    detalles[cnt] = {
                                                        campo: key5,
                                                        valorAnterior,
                                                        nuevoValor
                                                    }
                                                }
                                            } else if (key5 == 'matriculas') {
                                                let matriculasRecurrentes = [],
                                                    tmpMatriculas = tmpGarantias[key].configuracion[key3].matriculas;
                                                for (let key6 in tmpMatriculas) {
                                                    for (let key7 in tmpConfiguracionActual[key4].matriculas) {
                                                        if (tmpMatriculas[key6].id = tmpConfiguracionActual[key4].matriculas[key7].id) {
                                                            matriculasRecurrentes.push(tmpMatriculas[key6].id);
                                                            if (tmpMatriculas[key6].matricula != tmpConfiguracionActual[key4].matriculas[key7].matricula) {
                                                                this.datosGuardar.garantias[key2].configuracion[key4].matriculas[key7].logs = {
                                                                    accion: "Guardar",
                                                                    descripcion: "Edición de matrícula",
                                                                    detalles: {
                                                                        campo: 'matrícula',
                                                                        valorAnterior: tmpMatriculas[key6].matricula,
                                                                        nuevoValor: tmpConfiguracionActual[key4].matriculas[key7].matricula
                                                                    }
                                                                }
                                                                existenCambios = true;
                                                            }
                                                        }
                                                    }
                                                }

                                                let indicesMatriculas = Object.keys(tmpConfiguracionActual[key4].matriculas),
                                                    tmpCnt = 0;

                                                indicesMatriculas.sort(function (a, b) {
                                                    return a - b;
                                                });

                                                tmpCnt = indicesMatriculas[indicesMatriculas.length - 1];

                                                for (let key6 in tmpMatriculas) {
                                                    if (!matriculasRecurrentes.includes(tmpMatriculas[key6].id)) {
                                                        tmpCnt++;
                                                        this.datosGuardar.garantias[key2].configuracion[key4].matriculas[tmpCnt] = {
                                                            id: tmpMatriculas[key6].id,
                                                            logs: {
                                                                accion: "Eliminar",
                                                                descripcion: "Eliminar matrícula"
                                                            }
                                                        }
                                                        existenCambios = true;
                                                    }
                                                }

                                                for (let key6 in tmpConfiguracionActual[key4].matriculas) {
                                                    if (tmpConfiguracionActual[key4].matriculas[key6].id == 0 && !tmpConfiguracionActual[key4].matriculas[key6].logs) {
                                                        this.datosGuardar.garantias[key2].configuracion[key4].matriculas[key6].logs = {
                                                            accion: "Guardar",
                                                            descripcion: "Creación de matrícula",
                                                        }
                                                        existenCambios = true;
                                                    }
                                                }
                                            } else if (key5 == 'modificaciones') {
                                                let modificacionesRecurrentes = [],
                                                    tmpModificaciones = tmpGarantias[key].configuracion[key3].modificaciones;
                                                for (let key6 in tmpModificaciones) {
                                                    for (let key7 in tmpConfiguracionActual[key4].modificaciones) {
                                                        if (tmpModificaciones[key6].id == tmpConfiguracionActual[key4].modificaciones[key7].id) {
                                                            modificacionesRecurrentes.push(tmpModificaciones[key6].id);
                                                            let detalles = {},
                                                                tmpCnt = 0;
                                                            for (let key8 in tmpModificaciones[key6]) {
                                                                let valorAnterior = tmpModificaciones[key6][key8],
                                                                    nuevoValor = tmpConfiguracionActual[key4].modificaciones[key7][key8];

                                                                (valorAnterior.length > 0 && !isNaN(valorAnterior)) && (valorAnterior = parseFloat(valorAnterior));
                                                                (nuevoValor.length > 0 && !isNaN(nuevoValor)) && (nuevoValor = parseFloat(nuevoValor));
                                                                if (valorAnterior != nuevoValor) {
                                                                    tmpCnt++;
                                                                    detalles[tmpCnt] = {
                                                                        campo: key8,
                                                                        valorAnterior,
                                                                        nuevoValor
                                                                    }
                                                                }
                                                            }

                                                            if (Object.keys(detalles).length > 0) {
                                                                this.datosGuardar.garantias[key2].configuracion[key4].modificaciones[key7].logs = {
                                                                    accion: "Guardar",
                                                                    descripcion: "Edición de modificación de hipoteca",
                                                                    detalles
                                                                }
                                                                existenCambios = true;
                                                            }
                                                        }
                                                    }
                                                }

                                                let indicesModificaciones = Object.keys(tmpConfiguracionActual[key4].modificaciones),
                                                    tmpCnt = 0;

                                                indicesModificaciones.sort(function (a, b) {
                                                    return a - b;
                                                });

                                                tmpCnt = indicesModificaciones[indicesModificaciones.length - 1];

                                                for (let key6 in tmpModificaciones) {
                                                    if (!modificacionesRecurrentes.includes(tmpModificaciones[key6].id)) {
                                                        tmpCnt++;
                                                        this.datosGuardar.garantias[key2].configuracion[key4].modificaciones[tmpCnt] = {
                                                            id: tmpModificaciones[key6].id,
                                                            logs: {
                                                                accion: "Eliminar",
                                                                descripcion: "Eliminación de modificación de hipoteca"
                                                            }
                                                        }
                                                        existenCambios = true;
                                                    }
                                                }

                                                for (let key6 in tmpConfiguracionActual[key4].modificaciones) {
                                                    if (tmpConfiguracionActual[key4].modificaciones[key6].id == 0 && !tmpConfiguracionActual[key4].modificaciones[key6].logs) {
                                                        this.datosGuardar.garantias[key2].configuracion[key4].modificaciones[key6].logs = {
                                                            accion: "Guardar",
                                                            descripcion: "Creación de modificación de hipoteca"
                                                        }
                                                        existenCambios = true;
                                                    }
                                                }
                                            }
                                        }
                                        if (Object.keys(detalles).length > 0) {
                                            let descripcion = 'Edición de parámetros de garantía';
                                            switch (this.datosGuardar.garantias[key2].idTipoGarantia) {
                                                case "5":
                                                    descripcion = 'Edición de datos de fiador';
                                                    break;
                                                case "6":
                                                    descripcion = 'Edición de datos de hipoteca';
                                                    break;
                                            }
                                            this.datosGuardar.garantias[key2].configuracion[key4].logs = {
                                                accion: "Guardar",
                                                descripcion,
                                                detalles
                                            }
                                            existenCambios = true;
                                        }
                                    }
                                }
                            }

                            let indicesActuales = Object.keys(this.datosGuardar.garantias[key2].configuracion),
                                tmpCnt = 0;
                            indicesActuales.sort(function (a, b) {
                                return a - b;
                            });

                            tmpCnt = indicesActuales[indicesActuales.length - 1];

                            for (let key3 in tmpGarantias[key].configuracion) {
                                if (!configuracionRecurrente.includes(tmpGarantias[key].configuracion[key3].id)) {
                                    let descripcion = 'Eliminación de parámetros de garantía';
                                    switch (this.datosGuardar.garantias[key2].idTipoGarantia) {
                                        case "5":
                                            descripcion = 'Eliminación de datos de fiador';
                                            break;
                                        case "6":
                                            descripcion = 'Eliminación de datos de hipoteca';
                                            break;
                                    }
                                    tmpCnt++;
                                    this.datosGuardar.garantias[key2].configuracion[tmpCnt] = {
                                        id: tmpGarantias[key].configuracion[key3].id,
                                        logs: {
                                            accion: "Eliminar",
                                            descripcion,
                                        }
                                    }
                                    existenCambios = true;
                                }
                            }

                            for (let key3 in tmpConfiguracionActual) {
                                if (tmpConfiguracionActual[key3].id == 0 && !tmpConfiguracionActual[key3].logs) {
                                    this.datosGuardar.garantias[key2].configuracion[key3].logs = {
                                        accion: "Guardar",
                                        descripcion: "Creación de datos"
                                    }
                                    existenCambios = true;
                                }
                            }
                        }
                    }
                }
            }

            for (let key in tmpGarantias) {
                if (!garantiasRecurrentes.includes(tmpGarantias[key].idTipoGarantia)) {
                    configGarantia.cnt++;
                    this.datosGuardar.garantias[configGarantia.cnt] = {
                        id: tmpGarantias[key].id,
                        logs: {
                            accion: "Eliminar",
                            descripcion: "Eliminar tipo de garantía"
                        }
                    }
                    existenCambios = true;
                }
            }

            for (let key in this.datosGuardar.garantias) {
                if (this.datosGuardar.garantias[key].id == 0 && !this.datosGuardar.garantias[key].logs) {
                    this.datosGuardar.garantias[key].logs = {
                        accion: "Guardar",
                        descripcion: "Creación de garantía"
                    }
                    existenCambios = true;
                }
            }
        }

        return existenCambios;
    }
}

const configDatosDeudor = {
    id: 0,
    datosOriginales: {},
    campoCodigoCliente: document.getElementById("txtCodigoCliente"),
    cboTipoDocumento: document.getElementById("cboTipoDocumentoDeudor"),
    campoNumeroDocumento: document.getElementById("txtNumeroDocumentoDeudor"),
    campoNIT: document.getElementById("txtNITDeudor"),
    campoNombres: document.getElementById("txtNombresDeudor"),
    campoApellidos: document.getElementById("txtApellidosDeudor"),
    campoConocido: document.getElementById("txtConocidoDeudor"),
    cboGenero: document.getElementById("cboGeneroDeudor"),
    cboPais: document.getElementById("cboPaisDeudor"),
    cboDepartamento: document.getElementById("cboDepartamentoDeudor"),
    cboMunicipio: document.getElementById("cboMunicipioDeudor"),
    campoProfesion: document.getElementById("txtProfesionDeudor"),
    campoFechaNacimiento: document.getElementById("txtFechaNacimientoDeudor"),
    campoEdad: document.getElementById("numEdadDeudor"),
    cboFirma: document.getElementById("cboFirmaDeudor"),
    init: function (tmpDatos) {
        let allSelectpickerCbos = document.querySelectorAll('#deudor select.selectpicker');
        this.campoCodigoCliente.value = tmpDatos.codigoCliente;
        this.cboTipoDocumento.value = tmpDatos.tipoDocumento;
        $("#" + this.cboTipoDocumento.id).trigger("change");
        this.campoNumeroDocumento.value = tmpDatos.numeroDocumento;
        this.campoNIT.value = tmpDatos.nit;
        this.campoNombres.value = tmpDatos.nombres;
        this.campoApellidos.value = tmpDatos.apellidos;
        this.campoConocido.value = tmpDatos.conocido;
        this.cboGenero.value = tmpDatos.genero;
        this.cboPais.value = tmpDatos.pais;
        $("#" + this.cboPais.id).trigger("change");
        this.cboDepartamento.value = tmpDatos.departamento;
        $("#" + this.cboDepartamento.id).trigger("change");
        this.cboMunicipio.value = tmpDatos.municipio;
        this.campoProfesion.value = tmpDatos.profesion;
        this.campoFechaNacimiento.value = tmpDatos.fechaNacimiento;
        $("#" + this.campoFechaNacimiento.id).trigger("change");
        this.cboFirma.value = tmpDatos.sabeFirmar;

        allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
        configMutuo.evalFirma();
        this.id = tmpDatos.id;
        this.datosOriginales = {
            ...this.getJSON()
        };
    },
    getJSON: function () {
        let datos = {
            id: this.id,
            codigoCliente: this.campoCodigoCliente.value,
            tipoDocumento: this.cboTipoDocumento.value,
            labelTipoDocumento: this.cboTipoDocumento.options[this.cboTipoDocumento.options.selectedIndex].text,
            numeroDocumento: this.campoNumeroDocumento.value,
            nit: this.campoNIT.value,
            nombres: this.campoNombres.value,
            apellidos: this.campoApellidos.value,
            conocido: this.campoConocido.value,
            genero: this.cboGenero.value,
            labelGenero: this.cboGenero.options[this.cboGenero.options.selectedIndex].text,
            pais: this.cboPais.value,
            labelPais: this.cboPais.options[this.cboPais.options.selectedIndex].text,
            departamento: this.cboDepartamento.value,
            labelDepartamento: this.cboDepartamento.options[this.cboDepartamento.options.selectedIndex].text,
            municipio: this.cboMunicipio.value,
            labelMunicipio: this.cboMunicipio.options[this.cboMunicipio.options.selectedIndex].text,
            profesion: this.campoProfesion.value,
            fechaNacimiento: this.campoFechaNacimiento.value,
            edad: this.campoEdad.value,
            firma: this.cboFirma.value
        };

        return datos;
    }
}

const configDatosCredito = {
    datosOriginales: {},
    cboAgencia: document.getElementById("cboAgencia"),
    campoNumeroCredito: document.getElementById("txtNumeroCredito"),
    campoMonto: document.getElementById("numMontoCredito"),
    campoTasaInteres: document.getElementById("numTasaInteres"),
    cboLineaFinanciera: document.getElementById("cboLineaFinanciera"),
    campoDestino: document.getElementById("txtDestinoCredito"),
    cboVencimiento: document.getElementById("cboVencimiento"),
    cboPeriodicidad: document.getElementById("cboPeriodicidad"),
    campoPlazo: document.getElementById("numPlazo"),
    cboPlazo: document.getElementById("cboPlazo"),
    campoFechaGeneracion: document.getElementById("txtFechaGeneracion"),
    cboFuenteFondos: document.getElementById("cboFuenteFondos"),
    cboEmpleado: document.getElementById("cboEmpleadoAcacypac"),
    cboDesembolsoParcial: document.getElementById("cboDesembolsoParcial"),
    cboLineaRotativa: document.getElementById("cboLineaRotativa"),
    campoFechaVencimiento: document.getElementById("txtFechaVencimiento"),
    init: function (tmpDatos) {
        let allSelectpickerCbos = document.querySelectorAll('#credito select.selectpicker');
        // console.log(tmpDatos);
        this.cboAgencia.value = tmpDatos.agencia;
        this.campoNumeroCredito.value = tmpDatos.numeroCredito;
        this.campoMonto.value = tmpDatos.monto;
        this.campoTasaInteres.value = tmpDatos.tasaInteres;
        this.cboLineaFinanciera.value = tmpDatos.lineaFinanciera;
        this.campoDestino.value = tmpDatos.destino;
        this.cboVencimiento.value = tmpDatos.alVencimiento;
        this.cboPeriodicidad.value = tmpDatos.periodicidad;
        this.campoPlazo.value = tmpDatos.plazo;
        this.cboPlazo.value = tmpDatos.tipoPlazo;
        this.campoFechaGeneracion.value = tmpDatos.fechaGeneracion;
        this.cboFuenteFondos.value = tmpDatos.fuenteFondos;
        this.cboEmpleado.value = tmpDatos.empleadoACACYPAC;
        this.cboDesembolsoParcial.value = tmpDatos.desembolsoParcial;
        $("#" + this.cboDesembolsoParcial.id).trigger("change");
        this.cboLineaRotativa.value = tmpDatos.lineaRotativa;
        $("#" + this.cboLineaRotativa.id).trigger('change');
        this.campoFechaVencimiento.value = tmpDatos.fechaVencimiento;
        this.configCuota.capitalInteres.value = tmpDatos.capitalInteres;

        tmpDatos.aditivos.forEach(aditivo => {
            this.configCuota.configAditivos.cnt++;
            this.configCuota.configAditivos.aditivos[this.configCuota.configAditivos.cnt] = {
                id: aditivo.id,
                idAditivo: aditivo.idAditivo,
                aditivo: this.configCuota.configAditivos.catalogo[aditivo.idAditivo].aditivo,
                monto: aditivo.monto
            };
            this.configCuota.configAditivos.addRowTbl({
                ...this.configCuota.configAditivos.aditivos[this.configCuota.configAditivos.cnt],
                nFila: this.configCuota.configAditivos.cnt
            });
        });
        tblAditivos.columns.adjust().draw();
        this.configCuota.calcularCuota();

        if (tmpDatos.desembolsoParcial == 'S') {
            if (tmpDatos.parciales) {
                configDesembolsos.init(tmpDatos.parciales);
            }
        }

        allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
        this.datosOriginales = JSON.parse(JSON.stringify(this.getJSON()));
    },
    getJSON: function () {
        let datos = {
            agencia: this.cboAgencia.value,
            labelAgencia: this.cboAgencia.options[this.cboAgencia.options.selectedIndex].text,
            numeroCredito: this.campoNumeroCredito.value,
            monto: this.campoMonto.value,
            tasaInteres: this.campoTasaInteres.value,
            capitalInteres: this.configCuota.capitalInteres.value,
            aditivos: JSON.parse(JSON.stringify(this.configCuota.configAditivos.aditivos)),
            totalCuota: this.configCuota.campoTotalCuota.value,
            lineaFinanciera: this.cboLineaFinanciera.value,
            labelLineaFinanciera: this.cboLineaFinanciera.options[this.cboLineaFinanciera.options.selectedIndex].text,
            destino: this.campoDestino.value,
            vencimiento: this.cboVencimiento.value,
            periodicidad: this.cboPeriodicidad.value,
            labelPeriodicidad: this.cboPeriodicidad.options[this.cboPeriodicidad.options.selectedIndex].text,
            plazo: this.campoPlazo.value,
            tipoPlazo: this.cboPlazo.value,
            labelTipoPlazo: this.cboPlazo.options[this.cboPlazo.options.selectedIndex].text,
            fechaGeneracion: this.campoFechaGeneracion.value,
            fuenteFondos: this.cboFuenteFondos.value,
            labelFuenteFondos: this.cboFuenteFondos.options[this.cboFuenteFondos.options.selectedIndex].text,
            empleadoACACYPAC: this.cboEmpleado.value,
            desembolsoParcial: this.cboDesembolsoParcial.value,
            detalleDesembolsos: JSON.parse(JSON.stringify(configDesembolsos.desembolsos)),
            lineaRotativa: this.cboLineaRotativa.value,
            fechaVencimiento: this.campoFechaVencimiento.value
        };

        return datos;
    },
    configCuota: {
        campoTotalCuota: document.getElementById("numMontoTotal"),
        capitalInteres: document.getElementById("numCapitalInteres"),
        configAditivos: {
            catalogo: {},
            aditivos: {},
            id: 0,
            cnt: 0,
            cboAditivo: document.getElementById("cboAditivo"),
            montoAditivo: document.getElementById("numMontoAditivo"),
            mdlTitle: document.querySelector("#mdlAditivo h5.modal-title"),
            init: function (tmpDatos) {
                return new Promise((resolve, reject) => {

                });
            },
            edit: function (id = 0) {
                formActions.clean("mdlAditivo").then(() => {
                    const leyendo = new Promise((resolve, reject) => {
                        try {
                            let opciones = [`<option selected disables value="">seleccione</option>`],
                                incluidos = [];
                            this.id = id;
                            for (let key in this.aditivos) {
                                if (!this.aditivos[id] || this.aditivos[key].idAditivo != this.aditivos[id].idAditivo) {
                                    incluidos.push(this.aditivos[key].idAditivo);
                                }
                            }
                            for (let key in this.catalogo) {
                                if (!incluidos.includes(this.catalogo[key].id)) {
                                    opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].aditivo}</option>`);
                                }
                            }

                            this.cboAditivo.innerHTML = opciones.join("");
                            this.cboAditivo.disabled = false;

                            if (id > 0) {
                                this.cboAditivo.value = this.aditivos[id].idAditivo;
                                this.montoAditivo.value = this.aditivos[id].monto;
                                this.cboAditivo.disabled = true;
                                resolve();
                            } else {
                                resolve();
                            }
                        } catch (err) {
                            reject(err.message);
                        }
                    });

                    leyendo.then(() => {
                        $("#" + this.cboAditivo.id).selectpicker("refresh");
                        $("#mdlAditivo").modal("show");
                    }).catch(generalMostrarError);
                });
            },
            save: function () {
                formActions.validate('frmAditivo').then(({
                    errores
                }) => {
                    if (errores == 0) {
                        let tmp = {
                            id: this.id > 0 ? this.aditivos[this.id].id : 0,
                            idAditivo: this.cboAditivo.value,
                            aditivo: this.cboAditivo.options[this.cboAditivo.options.selectedIndex].text,
                            monto: this.montoAditivo.value
                        }
                        let guardando = new Promise((resolve, reject) => {
                            if (this.id == 0) {
                                this.cnt++;
                                this.id = this.cnt;
                                this.addRowTbl({
                                    ...tmp,
                                    nFila: this.cnt
                                }).then(resolve).catch(reject);
                            } else {
                                $("#trAditivo" + this.id).find("td:eq(1)").html(this.catalogo[this.cboAditivo.value].aditivo);
                                $("#trAditivo" + this.id).find("td:eq(2)").html("$" + parseFloat(this.montoAditivo.value).toFixed(2));
                                resolve();
                            }
                        });
                        guardando.then(() => {
                            this.aditivos[this.id] = {
                                ...tmp
                            };
                            tblAditivos.columns.adjust().draw();
                            $("#mdlAditivo").modal("hide");
                            configDatosCredito.configCuota.calcularCuota();
                        }).catch(generalMostrarError);
                    }
                }).catch(generalMostrarError);
            },
            addRowTbl: function ({
                nFila,
                idAditivo,
                aditivo,
                monto
            }) {
                return new Promise((resolve, reject) => {
                    try {
                        let botones = "<div class='tblButtonContainer'>" +
                            "<span class='btnTbl' title='Editar' onclick='configDatosCredito.configCuota.configAditivos.edit(" + nFila + ");'>" +
                            "<i class='fas fa-edit'></i>" +
                            "</span>" +
                            "<span class='btnTbl' title='Eliminar' onclick='configDatosCredito.configCuota.configAditivos.delete(" + nFila + ");'>" +
                            "<i class='fas fa-trash'></i>" +
                            "</span>" +
                            "</div>";
                        botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                        tblAditivos.row.add([
                            nFila,
                            aditivo,
                            "$" + parseFloat(monto).toFixed(2),
                            botones
                        ]).node().id = "trAditivo" + nFila;
                        resolve();
                    } catch (err) {
                        reject(err.message);
                    }
                });
            },
            delete: function (id) {
                let aditivoLabel = this.aditivos[id].aditivo;
                Swal.fire({
                    title: 'Eliminar aditivo',
                    icon: 'warning',
                    html: '¿Seguro/a que quieres <b>eliminar</b> el aditivo ' + aditivoLabel + '?.',
                    showCloseButton: false,
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
                    cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
                }).then((result) => {
                    if (result.isConfirmed) {
                        tblAditivos.row('#trAditivo' + id).remove().draw();
                        delete this.aditivos[id];
                        configDatosCredito.configCuota.calcularCuota();
                    }
                });
            }
        },
        calcularCuota: function () {
            let totalCuota = 0;
            this.capitalInteres.value && (totalCuota += parseFloat(this.capitalInteres.value));
            for (let key in this.configAditivos.aditivos) {
                totalCuota += parseFloat(this.configAditivos.aditivos[key].monto);
            }

            this.campoTotalCuota.value = totalCuota.toFixed(2);
        }
    },
    evalRotativa: function (opcion) {
        this.campoFechaVencimiento.value = '';
        $("#" + this.campoFechaVencimiento.id).datepicker("update", "");
        if (opcion == 'S') {
            this.campoFechaVencimiento.disabled = false;
        } else {
            this.campoFechaVencimiento.disabled = true;
        }
    }
}

const configGarantia = {
    id: 0,
    cnt: 0,
    catalogo: {},
    garantias: {},
    datosOriginales: {},
    cboTipoGarantia: document.getElementById("cboTipoGarantia"),
    init: function (tmpDatos) {
        tmpDatos.forEach(garantia => {
            this.cnt++;
            let configuracion = {};

            switch (garantia.idTipoGarantia) {
                case '2':
                    configuracion = {
                        id: garantia.configuracion.id,
                        cuenta: garantia.configuracion.cuenta,
                        porcentaje: garantia.configuracion.porcentaje
                    };
                    break;
                case '3':
                    configuracion = {
                        id: garantia.configuracion.id,
                        cuenta: garantia.configuracion.cuenta,
                        certificado: garantia.configuracion.certificado,
                        monto: garantia.configuracion.monto,
                        fechaCreacion: garantia.configuracion.fechaCreacion,
                        fechaVencimiento: garantia.configuracion.fechaVencimiento,
                        porcentaje: garantia.configuracion.porcentaje
                    };
                    break;
                case '5':
                    garantia.configuracion.forEach(fiador => {
                        configFiduciaria.cnt++;
                        let labelDepartamento = '',
                            labelMunicipio = '';
                        datosGeograficos[fiador.pais].departamentos.forEach(departamento => {
                            if (departamento.id == fiador.departamento) {
                                labelDepartamento = departamento.nombreDepartamento;
                                departamento.municipios.forEach(municipio => {
                                    if (municipio.id == fiador.municipio) {
                                        labelMunicipio = municipio.nombreMunicipio;
                                    }
                                });
                            }
                        });
                        configuracion[configFiduciaria.cnt] = {
                            id: fiador.id,
                            tipoDocumento: fiador.tipoDocumento,
                            labelTipoDocumento: tiposDocumento[fiador.tipoDocumento].tipoDocumento,
                            numeroDocumento: fiador.numeroDocumento,
                            nit: fiador.nit,
                            nombres: fiador.nombres,
                            apellidos: fiador.apellidos,
                            conocido: fiador.conocido,
                            genero: fiador.genero,
                            labelGenero: fiador.genero == 'M' ? 'Masculino' : 'Femenino',
                            pais: fiador.pais,
                            labelPais: datosGeograficos[fiador.pais].nombrePais,
                            departamento: fiador.departamento,
                            labelDepartamento,
                            municipio: fiador.municipio,
                            labelMunicipio,
                            profesion: fiador.profesion,
                            fechaNacimiento: fiador.fechaNacimiento,
                            edad: fiador.edad,
                            firma: fiador.sabeFirmar,
                            labelFirma: fiador.sabeFirmar == 'S' ? 'SI' : 'NO'
                        }

                        configFiduciaria.addRowTbl({
                            ...configuracion[configFiduciaria.cnt],
                            nFila: configFiduciaria.cnt
                        });
                    });

                    tblFiadores.columns.adjust().draw();
                    break;
                case '6':
                    garantia.configuracion.forEach(hipoteca => {
                        let matriculas = {},
                            modificaciones = {};
                        for (let key in hipoteca.matriculas) {
                            matriculas[(parseInt(key) + 1)] = hipoteca.matriculas[key];
                        }
                        configHipotecaria.cnt++;

                        for (let key in hipoteca.modificaciones) {
                            modificaciones[(key + 1)] = {
                                id: hipoteca.modificaciones[key].id,
                                fecha: hipoteca.modificaciones[key].fecha,
                                abogado: hipoteca.modificaciones[key].abogado,
                                generoAbogado: hipoteca.modificaciones[key].generoAbogado,
                                labelGeneroAbogado: hipoteca.modificaciones[key].generoAbogado == 'M' ? 'Masculino' : 'Femenino',
                                asiento: hipoteca.modificaciones[key].asiento,
                                monto: hipoteca.modificaciones[key].monto,
                                plazo: hipoteca.modificaciones[key].plazo
                            };
                        }
                        configuracion[configHipotecaria.cnt] = {
                            id: hipoteca.id,
                            fechaEmision: hipoteca.fechaEmision,
                            abogado: hipoteca.abogado,
                            generoAbogado: hipoteca.generoAbogado,
                            labelGeneroAbogado: hipoteca.generoAbogado == 'M' ? 'Masculino' : 'Femenino',
                            asiento: hipoteca.asiento,
                            monto: hipoteca.monto,
                            plazo: hipoteca.plazo,
                            zona: hipoteca.zona,
                            labelZona: configHipotecaria.zonas[hipoteca.zona].zona,
                            seccion: hipoteca.seccion,
                            labelSeccion: configHipotecaria.zonas[hipoteca.zona].secciones[hipoteca.seccion].seccion,
                            departamento: configHipotecaria.zonas[hipoteca.zona].secciones[hipoteca.seccion].departamento,
                            presenta: hipoteca.presentadaPor,
                            matriculas,
                            modificaciones
                        }

                        configHipotecaria.addRowTbl({
                            ...configuracion[configHipotecaria.cnt],
                            nFila: configHipotecaria.cnt
                        });
                    });

                    tblHipotecas.columns.adjust().draw();
                    break;
            }

            this.garantias[this.cnt] = {
                id: garantia.id,
                idTipoGarantia: garantia.idTipoGarantia,
                tipoGarantia: this.catalogo[garantia.idTipoGarantia].tipoGarantia,
                configurada: 'SI',
                configuracion
            };

            configGarantia.id = this.cnt;
            configFiduciaria.evalFiadoresHipoteca();
            configMutuo.evalFirma();
            configGarantia.datosOriginales[this.cnt] = JSON.parse(JSON.stringify({
                id: garantia.id,
                idTipoGarantia: garantia.idTipoGarantia,
                tipoGarantia: this.catalogo[garantia.idTipoGarantia].tipoGarantia,
                configurada: 'SI',
                configuracion: {
                    ...configuracion
                }
            }));

            this.addRowTbl({
                ...this.garantias[this.cnt],
                nFila: this.cnt
            });
        });

        tblGarantias.columns.adjust().draw();
    },
    edit: function (id = 0) {
        formActions.clean('mdlGarantia').then(() => {
            this.id = id;
            let incluidos = [],
                opciones = [`<option selected disabled value="">seleccione</option>`];
            for (let key in this.garantias) {
                incluidos.push(this.garantias[key].idTipoGarantia);
            }
            for (let key in this.catalogo) {
                if (!incluidos.includes(this.catalogo[key].id)) {
                    opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].tipoGarantia}</option>`);
                }
            }
            this.cboTipoGarantia.innerHTML = opciones.join("");
            $("#" + this.cboTipoGarantia.id).selectpicker("refresh");
            $("#mdlGarantia").modal("show");
        }).catch(generalMostrarError);
    },
    save: function () {
        formActions.validate('frmGarantia').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.garantias[this.id].id : 0,
                    idTipoGarantia: this.cboTipoGarantia.value,
                    tipoGarantia: this.cboTipoGarantia.options[this.cboTipoGarantia.options.selectedIndex].text,
                    configurada: "NO",
                    configuracion: this.id > 0 ? this.garantias[this.id].configuracion : {}
                }
                let guardando = new Promise((resolve, reject) => {
                    try {
                        if (this.id > 0) {
                            $("#trGarantia" + this.id).find("td:eq(1)").html(tmp.tipoGarantia);
                            resolve();
                        } else {
                            this.cnt++;
                            this.id = this.cnt;
                            this.addRowTbl({
                                ...tmp,
                                nFila: this.cnt
                            }).then(resolve).catch(reject);
                        }
                    } catch (err) {
                        reject(err.message);
                    }
                });

                guardando.then(() => {
                    this.garantias[this.id] = {
                        ...tmp
                    };
                    tblGarantias.columns.adjust().draw();
                    $("#mdlGarantia").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        tipoGarantia,
        configurada,
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Configurar' onclick='configGarantia.config(" + nFila + ");'>" +
                    "<i class='fas fa-cogs'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configGarantia.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblGarantias.row.add([
                    nFila,
                    tipoGarantia,
                    '',
                    configurada,
                    botones
                ]).node().id = "trGarantia" + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id) {
        let labelTipoGarantia = this.garantias[id].tipoGarantia,
            tmpTipo = this.garantias[id].idTipoGarantia;;
        Swal.fire({
            title: 'Eliminar garantía',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la garantia de <strong>' + labelTipoGarantia + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblGarantias.row('#trGarantia' + id).remove().draw();
                delete this.garantias[id];
                switch (tmpTipo) {
                    case "5":
                        tblFiadores.clear().draw();
                        configFiduciaria.evalFiadoresHipoteca();
                        break;
                    case "6":
                        tblHipotecas.clear().draw();
                        break;
                }
            }
        });
    },
    config: function (id = 0) {
        let idTipoGarantia = this.garantias[id].idTipoGarantia;
        this.id = id;

        switch (idTipoGarantia) {
            case "1":
                configSinFiador.edit();
                break;
            case "2":
                configAportaciones.edit();
                break;
            case "3":
                configCDP.edit();
                break;
            case "4":
                configOID.edit();
                break;
            case "5":
                configFiduciaria.showScreen();
                break;
            case "6":
                configHipotecaria.showScreen();
                break;
            case "7":
                configFSG.edit();
                break;
            case "8":
                configProgara.edit();
                break;
            default:
                Swal.fire({
                    title: "Error",
                    html: "Ups! Parece que no se pudo identificar el tipo de garantía",
                    icon: "warning"
                });
                break;
        }
    }
}

const configSinFiador = {
    cnt: 0,
    id: 0,
    init: function () {},
    edit: function (id = 0) {
        this.id = id;
        Swal.fire({
            title: "",
            html: "Este tipo de garantía no requiere configuración",
            icon: "info"
        });
    }
}

const configFiduciaria = {
    cnt: 0,
    id: 0,
    datosOriginales: {},
    cboTipoDocumento: document.getElementById("cboTipoDocumentoFiador"),
    campoDocumento: document.getElementById("txtNumeroDocumentoFiador"),
    campoNIT: document.getElementById("txtNITFiador"),
    campoNombres: document.getElementById("txtNombresFiador"),
    campoApellidos: document.getElementById("txtApellidosFiador"),
    campoConocido: document.getElementById("txtConocidoFiador"),
    cboGenero: document.getElementById("cboGeneroFiador"),
    cboPais: document.getElementById("cboPaisFiador"),
    cboDepartamento: document.getElementById("cboDepartamentoFiador"),
    cboMunicipio: document.getElementById("cboMunicipioFiador"),
    campoProfesion: document.getElementById("txtProfesionFiador"),
    campoFechaNacimiento: document.getElementById("txtFechaNacimientoFiador"),
    campoEdad: document.getElementById("numEdadFiador"),
    cboFirma: document.getElementById("cboFirmaFiador"),
    cboPresentada: document.getElementById('cboPresentaHipoteca'),
    showScreen: function () {
        sgaShowScreen('principalContainer', 'fiadoresContainer').then(() => {
            tblFiadores.columns.adjust().draw();
        });
    },
    init: function () {},
    edit: function (id = 0) {
        formActions.clean('frmFiador').then(() => {
            this.id = id;
            let allSelectpickerCbos = document.querySelectorAll("#frmFiador select.selectpicker");
            if (this.id > 0) {
                let tmp = configGarantia.garantias[configGarantia.id].configuracion[this.id];

                this.cboTipoDocumento.value = tmp.tipoDocumento;
                this.campoDocumento.value = tmp.numeroDocumento;
                this.campoNIT.value = tmp.nit;
                this.campoNombres.value = tmp.nombres;
                this.campoApellidos.value = tmp.apellidos;
                this.campoConocido.value = tmp.conocido;
                this.cboGenero.value = tmp.genero;
                this.cboPais.value = tmp.pais;
                $("#" + this.cboPais.id).trigger("change");
                this.cboDepartamento.value = tmp.departamento;
                $("#" + this.cboDepartamento.id).trigger("change");
                this.cboMunicipio.value = tmp.municipio;
                this.campoProfesion.value = tmp.profesion;
                this.campoFechaNacimiento.value = tmp.fechaNacimiento;
                this.campoEdad.value = tmp.edad;
                this.cboFirma.value = tmp.firma;
            }

            allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));

            $("#mdlFiador").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmFiador').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? configGarantia.garantias[configGarantia.id].configuracion[this.id].id : 0,
                    tipoDocumento: this.cboTipoDocumento.value,
                    labelTipoDocumento: this.cboTipoDocumento.options[this.cboTipoDocumento.options.selectedIndex].text,
                    numeroDocumento: this.campoDocumento.value,
                    nit: this.campoNIT.value,
                    nombres: this.campoNombres.value,
                    apellidos: this.campoApellidos.value,
                    conocido: this.campoConocido.value,
                    genero: this.cboGenero.value,
                    labelGenero: this.cboGenero.options[this.cboGenero.options.selectedIndex].text,
                    pais: this.cboPais.value,
                    labelPais: this.cboPais.options[this.cboPais.options.selectedIndex].text,
                    departamento: this.cboDepartamento.value,
                    labelDepartamento: this.cboDepartamento.options[this.cboDepartamento.options.selectedIndex].text,
                    municipio: this.cboMunicipio.value,
                    labelMunicipio: this.cboMunicipio.options[this.cboMunicipio.options.selectedIndex].text,
                    profesion: this.campoProfesion.value,
                    fechaNacimiento: this.campoFechaNacimiento.value,
                    edad: this.campoEdad.value,
                    firma: this.cboFirma.value,
                    labelFirma: this.cboFirma.options[this.cboFirma.options.selectedIndex].text
                };

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trFiador" + this.id).find("td:eq(1)").html(tmp.numeroDocumento);
                        $("#trFiador" + this.id).find("td:eq(2)").html(tmp.nit);
                        $("#trFiador" + this.id).find("td:eq(3)").html(tmp.nombres + ' ' + tmp.apellidos);
                        $("#trFiador" + this.id).find("td:eq(4)").html(tmp.edad);
                        $("#trFiador" + this.id).find("td:eq(5)").html(tmp.profesion);
                        $("#trFiador" + this.id).find("td:eq(6)").html(tmp.labelPais);
                        $("#trFiador" + this.id).find("td:eq(7)").html(tmp.labelDepartamento);
                        $("#trFiador" + this.id).find("td:eq(8)").html(tmp.labelMunicipio);
                        $("#trFiador" + this.id).find("td:eq(9)").html(tmp.labelFirma);
                        resolve();
                    }
                });

                guardar.then(() => {
                    configGarantia.garantias[configGarantia.id].configuracion[this.id] = {
                        ...tmp
                    };
                    tblFiadores.columns.adjust().draw();
                    tblFiadores.responsive.recalc();
                    $("#mdlFiador").modal("hide");
                    configFiduciaria.evalFiadoresHipoteca();
                    configMutuo.evalFirma();
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        numeroDocumento,
        nit,
        nombres,
        apellidos,
        edad,
        profesion,
        labelPais,
        labelDepartamento,
        labelMunicipio,
        labelFirma,
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configFiduciaria.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configFiduciaria.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblFiadores.row.add([
                    nFila,
                    numeroDocumento,
                    nit,
                    `${nombres} ${apellidos}`,
                    edad,
                    profesion,
                    labelPais,
                    labelDepartamento,
                    labelMunicipio,
                    labelFirma,
                    botones
                ]).node().id = 'trFiador' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let granted = true,
            tmp = {};

        for (let key in configGarantia.garantias) {
            if (configGarantia.garantias[key].idTipoGarantia == '6') {
                tmp = {
                    ...configGarantia.garantias[key].configuracion
                };
            }
        }

        for (let key in tmp) {
            if (id == parseInt(tmp[key].presenta)) {
                granted = false;
            }
        }

        if (granted) {
            let tmp = configGarantia.garantias[configGarantia.id].configuracion[id],
                label = tmp.nombres + ' ' + tmp.apellidos;
            Swal.fire({
                title: 'Eliminar fiador',
                icon: 'warning',
                html: '¿Seguro/a que quieres <b>eliminar</b> los datos del fiador <strong>' + label + '</strong>?.',
                showCloseButton: false,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
                cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    tblFiadores.row('#trFiador' + id).remove().draw();
                    delete configGarantia.garantias[configGarantia.id].configuracion[id];
                    configFiduciaria.evalFiadoresHipoteca();
                    configMutuo.evalFirma();
                }
            });
        } else {
            Swal.fire({
                title: "¡Atención!",
                html: "No es posible eliminar al fiador, debido a que está asignado en una hipoteca o más. <br/>Deberás quitar la asignación para poder eliminarlo.",
                icon: "warning"
            });
        }
    },
    evalFiadoresHipoteca: function () {
        let opciones = [`<option selected disabled value="">seleccione</option>`, `<option value="deudor">Deudor/a</option>`];
        for (let key in configGarantia.garantias) {
            if (configGarantia.garantias[key].idTipoGarantia == '5') {
                let tmp = configGarantia.garantias[key].configuracion;
                for (let key2 in tmp) {
                    opciones.push(`<option value="${key2}">${tmp[key2].nombres} ${tmp[key2].apellidos}</option>`);
                }
            }
        }

        this.cboPresentada.innerHTML = opciones.join("");
        $("#" + this.cboPresentada.id).selectpicker("refresh");
    }
}

const configHipotecaria = {
    id: 0,
    cnt: 0,
    datosOriginales: {},
    tabHipotecas: document.getElementById("hipotecas-tab"),
    tabConfiguracion: document.getElementById("configuracionHipoteca-tab"),
    campoFechaEmision: document.getElementById('txtFechaEmisionHipoteca'),
    campoAbogado: document.getElementById('txtAbogadoHipoteca'),
    cboGeneroAbogado: document.getElementById('cboGeneroAbogado'),
    campoAsiento: document.getElementById('txtAsientoHipoteca'),
    campoMonto: document.getElementById('numMontoHipoteca'),
    campoPlazo: document.getElementById('numPlazoHipoteca'),
    cboZona: document.getElementById('cboZonaHipoteca'),
    cboSeccion: document.getElementById('cboSeccionHipoteca'),
    campoDepartamento: document.getElementById('txtDepartamentoHipoteca'),
    cboPresentada: document.getElementById('cboPresentaHipoteca'),
    campoMontoFinal: document.getElementById("numMontoFinalHipoteca"),
    campoPlazoFinal: document.getElementById("numPlazoFinalHipoteca"),
    zonas: {},
    showScreen: function () {
        sgaShowScreen('principalContainer', 'hipotecasContainer').then(() => {
            tblHipotecas.columns.adjust().draw();
        });
    },
    init: function () {},
    evalMonto: function () {
        let montoTotal = 0;

        configHipotecaria.campoMonto.value && (montoTotal += parseFloat(configHipotecaria.campoMonto.value));

        for (let key in configHipotecaria.configModificaciones.modificaciones) {
            configHipotecaria.configModificaciones.modificaciones[key].monto.length > 0 && (montoTotal += parseFloat(configHipotecaria.configModificaciones.modificaciones[key].monto));
        }

        configHipotecaria.campoMontoFinal.value = montoTotal.toFixed(2);
    },
    evalPlazo: function () {
        let plazoTotal = 0;

        configHipotecaria.campoPlazo.value && (plazoTotal += parseInt(configHipotecaria.campoPlazo.value));

        for (let key in configHipotecaria.configModificaciones.modificaciones) {
            configHipotecaria.configModificaciones.modificaciones[key].plazo.length > 0 && (plazoTotal += parseInt(configHipotecaria.configModificaciones.modificaciones[key].plazo));
        }

        configHipotecaria.campoPlazoFinal.value = plazoTotal;
    },
    evalZona: function (idZona, idCboSeccion) {
        let opciones = [`<option selected disabled value="">seleccione</option>`];
        if (this.zonas[idZona]) {
            for (let key in this.zonas[idZona].secciones) {
                opciones.push(`<option value="${this.zonas[idZona].secciones[key].id}">${this.zonas[idZona].secciones[key].seccion}</option>`);
            }
        }

        document.getElementById(idCboSeccion).innerHTML = opciones.join("");
        document.getElementById(idCboSeccion).className.includes("selectpicker") && ($("#" + idCboSeccion).selectpicker("refresh"));
    },
    evalSeccion: function (idCboZona, idSeccion, idCampoDepartamento) {
        let departamento = '',
            idZona = document.getElementById(idCboZona).value;

        if (this.zonas[idZona]) {
            if (this.zonas[idZona].secciones[idSeccion]) {
                departamento = this.zonas[idZona].secciones[idSeccion].departamento;
            }
        }

        document.getElementById(idCampoDepartamento).value = departamento;
    },
    edit: function (id = 0) {
        formActions.clean('frmHipoteca').then(() => {
            this.id = id;
            tblMatriculas.clear().draw();
            tblModificacionesHipoteca.clear().draw();
            configHipotecaria.configMatricula.id = 0;
            configHipotecaria.configMatricula.cnt = 0;
            configHipotecaria.configMatricula.matriculas = {};
            configHipotecaria.configModificaciones.modificaciones = {};
            configHipotecaria.configModificaciones.id = 0;
            configHipotecaria.configModificaciones.cnt = 0;

            if (this.id > 0) {
                let tmp = configGarantia.garantias[configGarantia.id].configuracion[this.id],
                    idPresenta = 0;

                for (let a = 0; a < this.cboPresentada.options.length; a++) {
                    if (this.cboPresentada.options[a].text == tmp.presenta) {
                        idPresenta = this.cboPresentada.options[a].value;
                    }
                }
                this.campoFechaEmision.value = tmp.fechaEmision;
                $("#" + this.campoFechaEmision.id).datetimepicker("update", tmp.fechaEmision);
                this.campoAbogado.value = tmp.abogado;
                this.cboGeneroAbogado.value = tmp.generoAbogado;
                this.campoAsiento.value = tmp.asiento;
                this.campoMonto.value = tmp.monto;
                this.campoPlazo.value = tmp.plazo;
                this.cboZona.value = tmp.zona;
                $("#" + this.cboZona.id).trigger("change");
                this.cboSeccion.value = tmp.seccion;
                $("#" + this.cboSeccion.id).trigger("change");
                this.cboPresentada.value = idPresenta;
                $("#" + this.cboPresentada.id).selectpicker("refresh");

                this.configMatricula.init(tmp.matriculas);
                this.configModificaciones.init(tmp.modificaciones);
            }

            this.tabConfiguracion.classList.remove('disabled');
            $("#" + this.tabConfiguracion.id).trigger("click");
            this.tabHipotecas.classList.add("disabled");
        });
    },
    save: function () {
        formActions.validate('frmHipoteca').then(({
            errores
        }) => {
            if (errores == 0) {
                if (Object.keys(this.configMatricula.matriculas).length == 0) {
                    errores++;
                    toastr.warning("Debe ingresar al menos una matrícula");
                }
            }
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? configGarantia.garantias[configGarantia.id].configuracion[this.id].id : 0,
                    fechaEmision: this.campoFechaEmision.value,
                    abogado: this.campoAbogado.value,
                    generoAbogado: this.cboGeneroAbogado.value,
                    labelGeneroAbogado: this.cboGeneroAbogado.options[this.cboGeneroAbogado.options.selectedIndex].text,
                    asiento: this.campoAsiento.value,
                    monto: this.campoMonto.value,
                    plazo: this.campoPlazo.value,
                    zona: this.cboZona.value,
                    labelZona: this.cboZona.options[this.cboZona.options.selectedIndex].text,
                    seccion: this.cboSeccion.value,
                    labelSeccion: this.cboSeccion.options[this.cboSeccion.options.selectedIndex].text,
                    departamento: this.campoDepartamento.value,
                    presenta: this.cboPresentada.options[this.cboPresentada.options.selectedIndex].text,
                    matriculas: {
                        ...this.configMatricula.matriculas
                    },
                    modificaciones: {
                        ...this.configModificaciones.modificaciones
                    }
                }

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        let matriculasImp = [];
                        for (let key in tmp.matriculas) {
                            matriculasImp.push(tmp.matriculas[key].matricula);
                        }

                        let plazoImp = parseInt(tmp.plazo) > 1 ? tmp.plazo + ' años' : tmp.plazo + ' año';
                        $("#trHipoteca" + this.id).find("td:eq(1)").html(tmp.fechaEmision);
                        $("#trHipoteca" + this.id).find("td:eq(2)").html(tmp.abogado);
                        $("#trHipoteca" + this.id).find("td:eq(3)").html(matriculasImp.join(`<br />`));
                        $("#trHipoteca" + this.id).find("td:eq(4)").html(tmp.asiento);
                        $("#trHipoteca" + this.id).find("td:eq(5)").html("$" + new Intl.NumberFormat().format(parseFloat(tmp.monto).toFixed(2)));
                        $("#trHipoteca" + this.id).find("td:eq(6)").html(plazoImp);
                        $("#trHipoteca" + this.id).find("td:eq(7)").html(tmp.labelZona);
                        $("#trHipoteca" + this.id).find("td:eq(8)").html(tmp.labelSeccion);
                        $("#trHipoteca" + this.id).find("td:eq(9)").html(tmp.departamento);
                        resolve();
                    }
                });

                guardar.then(() => {
                    configGarantia.garantias[configGarantia.id].configuracion[this.id] = {
                        ...tmp
                    };
                    tblHipotecas.columns.adjust().draw();
                    this.showPrincipal();
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        fechaEmision,
        abogado,
        matriculas,
        asiento,
        monto,
        plazo,
        labelZona,
        labelSeccion,
        departamento,
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configHipotecaria.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configHipotecaria.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>",
                    matriculasImp = [];
                tipoPermiso != 'ESCRITURA' && (botones = '');

                let plazoImp = parseInt(plazo) > 1 ? plazo + ' años' : plazo + ' año';

                for (let key in matriculas) {
                    matriculasImp.push(matriculas[key].matricula);
                }
                tblHipotecas.row.add([
                    nFila,
                    fechaEmision,
                    abogado,
                    matriculasImp.join('<br />'),
                    asiento,
                    "$" + new Intl.NumberFormat().format(parseFloat(monto).toFixed(2)),
                    plazoImp,
                    labelZona,
                    labelSeccion,
                    departamento,
                    botones
                ]).node().id = 'trHipoteca' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: 'Eliminar hipoteca',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la hipoteca?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblHipotecas.row('#trHipoteca' + id).remove().draw();
                delete configGarantia.garantias[configGarantia.id].configuracion[id];
            }
        });
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar hipoteca',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Salir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                configHipotecaria.showPrincipal();
            }
        });
    },
    showPrincipal: function () {
        configHipotecaria.tabHipotecas.classList.remove("disabled");
        $("#" + configHipotecaria.tabHipotecas.id).trigger("click");
        configHipotecaria.tabConfiguracion.classList.add("disabled");
    },
    configMatricula: {
        id: 0,
        cnt: 0,
        matriculas: {},
        campoMatricula: document.getElementById("txtMatriculaHipoteca"),
        init: function (tmpDatos) {
            return new Promise((resolve, reject) => {
                try {
                    for (let key in tmpDatos) {
                        configHipotecaria.configMatricula.cnt++;
                        configHipotecaria.configMatricula.matriculas[configHipotecaria.configMatricula.cnt] = {
                            ...tmpDatos[key]
                        };
                        configHipotecaria.configMatricula.addRowTbl({
                            ...tmpDatos[key],
                            nFila: configHipotecaria.configMatricula.cnt
                        });
                    }

                    tblMatriculas.columns.adjust().draw();
                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            });
        },
        edit: function (id = 0) {
            formActions.clean('frmMatricula').then(() => {
                this.id = id;

                if (this.id > 0) {
                    let tmp = this.matriculas[this.id];
                    this.campoMatricula.value = tmp.matricula;
                }
                $("#mdlMatricula").modal("show");
            });
        },
        save: function () {
            formActions.validate('frmMatricula').then(({
                errores
            }) => {
                if (errores == 0) {
                    let tmp = {
                        id: this.id > 0 ? this.matriculas[this.id].id : 0,
                        matricula: this.campoMatricula.value
                    }

                    let guardar = new Promise((resolve, reject) => {
                        if (this.id == 0) {
                            this.cnt++;
                            this.id = this.cnt;
                            this.addRowTbl({
                                ...tmp,
                                nFila: this.cnt
                            }).then(resolve).catch(reject);
                        } else {
                            $("#trMatricula" + this.id).find("td:eq(1)").html(tmp.matricula);
                            resolve();
                        }
                    });

                    guardar.then(() => {
                        tblMatriculas.columns.adjust().draw();
                        this.matriculas[this.id] = {
                            ...tmp
                        };
                        $("#mdlMatricula").modal("hide");
                    }).catch(generalMostrarError);
                }
            }).catch(generalMostrarError);
        },
        addRowTbl: function ({
            nFila,
            matricula
        }) {
            return new Promise((resolve, reject) => {
                try {
                    let botones = "<div class='tblButtonContainer'>" +
                        "<span class='btnTbl' title='Editar' onclick='configHipotecaria.configMatricula.edit(" + nFila + ");'>" +
                        "<i class='fas fa-edit'></i>" +
                        "</span>" +
                        "<span class='btnTbl' title='Eliminar' onclick='configHipotecaria.configMatricula.delete(" + nFila + ");'>" +
                        "<i class='fas fa-trash'></i>" +
                        "</span>" +
                        "</div>";
                    tipoPermiso != 'ESCRITURA' && (botones = '');
                    tblMatriculas.row.add([
                        nFila,
                        matricula,
                        botones
                    ]).node().id = 'trMatricula' + nFila;
                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            });
        },
        delete: function (id = 0) {
            Swal.fire({
                title: 'Eliminar matrícula',
                icon: 'warning',
                html: '¿Seguro/a que quieres <b>eliminar</b> la matrícula?.',
                showCloseButton: false,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
                cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    tblMatriculas.row('#trMatricula' + id).remove().draw();
                    delete configHipotecaria.configMatricula.matriculas[id];
                }
            });
        },
    },
    configModificaciones: {
        modificaciones: {},
        campoFecha: document.getElementById("txtFechaModificacionHipoteca"),
        campoAbogado: document.getElementById("txtAbogadoModificacion"),
        cboGeneroAbogado: document.getElementById("cboGeneroAbogadoModificacion"),
        campoAsiento: document.getElementById("txtAsientoModificacionHipoteca"),
        campoMonto: document.getElementById("numMontoModificacionHipoteca"),
        campoPlazo: document.getElementById("numPlazoModificacionHipoteca"),
        id: 0,
        cnt: 0,
        init: function (tmpDatos) {
            return new Promise((resolve, reject) => {
                try {
                    for (let key in tmpDatos) {
                        configHipotecaria.configModificaciones.cnt++;
                        configHipotecaria.configModificaciones.modificaciones[configHipotecaria.configModificaciones.cnt] = {
                            ...tmpDatos[key]
                        };
                        configHipotecaria.configModificaciones.addRowTbl({
                            ...tmpDatos[key],
                            nFila: configHipotecaria.configModificaciones.cnt
                        });
                    }

                    tblModificacionesHipoteca.columns.adjust().draw();
                    configHipotecaria.evalMonto();
                    configHipotecaria.evalPlazo();
                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            });
        },
        edit: function (id = 0) {
            formActions.clean('frmModificacionHipoteca').then(() => {
                this.id = id;
                if (this.id > 0) {
                    let tmp = this.modificaciones[this.id];
                    this.campoFecha.value = tmp.fecha;
                    this.campoAbogado.value = tmp.abogado;
                    this.cboGeneroAbogado.value = tmp.generoAbogado;
                    this.campoAsiento.value = tmp.asiento;
                    this.campoMonto.value = tmp.monto;
                    this.campoPlazo.value = tmp.plazo;
                }
                $("#mdlModificacionHipoteca").modal("show");
            });
        },
        save: function () {
            formActions.validate('frmModificacionHipoteca').then(({
                errores
            }) => {
                if (errores == 0) {
                    if (!this.campoMonto.value && !this.campoPlazo.value) {
                        toastr.warning("Debe ingresar modificación en monto o en plazo");
                        errores++;
                    }
                }

                if (errores == 0) {
                    let tmp = {
                        id: this.id > 0 ? this.modificaciones[this.id].id : 0,
                        fecha: this.campoFecha.value,
                        abogado: this.campoAbogado.value,
                        generoAbogado: this.cboGeneroAbogado.value,
                        labelGeneroAbogado: this.cboGeneroAbogado.options[this.cboGeneroAbogado.options.selectedIndex].text,
                        asiento: this.campoAsiento.value,
                        monto: this.campoMonto.value,
                        plazo: this.campoPlazo.value
                    };

                    let guardar = new Promise((resolve, reject) => {
                        if (this.id == 0) {
                            this.cnt++;
                            this.id = this.cnt;
                            this.addRowTbl({
                                ...tmp,
                                nFila: this.cnt
                            }).then(resolve).catch(reject);
                        } else {
                            let montoImp = tmp.monto.length > 0 ? '$' + parseFloat(tmp.monto).toFixed(2) : '',
                                plazoImp = tmp.plazo.length > 0 ? (parseInt(tmp.plazo) > 1 ? tmp.plazo + ' años' : tmp.plazo + ' año') : '';
                            $("#trModificacion" + this.id).find("td:eq(1)").html(tmp.fecha);
                            $("#trModificacion" + this.id).find("td:eq(2)").html(tmp.abogado);
                            $("#trModificacion" + this.id).find("td:eq(3)").html(tmp.asiento);
                            $("#trModificacion" + this.id).find("td:eq(4)").html(montoImp);
                            $("#trModificacion" + this.id).find("td:eq(5)").html(plazoImp);
                            resolve();
                        }
                    });

                    guardar.then(() => {
                        tblModificacionesHipoteca.columns.adjust().draw();
                        this.modificaciones[this.id] = {
                            ...tmp
                        };
                        $("#mdlModificacionHipoteca").modal("hide");
                        configHipotecaria.evalMonto();
                        configHipotecaria.evalPlazo();
                    }).catch(generalMostrarError);
                }
            }).catch(generalMostrarError);
        },
        addRowTbl: function ({
            nFila,
            fecha,
            abogado,
            asiento,
            monto,
            plazo,
        }) {
            return new Promise((resolve, reject) => {
                try {
                    let montoImp = monto.length > 0 ? '$' + parseFloat(monto).toFixed(2) : '',
                        plazoImp = plazo.length > 0 ? (parseInt(plazo) > 1 ? plazo + ' años' : plazo + ' año') : '';

                    let botones = "<div class='tblButtonContainer'>" +
                        "<span class='btnTbl' title='Editar' onclick='configHipotecaria.configModificaciones.edit(" + nFila + ");'>" +
                        "<i class='fas fa-edit'></i>" +
                        "</span>" +
                        "<span class='btnTbl' title='Eliminar' onclick='configHipotecaria.configModificaciones.delete(" + nFila + ");'>" +
                        "<i class='fas fa-trash'></i>" +
                        "</span>" +
                        "</div>";
                    tipoPermiso != 'ESCRITURA' && (botones = '');
                    tblModificacionesHipoteca.row.add([
                        nFila,
                        fecha,
                        abogado,
                        asiento,
                        montoImp,
                        plazoImp,
                        botones
                    ]).node().id = "trModificacion" + nFila;
                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            });
        },
        delete: function (id = 0) {
            Swal.fire({
                title: 'Eliminar modificación de hipoteca',
                icon: 'warning',
                html: '¿Seguro/a que quieres <b>eliminar</b> los datos de la modificación?.',
                showCloseButton: false,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
                cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    tblModificacionesHipoteca.row('#trModificacion' + id).remove().draw();
                    delete configHipotecaria.configModificaciones.modificaciones[id];
                    configHipotecaria.evalMonto();
                    configHipotecaria.evalPlazo();
                }
            });
        },
    }
}

const configAportaciones = {
    cnt: 0,
    id: 0,
    datosOriginales: {},
    campoCuenta: document.getElementById("txtCuentaAportaciones"),
    campoPorcentaje: document.getElementById("numPorcentajeAportaciones"),
    init: function (tmpDatos) {

    },
    edit: function (id = 0) {
        formActions.clean('frmAportaciones').then(() => {
            this.id = id;
            if (Object.keys(configGarantia.garantias[configGarantia.id].configuracion).length > 0) {
                let tmp = configGarantia.garantias[configGarantia.id].configuracion;
                this.campoCuenta.value = tmp.cuenta;
                this.campoPorcentaje.value = tmp.porcentaje;
            }
            $("#mdlAportaciones").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmAportaciones').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: Object.keys(configGarantia.garantias[configGarantia.id].configuracion).length > 0 ? configGarantia.garantias[configGarantia.id].configuracion.id : 0,
                    cuenta: this.campoCuenta.value,
                    porcentaje: this.campoPorcentaje.value
                }

                configGarantia.garantias[configGarantia.id].configuracion = {
                    ...tmp
                };

                $("#mdlAportaciones").modal("hide");
            }
        }).catch(generalMostrarError);
    }
}

const configCDP = {
    cnt: 0,
    id: 0,
    datosOriginales: {},
    campoCuenta: document.getElementById('txtCDP'),
    campoCertificado: document.getElementById('txtCertificadoCDP'),
    campoMonto: document.getElementById('numMontoCDP'),
    campoFechaCreacion: document.getElementById('txtFechaCreacionCDP'),
    campoFechaVencimiento: document.getElementById('txtFechaVencimientoCDP'),
    campoPorcentaje: document.getElementById("numPorcentajeCDP"),
    init: function () {},
    edit: function (id = 0) {
        formActions.clean('frmCDP').then(() => {
            this.id = id;
            if (Object.keys(configGarantia.garantias[configGarantia.id].configuracion).length > 0) {
                let tmp = configGarantia.garantias[configGarantia.id].configuracion;
                this.campoCuenta.value = tmp.cuenta;
                this.campoCertificado.value = tmp.certificado;
                this.campoMonto.value = tmp.monto;
                this.campoFechaCreacion.value = tmp.fechaCreacion;
                $("#" + this.campoFechaCreacion.id).datepicker("update", tmp.fechaCreacion);
                this.campoFechaVencimiento.value = tmp.fechaVencimiento;
                $("#" + this.campoFechaVencimiento.id).datepicker("update", tmp.fechaVencimiento);
                this.campoPorcentaje.value = tmp.porcentaje;
            }
            $("#mdlCDP").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmCDP').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: Object.keys(configGarantia.garantias[configGarantia.id].configuracion).length > 0 ? configGarantia.garantias[configGarantia.id].configuracion.id : 0,
                    cuenta: this.campoCuenta.value,
                    certificado: this.campoCertificado.value,
                    monto: this.campoMonto.value,
                    fechaCreacion: this.campoFechaCreacion.value,
                    fechaVencimiento: this.campoFechaVencimiento.value,
                    porcentaje: this.campoPorcentaje.value
                }

                configGarantia.garantias[configGarantia.id].configuracion = {
                    ...tmp
                };

                $("#mdlCDP").modal("hide");
            }
        }).catch(generalMostrarError);
    }
}

const configOID = {
    cnt: 0,
    id: 0,
    init: function () {},
    edit: function (id = 0) {
        this.id = id;
        Swal.fire({
            title: "",
            html: "Este tipo de garantía no requiere configuración",
            icon: "info"
        });
    }
}

const configFSG = {
    cnt: 0,
    id: 0,
    init: function () {},
    edit: function (id = 0) {
        this.id = id;
        Swal.fire({
            title: "",
            html: "Este tipo de garantía no requiere configuración",
            icon: "info"
        });
    }
}

const configProgara = {
    cnt: 0,
    id: 0,
    init: function () {},
    edit: function (id = 0) {
        this.id = id;
        Swal.fire({
            title: "",
            html: "Este tipo de garantía no requiere configuración",
            icon: "info"
        });
    }
}

const configFirmante = {
    id: 0,
    datosOriginales: {},
    cboTipoDocumento: document.getElementById("cboTipoDocumentoFirmante"),
    campoNumeroDocumento: document.getElementById("txtTipoDocumentoFirmante"),
    campoNombres: document.getElementById("txtNombresFirmante"),
    campoApellidos: document.getElementById("txtApellidosFirmante"),
    campoConocido: document.getElementById("txtConocidoFirmante"),
    cboPais: document.getElementById("cboPaisFirmante"),
    cboDepartamento: document.getElementById("cboDepartamentoFirmante"),
    cboMunicipio: document.getElementById("cboMunicipioFirmante"),
    campoProfesion: document.getElementById("txtProfesionFirmante"),
    campoFechaNacimiento: document.getElementById("txtFechaNacimientoFirmante"),
    campoEdad: document.getElementById("numEdadFirmante"),
    init: function (tmpDatos) {
        let allSelectpickerCbos = document.querySelectorAll("#ruego select.selectpicker");
        this.id = parseInt(tmpDatos.id);
        this.cboTipoDocumento.value = tmpDatos.tipoDocumento;
        $("#" + this.cboTipoDocumento.id).trigger("change");
        this.campoNumeroDocumento.value = tmpDatos.numeroDocumento;
        this.campoNombres.value = tmpDatos.nombres;
        this.campoApellidos.value = tmpDatos.apellidos;
        this.campoConocido.value = tmpDatos.conocido;
        this.cboPais.value = tmpDatos.pais;
        $("#" + this.cboPais.id).trigger("change");
        this.cboDepartamento.value = tmpDatos.departamento;
        $("#" + this.cboDepartamento.id).trigger("change");
        this.cboMunicipio.value = tmpDatos.municipio;
        this.campoProfesion.value = tmpDatos.profesion;
        this.campoFechaNacimiento.value = tmpDatos.fechaNacimiento;
        $("#" + this.campoFechaNacimiento.id).trigger("change");

        allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
        this.datosOriginales = JSON.parse(JSON.stringify(this.getJSON()));
    },
    getJSON: function () {
        let datos = {
            id: this.id,
            tipoDocumento: this.cboTipoDocumento.value,
            labelTipoDocumento: this.cboTipoDocumento.options[this.cboTipoDocumento.options.selectedIndex].text,
            numeroDocumento: this.campoNumeroDocumento.value,
            nombres: this.campoNombres.value,
            apellidos: this.campoApellidos.value,
            conocido: this.campoConocido.value,
            pais: this.cboPais.value,
            labelPais: this.cboPais.options[this.cboPais.options.selectedIndex].text,
            departamento: this.cboDepartamento.value,
            labelDepartamento: this.cboDepartamento.options[this.cboDepartamento.options.selectedIndex].text,
            municipio: this.cboMunicipio.value,
            labelMunicipio: this.cboMunicipio.options[this.cboMunicipio.options.selectedIndex].text,
            profesion: this.campoProfesion.value,
            fechaNacimiento: this.campoFechaNacimiento.value,
            edad: this.campoEdad.value,
        }

        return datos;
    }
}

const configDesembolsos = {
    id: 0,
    cnt: 0,
    desembolsos: {},
    campoFecha: document.getElementById("txtFechaDesembolso"),
    campoMonto: document.getElementById("numMontoDesembolso"),
    init: function (tmpDatos) {
        tmpDatos.forEach(parcial => {
            this.cnt++;
            this.desembolsos[this.cnt] = {
                id: parcial.id,
                fecha: parcial.fecha,
                monto: parcial.monto
            };

            this.addRowTbl({
                ...this.desembolsos[this.cnt],
                nFila: this.cnt
            });
        });
        tblDesembolsos.columns.adjust().draw();
    },
    edit: function (id = 0) {
        formActions.clean('frmParcial').then(() => {
            this.id = id;

            if (this.id > 0) {
                let tmp = this.desembolsos[this.id];
                this.campoFecha.value = tmp.fecha;
                $("#" + this.campoFecha.id).datepicker("update", tmp.fecha);
                this.campoMonto.value = tmp.monto;
            }

            $("#mdlDesembolsoParcial").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmParcial').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.desembolsos[this.id].id : 0,
                    fecha: this.campoFecha.value,
                    monto: this.campoMonto.value
                }

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trDesembolso" + this.id).find("td:eq(1)").html(tmp.fecha);
                        $("#trDesembolso" + this.id).find("td:eq(2)").html("$" + new Intl.NumberFormat().format(parseFloat(tmp.monto).toFixed(2)));
                        resolve();
                    }
                });

                guardar.then(() => {
                    this.desembolsos[this.id] = {
                        ...tmp
                    };
                    tblDesembolsos.columns.adjust().draw();
                    $("#mdlDesembolsoParcial").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        fecha,
        monto
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configDesembolsos.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configDesembolsos.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblDesembolsos.row.add([
                    nFila,
                    fecha,
                    "$" + new Intl.NumberFormat().format(parseFloat(monto).toFixed(2)),
                    botones
                ]).node().id = 'trDesembolso' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        Swal.fire({
            title: 'Eliminar desembolso',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el desembolso?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblDesembolsos.row('#trDesembolso' + id).remove().draw();
                delete this.desembolsos[id];
            }
        });
    }
}

function sgaShowScreen(parentContainerId, containerId) {
    return new Promise((resolve, reject) => {
        let containers = document.querySelectorAll(`#${parentContainerId} .sgaContainer`);

        containers.forEach(container => {
            if (container.id && container.id != containerId) {
                container.style.display = 'none';
            } else {
                if ($("#" + containerId).is(":visible") == false) {
                    $("#" + containerId).fadeIn('fast', function () {
                        tblGarantias.columns.adjust().draw();
                        resolve();
                    });
                }
            }
        });
    });
}