<div class="pagetitle">
    <h1>Reporte detallado de SGA-MUTUOS</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">SGA-MUTUOS</li>
            <li class="breadcrumb-item">Reportería</li>
            <li class="breadcrumb-item active">Detallado</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <form action="javascript:configBusqueda.search();" id="frmReporte" name="frmReporte" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-1 col-xl-1"></div>
                    <div class="col-lg-10 col-xl-10">
                        <div class="row">
                            <div class="col-lg-4 col-xl-4">
                                <label class="form-label" for="cboAgencia">Agencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboAgencia" name="cboAgencia" class="selectpicker cboAgencia form-control" required title="">
                                        <option selected value="0">Todas las agencias</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <label for="txtInicio" class="form-label">Inicio <span class="requerido">*</span></label>
                                <input type="text" id="txtInicio" name="txtInicio" class="form-control fechaFinLimitado readonly" required>
                                <div class="invalid-feedback">
                                    Ingrese la fecha de inicio de búsqueda
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <label for="txtFin" class="form-label">Fin <span class="requerido">*</span></label>
                                <input type="text" id="txtFin" name="txtFin" class="form-control fechaFinLimitado readonly" required>
                                <div class="invalid-feedback">
                                    Ingrese la fecha de fin de búsqueda
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-xl-1"></div>
                    <div class="col-lg-12 col-xl-12 mt-2" align="center">
                        <button type="submit" class="btn btn-sm btn-outline-primary">
                            <i class="fas fa-search"></i> Buscar
                        </button>
                        <button type="button" class="btn btn-sm btn-outline-success" id="btnExcel" disabled onclick="configBusqueda.generarExcel();">
                            <i class="fas fa-file-excel"></i> Excel
                        </button>
                    </div>
                </div>
            </form>
            <hr class="mb-0 mt-2">
        </div>
    </div>
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="mutuos-tab" data-bs-toggle="tab" data-bs-target="#mutuos" type="button" role="tab" aria-controls="mutuos" aria-selected="true">
                Mutuos
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="pagares-tab" data-bs-toggle="tab" data-bs-target="#pagares" type="button" role="tab" aria-controls="pagares" aria-selected="true">
                Pagarés
            </button>
        </li>
    </ul>
    <div class="tab-content pt-2">
        <div class="tab-pane fade show active" id="mutuos" role="tabpanel" aria-labelledby="mutuos-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12" id="divContenedorMutuos">
                    <table id="tblMutuos" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Agencia</th>
                                <th>Código de cliente</th>
                                <th>Nombre deudor</th>
                                <th>Monto de crédito</th>
                                <th>Número de crédito</th>
                                <th>Fecha de documento</th>
                                <th>Garantías</th>
                                <th>Matrículas</th>
                                <th>Propietario de inmueble</th>
                                <th>Usuario</th>
                                <th>¿Línea rotativa?</th>
                                <th>Fecha de vencimiento</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="pagares" role="tabpanel" aria-labelledby="pagares-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12" id="divContenedorPagares">
                    <table id="tblPagares" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Agencia</th>
                                <th>Código de cliente</th>
                                <th>Tipo de documento</th>
                                <th>Número de documento</th>
                                <th>NIT</th>
                                <th>Nombre deudor</th>
                                <th>País</th>
                                <th>Departamento</th>
                                <th>Municipio</th>
                                <th>Dirección</th>
                                <th>Edad</th>
                                <th>Número de crédito</th>
                                <th>Monto de desembolso</th>
                                <th>Tasa de interés</th>
                                <th>Fecha de vencimiento</th>
                                <th>Fecha de documento</th>
                                <th>Usuario</th>
                                <th>Extra</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$_GET['js'] = ['sgaReporteDetallado'];
