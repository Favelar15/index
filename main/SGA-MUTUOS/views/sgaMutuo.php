<div id="principalContainer">
    <div class="sgaContainer" id="mainContainer">
        <div class="pagetitle">
            <div class="row">
                <div class="col">
                    <h1>Generación de mutuo</h1>
                </div>
                <div class="col-4 topBtnContainer">
                    <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configMutuo.cancel();">
                        <i class="fas fa-times"></i> <span>Cancelar</span>
                    </button>
                    <button class="btn btn-sm btn-outline-success float-end" title="Guardar" type="button" onclick="configMutuo.save();">
                        <i class="fas fa-save"></i> <span>Generar documentos</span>
                    </button>
                </div>
            </div>
            <nav>
                <ol class="breadcrumb mb-1">
                    <li class="breadcrumb-item">SGA-MUTUOS</li>
                    <li class="breadcrumb-item">Generación</li>
                    <li class="breadcrumb-item active">Mutuo</li>
                </ol>
            </nav>
        </div>
        <hr class="mb-1 mt-1">
        <section class="section">
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="deudor-tab" data-bs-toggle="tab" data-bs-target="#deudor" type="button" role="tab" aria-controls="deudor" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesDeudor');">
                        Datos de deudor
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="credito-tab" data-bs-toggle="tab" data-bs-target="#credito" type="button" role="tab" aria-controls="credito" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesCredito');">
                        Datos de crédito
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="garantia-tab" data-bs-toggle="tab" data-bs-target="#garantia" type="button" role="tab" aria-controls="garantia" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesGarantia');">
                        Garantía
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link disabled" id="ruego-tab" data-bs-toggle="tab" data-bs-target="#ruego" type="button" role="tab" aria-controls="ruego" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesRuego');">
                        Firmante a ruego
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link disabled" id="parcial-tab" data-bs-toggle="tab" data-bs-target="#parcial" type="button" role="tab" aria-controls="parcial" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesParcial');">
                        Desembolso parcial
                    </button>
                </li>
                <li class="nav-item ms-auto" id="mainButtonContainer">
                    <div class="tabButtonContainer" id="botonesDeudor" style="display: none;"></div>
                    <div class="tabButtonContainer" id="botonesCredito" style="display: none;"></div>
                    <div class="tabButtonContainer" id="botonesGarantia" style="display: none;">
                        <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configGarantia.edit();">
                            <i class="fas fa-plus-circle"></i> Garantía
                        </button>
                    </div>
                    <div class="tabButtonContainer" id="botonesParcial" style="display: none;">
                        <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="configDesembolsos.edit();">
                            <i class="fas fa-plus-circle"></i> Desembolso
                        </button>
                    </div>
                </li>
            </ul>
            <div class="tab-content pt-2">
                <div class="tab-pane fade show active" id="deudor" role="tabpanel" aria-labelledby="deudor-tab">
                    <form action="javascript:configMutuo.save();" id="frmDeudor" name="frmDeudor" method="POST" accept-charset="utf-8" class="needs-validation" novalidate>
                        <div class="row">
                            <div class='col-lg-3 col-xl-3'>
                                <label for='txtCodigoCliente' class="form-label">Código de cliente <span class="requerido">*</span></label>
                                <input type='text' class='form-control' id='txtCodigoCliente' name='txtCodigoCliente' value="" required onkeypress="return generalSoloNumeros(event);">
                                <div class='invalid-feedback'>
                                    Ingrese el código de cliente
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3'>
                                <label for='cboTipoDocumentoDeudor' class="form-label">Tipo de documento <span class="requerido">*</span></label>
                                <select id="cboTipoDocumentoDeudor" name="cboTipoDocumentoDeudor" class="form-select cboTipoDocumento" required onchange="generalMonitoreoTipoDocumento(this.value,'txtNumeroDocumentoDeudor');">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione un tipo de documento
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3'>
                                <label for='txtNumeroDocumentoDeudor' class="form-label">Número de documento <span class="requerido">*</span></label>
                                <input type='text' class='form-control' id='txtNumeroDocumentoDeudor' name='txtNumeroDocumentoDeudor' value="" required readonly>
                                <div class='invalid-feedback'>
                                    Ingrese el número de documento
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3'>
                                <label for='txtNITDeudor' class="form-label">Número de NIT</label>
                                <input type='text' class='form-control NIT' id='txtNITDeudor' name='txtNITDeudor' value="">
                                <div class='invalid-feedback'>
                                    Ingrese el número de NIT
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtNombresDeudor" class="form-label">Nombres de deudor/a <span class="requerido">*</span> </label>
                                <input type="text" id="txtNombresDeudor" name="txtNombresDeudor" placeholder="" title="nombres" class="form-control" value="" required>
                                <div class="invalid-feedback">
                                    Ingrese los nombres del deudor
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtApellidosDeudor" class="form-label">Apellidos de deudor/a <span class="requerido">*</span> </label>
                                <input type="text" id="txtApellidosDeudor" name="txtApellidosDeudor" placeholder="" title="apellidos" class="form-control" value="" required>
                                <div class="invalid-feedback">
                                    Ingrese los apellidos del deudor
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtConocidoDeudor" class="form-label">Conocido por </label>
                                <input type="text" id="txtConocidoDeudor" name="txtConocidoDeudor" placeholder="" title="conocido por" class="form-control" value="">
                                <div class="invalid-feedback">
                                    Ingrese el conocido por...
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboGeneroDeudor' class="form-label">Género <span class="requerido">*</span></label>
                                <select id="cboGeneroDeudor" name="cboGeneroDeudor" class="form-select cboGenero" required>
                                    <option selected value="" disabled>seleccione</option>
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione el género del deudor
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboPaisDeudor' class="form-label">Pais de residencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboPaisDeudor" name="cboPaisDeudor" class="selectpicker form-control cboPais" title="" data-live-search="true" required onchange="generalMonitoreoPais(this.value,'cboDepartamentoDeudor');">
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboDepartamentoDeudor' class="form-label">Departamento de residencia <span class="requerido">*</span></label>
                                <div class='form-group has-validation'>
                                    <select id="cboDepartamentoDeudor" title="" name="cboDepartamentoDeudor" class="selectpicker form-control" data-live-search="true" title="" required onchange="generalMonitoreoDepartamento('cboPaisDeudor',this.value,'cboMunicipioDeudor');">
                                        <option selected value="" disabled>seleccione un país</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboMunicipioDeudor' class="form-label">Municipio de residencia <span class="requerido">*</span></label>
                                <div class='form-group has-validation'>
                                    <select id="cboMunicipioDeudor" title="" name="cboMunicipioDeudor" class="selectpicker form-control" data-live-search="true" required>
                                        <option selected value="" disabled>seleccione un departamento</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtProfesionDeudor" class="form-label">Profesión <span class="requerido">*</span> </label>
                                <input type="text" id="txtProfesionDeudor" name="txtProfesionDeudor" placeholder="" title="profesión" class="form-control" value="" required>
                                <div class="invalid-feedback">
                                    Ingrese la profesión
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for="txtFechaNacimientoDeudor" class="form-label">Fecha de nacimiento <span class="requerido">*</span> </label>
                                <input type="text" id="txtFechaNacimientoDeudor" name="txtFechaNacimientoDeudor" placeholder="YYYY-MM-DD" class="form-control readonly fechaAbierta" value="" onchange="generalAsignarEdad(this.value,'numEdadDeudor');" required>
                                <div class="invalid-feedback">
                                    Seleccione la fecha de nacimiento
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for="numEdadDeudor" class="form-label">Edad <span class="requerido">*</span> </label>
                                <input type="number" id="numEdadDeudor" name="numEdadDeudor" placeholder="" title="edad" class="form-control" value="" readonly required min="0" step="1">
                                <div class="invalid-feedback">
                                    Seleccione la fecha de nacimiento
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboFirmaDeudor' class="form-label">¿Sabe firmar? <span class="requerido">*</span></label>
                                <select id="cboFirmaDeudor" name="cboFirmaDeudor" class="form-select cboFirma" onchange="configMutuo.evalFirma();" required>
                                    <option selected value="" disabled>seleccione</option>
                                    <option value="S">SI</option>
                                    <option value="N">NO</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione una opción
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="credito" role="tabpanel" aria-labelledby="credito-tab">
                    <form action="javascript:configMutuo.save();" id="frmCredito" name="frmCredito" method="POST" accept-charset="utf-8" class="needs-validation" novalidate>
                        <div class="row">
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboAgencia' class="form-label">Agencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboAgencia" title="seleccione" name="cboAgencia" class="selectpicker form-control cboAgencia" data-live-search="true" required>
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3 cuenta-acacypac'>
                                <label for='txtNumeroCredito' class="form-label">Número de crédito <span class="requerido">*</span></label>
                                <input type='text' class='form-control cuentaAcacypac' id='txtNumeroCredito' name='txtNumeroCredito' value="" required onkeypress="return generalSoloNumeros(event);">
                                <div class='invalid-feedback'>
                                    Ingrese el número de crédito
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for="numMontoCredito" class="form-label">Monto <span class="requerido">*</span></label>
                                <input type="number" id="numMontoCredito" name="numMontoCredito" class="form-control" placeholder="" title="monto" min="0" step="0.01" required onkeypress="return generalSoloNumeros(event);" value="">
                                <div class="invalid-feedback">
                                    Ingrese el monto total del crédito
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for="numTasaInteres" class="form-label">Tasa de interés <span class="requerido">*</span></label>
                                <input type="number" id="numTasaInteres" name="numTasaInteres" class="form-control" placeholder="" title="tasa de interés" min="0" step="0.01" required onkeypress="return generalSoloNumeros(event);" value="">
                                <div class="invalid-feedback">
                                    Ingrese la tasa de interés
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboLineaFinanciera' class="form-label">Línea financiera <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboLineaFinanciera" name="cboLineaFinanciera" class="selectpicker form-control cboLineaFinanciera" data-live-search="true" required>
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtDestinoCredito" class="form-label">Destino <span class="requerido">*</span></label>
                                <input type="text" id="txtDestinoCredito" name="txtDestinoCredito" class="form-control" placeholder="" title="destino" min="0" step="0.01" required value="">
                                <div class="invalid-feedback">
                                    Ingrese el destino del crédito
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboVencimiento' class="form-label">¿Crédito al vencimiento? <span class="requerido">*</span></label>
                                <select id="cboVencimiento" name="cboVencimiento" class="form-select" required>
                                    <option selected value="" disabled>seleccione</option>
                                    <option value="N">NO</option>
                                    <option value="S">SI</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione una opción
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboPeriodicidad' class="form-label">Periodicidad de pago <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboPeriodicidad" name="cboPeriodicidad" class="selectpicker form-control cboPeriodicidad" data-live-search="true" required>
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3" style="border:1px solid rgba(206, 206, 206, 0.96); border-radius:.3em;">
                                <label class="form-label">Plazo <span class="requerido">*</span></label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label style="display:none;">Plazo</label>
                                        <div class="form-group has-validation">
                                            <input type="number" id="numPlazo" name="numPlazo" placeholder="plazo" title="plazo" min="0" step="1" onkeypress="return generalSoloNumeros(event);" class="form-control" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class='my_profile_setting_input ui_kit_select_search form-group'>
                                            <label style="display:none;">Unidad plazo</label>
                                            <div class="form-group has-validation">
                                                <select id="cboPlazo" name="cboPlazo" class="form-select cboPlazo" required>
                                                    <option selected value="" disabled>seleccione</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3 fecha_inicio_limitado-hora'>
                                <label for='txtFechaGeneracion' class="form-label">Fecha y hora de generación <span class="requerido">*</span></label>
                                <input type='text' class='form-control fechaHoraAbierta readonly' id='txtFechaGeneracion' name='txtFechaGeneracion' value="" required>
                                <div class='invalid-feedback'>
                                    Seleccione la fecha y hora
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboFuenteFondos' class="form-label">Fuente de fondos <span class="requerido">*</span></label>
                                <select id="cboFuenteFondos" name="cboFuenteFondos" class="form-select cboFuenteFondos" required>
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione la fuente de los fondos
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboEmpleadoAcacypac' class="form-label">¿Empleado ACACYPAC? <span class="requerido">*</span></label>
                                <select id="cboEmpleadoAcacypac" name="cboEmpleadoAcacypac" class="form-select" required>
                                    <option selected value="" disabled>seleccione</option>
                                    <option value="N">NO</option>
                                    <option value="S">SI</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione una opción
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboDesembolsoParcial' class="form-label">¿Desembolso parcial? <span class="requerido">*</span></label>
                                <select id="cboDesembolsoParcial" name="cboDesembolsoParcial" class="form-select" onchange="configMutuo.evalParciales(this.value);" required onchange="">
                                    <option selected value="" disabled>seleccione</option>
                                    <option value="N">NO</option>
                                    <option value="S">SI</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione una opción
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboLineaRotativa' class="form-label">¿Línea Rotativa? <span class="requerido">*</span></label>
                                <select id="cboLineaRotativa" name="cboLineaRotativa" class="form-select" required onchange="configDatosCredito.evalRotativa(this.value);">
                                    <option selected value="" disabled>seleccione</option>
                                    <option value="N">NO</option>
                                    <option value="S">SI</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione una opción
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3'>
                                <label for='txtFechaVencimiento' class="form-label">Fecha de vencimiento</label>
                                <input type='text' class='form-control fechaInicioLimitado readonly' id='txtFechaVencimiento' name='txtFechaVencimiento' value="" disabled required>
                                <div class='invalid-feedback'>
                                    Seleccione la fecha de vencimiento
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-12 mt-2">
                                <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                                    <li class="nav-item">
                                        <button class="nav-link active " id="construccionCuota-tab" data-bs-toggle="tab" data-bs-target="#construccionCuota" role="tab" aria-controls="construccionCuota" aria-selected="true">
                                            Construcción de cuota
                                        </button>
                                    </li>
                                    <li class="nav-item ms-auto">
                                        <div class="tabButtonContainerThird" id="btnsCuota">
                                            <button type="button" onclick="configDatosCredito.configCuota.configAditivos.edit();" class="btn btn-outline-primary btn-sm">
                                                <i class="fa fa-plus-circle"></i> Aditivo
                                            </button>
                                        </div>
                                    </li>
                                </ul>
                                <div class="tab-content" style="margin-top: 20px;">
                                    <div class="tab-pane fade show active" id="construccionCuota" role="tabpanel" aria-labelledby="construccionCuota-tab">
                                        <div class="row">
                                            <div class="col-lg-12 col-xl-12">
                                                <div class="row">
                                                    <div class="col-lg-6 col-xl-6">
                                                        <div class="input-group input-group-sm mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Monto capital e intereses: $</span>
                                                            </div>
                                                            <input type="number" id="numCapitalInteres" value="" name="numCapitalInteres" class="form-control" min="0" step="0.01" onkeypress="return generalSoloNumeros(event);" onkeyup="configDatosCredito.configCuota.calcularCuota();" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <div class="input-group input-group-sm mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Monto total de cuota: $</span>
                                                            </div>
                                                            <input type="number" disabled id="numMontoTotal" value="0.00" name="numMontoTotal" class="form-control" min="0" step="0.01">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-xl-12">
                                                <table id="tblAditivos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                                    <thead>
                                                        <th>N°</th>
                                                        <th>Aditivo</th>
                                                        <th>Monto</th>
                                                        <th>Opciones</th>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="garantia" role="tabpanel" aria-labelledby="garantia-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblGarantias" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                <thead>
                                    <th>N°</th>
                                    <th>Tipo de garantía</th>
                                    <th>Parámetros</th>
                                    <th>¿Configurada?</th>
                                    <th>Opciones</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="ruego" role="tabpanel" aria-labelledby="ruego-tab">
                    <form action="javascript:configMutuo.save();" id="frmFirmante" name="frmFirmante" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                        <div class="row">
                            <div class='col-lg-3 col-xl-3'>
                                <label for='cboTipoDocumentoFirmante' class="form-label">Tipo de documento <span class="requerido">*</span></label>
                                <select id="cboTipoDocumentoFirmante" name="cboTipoDocumentoFirmante" class="form-select cboTipoDocumento" onchange="generalMonitoreoTipoDocumento(this.value,'txtTipoDocumentoFirmante');" required>
                                    <option selected value="" disabled></option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione un tipo de documento
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3 dui'>
                                <label for='txtTipoDocumentoFirmante' class="form-label">Número de documento <span class="requerido">*</span></label>
                                <input type='text' class='form-control' id='txtTipoDocumentoFirmante' readonly name='txtTipoDocumentoFirmante' value="" required>
                                <div class='invalid-feedback'>
                                    Ingrese el número de documento
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtNombresFirmante" class="form-label">Nombres de firmante <span class="requerido">*</span> </label>
                                <input type="text" id="txtNombresFirmante" name="txtNombresFirmante" placeholder="" title="nombres de firmante" class="form-control" value="" required>
                                <div class="invalid-feedback">
                                    Ingrese los nombres del firmante
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtApellidosFirmante" class="form-label">Apellidos de firmante <span class="requerido">*</span> </label>
                                <input type="text" id="txtApellidosFirmante" name="txtApellidosFirmante" placeholder="" title="apellidos de firmante" class="form-control" value="" required>
                                <div class="invalid-feedback">
                                    Ingrese los apellidos del firmante
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtConocidoFirmante" class="form-label">Conocido por </label>
                                <input type="text" id="txtConocidoFirmante" name="txtConocidoFirmante" placeholder="" title="conocido por" class="form-control" value="">
                                <div class="invalid-feedback">
                                    Ingrese el conocido por...
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboPaisFirmante' class="form-label">Pais de residencia <span class="requerido">*</span></label>
                                <div class='form-group has-validation'>
                                    <select id="cboPaisFirmante" title="" name="cboPaisFirmante" class="selectpicker form-control cboPais" data-live-search="true" onchange="generalMonitoreoPais(this.value,'cboDepartamentoFirmante');" required>
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboDepartamentoFirmante' class="form-label">Departamento de residencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboDepartamentoFirmante" title="" name="cboDepartamentoFirmante" class="selectpicker form-control" data-live-search="true" onchange="generalMonitoreoDepartamento('cboPaisFirmante',this.value,'cboMunicipioFirmante');" required>
                                        <option selected value="" disabled>seleccione un pais</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboMunicipioFirmante' class="form-label">Municipio de residencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboMunicipioFirmante" title="" name="cboMunicipioFirmante" class="selectpicker form-control" data-live-search="true" required>
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtProfesionFirmante" class="form-label">Profesión <span class="requerido">*</span> </label>
                                <input type="text" id="txtProfesionFirmante" name="txtProfesionFirmante" placeholder="" title="profesión" class="form-control" value="" required>
                                <div class="invalid-feedback">
                                    Ingrese la profesión del firmante
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for="txtFechaNacimientoFirmante" class="form-label">Fecha de nacimiento <span class="requerido">*</span> </label>
                                <input type="text" id="txtFechaNacimientoFirmante" name="txtFechaNacimientoFirmante" placeholder="YYYY-MM-DD" class="form-control fechaAbierta readonly" onchange="generalAsignarEdad(this.value,'numEdadFirmante');" value="" required>
                                <div class="invalid-feedback">
                                    Seleccione la fecha de nacimiento
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for="numEdadFirmante" class="form-label">Edad <span class="requerido">*</span> </label>
                                <input type="number" id="numEdadFirmante" name="numEdadFirmante" placeholder="" title="edad" class="form-control" value="" readonly min="0" step="1">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="parcial" role="tabpanel" aria-labelledby="parcial-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblDesembolsos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Fecha de desembolso</th>
                                        <th>Monto de desembolso</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="sgaContainer" id="fiadoresContainer" style="display: none;">
        <div class="pagetitle">
            <div class="row">
                <div class="col">
                    <h1>Configuración de fiadores</h1>
                </div>
                <div class="col-4 topBtnContainer">
                    <button class="btn btn-sm btn-outline-secondary float-end ms-2" type="button" title="Regresar" onclick="sgaShowScreen('principalContainer','mainContainer');">
                        <i class="fas fa-arrow-left"></i> <span>Regresar</span>
                    </button>
                </div>
            </div>
            <nav>
                <ol class="breadcrumb mb-1">
                    <li class="breadcrumb-item">SGA-MUTUOS</li>
                    <li class="breadcrumb-item">Generar mutuo</li>
                    <li class="breadcrumb-item">Configuración de garantía</li>
                    <li class="breadcrumb-item active">Fiadores</li>
                </ol>
            </nav>
        </div>
        <hr class="mb-1 mt-1">
        <section class="section">
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="fiadores-tab" data-bs-toggle="tab" data-bs-target="#fiadores" type="button" role="tab" aria-controls="fiadores" aria-selected="true" onclick="generalShowHideButtonTab('fiadoresButtonContainer','botonesFiadores');">
                        Fiadores
                    </button>
                </li>
                <li class="nav-item ms-auto" id="fiadoresButtonContainer">
                    <div class="tabButtonContainer" id="botonesFiadores">
                        <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configFiduciaria.edit();">
                            <i class="fas fa-plus-circle"></i> Fiador
                        </button>
                    </div>
                </li>
            </ul>
            <div class="tab-content pt-2">
                <div class="tab-pane fade show active" id="fiadores" role="tabpanel" aria-labelledby="fiadores-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblFiadores" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                <thead>
                                    <th>N°</th>
                                    <th>Documento</th>
                                    <th>NIT</th>
                                    <th>Nombre completo</th>
                                    <th>Edad</th>
                                    <th>Profesión</th>
                                    <th>País</th>
                                    <th>Departamento</th>
                                    <th>Municipio</th>
                                    <th>¿Sabe firmar?</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="sgaContainer" id="hipotecasContainer" style="display: none;">
        <div class="pagetitle">
            <div class="row">
                <div class="col">
                    <h1>Configuración de hipotecas</h1>
                </div>
                <div class="col-4 topBtnContainer">
                    <button class="btn btn-sm btn-outline-secondary float-end ms-2" type="button" title="Regresar" onclick="sgaShowScreen('principalContainer','mainContainer');">
                        <i class="fas fa-arrow-left"></i> <span>Regresar</span>
                    </button>
                </div>
            </div>
            <nav>
                <ol class="breadcrumb mb-1">
                    <li class="breadcrumb-item">SGA-MUTUOS</li>
                    <li class="breadcrumb-item">Generar mutuo</li>
                    <li class="breadcrumb-item">Configuración de garantía</li>
                    <li class="breadcrumb-item active">Hipotecas</li>
                </ol>
            </nav>
        </div>
        <hr class="mb-1 mt-1">
        <section class="section">
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="hipotecas-tab" data-bs-toggle="tab" data-bs-target="#hipotecas" type="button" role="tab" aria-controls="hipotecas" aria-selected="true" onclick="generalShowHideButtonTab('hipotecasButtonContainer','botonesHipotecas');">
                        Hipotecas
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link disabled" id="configuracionHipoteca-tab" data-bs-toggle="tab" data-bs-target="#configuracionHipoteca" type="button" role="tab" aria-controls="configuracionHipoteca" aria-selected="true" onclick="generalShowHideButtonTab('hipotecasButtonContainer','botonesConfiguracionHipotecas');">
                        Configuración
                    </button>
                </li>
                <li class="nav-item ms-auto" id="hipotecasButtonContainer">
                    <div class="tabButtonContainer" id="botonesHipotecas">
                        <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configHipotecaria.edit();">
                            <i class="fas fa-plus-circle"></i> Hipoteca
                        </button>
                    </div>
                    <div class="tabButtonContainer" id="botonesConfiguracionHipotecas" style="display: none;">
                        <button type="submit" form="frmHipoteca" class="btn btn-sm btn-outline-success ml-2">
                            <i class="fas fa-save"></i> Guardar
                        </button>
                        <button type="button" class="btn btn-sm btn-outline-danger ml-2" onclick="configHipotecaria.cancel();">
                            <i class="fas fa-times"></i> Cancelar
                        </button>
                    </div>
                </li>
            </ul>
            <div class="tab-content pt-2">
                <div class="tab-pane fade show active" id="hipotecas" role="tabpanel" aria-labelledby="hipotecas-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblHipotecas" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                <thead>
                                    <th>N°</th>
                                    <th>Fecha de emisión</th>
                                    <th>Abogado</th>
                                    <th>Matrícula</th>
                                    <th>Asiento</th>
                                    <th>Monto</th>
                                    <th>Plazo</th>
                                    <th>Zona</th>
                                    <th>Sección</th>
                                    <th>Departamento</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="configuracionHipoteca" role="tabpanel" aria-labelledby="configuracionHipoteca-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <form action="javascript:configHipotecaria.save();" id="frmHipoteca" name="frmHipoteca" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                                <div class="row">
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='txtFechaEmisionHipoteca' class="form-label">Fecha y hora de emisión <span class="requerido">*</span></label>
                                        <input type='text' class='form-control fechaHoraAbierta readonly' id='txtFechaEmisionHipoteca' name='txtFechaEmisionHipoteca' value="" required>
                                        <div class='invalid-feedback'>
                                            Seleccione la fecha de emisión
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3 mayusculas">
                                        <label for='txtAbogadoHipoteca' class="form-label">Nombre de abogado <span class="requerido">*</span></label>
                                        <input type='text' class='form-control' id='txtAbogadoHipoteca' name='txtAbogadoHipoteca' value="" required>
                                        <div class='invalid-feedback'>
                                            Ingrese el nombre del abogado
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='cboGeneroAbogado' class="form-label">Género de abogado <span class="requerido">*</span></label>
                                        <select id="cboGeneroAbogado" name="cboGeneroAbogado" class="form-select" required>
                                            <option selected disabled value="">seleccione</option>
                                            <option value="M">Masculino</option>
                                            <option value="F">Femenino</option>
                                        </select>
                                        <div class='invalid-feedback'>
                                            Seleccione una opción
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='txtAsientoHipoteca' class="form-label">Asiento de registro <span class="requerido">*</span></label>
                                        <input type='text' class='form-control' id='txtAsientoHipoteca' name='txtAsientoHipoteca' value="" onkeypress="return generalSoloNumeros(event);" required>
                                        <div class='invalid-feedback'>
                                            Ingrese el número de asiento
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='numMontoHipoteca' class="form-label">Monto de hipoteca <span class="requerido">*</span></label>
                                        <input type='number' class='form-control' id='numMontoHipoteca' name='numMontoHipoteca' value="" onkeypress="return generalSoloNumeros(event);" min="0" step="0.01" onkeyup="configHipotecaria.evalMonto();" required>
                                        <div class='invalid-feedback'>
                                            Ingrese el monto de la hipoteca
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='numPlazoHipoteca' class="form-label">Plazo de hipoteca <span class="requerido">*</span></label>
                                        <input type='number' class='form-control' id='numPlazoHipoteca' name='numPlazoHipoteca' value="" onkeypress="return generalSoloNumeros(event);" min="0" step="1" onkeyup="configHipotecaria.evalPlazo();" required>
                                        <div class='invalid-feedback'>
                                            Ingrese el plazo de la hipoteca
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='cboZonaHipoteca' class="form-label">Zona <span class="requerido">*</span></label>
                                        <select id="cboZonaHipoteca" name="cboZonaHipoteca" class="form-select cboZonaHipoteca" onchange="configHipotecaria.evalZona(this.value,'cboSeccionHipoteca');" required>
                                            <option selected value="" disabled>seleccione</option>
                                        </select>
                                        <div class='invalid-feedback'>
                                            Seleccione la zona
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='cboSeccionHipoteca' class="form-label">Sección <span class="requerido">*</span></label>
                                        <select id="cboSeccionHipoteca" name="cboSeccionHipoteca" class="form-select" onchange="configHipotecaria.evalSeccion('cboZonaHipoteca',this.value,'txtDepartamentoHipoteca');" required>
                                            <option selected value="" disabled>seleccione una zona</option>
                                        </select>
                                        <div class='invalid-feedback'>
                                            Seleccione la sección
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='txtDepartamentoHipoteca' class="form-label">Departamento <span class="requerido">*</span></label>
                                        <input type='text' class='form-control' id='txtDepartamentoHipoteca' name='txtDepartamentoHipoteca' value="" readonly required>
                                        <div class='invalid-feedback'>
                                            Ingrese el departamento de la hipoteca
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='numMontoFinalHipoteca' class="form-label">Monto con modificaciones</label>
                                        <input type='number' class='form-control' id='numMontoFinalHipoteca' name='numMontoFinalHipoteca' value="" readonly>
                                        <div class='invalid-feedback'>
                                            Ingrese el monto
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='numPlazoFinalHipoteca' class="form-label">Plazo con modificaciones</label>
                                        <input type='number' class='form-control' id='numPlazoFinalHipoteca' name='numPlazoFinalHipoteca' value="" readonly>
                                        <div class='invalid-feedback'>
                                            Ingrese el plazo
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for='cboPresentaHipoteca' class="form-label">Presentada por <span class="requerido">*</span></label>
                                        <div class="form-group has-validation">
                                            <select id="cboPresentaHipoteca" title="" name="cboPresentaHipoteca" class="form-control selectpicker" data-live-search="true" required>
                                                <option selected value="" disabled>seleccione</option>
                                                <option value="deudor">Deudor/a</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12 col-xl-12 mt-2">
                            <div class="row">
                                <div class="col d-flex align-items-end">
                                    <label class="form-label">Matrícula's</label>
                                </div>
                                <div class="col">
                                    <button type="button" class="btn btn-sm btn-outline-primary float-end" onclick="javascript:configHipotecaria.configMatricula.edit();">
                                        <i class="fas fa-plus-circle"></i> Matrícula
                                    </button>
                                </div>
                            </div>
                            <hr class="mt-0">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table id="tblMatriculas" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Número de matrícula</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-12 mt-2">
                                <div class="row">
                                    <div class="col d-flex align-items-end">
                                        <label class="form-label">Modificaciones</label>
                                    </div>
                                    <div class="col">
                                        <button type="button" class="btn btn-sm btn-outline-primary float-end" onclick="javascript:configHipotecaria.configModificaciones.edit();">
                                            <i class="fas fa-plus-circle"></i> Modificación
                                        </button>
                                    </div>
                                </div>
                                <hr class="mt-0">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="tblModificacionesHipoteca" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                            <thead>
                                                <tr>
                                                    <th>N°</th>
                                                    <th>Fecha de modificación</th>
                                                    <th>Abogado</th>
                                                    <th>Asiento</th>
                                                    <th>Ampliación en monto</th>
                                                    <th>Ampliación en plazo</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal fade" id="mdlAditivo" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmAditivo" name="frmAditivo" accept-charset="utf-8" method="POST" class="needs-validation" novalidate action="javascript:configDatosCredito.configCuota.configAditivos.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Aditivo</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Aditivo -->
                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for='cboAditivo'>Aditivo <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboAditivo" name="cboAditivo" class="selectpicker form-control cboAditivo" required data-live-search="true">
                                    <option selected value="0" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <!-- Monto de aditivo -->
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for="numMontoAditivo">Monto <span class="requerido">*</span></label>
                            <input type="number" id="numMontoAditivo" name="numMontoAditivo" placeholder="" title="" class="form-control" min="0" step="0.01" required value="">
                            <div class="invalid-feedback">
                                Ingrese el monto
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlGarantia" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmGarantia" name="frmGarantia" class="needs-validation" novalidate accept-charset="utf-8" method="POST" action="javascript:configGarantia.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tipo de garantía</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Tipo de garantía -->
                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for='cboTipoGarantia'>Tipo de garantía <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboTipoGarantia" name="cboTipoGarantia" class="selectpicker form-control cboTipoGarantia" required data-live-search="true">
                                    <option selected value="0" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlDesembolsoParcial" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Desembolso</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configDesembolsos.save();" id="frmParcial" name="frmParcial" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for='txtFechaDesembolso' class="form-label">Fecha de desembolso</label>
                            <input type='text' class='form-control fechaInicioLimitado readonly' id='txtFechaDesembolso' name='txtFechaDesembolso' value="" required>
                            <div class='invalid-feedback'>
                                Seleccione una fecha
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-12">
                            <label for='numMontoDesembolso' class="form-label">Monto de desembolso</label>
                            <input type='number' class='form-control' id='numMontoDesembolso' name='numMontoDesembolso' value="" step="0.01" min="0" onkeypress="return generalSoloNumeros(event);" required>
                            <div class='invalid-feedback'>
                                Ingrese el monto del desembolso
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmParcial" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlAportaciones" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Configuración de garantía cuenta de aportaciones</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configAportaciones.save();" id="frmAportaciones" name="frmAportaciones" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <label for='txtCuentaAportaciones' class="form-label">N° de cuenta <span class="requerido">*</span></label>
                            <input type='text' class='form-control cuentaAcacypac' id='txtCuentaAportaciones' name='txtCuentaAportaciones' value="" required>
                            <div class='invalid-feedback'>
                                Ingrese el número de cuenta
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <label for='numPorcentajeAportaciones' class="form-label">Porcentaje de cobertura<span class="requerido">*</span></label>
                            <input type='number' class='form-control' id='numPorcentajeAportaciones' name='numPorcentajeAportaciones' value="" step="0.01" min="0" onkeypress="return generalSoloNumeros(event);" required>
                            <div class='invalid-feedback'>
                                Ingrese el porcentaje
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmAportaciones" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlCDP" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Configuración de garantía CDP</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configCDP.save();" id="frmCDP" name="frmCDP" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <label for='txtCDP' class="form-label">N° de cuenta <span class="requerido">*</span></label>
                            <input type='text' class='form-control cuentaAcacypac' id='txtCDP' name='txtCDP' value="" required>
                            <div class='invalid-feedback'>
                                Ingrese el número de cuenta
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label for='txtCertificadoCDP' class="form-label">N° de certificado <span class="requerido">*</span></label>
                            <input type='text' class='form-control' onkeypress="return generalSoloNumeros(event);" id='txtCertificadoCDP' name='txtCertificadoCDP' value="" required>
                            <div class='invalid-feedback'>
                                Ingrese el número de certificado
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label for='numMontoCDP' class="form-label">Monto de CDP<span class="requerido">*</span></label>
                            <input type='number' class='form-control' id='numMontoCDP' name='numMontoCDP' value="" step="0.01" min="0" onkeypress="return generalSoloNumeros(event);" required>
                            <div class='invalid-feedback'>
                                Ingrese el monto
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label for='txtFechaCreacionCDP' class="form-label">Fecha de creación <span class="requerido">*</span></label>
                            <input type='text' class='form-control fechaFinLimitado readonly' id='txtFechaCreacionCDP' name='txtFechaCreacionCDP' value="" required>
                            <div class='invalid-feedback'>
                                Seleccione la fecha de creación
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label for='txtFechaVencimientoCDP' class="form-label">Fecha de vencimiento <span class="requerido">*</span></label>
                            <input type='text' class='form-control fechaInicioLimitado readonly' id='txtFechaVencimientoCDP' name='txtFechaVencimientoCDP' value="" required>
                            <div class='invalid-feedback'>
                                Seleccione la fecha de vencimiento
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label for='numPorcentajeCDP' class="form-label">Porcentaje de cobertura<span class="requerido">*</span></label>
                            <input type='number' class='form-control' id='numPorcentajeCDP' name='numPorcentajeCDP' value="" step="0.01" min="0" onkeypress="return generalSoloNumeros(event);" required>
                            <div class='invalid-feedback'>
                                Ingrese el porcentaje
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmCDP" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlFiador" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Datos de fiador</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configFiduciaria.save();" id="frmFiador" name="frmFiador" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class='col-lg-3 col-xl-3'>
                            <label for='cboTipoDocumentoFiador' class="form-label">Tipo de documento <span class="requerido">*</span></label>
                            <select id="cboTipoDocumentoFiador" name="cboTipoDocumentoFiador" class="form-select cboTipoDocumento" required onchange="generalMonitoreoTipoDocumento(this.value,'txtNumeroDocumentoFiador');">
                                <option selected value="" disabled>seleccione</option>
                            </select>
                            <div class='invalid-feedback'>
                                Seleccione un tipo de documento
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='txtNumeroDocumentoFiador' class="form-label">Número de documento <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtNumeroDocumentoFiador' name='txtNumeroDocumentoFiador' value="" required readonly>
                            <div class='invalid-feedback'>
                                Ingrese el número de documento
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='txtNITFiador' class="form-label">Número de NIT</label>
                            <input type='text' class='form-control NIT' id='txtNITFiador' name='txtNITFiador' value="">
                            <div class='invalid-feedback'>
                                Ingrese el número de NIT
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtNombresFiador" class="form-label">Nombres de fiador/a <span class="requerido">*</span> </label>
                            <input type="text" id="txtNombresFiador" name="txtNombresFiador" placeholder="" title="nombres" class="form-control" value="" required>
                            <div class="invalid-feedback">
                                Ingrese los nombres del deudor
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtApellidosFiador" class="form-label">Apellidos de fiador/a <span class="requerido">*</span> </label>
                            <input type="text" id="txtApellidosFiador" name="txtApellidosFiador" placeholder="" title="apellidos" class="form-control" value="" required>
                            <div class="invalid-feedback">
                                Ingrese los apellidos del deudor
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtConocidoFiador" class="form-label">Conocido por </label>
                            <input type="text" id="txtConocidoFiador" name="txtConocidoFiador" placeholder="" title="conocido por" class="form-control" value="">
                            <div class="invalid-feedback">
                                Ingrese el conocido por...
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboGeneroFiador' class="form-label">Género <span class="requerido">*</span></label>
                            <select id="cboGeneroFiador" name="cboGeneroFiador" class="form-select cboGenero" required>
                                <option selected value="" disabled>seleccione</option>
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>
                            </select>
                            <div class='invalid-feedback'>
                                Seleccione el género del deudor
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboPaisFiador' class="form-label">Pais de residencia <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboPaisFiador" name="cboPaisFiador" class="selectpicker form-control cboPais" title="" data-live-search="true" required onchange="generalMonitoreoPais(this.value,'cboDepartamentoFiador');">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboDepartamentoFiador' class="form-label">Departamento de residencia <span class="requerido">*</span></label>
                            <div class='form-group has-validation'>
                                <select id="cboDepartamentoFiador" title="" name="cboDepartamentoFiador" class="selectpicker form-control" data-live-search="true" required onchange="generalMonitoreoDepartamento('cboPaisFiador',this.value,'cboMunicipioFiador');">
                                    <option selected value="" disabled>seleccione un país</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboMunicipioFiador' class="form-label">Municipio de residencia <span class="requerido">*</span></label>
                            <div class='form-group has-validation'>
                                <select id="cboMunicipioFiador" title="" name="cboMunicipioFiador" class="selectpicker form-control" data-live-search="true" required>
                                    <option selected value="" disabled>seleccione un departamento</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtProfesionFiador" class="form-label">Profesión <span class="requerido">*</span> </label>
                            <input type="text" id="txtProfesionFiador" name="txtProfesionFiador" placeholder="" title="profesión" class="form-control" value="" required>
                            <div class="invalid-feedback">
                                Ingrese la profesión
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="txtFechaNacimientoFiador" class="form-label">Fecha de nacimiento <span class="requerido">*</span> </label>
                            <input type="text" id="txtFechaNacimientoFiador" name="txtFechaNacimientoFiador" placeholder="YYYY-MM-DD" class="form-control readonly fechaAbierta" value="" onchange="generalAsignarEdad(this.value,'numEdadFiador');" required>
                            <div class="invalid-feedback">
                                Seleccione la fecha de nacimiento
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="numEdadFiador" class="form-label">Edad <span class="requerido">*</span> </label>
                            <input type="number" id="numEdadFiador" name="numEdadFiador" placeholder="" title="edad" class="form-control" value="" readonly required min="0" step="1">
                            <div class="invalid-feedback">
                                Seleccione la fecha de nacimiento
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboFirmaFiador' class="form-label">¿Sabe firmar? <span class="requerido">*</span></label>
                            <select id="cboFirmaFiador" name="cboFirmaFiador" class="form-select cboFirma" data-live-search="true" required>
                                <option selected value="" disabled>seleccione</option>
                                <option value="S">SI</option>
                                <option value="N">NO</option>
                            </select>
                            <div class='invalid-feedback'>
                                Seleccione una opción
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmFiador" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlMatricula" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Matrícula de hipoteca</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configHipotecaria.configMatricula.save();" id="frmMatricula" name="frmMatricula" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for='txtMatriculaHipoteca' class="form-label">Número de matrícula <span class="requerido">*</span></label>
                            <input type='text' class='form-control matriculaHipoteca' id='txtMatriculaHipoteca' name='txtMatriculaHipoteca' value="" required>
                            <div class='invalid-feedback'>
                                Ingrese el número de matrícula
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmMatricula" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlModificacionHipoteca" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modificación de hipoteca</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configHipotecaria.configModificaciones.save();" id="frmModificacionHipoteca" name="frmModificacionHipoteca" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-3 col-xl-3">
                            <label for='txtFechaModificacionHipoteca' class="form-label">Fecha y hora <span class="requerido">*</span></label>
                            <input type='text' class='form-control fechaHoraAbierta readonly' id='txtFechaModificacionHipoteca' name='txtFechaModificacionHipoteca' value="" required>
                            <div class='invalid-feedback'>
                                Seleccione la fecha de la modificación
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for='txtAbogadoModificacion' class="form-label">Nombre de abogado <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtAbogadoModificacion' name='txtAbogadoModificacion' value="" required>
                            <div class='invalid-feedback'>
                                Ingrese el nombre del abogado
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboGeneroAbogadoModificacion' class="form-label">Género de abogado <span class="requerido">*</span></label>
                            <select id="cboGeneroAbogadoModificacion" name="cboGeneroAbogadoModificacion" class="form-select" required>
                                <option selected disabled value="">seleccione</option>
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>
                            </select>
                            <div class='invalid-feedback'>
                                Seleccione una opción
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='txtAsientoModificacionHipoteca' class="form-label">Asiento de registro <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtAsientoModificacionHipoteca' name='txtAsientoModificacionHipoteca' value="" onkeypress="return generalSoloNumeros(event);" required>
                            <div class='invalid-feedback'>
                                Ingrese el número de asiento
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='numMontoModificacionHipoteca' class="form-label">Ampliación en monto</label>
                            <input type='number' class='form-control' id='numMontoModificacionHipoteca' name='numMontoModificacionHipoteca' value="" onkeypress="return generalSoloNumeros(event);" min="0" step="0.01">
                            <div class='invalid-feedback'>
                                Ingrese el monto
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='numPlazoModificacionHipoteca' class="form-label">Ampliación en plazo</label>
                            <input type='number' class='form-control' id='numPlazoModificacionHipoteca' name='numPlazoModificacionHipoteca' value="" onkeypress="return generalSoloNumeros(event);" min="0" step="1">
                            <div class='invalid-feedback'>
                                Ingrese el plazo
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmModificacionHipoteca" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlGeneracion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Generación de documentos</h5>
            </div>
            <div class="modal-body">
                <div class="row pt-2">
                    <div class="col-lg-2 col-xl-2"></div>
                    <div class="col-lg-4 col-xl-4">
                        <div class="d-grid">
                            <button type="button" class="btn btn-warning" title="Hoja de revisión" onclick="configMutuo.generarRevision();">
                                <i class="fas fa-spell-check"> Revisar</i>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xl-4">
                        <div class="d-grid">
                            <button type="button" class="btn btn-success" title="Mutuo" onclick="configMutuo.generarMutuo();">
                                <i class="fas fa-file-pdf"> Mutuo</i>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mt-2" align="center">
                        <span> Intentos restantes: <strong id="strIntentos"></strong></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<?php
$_GET["js"] = ['sgaMutuo'];
