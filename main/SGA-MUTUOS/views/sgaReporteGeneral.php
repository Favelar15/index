<div class="pagetitle">
    <h1>Reporte general de SGA-MUTUOS</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">SGA-MUTUOS</li>
            <li class="breadcrumb-item">Reportería</li>
            <li class="breadcrumb-item active">General</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <form action="javascript:configBusqueda.search();" id="frmReporte" name="frmReporte" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-1 col-xl-1"></div>
                    <div class="col-lg-10 col-xl-10">
                        <div class="row">
                            <div class="col-lg-4 col-xl-4">
                                <label class="form-label" for="cboTipoBusqueda">Tipo de búsqueda <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboTipoBusqueda" name="cboTipoBusqueda" class="selectpicker cboTipoBusqueda form-control" required title="">
                                        <option selected value="agencia">Por agencia</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <label for="txtInicio" class="form-label">Inicio <span class="requerido">*</span></label>
                                <input type="text" id="txtInicio" name="txtInicio" class="form-control fechaFinLimitado readonly" required>
                                <div class="invalid-feedback">
                                    Ingrese la fecha de inicio de búsqueda
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <label for="txtFin" class="form-label">Fin <span class="requerido">*</span></label>
                                <input type="text" id="txtFin" name="txtFin" class="form-control fechaFinLimitado readonly" required>
                                <div class="invalid-feedback">
                                    Ingrese la fecha de fin de búsqueda
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-xl-1"></div>
                    <div class="col-lg-12 col-xl-12 mt-2" align="center">
                        <button type="submit" class="btn btn-sm btn-outline-primary">
                            <i class="fas fa-search"></i> Buscar
                        </button>
                        <button type="button" class="btn btn-sm btn-outline-success" id="btnExcel" disabled onclick="configBusqueda.generarExcel();">
                            <i class="fas fa-file-excel"></i> Excel
                        </button>
                    </div>
                </div>
            </form>
            <hr class="mb-0 mt-2">
        </div>
    </div>
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="mutuos-tab" data-bs-toggle="tab" data-bs-target="#mutuos" type="button" role="tab" aria-controls="mutuos" aria-selected="true">
                Mutuos
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="pagares-tab" data-bs-toggle="tab" data-bs-target="#pagares" type="button" role="tab" aria-controls="pagares" aria-selected="true">
                Pagarés
            </button>
        </li>
    </ul>
    <div class="tab-content pt-2">
        <div class="tab-pane fade show active" id="mutuos" role="tabpanel" aria-labelledby="mutuos-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12" id="divContenedorMutuos">
                    <table id="tblMutuos" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th rowspan="2" colspan="1">Fecha</th>
                                <th colspan="2" rowspan="1">701</th>
                                <th colspan="2" rowspan="1">702</th>
                                <th colspan="2" rowspan="1">703</th>
                                <th colspan="2" rowspan="1">704</th>
                                <th colspan="2" rowspan="1">705</th>
                                <th colspan="2" rowspan="1">706</th>
                                <th colspan="2" rowspan="1">707</th>
                                <th colspan="2" rowspan="1">708</th>
                                <th colspan="2" rowspan="1">709</th>
                                <th colspan="2" rowspan="1">710</th>
                                <th colspan="2" rowspan="1">Total</th>
                            </tr>
                            <tr>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Totales</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="pagares" role="tabpanel" aria-labelledby="pagares-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12" id="divContenedorPagares">
                    <table id="tblPagares" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th rowspan="2" colspan="1">Fecha</th>
                                <th colspan="2" rowspan="1">701</th>
                                <th colspan="2" rowspan="1">702</th>
                                <th colspan="2" rowspan="1">703</th>
                                <th colspan="2" rowspan="1">704</th>
                                <th colspan="2" rowspan="1">705</th>
                                <th colspan="2" rowspan="1">706</th>
                                <th colspan="2" rowspan="1">707</th>
                                <th colspan="2" rowspan="1">708</th>
                                <th colspan="2" rowspan="1">709</th>
                                <th colspan="2" rowspan="1">710</th>
                                <th colspan="2" rowspan="1">Total</th>
                            </tr>
                            <tr>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                                <th rowspan="1" colspan="1" class="no-sort">Cant</th>
                                <th rowspan="1" colspan="1" class="no-sort">Monto</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Totales</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                                <th>0</th>
                                <th>$0.00</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$_GET['js'] = ['sgaReporteGeneral'];
