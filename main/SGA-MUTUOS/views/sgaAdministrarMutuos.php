<div class="pagetitle">
    <h1>Administración de mutuos</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">SGA-MUTUOS</li>
            <li class="breadcrumb-item">Administración de documentos</li>
            <li class="breadcrumb-item active">Mutuos</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <form id="frmBusqueda" name="frmBusqueda" accept-charset="utf-8" method="POST" action="javascript:configBusqueda.search();" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label for="cboAgencia" class="form-label">Agencia <span class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select id="cboAgencia" name="cboAgencia" class="form-control selectpicker cboAgencia" title="" required data-live-search="true">
                                <option selected disabled value="">seleccione</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtInicio" class="form-label">Inicio <span class="requerido">*</span></label>
                        <input type="text" id="txtInicio" name="txtInicio" class="form-control fechaAbierta readonly" required>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtFin" class="form-label">Fin <span class="requerido">*</span></label>
                        <input type="text" id="txtFin" name="txtFin" class="form-control fechaAbierta readonly" required>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <div class="d-grid gap-2">
                            <button type="submit" class="btn btn-primary" style="margin-top: 28.5px;">
                                <i class="fas fa-search"></i> Buscar
                            </button>
                        </div>
                    </div>
                </div>
                <hr class="mb-2 mt-3">
            </form>
        </div>
        <div class="col-lg-12 col-xl-12">
            <table id="tblMutuos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Número de crédito</th>
                        <th>Deudor</th>
                        <th>Monto</th>
                        <th>Plazo</th>
                        <th>Garantías</th>
                        <th>Fecha de documento</th>
                        <th>Fecha de generación</th>
                        <th>Agencia</th>
                        <th>Usuario</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
<div class="modal fade" id="mdlGeneracion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Impresión de documentos</h5>
            </div>
            <div class="modal-body">
                <div class="row pt-2">
                    <div class="col-lg-2 col-xl-2"></div>
                    <div class="col-lg-4 col-xl-4">
                        <div class="d-grid">
                            <button type="button" class="btn btn-warning" title="Hoja de revisión" onclick="configBusqueda.printHojaRevision();">
                                <i class="fas fa-spell-check"> Hoja de revisión</i>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xl-4">
                        <div class="d-grid">
                            <button type="button" class="btn btn-success" title="Mutuo" onclick="configBusqueda.printMutuo();">
                                <i class="fas fa-file-pdf"> Mutuo</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<?php
$_GET["js"] = ['sgaAdministrarMutuos'];
