<div id="principalContainer">
    <div class="sgaContainer" id="mainContainer">
        <div class="pagetitle">
            <div class="row">
                <div class="col">
                    <h1>Generación de resolución y cláusula</h1>
                </div>
                <div class="col-4 topBtnContainer">
                    <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configDocumento.cancel();">
                        <i class="fas fa-times"></i> <span>Cancelar</span>
                    </button>
                    <button class="btn btn-sm btn-outline-success float-end" disabled title="Guardar" type="button" id="btnGenerarDocumentos" onclick="configDocumento.save();">
                        <i class="fas fa-save"></i> <span>Generar documentos</span>
                    </button>
                </div>
            </div>
            <nav>
                <ol class="breadcrumb mb-1">
                    <li class="breadcrumb-item">SGA-MUTUOS</li>
                    <li class="breadcrumb-item">Generación</li>
                    <li class="breadcrumb-item active">Resolución y clausula</li>
                </ol>
            </nav>
        </div>
        <hr class="mb-1 mt-1">
        <section class="section">
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="credito-tab" data-bs-toggle="tab" data-bs-target="#credito" type="button" role="tab" aria-controls="credito" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesCredito');">
                        Datos de crédito
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="deudor-tab" data-bs-toggle="tab" data-bs-target="#deudor" type="button" role="tab" aria-controls="deudor" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesDeudor');">
                        Datos de deudor
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="garantia-tab" data-bs-toggle="tab" data-bs-target="#garantia" type="button" role="tab" aria-controls="garantia" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesGarantia');">
                        Garantía
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link disabled" id="parcial-tab" data-bs-toggle="tab" data-bs-target="#parcial" type="button" role="tab" aria-controls="parcial" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesParcial');">
                        Desembolso parcial
                    </button>
                </li>
                <li class="nav-item ms-auto" id="mainButtonContainer">
                    <div class="tabButtonContainer" id="botonesDeudor" style="display: none;"></div>
                    <div class="tabButtonContainer" id="botonesCredito" style="display: none;"></div>
                    <div class="tabButtonContainer" id="botonesGarantia" style="display: none;">
                        <button type="button" id="btnGarantias" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configGarantia.edit();" disabled>
                            <i class="fas fa-plus-circle"></i> Garantía
                        </button>
                    </div>
                    <div class="tabButtonContainer" id="botonesParcial" style="display: none;">
                        <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="configDesembolsos.edit();">
                            <i class="fas fa-plus-circle"></i> Desembolso
                        </button>
                    </div>
                </li>
            </ul>
            <div class="tab-content pt-2">
                <div class="tab-pane fade show active" id="credito" role="tabpanel" aria-labelledby="credito-tab">
                    <form action="javascript:configDocumento.search();" id="frmCredito" name="frmCredito" method="POST" accept-charset="utf-8" class="needs-validation" novalidate>
                        <div class="row">
                            <div class='col-lg-3 col-xl-3 cuenta-acacypac'>
                                <label for='txtNumeroCredito' class="form-label">Número de crédito <span class="requerido">*</span></label>
                                <div class="input-group has-validation">
                                    <input type='text' class='form-control cuentaAcacypac' id='txtNumeroCredito' name='txtNumeroCredito' value="" required onkeypress="return generalSoloNumeros(event);">
                                    <div class="input-group-append">
                                        <button type="submit" form="frmCredito" title="Buscar mutuo" class="input-group-text btn btn-outline-dark"><i class="fas fa-search"></i> </button>
                                    </div>
                                    <div class='invalid-feedback'>
                                        Ingrese el número de crédito
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboAgencia' class="form-label">Agencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboAgencia" title="seleccione" name="cboAgencia" class="selectpicker form-control cboAgencia" disabled data-live-search="true" required>
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for="numMontoCredito" class="form-label">Monto <span class="requerido">*</span></label>
                                <input type="number" id="numMontoCredito" name="numMontoCredito" class="form-control" placeholder="" readonly title="monto" min="0" step="0.01" required onkeypress="return generalSoloNumeros(event);" value="">
                                <div class="invalid-feedback">
                                    Ingrese el monto total del crédito
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for="numTasaInteres" class="form-label">Tasa de interés <span class="requerido">*</span></label>
                                <input type="number" id="numTasaInteres" name="numTasaInteres" class="form-control" placeholder="" readonly title="tasa de interés" min="0" step="0.01" required onkeypress="return generalSoloNumeros(event);" value="">
                                <div class="invalid-feedback">
                                    Ingrese la tasa de interés
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboLineaFinanciera' class="form-label">Línea financiera <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboLineaFinanciera" name="cboLineaFinanciera" class="selectpicker form-control cboLineaFinanciera" disabled data-live-search="true" required>
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtDestinoCredito" class="form-label">Destino <span class="requerido">*</span></label>
                                <input type="text" id="txtDestinoCredito" name="txtDestinoCredito" class="form-control" placeholder="" readonly title="destino" min="0" step="0.01" required value="">
                                <div class="invalid-feedback">
                                    Ingrese el destino del crédito
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboVencimiento' class="form-label">¿Crédito al vencimiento? <span class="requerido">*</span></label>
                                <select id="cboVencimiento" name="cboVencimiento" class="form-select" required disabled>
                                    <option selected value="" disabled>seleccione</option>
                                    <option value="N">NO</option>
                                    <option value="S">SI</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione una opción
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboPeriodicidad' class="form-label">Periodicidad de pago <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboPeriodicidad" name="cboPeriodicidad" disabled class="selectpicker form-control cboPeriodicidad" data-live-search="true" required>
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3" style="border:1px solid rgba(206, 206, 206, 0.96); border-radius:.3em;">
                                <label class="form-label">Plazo <span class="requerido">*</span></label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label style="display:none;">Plazo</label>
                                        <div class="form-group has-validation">
                                            <input type="number" id="numPlazo" name="numPlazo" readonly placeholder="plazo" title="plazo" min="0" step="1" onkeypress="return generalSoloNumeros(event);" class="form-control" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class='my_profile_setting_input ui_kit_select_search form-group'>
                                            <label style="display:none;">Unidad plazo</label>
                                            <div class="form-group has-validation">
                                                <select id="cboPlazo" name="cboPlazo" disabled class="form-select cboPlazo" required>
                                                    <option selected value="" disabled>seleccione</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboFuenteFondos' class="form-label">Fuente de fondos <span class="requerido">*</span></label>
                                <select id="cboFuenteFondos" name="cboFuenteFondos" class="form-select cboFuenteFondos" required disabled>
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione la fuente de los fondos
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboDesembolsoParcial' class="form-label">¿Desembolso parcial? <span class="requerido">*</span></label>
                                <select id="cboDesembolsoParcial" name="cboDesembolsoParcial" disabled class="form-select" onchange="configDocumento.evalParciales(this.value);" required onchange="">
                                    <option selected value="" disabled>seleccione</option>
                                    <option value="N">NO</option>
                                    <option value="S">SI</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione una opción
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3'>
                                <label for='txtFechaAprobacion' class="form-label">Fecha de aprobación <span class="requerido">*</span></label>
                                <input type='text' class='form-control fechaFinLimitado readonly' disabled id='txtFechaAprobacion' name='txtFechaAprobacion' value="" required>
                                <div class='invalid-feedback'>
                                    Seleccione la fecha
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3'>
                                <label for='txtFechaGeneracion' class="form-label">Fecha de generación <span class="requerido">*</span></label>
                                <input type='text' class='form-control fechaAbierta readonly' disabled id='txtFechaGeneracion' name='txtFechaGeneracion' value="" required>
                                <div class='invalid-feedback'>
                                    Seleccione la fecha
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboComite' class="form-label">Nivel de comité <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboComite" name="cboComite" disabled class="selectpicker form-control cboComite" data-live-search="true" required>
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtNotario" class="form-label">Notario que certifica <span class="requerido">*</span></label>
                                <input type="text" id="txtNotario" name="txtNotario" class="form-control" placeholder="" readonly title="destino" required value="">
                                <div class="invalid-feedback">
                                    Ingrese el nombre del notario
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-12 mt-2">
                                <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                                    <li class="nav-item">
                                        <button class="nav-link active " id="construccionCuota-tab" data-bs-toggle="tab" data-bs-target="#construccionCuota" role="tab" aria-controls="construccionCuota" aria-selected="true">
                                            Construcción de cuota
                                        </button>
                                    </li>
                                    <li class="nav-item ms-auto">
                                        <div class="tabButtonContainerThird" id="btnsCuota">
                                            <button type="button" id="btnAditivos" disabled onclick="configDatosCredito.configCuota.configAditivos.edit();" class="btn btn-outline-primary btn-sm">
                                                <i class="fa fa-plus-circle"></i> Aditivo
                                            </button>
                                        </div>
                                    </li>
                                </ul>
                                <div class="tab-content" style="margin-top: 20px;">
                                    <div class="tab-pane fade show active" id="construccionCuota" role="tabpanel" aria-labelledby="construccionCuota-tab">
                                        <div class="row">
                                            <div class="col-lg-12 col-xl-12">
                                                <div class="row">
                                                    <div class="col-lg-6 col-xl-6">
                                                        <div class="input-group input-group-sm mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Monto capital e intereses: $</span>
                                                            </div>
                                                            <input type="number" id="numCapitalInteres" value="" name="numCapitalInteres" class="form-control" min="0" step="0.01" onkeypress="return generalSoloNumeros(event);" onkeyup="configDatosCredito.configCuota.calcularCuota();" required readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <div class="input-group input-group-sm mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Monto total de cuota: $</span>
                                                            </div>
                                                            <input type="number" disabled id="numMontoTotal" value="0.00" name="numMontoTotal" class="form-control" min="0" step="0.01">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-xl-12">
                                                <table id="tblAditivos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                                    <thead>
                                                        <th>N°</th>
                                                        <th>Aditivo</th>
                                                        <th>Monto</th>
                                                        <th>Opciones</th>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="deudor" role="tabpanel" aria-labelledby="deudor-tab">
                    <form action="javascript:configDocumento.save();" id="frmDeudor" name="frmDeudor" method="POST" accept-charset="utf-8" class="needs-validation" novalidate>
                        <div class="row">
                            <div class='col-lg-3 col-xl-3'>
                                <label for='txtCodigoCliente' class="form-label">Código de cliente <span class="requerido">*</span></label>
                                <input type='text' class='form-control' id='txtCodigoCliente' name='txtCodigoCliente' value="" readonly required onkeypress="return generalSoloNumeros(event);">
                                <div class='invalid-feedback'>
                                    Ingrese el código de cliente
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3'>
                                <label for='cboTipoDocumentoDeudor' class="form-label">Tipo de documento <span class="requerido">*</span></label>
                                <select id="cboTipoDocumentoDeudor" name="cboTipoDocumentoDeudor" class="form-select cboTipoDocumento" disabled required onchange="generalMonitoreoTipoDocumento(this.value,'txtNumeroDocumentoDeudor');">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione un tipo de documento
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3'>
                                <label for='txtNumeroDocumentoDeudor' class="form-label">Número de documento <span class="requerido">*</span></label>
                                <input type='text' class='form-control' id='txtNumeroDocumentoDeudor' name='txtNumeroDocumentoDeudor' value="" required readonly>
                                <div class='invalid-feedback'>
                                    Ingrese el número de documento
                                </div>
                            </div>
                            <div class='col-lg-3 col-xl-3'>
                                <label for='txtNITDeudor' class="form-label">Número de NIT</label>
                                <input type='text' class='form-control NIT' id='txtNITDeudor' name='txtNITDeudor' value="" readonly>
                                <div class='invalid-feedback'>
                                    Ingrese el número de NIT
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtNombresDeudor" class="form-label">Nombres de deudor/a <span class="requerido">*</span> </label>
                                <input type="text" id="txtNombresDeudor" name="txtNombresDeudor" placeholder="" title="nombres" class="form-control" value="" required readonly>
                                <div class="invalid-feedback">
                                    Ingrese los nombres del deudor
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtApellidosDeudor" class="form-label">Apellidos de deudor/a <span class="requerido">*</span> </label>
                                <input type="text" id="txtApellidosDeudor" name="txtApellidosDeudor" placeholder="" title="apellidos" class="form-control" value="" required readonly>
                                <div class="invalid-feedback">
                                    Ingrese los apellidos del deudor
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtConocidoDeudor" class="form-label">Conocido por </label>
                                <input type="text" id="txtConocidoDeudor" name="txtConocidoDeudor" placeholder="" title="conocido por" class="form-control" value="" readonly>
                                <div class="invalid-feedback">
                                    Ingrese el conocido por...
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboGeneroDeudor' class="form-label">Género <span class="requerido">*</span></label>
                                <select id="cboGeneroDeudor" name="cboGeneroDeudor" class="form-select cboGenero" required disabled>
                                    <option selected value="" disabled>seleccione</option>
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                                <div class='invalid-feedback'>
                                    Seleccione el género del deudor
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboPaisDeudor' class="form-label">Pais de residencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboPaisDeudor" name="cboPaisDeudor" class="selectpicker form-control cboPais" title="" data-live-search="true" required disabled onchange="generalMonitoreoPais(this.value,'cboDepartamentoDeudor');">
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboDepartamentoDeudor' class="form-label">Departamento de residencia <span class="requerido">*</span></label>
                                <div class='form-group has-validation'>
                                    <select id="cboDepartamentoDeudor" title="" name="cboDepartamentoDeudor" class="selectpicker form-control" data-live-search="true" disabled title="" required onchange="generalMonitoreoDepartamento('cboPaisDeudor',this.value,'cboMunicipioDeudor');">
                                        <option selected value="" disabled>seleccione un país</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3">
                                <label for='cboMunicipioDeudor' class="form-label">Municipio de residencia <span class="requerido">*</span></label>
                                <div class='form-group has-validation'>
                                    <select id="cboMunicipioDeudor" title="" name="cboMunicipioDeudor" class="selectpicker form-control" data-live-search="true" required disabled>
                                        <option selected value="" disabled>seleccione un departamento</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xl-3 mayusculas">
                                <label for="txtDireccionDeudor" class="form-label">Dirección <span class="requerido">*</span> </label>
                                <input type="text" id="txtDireccionDeudor" name="txtDireccionDeudor" placeholder="" title="profesión" class="form-control" value="" required readonly>
                                <div class="invalid-feedback">
                                    Ingrese la dirección
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="garantia" role="tabpanel" aria-labelledby="garantia-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblGarantias" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                <thead>
                                    <th>N°</th>
                                    <th>Tipo de garantía</th>
                                    <th>Parámetros</th>
                                    <th>¿Configurada?</th>
                                    <th>Opciones</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="parcial" role="tabpanel" aria-labelledby="parcial-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblDesembolsos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Fecha de desembolso</th>
                                        <th>Monto de desembolso</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="sgaContainer" id="fiadoresContainer" style="display: none;">
        <div class="pagetitle">
            <div class="row">
                <div class="col">
                    <h1>Configuración de fiadores</h1>
                </div>
                <div class="col-4 topBtnContainer">
                    <button class="btn btn-sm btn-outline-secondary float-end ms-2" type="button" title="Regresar" onclick="sgaShowScreen('principalContainer','mainContainer');">
                        <i class="fas fa-arrow-left"></i> <span>Regresar</span>
                    </button>
                </div>
            </div>
            <nav>
                <ol class="breadcrumb mb-1">
                    <li class="breadcrumb-item">SGA-MUTUOS</li>
                    <li class="breadcrumb-item">Generar mutuo</li>
                    <li class="breadcrumb-item">Configuración de garantía</li>
                    <li class="breadcrumb-item active">Fiadores</li>
                </ol>
            </nav>
        </div>
        <hr class="mb-1 mt-1">
        <section class="section">
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="fiadores-tab" data-bs-toggle="tab" data-bs-target="#fiadores" type="button" role="tab" aria-controls="fiadores" aria-selected="true" onclick="generalShowHideButtonTab('fiadoresButtonContainer','botonesFiadores');">
                        Fiadores
                    </button>
                </li>
                <li class="nav-item ms-auto" id="fiadoresButtonContainer">
                    <div class="tabButtonContainer" id="botonesFiadores">
                        <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configFiduciaria.edit();">
                            <i class="fas fa-plus-circle"></i> Fiador
                        </button>
                    </div>
                </li>
            </ul>
            <div class="tab-content pt-2">
                <div class="tab-pane fade show active" id="fiadores" role="tabpanel" aria-labelledby="fiadores-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblFiadores" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                <thead>
                                    <th>N°</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="sgaContainer" id="hipotecasContainer" style="display: none;">
        <div class="pagetitle">
            <div class="row">
                <div class="col">
                    <h1>Configuración de hipotecas</h1>
                </div>
                <div class="col-4 topBtnContainer">
                    <button class="btn btn-sm btn-outline-secondary float-end ms-2" type="button" title="Regresar" onclick="sgaShowScreen('principalContainer','mainContainer');">
                        <i class="fas fa-arrow-left"></i> <span>Regresar</span>
                    </button>
                </div>
            </div>
            <nav>
                <ol class="breadcrumb mb-1">
                    <li class="breadcrumb-item">SGA-MUTUOS</li>
                    <li class="breadcrumb-item">Generar mutuo</li>
                    <li class="breadcrumb-item">Configuración de garantía</li>
                    <li class="breadcrumb-item active">Hipotecas</li>
                </ol>
            </nav>
        </div>
        <hr class="mb-1 mt-1">
        <section class="section">
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="hipotecas-tab" data-bs-toggle="tab" data-bs-target="#hipotecas" type="button" role="tab" aria-controls="hipotecas" aria-selected="true" onclick="generalShowHideButtonTab('hipotecasButtonContainer','botonesHipotecas');">
                        Hipotecas
                    </button>
                </li>
                <li class="nav-item ms-auto" id="hipotecasButtonContainer">
                    <div class="tabButtonContainer" id="botonesHipotecas">
                        <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configHipotecaria.edit();">
                            <i class="fas fa-plus-circle"></i> Hipoteca
                        </button>
                    </div>
                </li>
            </ul>
            <div class="tab-content pt-2">
                <div class="tab-pane fade show active" id="hipotecas" role="tabpanel" aria-labelledby="hipotecas-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblHipotecas" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                <thead>
                                    <th>N°</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal fade" id="mdlAditivo" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmAditivo" name="frmAditivo" accept-charset="utf-8" method="POST" class="needs-validation" novalidate action="javascript:configDatosCredito.configCuota.configAditivos.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Aditivo</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Aditivo -->
                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for='cboAditivo'>Aditivo <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboAditivo" name="cboAditivo" class="selectpicker form-control cboAditivo" required data-live-search="true">
                                    <option selected value="0" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <!-- Monto de aditivo -->
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for="numMontoAditivo">Monto <span class="requerido">*</span></label>
                            <input type="number" id="numMontoAditivo" name="numMontoAditivo" placeholder="" title="" class="form-control" min="0" step="0.01" required value="">
                            <div class="invalid-feedback">
                                Ingrese el monto
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlGarantia" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmGarantia" name="frmGarantia" class="needs-validation" novalidate accept-charset="utf-8" method="POST" action="javascript:configGarantia.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tipo de garantía</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Tipo de garantía -->
                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for='cboTipoGarantia'>Tipo de garantía <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboTipoGarantia" name="cboTipoGarantia" class="selectpicker form-control cboTipoGarantia" required data-live-search="true">
                                    <option selected value="0" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlDesembolsoParcial" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Desembolso</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configDesembolsos.save();" id="frmParcial" name="frmParcial" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for='txtFechaDesembolso' class="form-label">Fecha de desembolso</label>
                            <input type='text' class='form-control fechaInicioLimitado readonly' id='txtFechaDesembolso' name='txtFechaDesembolso' value="" required>
                            <div class='invalid-feedback'>
                                Seleccione una fecha
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-12">
                            <label for='numMontoDesembolso' class="form-label">Monto de desembolso</label>
                            <input type='number' class='form-control' id='numMontoDesembolso' name='numMontoDesembolso' value="" step="0.01" min="0" onkeypress="return generalSoloNumeros(event);" required>
                            <div class='invalid-feedback'>
                                Ingrese el monto del desembolso
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmParcial" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlGeneracion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Generación de documentos</h5>
            </div>
            <div class="modal-body">
                <div class="row pt-2">
                    <div class="col-lg-2 col-xl-2"></div>
                    <div class="col-lg-8 col-xl-8">
                        <div class="d-grid">
                            <button type="button" class="btn btn-success" title="Resolución y clausula" onclick="configDocumento.generarDocumentos();">
                                <i class="fas fa-spell-check"> Resolución y clausula</i>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mt-2" align="center">
                        <span> Intentos restantes: <strong id="strIntentos"></strong></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlAportaciones" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Descripción de garantía cuenta de aportaciones</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configAportaciones.save();" id="frmAportaciones" name="frmAportaciones" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for='txtDescripcionAportaciones' class="form-label">Descripción <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtDescripcionAportaciones' name='txtDescripcionAportaciones' value="" required>
                            <div class='invalid-feedback'>
                                Ingrese la descripción
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmAportaciones" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlCDP" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Descripción de garantía CDP</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configCDP.save();" id="frmCDP" name="frmCDP" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for='txtDescripcionCDP' class="form-label">Descripción <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtDescripcionCDP' name='txtDescripcionCDP' value="" required>
                            <div class='invalid-feedback'>
                                Ingrese la descripción
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmCDP" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlFiador" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Descripción de fiador</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configFiduciaria.save();" id="frmFiador" name="frmFiador" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class='col-lg-12 col-xl-12'>
                            <label for='txtDescripcionFiador' class="form-label">Descripción <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtDescripcionFiador' name='txtDescripcionFiador' value="" required>
                            <div class='invalid-feedback'>
                                Ingrese la descripción
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmFiador" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlHipoteca" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Descripción de hipoteca</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configHipotecaria.save();" id="frmHipoteca" name="frmHipoteca" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class='col-lg-12 col-xl-12'>
                            <label for='txtDescripcionHipoteca' class="form-label">Descripción <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtDescripcionHipoteca' name='txtDescripcionHipoteca' value="" required>
                            <div class='invalid-feedback'>
                                Ingrese la descripción
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmHipoteca" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<?php
$_GET["js"] = ["sgaResolucionClausula"];
