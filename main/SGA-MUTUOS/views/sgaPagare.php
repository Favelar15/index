<div class="sgaContainer" id="mainContainer">
    <div class="pagetitle">
        <div class="row">
            <div class="col">
                <h1>Generación de pagaré</h1>
            </div>
            <div class="col-4 topBtnContainer">
                <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configDocumento.cancel();">
                    <i class="fas fa-times"></i> <span>Cancelar</span>
                </button>
                <button class="btn btn-sm btn-outline-success float-end" disabled title="Guardar" type="button" id="btnGenerarDocumentos" onclick="configDocumento.save();">
                    <i class="fas fa-save"></i> <span>Generar documento</span>
                </button>
            </div>
        </div>
        <nav>
            <ol class="breadcrumb mb-1">
                <li class="breadcrumb-item">SGA-MUTUOS</li>
                <li class="breadcrumb-item">Generación</li>
                <li class="breadcrumb-item active">Pagaré</li>
            </ol>
        </nav>
    </div>
    <hr class="mb-1 mt-1">
    <section class="section">
        <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
            <li class="nav-item">
                <button class="nav-link active" id="credito-tab" data-bs-toggle="tab" data-bs-target="#credito" type="button" role="tab" aria-controls="credito" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesCredito');">
                    Datos de crédito
                </button>
            </li>
            <li class="nav-item">
                <button class="nav-link" id="deudor-tab" data-bs-toggle="tab" data-bs-target="#deudor" type="button" role="tab" aria-controls="deudor" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesDeudor');">
                    Datos de deudor
                </button>
            </li>
            <li class="nav-item">
                <button class="nav-link" id="fiadores-tab" data-bs-toggle="tab" data-bs-target="#fiadores" type="button" role="tab" aria-controls="fiadores" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesFiadores');">
                    Fiadores
                </button>
            </li>
            <li class="nav-item ms-auto" id="mainButtonContainer">
                <div class="tabButtonContainer" id="botonesDeudor" style="display: none;"></div>
                <div class="tabButtonContainer" id="botonesCredito" style="display: none;"></div>
                <div class="tabButtonContainer" id="botonesFiadores" style="display: none;">
                    <button type="button" id="btnFiadores" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configFiduciaria.edit();" disabled>
                        <i class="fas fa-plus-circle"></i> Fiador
                    </button>
                </div>
            </li>
        </ul>
        <div class="tab-content pt-2">
            <div class="tab-pane fade show active" id="credito" role="tabpanel" aria-labelledby="credito-tab">
                <form action="javascript:configDocumento.search();" id="frmCredito" name="frmCredito" method="POST" accept-charset="utf-8" class="needs-validation" novalidate>
                    <div class="row">
                        <div class='col-lg-3 col-xl-3 cuenta-acacypac'>
                            <label for='txtNumeroCredito' class="form-label">Número de crédito documento mutuo <span class="requerido">*</span></label>
                            <div class="input-group has-validation">
                                <input type='text' class='form-control cuentaAcacypac' id='txtNumeroCredito' name='txtNumeroCredito' value="" required onkeypress="return generalSoloNumeros(event);">
                                <div class="input-group-append">
                                    <button type="submit" form="frmCredito" title="Buscar mutuo" class="input-group-text btn btn-outline-dark"><i class="fas fa-search"></i> </button>
                                </div>
                                <div class='invalid-feedback'>
                                    Ingrese el número de crédito
                                </div>
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3 cuenta-acacypac'>
                            <label for='txtNumeroCreditoPagare' class="form-label">Número de crédito pagaré <span class="requerido">*</span></label>
                            <div class="input-group has-validation">
                                <input type='text' class='form-control cuentaAcacypac' id='txtNumeroCreditoPagare' name='txtNumeroCreditoPagare' value="" required onkeypress="return generalSoloNumeros(event);" readonly>
                                <div class='invalid-feedback'>
                                    Ingrese el número de crédito
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboAgencia' class="form-label">Agencia <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboAgencia" title="seleccione" name="cboAgencia" class="selectpicker form-control cboAgencia" disabled data-live-search="true" required>
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="numMontoCredito" class="form-label">Monto <span class="requerido">*</span></label>
                            <input type="number" id="numMontoCredito" name="numMontoCredito" class="form-control" placeholder="" readonly title="monto" min="0" step="0.01" required onkeypress="return generalSoloNumeros(event);" value="">
                            <div class="invalid-feedback">
                                Ingrese el monto total del crédito
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="numTasaInteres" class="form-label">Tasa de interés <span class="requerido">*</span></label>
                            <input type="number" id="numTasaInteres" name="numTasaInteres" class="form-control" placeholder="" readonly title="tasa de interés" min="0" step="0.01" required onkeypress="return generalSoloNumeros(event);" value="">
                            <div class="invalid-feedback">
                                Ingrese la tasa de interés
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='txtFechaVencimiento' class="form-label">Fecha de vencimiento <span class="requerido">*</span></label>
                            <input type='text' class='form-control fechaInicioLimitado readonly' disabled id='txtFechaVencimiento' name='txtFechaVencimiento' value="" required>
                            <div class='invalid-feedback'>
                                Seleccione la fecha
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='txtFechaGeneracion' class="form-label">Fecha de generación <span class="requerido">*</span></label>
                            <input type='text' class='form-control fechaAbierta readonly' disabled id='txtFechaGeneracion' name='txtFechaGeneracion' value="" required>
                            <div class='invalid-feedback'>
                                Seleccione la fecha
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="deudor" role="tabpanel" aria-labelledby="deudor-tab">
                <form action="javascript:configDocumento.save();" id="frmDeudor" name="frmDeudor" method="POST" accept-charset="utf-8" class="needs-validation" novalidate>
                    <div class="row">
                        <div class='col-lg-3 col-xl-3'>
                            <label for='txtCodigoCliente' class="form-label">Código de cliente <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtCodigoCliente' name='txtCodigoCliente' value="" readonly required onkeypress="return generalSoloNumeros(event);">
                            <div class='invalid-feedback'>
                                Ingrese el código de cliente
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='cboTipoDocumentoDeudor' class="form-label">Tipo de documento <span class="requerido">*</span></label>
                            <select id="cboTipoDocumentoDeudor" name="cboTipoDocumentoDeudor" class="form-select cboTipoDocumento" disabled required onchange="generalMonitoreoTipoDocumento(this.value,'txtNumeroDocumentoDeudor');">
                                <option selected value="" disabled>seleccione</option>
                            </select>
                            <div class='invalid-feedback'>
                                Seleccione un tipo de documento
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='txtNumeroDocumentoDeudor' class="form-label">Número de documento <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtNumeroDocumentoDeudor' name='txtNumeroDocumentoDeudor' value="" required readonly>
                            <div class='invalid-feedback'>
                                Ingrese el número de documento
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='txtNITDeudor' class="form-label">Número de NIT</label>
                            <input type='text' class='form-control NIT' id='txtNITDeudor' name='txtNITDeudor' value="" readonly>
                            <div class='invalid-feedback'>
                                Ingrese el número de NIT
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtNombresDeudor" class="form-label">Nombres de deudor/a <span class="requerido">*</span> </label>
                            <input type="text" id="txtNombresDeudor" name="txtNombresDeudor" placeholder="" title="nombres" class="form-control" value="" required readonly>
                            <div class="invalid-feedback">
                                Ingrese los nombres del deudor
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtApellidosDeudor" class="form-label">Apellidos de deudor/a <span class="requerido">*</span> </label>
                            <input type="text" id="txtApellidosDeudor" name="txtApellidosDeudor" placeholder="" title="apellidos" class="form-control" value="" required readonly>
                            <div class="invalid-feedback">
                                Ingrese los apellidos del deudor
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtConocidoDeudor" class="form-label">Conocido por </label>
                            <input type="text" id="txtConocidoDeudor" name="txtConocidoDeudor" placeholder="" title="conocido por" class="form-control" value="" readonly>
                            <div class="invalid-feedback">
                                Ingrese el conocido por...
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboGeneroDeudor' class="form-label">Género <span class="requerido">*</span></label>
                            <select id="cboGeneroDeudor" name="cboGeneroDeudor" class="form-select cboGenero" required disabled>
                                <option selected value="" disabled>seleccione</option>
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>
                            </select>
                            <div class='invalid-feedback'>
                                Seleccione el género del deudor
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboPaisDeudor' class="form-label">Pais de residencia <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboPaisDeudor" name="cboPaisDeudor" class="selectpicker form-control cboPais" title="" data-live-search="true" required disabled onchange="generalMonitoreoPais(this.value,'cboDepartamentoDeudor');">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboDepartamentoDeudor' class="form-label">Departamento de residencia <span class="requerido">*</span></label>
                            <div class='form-group has-validation'>
                                <select id="cboDepartamentoDeudor" title="" name="cboDepartamentoDeudor" class="selectpicker form-control" data-live-search="true" disabled title="" required onchange="generalMonitoreoDepartamento('cboPaisDeudor',this.value,'cboMunicipioDeudor');">
                                    <option selected value="" disabled>seleccione un país</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboMunicipioDeudor' class="form-label">Municipio de residencia <span class="requerido">*</span></label>
                            <div class='form-group has-validation'>
                                <select id="cboMunicipioDeudor" title="" name="cboMunicipioDeudor" class="selectpicker form-control" data-live-search="true" required disabled>
                                    <option selected value="" disabled>seleccione un departamento</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtDireccionDeudor" class="form-label">Dirección <span class="requerido">*</span> </label>
                            <input type="text" id="txtDireccionDeudor" name="txtDireccionDeudor" placeholder="" title="profesión" class="form-control" value="" required readonly>
                            <div class="invalid-feedback">
                                Ingrese la dirección
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="txtFechaNacimientoDeudor" class="form-label">Fecha de nacimiento <span class="requerido">*</span> </label>
                            <input type="text" id="txtFechaNacimientoDeudor" name="txtFechaNacimientoDeudor" placeholder="YYYY-MM-DD" class="form-control readonly fechaAbierta" value="" onchange="generalAsignarEdad(this.value,'numEdadDeudor');" required disabled>
                            <div class="invalid-feedback">
                                Seleccione la fecha de nacimiento
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="numEdadDeudor" class="form-label">Edad <span class="requerido">*</span> </label>
                            <input type="number" id="numEdadDeudor" name="numEdadDeudor" placeholder="" title="edad" class="form-control" value="" readonly required min="0" step="1">
                            <div class="invalid-feedback">
                                Seleccione la fecha de nacimiento
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="txtTelefono" class="form-label">Teléfono</label>
                            <input type="text" id="txtTelefono" name="txtTelefono" placeholder="" title="Teléfono" class="form-control telefono" value="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="fiadores" role="tabpanel" aria-labelledby="fiadores-tab">
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <table id="tblFiadores" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                            <thead>
                                <th>N°</th>
                                <th>Documento</th>
                                <th>NIT</th>
                                <th>Nombre completo</th>
                                <th>Edad</th>
                                <th>Dirección</th>
                                <th>País</th>
                                <th>Departamento</th>
                                <th>Municipio</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="mdlFiador" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Datos de fiador</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configFiduciaria.save();" id="frmFiador" name="frmFiador" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class='col-lg-3 col-xl-3'>
                            <label for='cboTipoDocumentoFiador' class="form-label">Tipo de documento <span class="requerido">*</span></label>
                            <select id="cboTipoDocumentoFiador" name="cboTipoDocumentoFiador" class="form-select cboTipoDocumento" required onchange="generalMonitoreoTipoDocumento(this.value,'txtNumeroDocumentoFiador');">
                                <option selected value="" disabled>seleccione</option>
                            </select>
                            <div class='invalid-feedback'>
                                Seleccione un tipo de documento
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='txtNumeroDocumentoFiador' class="form-label">Número de documento <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtNumeroDocumentoFiador' name='txtNumeroDocumentoFiador' value="" required readonly>
                            <div class='invalid-feedback'>
                                Ingrese el número de documento
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='txtNITFiador' class="form-label">Número de NIT</label>
                            <input type='text' class='form-control NIT' id='txtNITFiador' name='txtNITFiador' value="">
                            <div class='invalid-feedback'>
                                Ingrese el número de NIT
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtNombresFiador" class="form-label">Nombres de fiador/a <span class="requerido">*</span> </label>
                            <input type="text" id="txtNombresFiador" name="txtNombresFiador" placeholder="" title="nombres" class="form-control" value="" required>
                            <div class="invalid-feedback">
                                Ingrese los nombres del deudor
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtApellidosFiador" class="form-label">Apellidos de fiador/a <span class="requerido">*</span> </label>
                            <input type="text" id="txtApellidosFiador" name="txtApellidosFiador" placeholder="" title="apellidos" class="form-control" value="" required>
                            <div class="invalid-feedback">
                                Ingrese los apellidos del deudor
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtConocidoFiador" class="form-label">Conocido por </label>
                            <input type="text" id="txtConocidoFiador" name="txtConocidoFiador" placeholder="" title="conocido por" class="form-control" value="">
                            <div class="invalid-feedback">
                                Ingrese el conocido por...
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboPaisFiador' class="form-label">Pais de residencia <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboPaisFiador" name="cboPaisFiador" class="selectpicker form-control cboPais" title="" data-live-search="true" required onchange="generalMonitoreoPais(this.value,'cboDepartamentoFiador');">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboDepartamentoFiador' class="form-label">Departamento de residencia <span class="requerido">*</span></label>
                            <div class='form-group has-validation'>
                                <select id="cboDepartamentoFiador" title="" name="cboDepartamentoFiador" class="selectpicker form-control" data-live-search="true" required onchange="generalMonitoreoDepartamento('cboPaisFiador',this.value,'cboMunicipioFiador');">
                                    <option selected value="" disabled>seleccione un país</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboMunicipioFiador' class="form-label">Municipio de residencia <span class="requerido">*</span></label>
                            <div class='form-group has-validation'>
                                <select id="cboMunicipioFiador" title="" name="cboMunicipioFiador" class="selectpicker form-control" data-live-search="true" required>
                                    <option selected value="" disabled>seleccione un departamento</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 mayusculas">
                            <label for="txtDireccionFiador" class="form-label">Dirección <span class="requerido">*</span> </label>
                            <input type="text" id="txtDireccionFiador" name="txtDireccionFiador" placeholder="" title="profesión" class="form-control" value="" required>
                            <div class="invalid-feedback">
                                Ingrese la dirección
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="txtFechaNacimientoFiador" class="form-label">Fecha de nacimiento <span class="requerido">*</span> </label>
                            <input type="text" id="txtFechaNacimientoFiador" name="txtFechaNacimientoFiador" placeholder="YYYY-MM-DD" class="form-control readonly fechaAbierta" value="" onchange="generalAsignarEdad(this.value,'numEdadFiador');" required>
                            <div class="invalid-feedback">
                                Seleccione la fecha de nacimiento
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="numEdadFiador" class="form-label">Edad <span class="requerido">*</span> </label>
                            <input type="number" id="numEdadFiador" name="numEdadFiador" placeholder="" title="edad" class="form-control" value="" readonly required min="0" step="1">
                            <div class="invalid-feedback">
                                Seleccione la fecha de nacimiento
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmFiador" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlGeneracion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Generación de documentos</h5>
            </div>
            <div class="modal-body">
                <div class="row pt-2">
                    <div class="col-lg-2 col-xl-2"></div>
                    <div class="col-lg-8 col-xl-8">
                        <div class="d-grid">
                            <button type="button" class="btn btn-success" title="Pagaré" onclick="configDocumento.generarDocumentos();">
                                <i class="fas fa-spell-check"> Pagaré</i>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mt-2" align="center">
                        <span> Intentos restantes: <strong id="strIntentos"></strong></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<?php
$_GET["js"] = ["sgaPagare"];
