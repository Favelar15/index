<?php
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (isset($datosDeudor) && isset($datosCredito) && isset($garantias) && isset($incluyeParciales)) {
        require_once 'C:\xampp\htdocs\vendor\autoload.php';
        global $arrMeses;

        //Variables generales
        $tmpFecha = date('d-m-Y', strtotime($datosCredito['fechaGeneracion']));
        $enter1 = "<br>";
        $enter2 = "<br> <br>";
        $nombreCompleto = !empty($datosDeudor['conocido']) ? $datosDeudor['nombres'] . ' ' . $datosDeudor['apellidos'] . ' CONOCIDO POR ' . $datosDeudor['conocido'] : $datosDeudor['nombres'] . ' ' . $datosDeudor['apellidos'];
        $textoDesembolsoParcial = $datosCredito['desembolsoParcial'] == "S" ? "como desembolso parcial," : "";
        $arrComites = explode(' ', mb_strtoupper($datosCredito['labelComite'], 'UTF-8'));
        $txtTipoComite = $datosCredito['comite'] == 1 ? (isset($arrComites[2]) ? $arrComites[2] : $arrComites[1]) : $datosCredito['labelComite'];

        $direccionCompleta = $datosDeudor['direccion'] . ", " . mb_strtoupper($datosDeudor['labelMunicipio'], "UTF-8") . ", " . mb_strtoupper($datosDeudor['labelDepartamento'], "UTF-8");

        //Variables de control
        $cntAditivos = count($datosCredito['aditivos']);
        class MYPDF2 extends TCPDF
        {
            //Page header
            public function Header()
            {
                $bMargin = $this->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $this->AutoPageBreak;
                // disable auto-page-break
                $this->SetAutoPageBreak(false, 0);
                // set bacground image
                $img_file = '../../img/membrete.png';
                $this->Image('@' . file_get_contents($img_file), 0, 0,  217, 279, '', '', '', false, 300);
                // restore auto-page-break status
                $this->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $this->setPageMark();
            }

            // Page footer
            public function Footer()
            {
                // Position at 15 mm from bottom
                $this->SetY(-20);
                // Set font
                $this->SetFont('helvetica', 'I', 8);
                // Page number
                $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            }
        }

        $pdf = new MYPDF2(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Departamento IT');
        $pdf->SetTitle('Cláusula');
        $pdf->SetSubject('ACACYPAC');
        $pdf->SetKeywords('TCPDF, PDF, Mutuos');

        // Fuente de cabecera y pie de página
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // Espacios por defecto
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //Margenes
        $pdf->SetMargins(15, 25, 25);
        $pdf->SetHeaderMargin(14);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // Configurar Auto salto de página
        $pdf->SetAutoPageBreak(TRUE, 35);

        // Factor de escalado de imagen
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // Configurando lenguaje
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->SetFont('dejavusans', '', 8.5);

        // add a page
        $pdf->AddPage();
        $pdf->SetMargins(15, 50, 15);

        $incluyeSeguro = false;

        foreach ($datosCredito['aditivos'] as $key => $aditivo) {
            $tmpLabel =  mb_strtolower($aditivo['aditivo'], "UTF-8");
            if (strpos($tmpLabel, "seguro") !== false) {
                $incluyeSeguro = true;
            }
        }

        $parrafo1 = "Yo, " . $nombreCompleto . ", miembro de la Asociación Cooperativa de Ahorro, Crédito y Producción Agropecuaria Comunal,de Nueva Concepción de responsabilidad limitada, ACACYPAC N.C. DE R.L., del municipio de Nueva Concepción, departamento de Chalatenango, manifiesto que con relación al crédito solicitado por la suma de $" . number_format($datosCredito['monto'], 2, '.', ',') . " en la línea de " . $datosCredito['labelLineaFinanciera'] . ", por este medio y actuando bajo responsabilidad propia, libre y espontáneamente asumo y autorizo a La Coopertativa ACACYPAC N.C. DE R.L. para que proceda conforme a mi voluntad en los aspectos mencionados a continuación:";
        $pdf->Write(0, $parrafo1 . "\n", '', 0, 'J', true, 0, false, false, 0);

        if ($incluyeSeguro) {
            $parrafo2 = "1. Autorizo contratar el servicio de seguro de deuda con la empresa Aseguradora Seguros Futuro AC de R.L. para asegurar en caso de invalidez permanente o fallecimiento; el valor y saldo del préstamo que gestiono por la suma de $" . number_format($datosCredito['monto'], 2, '.', ',') . ".";
            $parrafo3 = "2. Por medio de la presente comunico a ustedes que he designado y autorizado al Notario LIC. " . $datosCredito['notario'] . " para que pueda legalizar las correspondientes escrituras y documentos legales relacionados con el crédito gestionado por un monto de $" . number_format($datosCredito['monto'], 2, '.', ',') . ".";
            $pdf->SetMargins(25, 50, 15);
            $pdf->writeHTML($enter2, true, false, true, false, '');
            $pdf->Write(0, $parrafo2 . "\n", '', 0, 'J', true, 0, false, false, 0);
            $pdf->writeHTML($enter2, true, false, true, false, '');
            $pdf->Write(0, $parrafo3 . "\n", '', 0, 'J', true, 0, false, false, 0);
        } else {
            $parrafo4 = "1. Por medio de la presente comunico a ustedes que he designado y autorizado al Notario LIC. " . $datosCredito['notario'] . " para que pueda legalizar las correspondientes escrituras y documentos legales relacionados con el crédito gestionado por un monto de $" . number_format($datosCredito['monto'], 2, '.', ',') . ".";
            $pdf->SetMargins(25, 50, 15);
            $pdf->writeHTML($enter2, true, false, true, false, '');
            $pdf->Write(0, $parrafo4 . "\n", '', 0, 'J', true, 0, false, false, 0);
        }
        $pdf->SetMargins(15, 50, 15);

        $pdf->writeHTML($enter2, true, false, true, false, '');
        $parrafo5 = "Manifiesto y declaro que en todos los aspectos anteriores no he tenido ninguna limitación ni dilación directa o indirecta de parte de La Cooperativa o de sus funcionarios, ejecutivos o empleados, para otorgar la presente autorización.";
        $pdf->Write(0, $parrafo5 . "\n", '', 0, 'J', true, 0, false, false, 0);
        $pdf->writeHTML("<br><br><br><br><br>", true, false, true, false, '');
        $pdf->Write(0, $nombreCompleto, '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML("<br>", true, false, true, false, '');
        $pdf->Write(0, "SOLICITANTE                                                                                                                             FIRMA", '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML("<br><br>", true, false, true, false, '');
        $pdf->Write(0, $datosDeudor['labelTipoDocumento'] . " : " . $datosDeudor['numeroDocumento'], '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML("<br>", true, false, true, false, '');
        $pdf->Write(0, "NIT: " . $datosDeudor['nit'], '', 0, '', true, 0, false, false, 0);

        $mypdf = __DIR__ . '\..\docs\Clausula\/' . $nombreClausula . '';
        $pdf->Output($mypdf, 'F');
    }
}
