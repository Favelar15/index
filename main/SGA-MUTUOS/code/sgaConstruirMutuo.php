<?php
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (isset($datosDeudor) && isset($datosCredito) && isset($datosFirmante) && isset($garantias) && isset($incluyeFirmante) && isset($incluyeParciales)) {
        require_once 'C:\xampp\htdocs\vendor\autoload.php';
        global $arrMeses;

        $nombreUsuario = $_SESSION["index"]->nombres . ' ' . $_SESSION["index"]->apellidos;
        $departamentoAgencia = $_SESSION["index"]->agenciaActual->departamento;
        $municipioAgencia = $_SESSION["index"]->agenciaActual->municipio;

        $cantidadFolios = 0;

        $tituloGarantia = "MUTUO LINEA ESPECIAL SIN FIADOR";

        class MYPDF extends TCPDF
        {

            //Page header
            public function Header()
            {
                $bMargin = $this->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $this->AutoPageBreak;
                // disable auto-page-break
                $this->SetAutoPageBreak(false, 0);
                $this->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $this->setPageMark();
            }

            // Page footer
            public function Footer()
            {
                // Position at 15 mm from bottom
                // $this->SetY(-20);
                // Set font
                $this->SetFont('helvetica', 'I', 8);
            }
        }

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Departamento IT');
        $pdf->SetTitle('Documento de mutuo');
        $pdf->SetSubject('ACACYPAC');
        $pdf->SetKeywords('TCPDF, PDF, Mutuos');

        // Fuente de encabezado y pie de página
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // Espacios por defecto
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //Margenes
        $pdf->SetMargins(30, 25, 25);
        $pdf->SetHeaderMargin(7);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // Configurar Auto salto de página
        $pdf->SetAutoPageBreak(TRUE, 28);

        // Factor de escalado de imagen
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // Configurando lenguaje
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // Establecer fuente
        $pdf->SetFont('dejavusans', '', 6);

        $template = file_get_contents("../docs/Plantilla/mutuo.json");
        $plantilla = json_decode($template, false);

        $incluyeGarantia = false;

        //Variables con datos de  deudor/a
        $nombreDeudor = $datosDeudor["nombres"] . ' ' . $datosDeudor["apellidos"];
        !empty($datosDeudor["conocido"]) && $nombreDeudor .= ' CONOCIDO POR ' . $datosDeudor["conocido"];
        $edadDeudorLetras = mb_strtolower(NumeroALetras::convertir($datosDeudor["edad"]));
        $profesionDeudor = $datosDeudor["profesion"];
        $municipioDeudor = $datosDeudor["labelMunicipio"];
        $departamentoDeudor = $datosDeudor["labelDepartamento"];
        $documentoDeudorLetras = mb_strtolower(documentoALetras(str_split($datosDeudor["numeroDocumento"])), "UTF-8");
        $nitDeudor = !empty($datosDeudor["nit"]) ? mb_strtolower(documentoALetras(str_split($datosDeudor["nit"])), "UTF-8") : $documentoDeudorLetras;
        $documentoDeudor = !empty($datosDeudor['nit']) ? 'número ' . $documentoDeudorLetras . ' y con Número de Identificación Tributaria ' . $nitDeudor : 'y Número de Identificación Tributaria ' . $documentoDeudorLetras;
        $generoDeudor = definicionGeneroSingular("DEUDOR", $datosDeudor["genero"]);
        $enteradoDeudor = $datosDeudor["genero"] == "M" ? "enterado" : "enterada";

        $tmpFechaGeneracion = explode(' ', $datosCredito["fechaGeneracion"]);

        $fechaGeneracion = explode('-', $tmpFechaGeneracion[0]);
        $mesGeneracionLetras = $arrMeses[intval($fechaGeneracion[1])];

        $fechaGeneracionLetra = generalFechaLetras($fechaGeneracion[0], $fechaGeneracion[1], $fechaGeneracion[2]);
        $fechaHoraGeneracionLetra = 'a las ' . generalHoraLetras($tmpFechaGeneracion[1]) . ' del día ' . generalFechaLetras($fechaGeneracion[0], $fechaGeneracion[1], $fechaGeneracion[2]);

        //Variables con datos de crédito
        $montoCreditoLetras = conversionDineroLetras($datosCredito["monto"]);
        $tasaInteresLetras = conversionNumeroLetras($datosCredito["tasaInteres"]);
        $lineaFinanciera = $datosCredito["labelLineaFinanciera"];
        $destinoCredito = $lineaFinanciera . ": " . $datosCredito["destino"];
        $fuente = $datosCredito["labelFuenteFondos"];
        $fuenteFondos = $fuente == "ACACYPAC DE RL" ? "propios de <b>La Cooperativa</b>" : "de <b>" . $fuente . "</b>";
        $plazoNumero = $datosCredito["plazo"];
        $plazoUnidad = $datosCredito["labelTipoPlazo"];
        $periodicidad = $datosCredito["labelPeriodicidad"];
        $plazoLetras = NumeroALetras::convertir($plazoNumero) . " " . $plazoUnidad;
        $creditoVencimiento = $datosCredito["vencimiento"];

        $incluyeSeguro = false;

        $detalleAditivos = array_merge([['aditivo' => 'Capital e intereses', 'monto' => $datosCredito["capitalInteres"]]], $datosCredito["aditivos"]);

        foreach ($datosCredito["aditivos"] as $aditivo) {
            if (strpos(mb_strtolower($aditivo["aditivo"], "UTF-8"), "seguro") !== false && floatval($aditivo["monto"]) > 0) {
                $incluyeSeguro = true;
            }
        }

        $definicionPlazo = construccionDefinicionPlazo($plazoNumero, $plazoUnidad, $periodicidad, $creditoVencimiento, $detalleAditivos);

        $fechaVencimientoLetras = "";

        $definicionGarantiasPrivada = [];
        $definicionGarantiasLegal = [];

        //Incluir clausula de seguro si aplica
        $clausulaSeguroPrivada = $incluyeSeguro ? clausulaPrivadaSeguro : "";
        $clausulaSeguroLegal = $incluyeSeguro ? clausulaLegalSeguro : "";

        //Variables abierta a cambios si hay mas participantes
        $tituloParticipantes = "{titulo_deudor}";

        $obligacionParticipantesPrivada = "me obligo";
        $obligacionParticipantesLegal = "se obliga";

        $aceptacionParticipantesPrivada = "acepto";
        $aceptacionParticipantesLegal = "acepta";

        $senialaParticipantesPrivada = "señalo";
        $senialaParticipantesLegal = "señala";

        $someteParticipantesPrivada = "me someto";
        $someteParticipantesLegal = "somete";

        $expresionParticipantesPrivada = "me expreso";

        $firmaParticipantesPrivada = "firmo el presente documento de mutuo";
        $firmaParticipantesLegal = "firmamos";

        $complementoPresentacionLegal = "<b>COMPARECE: </b>" . $generoDeudor->senior . " <b>" . $nombreDeudor . "</b>, de " . $edadDeudorLetras . " años de edad, " . $profesionDeudor . ", del domicilio de " . $municipioDeudor . ", departamento de " . $departamentoDeudor . ", a quien no conozco, pero identifico por su Documento Único de Identidad " . $documentoDeudor . "; en adelante denominado <b>\"{titulo_deudor}\"</b> y <b>ME DICE: </b>";

        $reconocimientoFirmaLegal = "Que reconoce como suya la firma estampada en el anterior contrato, por lo cual <b>{titulo_deudor}</b> declara";

        $reconocimientoFirmaParticipantesLegal = "Que la firma reconocida es AUTÉNTICA, por haber sido puesta a mi presencia de su puño y letra por el otorgante, a quien no conozco, pero identifiqué por medio de su Documento Único de Identidad ya relacionado";

        $expresaParticipantesLegal = "expresó " . $generoDeudor->compareciente . " a quien";

        $leLegal = "le hube";

        $manifiestaParticipantesLegal = "manifiesta";

        $ratificaParticipantesLegal = "ratifica";

        $desembolsosParcialesPrivada = "";
        $desembolsosParcialesLegal = "";

        $incluyeFiadores = false;
        $incluyeHipotecas = false;

        foreach ($garantias as $garantia) {
            if ($garantia["idTipoGarantia"] == '5') {
                $incluyeFiadores = true;
            } elseif ($garantia["idTipoGarantia"] == '6') {
                $incluyeHipotecas = true;
            }
        }

        if ($incluyeFirmante) {
            $nombreCompletoFirmante = $datosFirmante["nombres"] . " " . $datosFirmante["apellidos"];
            $edadFirmanteLetras = mb_strtolower(NumeroALetras::convertir($datosFirmante["edad"]), 'UTF-8');
            $municipioFirmante = $datosFirmante["labelMunicipio"];
            $departamentoFirmante = $datosFirmante["labelDepartamento"];
            $documentoFirmanteLetras = mb_strtolower(documentoALetras(str_split($datosFirmante["numeroDocumento"])), "UTF-8");
            $profesionFirmante = $datosFirmante["profesion"];
            $datosFirmanteRuego = $nombreCompletoFirmante . ", de " . $edadFirmanteLetras . " años de edad, " . $profesionFirmante . ", del domicilio de " . $municipioFirmante . ", departamento de " . $departamentoFirmante . ", con Documento Único de Identidad número " . $documentoFirmanteLetras;

            $reconocimientoFirmaLegal = "Que reconoce como suya la huella estampada en el anterior contrato, por lo cual <b>{titulo_deudor}</b> declara";
            $firmaParticipantesPrivada = "por no saber firmar, dejo impresa la huella dactilar del dedo pulgar de mi mano derecha y a mi ruego firma " . $datosFirmanteRuego;
            $firmaParticipantesLegal = "firmamos, no así <b>{titulo_deudor}}</b>, por no saber hacerlo pero deja impresa la huella dactilar del dedo pulgar de su mano derecha y a su ruego lo hace en una sola firma " . $datosFirmanteRuego;
        }

        if ($incluyeParciales) {
            $datosDesembolsos = $datosCredito["detalleDesembolsos"];
            $tmpAceptaPrivada = $incluyeFiadores ? "aceptamos" : "acepto";
            $tmpAceptaLegal = $incluyeFiadores ? "aceptan" : "acepta";
            $cantidadDesembolsos = 0;
            $detalleDesembolsos = [];

            foreach ($datosDesembolsos as $datosDesembolso) {
                $cantidadDesembolsos++;
                $montoDesembolsoLetras = trim(conversionDineroLetras($datosDesembolso["monto"]));
                $fechaDesembolso = explode("-", $datosDesembolso["fecha"]);
                $fechaDesembolsoLetras = generalFechaLetras($fechaDesembolso[0], $fechaDesembolso[1], $fechaDesembolso[2]);
                $nomenclaturaDesembolso = new tmpNomenclatura("M", $cantidadDesembolsos);
                if ($cantidadDesembolsos == 1) {
                    array_push($detalleDesembolsos, "primer desembolso con la firma del presente instrumento por la cantidad de " . $montoDesembolsoLetras);
                } else {
                    array_push($detalleDesembolsos, $nomenclaturaDesembolso->nomenclatura . " desembolso programado en el día " . $fechaDesembolsoLetras . ", por la cantidad de " . $montoDesembolsoLetras);
                }
            }

            $cantidadDesembolsosLetras = trim(mb_strtolower(NumeroALetras::convertir($cantidadDesembolsos), "UTF-8"));
            $definicionDesembolsos = implode("; ", $detalleDesembolsos);
            $clausulaDesembolsos = "ACACYPAC, N.C. DE R.L. a su libre opción podrá en cualquier momento, durante la vigencia del presente contrato, suspender su uso siempre y cuando se den las siguientes condicionantes: a) que el deudor incurra en una de las causales de caducidad del plazo; b) en caso de encontrar inexactitudes o incongruencias en la información presentada a ACACYPAC N.C. DE R.L. para el análisis u otorgamiento del crédito, su seguimiento o futura evaluación; c) La existencia de una ley, regulación, orden o decreto que restrinja o prohíba la presente transacción o su recuperación por la vía judicial. En todo caso cada desembolso deberá ser autorizado por ACACYPAC, N.C DE R.L, quedando este facultado para suspender el uso de la presente Apertura de Crédito.";

            $desembolsosParcialesPrivada = "<b>CLAUSULA ESPECIAL: DESEMBOLSOS PARCIALES. </b>Por medio del presente instrumento {titulo_participantes} " . $tmpAceptaPrivada . " los " . $cantidadDesembolsosLetras . " desembolsos parciales, detallados de la siguiente manera: " . implode("; ", $detalleDesembolsos) . ". ";
            $desembolsosParcialesLegal = "<b>CLAUSULA ESPECIAL: DESEMBOLSOS PARCIALES. </b>Por medio del presente instrumento {titulo_participantes} " . $tmpAceptaLegal . " los " . $cantidadDesembolsosLetras . " desembolsos parciales, detallados de la siguiente manera: " . implode("; ", $detalleDesembolsos) . ". ";

            $desembolsosParcialesPrivada .= $clausulaDesembolsos;
            $desembolsosParcialesLegal .= $clausulaDesembolsos;
        }

        //Verificación de garantia
        foreach ($garantias as $garantia) {
            if ($garantia["idTipoGarantia"] != '1') {
                $incluyeGarantia = true;
                switch (intval($garantia["idTipoGarantia"])) {
                    case 6:
                        $tituloGarantia = "MUTUO GARANTIZADO CON HIPOTECA ABIERTA";
                        $parrafoPrivada = $incluyeFiadores ? "Además el presente crédito queda garantizado con " : "El presente crédito queda garantizado con ";
                        $parrafoLegal = $incluyeFiadores ? "Además el presente crédito queda garantizado con " : "El presente crédito queda garantizado con ";

                        $arregloDatosHipotecas = [];

                        foreach ($garantia["configuracion"] as $datosHipoteca) {
                            $quienPresenta = $incluyeFiadores ? ($datosHipoteca["presenta"] == 'Deudor/a' ? $nombreDeudor : $datosHipoteca["presenta"]) : "mi persona";
                            $fechaHoraEmision = explode(" ", $datosHipoteca["fechaEmision"]);
                            $fechaEmision = explode("-", $fechaHoraEmision[0]);
                            $fechaHoraEmisionLetra = generalHoraLetras($fechaHoraEmision[1]) . ' del día ' . generalFechaLetras($fechaEmision[0], $fechaEmision[1], $fechaEmision[2]);
                            $generoAbogado = definicionGeneroSingular("ABOGADO", $datosHipoteca["generoAbogado"]);
                            $nombreAbogado = $datosHipoteca["abogado"];
                            $montoHipoteca = floatval($datosHipoteca["monto"]);
                            $montoHipotecaLetras = trim(conversionDineroLetras($datosHipoteca["monto"]));
                            $plazoHipoteca = intval($datosHipoteca["plazo"]);
                            $plazoHipotecaLetras = trim(NumeroALetras::convertir($datosHipoteca["plazo"]));
                            $asientoLetras = trim(mb_strtolower(NumeroALetras::convertir($datosHipoteca["asiento"])), "UTF-8");
                            $asientoLetras = $asientoLetras == "un" ? "uno" : $asientoLetras;
                            $seccionHipoteca = $datosHipoteca["labelSeccion"];
                            $zonaHipoteca = $datosHipoteca["labelZona"];
                            $departamentoHipoteca = $datosHipoteca["departamento"];

                            $arrMatriculas = [];
                            foreach ($datosHipoteca["matriculas"] as $matricula) {
                                // echo $matricula["matricula"];
                                $arrMatriculas[] = '<b>' . trim(mb_strtolower(documentoALetras(str_split($matricula["matricula"])), 'UTF-8')) . '</b>';
                            }

                            $matriculaLetras = '';

                            if (count($arrMatriculas) > 1) {
                                $ultimaMatricula = $arrMatriculas[count($arrMatriculas) - 1];
                                array_pop($arrMatriculas);
                                $matriculaLetras = 'las Matrículas número ' . implode(', ', $arrMatriculas) . ' y ' . $ultimaMatricula;
                            } else {
                                $matriculaLetras = 'la Matrícula número ' . implode(', ', $arrMatriculas);
                            }

                            $parrafoHipoteca = "Hipoteca Abierta, otorgada por  <b>" . $quienPresenta . "</b>, en esta ciudad a las " . $fechaHoraEmisionLetra . ", ante los oficios notariales de " . $generoAbogado->licenciado . "  <b>" . $nombreAbogado . "</b>, para cubrir créditos hasta por un monto máximo de <b>" . $montoHipotecaLetras . "</b>, para el plazo de <b>" . $plazoHipotecaLetras . " AÑOS</b>, inscrita a Favor de La Cooperativa, bajo " . $matriculaLetras . ", asiento número " . $asientoLetras . " del Registro de la Propiedad Raíz e Hipotecas de la " . $seccionHipoteca . " sección de " . $zonaHipoteca . ", departamento de " . $departamentoHipoteca;

                            $cantidadModificaciones = 0;

                            $arreglodatosModificaciones = [];

                            foreach ($datosHipoteca["modificaciones"] as $datosModificacion) {
                                $cantidadModificaciones++;

                                $nomenclaturaModificacion = new tmpNomenclatura("F", $cantidadModificaciones);
                                $fechaHoraModificacion = explode(" ", $datosModificacion["fecha"]);
                                $fechaModificacion = explode("-", $fechaHoraModificacion[0]);
                                $fechaHoraModificacionLetra = generalHoraLetras($fechaHoraModificacion[1]) . ' del día ' . generalFechaLetras($fechaModificacion[0], $fechaModificacion[1], $fechaModificacion[2]);
                                $generoAbogadoModificacion = definicionGeneroSingular("ABOGADO", $datosModificacion["generoAbogado"]);
                                $nombreAbogadoModificacion = $datosModificacion["abogado"];
                                $montoModificacion = !empty($datosModificacion["monto"]) ? floatval($datosModificacion["monto"]) : 0;
                                $plazoModificacion = !empty($datosModificacion["plazo"]) ? intval($datosModificacion["plazo"]) : 0;
                                $asientoModificacionLetras = trim(mb_strtolower(NumeroALetras::convertir($datosModificacion["asiento"]), "UTF-8"));
                                $asientoModificacionLetras = $asientoModificacionLetras == "un" ? "uno" : $asientoModificacionLetras;

                                $parrafoModificacion = "Hipoteca modificada por " . $nomenclaturaModificacion->nomenclatura . " vez según Escritura Pública, otorgada en esta ciudad, a las " . $fechaHoraModificacionLetra . ", ante los oficios notariales de " . $generoAbogadoModificacion->licenciado . " <b>" . $nombreAbogadoModificacion . "</b>, ";

                                if ($montoModificacion > 0) {
                                    $montoModificacionLetras = trim(conversionDineroLetras($datosModificacion["monto"]));
                                    $montoHipoteca += $montoModificacion;
                                    $montoHipotecaLetrasNuevo = trim(conversionDineroLetras($montoHipoteca));

                                    $parrafoModificacion .= " ampliando su monto en " . $montoModificacionLetras . ", por lo que su nuevo monto a cubrir será de  <b>" . trim($montoHipotecaLetrasNuevo) . "</b>, ";
                                }

                                if ($plazoModificacion > 0) {
                                    $plazoModificacionLetras = trim(NumeroALetras::convertir($datosModificacion["plazo"]));
                                    $plazoHipoteca += $plazoModificacion;
                                    $plazoHipotecaLetrasNuevo = trim(NumeroALetras::convertir($plazoHipoteca));

                                    $tmpComplemento = $montoModificacion > 0 ? " y " : "";

                                    $parrafoModificacion .= $tmpComplemento . "ampliando su plazo en " . $plazoModificacionLetras . " AÑOS, por lo que su nuevo plazo es de " . $plazoHipotecaLetrasNuevo . " AÑOS, ";
                                }

                                $parrafoModificacion .= "inscrito bajo el asiento número " . $asientoModificacionLetras;


                                array_push($arreglodatosModificaciones, $parrafoModificacion);
                            }

                            if (count($arreglodatosModificaciones) > 0) {
                                $parrafoHipoteca .= ". " . implode(". ", $arreglodatosModificaciones);
                            }

                            array_push($arregloDatosHipotecas, $parrafoHipoteca);
                        }

                        if (count($arregloDatosHipotecas) > 1) {
                            $ultimaHipoteca = $arregloDatosHipotecas[count($arregloDatosHipotecas) - 1];
                            array_pop($arregloDatosHipotecas);

                            $parrafoPrivada .= implode(". ", $arregloDatosHipotecas) . ". Y por " . $ultimaHipoteca;
                            $parrafoLegal .= implode(". ", $arregloDatosHipotecas) . ". Y por " . $ultimaHipoteca;
                        } else {
                            $parrafoPrivada .= implode("", $arregloDatosHipotecas);
                            $parrafoLegal .= implode("", $arregloDatosHipotecas);
                        }

                        $tmpAceptaPrivada = $incluyeFiadores ? "aceptamos" : "acepto";
                        $tmpAceptaLegal = $incluyeFiadores ? "aceptan" : "acepta";

                        if (!$incluyeFiadores) {
                            $parrafoPrivada .= ".  <b>{titulo_participantes}</b> " . $tmpAceptaPrivada . " que La Cooperativa pueda dar por terminado el plazo de la presente obligación por incumplimiento de cualquiera de las estipulaciones de este contrato";
                            $parrafoLegal .= ".  <b>{titulo_participantes}</b> " . $tmpAceptaLegal . " que La Cooperativa pueda dar por terminado el plazo de la presente obligación por incumplimiento de cualquiera de las estipulaciones de este contrato";
                        }

                        array_push($definicionGarantiasPrivada, $parrafoPrivada);
                        array_push($definicionGarantiasLegal, $parrafoLegal);
                        break;
                    case 5:
                        $tituloGarantia = "MUTUO CON GARANTIA FIDUCIARIA";
                        if ($incluyeHipotecas) {
                            $tituloGarantia .= " Y CON GARANTIA HIPOTECARIA";
                        }

                        $llamadoFiadores = definicionLlamadoParticipantes($garantia["configuracion"]);

                        $comparecientes = $datosDeudor["genero"] == "F" && ($llamadoFiadores->fiador == "FIADORA" || $llamadoFiadores->fiador == "FIADORAS") ? "las comparecientes" : "los comparecientes";
                        $otorgantes = $datosDeudor["genero"] == "F" && ($llamadoFiadores->fiador == "FIADORA" || $llamadoFiadores->fiador == "FIADORAS") ? "las otorgantes" : "los otorgantes";
                        $seniores = $datosDeudor["genero"] == "F" && ($llamadoFiadores->fiador == "FIADORA" || $llamadoFiadores->fiador == "FIADORAS") ? "las señoras" : "los señores";
                        $losParticipantes = $datosDeudor["genero"] == "F" && ($llamadoFiadores->fiador == "FIADORA" || $llamadoFiadores->fiador == "FIADORAS") ? "las" : "los";

                        $presentacionFiadoresLegal = "";
                        $presentacionFiadoresGarantia = "";

                        $datosFiadores = [];
                        $nomenclaturasFiadores = [];
                        $cantidadFiadores = 0;
                        $posicionFiador = 2;

                        $nomenclaturasFiadoresLegal = "";
                        $tmpNomenclaturasFiadores = [];

                        $documentosNoFirmantes = [];

                        $datosNoFirmantes = [];

                        $arregloNomenclaturasNoFirmantes = [];
                        $arregloNomenclaturasFirmantes = [];

                        if ($datosDeudor["firma"] == "N") {
                            array_push($documentosNoFirmantes, $datosDeudor["numeroDocumento"]);
                            $datosNoFirmantes[$datosDeudor["numeroDocumento"]] = new tmpNoFirmante($datosDeudor["numeroDocumento"], $nombreDeudor, $generoDeudor->nomenclatura, $datosDeudor["genero"]);
                            array_push($arregloNomenclaturasNoFirmantes, $generoDeudor->nomenclatura);
                        } else {
                            array_push($arregloNomenclaturasFirmantes, $generoDeudor->nomenclatura);
                        }

                        foreach ($garantia["configuracion"] as $datosFiador) {
                            $cantidadFiadores++;
                            //Datos de fiador
                            $nombreCompletoFiador = $datosFiador["nombres"] . " " . $datosFiador["apellidos"];
                            !empty($datosFiador["conocido"]) && $nombreCompletoFiador .= " CONOCIDO POR " . $datosFiador["conocido"];
                            $edadFiadorLetras = mb_strtolower(NumeroALetras::convertir($datosFiador["edad"]));
                            $profesionFiador = $datosFiador["profesion"];
                            $municipioFiador = $datosFiador["labelMunicipio"];
                            $departamentoFiador = $datosFiador["labelDepartamento"];
                            $documentoFiadorLetras = mb_strtolower(documentoALetras(str_split($datosFiador["numeroDocumento"])), "UTF-8");
                            $nitFiadorLetras = !empty($datosFiador["nit"]) ? mb_strtolower(documentoALetras(str_split($datosFiador["nit"])), "UTF-8") : $documentoFiadorLetras;
                            $tmpDocumentoFiador = !empty($datosFiador['nit']) ? 'número ' . $documentoFiadorLetras . ' y  Número de Identificación Tributaria ' . $nitFiadorLetras : 'y Número de Identificación Tributaria ' . $nitFiadorLetras;

                            $nomenclaturasFiadores[$datosFiador["numeroDocumento"]] = new tmpNomenclatura($datosFiador["genero"], $posicionFiador);
                            $posicionFiador++;

                            $tmpDatosFiador = "<b>" . $nombreCompletoFiador . "</b>, de " . $edadFiadorLetras . " años de edad, " . $profesionFiador . ", del domicilio de " . $municipioFiador . ", departamento de " . $departamentoFiador . ", con Documento Único de Identidad número " . $documentoFiadorLetras . " y con Número de Identificación Tributaria " . $nitFiadorLetras;

                            array_push($datosFiadores, $tmpDatosFiador);
                            $nomenclaturaFiador = $nomenclaturasFiadores[$datosFiador["numeroDocumento"]]->pronombre . " " . $nomenclaturasFiadores[$datosFiador["numeroDocumento"]]->nomenclatura;
                            $tmpNomenclaturasFiadores[$datosFiador["numeroDocumento"]] = $nomenclaturaFiador;

                            if ($datosFiador["firma"] == "N") {
                                array_push($documentosNoFirmantes, $datosFiador["numeroDocumento"]);
                                $datosNoFirmantes[$datosFiador["numeroDocumento"]] = new tmpNoFirmante($datosFiador["numeroDocumento"], $nombreCompletoFiador, $nomenclaturaFiador, $datosFiador["genero"]);
                                array_push($arregloNomenclaturasNoFirmantes, $nomenclaturaFiador);
                            } else {
                                array_push($arregloNomenclaturasFirmantes, $nomenclaturaFiador);
                            }
                        }

                        $nomenclaturasFiadoresFirmas = $tmpNomenclaturasFiadores;

                        if (count($datosFiadores) > 1) {
                            $ultimoFiador = $datosFiadores[count($datosFiadores) - 1];
                            // $ultimaNomenclatura = end($tmpNomenclaturasFiadores);
                            $ultimaNomenclatura = $tmpNomenclaturasFiadores[array_key_last($tmpNomenclaturasFiadores)];
                            array_pop($datosFiadores);
                            array_pop($tmpNomenclaturasFiadores);

                            $presentacionFiadoresLegal = ", " . implode(", ", $datosFiadores) . " y " . $ultimoFiador;
                            $presentacionFiadoresGarantia = implode(", ", $datosFiadores) . " y " . $ultimoFiador;
                            $nomenclaturasFiadoresLegal = implode(", ", $tmpNomenclaturasFiadores) . " y " . $ultimaNomenclatura;
                        } else {
                            $presentacionFiadoresLegal = " y " . implode(", ", $datosFiadores);
                            $presentacionFiadoresGarantia = implode(", ", $datosFiadores);
                            $nomenclaturasFiadoresLegal = implode(", ", $tmpNomenclaturasFiadores);
                        }

                        $presentacionFiadoresLegal .= " en adelante " . $llamadoFiadores->denominado . " " . $llamadoFiadores->el_fiador;

                        //Variables abierta a cambios si hay mas participantes
                        $tituloParticipantes = "{titulo_deudor} Y " . $llamadoFiadores->el_fiador;

                        $obligacionParticipantesPrivada = "nos obligamos";
                        $obligacionParticipantesLegal = "se obligan";

                        $aceptacionParticipantesPrivada = "aceptamos";
                        $aceptacionParticipantesLegal = "aceptan";

                        $senialaParticipantesPrivada = "señalamos";
                        $senialaParticipantesLegal = "señalan";

                        $someteParticipantesPrivada = "nos sometemos";
                        $someteParticipantesLegal = "someten";

                        $expresionParticipantesPrivada = "nos expresamos";

                        $firmaParticipantesPrivada = "firmamos el presente documento de mutuo";
                        $firmaParticipantesLegal = "firmamos";

                        $complementoPresentacionLegal = "<b>COMPARECEN: </b>" . $seniores . " <b>" . $nombreDeudor . "</b>, de " . $edadDeudorLetras . " años de edad, " . $profesionDeudor . ", del domicilio de " . $municipioDeudor . ", departamento de " . $departamentoDeudor . ", a quien no conozco, pero identifico por su Documento Único de Identidad " . $documentoDeudor . "; en adelante denominado <b>\"{titulo_deudor}\"</b>" . $presentacionFiadoresLegal . " y <b>ME DICEN: </b>";

                        $reconocimientoFirmaLegal = "Que reconocen como suyas las firmas estampadas en el anterior contrato, por lo cual " . $generoDeudor->nomenclatura . " de " . $comparecientes . ", " . $generoDeudor->denominado . " <b>{titulo_deudor}</b> declara";

                        $reconocimientoFirmaParticipantesLegal = "Que las firmas reconocidas son AUTÉNTICAS, por haber sido puestas a mi presencia de su puño y letra por " . $otorgantes . ", a quienes no conozco, pero identifiqué por medio de sus Documentos Únicos de Identidad ya relacionados";

                        $expresaParticipantesLegal = "expresaron " . $comparecientes . " a quienes";

                        $leLegal = "les hube";

                        $manifiestaParticipantesLegal = "manifiestan";

                        $ratificaParticipantesLegal = "ratifican";

                        $parrafoPrivada = "<b>" . $llamadoFiadores->yo_nosotros . " </b>: " . $presentacionFiadoresGarantia . "; <b>" . $llamadoFiadores->manifiesto . ": </b>que para garantizar las obligaciones contraídas en este documento por <b>{titulo_deudor}</b>, " . $generoDeudor->senior . " <b>" . $nombreDeudor . "</b> a favor de La Cooperativa, de la cual " . $llamadoFiadores->enterado . " y con el fin de responder por el pago y exacto cumplimiento de la misma, " . $llamadoFiadores->constituir . " en " . $llamadoFiadores->fiador . " y " . $llamadoFiadores->codeudor . " y " . $llamadoFiadores->pagador . " de la deuda contraida por <b>{titulo_deudor}</b> antes " . $generoDeudor->expresado . ", y en tal carácter " . $llamadoFiadores->acepta . " todas y cada una de las condiciones pactadas y " . $llamadoFiadores->somete . " a las renuncias de <b>{titulo_deudor}</b> principal. " . $losParticipantes . " contratantes aceptamos que La Cooperativa pueda dar por terminado el plazo de la presente obligación por incumplimiento de cualquiera de las estipulaciones de este contrato. <b>{titulo_deudor} Y " . $llamadoFiadores->el_fiador . "</b> aceptamos que de tener constituidas garantías hipotecarias o prendarias a favor de La Cooperativa, no se nos liberen mientras esté vigente esta obligación";

                        $manifiestan_constitucion_legal = $posicionFiador > 3 ? "manifiestan que se han constituido en" : "manifiesta que se ha constituido en";

                        $parrafoLegal = "Por su parte, " . $nomenclaturasFiadoresLegal . " de " . strtolower($losParticipantes) . " comparecientes " . $manifiestan_constitucion_legal . " <b>" . $llamadoFiadores->fiador . " Y " . $llamadoFiadores->codeudor . "</b> y " . $llamadoFiadores->pagador . " de la obligación contraída en el presente documento por " . $generoDeudor->senior . " <b>" . $nombreDeudor . "</b>, a favor de La Cooperativa, aceptanto todas y cada una de las condiciones y renuncias a que se somete <b>{titulo_deudor}</b> principal";

                        array_push($definicionGarantiasPrivada, $parrafoPrivada);
                        array_push($definicionGarantiasLegal, $parrafoLegal);

                        if ($incluyeFirmante) {
                            if (($cantidadFiadores + 1) == count($documentosNoFirmantes)) {
                                $reconocimientoFirmaParticipantesLegal = "Que la firma reconocida es AUTÉNTICA, por haber sido puesta a mi presencia de su puño y letra y reconocida como suya por " . $nombreCompletoFirmante . ", quien firma a ruego de " . $otorgantes . ", a quien conozco e identifiqué por medio de su Documento Único de Identidad ya relacionado";

                                $firmaParticipantesPrivada = "por no saber firmar, dejamos impresas las huellas dactilares de nuestros dedos pulgares de nuestra mano derecha y a nuestro ruego lo hace en una sola firma " . $datosFirmanteRuego;

                                $reconocimientoFirmaLegal = "Que reconocen como suyas las huellas estampadas en el anterior contrato, por lo cual " . $generoDeudor->nomenclatura . " de " . $comparecientes . ", " . $generoDeudor->denominado . " <b>{titulo_deudor}</b> declara";

                                $firmaParticipantesLegal = "firmamos; no así {titulo_deudor} Y " . $llamadoFiadores->el_fiador . ", por no saber hacerlo pero dejan impresas las huellas dactilares del dedo pulgar de su mano derecha y a su ruego lo hace en una sola firma ";
                            } else {
                                $titulosNoFirmantes = isset($datosNoFirmantes[$datosDeudor["numeroDocumento"]]) ? (count($datosNoFirmantes) > 1 ? $generoDeudor->titulo . " y " : $generoDeudor->titulo) : "";
                                $cantidadFiadoresNoFirmantes = 0;
                                $cantidadNoFirmantesMujeres = 0;
                                $nombresFiadoresNoFirmantes = "";
                                $nomenclaturasNoFirmantes = "";
                                $nomenclaturasFirmantes = "";

                                $arregloNombresFiadoresNoFirmantes = [];

                                foreach ($datosNoFirmantes as $noFirmante) {
                                    if ($noFirmante->documentoPrimario != $datosDeudor["numeroDocumento"]) {
                                        $cantidadFiadoresNoFirmantes++;
                                        if ($noFirmante->genero == "F") {
                                            $cantidadNoFirmantesMujeres++;
                                        }

                                        array_push($arregloNombresFiadoresNoFirmantes, $noFirmante->nombreCompleto);
                                    }
                                }

                                if (count($arregloNombresFiadoresNoFirmantes) > 0) {
                                    if (count($arregloNombresFiadoresNoFirmantes) > 1) {
                                        $nombreUltimoFiadorNoFirmante = $arregloNombresFiadoresNoFirmantes[count($arregloNombresFiadoresNoFirmantes) - 1];
                                        array_pop($arregloNombresFiadoresNoFirmantes);
                                        $nombresFiadoresNoFirmantes = " " . implode("", $arregloNombresFiadoresNoFirmantes) . " y " . $nombreUltimoFiadorNoFirmante;
                                    } else {
                                        $nombresFiadoresNoFirmantes = " " . implode("", $arregloNombresFiadoresNoFirmantes);
                                    }
                                }

                                if (count($arregloNomenclaturasFirmantes) > 1) {
                                    $ultimaNomenclaturaFirmante = $arregloNomenclaturasFirmantes[count($arregloNomenclaturasFirmantes) - 1];
                                    array_pop($arregloNomenclaturasFirmantes);
                                    $nomenclaturasFirmantes = implode(", ", $arregloNomenclaturasFirmantes) . " y " . $ultimaNomenclaturaFirmante . " de " . $comparecientes . " reconocen como suyas las firmas estampadas en el anterior contrato";
                                } else {
                                    $nomenclaturasFirmantes = implode("", $arregloNomenclaturasFirmantes) . " de " . $comparecientes . " reconoce como suya la firma estampada en el anterior contrato";
                                }

                                $reconocimientoFirmaParticipantesLegal = "Que las firmas reconocidas son AUTÉNTICAS, por haber sido puestas a mi presencia de su puño y letra por " . $otorgantes . ", a quienes no conozco, pero identifiqué por medio de sus Documentos Únicos de Identidad ya relacionados, a excepción de ";

                                if (count($arregloNomenclaturasNoFirmantes) > 1) {
                                    $ultimaNomenclaturaNoFirmante = $arregloNomenclaturasNoFirmantes[count($arregloNomenclaturasNoFirmantes) - 1];
                                    array_pop($arregloNomenclaturasNoFirmantes);
                                    $nomenclaturasNoFirmantes = implode(", ", $arregloNomenclaturasNoFirmantes) . " y " . $ultimaNomenclaturaNoFirmante . " de " . $comparecientes . " reconocen como suyas las huellas estampadas en el anterior contrato";
                                    $firmaParticipantesPrivada = "firmamos el presente documento de mutuo, no así " . $titulosNoFirmantes . "" . $nombresFiadoresNoFirmantes . ", por no saber hacerlo, pero dejamos impresas las huellas dactilares de nuestros dedos pulgares de nuestra mano derecha y a nuestro ruego lo hace en una sola firma " . $datosFirmanteRuego;

                                    $reconocimientoFirmaParticipantesLegal .= implode(", ", $arregloNomenclaturasNoFirmantes) . " y " . $ultimaNomenclaturaNoFirmante . " quienes no firman, por no saber hacerlo, asegurando que está firmando a su ruego " . $nombreCompletoFirmante;
                                    $firmaParticipantesLegal = "firmamos; no así " . $titulosNoFirmantes . "" . $nombresFiadoresNoFirmantes . ", por no saber hacerlo, pero dejan impresas las huellas dactilares de sus dedos pulgares de su mano derecha y a su ruego lo hace en una sola firma " . $datosFirmanteRuego;
                                } else {
                                    $nomenclaturasNoFirmantes = implode("", $arregloNomenclaturasNoFirmantes) . " de " . $comparecientes . " reconoce como suya la huella estampada en el anterior contrato";

                                    $reconocimientoFirmaParticipantesLegal .= implode("", $arregloNomenclaturasNoFirmantes) . " quien no firma, por no saber hacerlo, asegurando que está firmando a su ruego " . $nombreCompletoFirmante;
                                    $firmaParticipantesPrivada = "firmamos el presente documento de mutuo, no así " . $titulosNoFirmantes . "" . $nombresFiadoresNoFirmantes . ", por no saber hacerlo, pero deja impresa la huella dactilar de su dedo pulgar de su mano derecha y a su ruego lo hace en una sola firma " . $datosFirmanteRuego;
                                    $firmaParticipantesLegal = "firmamos; no así " . $titulosNoFirmantes . "" . $nombresFiadoresNoFirmantes . ", por no saber hacerlo, pero deja impresa la huella dactilar de su dedo pulgar de su mano derecha y a su ruego lo hace en una sola firma " . $datosFirmanteRuego;
                                }

                                $titulosNoFirmantes .= $cantidadFiadoresNoFirmantes > 0 ? ($cantidadFiadoresNoFirmantes > 1 ? (($cantidadNoFirmantesMujeres == $cantidadFiadores ? "LAS FIADORAS " : "LOS FIADORES ")) : (($cantidadNoFirmantesMujeres == $cantidadFiadores ? "LA FIADORA " : "EL FIADOR "))) : "";

                                $reconocimientoFirmaLegal = "Que " . $nomenclaturasFirmantes . " y " . $nomenclaturasNoFirmantes . ", por lo cual " . $generoDeudor->nomenclatura . " de " . $comparecientes . ", " . $generoDeudor->denominado . " <b>{titulo_deudor}</b> declara";
                            }
                        }

                        break;
                    case 2:
                        $tituloGarantia = "MUTUO GARANTIZADO CON APORTACIONES";
                        $cuentaAportacionesLetra = mb_strtolower(documentoALetras(str_split($garantia["configuracion"]["cuenta"])), "UTF-8");
                        $porcentajeCobertura = mb_strtolower(conversionNumeroLetras($garantia["configuracion"]["porcentaje"]), 'UTF-8');
                        $parrafoPrivada = "El presente crédito queda garantizado con el " . $porcentajeCobertura . " por ciento del saldo de las aportaciones de la cuenta número <b> " . $cuentaAportacionesLetra . "</b>, a favor de <b>{titulo_deudor}</b>. <b>{titulo_deudor}</b> acepto que La Cooperativa pueda dar por terminado el plazo de la presente obligación por incumplimiento de cualquiera de las estipulaciones de este contrato";
                        $parrafoLegal = str_replace("acepto", "aceptó", $parrafoPrivada);
                        array_push($definicionGarantiasPrivada, $parrafoPrivada);
                        array_push($definicionGarantiasLegal, $parrafoLegal);
                        break;
                    case 3:
                        $tituloGarantia = "MUTUO GARANTIZADO CON CDP";
                        $cdpLetras = trim(mb_strtolower(documentoALetras(str_split($garantia["configuracion"]["cuenta"])), "UTF-8"));
                        $fechaCreacion = explode("-", $garantia["configuracion"]["fechaCreacion"]);
                        $fechaVencimiento = explode("-", $garantia["configuracion"]["fechaVencimiento"]);
                        $fechaAperturaLetras = generalFechaLetras($fechaCreacion[0], $fechaCreacion[1], $fechaCreacion[2]);
                        $fechaVencimientoLetras = generalFechaLetras($fechaVencimiento[0], $fechaVencimiento[1], $fechaVencimiento[2]);
                        $certificadoLetras = trim(mb_strtolower(documentoALetras(str_split($garantia["configuracion"]["certificado"])), "UTF-8"));
                        $montoCDPLetras = conversionDineroLetras($garantia["configuracion"]["monto"]);
                        $porcentajeCobertura = mb_strtolower(conversionNumeroLetras($garantia["configuracion"]["porcentaje"]), 'UTF-8');
                        $parrafoPrivada = "El presente crédito queda garantizado con el " . $porcentajeCobertura . " por ciento del Monto del Certificado de Depósito a Plazo, cuenta número <b> " . $cdpLetras . "</b>, de fecha " . $fechaAperturaLetras . ", con fecha de vencimiento " . $fechaVencimientoLetras . " y  número de certificado <b>" . $certificadoLetras . "</b>, el cual es por un monto de " . $montoCDPLetras . ", a favor de <b>{titulo_deudor}</b>. <b>{titulo_deudor}</b> acepto que La Cooperativa pueda dar por terminado el plazo de la presente obligación por incumplimiento de cualquiera de las estipulaciones de este contrato";
                        $parrafoLegal = str_replace("acepto", "aceptó", $parrafoPrivada);
                        array_push($definicionGarantiasPrivada, $parrafoPrivada);
                        array_push($definicionGarantiasLegal, $parrafoLegal);
                        break;
                    case 7:
                        $tituloGarantia .= " Y CON FSG";
                        $parrafoPrivada = "Además el presente crédito queda garantizado con el Fondo Salvadoreño de Garantías (Contrato de Garantías), de acuerdo a lo establecido en el Contrato de Garantías, suscrito entre la ASOCIACIÓN COOPERATIVA DE AHORRO, CRÉDITO Y PRODUCCIÓN AGROPECUARIA COMUNAL DE NUEVA CONCEPCIÓN, DE RESPONSABILIDAD LIMITADA, que se abrevia ACACYPAC N.C. de R.L., y el BANCO DE DESARROLLO DE EL SALVADOR, que puede abreviarse BDES, el día veintiocho de septiembre del año dos mil quince, en la ciudad de San Salvador, departamento de San Salvador, en donde ha quedado plenamente establecido que ambas partes con base a su mutuo interés de facilitar a los Asociados de ACACYPAC N.C. de R.L. que apliquen y califiquen como beneficiarios, particularmente las micros, pequeñas y medianas empresas, y que no posean garantías suficientes, puedan acceder a operaciones de créditos a su cargo utilizando las garantías que puede otorgar el Fondo Salvadoreño de Garantías, a ACACYPAC N.C. de R.L.";
                        $parrafoLegal = $parrafoPrivada;

                        array_push($definicionGarantiasPrivada, $parrafoPrivada);
                        array_push($definicionGarantiasLegal, $parrafoLegal);
                        break;
                    case 8:
                        $tituloGarantia .= " Y  CON PROGARA";
                        $parrafoPrivada = "Además el presente crédito queda garantizado con el PROGRAMA DE GARANTIA AGROPECUARIA (Contrato de Garantías), de acuerdo a lo establecido en el Contrato de Garantías, suscrito entre la ASOCIACIÓN COOPERATIVA DE AHORRO, CRÉDITO Y PRODUCCIÓN AGROPECUARIA COMUNAL DE NUEVA CONCEPCIÓN, DE RESPONSABILIDAD LIMITADA, que se abrevia ACACYPAC N. C. de R.L., y el BANCO DE DESARROLLO DE EL SALVADOR, que puede abreviarse BDES, el día veintidós de noviembre del año dos mil diecisiete, en la ciudad de San Salvador, departamento de San Salvador, en donde ha quedado plenamente establecido que ambas partes con base a su mutuo interés de facilitar a los Asociados de ACACYPAC,  N.C. de R.L. que apliquen y califiquen como beneficiarios, particularmente a pequeños empresarios, productores del sector agropecuario, cooperativas y asociaciones cooperativas, y que no posean garantías suficientes, puedan acceder a operaciones de créditos a su cargo utilizando las garantías que puede otorgar el PROGRAMA DE GARANTIA AGROPECUARIA, a ACACYPAC, N.C. de R.L.";
                        $parrafoLegal = $parrafoPrivada;

                        array_push($definicionGarantiasPrivada, $parrafoPrivada);
                        array_push($definicionGarantiasLegal, $parrafoLegal);
                        break;
                    case 4:
                        $tituloGarantia = $incluyeFiadores ? "MUTUO CON GARANTIA FIDUCIARIA Y ORDEN DE DESCUENTO" : "MUTUO GARANTIZADO CON ORDEN DE DESCUENTO";
                        $parrafoPrivada = "ORDEN IRREVOCABLE DE DESCUENTO: En garantía del presente crédito el {titulo_deudor} ha emitido Orden Irrevocable de Descuento y aceptada por el pagador a favor de la cooperativa acreedora, para que de su sueldo se le descuente el pago de la cuota mensual correspondiente";
                        $parrafoLegal = $parrafoPrivada;

                        array_push($definicionGarantiasPrivada, $parrafoPrivada);
                        array_push($definicionGarantiasLegal, $parrafoLegal);
                        break;
                }
            }
        }

        if ($datosCredito["lineaRotativa"] == "S") {
            $tituloGarantia .= " / LINEA ROTATIVA";
        }


        $pdf->AddPage();

        $pdf->writeHTML('<style>table{font-size:20px; text-align:center;}</style>
    <table border="0" style="font-size:8px; text-align:left;">
    <tr>
    <td style="width:160px;">
    Código de cliente: ' . $datosDeudor["codigoCliente"] . '
    </td>
    <td style="width:180px;">
    Número de crédito: ' . $datosCredito["numeroCredito"] . '
    </td>
    <td style="width:230px;">
    Elaborado por: ' . $nombreUsuario . '
    </td>
    </tr>
    </table>
    <table border="0">
    <tr>
    <td border="1" style="height:80px; line-height:100px; font-weight:bold;">DOCUMENTO AUTENTICADO
    </td>
    </tr>
    <tr>
    <td border="0" style="height:35px;"></td>
    </tr>
    <tr>
    <td border="1" style="height:620px; line-height:40px; font-size:15px; overflow-y:hidden;">
    <br />
    DE<br />
    <b>' . $tituloGarantia . '</b>
    <br />
    <br />
    OTORGADO POR:
    <br />
    <b>' . $nombreDeudor . '</b>
    <br />
    A FAVOR DE:
    <br />
    <b>ASOCIACIÓN COOPERATIVA DE AHORRO CREDITO Y PRODUCCIÓN AGROPECUARIA COMUNAL DE NUEVA CONCEPCIÓN DE RESPONSABILIDAD LIMITADA ACACYPAC, N.C. DE  R.L.</b>
    <br />
    ANTE LOS OFICIOS DEL LICENCIADO
    <br />
    <b>JOSE LUIS REYES AMAYA</b>
    <br />
    ABOGADO Y NOTARIO
    <br />
    <b>' . $municipioAgencia . ', ' . $fechaGeneracion[0] . ' DE ' . mb_strtoupper($mesGeneracionLetras, "UTF-8") . ' DE ' . $fechaGeneracion[2] . '</b>
    </td>
    </tr>
    </table>', true, false, true, false, '');

        $pdf->AddPage();

        $pdf->AddPage();

        $partePrivada = [];
        $parteLegal = [];
        $cntPrivada = 0;
        $cntLegal = 0;

        array_push($partePrivada, $plantilla[0]->presentacion_inicial->privada);
        array_push($parteLegal, $plantilla[0]->presentacion_inicial->legal);

        $cntPrivada++;
        $cntLegal++;

        array_push($partePrivada, '<table>' . aRomano($cntPrivada) . ')</table> ' . $plantilla[0]->descripcion_coope_monto->privada);
        array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_coope_monto->legal);

        $cntPrivada++;
        $cntLegal++;

        $definicionInteresesPrivada = $datosCredito["empleadoACACYPAC"] == "N" ? $plantilla[0]->descripcion_intereses->privada : str_replace(". ", "", $plantilla[0]->descripcion_intereses->privada) . " ó por modificación por acuerdo del Consejo de Administración. ";
        $definicionInteresesLegal = $datosCredito["empleadoACACYPAC"] == "N" ? $plantilla[0]->descripcion_intereses->legal : str_replace(". ", "", $plantilla[0]->descripcion_intereses->legal) . " ó por modificación por acuerdo del Consejo de Administración. ";

        array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $definicionInteresesPrivada);
        array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $definicionInteresesLegal);

        $cntPrivada++;
        $cntLegal++;

        if ($datosCredito["lineaRotativa"] == "N") {
            array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_plazo->privada);
            array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_plazo->legal);
        } else {
            $fechaVencimiento = explode("-", $datosCredito["fechaVencimiento"]);
            $fechaVencimientoLetras = generalFechaLetras($fechaVencimiento[0], $fechaVencimiento[1], $fechaVencimiento[2]);
            array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_plazo_rotativa->privada);
            array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_plazo_rotativa->legal);

            $cntPrivada++;
            $cntLegal++;

            array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_plazo_giro->privada);
            array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_plazo_giro->legal);

            $cntPrivada++;
            $cntLegal++;

            array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_limites_rotativo->privada);
            array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_limites_rotativo->legal);

            $cntPrivada++;
            $cntLegal++;

            array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_amortizacion->privada);
            array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_amortizacion->legal);
        }

        $cntPrivada++;
        $cntLegal++;

        array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_lugar_pagos->privada);
        array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_lugar_pagos->legal);

        $cntPrivada++;
        $cntLegal++;

        array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_fuente_fondos->privada);
        array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_fuente_fondos->legal);

        $cntPrivada++;
        $cntLegal++;

        array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_destino->privada);
        array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_destino->legal);

        if ($incluyeGarantia) {
            $cntPrivada++;
            $cntLegal++;

            $tmpGarantiasPrivada = [];
            $tmpGarantiasLegal = [];

            if ($incluyeHipotecas && $incluyeFiadores) {
                $tmpPrivada = [];
                $tmpLegal = [];

                $garantiaHipotecariaPrivada = $definicionGarantiasPrivada[0];
                $garantiaHipotecariaLegal = $definicionGarantiasLegal[0];

                array_shift($definicionGarantiasPrivada);
                array_shift($definicionGarantiasLegal);

                $garantiaFiduciariaPrivada = $definicionGarantiasPrivada[0];
                $garantiaFiduciariaLegal = $definicionGarantiasLegal[0];

                array_shift($definicionGarantiasPrivada);
                array_shift($definicionGarantiasLegal);

                array_push($tmpPrivada, $garantiaFiduciariaPrivada, $garantiaHipotecariaPrivada);
                array_push($tmpLegal, $garantiaFiduciariaLegal, $garantiaHipotecariaLegal);

                $tmpGarantiasPrivada = array_merge($tmpPrivada, $definicionGarantiasPrivada);
                $tmpGarantiasLegal = array_merge($tmpLegal, $definicionGarantiasLegal);
            } else {
                $tmpGarantiasPrivada = $definicionGarantiasPrivada;
                $tmpGarantiasLegal = $definicionGarantiasLegal;
            }

            $definicionGarantiasPrivadaCompleta = implode(". ", $tmpGarantiasPrivada);
            $definicionGarantiasLegalCompleta = implode(". ", $tmpGarantiasLegal);

            array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ') GARANTÍA:</b> ' . $definicionGarantiasPrivadaCompleta . ". ");
            array_push($parteLegal, '<b>' . aRomano($cntLegal) . ') GARANTÍA:</b> ' .  $definicionGarantiasLegalCompleta . ". ");
        }

        $cntPrivada++;
        $cntLegal++;

        array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_autorizacion_especial->privada);
        array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_autorizacion_especial->legal);

        $cntPrivada++;
        $cntLegal++;

        array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_caducidad_plazo->privada);
        array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_caducidad_plazo->legal);

        $cntPrivada++;
        $cntLegal++;

        array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_interes_mora->privada);
        array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_interes_mora->legal);

        $cntPrivada++;
        $cntLegal++;

        array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_obligacion_pago->privada);
        array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_obligacion_pago->legal);

        $cntPrivada++;
        $cntLegal++;

        array_push($partePrivada, '<b>' . aRomano($cntPrivada) . ')</b> ' . $plantilla[0]->descripcion_domicilio_firma->privada);
        array_push($parteLegal, '<b>' . aRomano($cntLegal) . ')</b> ' . $plantilla[0]->descripcion_domicilio_firma->legal);

        $partePrivadaCompleta = implode("", $partePrivada);
        $parteLegalCompleta = implode("", $parteLegal);

        //Susticiones por partes
        $partePrivadaCompleta  = str_replace("{clausula_seguro_privada}", $clausulaSeguroPrivada, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{clausula_seguro_legal}", $clausulaSeguroLegal, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{fecha_generacion}", $fechaGeneracionLetra, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{fecha_hora_generacion}", $fechaHoraGeneracionLetra, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{obligacion_participantes_privada}", $obligacionParticipantesPrivada, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{obligacion_participantes_legal}", $obligacionParticipantesLegal, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{aceptacion_participantes_privada}", $aceptacionParticipantesPrivada, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{aceptacion_participantes_legal}", $aceptacionParticipantesLegal, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{seniala_participantes_privada}", $senialaParticipantesPrivada, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{seniala_participantes_legal}", $senialaParticipantesLegal, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{somete_participantes_privada}", $someteParticipantesPrivada, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{somete_participantes_legal}", $someteParticipantesLegal, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{expresion_participantes_privada}", $expresionParticipantesPrivada, $partePrivadaCompleta);

        $partePrivadaCompleta  = str_replace("{firma_participantes_privada}", $firmaParticipantesPrivada, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{firma_participantes_legal}", $firmaParticipantesLegal, $parteLegalCompleta);

        $parteLegalCompleta = str_replace("{complemento_presentacion_inicial_legal}", $complementoPresentacionLegal, $parteLegalCompleta);

        $parteLegalCompleta = str_replace("{reconocimiento_firma_legal}", $reconocimientoFirmaLegal, $parteLegalCompleta);

        $parteLegalCompleta = str_replace("{reconocimiento_firma_participantes}", $reconocimientoFirmaParticipantesLegal, $parteLegalCompleta);

        $parteLegalCompleta = str_replace("{expresa_participantes}", $expresaParticipantesLegal, $parteLegalCompleta);

        $parteLegalCompleta = str_replace("{le_legal}", $leLegal, $parteLegalCompleta);

        $parteLegalCompleta = str_replace("{manifiesta_participantes}", $manifiestaParticipantesLegal, $parteLegalCompleta);

        $parteLegalCompleta = str_replace("{ratifica_participantes}", $ratificaParticipantesLegal, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{desembolsos_parciales_privada}", $desembolsosParcialesPrivada, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{desembolsos_parciales_legal}", $desembolsosParcialesLegal, $parteLegalCompleta);

        //Sustituciones generales
        $partePrivadaCompleta  = str_replace("{definicion_plazo}", $definicionPlazo, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{definicion_plazo}", $definicionPlazo, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{fecha_vencimiento}", $fechaVencimientoLetras, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{fecha_vencimiento}", $fechaVencimientoLetras, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{departamento_agencia}", $departamentoAgencia, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{departamento_agencia}", $departamentoAgencia, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{municipio_agencia}", $municipioAgencia, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{municipio_agencia}", $municipioAgencia, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{enterado}", $enteradoDeudor, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{enterado}", $enteradoDeudor, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{titulo_participantes}", $tituloParticipantes, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{titulo_participantes}", $tituloParticipantes, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{nombre_deudor}", $nombreDeudor, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{nombre_deudor}", $nombreDeudor, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{edad_deudor}", $edadDeudorLetras, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{edad_deudor}", $edadDeudorLetras, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{profesion_deudor}", $profesionDeudor, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{profesion_deudor}", $profesionDeudor, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{municipio_deudor}", $municipioDeudor, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{municipio_deudor}", $municipioDeudor, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{departamento_deudor}", $departamentoDeudor, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{departamento_deudor}", $departamentoDeudor, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{documentoIdentificacionDeudor}", $documentoDeudor, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{documentoIdentificacionDeudor}", $documentoDeudor, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{nit_deudor}", $nitDeudor, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{nit_deudor}", $nitDeudor, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{titulo_deudor}", $generoDeudor->titulo, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{titulo_deudor}", $generoDeudor->titulo, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{monto_credito}", $montoCreditoLetras, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{monto_credito}", $montoCreditoLetras, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{tasa_interes}", $tasaInteresLetras, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{tasa_interes}", $tasaInteresLetras, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{plazo}", $plazoLetras, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{plazo}", $plazoLetras, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{fuente_fondos}", $fuenteFondos, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{fuente_fondos}", $fuenteFondos, $parteLegalCompleta);

        $partePrivadaCompleta  = str_replace("{destino}", $destinoCredito, $partePrivadaCompleta);
        $parteLegalCompleta = str_replace("{destino}", $destinoCredito, $parteLegalCompleta);

        $espacio_entre_partes = "<br /><br /><br /><br /><br/><br/><br />";

        $pdf->writeHTML('<style>p{line-height:20px; font-size: 10.8px; text-align:justify;}</style><p>' . $partePrivadaCompleta . '</p>', true, false, true, false, '');
        $pdf->writeHTML('<style>p{line-height:20px; font-size: 10.8px; text-align:justify;}</style><p>' . $espacio_entre_partes . '</p>', true, false, true, false, '');

        $cantidadPaginasActual = (intval($pdf->getNumPages()) - 2) * 2;

        $cantidadFolios = mb_strtolower(NumeroALetras::convertir(intval($cantidadPaginasActual / 2) + ($cantidadPaginasActual % 2)), "UTF-8") . " folios";

        $parteLegalCompleta = str_replace("{cantidad_folios}", $cantidadFolios, $parteLegalCompleta);

        $pdf->writeHTML('<style>p{line-height:20px; font-size: 10.8px; text-align:justify;}</style><p>' . $parteLegalCompleta . '</p>', true, false, true, false, '');

        if (!isset($nombreArchivo)) {
            $nombreArchivo = "prueba.pdf";
        }

        $mypdf = __DIR__ . '\..\docs\Mutuo\/' . $nombreArchivo . '';
        $pdf->Output($mypdf, 'F');

        // header('Content-type: application/pdf');
        // header('Content-Disposition: inline;filename=' . $nombreArchivo);
        // readfile($mypdf);
    }
}

function definicionGeneroSingular($participante, $genero)
{
    $tmpParticipante = str_split($participante);
    array_pop($tmpParticipante);

    if ($genero == "M") {
        $definicion = (object) array(
            "titulo" => "EL " . implode($tmpParticipante) . "R",
            "enterado" => "enterado",
            "senior" => "el señor",
            "expresado" => "expresado",
            "compareciente" => "el compareciente",
            "nomenclatura" => "el primero",
            "denominado" => "denominado",
            "licenciado" => "el Licenciado"
        );
    } else {
        $definicion = (object) array(
            "titulo" => "LA " . implode($tmpParticipante) . "RA",
            "enterado" => "enterada",
            "senior" => "la señora",
            "expresado" => "expresada",
            "compareciente" => "la compareciente",
            "nomenclatura" => "la primera",
            "denominado" => "denominada",
            "licenciado" => "la Licenciada"
        );
    }

    return $definicion;
}

function construccionDefinicionPlazo($plazoNumero, $plazoUnidad, $periodicidad, $creditoVencimiento, $aditivos)
{
    global $periodicidadesPago;
    $definicionPlazo = '';
    $plazoNumero = intval($plazoNumero);
    $tmpCuota = 0;

    $tmpDivisor = $plazoUnidad != "MENSUAL" ? (isset($periodicidadesPago->$periodicidad) ? $periodicidadesPago->$periodicidad : 1) : 1;

    $labelsAditivos = [];
    $definicionAditivos = [];

    foreach ($aditivos as $aditivo) {
        if (floatval($aditivo["monto"]) > 0) {
            $montoAditivo = floatval($aditivo["monto"]);
            $tmpCuota += $montoAditivo;
            array_push($labelsAditivos, $aditivo["aditivo"]);
            $aditivoALetras = mb_strtolower(conversionDineroLetras($montoAditivo), "UTF-8");
            $aditivoALetras = str_replace("estados unidos de américa", "Estados Unidos de América", $aditivoALetras);
            array_push($definicionAditivos, "en concepto de " . $aditivo["aditivo"] . " " . $aditivoALetras);
        }
    }

    $aditivosIncluidos = "";
    $definicionAditivosIncluidos = "";

    if (count($labelsAditivos) > 1) {
        $ultimoAditivo = $labelsAditivos[(count($labelsAditivos) - 1)];
        $definicionUltimoAditivo = $definicionAditivos[(count($definicionAditivos) - 1)];
        array_pop($labelsAditivos);
        array_pop($definicionAditivos);

        $aditivosIncluidos = implode(", ", $labelsAditivos) . " y " . $ultimoAditivo;
        $definicionAditivosIncluidos = implode(", ", $definicionAditivos) . " y " . $definicionUltimoAditivo;
    } else {
        $aditivosIncluidos = implode("", $labelsAditivos);
        $definicionAditivosIncluidos = implode($definicionAditivos);
    }

    $cuotaLetras = conversionDineroLetras($tmpCuota);

    if ($creditoVencimiento == "S") {
        $definicionPlazo = 'UNA SOLA CUOTA al vencimiento del plazo, fija, vencida y sucesiva de ' . mb_strtoupper($cuotaLetras, 'UTF-8') . ', la cual incluye: ' . $definicionAditivosIncluidos;
    } else {
        $periodicidadPago = mb_strtolower($periodicidad . "ES", "UTF-8");
        $cantidadCuotas = $plazoNumero / $tmpDivisor;
        $cantidadCuotas--;
        $incluision = $cantidadCuotas > 1 ? "incluyen" : "incluye";
        $calculoCuotas = $cantidadCuotas > 1 ? mb_strtoupper(NumeroALetras::convertir($cantidadCuotas), "UTF-8") . " CUOTAS " . $periodicidadPago . " fijas, vencidas y sucesivas de " : "UNA CUOTA " . mb_strtolower($periodicidad, "UTF-8") . " fija, vencida y sucesiva de ";

        $definicionPlazo = $calculoCuotas . mb_strtoupper($cuotaLetras, 'UTF-8') . ', que ' . $incluision . ': ' . $definicionAditivosIncluidos . ' y una última cuota por el saldo de ' . $aditivosIncluidos;
    }

    return $definicionPlazo;
}

function definicionLlamadoParticipantes($participantes)
{
    $definiciones = (object)[];
    $cntParticipantes = 0;
    $participantesHombres = 0;
    $participantesMujeres = 0;

    foreach ($participantes as $participante) {
        $cntParticipantes++;
        if ($participante["genero"] == "M") {
            $participantesHombres++;
        } elseif ($participante["genero"] == "F") {
            $participantesMujeres++;
        }
    }

    if ($participantesMujeres == $cntParticipantes) {
        $definiciones = (object)array(
            "yo_nosotros" => $cntParticipantes > 1 ? "NOSOTRAS" : "YO",
            "manifiesto" => $cntParticipantes > 1 ? "MANIFESTAMOS" : "MANIFIESTO",
            "seniores" => $cntParticipantes > 1 ? "las señoras " : "la señora",
            "enterado" => $cntParticipantes > 1 ? "estamos enteradas" : "estoy enterada",
            "constituir" => $cntParticipantes > 1 ? "nos constituimos" : "me constituyo",
            "fiador" => $cntParticipantes > 1 ? "FIADORAS" : "FIADORA",
            "el_fiador" => $cntParticipantes > 1 ? "LAS FIADORAS" : "LA FIADORA",
            "codeudor" => $cntParticipantes > 1 ? "CODEUDORAS SOLIDARIAS" : "CODEUDORA SOLIDARIA",
            "pagador" => $cntParticipantes > 1 ? "principales pagadoras" : "principal pagadora",
            "acepta" => $cntParticipantes > 1 ? "aceptamos" : "acepto",
            "somete" => $cntParticipantes > 1 ? "nos sometemos" : "me someto",
            "denominado" => $cntParticipantes > 1 ? "denominadas" : "denominada"
        );
    } else {
        $definiciones = (object)array(
            "yo_nosotros" => $cntParticipantes > 1 ? "NOSOTROS" : "YO",
            "manifiesto" => $cntParticipantes > 1 ? "MANIFESTAMOS" : "MANIFIESTO",
            "seniores" => $cntParticipantes > 1 ? "los señores " : "el señor",
            "enterado" => $cntParticipantes > 1 ? "estamos enterados" : "estoy enterado",
            "constituir" => $cntParticipantes > 1 ? "nos constituimos" : "me constituyo",
            "fiador" => $cntParticipantes > 1 ? "FIADORES" : "FIADOR",
            "el_fiador" => $cntParticipantes > 1 ? "LOS FIADORES" : "EL FIADOR",
            "codeudor" => $cntParticipantes > 1 ? "CODEUDORES SOLIDARIOS" : "CODEUDOR SOLIDARIO",
            "pagador" => $cntParticipantes > 1 ? "principales pagadores" : "principal pagador",
            "acepta" => $cntParticipantes > 1 ? "aceptamos" : "acepto",
            "somete" => $cntParticipantes > 1 ? "nos sometemos" : "me someto",
            "denominado" => $cntParticipantes > 1 ? "denominados" : "denominado"
        );
    }

    return $definiciones;
}

function definicionLlamadoParticipantesLegal($participantes)
{
    $definiciones = (object)[];
    $cntParticipantes = 0;
    $participantesHombres = 0;
    $participantesMujeres = 0;

    foreach ($participantes as $participante) {
        $cntParticipantes++;
        if ($participante["genero"] == "M") {
            $participantesHombres++;
        } elseif ($participante["genero"] == "F") {
            $participantesMujeres++;
        }
    }

    if ($participantesMujeres == $cntParticipantes) {
        $definiciones = (object)[];
    } else {
        $definiciones = (object)[];
    }

    return $definiciones;
}

class tmpNomenclatura
{
    var $pronombre, $nomenclatura;

    function __construct($genero, $posicion)
    {
        $asignaciones = array("", "primer", "segund", "tercer", "cuart", "quint", "sext", "séptim", "octav", "noven", "decim");
        $pronombre = $genero == "M" ? "el" : "la";
        $nomenclatura = isset($asignaciones[$posicion]) ? ($genero == "M" ? $asignaciones[$posicion] . "o" : $asignaciones[$posicion] . "a") : "No definido";
        $this->pronombre = $pronombre;
        $this->nomenclatura = $nomenclatura;
    }
}

class tmpNoFirmante
{
    var $documentoPrimario, $nombreCompleto, $nomenclatura, $genero;

    function __construct($documentoPrimario, $nombreCompleto, $nomenclatura, $genero)
    {
        $this->documentoPrimario = $documentoPrimario;
        $this->nombreCompleto = $nombreCompleto;
        $this->nomenclatura = $nomenclatura;
        $this->genero = $genero;
    }
}
