<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET)) {
        include '../../code/connectionSqlServer.php';
        require_once './Models/mutuo.php';

        $mutuo = new mutuo();
        $mutuo->id = $_GET["id"];

        $mutuos = $mutuo->getMutuo();

        if (isset($mutuos["respuesta"])) {
            $respuesta = $mutuos;
        } else {
            $respuesta->{"respuesta"} = "Error al conectar con la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = 'SESION';
}

echo json_encode($respuesta);
