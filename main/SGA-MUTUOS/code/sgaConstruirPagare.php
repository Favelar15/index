<?php
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (isset($datosDeudor) && isset($datosCredito) && isset($datosFiadores)) {
        require_once 'C:\xampp\htdocs\vendor\autoload.php';
        global $arrMeses;

        class MYPDF extends TCPDF
        {

            //Page header
            public function Header()
            {
                $bMargin = $this->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $this->AutoPageBreak;
                // disable auto-page-break
                $this->SetAutoPageBreak(false, 0);
                // set bacground image
                $img_file = '../../img/membrete.png';
                $this->Image('@' . file_get_contents($img_file), 0, 0, 220, 275, '', '', '', false, 300);
                // restore auto-page-break status
                $this->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $this->setPageMark();
            }

            // Page footer
            public function Footer()
            {
                // Position at 15 mm from bottom
                $this->SetY(-20);
                // Set font
                $this->SetFont('helvetica', 'I', 8);
                // Page number
                $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            }
        }

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Departamento IT');
        $pdf->SetTitle('Pagaré');
        $pdf->SetSubject('ACACYPAC');
        $pdf->SetKeywords('TCPDF, PDF, Mutuos');

        // Fuente de cabecera y pie de página
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // Espacios por defecto
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //Margenes
        $pdf->SetMargins(18, 15, 15);
        $pdf->SetHeaderMargin(7);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // Configurar Auto salto de página
        $pdf->SetAutoPageBreak(TRUE, 40);

        // Factor de escalado de imagen
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // Configurando lenguaje
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // Establecer fuente
        $pdf->SetFont('dejavusans', '', 6);

        $pdf->AddPage();

        $nombreCompletoDeudor = $datosDeudor['nombres'] . " " . $datosDeudor['apellidos'];
        $nombreCompletoDeudor .= !empty($datosDeudor['conocido']) ? " CONOCIDO POR " . $datosDeudor['conocido'] : "";
        $tmpFechaVencimiento = explode("-", $datosCredito['fechaVencimiento']);
        $fechaVemcimiento = $tmpFechaVencimiento[0] . " de " . mb_strtoupper($arrMeses[intval($tmpFechaVencimiento[1])], "UTF-8") . " de " . $tmpFechaVencimiento[2];
        $montoCreditoLetras = conversionDineroLetras($datosCredito['monto']);
        $tasaInteresLetras = conversionNumeroLetras($datosCredito['tasaInteres']);
        $departamentoAgencia = $_SESSION['index']->agenciaActual->departamento;
        $municipioAgencia = $_SESSION['index']->agenciaActual->municipio;
        $nombreUsuario = $_SESSION['index']->nombres.' '.$_SESSION['index']->apellidos;
        $tmpFechaElaboracion = explode("-", $datosCredito['fechaGeneracion']);
        $fechaElaboracionLetras = generalFechaLetras($tmpFechaElaboracion[0], $tmpFechaElaboracion[1], $tmpFechaElaboracion[2]);
        $edadDeudor = $datosDeudor['edad'] . " AÑOS";
        $tipoDocumentoPrimario = $datosDeudor['labelTipoDocumento'];
        $documentoPrimario = $datosDeudor['numeroDocumento'];
        $documentoSecundario = $datosDeudor['nit'];
        $direccionCompleta = $datosDeudor['direccion'] . ", " . mb_strtoupper($datosDeudor['labelMunicipio'], "UTF-8") . ", " . mb_strtoupper($datosDeudor['labelDepartamento'], "UTF-8");

        $fiadores = [];

        $parrafoCompleto = 'Por el presente PAGARÉ, Yo, <b>' . $nombreCompletoDeudor . '</b> me obligo a pagar incondicionalmente a la orden de la ASOCIACIÓN COOPERATIVA DE AHORRO Y CREDITO DE PRODUCCIÓN AGROPECUARIA COMUNAL DE NUEVA CONCEPCIÓN DE RESPONSABILIDAD LIMITADA ( ACACYPAC N.C. DE R. L. ), Institución ahorro y créditos con domicilio en NUEVA CONCEPCION, departamento de CHALATENANGO, a partir de esta fecha, que vencerá el <b>' . $fechaVemcimiento . '</b>, en cualquiera de las Agencias de ACACYPAC, N.C. DE R.L., la cantidad de <b>' . $montoCreditoLetras . '</b>, obligándome a pagar sobre esta deuda y partir de esta fecha, el interés nominal variable del <b>' . trim($tasaInteresLetras) . '</b> por ciento anual sobre saldos. En caso de mora me obligo a pagarle a la Cooperativa <b>TREINTA Y SEIS</b> puntos adicionales al interés nominal variable que esté vigente a la fecha de la mora, sin que ellos signifiquen prórroga del plazo y sin perjuicio de los demás efectos legales de la mora. Para todos los efectos judiciales y extrajudiciales del presente pagaré, las variaciones del interés se probarán plena y fehacientemente con la constancia extendida por el contador de la Cooperativa con el visto bueno del Gerente. Señalo como domicilio especial el de la ciudad de ' . $municipioAgencia . ' a cuyos tribunales me someto, siendo a mi cargo cualquier gasto que la Cooperativa hiciere en el cobro de ésta obligación, inclusive los llamados personales aún cuando no fuere condenado en costas. Suscribo el presente PAGARE en la ciudad de ' . $municipioAgencia . ', departamento de ' . $departamentoAgencia . ', el día ' . $fechaElaboracionLetras . '.';

        $datosSuscriptor = 'F)__________________________________________________________________<br/>Nombre:   <b>' . $nombreCompletoDeudor . '</b><br/>Datos del Suscriptor:<br/>Edad: <b>' . $edadDeudor . '</b><br/>Documento de Identificación No:<br/>' . $tipoDocumentoPrimario . ': <b>' . $documentoPrimario . '</b><br/>NIT: <b>' . $documentoSecundario . '</b><br/>DIRECCIÓN: <b>' . $direccionCompleta . '</b>';

        foreach ($datosFiadores as $datosFiador) {
            $nombreCompletoFiador = $datosFiador['nombres'] . " " . $datosFiador['apellidos'];
            $nombreCompletoFiador .= !empty($datosFiador['conocido']) ? " CONOCIDO POR " . $datosFiador['conocido'] : "";
            $edadFiador = $datosFiador['edad'] . " AÑOS";
            $tipoDocumentoPrimarioFiador = $datosFiador['labelTipoDocumento'];
            $documentoPrimarioFiador = $datosFiador['numeroDocumento'];
            $documentoSecundarioFiador = $datosFiador['nit'];
            $direccionCompletaFiador = $datosFiador['direccion'] . ", " . mb_strtoupper($datosFiador['labelMunicipio'], "UTF-8") . ", " . mb_strtoupper($datosFiador['labelDepartamento'], "UTF-8");

            $detallesFiador = 'F)__________________________________________________________________<br/>Nombre:   <b>' . $nombreCompletoFiador . '</b><br/>Datos del aval:<br/>Edad: <b>' . $edadFiador . '</b><br/>Documento de Identificación No:<br/>' . $tipoDocumentoPrimarioFiador . ': <b>' . $documentoPrimarioFiador . '</b><br/>NIT: <b>' . $documentoSecundarioFiador . '</b><br/>DIRECCIÓN: <b>' . $direccionCompletaFiador . '</b>';

            array_push($fiadores, $detallesFiador);
        }

        $pdf->SetFont('dejavusans', 'B', 12.5);
        $pdf->Write(0, "ASOCIACION COOPERATIVA DE AHORRO, CREDITO Y PRODUCCIÓN AGROPECUARIA COMUNAL DE NUEVA CONCEPCIÓN DE RESPONSABILIDAD LIMITADA ACACYPAC, N.C. DE R.L.", '', 0, 'C', true, 0, false, false, 0);
        $pdf->SetFont('dejavusans', 'B', 10);
        $pdf->writeHTML('<br/><br/><span style="color:red; text-align:center;">PAGARE SIN PROTESTO</span>', true, false, true, false, '');

        $pdf->SetFont('dejavusans', '', 9);
        $pdf->writeHTML('<style>p{text-align:justify;}</style><br/><br/><p>' . $parrafoCompleto . '</p>', true, false, true, false, '');
        $pdf->writeHTML('<style>p{line-height:16px; text-align:justify;}</style><br/><br/><br/><br><p>' . $datosSuscriptor . '</p>', true, false, true, false, '');

        if (count($fiadores) > 0) {
            $detalleFiadores = implode("<br><br/><br/><br/>", $fiadores);
            $pdf->writeHTML('<style>p{line-height:16px; text-align:justify;}</style><p>OTROS:<br/>Por aval de la anterior obligación mercantil, firmo en la ciudad de ' . $municipioAgencia . ', departamento de ' . $departamentoAgencia . ', el día ' . $fechaElaboracionLetras . '.<br/><br/>', true, false, true, false, '');
            $pdf->writeHTML('<style>p{line-height:16px; text-align:justify;}</style><p>' . $detalleFiadores . '</p>', true, false, true, false, '');
        }

        $mypdf = __DIR__ . '\..\docs\Pagare\/' . $nombrePagare . '';
        $pdf->Output($mypdf, 'F');
    }
}
