<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET)) {
        include '../../code/connectionSqlServer.php';

        $tipoBusqueda = urldecode($_GET['tipoBusqueda']);
        $inicio = date('Y-m-d', strtotime(urldecode($_GET['inicio'])));
        $fin = date('Y-m-d', strtotime(urldecode($_GET['fin'])));

        $datosMutuos = new tmpResultado();
        $datosPagares = new tmpResultado();

        $cntResultados = 0;

        $queryMutuos = 'SELECT * FROM sga_viewReporteGeneralCantidad where fechaRegistro between :inicio and :fin;';
        $resultMutuos = $conexion->prepare($queryMutuos, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultMutuos->bindParam(':inicio', $inicio, PDO::PARAM_STR);
        $resultMutuos->bindParam(':fin', $fin, PDO::PARAM_STR);
        $resultMutuos->execute();

        if ($resultMutuos->rowCount() > 0) {
            $cntResultados++;
            $dataCantidad = $resultMutuos->fetchAll(PDO::FETCH_OBJ);
            $dataMonto = [];
            $datosMutuos = new tmpResultado();

            $totalCantidad = 0;
            $totalMonto = 0;

            $queryMutuos = 'SELECT * FROM sga_viewReporteGeneralMonto where fechaRegistro between :inicio and :fin';
            $resultMutuos = $conexion->prepare($queryMutuos, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultMutuos->bindParam(':inicio', $inicio, PDO::PARAM_STR);
            $resultMutuos->bindParam(':fin', $fin, PDO::PARAM_STR);
            $resultMutuos->execute();

            if ($resultMutuos->rowCount() > 0) {
                $dataMonto = $resultMutuos->fetchAll(PDO::FETCH_OBJ);
            }

            for ($agencia = 701; $agencia <= 710; $agencia++) {
                ${'totalCantidad' . $agencia} = 0;
                ${'totalMonto' . $agencia} = 0;
            }

            foreach ($dataCantidad as $key => $cantidad) {
                $fecha = date('d-m-Y', strtotime($cantidad->fechaRegistro));
                $totalCantidadFecha = 0;
                $totalMontoFecha = 0;
                !isset($datosMutuos->fechas->$fecha) && $datosMutuos->fechas->$fecha = new tmpValoresResultado();

                for ($agencia = 701; $agencia <= 710; $agencia++) {
                    $tmpCantidad = intval($cantidad->$agencia);
                    $monto = isset($dataMonto[$key]->$agencia) ? floatval($dataMonto[$key]->$agencia) : 0;
                    ${'totalCantidad' . $agencia} += $tmpCantidad;
                    ${'totalMonto' . $agencia} += $monto;
                    $totalCantidadFecha += $tmpCantidad;
                    $totalMontoFecha += $monto;
                    $datosMutuos->fechas->$fecha->{'A' . $agencia}->cantidad = $tmpCantidad;
                    $datosMutuos->fechas->$fecha->{'A' . $agencia}->monto = '$' . number_format($monto, 2, '.', ',');
                }

                $datosMutuos->fechas->$fecha->totales->cantidad = $totalCantidadFecha;
                $datosMutuos->fechas->$fecha->totales->monto = '$' . number_format($totalMontoFecha, 2, '.', ',');
                $totalCantidad += $totalCantidadFecha;
                $totalMonto += $totalMontoFecha;
            }

            for ($agencia = 701; $agencia <= 710; $agencia++) {
                $datosMutuos->totales->{'A' . $agencia}->cantidad = ${'totalCantidad' . $agencia};
                $datosMutuos->totales->{'A' . $agencia}->monto = '$' . number_format(${'totalMonto' . $agencia}, 2, '.', ',');
            }

            $datosMutuos->totales->totales->cantidad = $totalCantidad;
            $datosMutuos->totales->totales->monto = '$' . number_format($totalMonto, 2, '.', ',');
        }

        $queryPagares = 'SELECT * FROM sga_viewReporteGeneralPagareCantidad where fechaRegistro between :inicio and :fin;';
        $resultPagares = $conexion->prepare($queryPagares, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultPagares->bindParam(':inicio', $inicio, PDO::PARAM_STR);
        $resultPagares->bindParam(':fin', $fin, PDO::PARAM_STR);
        $resultPagares->execute();

        if ($resultPagares->rowCount() > 0) {
            $cntResultados++;
            $dataCantidad = $resultPagares->fetchAll(PDO::FETCH_OBJ);
            $dataMonto = [];
            $datosPagares = new tmpResultado();

            $totalCantidad = 0;
            $totalMonto = 0;

            $queryPagares = 'SELECT * FROM sga_viewReporteGeneralPagareMonto where fechaRegistro between :inicio and :fin';
            $resultPagares = $conexion->prepare($queryPagares, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultPagares->bindParam(':inicio', $inicio, PDO::PARAM_STR);
            $resultPagares->bindParam(':fin', $fin, PDO::PARAM_STR);
            $resultPagares->execute();

            if ($resultPagares->rowCount() > 0) {
                $dataMonto = $resultPagares->fetchAll(PDO::FETCH_OBJ);
            }

            for ($agencia = 701; $agencia <= 710; $agencia++) {
                ${'totalCantidad' . $agencia} = 0;
                ${'totalMonto' . $agencia} = 0;
            }

            foreach ($dataCantidad as $key => $cantidad) {
                $fecha = date('d-m-Y', strtotime($cantidad->fechaRegistro));
                $totalCantidadFecha = 0;
                $totalMontoFecha = 0;
                !isset($datosPagares->fechas->$fecha) && $datosPagares->fechas->$fecha = new tmpValoresResultado();

                for ($agencia = 701; $agencia <= 710; $agencia++) {
                    $tmpCantidad = intval($cantidad->$agencia);
                    $monto = isset($dataMonto[$key]->$agencia) ? floatval($dataMonto[$key]->$agencia) : 0;
                    ${'totalCantidad' . $agencia} += $tmpCantidad;
                    ${'totalMonto' . $agencia} += $monto;
                    $totalCantidadFecha += $tmpCantidad;
                    $totalMontoFecha += $monto;
                    $datosPagares->fechas->$fecha->{'A' . $agencia}->cantidad = $tmpCantidad;
                    $datosPagares->fechas->$fecha->{'A' . $agencia}->monto = '$' . number_format($monto, 2, '.', ',');
                }

                $datosPagares->fechas->$fecha->totales->cantidad = $tmpCantidad;
                $datosPagares->fechas->$fecha->totales->monto = '$' . number_format($monto, 2, '.', ',');
                $totalCantidad += $totalCantidadFecha;
                $totalMonto += $totalMontoFecha;
            }

            for ($agencia = 701; $agencia <= 710; $agencia++) {
                $datosPagares->totales->{'A' . $agencia}->cantidad = ${'totalCantidad' . $agencia};
                $datosPagares->totales->{'A' . $agencia}->monto = '$' . number_format(${'totalMonto' . $agencia}, 2, '.', ',');
            }

            $datosPagares->totales->totales->cantidad = $totalCantidad;
            $datosPagares->totales->totales->monto = '$' . number_format($totalMonto, 2, '.', ',');
        }

        if ($cntResultados > 0) {
            $respuesta->{'respuesta'} = 'EXITO';
            $respuesta->{'datosMutuos'} = $datosMutuos;
            $respuesta->{'datosPagares'} = $datosPagares;
        } else {
            $respuesta->{'respuesta'} = 'SinRegistros';
        }

        $conexion = null;
    }
} else {
    $respuesta->{'resuesta'} = 'SESION';
}

echo json_encode($respuesta);

class tmpResultado
{
    public $fechas;
    public $totales;

    function __construct()
    {
        $this->fechas = (object)[];
        $this->totales = new tmpValoresResultado();
    }
}

class tmpValoresResultado
{
    public $A701;
    public $A702;
    public $A703;
    public $A704;
    public $A705;
    public $A706;
    public $A707;
    public $A708;
    public $A709;
    public $A710;
    public $totales;

    function __construct()
    {
        $this->A701 = new tmpValores();
        $this->A702 = new tmpValores();
        $this->A703 = new tmpValores();
        $this->A704 = new tmpValores();
        $this->A705 = new tmpValores();
        $this->A706 = new tmpValores();
        $this->A707 = new tmpValores();
        $this->A708 = new tmpValores();
        $this->A709 = new tmpValores();
        $this->A710 = new tmpValores();
        $this->totales = new tmpValores();
    }
}

class tmpValores
{
    public $cantidad;
    public $monto;

    function __construct()
    {
        $this->cantidad = 0;
        $this->monto = '$0.00';
    }
}
