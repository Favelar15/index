<?php
require_once "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include '../../code/connectionSqlServer.php';
        require_once './Models/mutuo.php';

        $idUsuario = $_SESSION["index"]->id;
        $tipoApartado = $input["tipoApartado"];
        $idApartado = base64_decode(urldecode($input["idApartado"]));

        $mutuo = new mutuo();
        $mutuo->id = $input["id"];
        $mutuo->datosCredito = $input["datosCredito"];
        $mutuo->datosDeudor = $input["datosDeudor"];
        $mutuo->datosFirmante = $input["datosFirmante"];
        $mutuo->garantias = $input["garantias"];
        $mutuo->incluyeFirmante = $input["incluyeFirmante"];
        $mutuo->incluyeParciales = $input["incluyeParciales"];

        $guardar = $mutuo->saveMutuo($idUsuario, $ipActual, $tipoApartado, $idApartado);

        $respuesta = $guardar;

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
