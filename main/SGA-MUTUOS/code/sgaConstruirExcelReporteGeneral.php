<?php
require_once "../../code/generalParameters.php";
require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        $filename = "ReporteGeneral.xlsx";

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Fecha');
        $sheet->mergeCells("A1:A2");
        $sheet->setCellValue('B1', '701');
        $sheet->mergeCells("B1:C1");
        $sheet->setCellValue('B2', 'Cantidad');
        $sheet->setCellValue('C2', 'Monto');
        $sheet->setCellValue('D1', '702');
        $sheet->mergeCells("D1:E1");
        $sheet->setCellValue('D2', 'Cantidad');
        $sheet->setCellValue('E2', 'Monto');
        $sheet->setCellValue('F1', '703');
        $sheet->mergeCells("F1:G1");
        $sheet->setCellValue('F2', 'Cantidad');
        $sheet->setCellValue('G2', 'Monto');
        $sheet->setCellValue('H1', '704');
        $sheet->mergeCells("H1:I1");
        $sheet->setCellValue('H2', 'Cantidad');
        $sheet->setCellValue('I2', 'Monto');
        $sheet->setCellValue('J1', '705');
        $sheet->mergeCells("J1:K1");
        $sheet->setCellValue('J2', 'Cantidad');
        $sheet->setCellValue('K2', 'Monto');
        $sheet->setCellValue('L1', '706');
        $sheet->mergeCells("L1:M1");
        $sheet->setCellValue('L2', 'Cantidad');
        $sheet->setCellValue('M2', 'Monto');
        $sheet->setCellValue('N1', '707');
        $sheet->mergeCells("N1:O1");
        $sheet->setCellValue('N2', 'Cantidad');
        $sheet->setCellValue('O2', 'Monto');
        $sheet->setCellValue('P1', '708');
        $sheet->mergeCells("P1:Q1");
        $sheet->setCellValue('P2', 'Cantidad');
        $sheet->setCellValue('Q2', 'Monto');
        $sheet->setCellValue('R1', '709');
        $sheet->mergeCells("R1:S1");
        $sheet->setCellValue('R2', 'Cantidad');
        $sheet->setCellValue('S2', 'Monto');
        $sheet->setCellValue('T1', '710');
        $sheet->mergeCells("T1:U1");
        $sheet->setCellValue('T2', 'Cantidad');
        $sheet->setCellValue('U2', 'Monto');
        $sheet->setCellValue('V1', 'Totales');
        $sheet->mergeCells("V1:W1");
        $sheet->setCellValue('V2', 'Cantidad');
        $sheet->setCellValue('W2', 'Monto');

        $cntLineas = 2;

        foreach ($input['datosMutuos']['fechas'] as $key => $registro) {
            $cntLineas++;
            $fecha = $key;
            $sheet->setCellValue("A" . $cntLineas, $fecha);
            $sheet->setCellValue("B" . $cntLineas, $registro['A701']['cantidad']);
            $sheet->setCellValue("C" . $cntLineas, $registro['A701']['monto']);
            $sheet->setCellValue("D" . $cntLineas, $registro['A702']['cantidad']);
            $sheet->setCellValue("E" . $cntLineas, $registro['A702']['monto']);
            $sheet->setCellValue("F" . $cntLineas, $registro['A703']['cantidad']);
            $sheet->setCellValue("G" . $cntLineas, $registro['A703']['monto']);
            $sheet->setCellValue("H" . $cntLineas, $registro['A704']['cantidad']);
            $sheet->setCellValue("I" . $cntLineas, $registro['A704']['monto']);
            $sheet->setCellValue("J" . $cntLineas, $registro['A705']['cantidad']);
            $sheet->setCellValue("K" . $cntLineas, $registro['A705']['monto']);
            $sheet->setCellValue("L" . $cntLineas, $registro['A706']['cantidad']);
            $sheet->setCellValue("M" . $cntLineas, $registro['A706']['monto']);
            $sheet->setCellValue("N" . $cntLineas, $registro['A707']['cantidad']);
            $sheet->setCellValue("O" . $cntLineas, $registro['A707']['monto']);
            $sheet->setCellValue("P" . $cntLineas, $registro['A708']['cantidad']);
            $sheet->setCellValue("Q" . $cntLineas, $registro['A708']['monto']);
            $sheet->setCellValue("R" . $cntLineas, $registro['A709']['cantidad']);
            $sheet->setCellValue("S" . $cntLineas, $registro['A709']['monto']);
            $sheet->setCellValue("T" . $cntLineas, $registro['A710']['cantidad']);
            $sheet->setCellValue("U" . $cntLineas, $registro['A710']['monto']);
            $sheet->setCellValue("V" . $cntLineas, $registro['totales']['cantidad']);
            $sheet->setCellValue("W" . $cntLineas, $registro['totales']['monto']);
        }

        $cntLineas++;
        $sheet->setCellValue("A" . $cntLineas, 'Totales');
        $sheet->setCellValue("B" . $cntLineas, $input['datosMutuos']['totales']['A701']['cantidad']);
        $sheet->setCellValue("C" . $cntLineas, $input['datosMutuos']['totales']['A701']['monto']);
        $sheet->setCellValue("D" . $cntLineas, $input['datosMutuos']['totales']['A702']['cantidad']);
        $sheet->setCellValue("E" . $cntLineas, $input['datosMutuos']['totales']['A702']['monto']);
        $sheet->setCellValue("F" . $cntLineas, $input['datosMutuos']['totales']['A703']['cantidad']);
        $sheet->setCellValue("G" . $cntLineas, $input['datosMutuos']['totales']['A703']['monto']);
        $sheet->setCellValue("H" . $cntLineas, $input['datosMutuos']['totales']['A704']['cantidad']);
        $sheet->setCellValue("I" . $cntLineas, $input['datosMutuos']['totales']['A704']['monto']);
        $sheet->setCellValue("J" . $cntLineas, $input['datosMutuos']['totales']['A705']['cantidad']);
        $sheet->setCellValue("K" . $cntLineas, $input['datosMutuos']['totales']['A705']['monto']);
        $sheet->setCellValue("L" . $cntLineas, $input['datosMutuos']['totales']['A706']['cantidad']);
        $sheet->setCellValue("M" . $cntLineas, $input['datosMutuos']['totales']['A706']['monto']);
        $sheet->setCellValue("N" . $cntLineas, $input['datosMutuos']['totales']['A707']['cantidad']);
        $sheet->setCellValue("O" . $cntLineas, $input['datosMutuos']['totales']['A707']['monto']);
        $sheet->setCellValue("P" . $cntLineas, $input['datosMutuos']['totales']['A708']['cantidad']);
        $sheet->setCellValue("Q" . $cntLineas, $input['datosMutuos']['totales']['A708']['monto']);
        $sheet->setCellValue("R" . $cntLineas, $input['datosMutuos']['totales']['A709']['cantidad']);
        $sheet->setCellValue("S" . $cntLineas, $input['datosMutuos']['totales']['A709']['monto']);
        $sheet->setCellValue("T" . $cntLineas, $input['datosMutuos']['totales']['A710']['cantidad']);
        $sheet->setCellValue("U" . $cntLineas, $input['datosMutuos']['totales']['A710']['monto']);
        $sheet->setCellValue("V" . $cntLineas, $input['datosMutuos']['totales']['totales']['cantidad']);
        $sheet->setCellValue("W" . $cntLineas, $input['datosMutuos']['totales']['totales']['monto']);

        $spreadsheet->getActiveSheet()->getStyle('A1:W1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:W1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');
        $spreadsheet->getActiveSheet()->getStyle('A2:W2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:W2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');
        $spreadsheet->getActiveSheet()->getStyle('A' . $cntLineas . ':W' . $cntLineas . '')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A' . $cntLineas . ':W' . $cntLineas . '')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');
        $spreadsheet->getActiveSheet()->getStyle('A:W')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->setTitle('Mutuos');

        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1);

        $spreadsheet->getActiveSheet()->setTitle('Pagares');

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Fecha');
        $sheet->mergeCells("A1:A2");
        $sheet->setCellValue('B1', '701');
        $sheet->mergeCells("B1:C1");
        $sheet->setCellValue('B2', 'Cantidad');
        $sheet->setCellValue('C2', 'Monto');
        $sheet->setCellValue('D1', '702');
        $sheet->mergeCells("D1:E1");
        $sheet->setCellValue('D2', 'Cantidad');
        $sheet->setCellValue('E2', 'Monto');
        $sheet->setCellValue('F1', '703');
        $sheet->mergeCells("F1:G1");
        $sheet->setCellValue('F2', 'Cantidad');
        $sheet->setCellValue('G2', 'Monto');
        $sheet->setCellValue('H1', '704');
        $sheet->mergeCells("H1:I1");
        $sheet->setCellValue('H2', 'Cantidad');
        $sheet->setCellValue('I2', 'Monto');
        $sheet->setCellValue('J1', '705');
        $sheet->mergeCells("J1:K1");
        $sheet->setCellValue('J2', 'Cantidad');
        $sheet->setCellValue('K2', 'Monto');
        $sheet->setCellValue('L1', '706');
        $sheet->mergeCells("L1:M1");
        $sheet->setCellValue('L2', 'Cantidad');
        $sheet->setCellValue('M2', 'Monto');
        $sheet->setCellValue('N1', '707');
        $sheet->mergeCells("N1:O1");
        $sheet->setCellValue('N2', 'Cantidad');
        $sheet->setCellValue('O2', 'Monto');
        $sheet->setCellValue('P1', '708');
        $sheet->mergeCells("P1:Q1");
        $sheet->setCellValue('P2', 'Cantidad');
        $sheet->setCellValue('Q2', 'Monto');
        $sheet->setCellValue('R1', '709');
        $sheet->mergeCells("R1:S1");
        $sheet->setCellValue('R2', 'Cantidad');
        $sheet->setCellValue('S2', 'Monto');
        $sheet->setCellValue('T1', '710');
        $sheet->mergeCells("T1:U1");
        $sheet->setCellValue('T2', 'Cantidad');
        $sheet->setCellValue('U2', 'Monto');
        $sheet->setCellValue('V1', 'Totales');
        $sheet->mergeCells("V1:W1");
        $sheet->setCellValue('V2', 'Cantidad');
        $sheet->setCellValue('W2', 'Monto');

        $cntLineas = 2;

        foreach ($input['datosPagares']['fechas'] as $key => $registro) {
            $cntLineas++;
            $fecha = $key;
            $sheet->setCellValue("A" . $cntLineas, $fecha);
            $sheet->setCellValue("B" . $cntLineas, $registro['A701']['cantidad']);
            $sheet->setCellValue("C" . $cntLineas, $registro['A701']['monto']);
            $sheet->setCellValue("D" . $cntLineas, $registro['A702']['cantidad']);
            $sheet->setCellValue("E" . $cntLineas, $registro['A702']['monto']);
            $sheet->setCellValue("F" . $cntLineas, $registro['A703']['cantidad']);
            $sheet->setCellValue("G" . $cntLineas, $registro['A703']['monto']);
            $sheet->setCellValue("H" . $cntLineas, $registro['A704']['cantidad']);
            $sheet->setCellValue("I" . $cntLineas, $registro['A704']['monto']);
            $sheet->setCellValue("J" . $cntLineas, $registro['A705']['cantidad']);
            $sheet->setCellValue("K" . $cntLineas, $registro['A705']['monto']);
            $sheet->setCellValue("L" . $cntLineas, $registro['A706']['cantidad']);
            $sheet->setCellValue("M" . $cntLineas, $registro['A706']['monto']);
            $sheet->setCellValue("N" . $cntLineas, $registro['A707']['cantidad']);
            $sheet->setCellValue("O" . $cntLineas, $registro['A707']['monto']);
            $sheet->setCellValue("P" . $cntLineas, $registro['A708']['cantidad']);
            $sheet->setCellValue("Q" . $cntLineas, $registro['A708']['monto']);
            $sheet->setCellValue("R" . $cntLineas, $registro['A709']['cantidad']);
            $sheet->setCellValue("S" . $cntLineas, $registro['A709']['monto']);
            $sheet->setCellValue("T" . $cntLineas, $registro['A710']['cantidad']);
            $sheet->setCellValue("U" . $cntLineas, $registro['A710']['monto']);
            $sheet->setCellValue("V" . $cntLineas, $registro['totales']['cantidad']);
            $sheet->setCellValue("W" . $cntLineas, $registro['totales']['monto']);
        }

        $cntLineas++;
        $sheet->setCellValue("A" . $cntLineas, 'Totales');
        $sheet->setCellValue("B" . $cntLineas, $input['datosPagares']['totales']['A701']['cantidad']);
        $sheet->setCellValue("C" . $cntLineas, $input['datosPagares']['totales']['A701']['monto']);
        $sheet->setCellValue("D" . $cntLineas, $input['datosPagares']['totales']['A702']['cantidad']);
        $sheet->setCellValue("E" . $cntLineas, $input['datosPagares']['totales']['A702']['monto']);
        $sheet->setCellValue("F" . $cntLineas, $input['datosPagares']['totales']['A703']['cantidad']);
        $sheet->setCellValue("G" . $cntLineas, $input['datosPagares']['totales']['A703']['monto']);
        $sheet->setCellValue("H" . $cntLineas, $input['datosPagares']['totales']['A704']['cantidad']);
        $sheet->setCellValue("I" . $cntLineas, $input['datosPagares']['totales']['A704']['monto']);
        $sheet->setCellValue("J" . $cntLineas, $input['datosPagares']['totales']['A705']['cantidad']);
        $sheet->setCellValue("K" . $cntLineas, $input['datosPagares']['totales']['A705']['monto']);
        $sheet->setCellValue("L" . $cntLineas, $input['datosPagares']['totales']['A706']['cantidad']);
        $sheet->setCellValue("M" . $cntLineas, $input['datosPagares']['totales']['A706']['monto']);
        $sheet->setCellValue("N" . $cntLineas, $input['datosPagares']['totales']['A707']['cantidad']);
        $sheet->setCellValue("O" . $cntLineas, $input['datosPagares']['totales']['A707']['monto']);
        $sheet->setCellValue("P" . $cntLineas, $input['datosPagares']['totales']['A708']['cantidad']);
        $sheet->setCellValue("Q" . $cntLineas, $input['datosPagares']['totales']['A708']['monto']);
        $sheet->setCellValue("R" . $cntLineas, $input['datosPagares']['totales']['A709']['cantidad']);
        $sheet->setCellValue("S" . $cntLineas, $input['datosPagares']['totales']['A709']['monto']);
        $sheet->setCellValue("T" . $cntLineas, $input['datosPagares']['totales']['A710']['cantidad']);
        $sheet->setCellValue("U" . $cntLineas, $input['datosPagares']['totales']['A710']['monto']);
        $sheet->setCellValue("V" . $cntLineas, $input['datosPagares']['totales']['totales']['cantidad']);
        $sheet->setCellValue("W" . $cntLineas, $input['datosPagares']['totales']['totales']['monto']);

        $spreadsheet->getActiveSheet()->getStyle('A1:W1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:W1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');
        $spreadsheet->getActiveSheet()->getStyle('A2:W2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:W2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');
        $spreadsheet->getActiveSheet()->getStyle('A' . $cntLineas . ':W' . $cntLineas . '')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A' . $cntLineas . ':W' . $cntLineas . '')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');
        $spreadsheet->getActiveSheet()->getStyle('A:W')->getAlignment()->setHorizontal('center');

        $spreadsheet->setActiveSheetIndex(0);

        $writer = new Xlsx($spreadsheet);
        $writer->save("../docs/Reportes/" . $filename);

        $respuesta->{'respuesta'} = 'EXITO';
        $respuesta->{'nombreArchivo'} = $filename;
    }
} else {
    $respuesta->{'respuesta'} = 'SESION';
}

echo json_encode($respuesta);
