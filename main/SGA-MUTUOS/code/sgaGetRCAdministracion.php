<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET)) {
        include '../../code/connectionSqlServer.php';
        require_once './Models/resolucionClausula.php';

        $tipoApartado = $_GET["tipoApartado"];
        $idApartado = base64_decode(urldecode($_GET["idApartado"]));
        $idUsuario = $_SESSION["index"]->id;
        $agencia = $_GET["agencia"];
        $inicio = date('Y-m-d', strtotime(urldecode($_GET["inicio"])));
        $fin = date('Y-m-d', strtotime(urldecode($_GET["fin"])));

        $RC = new resolucionClausula();
        $tipoRol = getTipoRol($tipoApartado, $idApartado);

        $RCs = $RC->getRCAdministracion($tipoRol, $idUsuario, $agencia, $inicio, $fin);

        if (isset($RCs["respuesta"])) {
            $respuesta->{"respuesta"} = $RCs["respuesta"];
            isset($RCs["resultados"]) && $respuesta->{"resultados"} = $RCs["resultados"];
        } else {
            $respuesta->{"respuesta"} = "Error al conectar con la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = 'SESION';
}

echo json_encode($respuesta);
