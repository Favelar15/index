<?php
class zona
{
    public $idZona;
    public $zona;
    public $secciones;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function getZonas()
    {
        $query = "SELECT * FROM sga_viewZonasHipotecas;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $data = $result->fetchAll(PDO::FETCH_OBJ);
            $zonas = new stdClass();

            foreach ($data as $zona) {
                if (!isset($zonas->{$zona->idZona})) {
                    $tmpZona = new stdClass();
                    $tmpZona->id = $zona->idZona;
                    $tmpZona->zona = $zona->zona;
                    $tmpZona->secciones = new stdClass();

                    $zonas->{$zona->idZona} = $tmpZona;
                }

                $tmpSeccion = new stdClass();
                $tmpSeccion->id = $zona->idSeccion;
                $tmpSeccion->seccion = $zona->seccion;
                $tmpSeccion->idDepartamento = $zona->idDepartamento;
                $tmpSeccion->departamento = $zona->departamento;

                $zonas->{$zona->idZona}->secciones->{$zona->idSeccion} = $tmpSeccion;
            }

            $this->conexion = null;
            return $zonas;
        }

        $this->conexion = null;
        return new stdClass();
    }
}
