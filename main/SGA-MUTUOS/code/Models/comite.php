<?php
class comite
{
    public $id;
    public $comite;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function getComitesCbo()
    {
        $query = 'SELECT * FROM sga_viewComites;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }
}
