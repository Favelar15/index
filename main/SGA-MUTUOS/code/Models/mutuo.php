<?php
class mutuo
{
    public $id;
    public $datosCredito;
    public $datosDeudor;
    public $datosFirmante;
    public $garantias;
    public $incluyeFirmante;
    public $incluyeParciales;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function saveMutuo($idUsuario, $ipActual, $tipoApartado, $idApartado)
    {
        $arrErrores = [];
        $idsAditivos = [];
        $idsGarantias = [];
        $idsDesembolsos = [];
        $responseCredito = null;
        $id = null;
        $idDeudor = null;
        $idFirmante = null;
        $permitirGuardado = true;

        if (isset($this->datosCredito["logs"])) {
            $queryCredito = "EXEC sga_spGuardarCredito :id,:agencia,:numeroCredito,:monto,:tasaInteres,:lineaFinanciera,:destino,:vencimiento,:periodicidad,:plazo,:tipoPlazo,:fechaGeneracion,:fuenteFondos,:empleado,:desembolsoParcial,:lineaRotativa,:fechaVencimiento,:capitalInteres,:descripcionLog,:detallesLog,:tipoApartado,:idApartado,:usuario,:ip,:response,:idCredito;";
            $fechaVencimiento = !empty($this->datosCredito["fechaVencimiento"]) ? date('Y-m-d H:i', strtotime($this->datosCredito["fechaVencimiento"])) : null;
            $fechaGeneracion = date('Y-m-d H:i', strtotime($this->datosCredito["fechaGeneracion"]));

            $arrDetallesLogs = [];


            if (isset($this->datosCredito["logs"]["detalles"])) {
                foreach ($this->datosCredito["logs"]["detalles"] as $detalleLog) {
                    $arrDetallesLogs[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                }
            }
            $detallesLog = count($arrDetallesLogs) > 0 ? implode('@@@', $arrDetallesLogs) : null;

            $resultCredito = $this->conexion->prepare($queryCredito, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultCredito->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultCredito->bindParam(':agencia', $this->datosCredito["agencia"], PDO::PARAM_INT);
            $resultCredito->bindParam(':numeroCredito', $this->datosCredito["numeroCredito"], PDO::PARAM_STR);
            $resultCredito->bindParam(':monto', $this->datosCredito["monto"], PDO::PARAM_STR);
            $resultCredito->bindParam(':tasaInteres', $this->datosCredito["tasaInteres"], PDO::PARAM_STR);
            $resultCredito->bindParam(':lineaFinanciera', $this->datosCredito["lineaFinanciera"], PDO::PARAM_INT);
            $resultCredito->bindParam(':destino', $this->datosCredito["destino"], PDO::PARAM_STR);
            $resultCredito->bindParam(':vencimiento', $this->datosCredito["vencimiento"], PDO::PARAM_STR);
            $resultCredito->bindParam(':periodicidad', $this->datosCredito["periodicidad"], PDO::PARAM_INT);
            $resultCredito->bindParam(':plazo', $this->datosCredito["plazo"], PDO::PARAM_INT);
            $resultCredito->bindParam(':tipoPlazo', $this->datosCredito["tipoPlazo"], PDO::PARAM_INT);
            $resultCredito->bindParam(':fechaGeneracion', $fechaGeneracion, PDO::PARAM_STR);
            $resultCredito->bindParam(':fuenteFondos', $this->datosCredito["fuenteFondos"], PDO::PARAM_INT);
            $resultCredito->bindParam(':empleado', $this->datosCredito["empleadoACACYPAC"], PDO::PARAM_STR);
            $resultCredito->bindParam(':desembolsoParcial', $this->datosCredito["desembolsoParcial"], PDO::PARAM_STR);
            $resultCredito->bindParam(':lineaRotativa', $this->datosCredito["lineaRotativa"], PDO::PARAM_STR);
            $resultCredito->bindParam(':fechaVencimiento', $fechaVencimiento, PDO::PARAM_STR);
            $resultCredito->bindParam(':capitalInteres', $this->datosCredito["capitalInteres"], PDO::PARAM_STR);
            $resultCredito->bindParam(':descripcionLog', $this->datosCredito["logs"]["descripcion"], PDO::PARAM_STR);
            $resultCredito->bindParam(':detallesLog', $detallesLog, PDO::PARAM_STR);
            $resultCredito->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
            $resultCredito->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
            $resultCredito->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
            $resultCredito->bindParam(':ip', $ipActual, PDO::PARAM_STR);
            $resultCredito->bindParam(':response', $responseCredito, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
            $resultCredito->bindParam(':idCredito', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

            if ($resultCredito->execute()) {
                $responseCredito != 'EXITO' && $arrErrores["Crédito"] = $responseCredito;
                $responseCredito != 'EXITO' && $permitirGuardado = false;
                $this->id = $id;
            }
        }

        if ($permitirGuardado) {
            if ($this->id > 0 && count($this->datosCredito["aditivos"]) > 0) {
                foreach ($this->datosCredito["aditivos"] as $key => $aditivo) {
                    if (isset($aditivo["logs"])) {
                        $responseAditivo = null;
                        $idAditivo = null;
                        $arrDetallesLogs = [];
                        if (isset($aditivo["logs"]["detalles"])) {
                            $arrDetallesLogs[] = $aditivo["logs"]["detalles"]["campo"] . '%%%' . $aditivo["logs"]["detalles"]["valorAnterior"] . '%%%' . $aditivo["logs"]["detalles"]["nuevoValor"];
                        }
                        $detallesLogAditivo = count($arrDetallesLogs) > 0 ? implode("@@@", $arrDetallesLogs) : null;
                        if ($aditivo["logs"]["accion"] == 'Guardar') {
                            $queryAditivo = "EXEC sga_spGuardarAditivo :idCredito,:id,:idAditivo,:monto,:tipoApartado,:idApartado,:usuario,:ip,:descripcionLog,:detallesLog,:response,:idDb;";
                            $resultAditivo = $this->conexion->prepare($queryAditivo, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultAditivo->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                            $resultAditivo->bindParam(':id', $aditivo["id"], PDO::PARAM_INT);
                            $resultAditivo->bindParam(':idAditivo', $aditivo["idAditivo"], PDO::PARAM_INT);
                            $resultAditivo->bindParam(':monto', $aditivo["monto"], PDO::PARAM_STR);
                            $resultAditivo->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultAditivo->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultAditivo->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultAditivo->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultAditivo->bindParam(':descripcionLog', $aditivo["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultAditivo->bindParam(':detallesLog', $detallesLogAditivo, PDO::PARAM_STR);
                            $resultAditivo->bindParam(':response', $responseAditivo, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                            $resultAditivo->bindParam(':idDb', $idAditivo, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultAditivo->execute()) {
                                $responseAditivo != 'EXITO' && $arrErrores["Aditivo" . $key] = $responseAditivo;
                                $idsAditivos[$key] = $idAditivo;
                            }
                        } elseif ($aditivo["logs"]["accion"] == 'Eliminar') {
                            $queryAditivo = 'EXEC sga_spEliminarAditivo :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                            $resultAditivo = $this->conexion->prepare($queryAditivo, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultAditivo->bindParam(':id', $aditivo["id"], PDO::PARAM_INT);
                            $resultAditivo->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultAditivo->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultAditivo->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultAditivo->bindParam(':descripcion', $aditivo["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultAditivo->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultAditivo->bindParam(':response', $responseAditivo, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultAditivo->execute()) {
                                $responseAditivo != 'EXITO' && $arrErrores["Aditivo" . $key] = $responseAditivo;
                                $idsAditivos[$key] = $aditivo["id"];
                            }
                        }
                    } else {
                        $idsAditivos[$key] = $aditivo["id"];
                    }
                }
            }

            if ($this->id > 0 && isset($this->datosDeudor["logs"])) {
                $responseDeudor = null;
                $arrDetallesLogs = [];

                if (isset($this->datosDeudor["logs"]["detalles"])) {
                    foreach ($this->datosDeudor["logs"]["detalles"] as $detalleLog) {
                        $arrDetallesLogs[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                    }
                }
                $detallesLogDeudor = count($arrDetallesLogs) > 0 ? implode("@@@", $arrDetallesLogs) : null;
                $queryDeudor = 'EXEC sga_spGuardarDeudor :idCredito,:id,:codigoCliente,:tipoDocumento,:numeroDocumento,:nit,:nombres,:apellidos,:conocido,:genero,:pais,:departamento,:municipio,:profesion,:fechaNacimiento,:edad,:sabeFirmar,:usuario,:ip,:descripcionLog,:detallesLog,:tipoApartado,:idApartado,:response,:idDeudor;';
                $fechaNacimiento = date('Y-m-d', strtotime($this->datosDeudor["fechaNacimiento"]));
                $resultDeudor = $this->conexion->prepare($queryDeudor, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultDeudor->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                $resultDeudor->bindParam(':id', $this->datosDeudor["id"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':codigoCliente', $this->datosDeudor["codigoCliente"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':tipoDocumento', $this->datosDeudor["tipoDocumento"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':numeroDocumento', $this->datosDeudor["numeroDocumento"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':nit', $this->datosDeudor["nit"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':nombres', $this->datosDeudor["nombres"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':apellidos', $this->datosDeudor["apellidos"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':conocido', $this->datosDeudor["conocido"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':genero', $this->datosDeudor["genero"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':pais', $this->datosDeudor["pais"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':departamento', $this->datosDeudor["departamento"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':municipio', $this->datosDeudor["municipio"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':profesion', $this->datosDeudor["profesion"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':fechaNacimiento', $fechaNacimiento, PDO::PARAM_STR);
                $resultDeudor->bindParam(':edad', $this->datosDeudor["edad"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':sabeFirmar', $this->datosDeudor["firma"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                $resultDeudor->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                $resultDeudor->bindParam(':descripcionLog', $this->datosDeudor["logs"]["descripcion"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':detallesLog', $detallesLogDeudor, PDO::PARAM_STR);
                $resultDeudor->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                $resultDeudor->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                $resultDeudor->bindParam(':response', $responseDeudor, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                $resultDeudor->bindParam(':idDeudor', $idDeudor, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                if ($resultDeudor->execute()) {
                    $responseDeudor != 'EXITO' && $arrErrores["Deudor"] = $responseDeudor;
                }
            } else {
                $idDeudor = $this->datosDeudor["id"];
            }

            $idFirmante = $this->datosFirmante["id"];
            if ($this->id > 0 && isset($this->datosFirmante["logs"])) {
                $responseFirmante = null;
                $arrDetallesLogs = [];
                if (isset($this->datosFirmante["logs"]["detalles"])) {
                    foreach ($this->datosFirmante["logs"]["detalles"] as $detalleLog) {
                        $arrDetallesLogs[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                    }
                }
                $detallesLogFirmante = count($arrDetallesLogs) > 0 ? implode('@@@', $arrDetallesLogs) : null;
                if ($this->datosFirmante["logs"]["accion"] == 'Guardar') {
                    echo $responseFirmante;
                    $fechaNacimiento = date('Y-m-d', strtotime($this->datosFirmante["fechaNacimiento"]));
                    $queryFirmante = "EXEC sga_spGuardarFirmante :idCredito,:id,:tipoDocumento,:documento,:nombres,:apellidos,:conocido,:pais,:departamento,:municipio,:profesion,:fechaNacimiento,:edad,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detalleLog,:response,:idFirmante;";
                    $resultFirmante = $this->conexion->prepare($queryFirmante, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                    $resultFirmante->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                    $resultFirmante->bindParam(':id', $this->datosFirmante["id"], PDO::PARAM_INT);
                    $resultFirmante->bindParam(':tipoDocumento', $this->datosFirmante["tipoDocumento"], PDO::PARAM_INT);
                    $resultFirmante->bindParam(':documento', $this->datosFirmante["numeroDocumento"], PDO::PARAM_STR);
                    $resultFirmante->bindParam(':nombres', $this->datosFirmante["nombres"], PDO::PARAM_STR);
                    $resultFirmante->bindParam(':apellidos', $this->datosFirmante["apellidos"], PDO::PARAM_STR);
                    $resultFirmante->bindParam(':conocido', $this->datosFirmante["conocido"], PDO::PARAM_STR);
                    $resultFirmante->bindParam(':pais', $this->datosFirmante["pais"], PDO::PARAM_INT);
                    $resultFirmante->bindParam(':departamento', $this->datosFirmante["departamento"], PDO::PARAM_INT);
                    $resultFirmante->bindParam(':municipio', $this->datosFirmante["municipio"], PDO::PARAM_INT);
                    $resultFirmante->bindParam(':profesion', $this->datosFirmante["profesion"], PDO::PARAM_STR);
                    $resultFirmante->bindParam(':fechaNacimiento', $fechaNacimiento, PDO::PARAM_STR);
                    $resultFirmante->bindParam(':edad', $this->datosFirmante["edad"], PDO::PARAM_INT);
                    $resultFirmante->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                    $resultFirmante->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                    $resultFirmante->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                    $resultFirmante->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                    $resultFirmante->bindParam(':descripcionLog', $this->datosFirmante["logs"]["descripcion"], PDO::PARAM_STR);
                    $resultFirmante->bindParam(':detalleLog', $detallesLogFirmante, PDO::PARAM_STR);
                    $resultFirmante->bindParam(':response', $responseFirmante, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                    $resultFirmante->bindParam(':idFirmante', $idFirmante, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                    if ($resultFirmante->execute()) {
                        $responseFirmante != 'EXITO' && $arrErrores["firmante"] = $responseFirmante;
                    }
                } elseif ($this->datosFirmante["logs"]["accion"] == 'Eliminar') {
                    $queryFirmante = 'EXEC sga_spEliminarFirmanteRuego :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                    $resultFirmante = $this->conexion->prepare($queryFirmante, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                    $resultFirmante->bindParam(':id', $this->datosFirmante["id"], PDO::PARAM_INT);
                    $resultFirmante->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                    $resultFirmante->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                    $resultFirmante->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                    $resultFirmante->bindParam(':descripcion', $this->datosFirmante["logs"]["descripcion"], PDO::PARAM_STR);
                    $resultFirmante->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                    $resultFirmante->bindParam(':response', $responseFirmante, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                    if ($resultFirmante->execute()) {
                        $responseFirmante != 'EXITO' && $arrErrores["Desembolso" . $key] = $responseFirmante;
                    }
                }
            }

            if ($this->id > 0 && $this->incluyeParciales) {
                foreach ($this->datosCredito["detalleDesembolsos"] as $key => $desembolso) {
                    $idsDesembolsos[$key] = $desembolso["id"];
                    if (isset($desembolso["logs"])) {
                        $responseDesembolso = null;
                        if ($desembolso["logs"]["accion"] == 'Guardar') {
                            $idDesembolso = null;
                            $arrDetallesLogs = [];

                            if (isset($desembolso["logs"]["detalles"])) {
                                foreach ($desembolso["logs"]["detalles"] as $detalleLog) {
                                    $arrDetallesLogs[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                }
                            }

                            $fechaDesembolso = date('Y-m-d', strtotime($desembolso["fecha"]));
                            $detallesLogDesembolso = count($arrDetallesLogs) > 0 ? implode("@@@", $arrDetallesLogs) : null;

                            $queryDesembolso = 'EXEC sga_spGuardarDesembolso :idCredito,:id,:fecha,:monto,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idDesembolso;';
                            $resultDesembolso = $this->conexion->prepare($queryDesembolso, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultDesembolso->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':id', $desembolso["id"], PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':fecha', $fechaDesembolso, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':monto', $desembolso["monto"], PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':descripcionLog', $desembolso["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':detallesLog', $detallesLogDesembolso, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':response', $responseDesembolso, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                            $resultDesembolso->bindParam(':idDesembolso', $idDesembolso, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultDesembolso->execute()) {
                                $responseDesembolso != 'EXITO' && $arrErrores["Desembolso" . $key] = $responseDesembolso;
                                $idsDesembolsos[$key] = $idDesembolso;
                            }
                        } elseif ($desembolso["logs"]["accion"] == 'Eliminar') {
                            $queryDesembolso = 'EXEC sga_spEliminarDesembolso :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                            $resultDesembolso = $this->conexion->prepare($queryDesembolso, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultDesembolso->bindParam(':id', $desembolso["id"], PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':descripcion', $desembolso["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':response', $responseDesembolso, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultDesembolso->execute()) {
                                $responseDesembolso != 'EXITO' && $arrErrores["Desembolso" . $key] = $responseDesembolso;
                                $idsDesembolsos[$key] = $desembolso["id"];
                            }
                        }
                    }
                }
            } else {
                foreach ($this->datosCredito["detalleDesembolsos"] as $key => $desembolso) {
                    if (isset($desembolso["logs"])) {
                        $responseDesembolso = null;
                        if ($desembolso["logs"]["accion"] == 'Eliminar') {
                            $queryDesembolso = 'EXEC sga_spEliminarDesembolso :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                            $resultDesembolso = $this->conexion->prepare($queryDesembolso, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultDesembolso->bindParam(':id', $desembolso["id"], PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':descripcion', $desembolso["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':response', $responseDesembolso, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultDesembolso->execute()) {
                                $responseDesembolso != 'EXITO' && $arrErrores["Desembolso" . $key] = $responseDesembolso;
                            }
                        }
                    }
                }
            }

            if ($this->id > 0) {
                // var_dump($this->garantias);
                foreach ($this->garantias as $key => $garantia) {
                    $tmp = new stdClass();

                    $tmp->{"id"} = $garantia["id"];
                    $tmp->{"configuracion"} = [];

                    if (isset($garantia["logs"])) {
                        $responseGarantia = null;
                        $idGarantia = null;
                        if ($garantia["logs"]["accion"] == 'Guardar') {
                            $queryGarantia = 'EXEC sga_spGuardarGarantia :idCredito,:id,:tipoGarantia,:usuario,:ip,:descripcionLog,:tipoApartado,:idApartado,:response,:idGarantia;';
                            $resultGarantia = $this->conexion->prepare($queryGarantia, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultGarantia->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                            $resultGarantia->bindParam(':id', $garantia["id"], PDO::PARAM_INT);
                            $resultGarantia->bindParam(':tipoGarantia', $garantia["idTipoGarantia"], PDO::PARAM_INT);
                            $resultGarantia->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultGarantia->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultGarantia->bindParam(':descripcionLog', $garantia["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultGarantia->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultGarantia->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultGarantia->bindParam(':response', $responseGarantia, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                            $resultGarantia->bindParam(':idGarantia', $idGarantia, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultGarantia->execute()) {
                                $responseGarantia != 'EXITO' && $arrErrores["Garantia" . $key] = $responseGarantia;
                                $tmp->{"id"} = $idGarantia;
                            }
                        } elseif ($garantia["logs"]["accion"] == 'Eliminar') {
                            $queryGarantia = 'EXEC sga_spEliminarGarantia :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                            $resultGarantia = $this->conexion->prepare($queryGarantia, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultGarantia->bindParam(':id', $garantia["id"], PDO::PARAM_INT);
                            $resultGarantia->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultGarantia->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultGarantia->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultGarantia->bindParam(':descripcion', $garantia["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultGarantia->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultGarantia->bindParam(':response', $responseGarantia, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultGarantia->execute()) {
                                $responseGarantia != 'EXITO' && $arrErrores["Garantia" . $key] = $responseGarantia;
                            }
                        }
                    }

                    if ($tmp->{"id"} > 0) {
                        if (count($garantia["configuracion"]) > 0) {
                            switch ($garantia["idTipoGarantia"]) {
                                case "2":
                                    $tmp->{"configuracion"}["id"] = $garantia["configuracion"]["id"];
                                    if (isset($garantia["configuracion"]["logs"])) {
                                        if ($garantia["configuracion"]["logs"]["accion"] == 'Guardar') {
                                            $arrDetallesLogsConf = [];
                                            $responseConfiguracion = null;
                                            $idConfiguracion = null;

                                            if (isset($garantia["configuracion"]["logs"]["detalles"])) {
                                                foreach ($garantia["configuracion"]["logs"]["detalles"] as $detalleLog) {
                                                    $arrDetallesLogsConf[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                                }
                                            }

                                            $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;
                                            $queryConfiguracion = 'EXEC sga_spGuardarAportaciones :idCredito,:id,:cuenta,:porcentaje,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idAportaciones;';
                                            $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                            $resultConfiguracion->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':id', $garantia["configuracion"]["id"], PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':cuenta', $garantia["configuracion"]["cuenta"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':porcentaje', $garantia["configuracion"]["porcentaje"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':descripcionLog', $garantia["configuracion"]["logs"]["descripcion"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                                            $resultConfiguracion->bindParam(':idAportaciones', $idConfiguracion, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                                            if ($resultConfiguracion->execute()) {
                                                $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . "C"] = $responseConfiguracion;
                                                $tmp->{"configuracion"}["id"] = $idConfiguracion;
                                            }
                                        }
                                    }
                                    break;
                                case "3":
                                    $tmp->{"configuracion"}["id"] = $garantia["configuracion"]["id"];
                                    if (isset($garantia["configuracion"]["logs"])) {
                                        if ($garantia["configuracion"]["logs"]["accion"] == 'Guardar') {
                                            $arrDetallesLogsConf = [];
                                            $responseConfiguracion = null;
                                            $idConfiguracion = null;

                                            $fechaCreacion = date('Y-m-d', strtotime($garantia["configuracion"]["fechaCreacion"]));
                                            $fechaVencimiento = date('Y-m-d', strtotime($garantia["configuracion"]["fechaVencimiento"]));

                                            if (isset($garantia["configuracion"]["logs"]["detalles"])) {
                                                foreach ($garantia["configuracion"]["logs"]["detalles"] as $detalleLog) {
                                                    $arrDetallesLogsConf[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                                }
                                            }

                                            $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;

                                            $queryConfiguracion = 'EXEC sga_spGuardarCDP :idCredito,:id,:cuenta,:monto,:porcentaje,:certificado,:fechaCreacion,:fechaVencimiento,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idCDP;';
                                            $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                            $resultConfiguracion->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':id', $garantia["configuracion"]["id"], PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':cuenta', $garantia["configuracion"]["cuenta"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':monto', $garantia["configuracion"]["monto"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':porcentaje', $garantia["configuracion"]["porcentaje"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':certificado', $garantia["configuracion"]["certificado"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':fechaCreacion', $fechaCreacion, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':fechaVencimiento', $fechaVencimiento, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':descripcionLog', $garantia["configuracion"]["logs"]["descripcion"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                                            $resultConfiguracion->bindParam(':idCDP', $idConfiguracion, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                                            if ($resultConfiguracion->execute()) {
                                                $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . "C"] = $responseConfiguracion;
                                                $tmp->{"configuracion"}["id"] = $idConfiguracion;
                                            }
                                        }
                                    }
                                    break;
                                case "5":
                                    foreach ($garantia["configuracion"] as $key2 => $fiador) {
                                        $tmp->{"configuracion"}[$key2]["id"] = $fiador["id"];
                                        if (isset($fiador["logs"])) {
                                            $responseConfiguracion = null;
                                            if ($fiador["logs"]["accion"] == 'Guardar') {
                                                $arrDetallesLogsConf = [];
                                                $idConfiguracion = null;

                                                $fechaNacimiento = date('Y-m-d', strtotime($fiador["fechaNacimiento"]));

                                                if (isset($fiador["logs"]["detalles"])) {
                                                    foreach ($fiador["logs"]["detalles"] as $detalleLog) {
                                                        $arrDetallesLogsConf[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                                    }
                                                }

                                                $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;
                                                $queryConfiguracion = 'EXEC sga_spGuardarFiador :idCredito,:id,:tipoDocumento,:numeroDocumento,:nit,:nombres,:apellidos,:conocido,:genero,:pais,:departamento,:municipio,:profesion,:fechaNacimiento,:edad,:firma,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idFiador;';
                                                $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                $resultConfiguracion->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':id', $fiador["id"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':tipoDocumento', $fiador["tipoDocumento"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':numeroDocumento', $fiador["numeroDocumento"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':nit', $fiador["nit"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':nombres', $fiador["nombres"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':apellidos', $fiador["apellidos"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':conocido', $fiador["conocido"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':genero', $fiador["genero"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':pais', $fiador["pais"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':departamento', $fiador["departamento"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':municipio', $fiador["municipio"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':profesion', $fiador["profesion"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':fechaNacimiento', $fechaNacimiento, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':edad', $fiador["edad"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':firma', $fiador["firma"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':descripcionLog', $fiador["logs"]["descripcion"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                                                $resultConfiguracion->bindParam(':idFiador', $idConfiguracion, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                if ($resultConfiguracion->execute()) {
                                                    $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . "C" . $key2] = $responseConfiguracion;
                                                    $tmp->{"configuracion"}[$key2]["id"] = $idConfiguracion;
                                                }
                                            } elseif ($fiador["logs"]["accion"] == 'Eliminar') {
                                                $queryConfiguracion = 'EXEC sga_spEliminarFiador :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                                                $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                $resultConfiguracion->bindParam(':id', $fiador["id"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':descripcion', $fiador["logs"]["descripcion"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                if ($resultConfiguracion->execute()) {
                                                    $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . 'C' . $key2] = $responseConfiguracion;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case "6":
                                    foreach ($garantia["configuracion"] as $key2 => $hipoteca) {
                                        $idHipoteca = $hipoteca["id"];
                                        $tmpHipoteca = new stdClass();
                                        $tmp->{"configuracion"}[$key2]["id"] = $hipoteca["id"];
                                        if (isset($hipoteca["logs"])) {
                                            $responseConfiguracion = null;
                                            if ($hipoteca["logs"]["accion"] == 'Guardar') {
                                                $arrDetallesLogsConf = [];

                                                $fechaEmision = date('Y-m-d H:i', strtotime($hipoteca["fechaEmision"]));

                                                if (isset($hipoteca["logs"]["detalles"])) {
                                                    foreach ($hipoteca["logs"]["detalles"] as $detalleLog) {
                                                        $arrDetallesLogsConf[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                                    }
                                                }

                                                $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;
                                                $queryHipoteca = 'EXEC sga_spGuardarHipoteca :idCredito,:id,:fechaEmision,:abogado,:genero,:asiento,:monto,:plazo,:zona,:seccion,:presentada,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idHipoteca;';
                                                $resultHipoteca = $this->conexion->prepare($queryHipoteca, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                $resultHipoteca->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':id', $hipoteca["id"], PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':fechaEmision', $fechaEmision, PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':abogado', $hipoteca["abogado"], PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':genero', $hipoteca["generoAbogado"], PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':asiento', $hipoteca["asiento"], PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':monto', $hipoteca["monto"], PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':plazo', $hipoteca["plazo"], PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':zona', $hipoteca["zona"], PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':seccion', $hipoteca["seccion"], PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':presentada', $hipoteca["presenta"], PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':descripcionLog', $hipoteca["logs"]["descripcion"], PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                                                $resultHipoteca->bindParam(':idHipoteca', $idHipoteca, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                if ($resultHipoteca->execute()) {
                                                    $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . "C" . $key2 . "spGuardarHipoteca"] = $responseConfiguracion;
                                                    $tmp->{"configuracion"}[$key2]["id"] = $idHipoteca;
                                                }
                                            } elseif ($hipoteca["logs"]["accion"] == 'Eliminar') {
                                                $queryConfiguracion = 'EXEC sga_spEliminarHipoteca :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                                                $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                $resultConfiguracion->bindParam(':id', $hipoteca["id"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':descripcion', $hipoteca["logs"]["descripcion"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                if ($resultConfiguracion->execute()) {
                                                    $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . 'C' . $key2] = $responseConfiguracion;
                                                }
                                            }
                                        }

                                        foreach ($hipoteca["matriculas"] as $key3 => $matricula) {
                                            $tmp->{"configuracion"}[$key2]["matriculas"][$key3]["id"] = $matricula["id"];
                                            if (isset($matricula["logs"])) {
                                                $responseConfiguracion = null;
                                                if ($matricula["logs"]["accion"] == 'Guardar') {
                                                    $arrDetallesLogsConf = [];
                                                    $idMatricula = null;

                                                    if (isset($matricula["logs"]["detalles"])) {
                                                        $arrDetallesLogsConf[] = $matricula["logs"]["detalles"]["campo"] . '%%%' . $matricula["logs"]["detalles"]["valorAnterior"] . '%%%' . $matricula["logs"]["detalles"]["nuevoValor"];
                                                    }

                                                    $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;
                                                    $queryMatricula = 'EXEC sga_spGuardarMatriculaHipoteca :idHipoteca,:id,:matricula,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idMatricula;';
                                                    $resultMatricula = $this->conexion->prepare($queryMatricula, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                    $resultMatricula->bindParam(':idHipoteca', $idHipoteca, PDO::PARAM_INT);
                                                    $resultMatricula->bindParam(':id', $matricula["id"], PDO::PARAM_INT);
                                                    $resultMatricula->bindParam(':matricula', $matricula["matricula"], PDO::PARAM_STR);
                                                    $resultMatricula->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                    $resultMatricula->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                    $resultMatricula->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                    $resultMatricula->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                    $resultMatricula->bindParam(':descripcionLog', $matricula["logs"]["descripcion"], PDO::PARAM_STR);
                                                    $resultMatricula->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                                                    $resultMatricula->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                                                    $resultMatricula->bindParam(':idMatricula', $idMatricula, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                    if ($resultMatricula->execute()) {
                                                        $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . "C" . $key2] = $responseConfiguracion;
                                                        $tmp->{"configuracion"}[$key2]["matriculas"][$key3]["id"] = $idMatricula;
                                                    }
                                                } elseif ($matricula["logs"]["accion"] == 'Eliminar') {
                                                    $queryConfiguracion = 'EXEC sga_spEliminarMatricula :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                                                    $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                    $resultConfiguracion->bindParam(':id', $matricula["id"], PDO::PARAM_INT);
                                                    $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                    $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                    $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                    $resultConfiguracion->bindParam(':descripcion', $matricula["logs"]["descripcion"], PDO::PARAM_STR);
                                                    $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                    $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                    if ($resultConfiguracion->execute()) {
                                                        $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . 'C' . $key2] = $responseConfiguracion;
                                                    }
                                                }
                                            }
                                        }

                                        foreach ($hipoteca["modificaciones"] as $key3 => $modificacion) {
                                            $tmp->{"configuracion"}[$key2]["modificaciones"][$key3]["id"] = $modificacion["id"];
                                            if (isset($modificacion["logs"])) {
                                                $responseConfiguracion = null;
                                                if ($modificacion["logs"]["accion"] == 'Guardar') {
                                                    $arrDetallesLogsConf = [];
                                                    $idModificacion = null;

                                                    $fechaModificacion = date('Y-m-d H:i', strtotime($modificacion["fecha"]));

                                                    if (isset($modificacion["logs"]["detalles"])) {
                                                        foreach ($modificacion["logs"]["detalles"] as $detalleLog) {
                                                            $arrDetallesLogsConf[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                                        }
                                                    }

                                                    $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;
                                                    $queryModificacion = 'EXEC sga_spGuardarModificacionHipoteca :idHipoteca,:id,:fecha,:abogado,:generoAbogado,:asiento,:monto,:plazo,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idModificacion;';
                                                    $resultModificacion = $this->conexion->prepare($queryModificacion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                    $resultModificacion->bindParam(':idHipoteca', $idHipoteca, PDO::PARAM_INT);
                                                    $resultModificacion->bindParam(':id', $modificacion["id"], PDO::PARAM_INT);
                                                    $resultModificacion->bindParam(':fecha', $fechaModificacion, PDO::PARAM_STR);
                                                    $resultModificacion->bindParam(':abogado', $modificacion["abogado"], PDO::PARAM_STR);
                                                    $resultModificacion->bindParam(':generoAbogado', $modificacion["generoAbogado"], PDO::PARAM_STR);
                                                    $resultModificacion->bindParam(':asiento', $modificacion["asiento"], PDO::PARAM_INT);
                                                    $resultModificacion->bindParam(':monto', $modificacion["monto"], PDO::PARAM_STR);
                                                    $resultModificacion->bindParam(':plazo', $modificacion["plazo"], PDO::PARAM_INT);
                                                    $resultModificacion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                    $resultModificacion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                    $resultModificacion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                    $resultModificacion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                    $resultModificacion->bindParam(':descripcionLog', $modificacion["logs"]["descripcion"], PDO::PARAM_STR);
                                                    $resultModificacion->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                                                    $resultModificacion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                                                    $resultModificacion->bindParam(':idModificacion', $idModificacion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                    if ($resultModificacion->execute()) {
                                                        $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . "C" . $key2] = $responseConfiguracion;
                                                        $tmp->{"configuracion"}[$key2]["modificaciones"][$key3]["id"] = $idModificacion;
                                                    }
                                                } elseif ($modificacion["logs"]["accion"] == 'Eliminar') {
                                                    $queryConfiguracion = 'EXEC sga_spEliminarModificacionHipoteca :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                                                    $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                    $resultConfiguracion->bindParam(':id', $modificacion["id"], PDO::PARAM_INT);
                                                    $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                    $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                    $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                    $resultConfiguracion->bindParam(':descripcion', $modificacion["logs"]["descripcion"], PDO::PARAM_STR);
                                                    $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                    $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                    if ($resultConfiguracion->execute()) {
                                                        $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . 'C' . $key2] = $responseConfiguracion;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }

                    $idsGarantias[$key] = $tmp;
                }
            }
        }

        $respuesta = 'EXITO';
        (!empty($responseCredito) && $responseCredito != 'EXITO') && $respuesta = $responseCredito;

        $nombreArchivo = null;

        if ($respuesta == 'EXITO') {
            $nombreArchivo = 'M' . $this->datosCredito["numeroCredito"] . '.pdf';
            $datosCredito = $this->datosCredito;
            $datosDeudor = $this->datosDeudor;
            $datosFirmante = $this->datosFirmante;
            $garantias = $this->garantias;
            $incluyeFirmante = $this->incluyeFirmante;
            $incluyeParciales = $this->incluyeParciales;
            require_once __DIR__ . '/../sgaConstruirMutuo.php';
        }

        return ["respuesta" => $respuesta, "response" => $responseCredito, "id" => $this->id, "Errores" => $arrErrores, "idsAditivos" => $idsAditivos, "idDeudor" => $idDeudor, "idFirmante" => $idFirmante, 'idsGarantias' => $idsGarantias, 'idsDesembolsos' => $idsDesembolsos, 'nombreArchivo' => $nombreArchivo];

        $this->conexion = null;
        return ["respuesta" => "Pendiente"];
    }

    public function getMutuosAdministracion(string $tipoRol, int $usuario, int $agencia, string $inicio, string $fin)
    {
        $condicion = $tipoRol == 'Administrativo' ? '' : ' AND idUsuario=' . $usuario;
        $query = "SELECT * FROM sga_viewMutuosAdministracion WHERE CAST(fechaGeneracion as date) BETWEEN '{$inicio}' and '{$fin}' AND agencia = " . $agencia . "" . $condicion . ";";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            $datos = $result->fetchAll(PDO::FETCH_OBJ);

            foreach ($datos as $key => $mutuo) {
                $datos[$key]->fechaGeneracion = date('d-m-Y h:i a', strtotime($mutuo->fechaGeneracion));
                $datos[$key]->fechaRegistro = date('d-m-Y h:i a', strtotime($mutuo->fechaRegistro));
                $garantias = explode('@@@', $mutuo->garantias);
                $datos[$key]->garantias = $garantias;
            }

            return ["respuesta" => "EXITO", "resultados" => $datos];
        }

        $this->conexion = null;
        return [];
    }

    public function eliminarMutuo($idUsuario, $tipoApartado, $idApartado)
    {
        $response = null;
        $query = 'EXEC sga_spEliminarMutuo :id,:usuario,:tipoApartado,:idApartado,:response;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return $response;
        }

        $this->conexion = null;
        return [];
    }

    public function getMutuo()
    {
        $queryCredito = 'SELECT top(1) * FROM sga_viewDatosCredito where id = :id;';
        $resultCredito = $this->conexion->prepare($queryCredito, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultCredito->bindParam(':id', $this->id, PDO::PARAM_INT);

        if ($resultCredito->execute() && $resultCredito->rowCount() > 0) {
            $datosCredito = $resultCredito->fetchAll(PDO::FETCH_OBJ)[0];
            $incluyeFirmante = false;
            !empty($datosCredito->fechaGeneracion) && $datosCredito->fechaGeneracion = date('d-m-Y H:i', strtotime($datosCredito->fechaGeneracion));
            !empty($datosCredito->fechaVencimiento) && $datosCredito->fechaVencimiento = date('d-m-Y H:i', strtotime($datosCredito->fechaVencimiento));
            $datosCredito->capitalInteres = number_format($datosCredito->capitalInteres, 2, '.', '');

            $queryAditivos = 'SELECT * FROM sga_viewAditivosCredito where idCredito = :id;';
            $resultAditivos = $this->conexion->prepare($queryAditivos, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultAditivos->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultAditivos->execute();

            $datosCredito->aditivos = $resultAditivos->rowCount() > 0 ? $resultAditivos->fetchAll(PDO::FETCH_OBJ) : [];
            foreach ($datosCredito->aditivos as $key => $aditivo) {
                $datosCredito->aditivos[$key]->monto = number_format($aditivo->monto, 2, '.', '');
            }

            $queryDeudor = 'SELECT top(1) * FROM sga_viewDatosDeudores where idCredito = :id;';
            $resultDeudor = $this->conexion->prepare($queryDeudor, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultDeudor->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultDeudor->execute();

            $datosDeudor = $resultDeudor->rowCount() > 0 ? $resultDeudor->fetchAll(PDO::FETCH_OBJ)[0] : new stdClass();

            if (isset($datosDeudor->fechaNacimiento) && !empty($datosDeudor->fechaNacimiento)) {
                $datosDeudor->fechaNacimiento = date('d-m-Y', strtotime($datosDeudor->fechaNacimiento));
            }

            isset($datosDeudor->sabeFirmar) && ($datosDeudor->sabeFirmar == 'N' && $incluyeFirmante = true);

            if ($datosCredito->desembolsoParcial == 'S') {
                $queryParciales = 'SELECT * FROM sga_viewDesembolsosCredito where idCredito = :id;';
                $resultParciales = $this->conexion->prepare($queryParciales, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultParciales->bindParam(':id', $this->id, PDO::PARAM_INT);
                $resultParciales->execute();
                $datosParciales = $resultParciales->rowCount() > 0 ? $resultParciales->fetchAll(PDO::FETCH_OBJ) : [];

                foreach ($datosParciales as $key => $parcial) {
                    $datosParciales[$key]->fecha = date('d-m-Y', strtotime($parcial->fecha));
                }

                $datosCredito->parciales = $datosParciales;
            }

            $queryGarantias = 'SELECT * FROM sga_viewGarantiasCredito where idCredito = :id;';
            $resultGarantias = $this->conexion->prepare($queryGarantias, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultGarantias->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultGarantias->execute();

            $datosGarantias = $resultGarantias->rowCount() > 0 ? $resultGarantias->fetchAll(PDO::FETCH_OBJ) : [];

            foreach ($datosGarantias as $key => $tipoGarantia) {
                switch ($tipoGarantia->idTipoGarantia) {
                    case '2':
                        $queryAportaciones = 'SELECT top(1) * FROM sga_viewAportacionesCredito where idCredito = :id;';
                        $resultAportaciones = $this->conexion->prepare($queryAportaciones, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $resultAportaciones->bindParam(':id', $this->id, PDO::PARAM_INT);
                        $resultAportaciones->execute();

                        $datosGarantias[$key]->configuracion = $resultAportaciones->rowCount() > 0 ? $resultAportaciones->fetchAll(PDO::FETCH_OBJ)[0] : new stdClass();
                        break;
                    case '3':
                        $queryCDP = 'SELECT TOP(1) * FROM sga_viewCDPCredito where idCredito = :id;';
                        $resultCDP = $this->conexion->prepare($queryCDP, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $resultCDP->bindParam(':id', $this->id, PDO::PARAM_INT);
                        $resultCDP->execute();

                        $datosGarantias[$key]->configuracion = $resultCDP->rowCount() > 0 ? $resultCDP->fetchAll(PDO::FETCH_OBJ)[0] : new stdClass();
                        $datosGarantias[$key]->configuracion->fechaCreacion = date('d-m-Y', strtotime($datosGarantias[$key]->configuracion->fechaCreacion));
                        $datosGarantias[$key]->configuracion->fechaVencimiento = date('d-m-Y', strtotime($datosGarantias[$key]->configuracion->fechaVencimiento));
                        $datosGarantias[$key]->configuracion->porcentaje = number_format($datosGarantias[$key]->configuracion->porcentaje, 2, '.', '');
                        $datosGarantias[$key]->configuracion->monto = number_format($datosGarantias[$key]->configuracion->monto, 2, '.', '');
                        break;
                    case '5':
                        $queryFiadores = 'SELECT * FROM sga_viewDatosFiadores where idCredito = :id;';
                        $resultFiadores = $this->conexion->prepare($queryFiadores, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $resultFiadores->bindParam(':id', $this->id, PDO::PARAM_INT);
                        $resultFiadores->execute();

                        $datosFiadores = $resultFiadores->fetchAll(PDO::FETCH_OBJ);

                        foreach ($datosFiadores as $key2 => $fiador) {
                            $datosFiadores[$key2]->fechaNacimiento = date('d-m-Y', strtotime($fiador->fechaNacimiento));
                            $fiador->sabeFirmar == 'N' && $incluyeFirmante = true;
                        }

                        $datosGarantias[$key]->configuracion = $datosFiadores;
                        break;
                    case '6':
                        $queryHipotecas = 'SELECT * FROM sga_viewDatosHipotecas where idCredito = :id;';
                        $resultHipotecas = $this->conexion->prepare($queryHipotecas, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $resultHipotecas->bindParam(':id', $this->id, PDO::PARAM_INT);
                        $resultHipotecas->execute();

                        $datosHipotecas = $resultHipotecas->fetchAll(PDO::FETCH_OBJ);

                        foreach ($datosHipotecas as $key2 => $hipoteca) {
                            $matriculas = [];
                            $arrMatriculas = explode('@@@', $hipoteca->matriculas);

                            foreach ($arrMatriculas as $detalles) {
                                $valores = explode('%%%', $detalles);
                                $matricula = new stdClass();
                                $matricula->id = $valores[0];
                                $matricula->matricula = $valores[1];

                                $matriculas[] = $matricula;
                            }

                            $datosHipotecas[$key2]->matriculas = $matriculas;
                            $datosHipotecas[$key2]->fechaEmision = date('d-m-Y H:i', strtotime($hipoteca->fechaEmision));

                            $queryModificaciones = 'SELECT * FROM sga_viewModificacionesHipoteca where idHipoteca = :id;';
                            $resultModificaciones = $this->conexion->prepare($queryModificaciones, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultModificaciones->bindParam(':id', $hipoteca->id, PDO::PARAM_INT);
                            $resultModificaciones->execute();

                            $modificaciones = $resultModificaciones->fetchAll(PDO::FETCH_OBJ);

                            foreach ($modificaciones as $key3 => $modificacion) {
                                $modificaciones[$key3]->fecha = date('d-m-Y H:i', strtotime($modificacion->fecha));
                                $modificacion->plazo == 0 && $modificaciones[$key3]->plazo = '';
                                $modificacion->monto == 0 && $modificaciones[$key3]->monto = '';
                            }

                            $datosHipotecas[$key2]->modificaciones = $modificaciones;
                        }

                        $datosGarantias[$key]->configuracion = $datosHipotecas;
                        break;
                }
            }

            $datosFirmante = new stdClass();
            if ($incluyeFirmante) {
                $queryFirmante = 'SELECT * FROM sga_viewDatosFirmantes where idCredito = :id;';
                $resultFirmante = $this->conexion->prepare($queryFirmante, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultFirmante->bindParam(':id', $this->id, PDO::PARAM_INT);
                $resultFirmante->execute();

                $datosFirmante = $resultFirmante->rowCount() > 0 ? $resultFirmante->fetchAll(PDO::FETCH_OBJ)[0] : new stdClass();
                isset($datosFirmante->fechaNacimiento) && $datosFirmante->fechaNacimiento = date('d-m-Y', strtotime($datosFirmante->fechaNacimiento));
            }

            $this->conexion = null;
            return ['respuesta' => 'EXITO', 'datosCredito' => $datosCredito, 'datosDeudor' => $datosDeudor, 'datosGarantias' => $datosGarantias, 'datosFirmante' => $datosFirmante];
        }

        $this->conexion = null;
        return ['respuesta' => 'SinRegistros'];
    }
}
