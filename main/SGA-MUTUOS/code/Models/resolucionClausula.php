<?php
class resolucionClausula
{
    public $id;
    public $numeroCredito;
    public $datosCredito;
    public $datosDeudor;
    public $garantias;
    public $incluyeParciales;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function searchResolucion()
    {
        $query = "SELECT id from sga_viewRCAdministracion WHERE numeroCredito = :numeroCredito;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':numeroCredito', $this->numeroCredito);
        $result->execute();

        $this->conexion = null;
        return $result->fetchAll(PDO::FETCH_OBJ);
    }

    public function saveResolucionClausula($idUsuario, $ipActual, $tipoApartado, $idApartado)
    {
        $arrErrores = [];
        $idsAditivos = [];
        $idsGarantias = [];
        $idsDesembolsos = [];
        $responseCredito = null;
        $id = null;
        $idDeudor = null;
        $permitirGuardado = true;

        if (isset($this->datosCredito["logs"])) {
            $queryCredito = "EXEC sga_spGuardarCreditoRC :id,:idMutuo,:agencia,:numeroCredito,:monto,:tasaInteres,:lineaFinanciera,:destino,:vencimiento,:periodicidad,:plazo,:tipoPlazo,:fechaGeneracion,:fuenteFondos,:desembolsoParcial,:fechaAprobacion,:comite,:notario,:capitalInteres,:descripcionLog,:detallesLog,:tipoApartado,:idApartado,:usuario,:ip,:response,:idCredito;";
            $fechaGeneracion = date('Y-m-d H:i', strtotime($this->datosCredito["fechaGeneracion"]));
            $fechaAprobacion = date('Y-m-d H:i', strtotime($this->datosCredito["fechaAprobacion"]));

            $arrDetallesLogs = [];


            if (isset($this->datosCredito["logs"]["detalles"])) {
                foreach ($this->datosCredito["logs"]["detalles"] as $detalleLog) {
                    $arrDetallesLogs[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                }
            }
            $detallesLog = count($arrDetallesLogs) > 0 ? implode('@@@', $arrDetallesLogs) : null;
            $idMutuo = !empty($this->datosCredito['idMutuo']) ? $this->datosCredito['idMutuo'] : null;

            $resultCredito = $this->conexion->prepare($queryCredito, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultCredito->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultCredito->bindParam(':idMutuo', $idMutuo, PDO::PARAM_INT);
            $resultCredito->bindParam(':agencia', $this->datosCredito["agencia"], PDO::PARAM_INT);
            $resultCredito->bindParam(':numeroCredito', $this->datosCredito["numeroCredito"], PDO::PARAM_STR);
            $resultCredito->bindParam(':monto', $this->datosCredito["monto"], PDO::PARAM_STR);
            $resultCredito->bindParam(':tasaInteres', $this->datosCredito["tasaInteres"], PDO::PARAM_STR);
            $resultCredito->bindParam(':lineaFinanciera', $this->datosCredito["lineaFinanciera"], PDO::PARAM_INT);
            $resultCredito->bindParam(':destino', $this->datosCredito["destino"], PDO::PARAM_STR);
            $resultCredito->bindParam(':vencimiento', $this->datosCredito["vencimiento"], PDO::PARAM_STR);
            $resultCredito->bindParam(':periodicidad', $this->datosCredito["periodicidad"], PDO::PARAM_INT);
            $resultCredito->bindParam(':plazo', $this->datosCredito["plazo"], PDO::PARAM_INT);
            $resultCredito->bindParam(':tipoPlazo', $this->datosCredito["tipoPlazo"], PDO::PARAM_INT);
            $resultCredito->bindParam(':fechaGeneracion', $fechaGeneracion, PDO::PARAM_STR);
            $resultCredito->bindParam(':fuenteFondos', $this->datosCredito["fuenteFondos"], PDO::PARAM_INT);
            $resultCredito->bindParam(':desembolsoParcial', $this->datosCredito["desembolsoParcial"], PDO::PARAM_STR);
            $resultCredito->bindParam(':fechaAprobacion', $fechaAprobacion, PDO::PARAM_STR);
            $resultCredito->bindParam(':comite', $this->datosCredito["comite"], PDO::PARAM_INT);
            $resultCredito->bindParam(':notario', $this->datosCredito["notario"], PDO::PARAM_STR);
            $resultCredito->bindParam(':capitalInteres', $this->datosCredito["capitalInteres"], PDO::PARAM_STR);
            $resultCredito->bindParam(':descripcionLog', $this->datosCredito["logs"]["descripcion"], PDO::PARAM_STR);
            $resultCredito->bindParam(':detallesLog', $detallesLog, PDO::PARAM_STR);
            $resultCredito->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
            $resultCredito->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
            $resultCredito->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
            $resultCredito->bindParam(':ip', $ipActual, PDO::PARAM_STR);
            $resultCredito->bindParam(':response', $responseCredito, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
            $resultCredito->bindParam(':idCredito', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

            if ($resultCredito->execute()) {
                $responseCredito != 'EXITO' && $arrErrores["Crédito"] = $responseCredito;
                $responseCredito != 'EXITO' && $permitirGuardado = false;
                $this->id = $id;
            }
        }

        if ($permitirGuardado) {
            if ($this->id > 0 && count($this->datosCredito["aditivos"]) > 0) {
                foreach ($this->datosCredito["aditivos"] as $key => $aditivo) {
                    if (isset($aditivo["logs"])) {
                        $responseAditivo = null;
                        $idAditivo = null;
                        $arrDetallesLogs = [];
                        if (isset($aditivo["logs"]["detalles"])) {
                            $arrDetallesLogs[] = $aditivo["logs"]["detalles"]["campo"] . '%%%' . $aditivo["logs"]["detalles"]["valorAnterior"] . '%%%' . $aditivo["logs"]["detalles"]["nuevoValor"];
                        }
                        $detallesLogAditivo = count($arrDetallesLogs) > 0 ? implode("@@@", $arrDetallesLogs) : null;
                        if ($aditivo["logs"]["accion"] == 'Guardar') {
                            $queryAditivo = "EXEC sga_spGuardarAditivoRC :idCredito,:id,:idAditivo,:monto,:tipoApartado,:idApartado,:usuario,:ip,:descripcionLog,:detallesLog,:response,:idDb;";
                            $resultAditivo = $this->conexion->prepare($queryAditivo, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultAditivo->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                            $resultAditivo->bindParam(':id', $aditivo["id"], PDO::PARAM_INT);
                            $resultAditivo->bindParam(':idAditivo', $aditivo["idAditivo"], PDO::PARAM_INT);
                            $resultAditivo->bindParam(':monto', $aditivo["monto"], PDO::PARAM_STR);
                            $resultAditivo->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultAditivo->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultAditivo->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultAditivo->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultAditivo->bindParam(':descripcionLog', $aditivo["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultAditivo->bindParam(':detallesLog', $detallesLogAditivo, PDO::PARAM_STR);
                            $resultAditivo->bindParam(':response', $responseAditivo, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                            $resultAditivo->bindParam(':idDb', $idAditivo, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultAditivo->execute()) {
                                $responseAditivo != 'EXITO' && $arrErrores["Aditivo" . $key] = $responseAditivo;
                                $idsAditivos[$key] = $idAditivo;
                            }
                        } elseif ($aditivo["logs"]["accion"] == 'Eliminar') {
                            $queryAditivo = 'EXEC sga_spEliminarAditivoRC :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                            $resultAditivo = $this->conexion->prepare($queryAditivo, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultAditivo->bindParam(':id', $aditivo["id"], PDO::PARAM_INT);
                            $resultAditivo->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultAditivo->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultAditivo->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultAditivo->bindParam(':descripcion', $aditivo["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultAditivo->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultAditivo->bindParam(':response', $responseAditivo, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultAditivo->execute()) {
                                $responseAditivo != 'EXITO' && $arrErrores["Aditivo" . $key] = $responseAditivo;
                                $idsAditivos[$key] = $aditivo["id"];
                            }
                        }
                    } else {
                        $idsAditivos[$key] = $aditivo["id"];
                    }
                }
            }

            if ($this->id > 0 && isset($this->datosDeudor["logs"])) {
                $responseDeudor = null;
                $arrDetallesLogs = [];

                if (isset($this->datosDeudor["logs"]["detalles"])) {
                    foreach ($this->datosDeudor["logs"]["detalles"] as $detalleLog) {
                        $arrDetallesLogs[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                    }
                }
                $detallesLogDeudor = count($arrDetallesLogs) > 0 ? implode("@@@", $arrDetallesLogs) : null;
                $queryDeudor = 'EXEC sga_spGuardarDeudorRC :idCredito,:id,:codigoCliente,:tipoDocumento,:numeroDocumento,:nit,:nombres,:apellidos,:conocido,:genero,:pais,:departamento,:municipio,:direccion,:usuario,:ip,:descripcionLog,:detallesLog,:tipoApartado,:idApartado,:response,:idDeudor;';
                $resultDeudor = $this->conexion->prepare($queryDeudor, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultDeudor->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                $resultDeudor->bindParam(':id', $this->datosDeudor["id"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':codigoCliente', $this->datosDeudor["codigoCliente"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':tipoDocumento', $this->datosDeudor["tipoDocumento"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':numeroDocumento', $this->datosDeudor["numeroDocumento"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':nit', $this->datosDeudor["nit"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':nombres', $this->datosDeudor["nombres"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':apellidos', $this->datosDeudor["apellidos"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':conocido', $this->datosDeudor["conocido"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':genero', $this->datosDeudor["genero"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':pais', $this->datosDeudor["pais"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':departamento', $this->datosDeudor["departamento"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':municipio', $this->datosDeudor["municipio"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':direccion', $this->datosDeudor["direccion"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                $resultDeudor->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                $resultDeudor->bindParam(':descripcionLog', $this->datosDeudor["logs"]["descripcion"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':detallesLog', $detallesLogDeudor, PDO::PARAM_STR);
                $resultDeudor->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                $resultDeudor->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                $resultDeudor->bindParam(':response', $responseDeudor, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                $resultDeudor->bindParam(':idDeudor', $idDeudor, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                if ($resultDeudor->execute()) {
                    $responseDeudor != 'EXITO' && $arrErrores["Deudor"] = $responseDeudor;
                }
            } else {
                $idDeudor = $this->datosDeudor["id"];
            }

            if ($this->id > 0 && $this->incluyeParciales) {
                foreach ($this->datosCredito["detalleDesembolsos"] as $key => $desembolso) {
                    $idsDesembolsos[$key] = $desembolso["id"];
                    if (isset($desembolso["logs"])) {
                        $responseDesembolso = null;
                        if ($desembolso["logs"]["accion"] == 'Guardar') {
                            $idDesembolso = null;
                            $arrDetallesLogs = [];

                            if (isset($desembolso["logs"]["detalles"])) {
                                foreach ($desembolso["logs"]["detalles"] as $detalleLog) {
                                    $arrDetallesLogs[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                }
                            }

                            $fechaDesembolso = date('Y-m-d', strtotime($desembolso["fecha"]));
                            $detallesLogDesembolso = count($arrDetallesLogs) > 0 ? implode("@@@", $arrDetallesLogs) : null;

                            $queryDesembolso = 'EXEC sga_spGuardarDesembolsoRC :idCredito,:id,:fecha,:monto,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idDesembolso;';
                            $resultDesembolso = $this->conexion->prepare($queryDesembolso, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultDesembolso->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':id', $desembolso["id"], PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':fecha', $fechaDesembolso, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':monto', $desembolso["monto"], PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':descripcionLog', $desembolso["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':detallesLog', $detallesLogDesembolso, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':response', $responseDesembolso, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                            $resultDesembolso->bindParam(':idDesembolso', $idDesembolso, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultDesembolso->execute()) {
                                $responseDesembolso != 'EXITO' && $arrErrores["Desembolso" . $key] = $responseDesembolso;
                                $idsDesembolsos[$key] = $idDesembolso;
                            }
                        } elseif ($desembolso["logs"]["accion"] == 'Eliminar') {
                            $queryDesembolso = 'EXEC sga_spEliminarDesembolsoRC :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                            $resultDesembolso = $this->conexion->prepare($queryDesembolso, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultDesembolso->bindParam(':id', $desembolso["id"], PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':descripcion', $desembolso["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':response', $responseDesembolso, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultDesembolso->execute()) {
                                $responseDesembolso != 'EXITO' && $arrErrores["Desembolso" . $key] = $responseDesembolso;
                                $idsDesembolsos[$key] = $desembolso["id"];
                            }
                        }
                    }
                }
            } else {
                foreach ($this->datosCredito["detalleDesembolsos"] as $key => $desembolso) {
                    if (isset($desembolso["logs"])) {
                        $responseDesembolso = null;
                        if ($desembolso["logs"]["accion"] == 'Eliminar') {
                            $queryDesembolso = 'EXEC sga_spEliminarDesembolsoRC :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                            $resultDesembolso = $this->conexion->prepare($queryDesembolso, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultDesembolso->bindParam(':id', $desembolso["id"], PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultDesembolso->bindParam(':descripcion', $desembolso["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultDesembolso->bindParam(':response', $responseDesembolso, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultDesembolso->execute()) {
                                $responseDesembolso != 'EXITO' && $arrErrores["Desembolso" . $key] = $responseDesembolso;
                            }
                        }
                    }
                }
            }

            if ($this->id > 0) {
                foreach ($this->garantias as $key => $garantia) {
                    $tmp = new stdClass();

                    $tmp->{"id"} = $garantia["id"];
                    $tmp->{"configuracion"} = [];

                    if (isset($garantia["logs"])) {
                        $responseGarantia = null;
                        $idGarantia = null;
                        if ($garantia["logs"]["accion"] == 'Guardar') {
                            $queryGarantia = 'EXEC sga_spGuardarGarantiaRC :idCredito,:id,:tipoGarantia,:usuario,:ip,:descripcionLog,:tipoApartado,:idApartado,:response,:idGarantia;';
                            $resultGarantia = $this->conexion->prepare($queryGarantia, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultGarantia->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                            $resultGarantia->bindParam(':id', $garantia["id"], PDO::PARAM_INT);
                            $resultGarantia->bindParam(':tipoGarantia', $garantia["idTipoGarantia"], PDO::PARAM_INT);
                            $resultGarantia->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultGarantia->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultGarantia->bindParam(':descripcionLog', $garantia["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultGarantia->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultGarantia->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultGarantia->bindParam(':response', $responseGarantia, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                            $resultGarantia->bindParam(':idGarantia', $idGarantia, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultGarantia->execute()) {
                                $responseGarantia != 'EXITO' && $arrErrores["Garantia" . $key] = $responseGarantia;
                                $tmp->{"id"} = $idGarantia;
                            }
                        } elseif ($garantia["logs"]["accion"] == 'Eliminar') {
                            $queryGarantia = 'EXEC sga_spEliminarGarantiaRC :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                            $resultGarantia = $this->conexion->prepare($queryGarantia, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultGarantia->bindParam(':id', $garantia["id"], PDO::PARAM_INT);
                            $resultGarantia->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultGarantia->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultGarantia->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultGarantia->bindParam(':descripcion', $garantia["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultGarantia->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultGarantia->bindParam(':response', $responseGarantia, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultGarantia->execute()) {
                                $responseGarantia != 'EXITO' && $arrErrores["Garantia" . $key] = $responseGarantia;
                            }
                        }
                    }

                    if ($tmp->{"id"} > 0) {
                        if (count($garantia["configuracion"]) > 0) {
                            switch ($garantia["idTipoGarantia"]) {
                                case "2":
                                    $tmp->{"configuracion"}["id"] = $garantia["configuracion"]["id"];
                                    if (isset($garantia["configuracion"]["logs"])) {
                                        if ($garantia["configuracion"]["logs"]["accion"] == 'Guardar') {
                                            $arrDetallesLogsConf = [];
                                            $responseConfiguracion = null;
                                            $idConfiguracion = null;

                                            if (isset($garantia["configuracion"]["logs"]["detalles"])) {
                                                foreach ($garantia["configuracion"]["logs"]["detalles"] as $detalleLog) {
                                                    $arrDetallesLogsConf[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                                }
                                            }

                                            $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;
                                            $queryConfiguracion = 'EXEC sga_spGuardarAportacionesRC :idCredito,:id,:descripcion,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idAportaciones;';
                                            $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                            $resultConfiguracion->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':id', $garantia["configuracion"]["id"], PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':descripcion', $garantia["configuracion"]["descripcion"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':descripcionLog', $garantia["configuracion"]["logs"]["descripcion"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                                            $resultConfiguracion->bindParam(':idAportaciones', $idConfiguracion, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                                            if ($resultConfiguracion->execute()) {
                                                $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . "C"] = $responseConfiguracion;
                                                $tmp->{"configuracion"}["id"] = $idConfiguracion;
                                            }
                                        }
                                    }
                                    break;
                                case "3":
                                    $tmp->{"configuracion"}["id"] = $garantia["configuracion"]["id"];
                                    if (isset($garantia["configuracion"]["logs"])) {
                                        if ($garantia["configuracion"]["logs"]["accion"] == 'Guardar') {
                                            $arrDetallesLogsConf = [];
                                            $responseConfiguracion = null;
                                            $idConfiguracion = null;

                                            if (isset($garantia["configuracion"]["logs"]["detalles"])) {
                                                foreach ($garantia["configuracion"]["logs"]["detalles"] as $detalleLog) {
                                                    $arrDetallesLogsConf[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                                }
                                            }

                                            $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;

                                            $queryConfiguracion = 'EXEC sga_spGuardarCDPRC :idCredito,:id,:descripcion,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idCDP;';
                                            $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                            $resultConfiguracion->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':id', $garantia["configuracion"]["id"], PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':descripcion', $garantia["configuracion"]["descripcion"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                            $resultConfiguracion->bindParam(':descripcionLog', $garantia["configuracion"]["logs"]["descripcion"], PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                                            $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                                            $resultConfiguracion->bindParam(':idCDP', $idConfiguracion, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                                            if ($resultConfiguracion->execute()) {
                                                $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . "C"] = $responseConfiguracion;
                                                $tmp->{"configuracion"}["id"] = $idConfiguracion;
                                            }
                                        }
                                    }
                                    break;
                                case "5":
                                    foreach ($garantia["configuracion"] as $key2 => $fiador) {
                                        $tmp->{"configuracion"}[$key2]["id"] = $fiador["id"];
                                        if (isset($fiador["logs"])) {
                                            $responseConfiguracion = null;
                                            if ($fiador["logs"]["accion"] == 'Guardar') {
                                                $arrDetallesLogsConf = [];
                                                $idConfiguracion = null;

                                                if (isset($fiador["logs"]["detalles"])) {
                                                    foreach ($fiador["logs"]["detalles"] as $detalleLog) {
                                                        $arrDetallesLogsConf[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                                    }
                                                }

                                                $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;
                                                $queryConfiguracion = 'EXEC sga_spGuardarFiadorRC :idCredito,:id,:descripcion,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idFiador;';
                                                $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                $resultConfiguracion->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':id', $fiador["id"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':descripcion', $fiador["descripcion"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':descripcionLog', $fiador["logs"]["descripcion"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                                                $resultConfiguracion->bindParam(':idFiador', $idConfiguracion, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                if ($resultConfiguracion->execute()) {
                                                    $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . "C" . $key2] = $responseConfiguracion;
                                                    $tmp->{"configuracion"}[$key2]["id"] = $idConfiguracion;
                                                }
                                            } elseif ($fiador["logs"]["accion"] == 'Eliminar') {
                                                $queryConfiguracion = 'EXEC sga_spEliminarFiadorRC :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                                                $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                $resultConfiguracion->bindParam(':id', $fiador["id"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':descripcion', $fiador["logs"]["descripcion"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                if ($resultConfiguracion->execute()) {
                                                    $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . 'C' . $key2] = $responseConfiguracion;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case "6":
                                    foreach ($garantia["configuracion"] as $key2 => $hipoteca) {
                                        $idHipoteca = $hipoteca["id"];
                                        $tmp->{"configuracion"}[$key2]["id"] = $hipoteca["id"];
                                        if (isset($hipoteca["logs"])) {
                                            $responseConfiguracion = null;
                                            if ($hipoteca["logs"]["accion"] == 'Guardar') {
                                                $arrDetallesLogsConf = [];

                                                if (isset($hipoteca["logs"]["detalles"])) {
                                                    foreach ($hipoteca["logs"]["detalles"] as $detalleLog) {
                                                        $arrDetallesLogsConf[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                                    }
                                                }

                                                $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;
                                                $queryHipoteca = 'EXEC sga_spGuardarHipotecaRC :idCredito,:id,:descripcion,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idHipoteca;';
                                                $resultHipoteca = $this->conexion->prepare($queryHipoteca, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                $resultHipoteca->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':id', $hipoteca["id"], PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':descripcion', $hipoteca["descripcion"], PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                $resultHipoteca->bindParam(':descripcionLog', $hipoteca["logs"]["descripcion"], PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                                                $resultHipoteca->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                                                $resultHipoteca->bindParam(':idHipoteca', $idHipoteca, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                if ($resultHipoteca->execute()) {
                                                    $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . "C" . $key2] = $responseConfiguracion;
                                                    $tmp->{"configuracion"}[$key2]["id"] = $idHipoteca;
                                                }
                                            } elseif ($hipoteca["logs"]["accion"] == 'Eliminar') {
                                                $queryConfiguracion = 'EXEC sga_spEliminarHipotecaRC :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                                                $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                                                $resultConfiguracion->bindParam(':id', $hipoteca["id"], PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                                                $resultConfiguracion->bindParam(':descripcion', $hipoteca["logs"]["descripcion"], PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                                                $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                                                if ($resultConfiguracion->execute()) {
                                                    $responseConfiguracion != 'EXITO' && $arrErrores["G" . $key . 'C' . $key2] = $responseConfiguracion;
                                                }
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }

                    $idsGarantias[$key] = $tmp;
                }
            }
        }

        $respuesta = 'EXITO';
        (!empty($responseCredito) && $responseCredito != 'EXITO') && $respuesta = $responseCredito;

        $nombreArchivo = null;

        if ($respuesta == 'EXITO') {
            $nombreResolucion = 'R' . $this->datosCredito["numeroCredito"] . '.pdf';
            $nombreClausula = 'C' . $this->datosCredito["numeroCredito"] . '.pdf';
            $datosCredito = $this->datosCredito;
            $datosDeudor = $this->datosDeudor;
            $garantias = $this->garantias;
            $incluyeParciales = $this->incluyeParciales;
            require_once __DIR__ . '/../sgaConstruirResolucion.php';
            require_once __DIR__ . '/../sgaConstruirClausula.php';
        }

        return ["respuesta" => $respuesta, "response" => $responseCredito, "id" => $this->id, "Errores" => $arrErrores, "idsAditivos" => $idsAditivos, "idDeudor" => $idDeudor, 'idsGarantias' => $idsGarantias, 'idsDesembolsos' => $idsDesembolsos, 'nombreResolucion' => $nombreResolucion, 'nombreClausula' => $nombreClausula];

        $this->conexion = null;
        return ["respuesta" => "Pendiente"];
    }

    public function getRCAdministracion(string $tipoRol, int $usuario, int $agencia, string $inicio, string $fin)
    {
        $condicion = $tipoRol == 'Administrativo' ? '' : ' AND idUsuario=' . $usuario;
        $query = "SELECT * FROM sga_viewRCAdministracion WHERE CAST(fechaRegistro as date) BETWEEN '{$inicio}' and '{$fin}' AND agencia = " . $agencia . "" . $condicion . ";";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            $datos = $result->fetchAll(PDO::FETCH_OBJ);

            foreach ($datos as $key => $mutuo) {
                $datos[$key]->fechaGeneracion = date('d-m-Y h:i a', strtotime($mutuo->fechaGeneracion));
                $datos[$key]->fechaAprobacion = date('d-m-Y', strtotime($mutuo->fechaAprobacion));
                $datos[$key]->fechaRegistro = date('d-m-Y h:i a', strtotime($mutuo->fechaRegistro));
                $garantias = explode('@@@', $mutuo->garantias);
                $datos[$key]->garantias = $garantias;
            }

            return ["respuesta" => "EXITO", "resultados" => $datos];
        }

        $this->conexion = null;
        return [];
    }

    public function eliminarRC($idUsuario, $tipoApartado, $idApartado)
    {
        $response = null;
        $query = 'EXEC sga_spEliminarRC :id,:usuario,:tipoApartado,:idApartado,:response;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return $response;
        }

        $this->conexion = null;
        return [];
    }

    public function getRC()
    {
        $queryCredito = 'SELECT top(1) * FROM sga_viewDatosCreditoRC where id = :id;';
        $resultCredito = $this->conexion->prepare($queryCredito, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultCredito->bindParam(':id', $this->id, PDO::PARAM_INT);

        if ($resultCredito->execute() && $resultCredito->rowCount() > 0) {
            $datosCredito = $resultCredito->fetchAll(PDO::FETCH_OBJ)[0];
            $incluyeFirmante = false;
            !empty($datosCredito->fechaGeneracion) && $datosCredito->fechaGeneracion = date('d-m-Y', strtotime($datosCredito->fechaGeneracion));
            !empty($datosCredito->fechaAprobacion) && $datosCredito->fechaAprobacion = date('d-m-Y', strtotime($datosCredito->fechaAprobacion));

            $queryAditivos = 'SELECT * FROM sga_viewAditivosCreditoRC where idCredito = :id;';
            $resultAditivos = $this->conexion->prepare($queryAditivos, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultAditivos->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultAditivos->execute();

            $datosCredito->aditivos = $resultAditivos->rowCount() > 0 ? $resultAditivos->fetchAll(PDO::FETCH_OBJ) : [];
            foreach ($datosCredito->aditivos as $key => $aditivo) {
                $datosCredito->aditivos[$key]->monto = number_format($aditivo->monto, 2, '.', '');
            }

            $queryDeudor = 'SELECT top(1) * FROM sga_viewDatosDeudoresRC where idCredito = :id;';
            $resultDeudor = $this->conexion->prepare($queryDeudor, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultDeudor->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultDeudor->execute();

            $datosDeudor = $resultDeudor->rowCount() > 0 ? $resultDeudor->fetchAll(PDO::FETCH_OBJ)[0] : new stdClass();

            if ($datosCredito->desembolsoParcial == 'S') {
                $queryParciales = 'SELECT * FROM sga_viewDesembolsosCreditoRC where idCredito = :id;';
                $resultParciales = $this->conexion->prepare($queryParciales, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultParciales->bindParam(':id', $this->id, PDO::PARAM_INT);
                $resultParciales->execute();
                $datosParciales = $resultParciales->rowCount() > 0 ? $resultParciales->fetchAll(PDO::FETCH_OBJ) : [];

                foreach ($datosParciales as $key => $parcial) {
                    $datosParciales[$key]->fecha = date('d-m-Y', strtotime($parcial->fecha));
                }

                $datosCredito->parciales = $datosParciales;
            }

            $queryGarantias = 'SELECT * FROM sga_viewGarantiasCreditoRC where idCredito = :id;';
            $resultGarantias = $this->conexion->prepare($queryGarantias, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultGarantias->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultGarantias->execute();

            $datosGarantias = $resultGarantias->rowCount() > 0 ? $resultGarantias->fetchAll(PDO::FETCH_OBJ) : [];

            foreach ($datosGarantias as $key => $tipoGarantia) {
                switch ($tipoGarantia->idTipoGarantia) {
                    case '2':
                        $queryAportaciones = 'SELECT top(1) * FROM sga_viewAportacionesCreditoRC where idCredito = :id;';
                        $resultAportaciones = $this->conexion->prepare($queryAportaciones, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $resultAportaciones->bindParam(':id', $this->id, PDO::PARAM_INT);
                        $resultAportaciones->execute();

                        $datosGarantias[$key]->configuracion = $resultAportaciones->rowCount() > 0 ? $resultAportaciones->fetchAll(PDO::FETCH_OBJ)[0] : new stdClass();
                        break;
                    case '3':
                        $queryCDP = 'SELECT TOP(1) * FROM sga_viewCDPCreditoRC where idCredito = :id;';
                        $resultCDP = $this->conexion->prepare($queryCDP, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $resultCDP->bindParam(':id', $this->id, PDO::PARAM_INT);
                        $resultCDP->execute();

                        $datosGarantias[$key]->configuracion = $resultCDP->rowCount() > 0 ? $resultCDP->fetchAll(PDO::FETCH_OBJ)[0] : new stdClass();
                        break;
                    case '5':
                        $queryFiadores = 'SELECT * FROM sga_viewDatosFiadoresRC where idCredito = :id;';
                        $resultFiadores = $this->conexion->prepare($queryFiadores, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $resultFiadores->bindParam(':id', $this->id, PDO::PARAM_INT);
                        $resultFiadores->execute();

                        $datosFiadores = $resultFiadores->fetchAll(PDO::FETCH_OBJ);

                        $datosGarantias[$key]->configuracion = $datosFiadores;
                        break;
                    case '6':
                        $queryHipotecas = 'SELECT * FROM sga_viewDatosHipotecasRC where idCredito = :id;';
                        $resultHipotecas = $this->conexion->prepare($queryHipotecas, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $resultHipotecas->bindParam(':id', $this->id, PDO::PARAM_INT);
                        $resultHipotecas->execute();

                        $datosHipotecas = $resultHipotecas->fetchAll(PDO::FETCH_OBJ);

                        $datosGarantias[$key]->configuracion = $datosHipotecas;
                        break;
                }
            }

            $this->conexion = null;
            return ['respuesta' => 'EXITO', 'datosCredito' => $datosCredito, 'datosDeudor' => $datosDeudor, 'datosGarantias' => $datosGarantias];
        }

        $this->conexion = null;
        return ['respuesta' => 'SinRegistros'];
    }
}
