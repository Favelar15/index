<?php
class pagare
{
    public $id;
    public $numeroCreditoPagare;
    public $datosCredito;
    public $datosDeudor;
    public $fiadores;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function searchPagare()
    {
        $query = "SELECT id from sga_viewPagareAdministracion WHERE numeroCredito = :numeroCredito;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':numeroCredito', $this->numeroCredito);
        $result->execute();

        $this->conexion = null;
        return $result->fetchAll(PDO::FETCH_OBJ);
    }

    public function savePagare($idUsuario, $ipActual, $tipoApartado, $idApartado)
    {
        $arrErrores = [];
        $idsFiadores = [];
        $responseCredito = null;
        $id = null;
        $idDeudor = null;
        $permitirGuardado = true;

        if (isset($this->datosCredito["logs"])) {
            $queryCredito = "EXEC sga_spGuardarCreditoPagare :id,:idMutuo,:agencia,:numeroCredito,:monto,:tasaInteres,:fechaGeneracion,:fechaVencimiento,:descripcionLog,:detallesLog,:tipoApartado,:idApartado,:usuario,:ip,:response,:idCredito;";
            $fechaGeneracion = date('Y-m-d H:i', strtotime($this->datosCredito["fechaGeneracion"]));
            $fechaVencimiento = date('Y-m-d H:i', strtotime($this->datosCredito["fechaVencimiento"]));

            $arrDetallesLogs = [];


            if (isset($this->datosCredito["logs"]["detalles"])) {
                foreach ($this->datosCredito["logs"]["detalles"] as $detalleLog) {
                    $arrDetallesLogs[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                }
            }
            $detallesLog = count($arrDetallesLogs) > 0 ? implode('@@@', $arrDetallesLogs) : null;
            $idMutuo = !empty($this->datosCredito['idMutuo']) ? $this->datosCredito['idMutuo'] : null;

            $resultCredito = $this->conexion->prepare($queryCredito, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultCredito->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultCredito->bindParam(':idMutuo', $idMutuo, PDO::PARAM_INT);
            $resultCredito->bindParam(':agencia', $this->datosCredito["agencia"], PDO::PARAM_INT);
            $resultCredito->bindParam(':numeroCredito', $this->datosCredito["numeroCreditoPagare"], PDO::PARAM_STR);
            $resultCredito->bindParam(':monto', $this->datosCredito["monto"], PDO::PARAM_STR);
            $resultCredito->bindParam(':tasaInteres', $this->datosCredito["tasaInteres"], PDO::PARAM_STR);
            $resultCredito->bindParam(':fechaGeneracion', $fechaGeneracion, PDO::PARAM_STR);
            $resultCredito->bindParam(':fechaVencimiento', $fechaVencimiento, PDO::PARAM_STR);
            $resultCredito->bindParam(':descripcionLog', $this->datosCredito["logs"]["descripcion"], PDO::PARAM_STR);
            $resultCredito->bindParam(':detallesLog', $detallesLog, PDO::PARAM_STR);
            $resultCredito->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
            $resultCredito->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
            $resultCredito->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
            $resultCredito->bindParam(':ip', $ipActual, PDO::PARAM_STR);
            $resultCredito->bindParam(':response', $responseCredito, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
            $resultCredito->bindParam(':idCredito', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

            if ($resultCredito->execute()) {
                $responseCredito != 'EXITO' && $arrErrores["Crédito"] = $responseCredito;
                $responseCredito != 'EXITO' && $permitirGuardado = false;
                $this->id = $id;
            }
        }

        if ($permitirGuardado) {
            if ($this->id > 0 && isset($this->datosDeudor["logs"])) {
                $responseDeudor = null;
                $arrDetallesLogs = [];

                if (isset($this->datosDeudor["logs"]["detalles"])) {
                    foreach ($this->datosDeudor["logs"]["detalles"] as $detalleLog) {
                        $arrDetallesLogs[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                    }
                }
                $fechaNacimiento = date('Y-m-d', strtotime($this->datosDeudor['fechaNacimiento']));
                $detallesLogDeudor = count($arrDetallesLogs) > 0 ? implode("@@@", $arrDetallesLogs) : null;
                $queryDeudor = 'EXEC sga_spGuardarDeudorPagare :idCredito,:id,:codigoCliente,:tipoDocumento,:numeroDocumento,:nit,:nombres,:apellidos,:conocido,:genero,:pais,:departamento,:municipio,:direccion,:fechaNacimiento,:edad,:telefono,:usuario,:ip,:descripcionLog,:detallesLog,:tipoApartado,:idApartado,:response,:idDeudor;';
                $resultDeudor = $this->conexion->prepare($queryDeudor, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultDeudor->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                $resultDeudor->bindParam(':id', $this->datosDeudor["id"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':codigoCliente', $this->datosDeudor["codigoCliente"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':tipoDocumento', $this->datosDeudor["tipoDocumento"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':numeroDocumento', $this->datosDeudor["numeroDocumento"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':nit', $this->datosDeudor["nit"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':nombres', $this->datosDeudor["nombres"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':apellidos', $this->datosDeudor["apellidos"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':conocido', $this->datosDeudor["conocido"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':genero', $this->datosDeudor["genero"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':pais', $this->datosDeudor["pais"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':departamento', $this->datosDeudor["departamento"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':municipio', $this->datosDeudor["municipio"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':direccion', $this->datosDeudor["direccion"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':fechaNacimiento', $fechaNacimiento, PDO::PARAM_STR);
                $resultDeudor->bindParam(':edad', $this->datosDeudor["edad"], PDO::PARAM_INT);
                $resultDeudor->bindParam(':telefono', $this->datosDeudor["telefono"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                $resultDeudor->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                $resultDeudor->bindParam(':descripcionLog', $this->datosDeudor["logs"]["descripcion"], PDO::PARAM_STR);
                $resultDeudor->bindParam(':detallesLog', $detallesLogDeudor, PDO::PARAM_STR);
                $resultDeudor->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                $resultDeudor->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                $resultDeudor->bindParam(':response', $responseDeudor, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                $resultDeudor->bindParam(':idDeudor', $idDeudor, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                if ($resultDeudor->execute()) {
                    $responseDeudor != 'EXITO' && $arrErrores["Deudor"] = $responseDeudor;
                }
            } else {
                $idDeudor = $this->datosDeudor["id"];
            }

            if ($this->id > 0) {
                foreach ($this->fiadores as $key2 => $fiador) {
                    $idsFiadores[$key2]["id"] = $fiador["id"];
                    if (isset($fiador["logs"])) {
                        $responseConfiguracion = null;
                        if ($fiador["logs"]["accion"] == 'Guardar') {
                            $arrDetallesLogsConf = [];
                            $idConfiguracion = null;

                            if (isset($fiador["logs"]["detalles"])) {
                                foreach ($fiador["logs"]["detalles"] as $detalleLog) {
                                    $arrDetallesLogsConf[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["nuevoValor"];
                                }
                            }

                            $fechaNacimiento = date('Y-m-d', strtotime($fiador['fechaNacimiento']));

                            $detallesLogConfiguracion = count($arrDetallesLogsConf) > 0 ? implode('@@@', $arrDetallesLogsConf) : null;
                            $queryConfiguracion = 'EXEC sga_spGuardarFiadorPagare :idCredito,:id,:tipoDocumento,:numeroDocumento,:nit,:nombres,:apellidos,:conocido,:pais,:departamento,:municipio,:direccion,:fechaNacimiento,:edad,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idFiador;';
                            $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultConfiguracion->bindParam(':idCredito', $this->id, PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':id', $fiador["id"], PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':tipoDocumento', $fiador["tipoDocumento"], PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':numeroDocumento', $fiador["numeroDocumento"], PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':nit', $fiador["nit"], PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':nombres', $fiador["nombres"], PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':apellidos', $fiador["apellidos"], PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':conocido', $fiador["conocido"], PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':pais', $fiador["pais"], PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':departamento', $fiador["departamento"], PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':municipio', $fiador["municipio"], PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':direccion', $fiador["direccion"], PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':fechaNacimiento', $fechaNacimiento, PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':edad', $fiador["edad"], PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':descripcionLog', $fiador["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':detallesLog', $detallesLogConfiguracion, PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                            $resultConfiguracion->bindParam(':idFiador', $idConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultConfiguracion->execute()) {
                                $responseConfiguracion != 'EXITO' && $arrErrores["Fiador" . $key2] = $responseConfiguracion;
                                $idsFiadores[$key2]["id"] = $idConfiguracion;
                            }
                        } elseif ($fiador["logs"]["accion"] == 'Eliminar') {
                            $queryConfiguracion = 'EXEC sga_spEliminarFiadorPagare :id,:usuario,:tipoApartado,:idApartado,:descripcion,:ip,:response;';
                            $resultConfiguracion = $this->conexion->prepare($queryConfiguracion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                            $resultConfiguracion->bindParam(':id', $fiador["id"], PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                            $resultConfiguracion->bindParam(':descripcion', $fiador["logs"]["descripcion"], PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                            $resultConfiguracion->bindParam(':response', $responseConfiguracion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                            if ($resultConfiguracion->execute()) {
                                $responseConfiguracion != 'EXITO' && $arrErrores["Fiador" . $key2] = $responseConfiguracion;
                            }
                        }
                    }
                }
            }
        }

        $respuesta = 'EXITO';
        (!empty($responseCredito) && $responseCredito != 'EXITO') && $respuesta = $responseCredito;

        $nombrePagare = null;

        if ($respuesta == 'EXITO') {
            $nombrePagare = 'P' . $this->datosCredito["numeroCredito"] . '.pdf';
            $datosCredito = $this->datosCredito;
            $datosDeudor = $this->datosDeudor;
            $datosFiadores = $this->fiadores;
            require_once __DIR__ . '/../sgaConstruirPagare.php';
        }

        $this->conexion = null;
        return ["respuesta" => $respuesta, "response" => $responseCredito, "id" => $this->id, "Errores" => $arrErrores, "idDeudor" => $idDeudor, 'idsFiadores' => $idsFiadores, 'nombrePagare' => $nombrePagare];

        return ["respuesta" => "Pendiente"];
    }

    public function getPagareAdministracion(string $tipoRol, int $usuario, int $agencia, string $inicio, string $fin)
    {
        $condicion = $tipoRol == 'Administrativo' ? '' : ' AND idUsuario=' . $usuario;
        $query = "SELECT * FROM sga_viewPagareAdministracion WHERE CAST(fechaRegistro as date) BETWEEN '{$inicio}' and '{$fin}' AND agencia = " . $agencia . "" . $condicion . ";";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            $datos = $result->fetchAll(PDO::FETCH_OBJ);

            foreach ($datos as $key => $mutuo) {
                $datos[$key]->fechaGeneracion = date('d-m-Y h:i a', strtotime($mutuo->fechaGeneracion));
                $datos[$key]->fechaVencimiento = date('d-m-Y', strtotime($mutuo->fechaVencimiento));
                $datos[$key]->fechaRegistro = date('d-m-Y h:i a', strtotime($mutuo->fechaRegistro));
            }

            return ["respuesta" => "EXITO", "resultados" => $datos];
        }

        $this->conexion = null;
        return [];
    }

    public function eliminarPagare($idUsuario, $tipoApartado, $idApartado)
    {
        $response = null;
        $query = 'EXEC sga_spEliminarPagare :id,:usuario,:tipoApartado,:idApartado,:response;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return $response;
        }

        $this->conexion = null;
        return [];
    }

    public function getPagare()
    {
        $queryCredito = 'SELECT top(1) * FROM sga_viewDatosCreditoPagare where id = :id;';
        $resultCredito = $this->conexion->prepare($queryCredito, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultCredito->bindParam(':id', $this->id, PDO::PARAM_INT);

        if ($resultCredito->execute() && $resultCredito->rowCount() > 0) {
            $datosCredito = $resultCredito->fetchAll(PDO::FETCH_OBJ)[0];
            !empty($datosCredito->fechaGeneracion) && $datosCredito->fechaGeneracion = date('d-m-Y', strtotime($datosCredito->fechaGeneracion));
            !empty($datosCredito->fechaVencimiento) && $datosCredito->fechaVencimiento = date('d-m-Y', strtotime($datosCredito->fechaVencimiento));

            $queryDeudor = 'SELECT top(1) * FROM sga_viewDatosDeudoresPagare where idCredito = :id;';
            $resultDeudor = $this->conexion->prepare($queryDeudor, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultDeudor->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultDeudor->execute();

            $datosDeudor = $resultDeudor->rowCount() > 0 ? $resultDeudor->fetchAll(PDO::FETCH_OBJ)[0] : new stdClass();

            $queryFiadores = 'SELECT * FROM sga_viewDatosFiadoresPagare where idCredito = :id;';
            $resultFiadores = $this->conexion->prepare($queryFiadores, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultFiadores->bindParam(':id', $this->id, PDO::PARAM_INT);
            $resultFiadores->execute();

            $datosFiadores = $resultFiadores->fetchAll(PDO::FETCH_OBJ);

            $this->conexion = null;
            return ['respuesta' => 'EXITO', 'datosCredito' => $datosCredito, 'datosDeudor' => $datosDeudor, 'datosFiadores' => $datosFiadores];
        }

        $this->conexion = null;
        return ['respuesta' => 'SinRegistros'];
    }
}
