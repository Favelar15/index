<?php
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (isset($datosDeudor) && isset($datosCredito) && isset($garantias) && isset($incluyeParciales)) {
        require_once 'C:\xampp\htdocs\vendor\autoload.php';
        global $arrMeses;
        //Variables generales
        $tmpFecha = date('d-m-Y', strtotime($datosCredito['fechaGeneracion']));
        $enter1 = "<br>";
        $enter2 = "<br> <br>";
        $nombreCompleto = !empty($datosDeudor['conocido']) ? $datosDeudor['nombres'] . ' ' . $datosDeudor['apellidos'] . ' CONOCIDO POR ' . $datosDeudor['conocido'] : $datosDeudor['nombres'] . ' ' . $datosDeudor['apellidos'];
        $textoDesembolsoParcial = $datosCredito['desembolsoParcial'] == 'S' ? "como desembolso parcial," : "";
        $arrComites = explode(' ', mb_strtoupper($datosCredito['labelComite'], 'UTF-8'));
        $txtTipoComite = $datosCredito['comite'] == 1 ? (isset($arrComites[2]) ? $arrComites[2] : $arrComites[1]) : $datosCredito['labelComite'];

        $direccionCompleta = $datosDeudor['direccion'] . ", " . mb_strtoupper($datosDeudor['labelMunicipio'], "UTF-8") . ", " . mb_strtoupper($datosDeudor['labelDepartamento'], "UTF-8");

        $nombreUsuario = $_SESSION['index']->nombres . ' ' . $_SESSION['index']->apellidos;

        //Variables de control
        $cntAditivos = count($datosCredito['aditivos']);
        class MYPDF extends TCPDF
        {
            //Page header
            public function Header()
            {
                $bMargin = $this->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $this->AutoPageBreak;
                // disable auto-page-break
                $this->SetAutoPageBreak(false, 0);
                // set bacground image
                $img_file = '../../img/membrete.png';
                $this->Image('@' . file_get_contents($img_file), 0, 0, 217, 279, '', '', '', false, 300);
                // restore auto-page-break status
                $this->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $this->setPageMark();
            }

            // Page footer
            public function Footer()
            {
                // Position at 15 mm from bottom
                $this->SetY(-20);
                // Set font
                $this->SetFont('helvetica', 'I', 8);
                // Page number
                $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            }
        }

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Departamento IT');
        $pdf->SetTitle('Resolución');
        $pdf->SetSubject('ACACYPAC');
        $pdf->SetKeywords('TCPDF, PDF, Mutuos');

        // Fuente de cabecera y pie de página
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // Espacios por defecto
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //Margenes
        $pdf->SetMargins(15, 25, 15);
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // Configurar Auto salto de página
        $pdf->SetAutoPageBreak(TRUE, 35);

        // Factor de escalado de imagen
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // Configurando lenguaje
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // declaramos las clases a utilizar
        $pronombre = generoPronombre($datosDeudor['genero']);
        $calculoPlazo = formaPago($datosCredito, $datosDeudor, $cntAditivos, $pronombre, $garantias);
        $fuenteFondos =  $datosCredito['labelFuenteFondos'] == 'ACACYPAC DE RL' ? "FONDOS PROPIOS" : $datosCredito['labelFuenteFondos'];

        $pdf->AddPage();
        $pdf->SetFont('dejavusans', ' ', 7.5);
        $pdf->Write(0, $datosCredito['labelAgencia'] . ', ' . $tmpFecha, '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML($enter2, true, false, true, false, '');
        $pdf->Write(0, 'SEÑOR (A) (ITA):', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(0, $nombreCompleto, '', 0, '', true, 0, false, false, 0);
        $pdf->Write(0, $direccionCompleta, '', 0, '', true, 0, false, false, 0);
        $pdf->WriteHTML($enter2, true, false, true, false, '');
        $pdf->Write(0, 'Estimado(a) Asociado(a):', '', 0, '', true, 0, false, false, 0);
        $pdf->WriteHTML($enter2, true, false, true, false, '');

        $pdf->Write(0, 'Sirva el presente para comunicarle que en sesión de COMITÉ DE CRÉDITO ' . $txtTipoComite . ' celebrada el pasado ' .
            date('d-m-Y', strtotime($datosCredito['fechaAprobacion'])) . ', se analizó la solicitud presentada por usted en la línea de ' .
            $datosCredito['labelLineaFinanciera'] . ' por un monto de $' . number_format($datosCredito['monto'], 2, '.', ',') .
            ', la cual después de ser ampliamente ANALIZADA, fue APROBADA ' . $textoDesembolsoParcial . ' bajo las características y condiciones siguientes:' . "\n", '', 0, 'J', true, 0, false, false, 0);


        $tblDesembolsos = '<table cellspacing="0" border="0" style="margin-left:5px;">';
        if ($datosCredito['desembolsoParcial'] == 'N') {
            $tblDesembolsos .= '<tr><td style="width:62px;">Monto:</td><td>$' . number_format($datosCredito['monto'], 2, '.', ',') . '</td></tr>';
        } else {
            $cntDesembolsos = 0;
            $sinonimoTiempo = array("Primer", "Segundo", "Tercer", "Cuarto", "Quinto", "Sexto", "Septimo", "Octavo", "Noveno", "Décimo", "Onceavo", "Doceavo");
            foreach ($datosCredito['detalleDesembolsos'] as $desembolso) {
                $tblDesembolsos .= '<tr><td style="width:62px;">Monto:</td><td style="width:70px;">$' . number_format($desembolso['monto'], 2, '.', ',') . '</td><td>' . $sinonimoTiempo[$cntDesembolsos] . ' desembolso ' . date('d-m-Y', strtotime($desembolso['fecha'])) . '</td></tr>';
                $cntDesembolsos++;
            }
        }
        $tblDesembolsos .= "</table>";
        $pdf->writeHTML($enter2 . $tblDesembolsos, true, false, true, false, '');

        $pdf->Write(0, 'Plazo:            ' . $datosCredito['plazo'] . ' ' . $datosCredito['labelTipoPlazo'], '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML($enter2, true, false, true, false, '');

        $pdf->Write(0, 'INTERÉS CORRIENTE:', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(0, number_format($datosCredito['tasaInteres'], 2, '.', ',') . '% de interés anual sobre saldos sujetos a modificaciones posteriores, de acuerdo a fluctuación de mercado capital.', '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML($enter2, true, false, true, false, '');

        $pdf->Write(0, 'INTERÉS MORATORIO:', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(0, '36% de interés anual sobre saldos de capital en mora.', '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML($enter2, true, false, true, false, '');

        $pdf->Write(0, 'LÍNEA FINANCIERA: ' . $datosCredito['labelLineaFinanciera'], '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML($enter2, true, false, true, false, '');

        $pdf->Write(0, 'DESTINO: ' . $datosCredito['destino'], '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML($enter2, true, false, true, false, '');
        $pdf->Write(0, 'FORMA DE PAGO:', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(0, $calculoPlazo . "\n", '', 0, 'J', true, 0, false, false, 0);
        $pdf->writeHTML($enter2, true, false, true, false, '');
        $pdf->Write(0, 'FUENTE DE FONDOS:       ' . $fuenteFondos, '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML($enter2, true, false, true, false, '');


        foreach ($garantias as $datosGarantia) {
            if ($datosGarantia['idTipoGarantia'] == 1 || $datosGarantia['idTipoGarantia'] == 4) {
            } else {
                $pdf->Write(0, 'GARANTÍA\'S:', '', 0, '', true, 0, false, false, 0);
                $detalleGarantia = " ";
                $pdf->Write(0, $datosGarantia['tipoGarantia'] . ' ', '', 0, '', true, 0, false, false, 0);
                $arrDescripciones = [];

                if (count($datosGarantia['configuracion']) > 0) {
                    if (isset($datosGarantia['configuracion']['descripcion'])) {
                        $arrDescripciones[] = $datosGarantia['configuracion']['descripcion'];
                    } else {
                        foreach ($datosGarantia['configuracion'] as $key2 => $configuracion) {
                            $arrDescripciones[] = $configuracion['descripcion'];
                        }
                    }
                    // foreach ($datosGarantia->valores_garantia as $detalle) {
                    //     $detalles =  is_object($detalle) ? $detalle->detalleGarantia : $detalle;
                    // }
                    // $pdf->Write(0,  "- " . $detalles, '', 0, '', true, 0, false, false, 0);
                    $pdf->writeHTML('- ' . implode('<br />- ', $arrDescripciones), true, false, true, false, '');
                }
            }
        }

        // $pdf->writeHTML($enter1, true, false, true, false, '');
        // $pdf->Write(0, 'OTRAS CONDICIONES:', '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML($enter2, true, false, true, false, '');
        $pdf->Write(0, 'NOTARIO:            ' . $datosCredito['notario'], '', 0, '', true, 0, false, false, 0);
        $pdf->Write(0, 'Si está de acuerdo con las condiciones arriba detalladas, favor comunicarlo a esta Gerencia para seguirle el trámite de su solicitud.', '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML($enter2, true, false, true, false, '');
        $pdf->Write(0, 'Atentamente,', '', 0, '', true, 0, false, false, 0);
        $pdf->writeHTML("<br><br><br>", true, false, true, false, '');
        $pdf->Write(0, '                            ' . mb_strtoupper($nombreUsuario, "UTF-8") . "                                                                            FIRMA DE RECIBIDO", '', 0, '', true, 0, false, false, 0);
        $mypdf = __DIR__ . '\..\docs\Resolucion\/' . $nombreResolucion . '';
        $pdf->Output($mypdf, 'F');
    }
}

function formaPago($datosCredito, $datosDeudor, $cntAditivos, $pronombre, $garantias)
{
    $valorPeriodicidad = 0;
    switch (intval($datosCredito['periodicidad'])) {
        case 1:
            $valorPeriodicidad = 1;
            break;
        case 2:
            $valorPeriodicidad = 1;
            break;
        case 3:
            $valorPeriodicidad = 1;
            break;
        case 4:
            $valorPeriodicidad = 2;
            break;
        case 5:
            $valorPeriodicidad = 3;
            break;
        case 6:
            $valorPeriodicidad = 6;
            break;
        case 7:
            $valorPeriodicidad = 12;
            break;
        default:
            $valorPeriodicidad = 0;
            break;
    }
    $plazo = " ";
    $arregloAditivos = [];
    $arregloLabelAditivos = [];
    $clausulaSeguroSinFiador = "";
    $labelAditivos = "";
    $agregoAditivos = 0;
    $tmpAditivos = 0;
    $aditivos = "";
    $labelAditivos = "";
    $valorCuota = floatval($datosCredito['capitalInteres']);
    $valorCapitalInteres = number_format($datosCredito['capitalInteres'], 2, '.', ',');
    $incluyeSeguro = false;
    $plazo = intval(intval($datosCredito['plazo']) - 1);

    foreach ($datosCredito['aditivos'] as $key => $aditivo) {
        $valorCuota += floatval($aditivo['monto']);
        $arregloAditivos[] = 'en concepto de ' . $aditivo['aditivo'] . ' $' . number_format($aditivo['monto'], 2, '.', ',');
        $tmpLabelAditivo = mb_strtolower($aditivo['aditivo'], "UTF-8");
        $arregloLabelAditivos[] = $tmpLabelAditivo;
        if (strpos($tmpLabelAditivo, "seguro") !== false) {
            $incluyeSeguro = true;
        }
    }

    if (count($arregloAditivos) > 0) {
        if (count($arregloAditivos) > 1) {
            $ultimoAditivo = $arregloAditivos[count($arregloAditivos) - 1];
            array_pop($arregloAditivos);
            $aditivos = ", " . implode(", ", $arregloAditivos) . " y " . $ultimoAditivo;

            $ultimoLabelAditivo = $arregloLabelAditivos[count($arregloLabelAditivos) - 1];
            array_pop($arregloLabelAditivos);
            $labelAditivos = ", " . implode(", ", $arregloLabelAditivos) . " y " . $ultimoLabelAditivo;
        } else {
            $aditivos = " y " . implode(", ", $arregloAditivos);
            $labelAditivos = " y " . implode(", ", $arregloLabelAditivos);
        }
    } else {
        $aditivos = "";
        $labelAditivos = "";
    }

    $clausulaSeguro = $incluyeSeguro ? " Las cuotas de seguro de vida para el pago de deuda y de otros seguros relacionados en crédito podrán ser objeto de variación, de acuerdo a las políticas de cobro de las primas de seguro que al efecto la Compañía Aseguradora comunique a La Cooperativa, lo cual desde hoy queda " . $pronombre[1] . " y lo acepta " . strtolower($pronombre[0]) . ". " : "";

    foreach ($garantias as $datosGarantia) {
        if ($datosGarantia['idTipoGarantia'] == 4) {
            $clausulaSeguroSinFiador =  ", a pagarse la primera cuota el día treinta del presente mes y sucesivamente cada día treinta de los meses comprendidos en el plazo hasta su total cancelación, a excepción del mes de febrero, que será el día ultimo del mes";
            break;
        }
    }


    if ($datosCredito['vencimiento'] == 'N') {
        $tmpPlazo = intval($datosCredito['plazo']) / intval($valorPeriodicidad) - 1;
        if (intval($tmpPlazo) > 2) {
            $plazo = $pronombre[0] . ' se obliga a pagar la suma adeudada por medio de ' . $tmpPlazo . ' CUOTAS ' . $datosCredito['labelPeriodicidad'] . 'ES, fijas, vencidas y sucesivas de $' . number_format($valorCuota, 2, '.', ',') . ', las cuales incluyen: En concepto de capital e intereses $' . $valorCapitalInteres . $aditivos . ' y una última cuota por el saldo de capital e intereses' . $labelAditivos . $clausulaSeguroSinFiador . "." . $clausulaSeguro;
        } else {
            $plazo = $pronombre[0] . ' se obliga a pagar la suma adeudada por medio de UNA CUOTA ' . $datosCredito['labelPeriodicidad'] . ', fija, vencida y sucesiva de $' . number_format($valorCuota, 2, '.', ',') . ', que incluye: En concepto de capital e intereses $' . $valorCapitalInteres . $aditivos . '  y una última cuota por el saldo de capital e intereses' . $labelAditivos . $clausulaSeguroSinFiador . "." . $clausulaSeguro;
        }
    } else {
        $plazo = $pronombre[0] . ' se obliga a pagar la suma adeudada por medio de UNA SOLA CUOTA al vencimiento del plazo, fija, vencida y sucesiva de $' . number_format($valorCuota, 2, '.', ',') . ', la cual incluye: En concepto de capital e intereses $' . $valorCapitalInteres . $aditivos . $clausulaSeguroSinFiador . "." . $clausulaSeguro;
    }
    return $plazo;
}

function generoPronombre($genero)
{
    $pronombre = array();
    if ($genero == 'M') {
        array_push($pronombre, 'El deudor', 'enterado', 'Sr');
    } else {
        array_push($pronombre, 'La deudora', 'enterada', 'Sra', 'Srta');
    }
    return $pronombre;
}
