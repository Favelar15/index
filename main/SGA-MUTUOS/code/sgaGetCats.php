<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";

        $solicitud = explode("@@@", $_GET["q"]);

        if (in_array('tiposDocumento', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../code/Models/tipoDocumento.php';
            $tipoDocumento = new tipoDocumento();
            $tiposDocumento = $tipoDocumento->obtenerTiposDocumento();

            $respuesta->{"tiposDocumento"} = $tiposDocumento;
        }

        if (in_array('agencias', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../code/Models/agencia.php';
            $agencia = new agencia();
            $agencias = $agencia->getAgenciasCbo($_SESSION["index"]->agencias);

            $respuesta->{"agencias"} = $agencias;
        }

        if (in_array('geograficos', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../code/Models/datosGeograficos.php';
            $geografico = new geografico();
            $datosGeograficos = $geografico->obtenerGeograficos();

            $respuesta->{"datosGeograficos"} = $datosGeograficos;
        }

        if (in_array('lineasFinancierasCbo', $solicitud) || in_array('lineasFinancieras', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../SIMAC/code/Models/lineaFinanciera.php';
            $linea = new lineaFinanciera();
            if (in_array('lineasFinancierasCbo', $solicitud) || in_array('all', $solicitud)) {
                $lineasFinancieras = $linea->getLineasFinancierasCbo();
            } else {
                $lineasFinancieras = $linea->getLineasFinancieras();
            }

            $respuesta->{"lineasFinancieras"} = $lineasFinancieras;
        }

        if (in_array('tiposPlazo', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../SIMAC/code/Models/tipoPlazo.php';
            $tipo = new tipoPlazo();
            $tiposPlazo = $tipo->getTiposPlazoCbo();

            $respuesta->{"tiposPlazo"} = $tiposPlazo;
        }

        if (in_array('periodicidades', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../SIMAC/code/Models/periodicidadPago.php';
            $periodicidad = new periodicidadPago();
            $periodicidades = $periodicidad->getPeriodicidadesCbo();

            $respuesta->{"periodicidades"} = $periodicidades;
        }

        if (in_array('fuentesFondo', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../SIMAC/code/Models/fuenteFondos.php';
            $fuente = new fuenteFondos();
            $fuentesFondo = $fuente->getFuentesFondosCbo();

            $respuesta->{"fuentesFondo"} = $fuentesFondo;
        }

        if (in_array('aditivos', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../SIMAC/code/Models/aditivo.php';
            $aditivo = new simacAditivo();
            $aditivos = $aditivo->getAditivosCbo();

            $respuesta->{"aditivos"} = $aditivos;
        }

        if (in_array('tiposGarantia', $solicitud) || in_array('tiposGarantiaCbo', $solicitud) || in_array('all', $solicitud)) {
            require_once '../../SIMAC/code/Models/tipoGarantia.php';
            $tipo = new tipoGarantia();
            if (in_array('tiposGarantiaCbo', $solicitud) || in_array('all', $solicitud)) {
                $tiposGarantia = $tipo->getTiposGarantiaCbo();
            } else {
                $tiposGarantia = $tipo->getTiposGarantia();
            }

            $respuesta->{"tiposGarantia"} = $tiposGarantia;
        }

        if (in_array('zonasHipoteca', $solicitud) || in_array('all', $solicitud)) {
            require_once './Models/zona.php';
            $zona = new zona();
            $zonas = $zona->getZonas();

            $respuesta->{"zonasHipoteca"} = $zonas;
        }

        if(in_array('comites',$solicitud) || in_array('all',$solicitud)){
            require_once './Models/comite.php';
            $comite = new comite();
            $comites = $comite->getComitesCbo();

            $respuesta->{"comites"} = $comites;
        }

        $conexion = null;
    }
}

echo json_encode($respuesta);
