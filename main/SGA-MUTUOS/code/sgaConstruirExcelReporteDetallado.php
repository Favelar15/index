<?php
require_once "../../code/generalParameters.php";
require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        $filename = "ReporteDetallado.xlsx";
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'N°');
        $sheet->setCellValue('B1', 'Agencia');
        $sheet->setCellValue('C1', 'Código de cliente');
        $sheet->setCellValue('D1', 'Nombre del asociado');
        $sheet->setCellValue('E1', 'Monto');
        $sheet->setCellValue('F1', 'N° de crédito');
        $sheet->setCellValue('G1', 'Fecha elaboración de mutuo');
        $sheet->setCellValue('H1', 'Garantías');
        $sheet->setCellValue('I1', 'Matrícula');
        $sheet->setCellValue('J1', 'Propietario del inmueble');
        $sheet->setCellValue('K1', 'Usuario responsable');
        $sheet->setCellValue('L1', '¿Línea rotativa?');
        $sheet->setCellValue('M1', 'Fecha vencimiento');

        $spreadsheet->getActiveSheet()->setTitle('Mutuos');

        $cntLineas = 1;
        foreach ($input['datosMutuos'] as $registro) {
            $cntLineas++;
            $arrGarantias = !empty($registro['garantias']) ? explode('@@@', $registro['garantias']) : [];
            $impGarantias = count($arrGarantias) > 0 ? "- " . implode("\n - ", $arrGarantias) : '';
            $impRotativa = $registro['lineaRotativa'] == 'S' ? 'SI' : 'NO';
            $arrMatriculas = !empty($registro['matriculas']) ? explode('@@@', $registro['matriculas']) : [];
            $impMatriculas = count($arrMatriculas) > 0 ? "- " . implode("\n - ", $arrMatriculas) : '';
            $arrPresenta = !empty($registro['presentadaPor']) ? explode('@@@', $registro['presentadaPor']) : [];
            $impPresenta = count($arrPresenta) > 0 ? "- " . implode("\n - ", $arrPresenta) : '';

            $sheet->setCellValue('A' . $cntLineas, ($cntLineas - 1));
            $sheet->setCellValue('B' . $cntLineas, $registro['agencia']);
            $sheet->setCellValue('C' . $cntLineas, $registro['codigoCliente']);
            $sheet->setCellValue('D' . $cntLineas, $registro['nombreDeudor']);
            $sheet->setCellValue('E' . $cntLineas, $registro['monto']);
            $sheet->setCellValue('F' . $cntLineas, $registro['numeroCredito']);
            $sheet->setCellValue('G' . $cntLineas, $registro['fechaDocumento']);
            $sheet->setCellValue('H' . $cntLineas, $impGarantias);
            $sheet->setCellValue('I' . $cntLineas, $impMatriculas);
            $sheet->setCellValue('J' . $cntLineas, $impPresenta);
            $sheet->setCellValue('K' . $cntLineas, $registro['nombreUsuario']);
            $sheet->setCellValue('L' . $cntLineas, $impRotativa);
            $sheet->setCellValue('M' . $cntLineas, $registro['fechaVencimiento']);
            $spreadsheet->getActiveSheet()->getStyle('H' . $cntLineas)->getAlignment()->setWrapText(true);
        }

        foreach (range('A', 'M') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle('A1:M1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:M1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1);

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'N°');
        $sheet->setCellValue('B1', 'Agencia');
        $sheet->setCellValue('C1', 'Código de cliente');
        $sheet->setCellValue('D1', 'Tipo de documento');
        $sheet->setCellValue('E1', 'Número de documento');
        $sheet->setCellValue('F1', 'NIT');
        $sheet->setCellValue('G1', 'Nombre del asociado');
        $sheet->setCellValue('H1', 'País de residencia');
        $sheet->setCellValue('I1', 'Departamento de residencia');
        $sheet->setCellValue('J1', 'Municipio de residencia');
        $sheet->setCellValue('K1', 'Dirección completa');
        $sheet->setCellValue('L1', 'Edad');
        $sheet->setCellValue('M1', 'N° de préstamo');
        $sheet->setCellValue('N1', 'Monto desembolsado');
        $sheet->setCellValue('O1', 'Tasa de interés');
        $sheet->setCellValue('P1', 'Fecha elaboración');
        $sheet->setCellValue('Q1', 'Fecha vencimiento');
        $sheet->setCellValue('R1', 'Usuario responsable');

        $spreadsheet->getActiveSheet()->setTitle('Pagares');

        $cntLineas = 1;
        foreach ($input['datosPagares'] as $registro) {
            $cntLineas++;

            $sheet->setCellValue('A' . $cntLineas, ($cntLineas - 1));
            $sheet->setCellValue('B' . $cntLineas, $registro['agencia']);
            $sheet->setCellValue('C' . $cntLineas, $registro['codigoCliente']);
            $sheet->setCellValue('D' . $cntLineas, $registro['tipoDocumento']);
            $sheet->setCellValue('E' . $cntLineas, $registro['numeroDocumento']);
            $sheet->setCellValue('F' . $cntLineas, $registro['nit']);
            $sheet->setCellValue('G' . $cntLineas, $registro['nombreDeudor']);
            $sheet->setCellValue('H' . $cntLineas, $registro['pais']);
            $sheet->setCellValue('I' . $cntLineas, $registro['departamento']);
            $sheet->setCellValue('J' . $cntLineas, $registro['municipio']);
            $sheet->setCellValue('K' . $cntLineas, $registro['direccion']);
            $sheet->setCellValue('L' . $cntLineas, $registro['edad']);
            $sheet->setCellValue('M' . $cntLineas, $registro['numeroCredito']);
            $sheet->setCellValue('N' . $cntLineas, $registro['monto']);
            $sheet->setCellValue('O' . $cntLineas, $registro['tasaInteres']);
            $sheet->setCellValue('P' . $cntLineas, $registro['fechaGeneracion']);
            $sheet->setCellValue('Q' . $cntLineas, $registro['fechaVencimiento']);
            $sheet->setCellValue('R' . $cntLineas, $registro['nombreUsuario']);
        }

        foreach (range('A', 'R') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle('A1:R1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:R1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $spreadsheet->setActiveSheetIndex(0);

        $writer = new Xlsx($spreadsheet);
        $writer->save("../docs/Reportes/" . $filename);

        $respuesta->{'respuesta'} = 'EXITO';
        $respuesta->{'nombreArchivo'} = $filename;
    }
} else {
    $respuesta->{'respuesta'} = 'SESION';
}

echo json_encode($respuesta);
