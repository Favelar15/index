<?php
require_once "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        require_once 'C:\xampp\htdocs\vendor\autoload.php';
        class MYPDF extends TCPDF
        {

            //Page header
            public function Header()
            {
                $bMargin = $this->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $this->AutoPageBreak;
                // disable auto-page-break
                $this->SetAutoPageBreak(false, 0);
                // set bacground image
                $img_file = '../../img/membrete.png';
                $this->Image('@' . file_get_contents($img_file), 0, 0, 220, 275, '', '', '', false, 300);
                // restore auto-page-break status
                $this->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $this->setPageMark();
            }

            // Page footer
            public function Footer()
            {
                // Position at 15 mm from bottom
                $this->SetY(-20);
                // Set font
                $this->SetFont('helvetica', 'I', 8);
                // Page number
                $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            }
        }

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Departamento IT');
        $pdf->SetTitle('Hoja de revisión');
        $pdf->SetSubject('ACACYPAC');
        $pdf->SetKeywords('TCPDF, PDF, Mutuos');

        // Fuente de cabecera y pie de página
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // Espacios por defecto
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //Margenes
        $pdf->SetMargins(10, 15, 0);
        $pdf->SetHeaderMargin(7);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // Configurar Auto salto de página
        $pdf->SetAutoPageBreak(TRUE, 40);

        // Factor de escalado de imagen
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // Configurando lenguaje
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // Establecer fuente
        $pdf->SetFont('dejavusans', 'B', 6);

        $pdf->AddPage();

        $nombreArchivo = 'HR' . $input["datosCredito"]["numeroCredito"] . '.pdf';

        $datosDeudor = $input["datosDeudor"];
        $datosCredito = $input["datosCredito"];
        $datosFirmante = $input["datosFirmante"];
        $garantias = $input["garantias"];

        $generales = '<table border="0">' .
            '<tr>' .
            '<td rowspan="3" style="width:215px; text-align:center; font-size:12px;"></td>' .
            '<td rowspan="3" style="width:300px; text-align:center; font-size:12px;"><div style="font-size:5pt">&nbsp;</div>FORMATO DE REVISIÓN DE MUTUO</td>' .
            '<td></td>' .
            '</tr>' .
            '<tr>' .
            '<td>Código de cliente: ' . $datosDeudor["codigoCliente"] . '</td>' .
            '</tr>' .
            '<tr>' .
            '<td>Número de crédito: ' . $datosCredito["numeroCredito"] . '</td>' .
            '</tr>' .
            '</table>';
        $pdf->writeHTML($generales, true, false, true, false, '');
        $pdf->SetFont('dejavusans', 'B', 7.8);
        $pdf->Write(0, "Datos de deudor/a", '', 0, 'C', true, 0, false, false, 0);
        $pdf->SetFont('dejavusans', 'B', 7);

        $aperturaTabla = '<table cellpadding="1" cellspacing="0" style="text-align:center;" nobr="true"><tr nobr="true">';
        $cierreTabla = '</tr></table><br><br>';
        $labelFirmaDeudor = $datosDeudor["firma"] == 'S' ? "SI" : "NO";

        $tblDeudor = $aperturaTabla . '
        <th>Código de cliente</th>
        <th>Tipo de documento</th>
        <th>Número de documento</th>
        <th>NIT</th>
        <th>Nombres</th>
        </tr>
        <tr>
        <td>' . $datosDeudor["codigoCliente"] . '</td>
        <td>' . $datosDeudor["labelTipoDocumento"] . '</td>
        <td>' . $datosDeudor["numeroDocumento"] . '</td>
        <td>' . $datosDeudor["nit"] . '</td>
        <td>' . $datosDeudor["nombres"] . '</td>
        ' . $cierreTabla . $aperturaTabla . '
        <th>Apellidos</th>
        <th>Conocido por</th>
        <th>Género</th>
        <th>País</th>
        <th>Departamento</th>
        </tr>
        <tr>
        <td>' . $datosDeudor["apellidos"] . '</td>
        <td>' . $datosDeudor["conocido"] . '</td>
        <td>' . $datosDeudor["labelGenero"] . '</td>
        <td>' . $datosDeudor["labelPais"] . '</td>
        <td>' . $datosDeudor["labelDepartamento"] . '</td>
        ' . $cierreTabla . $aperturaTabla . '
        <th>Municipio</th>
        <th>Profesión</th>
        <th>Fecha de nacimiento</th>
        <th>Edad</th>
        <th>¿Sabe firmar?</th>
        </tr>
        <tr>
        <td>' . $datosDeudor["labelMunicipio"] . '</td>
        <td>' . $datosDeudor["profesion"] . '</td>
        <td>' . $datosDeudor["fechaNacimiento"] . '</td>
        <td>' . $datosDeudor["edad"] . '</td>
        <td>' . $labelFirmaDeudor . '</td>
        ' . $cierreTabla;

        $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:140px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblDeudor, true, false, true, false, '');

        $labelVencimiento = $datosCredito["vencimiento"] == 'S' ? 'SI' : 'NO';
        $labelEmpleado = $datosCredito["empleadoACACYPAC"] == 'S' ? 'SI' : 'NO';
        $labelRotativa = $datosCredito["lineaRotativa"] == 'S' ? 'SI' : 'NO';

        $tblCredito = $aperturaTabla . '
        <th>Agencia</th>
        <th>Número de crédito</th>
        <th>Monto</th>
        <th>Tasa de interés</th>
        <th>Línea financiera</th>
        </tr>
        <tr>
        <td>' . $datosCredito["labelAgencia"] . '</td>
        <td>' . $datosCredito["numeroCredito"] . '</td>
        <td>$' . number_format($datosCredito["monto"], 2, '.', ',') . '</td>
        <td>' . number_format($datosCredito["tasaInteres"], 2, '.', ',') . ' %</td>
        <td>' . $datosCredito["labelLineaFinanciera"] . '</td>
        ' . $cierreTabla . $aperturaTabla . '
        <th>Destino</th>
        <th>¿Crédito al vencimiento?</th>
        <th>Periodicidad de pago</th>
        <th>Plazo</th>
        <th>Unidad plazo</th>
        </tr>
        <tr>
        <td>' . $datosCredito["destino"] . '</td>
        <td>' . $labelVencimiento . '</td>
        <td>' . $datosCredito["labelPeriodicidad"] . '</td>
        <td>' . $datosCredito["plazo"] . '</td>
        <td>' . $datosCredito["labelTipoPlazo"] . '</td>
        ' . $cierreTabla . $aperturaTabla . '
        <th>Fecha de generación</th>
        <th>Fuente de fondos</th>
        <th>¿Empleado ACACYPAC?</th>
        <th>¿Línea rotativa?</th>
        <th>Fecha de vencimiento</th>
        </tr>
        <tr>
        <td>' . $datosCredito["fechaGeneracion"] . '</td>
        <td>' . $datosCredito["labelFuenteFondos"] . '</td>
        <td>' . $labelEmpleado . '</td>
        <td>' . $labelRotativa . '</td>
        <td>' . $datosCredito["fechaVencimiento"] . '</td>
        ' . $cierreTabla;

        $pdf->SetFont('dejavusans', 'B', 7.8);
        $pdf->Write(0, "Datos del crédito", '', 0, 'C', true, 0, false, false, 0);
        $pdf->SetFont('dejavusans', 'B', 7);

        $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:140px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblCredito, true, false, true, false, '');

        $pdf->SetFont('dejavusans', 'B', 7.8);
        $pdf->Write(0, "Definición de cuota", '', 0, 'C', true, 0, false, false, 0);
        $pdf->SetFont('dejavusans', 'B', 7);

        $totalCuota = 0;
        $totalCuota += floatval($datosCredito["capitalInteres"]);

        $tblCuota = $aperturaTabla . '<th>Capital e intereses</th>';

        $cntTd = 0;
        $arrMontos = [];
        $arrMontos[] = '<td>$' . number_format($datosCredito["capitalInteres"], 2, '.', ',') . '</td>';
        $cntTd++;
        foreach ($datosCredito["aditivos"] as $aditivo) {
            $cntTd++;
            $totalCuota += floatval($aditivo["monto"]);
            $tblCuota .= '<th>' . $aditivo["aditivo"] . '</th>';
            $arrMontos[] = '<td>$' . number_format($aditivo["monto"], 2, '.', ',') . '</td>';
            if ($cntTd % 5 == 0) {
                $tblCuota .= '</tr><tr>' . implode('', $arrMontos) . $cierreTabla;
                $arrMontos = [];

                count($datosCredito["aditivos"]) > $cntTd && ($tblCuota .= $aperturaTabla);
            }
        }

        if ($cntTd % 5 != 0) {
            $cntTd++;
            $arrMontos[] = '<td>$' . number_format($totalCuota, 2, '.', ',') . '</td>';
            $tblCuota .= '<th>Total de cuota</th>';
            if ($cntTd % 5 == 0) {
                $tblCuota .= '</tr><tr>' . implode('', $arrMontos) . $cierreTabla;
            }
        } else {
            $cntTd++;
            $arrMontos[] = '<td>$' . number_format($totalCuota, 2, '.', ',') . '</td>';
            $tblCuota .= $aperturaTabla . '<th>Total de cuota</th>';
        }

        if ($cntTd % 5 != 0) {
            $tblCuota .= '</tr><tr>' . implode("", $arrMontos) . $cierreTabla;
        }

        $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:140px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblCuota, true, false, true, false, '');

        if ($input["incluyeParciales"]) {
            $pdf->SetFont('dejavusans', 'B', 7.8);
            $pdf->Write(0, "Detalle de desembolsos parciales", '', 0, 'C', true, 0, false, false, 0);
            $pdf->SetFont('dejavusans', 'B', 7);

            $cntDesembolso = 0;
            foreach ($datosCredito["detalleDesembolsos"] as $desembolso) {
                $cntDesembolso++;

                $pdf->SetFont('dejavusans', 'B', 7.8);
                $pdf->Write(0, "Desembolso #" . $cntDesembolso, '', 0, 'C', true, 0, false, false, 0);
                $pdf->SetFont('dejavusans', 'B', 7);

                $tblDesembolso = $aperturaTabla . '
                <th>Fecha</th>
                <th>Monto</th>
                </tr>
                <tr>
                <td>' . $desembolso["fecha"] . '</td>
                <td>$' . number_format($desembolso["monto"], 2, '.', ',') . '</td>
                ' . $cierreTabla;

                $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:350px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblDesembolso, true, false, true, false, '');
            }
        }

        if ($input["incluyeFirmante"]) {
            $pdf->SetFont('dejavusans', 'B', 7.8);
            $pdf->Write(0, "Datos de firmante a ruego", '', 0, 'C', true, 0, false, false, 0);
            $pdf->SetFont('dejavusans', 'B', 7);
            $tblFirmante = $aperturaTabla . '
            <th>Tipo de documento</th>
            <th>Número de documento</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Conocido</th>
            </tr>
            <tr>
            <td>' . $datosFirmante["labelTipoDocumento"] . '</td>
            <td>' . $datosFirmante["numeroDocumento"] . '</td>
            <td>' . $datosFirmante["nombres"] . '</td>
            <td>' . $datosFirmante["apellidos"] . '</td>
            <td>' . $datosFirmante["conocido"] . '</td>
            ' . $cierreTabla . $aperturaTabla . '
            <th>País</th>
            <th>Departamento</th>
            <th>Municipio</th>
            <th>Profesión</th>
            <th>Fecha nacimiento / Edad</th>
            </tr>
            <tr>
            <td>' . $datosFirmante["labelPais"] . '</td>
            <td>' . $datosFirmante["labelDepartamento"] . '</td>
            <td>' . $datosFirmante["labelMunicipio"] . '</td>
            <td>' . $datosFirmante["profesion"] . '</td>
            <td>' . $datosFirmante["fechaNacimiento"] . ' / ' . $datosFirmante["edad"] . '</td>
            ' . $cierreTabla;

            $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:140px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblFirmante, true, false, true, false, '');
        }

        $pdf->SetFont('dejavusans', 'B', 7.8);
        $pdf->Write(0, "Garantías incluidas", '', 0, 'C', true, 0, false, false, 0);
        $pdf->SetFont('dejavusans', 'B', 7);

        $tblGarantias = '<table cellpadding="1" cellspacing="0" style="text-align:center;" nobr="true">';

        foreach ($garantias as $garantia) {
            $tblGarantias .= '<tr><td style="width:700px;">' . $garantia["tipoGarantia"] . '</td></tr>';
        }

        $tblGarantias .= '</table>';

        $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:140px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblGarantias, true, false, true, false, '');

        foreach ($garantias as $garantia) {
            if (count($garantia["configuracion"]) > 0) {
                switch ($garantia["idTipoGarantia"]) {
                    case "2":
                        $pdf->SetFont('dejavusans', 'B', 7.8);
                        $pdf->Write(0, "Parámetros de garantía aportaciones", '', 0, 'C', true, 0, false, false, 0);
                        $pdf->SetFont('dejavusans', 'B', 7);

                        $tblAportaciones = '<table cellpadding="1" cellspacing="0" style="text-align:center;" nobr="true"><tr><th>Número de cuenta</th><th>Porcentaje de cobertura</th></tr><tr><td>' . $garantia["configuracion"]["cuenta"] . '</td><td>' . $garantia["configuracion"]["porcentaje"] . ' %</td></tr></table>';

                        $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:350px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblAportaciones, true, false, true, false, '');
                        break;
                    case "3":
                        $pdf->SetFont('dejavusans', 'B', 7.8);
                        $pdf->Write(0, "Parámetros de garantía CDP", '', 0, 'C', true, 0, false, false, 0);
                        $pdf->SetFont('dejavusans', 'B', 7);

                        $tblCDP = '<table cellpadding="1" cellspacing="0" style="text-align:center;" nobr="true"><tr><th>Número de cuenta</th><th>Número de certificado</th><th>Monto de CDP</th><th>Porcentaje de cobertura</th></tr><tr><td>' . $garantia["configuracion"]["cuenta"] . '</td><td>' . $garantia["configuracion"]["certificado"] . '</td><td>$' . number_format($garantia["configuracion"]["monto"], 2, '.', ',') . '</td><td>' . $garantia["configuracion"]["porcentaje"] . ' %</td></tr></table><br /><br /><table cellpadding="1" cellspacing="0" style="text-align:center;" nobr="true"><tr><th>Fecha de creación</th><th>Fecha de vencimiento</th></tr><tr><td>' . $garantia["configuracion"]["fechaCreacion"] . '</td><td>' . $garantia["configuracion"]["fechaVencimiento"] . '</td></tr></table>';

                        $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:175px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblCDP, true, false, true, false, '');
                        break;
                    case "5":
                        $cntFiador = 0;
                        foreach ($garantia["configuracion"] as $key => $fiador) {
                            $cntFiador++;
                            $pdf->SetFont('dejavusans', 'B', 7.8);
                            $pdf->Write(0, "Fiador #" . $cntFiador, '', 0, 'C', true, 0, false, false, 0);
                            $pdf->SetFont('dejavusans', 'B', 7);

                            $tblFiador = $aperturaTabla . '
                            <th>Tipo de documento</th>
                            <th>Número de documento</th>
                            <th>NIT</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            </tr>
                            <tr>
                            <td>' . $fiador["labelTipoDocumento"] . '</td>
                            <td>' . $fiador["numeroDocumento"] . '</td>
                            <td>' . $fiador["nit"] . '</td>
                            <td>' . $fiador["nombres"] . '</td>
                            <td>' . $fiador["apellidos"] . '</td>
                            ' . $cierreTabla . $aperturaTabla . '
                            <th>Conocido por</th>
                            <th>Género</th>
                            <th>País</th>
                            <th>Departamento</th>
                            <th>Municipio</th>
                            </tr>
                            <tr>
                            <td>' . $fiador["conocido"] . '</td>
                            <td>' . $fiador["labelGenero"] . '</td>
                            <td>' . $fiador["labelPais"] . '</td>
                            <td>' . $fiador["labelDepartamento"] . '</td>
                            <td>' . $fiador["labelMunicipio"] . '</td>
                            ' . $cierreTabla . $aperturaTabla . '
                            <th>Profesión</th>
                            <th>Fecha de nacimiento</th>
                            <th>Edad</th>
                            <th>¿Sabe firmar?</th>
                            </tr>
                            <tr>
                            <td>' . $fiador["profesion"] . '</td>
                            <td>' . $fiador["fechaNacimiento"] . '</td>
                            <td>' . $fiador["edad"] . '</td>
                            <td>' . $fiador["labelFirma"] . '</td>
                            ' . $cierreTabla;
                            $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:140px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblFiador, true, false, true, false, '');
                        }
                        break;
                    case "6":
                        $cntHipoteca = 0;
                        foreach ($garantia["configuracion"] as $key => $hipoteca) {
                            $cntHipoteca++;
                            $pdf->SetFont('dejavusans', 'B', 7.8);
                            $pdf->Write(0, "Hipoteca #" . $cntHipoteca, '', 0, 'C', true, 0, false, false, 0);
                            $pdf->SetFont('dejavusans', 'B', 7);

                            $arrMatriculas = [];

                            foreach ($hipoteca["matriculas"] as $matricula) {
                                $arrMatriculas[] = $matricula["matricula"];
                            }

                            $tblHipoteca = $aperturaTabla . '
                            <th>Fecha de emisión</th>
                            <th>Abogado</th>
                            <th>Género</th>
                            <th>Asiento</th>
                            <th>Monto</th>
                            </tr>
                            <tr>
                            <td>' . $hipoteca["fechaEmision"] . '</td>
                            <td>' . $hipoteca["abogado"] . '</td>
                            <td>' . $hipoteca["labelGeneroAbogado"] . '</td>
                            <td>' . $hipoteca["asiento"] . '</td>
                            <td>$' . number_format($hipoteca["monto"], 2, '.', ',') . '</td>
                            ' . $cierreTabla . $aperturaTabla . '
                            <th>Plazo</th>
                            <th>Zona</th>
                            <th>Sección</th>
                            <th>Departamento</th>
                            <th>Presentada por</th>
                            </tr>
                            <tr>
                            <td>' . $hipoteca["plazo"] . ' años</td>
                            <td>' . $hipoteca["labelZona"] . '</td>
                            <td>' . $hipoteca["labelSeccion"] . '</td>
                            <td>' . $hipoteca["departamento"] . '</td>
                            <td>' . $hipoteca["presenta"] . '</td>
                            ' . $cierreTabla . $aperturaTabla . '
                            <th>Matrícula\'s</th>
                            </tr>
                            <tr>
                            <td>' . implode('<br />', $arrMatriculas) . '</td>
                            ' . $cierreTabla;

                            $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:140px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblHipoteca, true, false, true, false, '');

                            if (count($hipoteca["modificaciones"]) > 0) {
                                $cntModificacion = 0;

                                foreach ($hipoteca["modificaciones"] as $modificacion) {
                                    $cntModificacion++;
                                    $pdf->SetFont('dejavusans', 'B', 7.8);
                                    $pdf->Write(0, "Modificación #" . $cntModificacion . ' de hipoteca #' . $cntHipoteca, '', 0, 'C', true, 0, false, false, 0);
                                    $pdf->SetFont('dejavusans', 'B', 7);

                                    $ampliacionMonto = strlen($modificacion["monto"]) > 0 ? '$' . number_format($modificacion["monto"], 2, '.', ',') : '';
                                    $ampliacionPlazo = strlen($modificacion["plazo"]) > 0 ? $modificacion["plazo"] . ' años' : '';

                                    $tblModificacion = $aperturaTabla . '
                                    <th>Fecha de modificación</th>
                                    <th>Abogado</th>
                                    <th>Género</th>
                                    <th>Asiento</th>
                                    </tr>
                                    <tr>
                                    <td>' . $modificacion["fecha"] . '</td>
                                    <td>' . $modificacion["abogado"] . '</td>
                                    <td>' . $modificacion["labelGeneroAbogado"] . '</td>
                                    <td>' . $modificacion["asiento"] . '</td>
                                    ' . $cierreTabla . $aperturaTabla . '
                                    <th>Ampliación en monto</th>
                                    <th>Ampliación en plazo</th>
                                    </tr>
                                    <tr>
                                    <td>' . $ampliacionMonto . '</td>
                                    <td>' . $ampliacionPlazo . '</td>
                                    ' . $cierreTabla;

                                    $pdf->writeHTML('<style>table th{background-color:rgb(0,95,85); color:white; width:175px;border:1px solid black;} table td{border:1px solid black;}</style>' . $tblModificacion, true, false, true, false, '');
                                }
                            }
                        }
                        break;
                }
            }
        }


        $tblFirma = '<style>table{font-size:9.5px;}</style><br /><br /><br /><br /><br /><table cellspacing="0" cellpadding="0" border="0">
        <tr>
        <td style="width:420px; text-align:center;"></td>
        <td style="width:300px; text-align:center;">F. ___________________________________________</td>
        </tr>
        <tr>
        <td style="text-align:center; font-weight:bold;"></td>
        <td style="text-align:center; font-weight:bold;">Firma de autorización</td>
        </tr>
        <tr>
        <td style="text-align:center; font-weight:bold;"></td>
        <td style="text-align:center; font-weight:bold;">' . $_SESSION["index"]->agenciaActual->nombreAgencia . '</td>
        </tr>
        </table>';
        $pdf->writeHTML($tblFirma, true, false, true, false, '');

        // if (file_exists(__DIR__ . '\..\docs\HojaRevision\/' . $nombreArchivo)) {
        //     $cnt = 1;
        //     $nombreArchivo = 'HR' . $datosCredito["numeroCredito"] . '(' . $cnt . ').pdf';
        //     while (file_exists(__DIR__ . '\..\docs\HojaRevision\/' . $nombreArchivo)) {
        //         $cnt++;
        //         $nombreArchivo = 'HR' . $datosCredito["numeroCredito"] . '(' . $cnt . ').pdf';
        //     }
        // }
        $mypdf = __DIR__ . '\..\docs\HojaRevision\/' . $nombreArchivo;
        $pdf->Output($mypdf, 'F');

        $respuesta->{"respuesta"} = 'EXITO';
        $respuesta->{"nombreArchivo"} = $nombreArchivo;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
