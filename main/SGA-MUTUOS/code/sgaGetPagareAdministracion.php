<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET)) {
        include '../../code/connectionSqlServer.php';
        require_once './Models/pagare.php';

        $tipoApartado = $_GET["tipoApartado"];
        $idApartado = base64_decode(urldecode($_GET["idApartado"]));
        $idUsuario = $_SESSION["index"]->id;
        $agencia = $_GET["agencia"];
        $inicio = date('Y-m-d', strtotime(urldecode($_GET["inicio"])));
        $fin = date('Y-m-d', strtotime(urldecode($_GET["fin"])));

        $Pagare = new pagare();
        $tipoRol = getTipoRol($tipoApartado, $idApartado);

        $Pagares = $Pagare->getPagareAdministracion($tipoRol, $idUsuario, $agencia, $inicio, $fin);

        if (isset($Pagares["respuesta"])) {
            $respuesta->{"respuesta"} = $Pagares["respuesta"];
            isset($Pagares["resultados"]) && $respuesta->{"resultados"} = $Pagares["resultados"];
        } else {
            $respuesta->{"respuesta"} = "Error al conectar con la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = 'SESION';
}

echo json_encode($respuesta);
