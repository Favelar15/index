<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET)) {
        include '../../code/connectionSqlServer.php';

        $numeroCredito = urldecode($_GET["numeroCredito"]);
        $busquedaDocumento = [];
        if (!isset($_GET['pagare'])) {
            require_once './Models/resolucionClausula.php';
            $resolucionClausula = new resolucionClausula();
            $resolucionClausula->numeroCredito = $numeroCredito;

            $busquedaDocumento = $resolucionClausula->searchResolucion();
        } else {
            require_once './Models/pagare.php';
            $pagare = new pagare();
            $pagare->numeroCredito = $numeroCredito;

            $busquedaDocumento = $pagare->searchPagare();
        }

        if (count($busquedaDocumento) > 0) {
            $respuesta->{"respuesta"} = 'Generado';
        } else {
            $respuesta->{"respuesta"} = 'EXITO';
            require_once './Models/mutuo.php';

            $queryMutuo = 'SELECT top(1) id FROM sga_viewMutuosAdministracion where numeroCredito = :numeroCredito;';
            $resultMutuo = $conexion->prepare($queryMutuo, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultMutuo->bindParam(':numeroCredito', $numeroCredito, PDO::PARAM_STR);

            if ($resultMutuo->execute() && $resultMutuo->rowCount() > 0) {
                $data = $resultMutuo->fetchAll(PDO::FETCH_OBJ)[0];
                $idMutuo = $data->id;

                $mutuo = new mutuo();
                $mutuo->id = $idMutuo;
                $respuesta->{"datosMutuo"} = $mutuo->getMutuo();
                if (isset($respuesta->datosMutuo["datosCredito"])) {
                    $respuesta->datosMutuo["datosCredito"]->fechaGeneracion = date('d-m-Y', strtotime($respuesta->datosMutuo["datosCredito"]->fechaGeneracion));
                    !empty($respuesta->datosMutuo['datosCredito']->fechaVencimiento) && $respuesta->datosMutuo['datosCredito']->fechaVencimiento = date('d-m-Y', strtotime($respuesta->datosMutuo['datosCredito']->fechaVencimiento));
                    foreach ($respuesta->datosMutuo["datosCredito"]->aditivos as $key => $aditivo) {
                        $respuesta->datosMutuo["datosCredito"]->aditivos[$key]->id = 0;
                        $respuesta->datosMutuo["datosCredito"]->aditivos[$key]->idCredito = 0;
                    }

                    if (isset($respuesta->datosMutuo["datosCredito"]->parciales)) {
                        foreach ($respuesta->datosMutuo["datosCredito"]->parciales as $key => $parcial) {
                            $respuesta->datosMutuo["datosCredito"]->parciales[$key]->id = 0;
                            $respuesta->datosMutuo["datosCredito"]->parciales[$key]->idCredito = 0;
                        }
                    }
                }

                if (isset($respuesta->datosMutuo["datosDeudor"])) {
                    $respuesta->datosMutuo["datosDeudor"]->id = 0;
                    $respuesta->datosMutuo["datosDeudor"]->idCredito = 0;
                    $respuesta->datosMutuo['datosDeudor']->fechaNacimiento = date('d-m-Y', strtotime($respuesta->datosMutuo['datosDeudor']->fechaNacimiento));
                }

                if (isset($respuesta->datosMutuo["datosGarantias"])) {
                    foreach ($respuesta->datosMutuo["datosGarantias"] as $key => $garantia) {
                        $respuesta->datosMutuo["datosGarantias"][$key]->id = 0;
                        $respuesta->datosMutuo["datosGarantias"][$key]->idCredito = 0;
                    }
                }
            } else {
                $respuesta->{"datosMutuo"} = (object)[
                    'respuesta' => "SinRegistros"
                ];
            }
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = 'SESION';
}

echo json_encode($respuesta);
