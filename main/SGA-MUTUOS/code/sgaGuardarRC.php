<?php
require_once "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include '../../code/connectionSqlServer.php';
        require_once './Models/resolucionClausula.php';

        $idUsuario = $_SESSION["index"]->id;
        $tipoApartado = $input["tipoApartado"];
        $idApartado = base64_decode(urldecode($input["idApartado"]));

        $RC = new resolucionClausula();
        $RC->id = $input["id"];
        $RC->datosCredito = $input["datosCredito"];
        $RC->datosDeudor = $input["datosDeudor"];
        $RC->garantias = $input["garantias"];
        $RC->incluyeParciales = $input["incluyeParciales"];

        $guardar = $RC->saveResolucionClausula($idUsuario, $ipActual, $tipoApartado, $idApartado);

        $respuesta = $guardar;

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
