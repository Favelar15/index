<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET)) {
        include '../../code/connectionSqlServer.php';

        $datosMutuos = [];
        $datosPagares = [];

        $agencia = urldecode($_GET['agencia']);
        $inicio = date('Y-m-d', strtotime(urldecode($_GET['inicio'])));
        $fin = date('Y-m-d', strtotime(urldecode($_GET['fin'])));

        $condicion = $agencia == '0' ? '' : ' AND agencia = ' . $agencia;

        $queryMutuos = 'SELECT * FROM sga_viewReporteDetallado where (CAST(fechaDocumento as date) between :inicio and :fin)' . $condicion;
        $resultMutuos = $conexion->prepare($queryMutuos, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultMutuos->bindParam(':inicio', $inicio, PDO::PARAM_STR);
        $resultMutuos->bindParam(':fin', $fin, PDO::PARAM_STR);
        $resultMutuos->execute();

        $resultMutuos->rowCount() > 0 && $datosMutuos = $resultMutuos->fetchAll(PDO::FETCH_OBJ);

        foreach ($datosMutuos as $key => $registro) {
            $datosMutuos[$key]->monto = '$' . number_format($registro->monto, 2, '.', ',');
            $datosMutuos[$key]->fechaDocumento = date('d-m-Y h:i a', strtotime($registro->fechaDocumento));
            !empty($registro->fechaVencimiento) && $datosMutuos[$key]->fechaVencimiento = date('d-m-Y', strtotime($registro->fechaVencimiento));
        }

        $queryPagares = 'SELECT * FROM sga_viewReporteDetalladoPagares where (CAST(fechaGeneracion as date) between :inicio and :fin)' . $condicion;
        $resultPagares = $conexion->prepare($queryPagares, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultPagares->bindParam(':inicio', $inicio, PDO::PARAM_STR);
        $resultPagares->bindParam(':fin', $fin, PDO::PARAM_STR);
        $resultPagares->execute();

        $resultPagares->rowCount() > 0 && $datosPagares = $resultPagares->fetchAll(PDO::FETCH_OBJ);

        foreach ($datosPagares as $key => $registro) {
            $datosPagares[$key]->fechaGeneracion = date('d-m-Y', strtotime($registro->fechaGeneracion));
            !empty($registro->fechaVencimiento) && $datosPagares[$key]->fechaVencimiento = date('d-m-Y', strtotime($registro->fechaVencimiento));
            $datosPagares[$key]->monto = '$' . number_format($registro->monto, 2, '.', ',');
            $datosPagares[$key]->tasaInteres = number_format($registro->tasaInteres, 2, '.', ',') . '%';

            $arrFiadores = !empty($registro->fiadores) ? explode('@@@', $registro->fiadores) : [];
            $arrObjFiadores = [];
            foreach ($arrFiadores as $fiador) {
                $tmpFiador = (object)[];
                $valores = explode('%%%', $fiador);
                $tmpFiador->tipoDocumento = $valores[0];
                $tmpFiador->numeroDocumento = $valores[1];
                $tmpFiador->nit = $valores[2];
                $tmpFiador->nombreFiador = $valores[3];
                $tmpFiador->conocido = $valores[4] == 'null' ? null : $valores[4];
                $tmpFiador->pais = $valores[5];
                $tmpFiador->departamento = $valores[6];
                $tmpFiador->municipio = $valores[7];
                $tmpFiador->direccion = $valores[8];
                $tmpFiador->edad = $valores[9];

                $arrObjFiadores[] = $tmpFiador;
            }

            $datosPagares[$key]->fiadores = $arrObjFiadores;
        }

        $respuesta->{'respuesta'} = 'EXITO';
        $respuesta->{'datosMutuos'} = $datosMutuos;
        $respuesta->{'datosPagares'} = $datosPagares;

        $conexion = null;
    }
}

echo json_encode($respuesta);
