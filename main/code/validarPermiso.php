<?php
include "generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "connectionSqlServer.php";

        $valores = explode('@@@', $_GET["q"]);

        $idUsuario = $_SESSION["index"]->id;
        $tipo = $valores[0];
        $idApartado = base64_decode(urldecode($valores[1]));

        $response = '';
        $tipoPermiso = null;

        $query = "EXEC general_spValidarPermiso :usuario,:tipo,:idApartado,:response,:tipoPermiso;";
        $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipo', $tipo, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':tipoPermiso', $tipoPermiso, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        $result->execute();

        if ($response) {
            $respuesta->{'respuesta'} = $response;
            $respuesta->{'tipoPermiso'} = $tipoPermiso;
            if ($response == 'EXITO') {
                $responseUbicacion = null;
                $idIngreso = $_SESSION['index']->idIngreso;
                $queryUbicacion = 'EXEC general_spGuardarUbicacion :usuario,:ingreso,:tipoApartado,:idApartado,:response;';
                $resultUbicacion = $conexion->prepare($queryUbicacion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultUbicacion->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                $resultUbicacion->bindParam(':ingreso', $idIngreso, PDO::PARAM_INT);
                $resultUbicacion->bindParam(':tipoApartado', $tipo, PDO::PARAM_STR);
                $resultUbicacion->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                $resultUbicacion->bindParam(':response', $responseUbicacion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                $resultUbicacion->execute();
            }
        } else {
            $respuesta->{"respuesta"} = "Error en procedimiento validarPermiso";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
