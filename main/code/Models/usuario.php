<?php
class usuario
{
    public $id;
    public $username;
    public $nombres;
    public $apellidos;
    public $email;
    public $codigoAsignacion;
    public $foto;
    public $bloqueado;
    public $ultimoIngreso;
    public $fechaRegistro;
    public $roles;
    public $agencias;
    public $idAgenciaActual;
    protected $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        if (!empty($this->agencias)) {
            $agencias = new stdClass();

            $tmpAgencias = explode('@@@', $this->agencias);

            foreach ($tmpAgencias as $agencia) {
                $valores = explode("%%%", $agencia);
                $tmpAgencia = new stdClass();
                $tmpAgencia->id = $valores[0];
                $tmpAgencia->principal = $valores[1];

                $agencias->{$valores[0]} = $tmpAgencia;
            }

            $this->agencias = $agencias;
        } else {
            $this->agencias = new stdClass();
        }

        if (!empty($this->roles)) {
            $roles = [];

            $tmpRoles = explode('@@@', $this->roles);
            foreach ($tmpRoles as $rol) {
                $valores = explode("%%%", $rol);
                $tmpRol = new stdClass();
                $tmpRol->id = $valores[0];
                $tmpRol->tipo = $valores[1];
                array_push($roles, $tmpRol);
            }

            $this->roles = $roles;
        } else {
            $this->roles = [];
        }

        !empty($this->ultimoIngreso) && $this->ultimoIngreso = date('d-m-Y h:i a', strtotime($this->ultimoIngreso));
    }

    public function getUsers()
    {
        $query = "SELECT * FROM sistema_viewDatosUsuarios;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    public function getUsersCbo()
    {
        $query = "SELECT id,concat(nombres,' ',apellidos) as nombreCompleto FROM sistema_viewDatosUsuarios;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_OBJ);
        }

        $this->conexion = null;
        return [];
    }

    public function logIn($username, $password, $newPassword, $passwordChange, $ipActual)
    {
        $response = null;
        $id = null;
        $intentosRestantes = null;
        $sessionAnterior = null;
        $idIngreso = null;
        $changePassword = $passwordChange == true ? 'S' : 'N';

        $idSession = session_create_id('index,-');
        $segmentoRed = isset(explode(".", $ipActual)[2]) ? intval(explode(".", $ipActual)[2]) : 15;
        $query = "EXEC sistema_spValidarIngreso :username,:password,:newPassword,:ip,:session,:segmentoRed,:cambioContrasenia,:response,:id,:intentos,:sessionAnterior,:ingreso;";
        $result  = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':username', $username, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->bindParam(':newPassword', $newPassword, PDO::PARAM_STR);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':session', $idSession, PDO::PARAM_STR);
        $result->bindParam(':segmentoRed', $segmentoRed, PDO::PARAM_INT);
        $result->bindParam(':cambioContrasenia', $changePassword, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':id', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':intentos', $intentosRestantes, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':sessionAnterior', $sessionAnterior, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':ingreso', $idIngreso, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            if ($response && $response == "EXITO") {
                if (!empty($sessionAnterior)) {
                    session_id($sessionAnterior);
                    session_start();
                    if (isset($_SESSION["index"])) {
                        $_SESSION["index"]->locked = false;
                    }
                    session_commit();
                }

                $query = "SELECT top(1) * FROM sistema_viewDatosUsuarios WHERE id = :id";
                $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $result->bindParam(':id', $id, PDO::PARAM_INT);
                if ($result->execute()) {
                    $datos = $result->fetchAll(PDO::FETCH_CLASS, 'userlogged')[0];
                    $datos->idIngreso = $idIngreso;
                    $datos->locked = true;
                    session_id($idSession);
                    session_start();
                    $_SESSION["index"] = $datos;
                }
            }

            $this->conexion = null;
            return ["respuesta" => $response, "intentos" => $intentosRestantes, "sessionAnterior" => $sessionAnterior, "id" => $id, "idSession" => $idSession, "idIngreso" => $idIngreso];
        }

        $this->conexion = null;
        return ["respuesta" => "Error al intentar identificar al usuario"];
    }

    public function saveUsuario(string $password, string $extension)
    {
        $response = null;
        $nombreArchivo = null;

        $arrAgencia = [];

        $this->agencias = json_decode($this->agencias);
        $this->roles = json_decode($this->roles);

        foreach ($this->agencias as $agencia) {
            array_push($arrAgencia, $agencia->id . "%%%" . $agencia->principal);
        }

        $agencias = count($arrAgencia) > 0 ? implode("@@@", $arrAgencia) : '';
        $roles = count($this->roles) > 0 ? implode("@@@", $this->roles) : '';

        $query = "EXEC soporte_spGuardarUsuario :id,:nombres,:apellidos,:email,:username,:fotoUsuario,:extension,:contrasenia,:agencias,:roles,:response,:nombreArchivo;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':nombres', $this->nombres, PDO::PARAM_STR);
        $result->bindParam(':apellidos', $this->apellidos, PDO::PARAM_STR);
        $result->bindParam(':email', $this->email, PDO::PARAM_STR);
        $result->bindParam(':username', $this->username, PDO::PARAM_STR);
        $result->bindParam(':fotoUsuario', $this->foto, PDO::PARAM_STR);
        $result->bindParam(':extension', $extension, PDO::PARAM_STR);
        $result->bindParam(':contrasenia', $password, PDO::PARAM_STR);
        $result->bindParam(':agencias', $agencias, PDO::PARAM_STR);
        $result->bindParam(':roles', $roles, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':nombreArchivo', $nombreArchivo, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return ["respuesta" => $response, "nombreArchivo" => $nombreArchivo];
        }

        $this->conexion = null;
        return ["respuesta" => "Error en soporte_spGuardarUsuario"];
    }

    public function updateProfile(int $usuario,string $nombreImagen)
    {
        $query = "UPDATE sistema_datosUsuarios set foto = :imagen WHERE id = :id";
        $result = $this->conexion->prepare($query,[PDO::ATTR_CURSOR=>PDO::CURSOR_SCROLL]);
        $result->bindParam(':imagen',$nombreImagen,PDO::PARAM_STR);
        $result->bindParam(':id',$usuario,PDO::PARAM_INT);

        if($result->execute()){
            $this->conexion = null;
            return ["respuesta"=>"EXITO"];
        }
        $this->conexion = null;
        return ["respuesta"=>"Error al actualizar la base de datos"];
    }
}

class userlogged extends usuario
{
    public $agenciaActual;
    public $roles;
    public $agencias;
    function __construct()
    {
        if (!empty($this->agencias)) {
            $agencias = new stdClass();

            $tmpAgencias = explode('@@@', $this->agencias);

            foreach ($tmpAgencias as $agencia) {
                $valores = explode("%%%", $agencia);
                $tmpAgencia = new stdClass();
                $tmpAgencia->id = $valores[0];
                $tmpAgencia->principal = $valores[1];

                $agencias->{$valores[0]} = $tmpAgencia;
            }

            $this->agencias = $agencias;
        } else {
            $this->agencias = new stdClass();
        }

        if (!empty($this->roles)) {
            $roles = [];

            $tmpRoles = explode('@@@', $this->roles);
            foreach ($tmpRoles as $rol) {
                $valores = explode("%%%", $rol);
                $tmpRol = new stdClass();
                $tmpRol->id = $valores[0];
                $tmpRol->tipo = $valores[1];
                $tmpRol->label = $valores[2];
                array_push($roles, $tmpRol);
            }

            $this->roles = $roles;
        } else {
            $this->roles = [];
        }
    }
}
