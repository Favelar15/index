<?php
class geografico
{
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function obtenerGeograficos()
    {
        $response = (object)[];
        $query = "SELECT * FROM general_viewDatosGeograficos;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            while ($data = $result->fetch(PDO::FETCH_ASSOC)) {
                $identificador = $data["id"];
                $nombrePais = $data["nombrePais"];
                $identificadorDepartamento = $data["idDepartamento"];
                $nombreDepartamento = $data["nombreDepartamento"];
                $departamentos = [];
                $municipios = [];
                $tmpMunicipios = !empty($data["municipios"]) ? explode("@@@", $data["municipios"]) : [];

                if (!isset($response->$identificador)) {
                    $response->$identificador = new pais($identificador, $nombrePais, $departamentos);
                }

                foreach ($tmpMunicipios as $municipio) {
                    $datosMunicipio = explode("%%%", $municipio);
                    $identificadorMunicipio = $datosMunicipio[0];
                    $nombreMunicipio = $datosMunicipio[1];
                    $municipios[] = new municipio($identificadorMunicipio, $nombreMunicipio);
                }

                $response->$identificador->departamentos[] = new departamento($identificadorDepartamento, $nombreDepartamento, $municipios);
            }
        }

        $this->conexion = null;
        return $response;
    }
}

class pais
{
    public $id;
    public $nombrePais;
    public $departamentos;

    function __construct($id, $nombrePais, $departamentos)
    {
        $this->id = $id;
        $this->nombrePais = $nombrePais;
        $this->departamentos = $departamentos;
    }
}

class departamento
{
    public $id;
    public $nombreDepartamento;
    public $municipios;

    function __construct($id, $nombreDepartamento, $municipios)
    {
        $this->id = $id;
        $this->nombreDepartamento = $nombreDepartamento;
        $this->municipios = $municipios;
    }
}

class municipio
{
    public $id;
    public $nombreMunicipio;

    function __construct($id, $nombreMunicipio)
    {
        $this->id = $id;
        $this->nombreMunicipio = $nombreMunicipio;
    }
}
