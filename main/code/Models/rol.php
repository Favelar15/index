<?php
class rol
{
    public $id;
    public $rol;
    public $tipoRol;
    public $fechaRegistro;
    public $fechaActualizacion;
    public $habilitado;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
    }

    public function getRolesCbo()
    {
        $query = "SELECT id,rol,labelTipoRol FROM soporte_viewRoles where habilitado = 'S';";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_OBJ);
        }

        $this->conexion = null;
        return [];
    }

    public function getRoles()
    {
        $query = "SELECT * FROM soporte_viewRoles;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

    public function saveRol($idUsuario, $tipoApartado, $idApartado)
    {
        $response = null;
        $id = null;
        $fechaRegistro = null;
        $fechaActualizacion = null;
        $query = 'EXEC soporte_spGuardarRol :id,:rol,:tipoRol,:tipoApartado,:idApartado,:usuario,:response,:idRol,:fechaRegistro,:fechaActualizacion;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':rol', $this->rol, PDO::PARAM_STR);
        $result->bindParam(':tipoRol', $this->tipoRol, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idRol', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':fechaRegistro', $fechaRegistro, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            !empty($fechaRegistro) && $fechaRegistro = date('d-m-Y h:i a', strtotime($fechaRegistro));
            !empty($fechaActualizacion) && $fechaActualizacion = date('d-m-Y h:i a', strtotime($fechaActualizacion));

            return ["respuesta" => $response, "id" => $id, "fechaRegistro" => $fechaRegistro, "fechaActualizacion" => $fechaActualizacion];
        }

        $this->conexion = null;
        return ["respuesta" => "Error en soporte_spGuardarRol"];
    }
}
