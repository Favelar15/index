<?php
class actividadEconomica
{
    public $id;
    public $actividadEconomica;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function obtenerActividades()
    {
        $query = "SELECT * FROM general_viewActividadesEconomicas;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'actividadEconomica');
        }

        $this->conexion = null;
        return [];
    }
}
