<?php
class asociado
{
    public $codigoCliente;
    public $tipoDocumentoPrimario;
    public $numeroDocumentoPrimario;
    public $tipoDocumentoSecundario;
    public $numeroDocumentoSecundario;
    public $nombresAsociado;
    public $apellidosAsociado;
    public $fechaNacimiento;
    public $edad;
    public $telefonoFijo;
    public $telefonoMovil;
    public $genero;
    public $estadoCivil;
    public $profesion;
    public $pais;
    public $departamento;
    public $municipio;
    public $direccionCompleta;
    public $hojaLegal;
    public $actividadPrimaria;
    public $actividadSecundaria;
    public $agencia;
    public $email;
    public $fechaAfiliacion;
    public $usuario;
    public $nombreUsuario;
    public $deudaAportaciones;
    public $fechaActualizacion;
    public $habilitado;
    private $conexion;

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        !empty($this->fechaNacimiento) && $this->fechaNacimiento = date('d-m-Y', strtotime($this->fechaNacimiento));
        !empty($this->fechaAfiliacion) && $this->fechaAfiliacion = date('d-m-Y', strtotime($this->fechaAfiliacion));
        empty($this->deudaAportaciones) ? $this->deudaAportaciones = 0 : $this->deudaAportaciones  = sprintf('%0.2f', $this->deudaAportaciones );
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:m a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:m a', strtotime($this->fechaActualizacion));
    }

    public function buscarAsociado()
    {
        $query = "SELECT * FROM general_viewDatosAsociados WHERE codigoCliente = :codigoCliente AND habilitado = 'S';";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'asociado');
        }


        $this->conexion = null;
        return false;
    }

    function reporteRegistroAsociados( $agencias, $desde, $hasta )
    {
        $query = "SELECT * FROM general_viewLogsAsociados WHERE idAgencia IN (".implode(',', $agencias).") AND CAST(fechaRegistro AS DATE) BETWEEN '".$desde."' AND '".$hasta."'";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute())
        {
            $asociadosTemp = (Object)[];

            $asociados = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);

            foreach ( $asociados as $asociado )
            {
                if ( $asociado->descripcion == 'Registro de asociado' || ( $asociado->descripcion == 'Actualización de datos' && $asociado->afiliacion == 1) )
                {
                    $asociado->afiliacion = (int) $asociado->afiliacion;
                    if (property_exists($asociadosTemp, $asociado->codigoCliente ) )
                    {
                        $asociadosTemp->{$asociado->codigoCliente}->afiliacion += $asociado->afiliacion;

                    } else {
                        $asociadosTemp->{$asociado->codigoCliente} = $asociado;
                    }

                    $asociadosTemp->{$asociado->codigoCliente}->tipoAfiliacion = $asociadosTemp->{$asociado->codigoCliente}->afiliacion == 0 ? 'NUEVO' : 'REINGRESO';
                }
            }
            return $asociadosTemp;
        }

        return $query;
    }

    public function buscarAsociadoRegistroActualizacion()
    {
        $query = "SELECT * FROM general_viewDatosAsociados WHERE codigoCliente = :codigoCliente;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'asociado');
        }


        $this->conexion = null;
        return false;
    }
}