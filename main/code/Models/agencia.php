<?php
class agencia
{
    public $id;
    public $nombreAgencia;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function getAgenciasCbo($agencias = null)
    {
        $agenciasSolicitadas = [];
        if ($agencias != null) {
            foreach ($agencias as $agencia) {
                array_push($agenciasSolicitadas, $agencia->id);
            }
        }
        $condicion = count($agenciasSolicitadas) > 0 ? "id IN (" . implode(",", $agenciasSolicitadas) . ")" : "id>=700";
        $query = "SELECT id,nombreAgencia from general_viewDatosAgencias where " . $condicion . ";";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_OBJ);
        }

        $this->conexion = null;
        return [];
    }

    public function getJDAData()
    {
        $query = 'SELECT * FROM general_viewJDAData WHERE agencia = :agencia;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':agencia', $this->id, PDO::PARAM_INT);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_OBJ);
        }
        $this->conexion = null;
        return [];
    }
}
