<?php
class tipoDocumento
{
    public $id;
    public $tipoDocumento;
    public $mascara;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function obtenerTiposDocumento($tipo = 'all')
    {
        $query = "SELECT * FROM general_viewTiposDocumento;";
        if ($tipo == 'Adulto') {
            $query = "SELECT * FROM general_viewTiposDocumento WHERE tipoDocumento not like '%minoridad%';";
        }

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'tipoDocumento');
        }

        $this->conexion = null;
        return [];
    }
}
