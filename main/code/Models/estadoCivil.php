<?php
class estadoCivil
{
    public $id;
    public $estadoCivil;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function obtenerEstados()
    {
        $query = "SELECT * FROM general_viewEstadosCiviles;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'estadoCivil');
        }

        $this->conexion = null;
        return [];
    }
}
