<?php
include "generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "connectionSqlServer.php";
        require_once __DIR__ . '/../SOPORTE/code/Models/modulo.php';

        $idUsuario = $_SESSION["index"]->id;
        $rolesUsuario = [];

        foreach ($_SESSION["index"]->roles as $rol) {
            array_push($rolesUsuario, $rol->id);
        }

        $modulo = new modulo();
        $modulos = $modulo->getModulos();

        $menu = [];
        foreach ($modulos as $modulo) {
            $incluirModulo = false;
            $menuApartados = [];

            foreach ($modulo->{"apartados"} as $key => $apartado) {
                $incluirApartado = false;
                $menuSubApartados = [];

                foreach ($apartado->subApartados as $key2 => $subApartado) {
                    $incluirSub = false;
                    foreach ($subApartado->permisos as $permiso) {
                        if ($permiso->grupo == "USER") {
                            if ($permiso->asignacion == $idUsuario) {
                                $incluirSub = true;
                            }
                        } else {
                            if (in_array($permiso->asignacion, $rolesUsuario)) {
                                $incluirSub = true;
                            }
                        }
                    }

                    $subApartado->mantenimiento == 'S' && $incluirSub = false;

                    if (!$incluirSub) {
                        unset($modulo->apartados[$key]->subApartados->$key2);
                    } else {
                        $incluirApartado = true;
                        $menuSubApartados[] = $modulo->apartados[$key]->subApartados->$key2;
                    }
                }


                if (!$incluirApartado) {
                    foreach ($apartado->{"permisos"} as $permiso) {
                        if ($permiso->grupo == "USER") {
                            if ($permiso->asignacion == $idUsuario) {
                                $incluirApartado = true;
                            }
                        } else {
                            if (in_array($permiso->asignacion, $rolesUsuario)) {
                                $incluirApartado = true;
                            }
                        }
                    }
                }

                if ($apartado->mantenimiento == 'S') {
                    $incluirApartado = false;
                }

                if ($incluirApartado) {
                    $incluirModulo = true;
                    $modulo->apartados[$key]->subApartados = $menuSubApartados;
                    $menuApartados[] = $modulo->apartados[$key];
                } else {
                    unset($modulo->apartados[$key]);
                }
            }

            $modulo->apartados = $menuApartados;

            $incluirModulo && ($menu[] = $modulo);
        }


        $respuesta->{"menu"} = $menu;

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
