<?php
define('DURACION_SESION', '43200'); //12 horas
ini_set("session.cookie_lifetime", DURACION_SESION);
ini_set("session.gc_maxlifetime", DURACION_SESION);
ini_set('session.save_path', realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/tmp'));
session_cache_expire(DURACION_SESION);
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

if (count($input) > 0) {
    include "connectionSqlServer.php";
    include "generalParameters.php";

    session_start();
    if (isset($_SESSION["index"])) {
        $idUsuario = $_SESSION["index"]->id;
        $idIngreso = isset($_SESSION["index"]->idIngreso) ? $_SESSION["index"]->idIngreso : 0;
        $response = null;

        $query = "EXEC sistema_spCerrarSesion :identificadorUsuario,:identificadorIngreso,:response;";
        $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':identificadorUsuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':identificadorIngreso', $idIngreso, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        $result->execute();

        if ($response) {
            $respuesta->{"respuesta"} = $response;
            if ($response == "EXITO") {
                unset($_SESSION["index"]);
            }
        } else {
            $respuesta->{"respuesta"} = "Error en sistema_spCerrarSesion";
        }
    } else {
        $respuesta->{"respuesta"} = "EXITO";
    }

    $conexion = null;
}

echo json_encode($respuesta);
