<?php
define('DURACION_SESION', '43200'); //12 horas
ini_set("session.cookie_lifetime", DURACION_SESION);
ini_set("session.gc_maxlifetime", DURACION_SESION);
ini_set('session.save_path', realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/tmp'));
session_cache_expire(DURACION_SESION);

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

if (count($input) > 0) {
    include './connectionSqlServer.php';
    include './generalParameters.php';


    $usuario = new usuario();

    $username = $input["username"];
    $password =  hash('sha256', $input["password"]);
    $newPassword = isset($input["newPassword"]) ?  hash('sha256', $input["newPassword"]) : '';
    $passwordChange = isset($input["passwordChange"]) ? $input["passwordChange"] : false;

    $logIn = $usuario->logIn($username, $password, $newPassword, $passwordChange, $ipActual);
    if (isset($logIn["respuesta"])) {
        $respuesta->{"respuesta"} = $logIn["respuesta"];
        isset($logIn["id"]) && $respuesta->{"id"} = $logIn["id"];
        if ($logIn["respuesta"] == "EXITO") {
            $queryAgencia = "SELECT TOP(1) id,nombreAgencia,nombreJefe,email,idPais,pais,idDepartamento,departamento,idMunicipio,municipio FROM general_viewDatosAgencias WHERE id = :id;";
            $resultAgencia = $conexion->prepare($queryAgencia, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultAgencia->bindParam(':id', $_SESSION["index"]->idAgenciaActual, PDO::PARAM_INT);
            if ($resultAgencia->execute()) {
                $tmpAgencia = $resultAgencia->fetchAll(PDO::FETCH_OBJ)[0];
                $_SESSION["index"]->agenciaActual = $tmpAgencia;
            }

            $roles = [];

            foreach ($_SESSION["index"]->roles as $rol) {
                $roles[] = $rol->id;
            }
            $respuesta->{"roles"} = $roles;
        }
        isset($logIn["intentos"]) && $respuesta->{"intentos"} = $logIn["intentos"];
    } else {
        $respuesta->{"respuesta"} = "Error de comunicación con la base de datos";
    }

    $conexion = null;
}

echo json_encode($respuesta);
