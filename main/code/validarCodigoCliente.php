<?php
include "generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];
$respuesta->{'respuesta'} = false;

session_start();

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {
    if (!empty($_GET) && isset($_GET['q'])) {
        include 'connectionSqlServer.php';
        require_once './Models/asociado.php';

        $codigoCliente = (int) $_GET['q'];
        $agenciaActual = $_SESSION["index"]->agenciaActual->id;
        $asociado = new asociado();
        $asociado->codigoCliente = $codigoCliente;
        $datosAsociado = $asociado->buscarAsociado();

        count($datosAsociado) > 0 && $respuesta->{"respuesta"} = true;
        $respuesta->{"agenciaActual"} = $agenciaActual;
        $respuesta->{"asociado"} = count($datosAsociado) > 0 ? $datosAsociado[0] : [];
        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
