<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

if (count($input) > 0) {
    include "connectionSqlServer.php";
    include "generalParameters.php";
    session_start();

    $username = $input["username"];
    $codigoVerificacion = $input["codigoVerificacion"];
    $contraseniaTemporal = generateRandomString();

    if (isset($_SESSION["captcha"])) {
        if ($codigoVerificacion == $_SESSION["captcha"]) {
            $response = null;
            $idSolicitud = null;
            $nombreCompleto = null;
            $email = null;
            $query = "EXEC sistema_spSolicitudRestablecimiento :username,:temporal,:ip,:response,:id,:nombreCompleto,:email;";
            $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $result->bindParam(':username', $username, PDO::PARAM_STR);
            $result->bindParam(':temporal', $contraseniaTemporal, PDO::PARAM_STR);
            $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
            $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
            $result->bindParam(':id', $idSolicitud, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
            $result->bindParam(':nombreCompleto', $nombreCompleto, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
            $result->bindParam(':email', $email, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

            $result->execute();

            if ($response) {
                $respuesta->{"id"} = $idSolicitud;
                $respuesta->{"respuesta"} = $response;
                if ($response == "EXITO") {
                    $receiper = $email;
                    $respuesta->{"email"} = $receiper;
                    $nombreUsuario = $nombreCompleto;
                    $tags = "Recuperación de contraseña";
                    $htmlEmail = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                    
                    <head>
                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                        <meta name="viewport" content="width=device-width, initial-scale=1" />
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <meta name="x-apple-disable-message-reformatting" />
                        <meta name="apple-mobile-web-app-capable" content="yes" />
                        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
                        <meta name="format-detection" content="telephone=no" />
                        <title></title>
                        <style type="text/css">
                            /* Resets */
                            .ReadMsgBody {
                                width: 100%;
                                background-color: #ebebeb;
                            }
                    
                            .ExternalClass {
                                width: 100%;
                                background-color: #ebebeb;
                            }
                    
                            .ExternalClass,
                            .ExternalClass p,
                            .ExternalClass span,
                            .ExternalClass font,
                            .ExternalClass td,
                            .ExternalClass div {
                                line-height: 100%;
                            }
                    
                            a[x-apple-data-detectors] {
                                color: inherit !important;
                                text-decoration: none !important;
                                font-size: inherit !important;
                                font-family: inherit !important;
                                font-weight: inherit !important;
                                line-height: inherit !important;
                            }
                    
                            .restore-code {
                                height: 30px;
                                padding: 5px !important;
                                background-color: #004d47;
                                border: 1px solid black;
                                max-width: 100px;
                                display: flex;
                                align-items: center;
                                align-content: center;
                                justify-content: center;
                                border-radius: .5em;
                                text-align:center !important;
                            }
                    
                            .restore-code span {
                                margin-top: 5px !important;
                                color: white;
                                font-weight: bold;
                                font-size: 25px;
                                line-height: 30px;
                                max-width: 100px;
                                text-align:center !important;
                            }
                    
                            body {
                                -webkit-text-size-adjust: none;
                                -ms-text-size-adjust: none;
                            }
                    
                            body {
                                margin: 0;
                                padding: 0;
                            }
                    
                            .yshortcuts a {
                                border-bottom: none !important;
                            }
                    
                            .rnb-del-min-width {
                                min-width: 0 !important;
                            }
                    
                            /* Add new outlook css start */
                            .templateContainer {
                                max-width: 590px !important;
                                width: auto !important;
                            }
                    
                            /* Add new outlook css end */
                            /* Image width by default for 3 columns */
                            img[class="rnb-col-3-img"] {
                                max-width: 170px;
                            }
                    
                            /* Image width by default for 2 columns */
                            img[class="rnb-col-2-img"] {
                                max-width: 264px;
                            }
                    
                            /* Image width by default for 2 columns aside small size */
                            img[class="rnb-col-2-img-side-xs"] {
                                max-width: 180px;
                            }
                    
                            /* Image width by default for 2 columns aside big size */
                            img[class="rnb-col-2-img-side-xl"] {
                                max-width: 350px;
                            }
                    
                            /* Image width by default for 1 column */
                            img[class="rnb-col-1-img"] {
                                max-width: 550px;
                            }
                    
                            /* Image width by default for header */
                            img[class="rnb-header-img"] {
                                max-width: 590px;
                            }
                    
                            /* Ckeditor line-height spacing */
                            .rnb-force-col p,
                            ul,
                            ol {
                                margin: 0px !important;
                            }
                    
                            .rnb-del-min-width p,
                            ul,
                            ol {
                                margin: 0px !important;
                            }
                    
                            /* tmpl-2 preview */
                            .rnb-tmpl-width {
                                width: 100% !important;
                            }
                    
                            /* tmpl-11 preview */
                            .rnb-social-width {
                                padding-right: 15px !important;
                            }
                    
                            /* tmpl-11 preview */
                            .rnb-social-align {
                                float: right !important;
                            }
                    
                            /* Ul Li outlook extra spacing fix */
                            li {
                                mso-margin-top-alt: 0;
                                mso-margin-bottom-alt: 0;
                            }
                    
                            /* Outlook fix */
                            table {
                                mso-table-lspace: 0pt;
                                mso-table-rspace: 0pt;
                            }
                    
                            /* Outlook fix */
                            table,
                            tr,
                            td {
                                border-collapse: collapse;
                            }
                    
                            /* Outlook fix */
                            p,
                            a,
                            li,
                            blockquote {
                                mso-line-height-rule: exactly;
                            }
                    
                            /* Outlook fix */
                            .msib-right-img {
                                mso-padding-alt: 0 !important;
                            }
                    
                            /* Fix text line height on preview */
                            .content-spacing div {
                                line-height: 24px;
                            }
                    
                            @media only screen and (min-width:590px) {
                    
                                /* mac fix width */
                                .templateContainer {
                                    width: 590px !important;
                                }
                            }
                    
                            @media screen and (max-width: 360px) {
                    
                                /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
                                .rnb-yahoo-width {
                                    width: 360px !important;
                                }
                            }
                    
                            @media screen and (max-width: 380px) {
                    
                                /* fix width and font size "tmpl-4 tmpl-6" in mobile preview */
                                .element-img-text {
                                    font-size: 24px !important;
                                }
                    
                                .element-img-text2 {
                                    width: 230px !important;
                                }
                    
                                .content-img-text-tmpl-6 {
                                    font-size: 24px !important;
                                }
                    
                                .content-img-text2-tmpl-6 {
                                    width: 220px !important;
                                }
                            }
                    
                            @media screen and (max-width: 480px) {
                                td[class="rnb-container-padding"] {
                                    padding-left: 10px !important;
                                    padding-right: 10px !important;
                                }
                    
                                /* force container nav to (horizontal) blocks */
                                td.rnb-force-nav {
                                    display: inherit;
                                }
                    
                                /* fix text alignment "tmpl-11" in mobile preview */
                                .rnb-social-text-left {
                                    width: 100%;
                                    text-align: center;
                                    margin-bottom: 15px;
                                }
                    
                                .rnb-social-text-right {
                                    width: 100%;
                                    text-align: center;
                                }
                            }
                    
                            @media only screen and (max-width: 600px) {
                    
                                /* center the address &amp; social icons */
                                .rnb-text-center {
                                    text-align: center !important;
                                }
                    
                                /* force container columns to (horizontal) blocks */
                                th.rnb-force-col {
                                    display: block;
                                    padding-right: 0 !important;
                                    padding-left: 0 !important;
                                    width: 100%;
                                }
                    
                                table.rnb-container {
                                    width: 100% !important;
                                }
                    
                                table.rnb-btn-col-content {
                                    width: 100% !important;
                                }
                    
                                table.rnb-col-3 {
                                    /* unset table align="left/right" */
                                    float: none !important;
                                    width: 100% !important;
                                    /* change left/right padding and margins to top/bottom ones */
                                    margin-bottom: 10px;
                                    padding-bottom: 10px;
                                    /*border-bottom: 1px solid #eee;*/
                                }
                    
                                table.rnb-last-col-3 {
                                    /* unset table align="left/right" */
                                    float: none !important;
                                    width: 100% !important;
                                }
                    
                                table.rnb-col-2 {
                                    /* unset table align="left/right" */
                                    float: none !important;
                                    width: 100% !important;
                                    /* change left/right padding and margins to top/bottom ones */
                                    margin-bottom: 10px;
                                    padding-bottom: 10px;
                                    /*border-bottom: 1px solid #eee;*/
                                }
                    
                                table.rnb-col-2-noborder-onright {
                                    /* unset table align="left/right" */
                                    float: none !important;
                                    width: 100% !important;
                                    /* change left/right padding and margins to top/bottom ones */
                                    margin-bottom: 10px;
                                    padding-bottom: 10px;
                                }
                    
                                table.rnb-col-2-noborder-onleft {
                                    /* unset table align="left/right" */
                                    float: none !important;
                                    width: 100% !important;
                                    /* change left/right padding and margins to top/bottom ones */
                                    margin-top: 10px;
                                    padding-top: 10px;
                                }
                    
                                table.rnb-last-col-2 {
                                    /* unset table align="left/right" */
                                    float: none !important;
                                    width: 100% !important;
                                }
                    
                                table.rnb-col-1 {
                                    /* unset table align="left/right" */
                                    float: none !important;
                                    width: 100% !important;
                                }
                    
                                img.rnb-col-3-img {
                                    /**max-width:none !important;**/
                                    width: 100% !important;
                                }
                    
                                img.rnb-col-2-img {
                                    /**max-width:none !important;**/
                                    width: 100% !important;
                                }
                    
                                img.rnb-col-2-img-side-xs {
                                    /**max-width:none !important;**/
                                    width: 100% !important;
                                }
                    
                                img.rnb-col-2-img-side-xl {
                                    /**max-width:none !important;**/
                                    width: 100% !important;
                                }
                    
                                img.rnb-col-1-img {
                                    /**max-width:none !important;**/
                                    width: 100% !important;
                                }
                    
                                img.rnb-header-img {
                                    /**max-width:none !important;**/
                                    width: 100% !important;
                                    margin: 0 auto;
                                }
                    
                                img.rnb-logo-img {
                                    /**max-width:none !important;**/
                                    width: 100% !important;
                                }
                    
                                td.rnb-mbl-float-none {
                                    float: inherit !important;
                                }
                    
                                .img-block-center {
                                    text-align: center !important;
                                }
                    
                                .logo-img-center {
                                    float: inherit !important;
                                }
                    
                                /* tmpl-11 preview */
                                .rnb-social-align {
                                    margin: 0 auto !important;
                                    float: inherit !important;
                                }
                    
                                /* tmpl-11 preview */
                                .rnb-social-center {
                                    display: inline-block;
                                }
                    
                                /* tmpl-11 preview */
                                .social-text-spacing {
                                    margin-bottom: 0px !important;
                                    padding-bottom: 0px !important;
                                }
                    
                                /* tmpl-11 preview */
                                .social-text-spacing2 {
                                    padding-top: 15px !important;
                                }
                    
                                /* UL bullet fixed in outlook */
                                ul {
                                    mso-special-format: bullet;
                                }
                            }
                    
                        </style>
                        <style type="text/css">
                            table {
                                border-spacing: 0;
                            }
                    
                            table td {
                                border-collapse: collapse;
                            }
                    
                        </style>
                    </head>
                    
                    <body>
                        <table class="main-template" style="background-color: rgb(249, 250, 252);" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#f9fafc" align="center">
                            <tbody>
                                <tr>
                                    <td valign="top" align="center">
                                        <table class="templateContainer" style="max-width:590px!important; width: 590px;" width="590" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <table class="rnb-del-min-width" style="min-width:590px;" name="Layout_0" id="Layout_0" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="rnb-del-min-width" style="min-width:590px;" valign="top" align="center">
                                                                        <a href="#" name="Layout_0"></a>
                                                                        <table width="100%" height="38" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" height="38">
                                                                                        <img style="display:block; max-height:38px; max-width:20px;" alt="" src="https://img.mailinblue.com/new_images/rnb/rnb_space.gif" width="20" height="38">
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                                                            <table class="rnb-del-min-width" style="min-width:590px;" name="Layout_1" id="Layout_1" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="rnb-del-min-width" style="min-width:590px;" valign="top" align="center">
                                                                            <a href="#" name="Layout_1"></a>
                                                                            <table class="rnb-container" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="font-size:1px; line-height:0px; mso-hide: all;" height="20">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="rnb-container-padding" valign="top" align="left">
                                                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td valign="top" align="center">
                                                                                                            <table class="logo-img-center" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="line-height: 1px;" valign="middle" align="center">
                                                                                                                            <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block; " cellspacing="0" cellpadding="0" border="0">
                                                                                                                                <div><a style="text-decoration:none;" target="_blank" href="https://acacypac.coop"><img alt="ACACYPAC NC de RL" style="float: left;max-width:100px;" class="rnb-logo-img" src="https://img.mailinblue.com/3047110/images/rnb/original/5f4419b0470ed06e854843cd.png" width="100" vspace="0" hspace="0" border="0"></a></div>
                                                                                                                            </div>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-size:1px; line-height:0px; mso-hide: all;" height="5">&nbsp;</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                                                            <table name="Layout_2" id="Layout_2" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top" align="center"><a href="#" name="Layout_2"></a>
                                                                            <table class="rnb-container" style="height: 0px; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">
                    
                                                                                            <table class="rnb-columns-container" style="margin:auto;" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                <tbody>
                                                                                                    <tr>
                    
                                                                                                        <th class="rnb-force-col" style="text-align: center; font-weight: normal" align="center">
                    
                                                                                                            <table class="rnb-col-1" cellspacing="0" cellpadding="0" border="0" align="center">
                    
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td height="10"></td>
                                                                                                                    </tr>
                    
                                                                                                                    <tr>
                                                                                                                        <td class="content-spacing" style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">
                    
                                                                                                                            <span style="color:#3c4858;"><strong><span style="font-size:24px;">INDEX DE APLICACIONES</span></strong></span>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td height="10"></td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </th>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                    
                                                                                </tbody>
                                                                            </table>
                    
                                                                        </td>
                                                                    </tr>
                    
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                                                            <table name="Layout_10" id="Layout_10" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top" align="center"><a href="#" name="Layout_10"></a>
                                                                            <table class="rnb-container" style="height: 0px; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">
                    
                                                                                            <table class="rnb-columns-container" style="margin:auto;" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                <tbody>
                                                                                                    <tr>
                    
                                                                                                        <th class="rnb-force-col" style="text-align: center; font-weight: normal" align="center">
                    
                                                                                                            <table class="rnb-col-1" cellspacing="0" cellpadding="0" border="0" align="center">
                    
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td height="10"></td>
                                                                                                                    </tr>
                    
                                                                                                                    <tr>
                                                                                                                        <td class="content-spacing" style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">
                    
                                                                                                                            <span style="color:#3c4858;"><span style="font-size:19px;"><strong>Solicitud de restablecimiento de contraseña.</strong></span></span>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td height="10"></td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </th>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                    
                                                                                </tbody>
                                                                            </table>
                    
                                                                        </td>
                                                                    </tr>
                    
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <div style="background-color: transparent; border-radius: 0px;">
                                                            <table class="rnb-del-min-width" style="min-width:100%;" name="Layout_8" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="rnb-del-min-width" valign="top" align="center">
                                                                            <a href="#" name="Layout_8"></a>
                                                                            <table class="rnb-container" style="background-color: transparent; padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="transparent">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="font-size:1px; line-height:0px; mso-hide: all;" height="20">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="rnb-container-padding" valign="top" align="left">
                    
                                                                                            <table class="rnb-columns-container" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="rnb-force-col" style="text-align: left; font-weight: normal; padding-right: 0px;" valign="top">
                    
                                                                                                            <table valign="top" class="rnb-col-1" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td class="content-spacing" style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                                                                                            <div>&nbsp;<br>
                                                                                                                                Hola: ' . $nombreUsuario . '<br>
                                                                                                                                Hemos recibido una nueva solicitud de restablecimiento de contraseña para tu usuario.<br>
                                                                                                                                <br>
                                                                                                                                El siguiente código servirá como contraseña temporal para poder acceder al sistema.
                                                                                                                            </div>
                    
                                                                                                                            <center><br><br>
                                                                                                                                <div class="restore-code" style="text-align:center;">
                                                                                                                                 <span>' . $contraseniaTemporal . '</span>
                                                                                                                                </div>
                                                                                                                            </center>
                                                                                                                            <br>
                                                                                                                            <div>&nbsp;</div>
                    
                                                                                                                            <div><br>
                                                                                                                                Felíz día</div>
                                                                                                                            <ul>
                                                                                                                            </ul>
                                                                                                                            <div><strong>&nbsp;</strong></div>
                                                                                                                            <div>&nbsp;</div>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </th>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-size:1px; line-height:0px; mso-hide: all;" height="5">&nbsp;</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <table class="rnb-del-min-width" style="min-width:590px;" name="Layout_3164" id="Layout_3164" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="rnb-del-min-width" style="min-width:590px;" valign="top" align="center">
                                                                        <a href="#" name="Layout_3164"></a>
                                                                        <table width="100%" height="38" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" height="38">
                                                                                        <img style="display:block; max-height:38px; max-width:20px;" alt="" src="https://img.mailinblue.com/new_images/rnb/rnb_space.gif" width="20" height="38">
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <div style="background-color: rgb(249, 250, 252);">
                                                            <table class="rnb-del-min-width rnb-tmpl-width" style="min-width:590px;" name="Layout_5" id="Layout_5" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="rnb-del-min-width" style="min-width: 590px; background-color: rgb(249, 250, 252);" valign="top" bgcolor="#f9fafc" align="center">
                                                                            <a href="#" name="Layout_5"></a>
                                                                            <table class="rnb-container" width="590" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="font-size:1px; line-height:0px; mso-hide: all;" height="5">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="rnb-container-padding" style="font-size: 14px; font-family: Arial,Helvetica,sans-serif; color: #888888;" valign="top" align="left">
                                                                                            <table class="rnb-columns-container" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="rnb-force-col" style="padding-right:20px; padding-left:20px; mso-padding-alt: 0 0 0 20px; font-weight: normal;" valign="top">
                    
                                                                                                            <table valign="top" class="rnb-col-2 rnb-social-text-left" style="border-bottom:0;" width="264" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td valign="top">
                                                                                                                            <table class="rnb-btn-col-content" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif; color:#888888; line-height: 16px" class="rnb-text-center" valign="middle" align="left">
                                                                                                                                            <div>
                                                                                                                                                <div><strong>ACACYPAC N.C. DE R.L.</strong><br>
                                                                                                                                                    Av. Prof. Silvestre de Jesús Díaz y 4ª calle oriente, barrio San José, Nueva Concepción, Chalatenango</div>
                                                                                                                                            </div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </th>
                                                                                                        <th ng-if="item.text.align==\'left\'" class="rnb-force-col rnb-social-width" style="mso-padding-alt: 0 20px 0 0; padding-right: 15px;" valign="top">
                                                                                                            <table valign="top" class="rnb-last-col-2" width="246" cellspacing="0" cellpadding="0" border="0" align="right">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td valign="top">
                                                                                                                            <table class="rnb-social-align" style="float: right;" cellspacing="0" cellpadding="0" border="0" align="right">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td class="rnb-text-center" ng-init="width=setSocialIconsBlockWidth(item)" width="125" valign="middle" align="right">
                                                                                                                                            <div class="rnb-social-center">
                                                                                                                                                <table style="float:left; display: inline-block" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                                    <tbody>
                                                                                                                                                        <tr>
                                                                                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 2px 5px 0px;" align="left">
                                                                                                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                                                                                                    <a target="_blank" href="https://www.facebook.com/AcacypacDeRl/"><img alt="Facebook" style="vertical-align:top;" target="_blank" src="https://img.mailinblue.com/new_images/rnb/theme5/rnb_ico_fb.png" vspace="0" hspace="0" border="0"></a></span>
                                                                                                                                                            </td>
                                                                                                                                                        </tr>
                                                                                                                                                    </tbody>
                                                                                                                                                </table>
                                                                                                                                            </div>
                                                                                                                                            <div class="rnb-social-center">
                                                                                                                                                <table style="float:left; display: inline-block" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                                    <tbody>
                                                                                                                                                        <tr>
                                                                                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 2px 5px 0px;" align="left">
                                                                                                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                                                                                                    <a target="_blank" href="https://twitter.com/acacypacderl?lang=es"><img alt="Twitter" style="vertical-align:top;" target="_blank" src="https://img.mailinblue.com/new_images/rnb/theme5/rnb_ico_tw.png" vspace="0" hspace="0" border="0"></a></span>
                                                                                                                                                            </td>
                                                                                                                                                        </tr>
                                                                                                                                                    </tbody>
                                                                                                                                                </table>
                                                                                                                                            </div>
                                                                                                                                            <div class="rnb-social-center">
                                                                                                                                                <table style="float:left; display: inline-block" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                                                                                                    <tbody>
                                                                                                                                                        <tr>
                                                                                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 2px 5px 0px;" align="left">
                                                                                                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                                                                                                    <a target="_blank" href="https://www.instagram.com/acacypacderl/"><img alt="Instagram" style="vertical-align:top;" target="_blank" src="https://img.mailinblue.com/new_images/rnb/theme5/rnb_ico_ig.png" vspace="0" hspace="0" border="0"></a></span>
                                                                                                                                                            </td>
                                                                                                                                                        </tr>
                                                                                                                                                    </tbody>
                                                                                                                                                </table>
                                                                                                                                            </div>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </th>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-size:1px; line-height:0px; mso-hide: all;" height="20">&nbsp;</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                    
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                    
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                    
                                                    <td valign="top" align="center">
                    
                                                        <div style="background-color: rgb(249, 250, 252);">
                    
                                                            <table class="rnb-del-min-width rnb-tmpl-width" style="min-width:590px;" name="Layout_7" id="Layout_7" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="rnb-del-min-width" style="min-width:590px;" valign="top" align="center">
                                                                            <a href="#" name="Layout_7"></a>
                                                                            <table style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#f9fafc" align="center">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="font-size:1px; line-height:0px; mso-hide: all;" height="20">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                                                                            <div>
                                                                                                <div>Departamento de Tecnología, Información y Conexión</div>
                    
                                                                                                <div>Copyright © Todos los Derechos Reservados<br>
                                                                                                    ACACYPAC N.C. de R.L. 2020</div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-size:1px; line-height:0px; mso-hide: all;" height="20">&nbsp;</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                    
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    
                    </body>
                    
                    </html>
                    ';
                    $message = json_encode($htmlEmail, JSON_HEX_QUOT | JSON_HEX_TAG);
                    $subject = "Solicitud de restablecimiento de contraseña";
                    $sender = 'index.support@acacypac.coop';
                    $errorEnvio = generalEnviarEmail($sender, $receiper, $tags, $message, $subject);

                    if ($errorEnvio) {
                        $respuesta->{"respuesta"} = $errorEnvio;
                    }
                }
            } else {
                $respuesta->{"respuesta"} = "Error en sistema_spSolicitudRestablecimiento";
            }
        } else {
            $respuesta->{"respuesta"} = "Incorrecto";
        }
    } else {
        $respuesta->{"respuesta"} = "Timeout";
    }

    $conexion = null;
}

echo json_encode($respuesta);
