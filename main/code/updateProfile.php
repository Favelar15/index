<?php
require_once './generalParameters.php';
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (!empty($_POST)) {
        $accion = $_POST["accion"];
        $tmpActual = pathinfo($_SESSION["index"]->foto);
        $extActual = $tmpActual['extension'];
        $db = false;

        $nombreImagen = $_SESSION["index"]->foto;
        $accion == 'delete' && $nombreImagen = 'avatar.jpg';

        if (!empty($_FILES)) {
            $tmpNueva = pathinfo($_FILES["imagen"]["name"]);
            $extNueva = $tmpNueva["extension"];

            $extActual != $extNueva && $db = true;
            $nombreImagen = $extActual != $extNueva ? 'u' . $_SESSION["index"]->id . '.' . $extNueva : 'u' . $_SESSION["index"]->id . '.' . $extActual;
            $db = true;
        }

        $accion == 'delete' && $db = true;

        $respuesta->{"respuesta"} = 'EXITO';

        if ($db) {
            include './connectionSqlServer.php';
            $usuario = new usuario();
            $update = $usuario->updateProfile($_SESSION["index"]->id, $nombreImagen);
            isset($update["respuesta"]) && $respuesta->{"respuesta"} = $update["respuesta"];
            $conexion = null;
        }

        if ($respuesta->{"respuesta"} == 'EXITO' && !empty($_FILES)) {
            if (file_exists('../img/users/' . $nombreImagen)) {
                unlink('../img/users/' . $nombreImagen);
            }

            move_uploaded_file($_FILES['imagen']["tmp_name"], '../img/users/' . $nombreImagen);
        }

        $respuesta->{"respuesta"} == "EXITO" && $_SESSION["index"]->foto = $nombreImagen;
    }
} else {
    $respuesta->{"respuesta"} = 'SESION';
}

echo json_encode($respuesta);
