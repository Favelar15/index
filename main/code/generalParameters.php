<?php
if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
} else {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';
}
date_default_timezone_set('America/Guatemala');
$arrMeses = array('', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
$ipActual = generalObtenerIp();

const clausulaPrivadaSeguro = " Las cuotas de seguro de vida para el pago de deuda y de otros seguros relacionados en el presente crédito podrán ser objeto de variación, de acuerdo a las políticas de cobro de las primas de seguros que al efecto la Compañía Aseguradora comunique a <b>La Cooperativa</b>, lo cual desde hoy quedo {enterado} y acepto <b>{titulo_deudor}. </b>";
const clausulaLegalSeguro = " Las cuotas de seguro de vida para el pago de deuda y de otros seguros relacionados en el presente crédito podrán ser objeto de variación, de acuerdo a las políticas de cobro de las primas de seguros que al efecto la Compañía Aseguradora comunique a <b>La Cooperativa</b>, lo cual desde hoy queda {enterado} y acepta <b>{titulo_deudor}</b>. ";

$periodicidadesPago = (object) array(
    "SEMANAL" => 1,
    "MENSUAL" => 1,
    "BIMENSUAL" => 2,
    "TRIMESTRAL" => 3,
    "SEMESTRAL" => 6,
    "ANUAL" => 12
);

function generalObtenerIp()
{
    if (isset($_SERVER["HTTP_CLIENT_IP"])) {

        return $_SERVER["HTTP_CLIENT_IP"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {

        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {

        return $_SERVER["HTTP_X_FORWARDED"];
    } elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {

        return $_SERVER["HTTP_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_FORWARDED"])) {

        return $_SERVER["HTTP_FORWARDED"];
    } else {

        return $_SERVER["REMOTE_ADDR"];
    }
}

function generateRandomString($length = 6)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generalEnviarEmail($sender, $receiper, $tags, $message, $subject)
{
    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => "https://api.sendinblue.com/v3/smtp/email",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\"sender\":{\"name\":\"ACACYPAC de R.L. \",\"email\":\"$sender\"},\"to\":[{\"email\":\"$receiper\"}],\"tags\":[\"$tags\"],\"htmlContent\":$message,\"subject\":\"$subject\"}",
        CURLOPT_HTTPHEADER => [
            "Accept: application/json",
            "Content-Type: application/json",
            "api-key: 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"
        ],
    ]);

    $response_curl = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    return $err;
}

function generalFechaLetras($dia, $mes, $anio)
{
    global $arrMeses;
    $mes = intval($mes);

    $anioTmp = NumeroALetras::convertir($anio);

    $anioLetras = rtrim($anioTmp);

    if ($dia == '31') {
        $diaLetras = 'treinta y uno';
    } else {
        if ($dia == '21') {
            $diaLetras = 'veintiuno';
        } else {
            if ($dia == 1) {
                $diaLetras = 'uno';
            } else {
                $diaLetras = NumeroALetras::convertir($dia);
            }
        }
    }

    $anioLetras = $anioLetras == 'DOS MIL VENTIUN' ? $anioLetras = 'DOS MIL VEINTIUNO' : $anioLetras;


    $final = trim(strtolower($diaLetras)) . ' de ' . $arrMeses[$mes] . ' del año ' . trim(strtolower($anioLetras));
    return $final;
}

function generalHoraLetras($hora)
{
    $horaLetras = "";
    if (strlen($hora) > 0) {
        $partesHora = explode(":", $hora);
        $minutosLetras = intval($partesHora[1]) > 0 ? "con " . strtolower(NumeroALetras::convertir($partesHora[1])) . " minutos" : "";
        $horaLetras = strtolower(NumeroALetras::convertir($partesHora[0])) . " horas " . $minutosLetras;
    }

    return $horaLetras;
}

function object_sorter($clave, $orden = null)
{
    return function ($a, $b) use ($clave, $orden) {
        $result =  ($orden == "DESC") ? strnatcmp($b->$clave, $a->$clave) :  strnatcmp($a->$clave, $b->$clave);
        return $result;
    };
}

function getTipoRol($tipoApartado, $idApartado)
{
    global $conexion;
    $vista = $tipoApartado == 'Sub' ? 'general_viewRolesSubApartados' : 'general_viewRolesApartados';
    $queryRoles = "SELECT top(1) * from " . $vista . " WHERE id = " . $idApartado;
    $resultRoles = $conexion->prepare($queryRoles, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
    $resultRoles->execute();
    $dataRoles = $resultRoles->fetch(PDO::FETCH_ASSOC);
    $roles = explode('@@@', $dataRoles["roles"]);
    $tipoRol = 'Local';

    foreach ($_SESSION["index"]->roles as $rol) {
        if (in_array($rol->id, $roles)) {
            if ($rol->tipo == 'Administrativo') {
                $tipoRol = 'Administrativo';
                break;
            }
        }
    }

    return $tipoRol;
}

function documentoALetras($arregloDocumento)
{
    $tmpContenedor = array();

    foreach ($arregloDocumento as $caracter) {
        $caracterConvertido = is_numeric($caracter) ? ($caracter > 1 ? NumeroALetras::convertir($caracter) : ($caracter > 0 ? "UNO" : "CERO")) : $caracter;
        array_push($tmpContenedor, $caracterConvertido);
    }

    $documentoConvertido = count($tmpContenedor) > 0 ? implode(" ", $tmpContenedor) : "";

    return $documentoConvertido;
}

function conversionDineroLetras($monto, $mayus = "N")
{
    $tmpMonto = explode(".", number_format($monto, 2, ".", ""));
    $montoConvertido = "";

    if (intval($tmpMonto[1]) == 0) {
        $montoConvertido = intval($tmpMonto[0]) > 1 ? NumeroALetras::convertir($tmpMonto[0]) . " DÓLARES DE LOS ESTADOS UNIDOS DE AMÉRICA" : NumeroALetras::convertir($tmpMonto[0]) . " DÓLAR DE LOS ESTADOS UNIDOS DE AMÉRICA";
    } else {
        $dolaresCompletos = intval($tmpMonto[0]) > 0 ? (intval($tmpMonto[0]) > 1 ? NumeroALetras::convertir($tmpMonto[0]) . " DÓLARES CON " : NumeroALetras::convertir($tmpMonto[0]) . " DÓLAR CON ") : "";
        $montoConvertido = $dolaresCompletos . NumeroALetras::convertir($tmpMonto[1]) . " CENTAVOS DE DÓLAR DE LOS ESTADOS UNIDOS DE AMÉRICA";
    }

    return $montoConvertido;
}

function conversionNumeroLetras($numero)
{
    $tmpMonto = explode(".", number_format($numero, 2, ".", ""));
    $montoConvertido = "";

    if (intval($tmpMonto[1]) == 0) {
        $montoConvertido = NumeroALetras::convertir($tmpMonto[0]);
    } else {
        $decimales = intval($tmpMonto[1]) > 1 ? NumeroALetras::convertir($tmpMonto[1]) : "UNO";
        $montoConvertido = NumeroALetras::convertir($tmpMonto[0]) . " PUNTO " . $decimales;
    }

    return $montoConvertido;
}

function aRomano($integer, $upcase = true)
{
    $table = array(
        'M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100,
        'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9,
        'V' => 5, 'IV' => 4, 'I' => 1
    );
    $return = '';
    while ($integer > 0) {
        foreach ($table as $rom => $arb) {
            if ($integer >= $arb) {
                $integer -= $arb;
                $return .= $rom;
                break;
            }
        }
    }
    return $return;
}

function sumarDiasHabiles($fecha, $dias)
{
    $datestart = strtotime($fecha);
    // $datesuma = 15 * 86400;
    $diasemana = date('N', $datestart);
    $totaldias = $diasemana + $dias;
    $findesemana = intval($totaldias / 5) * 2;
    $diasabado = $totaldias % 5;
    if ($diasabado == 6) $findesemana++;
    if ($diasabado == 0) $findesemana = $findesemana - 2;

    $total = (($dias + $findesemana) * 86400) + $datestart;
    return $fechafinal = date('Y-m-d', $total);
}

require_once __DIR__ . '/Models/usuario.php';
