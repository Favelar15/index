<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

if (count($input) > 0) {
    include "connectionSqlServer.php";
    include "generalParameters.php";

    $identificador = $input["identificador"];
    $temporal = $input["contraseniaTemporal"];

    $response = null;
    $contrasenia = null;
    $query = "EXEC sistema_spRestorePass :identificador,:temporal,:ip,:response,:pass;";
    $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
    $result->bindParam(':identificador', $identificador, PDO::PARAM_INT);
    $result->bindParam(':temporal', $temporal, PDO::PARAM_STR);
    $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
    $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
    $result->bindParam(':pass', $contrasenia, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

    $result->execute();

    if ($response) {
        $respuesta->{"respuesta"} = $response;
        $respuesta->{"contrasenia"} = $contrasenia;
    } else {
        $respuesta->{"respuesta"} = "Error en sistema_spRestorePass";
    }

    $conexion = null;
}

echo json_encode($respuesta);
