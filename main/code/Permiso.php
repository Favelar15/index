<?php

class Permiso
{
    protected $idApartado, $tipoApartado, $administrativo = false;
    private $conexion;

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    function isAdministrativo ( $roles )
    {
        foreach ( $roles as $rol )
        {
            if ( in_array( $rol->id, $this->obtenerRoles() ))
            {
                if ( $rol->tipo == 'Administrativo' )
                {
                    $this->administrativo = true;
                    break;
                }
            }
        }
    }

    private function obtenerRoles ()
    {
        $vista = $this->tipoApartado == 'Sub' ? 'general_viewRolesSubApartados' : 'general_viewRolesApartados';
        $query = "SELECT top(1) * from " . $vista . " WHERE id = " . $this->idApartado;
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if( $result->rowCount() > 0  )
        {
            $dataRoles = $result->fetch(PDO::FETCH_ASSOC);
            $this->conexion = null;
            return explode('@@@', $dataRoles["roles"]);
        }
        $this->conexion = null;
        return [];
    }

}