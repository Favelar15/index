<?php
include "generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include "connectionSqlServer.php";

        $response = null;

        $idUsuario = $_SESSION["index"]->id;
        $passActual = hash('sha256', $input["passActual"]);
        $newPass = hash('sha256', $input["newPass"]);

        $query = "EXEC general_spUpdatePass :id,:passActual,:newPass,:ip,:response;";
        $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':passActual', $passActual, PDO::PARAM_STR);
        $result->bindParam(':newPass', $newPass, PDO::PARAM_STR);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ($response) {
            $respuesta->{"respuesta"} = $response;
        } else {
            $respuesta->{"respuesta"} = "Error en la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
