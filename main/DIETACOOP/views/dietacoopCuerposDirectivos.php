<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Cargos de directivos</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">DIETACOOP</li>
            <li class="breadcrumb-item active">Cargos de directivos</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="cargos-tab" data-toggle="tab" href="#cuerpo-directivo" role="tab"
                    aria-controls="Cuepos directivos" aria-selected="true">
                Cuerpos directivos
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer">
                <button type="button" class="nav-link active buttonModal" onclick="configCuerposDirectivos.mostrarModal()">
                    <i class="fa fa-plus"></i> Agregar cuerpo directivo
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active " id="cargos-directivos" role="tabpanel" aria-labelledby="evaluacion-tab">
            <table class="table table-bordered table-striped" id="tblCuerposDirectivos">
                <thead>
                <th>N°</th>
                <th>Cuerpo directivo</th>
                <th>Fecha de registro</th>
                <th>Última modificación</th>
                <th>Acciones</th>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</section>


<div class="modal fade" id="modal-cuerposDirectivos" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Agregar cuerpo directivo</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="javascript:configCuerposDirectivos.guardar()" id="formCuerposDirectivos"
                      name="formCargos" accept-charset="utf-8" method="post" class="needs-validation"
                      novalidate>

                    <div class="row">

                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for="txtNombre">Nombre del cuerpo directivo: <span
                                        class="requerido">*</span> </label>
                            <input type="text" class="form-control campoRequerido" id="txtNombre"
                                   name="txtNombre"
                                   placeholder="" required/>
                            <div class="invalid-feedback">
                                Nombre del cuerpo directivo es requerido
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-outline-primary btn-sm" form="formCuerposDirectivos">
                    <i class="fas fa-plus-circle"></i> Guardar
                </button>

                <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                    <i class="fas fa-times-circle"></i> Cancelar
                </button>

            </div>
        </div>
    </div>
</div>



<?php
$_GET['js'] = ['dietacoopCuerposDirectivos'];