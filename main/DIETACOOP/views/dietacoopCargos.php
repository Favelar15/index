<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Cargos de directivos</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">DIETACOOP</li>
            <li class="breadcrumb-item active">Cargos de directivos</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="cargos-tab" data-toggle="tab" href="#cargos-directivos" role="tab"
                    aria-controls="Inventario de Platico y pines" aria-selected="true">
                Cargos directivos
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="btnResidenciasRepresentante">
                <button type="button" class="nav-link active buttonModal" onclick="configCargos.mostrarModal()">
                    <i class="fa fa-plus"></i> Agregar cargo
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active " id="cargos-directivos" role="tabpanel" aria-labelledby="evaluacion-tab">
            <table class="table table-bordered table-striped" id="tblCargos">
                <thead>
                <th>N°</th>
                <th>Cargo directivo</th>
                <th>Fecha de registro</th>
                <th>Última modificación</th>
                <th>Acciones</th>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal-cargos" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
         aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar cargo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="javascript:configCargos.guardar()" id="formCargos"
                          name="formCargos" accept-charset="utf-8" method="post" class="needs-validation"
                          novalidate>

                        <div class="row">

                            <div class="col-lg-12 col-xl-12 mayusculas">
                                <label class="form-label" for="txtCargo">Nombre del cargo: <span
                                            class="requerido">*</span> </label>
                                <input type="text" class="form-control campoRequerido" id="txtCargo"
                                       name="txtCargo"
                                       placeholder="" required/>
                                <div class="invalid-feedback">
                                    Nombre del cargo es requerido
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-outline-primary btn-sm" form="formCargos">
                        <i class="fas fa-plus-circle"></i> Guardar
                    </button>

                    <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>

                </div>
            </div>
        </div>
    </div>


</section>

<?php
$_GET['js'] = ['dietacoopCargos'];

