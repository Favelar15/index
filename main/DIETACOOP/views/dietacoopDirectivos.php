<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Directivos</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">DIETACOOP</li>
            <li class="breadcrumb-item active">Directivos</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">

        <li class="nav-item">
            <button class="nav-link active" id="directivos-tab" data-bs-toggle="tab"
                    data-bs-target="#directivos" role="tab"
                    aria-controls="Directivos" aria-selected="true"
                    onclick="generalShowHideButtonTab('mainButtonContainer','btnFormDirectivo');">
                Directivos
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="crearDirectivos-tab" data-bs-toggle="tab"
                    data-bs-target="#crearDirectivos" role="tab"
                    aria-controls="Crear Directivos" aria-selected="true"
                    onclick="generalShowHideButtonTab('mainButtonContainer','btnActionFormulario');">
                Formulario directivos
            </button>

        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer">
                <div class="tabButtonContainer" id="btnFormDirectivo">
                    <button type="button" class="btn btn-sm btn-outline-dark" onclick="configDirectivos.formulario()">
                        <i class="fa fa-plus"></i> Agregar directivo
                    </button>
                </div>
                <div class="tabButtonContainer" id="btnActionFormulario" style="display: none;">
                    <button type="submit" class="btn btn-sm btn-outline-success" form="formDatosAsociado">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" class="btn btn-sm btn-outline-danger ms-1" onclick="configDirectivos.salir()">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>

            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="directivos" role="tabpanel" aria-labelledby="evaluacion-tab">
            <table class="table table-bordered table-striped" id="tblDirectivos">
                <thead>
                <th>N°</th>
                <th>Nombre del directivo</th>
                <th>DUI</th>
                <th>fecha de registro</th>
                <th>Acciones</th>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <div class="tab-pane fade" id="crearDirectivos" role="tabpanel" aria-labelledby="evaluacion-tab">

            <form id="formDatosAsociado" name="formDatosAsociado" accept-charset="utf-8" method="post" action="javascript:asociado.buscar()" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtCodigoCliente">Buscar asociado: <span class="requerido">*</span></label>
                        <div class="input-group form-group has-validation">
                            <input type="text" class="form-control" id="txtCodigoCliente" onkeypress="return generalSoloNumeros(event)" name="txtCodigoCliente" placeholder="Codigo del cliente" required>
                            <div class="input-group-append">
                                <button class="input-group-text btn btn-outline-dark" type="submit"
                                        id="btnBuscarAscociado"><i class="fas fa-search"></i></button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el código de cliente
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNombres">Nombres: </label>
                        <input type="text" class="form-control" id="txtNombres" name="txtNombres" placeholder="Nombres" readonly>
                        <div class="invalid-feedback">
                            Ingrese los nombres
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtApellidos">Apellidos: </label>
                        <input type="text" class="form-control" id="txtApellidos" name="txtApellidos" placeholder="Apellidos" readonly>
                        <div class="invalid-feedback">
                            Ingrese los apellidos
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoFijo">Teléfono fijo: </label>
                        <input type="text" class="form-control" id="txtTelefonoFijo" name="txtTelefonoFijo" placeholder="0000-0000" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de teléfono
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoMovil">Teléfono movil: </label>
                        <input type="text" class="form-control" id="txtTelefonoMovil" name="txtTelefonoMovil" placeholder="0000-0000" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de teléfono
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboTipoDocumento">Tipo de documento: <span class="requerido">*</span></label>
                        <select disabled class="selectpicker form-control cboTipoDocumento tipoPrimario" id="cboTipoDocumento" onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumentoPrimario');" required>
                            <option selected disabled value=''>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el tipo de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtDocumentoPrimario">Número de documento: </label>
                        <input type="text" class="form-control numeroDocumentoPrimario" id="txtDocumentoPrimario" name="txtDocumentoPrimario" placeholder="Ingrese el numero de documento" readonly required>
                        <div class="invalid-feedback">
                            Ingrese el número de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtAgenciaAfiliacion">Agencia de afiliación: </label>
                        <input type="text" class="form-control" id="txtAgenciaAfiliacion" name="txtAgenciaAfiliacion" placeholder="Agencia de Afiliacion" readonly>
                        <div class="invalid-feedback">
                            Ingrese el nombre de la agencia
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNit">Numero NIT: </label>
                        <input type="text" class="form-control" id="txtNit" name="txtNit" placeholder="0000-000000-000-0" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de NIT
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboPais">País: <span class="requerido">*</span></label>
                        <select class="selectpicker form-control cboPais" title="seleccione" data-live-search="true" id="cboPais" name="cboPais" onchange="generalMonitoreoPais(this.value,'cboDepartamento');" required disabled>
                            <option value="" selected disabled>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un país
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboDepartamento">Departamento de residencia: <span class="requerido">*</span>
                        </label>
                        <select class="form-select" data-live-search="true" id="cboDepartamento" name="cboDepartamento" onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');" required data-live-search="true" disabled>
                            <option value="" selected disabled>seleccione un país</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un departamento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboMunicipio">Municipio de residencia: <span class="requerido">*</span></label>
                        <select disabled class="form-select" data-live-search="true" id="cboMunicipio" name="cboMunicipio" required data-live-search="true">
                            <option selected value="" disabled>seleccione un departamento</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un municipio
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mayusculas">
                        <label class="form-label" for="txtDireccionCompleta">Dirección completa: <span class="requerido">*</span></label>
                        <input readonly type="text" class="form-control" id="txtDireccionCompleta" name="txtDireccionCompleta" placeholder="Detalle la dirección" required />
                        <div class="invalid-feedback">
                            Ingrese la dirección completa
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboContribuyente">¿Es contribuyente? </label>
                        <select disabled class="selectpicker form-control" id="cboContribuyente" name="cboContribuyente" required>
                            <option selected value="" disabled>seleccione</option>
                            <option value="S" >SI</option>
                            <option value="N" >NO</option>
                        </select>
                        <div class="invalid-feedback">
                            Este campo es obligatorio
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNumeroCuenta">N° de cuenta: </label>
                        <input type="text" required class="form-control" id="txtNumeroCuenta" name="txtNumeroCuenta" placeholder="000-000-0000" readonly>
                        <div class="invalid-feedback">
                            Numero de cuenta es requerido
                        </div>
                    </div>
                </div>
            </form>

            <hr>
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="comite-tab" data-bs-toggle="tab"
                            data-bs-target="#comites" role="tab"
                            aria-controls="Cuerpo directivo" aria-selected="true">
                        Comites
                    </button>
                </li>
                <li class="nav-item ms-auto" id="mainButtonContainer">
                    <div class="tabButtonContainer">
                        <button type="button" class="nav-link active buttonModal" onclick="configComite.mostrarModal()">
                            <i class="fa fa-plus"></i> Agregar comité
                        </button>
                    </div>
                </li>
            </ul>

            <div class="tab-pane fade show active " id="comites" role="tabpanel" aria-labelledby="evaluacion-tab">

                <br>
                <table class="table table-striped table-bordered mt-2" id="tblComites">
                    <thead>
                        <th>N°</th>
                        <th>Comité</th>
                        <th>Cargo</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

</section>

<div class="modal fade" id="modal-comite" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Agregar comité</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="javascript:configComite.guardar()"
                      id="formComites"
                      name="formComites" class="needs-validation" novalidate>
                    <div class="row">

                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for="cboComite">Nacionalidad: <span
                                        class="requerido">*</span></label>
                            <select class="form-control selectpicker cboComite"
                                    data-live-search="true" id="cboComite"
                                    name="cboComite" required>
                                <option value="" disabled selected>Seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un comité
                            </div>
                        </div>

                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for="cboCargo">Cargo: <span
                                        class="requerido">*</span></label>
                            <select class="form-control selectpicker cboCargo"
                                    data-live-search="true" id="cboCargo"
                                    name="cboCargo" required>
                                <option value="" disabled selected>Seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un cargo
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-outline-primary btn-sm" form="formComites">
                    <i class="fas fa-plus-circle"></i> Guardar
                </button>

                <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                    <i class="fas fa-times-circle"></i> Cancelar
                </button>

            </div>
        </div>
    </div>
</div>


<?php
$_GET['js'] = ['dietacoopDirectivos'];

