
let tblDirectivos, tblComites
const modulo = 'DIETACOOP'

window.onload = () => {

    const cboTipoDocumento = document.getElementById('cboTipoDocumento')
    const cboPais = document.getElementById('cboPais')
    const cboComite = document.querySelector('form#formComites select#cboComite')
    const cboCargo = document.querySelector('form#formComites select#cboCargo')

    const inicializando = initDatatable().then( () => {
        return new Promise( (resolve, reject ) => {
            fetchActions.getCats({
                modulo, archivo: 'dietacoopGetCats', solicitados: ['tiposDocumento', 'geograficos', 'cargos', 'cuerposDirectivos']
            }).then( (
                { tiposDocumento: documentos,
                    datosGeograficos: geograficos =[],
                    cuerposDirectivos =[],
                    cargos = []
                }) => {

                if ( cboTipoDocumento )
                {
                    let opciones = [`<option selected value="" disabled>Seleccione</option>`]

                    documentos.forEach( documento => {
                        tiposDocumento[documento.id] = {...documento}
                        if (documento.tipoDocumento == 'DUI' || documento.tipoDocumento == 'Pasaporte') {
                            opciones.push(` <option value="${documento.id}">${documento.tipoDocumento}</option> `)
                        }
                    })

                    cboTipoDocumento.innerHTML = opciones.join('')
                    $(`#${cboTipoDocumento.id}`).selectpicker('refresh')
                }

                if( cboPais )
                {
                    datosGeograficos = {
                        ...geograficos
                    }
                    let opciones = ['<option selected value="" disabled>seleccione</option>']

                    for (let key in geograficos) {
                        opciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
                    }

                    cboPais.innerHTML = opciones.join('')
                    $(`#${cboPais.id}`).selectpicker('refresh')
                }

                if(cboCargo)
                {
                    let opciones = [`<option value="" selected disabled>seleccione</option>`]

                    cargos.forEach( cargo => {
                        opciones.push(`<option value="${cargo.id}">${cargo.cargo}</option>`)
                    })

                    cboCargo.innerHTML = opciones.join('')
                    $(`#${cboCargo.id}`).selectpicker('refresh')
                }

                configComite.catCuerposDirectivos = [...cuerposDirectivos]

                resolve()
            }).catch( reject )
        })
    }).catch( generalMostrarError )

    inicializando.then( () => {
        $('.preloader').fadeOut('fast')

        fetchActions.get({
            modulo, archivo: 'Directivos/dietacoopObtenerDirectivos', params: { }
        }).then( ( { directivos = [] } ) => {

            directivos.forEach( directivo => {
                configDirectivos.contador++
                configDirectivos.directivos[configDirectivos.contador] = { ...directivo }
            })

            configDirectivos.cargarDirectivos();
        })

    }).catch( generalMostrarError )

}

const initDatatable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblDirectivos').length > 0) {
                tblDirectivos = $('#tblDirectivos').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                    },
                        {
                            targets: [2,3],
                            className: 'text-center',
                        },
                        {
                            targets: [2],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblDirectivos.columns.adjust().draw();
            }

            if ($('#tblComites').length > 0) {
                tblComites = $('#tblComites').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            className: 'text-center',
                            "orderable": true,
                            width: '10%'
                        },
                        {
                            targets: [3],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblDirectivos.columns.adjust().draw();
            }
            resolve();
        } catch (e) {
            reject(e.message);
        }
    });
}

const asociado = {
    txtCodigoCliente: document.getElementById('txtCodigoCliente'),
    buscar() {
        formActions.validate('formDatosAsociado')
            .then( ({ errores }) => {
                if( errores == 0 )
                {
                    let existeCodigoCliente = true
                    let idFila = 0

                    if ( Object.keys(configDirectivos.directivos).length > 0 )
                    {
                        for ( const key in configDirectivos.directivos )
                        {
                            if ( configDirectivos.directivos[key].codigoCliente ==  this.txtCodigoCliente.value )
                            {
                                idFila = key
                                existeCodigoCliente = false
                                break
                            }
                        }
                    }

                    if ( existeCodigoCliente )
                    {
                        this.txtCodigoCliente.readOnly = true

                        generalVerificarCodigoCliente(this.txtCodigoCliente.value)
                            .then( data => {

                                if (data.respuesta)
                                {
                                    let {
                                        nombresAsociado,
                                        apellidosAsociado,
                                        tipoDocumentoPrimario,
                                        numeroDocumentoPrimario,
                                        numeroDocumentoSecundario,
                                        agencia,
                                        telefonoFijo,
                                        telefonoMovil,
                                        pais,
                                        departamento,
                                        municipio,
                                        direccionCompleta
                                    } = data.asociado;

                                    document.getElementById('btnBuscarAscociado').disabled = true
                                    document.getElementById('txtNombres').value = nombresAsociado;
                                    document.getElementById('txtApellidos').value = apellidosAsociado;
                                    document.getElementById('txtTelefonoFijo').value = telefonoFijo;
                                    document.getElementById('txtTelefonoMovil').value = telefonoMovil;
                                    document.getElementById('cboTipoDocumento').value = tipoDocumentoPrimario
                                    document.getElementById('txtDocumentoPrimario').value = numeroDocumentoPrimario;
                                    document.getElementById('txtAgenciaAfiliacion').value = agencia;
                                    document.getElementById('txtNit').value = numeroDocumentoSecundario;

                                    document.getElementById('cboPais').value = pais;
                                    $("#cboPais").trigger("change");
                                    document.getElementById('cboDepartamento').value = departamento;
                                    $("#cboDepartamento").trigger("change");
                                    document.getElementById('cboMunicipio').value = municipio;

                                    document.getElementById('txtDireccionCompleta').value = direccionCompleta;

                                    document.getElementById('txtNumeroCuenta').readOnly = false
                                    document.getElementById('cboContribuyente').disabled = false

                                    const selects = document.querySelectorAll('form#formDatosAsociado select.selectpicker')

                                    selects.forEach( cbo => {
                                        $(`#${cbo.id}`).selectpicker('refresh')
                                    })

                                    document.getElementById('formDatosAsociado').action = 'javascript:guardar();';

                                } else if ( !data.respuesta )
                                {
                                    Swal.fire({
                                        title: 'INFORMACION',
                                        text: `No existe un asociado con el código ${this.txtCodigoCliente.value} en la base de datos. ¿Desea agregarlo?`,
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#46b1c3',
                                        cancelButtonColor: '#f54343',
                                        confirmButtonText: `<i class="fa fa-check-circle"></i> SI`,
                                        cancelButtonText: `<i class="fa fa-times-circle"></i> NO`
                                    }).then((result) => {
                                        if (result.isConfirmed) {
                                            location.href = `?page=controlesAsociados&mod=${encodeURIComponent(btoa('CONTROLES'))}`;
                                        }
                                    });
                                }
                            }).catch( generalMostrarError )
                    } else
                    {
                        this.txtCodigoCliente.value = ''
                        Swal.fire(
                            '¡INFORMACIÓN!',
                            `${configDirectivos.directivos[idFila].nombresAsociado} ${configDirectivos.directivos[idFila].apellidosAsociado} ya existe en el listado de directivos`,
                            'warning'
                        )
                    }
                }
            }).catch( generalMostrarError )
    }
}

const guardar = () => {

    formActions.validate('formDatosAsociado')
        .then( ( { errores } )  => {

            if (errores == 0 )
            {
                const {
                    txtCodigoCliente,
                    txtNumeroCuenta,
                    cboContribuyente } = formActions.getJSON('formDatosAsociado')

                if ( Object.keys(configComite.comites).length > 0 )
                {
                    fetchActions.set({
                        modulo, archivo: 'Directivos/dietacoopGuardarDirectivo',
                        datos: {
                            id: configDirectivos.idDirectivo ,txtCodigoCliente, txtNumeroCuenta, cboContribuyente, comites: { ...configComite.comites }, idApartado, tipoApartado }
                    }).then( response => {

                        switch (response.respuesta.trim()) {
                            case 'EXITO':
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: '¡Bien hecho!',
                                    text: 'El directivo fue registrado con exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then( () => {
                                    $(`#directivos-tab`).removeClass('disabled')
                                    $(`#crearDirectivos-tab`).addClass('disabled')
                                    $(`#directivos-tab`).trigger('click')
                                });
                                break;
                            default:
                                generalMostrarError(response);
                                break;
                        }

                    }).catch( generalMostrarError )
                } else
                {
                    toastr.warning('Debe agregar al menos un comite a la tabla')
                }
            }
        }).catch( generalMostrarError )
}

const configComite = {
    idFila: 0,
    contador: 0,
    comites: {},
    cuerposDirectivos : {},
    catCuerposDirectivos: [],
    cboComite : document.querySelector('form#formComites select#cboComite'),
    cboCargo: document.querySelector('form#formComites select#cboCargo'),
    mostrarModal( idFila = 0)
    {
        this.idFila = idFila

        formActions.clean('formComites')
            .then( () => {

                let idsIncluidos = []

                if ( this.idFila == 0 )
                {
                    for ( const key in this.comites )
                    {
                        idsIncluidos.push( this.comites[key].idComite)
                    }
                }

                let opciones = [`<option selected disabled value="">seleccione</option>`]

                this.catCuerposDirectivos.forEach( cuerpoDirectivo => {
                    if (!idsIncluidos.includes(cuerpoDirectivo.id)) {
                        opciones.push(`<option value="${cuerpoDirectivo.id}">${cuerpoDirectivo.nombre}</option>`)
                    }
                })

                this.cboComite.innerHTML = opciones.join('')

                if ( this.idFila > 0 )
                {
                    this.cboComite.value = this.comites[this.idFila].idComite
                    this.cboComite.disabled = true
                    this.cboCargo.value = this.comites[this.idFila].idCargo
                }

                $(`#${this.cboCargo.id}`).selectpicker('refresh')
                $(`#${this.cboComite.id}`).selectpicker('refresh')

            }).catch( generalMostrarError )
        $(`#modal-comite`).modal('show')
    },
    guardar()
    {
        formActions.validate('formComites')
            .then( ({ errores }) => {
                if( errores == 0 )
                {
                    const {
                        cboComite: { value: idComite, labelOption: comite},
                        cboCargo: { value: idCargo, labelOption: cargo }
                    } = formActions.getJSON('formComites')

                    if ( this.idFila == 0 )
                    {
                        this.contador++
                        this.comites[this.contador] = { id: 0, idComite, comite, idCargo, cargo }
                        this.agregarFila( { contador: this.contador, comite, cargo } )
                    } else
                    {
                        this.comites[this.idFila] = { ...this.comites[this.idFila], idCargo, cargo }
                        this.editar( this.idFila, { cargo } )
                    }

                    tblComites.columns.adjust().draw()

                    $(`#modal-comite`).modal('hide')
                }
            }).catch( generalMostrarError )
    },
    agregarFila( { contador, comite, cargo } )
    {
        let btnEditar = `<button type="button" class="btnTbl" onclick="configComite.mostrarModal(${contador})"><i class="fas fa-edit"></i></button>`
        let btnEliminar = `<button type="button" class="btnTbl" onclick="configComite.eliminar(${contador})"><i class="fas fa-trash-alt"></i></button>`

        tblComites.row.add([
            contador, comite, cargo,
            `<div class="tblButtonContainer">
                ${btnEditar} ${btnEliminar}
            </div>`
        ]).node().id = `trComite${contador}`
    },
    editar( idFila, { cargo })
    {
        $(`#trComite${idFila}`).find('td:eq(2)').html(cargo)
    },
    eliminar(idFila)
    {
        tblComites.row(`#trComite${idFila}`).remove().draw(false);
        delete this.comites[idFila];
    }
}

const configDirectivos = {
    idDirectivo: 0,
    idFila: 0,
    contador: 0,
    directivos : { },
    formulario()
    {
        formActions.clean('formDatosAsociado')
            .then( () => {
                tblComites.clear().draw()
                configComite.comites = { }
                configComite.contador = 0

                document.getElementById('formDatosAsociado').action = 'javascript:asociado.buscar();';
                document.getElementById('btnBuscarAscociado').disabled = false
                document.getElementById('txtCodigoCliente').readOnly = false
                document.getElementById('txtNumeroCuenta').readOnly = true
                document.getElementById('cboContribuyente').disabled = true

                $(`#cboContribuyente`).selectpicker('refresh')

                $(`#crearDirectivos-tab`).removeClass('disabled')
                $(`#directivos-tab`).addClass('disabled')
                $(`#crearDirectivos-tab`).trigger('click')

            }).catch( generalMostrarError )
    },
    editar( idFila )
    {
        this.formulario()
        this.idFila = idFila
        $(`.preloader`).fadeIn('fast')

        this.idDirectivo = this.directivos[this.idFila].id
        fetchActions.get({
            modulo, archivo: 'Directivos/dietacoopObtenerDirectivos', params: { id: this.idDirectivo }
        }).then( ( { directivos } ) => {

            document.getElementById('formDatosAsociado').action = 'javascript:guardar();';
            document.getElementById('btnBuscarAscociado').disabled = true

            document.getElementById('txtCodigoCliente').readOnly = true
            document.getElementById('txtCodigoCliente').value = directivos.codigoCliente
            document.getElementById('txtNombres').value = directivos.nombresAsociado
            document.getElementById('txtApellidos').value = directivos.apellidosAsociado
            document.getElementById('txtTelefonoFijo').value = directivos.telefonoFijo
            document.getElementById('txtTelefonoMovil').value = directivos.telefonoMovil
            document.getElementById('cboTipoDocumento').value = directivos.idTipoDocumentoPrimario
            document.getElementById('txtDocumentoPrimario').value = directivos.numeroDocumentoPrimario
            document.getElementById('txtNit').value = directivos.numeroDocumentoPrimario
            document.getElementById('cboPais').value = directivos.idPais
            $(`#cboPais`).trigger('change')
            document.getElementById('cboDepartamento').value = directivos.idDepartamento
            $(`#cboDepartamento`).trigger('change')
            document.getElementById('cboMunicipio').value = directivos.idMunicipio
            $(`#cboMunicipio`).trigger('change')
            document.getElementById('txtDireccionCompleta').value = directivos.direccionCompleta
            document.getElementById('cboContribuyente').value = directivos.contribuyente
            document.getElementById('txtNumeroCuenta').value = directivos.numeroDeCuenta
            document.getElementById('txtNumeroCuenta').readOnly = false
            document.getElementById('cboContribuyente').disabled = false

            const selects = document.querySelectorAll('select.selectpicker')

            selects.forEach( cbo => {
                $(`#${cbo.id}`).selectpicker('refresh')
            })

            directivos.comites.forEach( comite => {
                configComite.contador++
                configComite.comites[configComite.contador] = {
                    id: comite.id,
                    idComite: comite.idCuerpoDirectivo,
                    comite: comite.nombre,
                    idCargo: comite.idCargo,
                    cargo: comite.cargo
                }
                configComite.agregarFila( { contador: configComite.contador, comite: comite.nombre, cargo: comite.cargo})
            })
            tblComites.columns.adjust().draw()

        }).catch( generalMostrarError )
    },
    salir()
    {
        Swal.fire({
            title: '¿Estas seguro de cancelar el registro?',
            text: "¡ Los datos del formulario se perderán !",
            icon: 'warning',
            showCancelButton: true,
            showDenyButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: `<i class="fas fa-thumbs-up"></i> Salir`,
            cancelButtonText: '<i class="fas fa-thumbs-down"></i> Cancelar',
            denyButtonText: `<i class="fas fa-trash"></i> Limpiar`,
        }).then( result => {
            if ( result.isConfirmed )
            {
                $(`#directivos-tab`).removeClass('disabled')
                $(`#crearDirectivos-tab`).addClass('disabled')
                $(`#directivos-tab`).trigger('click')
            } else if (result.isDenied) {
               formActions.clean('formDatosAsociado')
                   .then( () => {
                       document.getElementById('txtCodigoCliente').readOnly = false
                       document.getElementById('btnBuscarAscociado').disabled = false
                       document.getElementById('txtNumeroCuenta').readOnly = true
                       document.getElementById('cboContribuyente').disabled = true

                       tblComites.clear().draw()
                       configComite.comites = {}
                       configComite.contador = 0

                   }).catch( generalMostrarError )
            }
        })
    },
    cargarDirectivos()
    {
        tblDirectivos.clear().draw()
        this.contador = 0

        for ( const key in this.directivos )
        {
            this.agregarFila({contador: key, ...this.directivos[key]})
        }
        tblDirectivos.columns.adjust().draw()
    },
    agregarFila( { contador, nombresAsociado, apellidosAsociado, numeroDocumentoPrimario, fechaRegistro } )
    {
        let btnEditar = `<button type="button" class="btnTbl" onclick="configDirectivos.editar(${contador})"><i class="fas fa-edit"></i></button>`
        let btnEliminar = `<button type="button" class="btnTbl" onclick="configDirectivos.eliminar(${contador})"><i class="fas fa-trash-alt"></i></button>`

        tblDirectivos.row.add([
            contador, `${nombresAsociado} ${apellidosAsociado}`, numeroDocumentoPrimario, fechaRegistro,
            `<div class="tblButtonContainer"> ${btnEditar} ${btnEliminar}</div>`
        ]).node().id = `trDirectivos${contador}`
    },
    eliminar( idFila )
    {
        const id = this.directivos[idFila].id
        const directivo = `${this.directivos[idFila].nombresAsociado} ${this.directivos[idFila].apellidosAsociado}`

        Swal.fire({
            title: '¡Importante!',
            text: `¿Estas seguro de eliminar a ${directivo} de la lista de directivos?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `<i class="fas fa-thumbs-up"></i> SI`,
            cancelButtonText: `<i class="fas fa-thumbs-down"></i> NO`
        }).then((result) => {
            if (result.isConfirmed) {

                fetchActions.set({
                    modulo, archivo: 'Directivos/dietacoopEliminarDirectivo', datos: { id, idApartado, tipoApartado }
                }).then( response => {

                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'El directivo fue eliminado con exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then( () => {
                                delete configDirectivos.directivos[idFila]
                                configDirectivos.cargarDirectivos();
                            });
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }

                }).catch( generalMostrarError )
            }
        })
    }
}