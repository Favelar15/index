
let tblCuerposDirectivos
const modulo = 'DIETACOOP'

window.onload = () => {
    initDatatable().then( () => {
        $('.preloader').fadeOut('fast')
        configCuerposDirectivos.cargarDatos()

    }).catch( generalMostrarError )

}

const initDatatable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblCuerposDirectivos').length > 0) {
                tblCuerposDirectivos = $('#tblCuerposDirectivos').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                    },
                        {
                            targets: [2,3],
                            className: 'text-center',
                        },
                        {
                            targets: [4],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblCuerposDirectivos.columns.adjust().draw();
            }
            resolve();
        } catch (e) {
            reject(e.message);
        }
    });
}

const configCuerposDirectivos = {
    idFila: 0,
    contador: 0,
    cuerposDirectivos: {},
    cargarDatos()
    {
        this.cuerposDirectivos = {}
        this.contador = 0
        tblCuerposDirectivos.clear().draw()

        fetchActions.get({
            modulo, archivo: 'CuerposDirectivos/dietacoopObtenerCuerposDirectivos', params: {}
        }).then( ( { cuerposDirectivos = [] } ) => {

            cuerposDirectivos.forEach( cuerpo => {
                this.contador++
                this.cuerposDirectivos[this.contador] = { ...cuerpo }
                this.agregarFila( this.contador, {...cuerpo } )
            })
            tblCuerposDirectivos.columns.adjust().draw()
        }).catch( generalMostrarError )
    },
    mostrarModal( idFila = 0 )
    {
        formActions.clean('formCuerposDirectivos')
            .then( () => {

                $('#modal-cuerposDirectivos').modal('show')
            }).catch( generalMostrarError )
    },
    guardar()
    {
        formActions.validate('formCuerposDirectivos')
            .then( ( { errores } ) => {

                if( errores == 0 )
                {
                    const datos = formActions.getJSON('formCuerposDirectivos')

                    let nombreValido = true

                    if ( Object.keys( this.cuerposDirectivos).length > 0 )
                    {
                        for ( const key in this.cuerposDirectivos )
                        {
                            if (datos.txtNombre.value === this.cuerposDirectivos[key].nombre )
                            {
                                nombreValido = false
                                break;
                            }
                        }
                    }

                    if ( nombreValido )
                    {
                        fetchActions.set( {
                            modulo,
                            archivo: 'CuerpoSDirectivos/dietacoopGuardarCuerpoDirectivo',
                            datos : { id: this.idFila, ...datos, idApartado, tipoApartado }
                        } ).then( response => {

                            switch (response.respuesta ) {
                                case 'EXITO':
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: '¡Bien hecho!',
                                        text: 'El cuerpo directivo fue registrado con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then(() => {
                                        $(`#modal-cuerposDirectivos`).modal('hide')
                                        this.contador++
                                        this.cuerposDirectivos[this.contador] = { ...response.cuerpoDirectivo }
                                        this.agregarFila( this.contador, { ...response.cuerpoDirectivo } )
                                        tblCuerposDirectivos.columns.adjust().draw()
                                    })
                                    break;
                                default:
                                    generalMostrarError(response);
                                    break;
                            }
                        }).catch( generalMostrarError )

                    } else
                    {
                        toastr.warning(`El cuerpo directivo "${datos.txtNombre.value}" ya existe`)
                    }
                }
            }).catch( generalMostrarError )
    },
    agregarFila(contador, { nombre, fechaRegistro, ultimaActualizacion } )
    {
        const button = ( tipoPermiso === 'ESCRITURA')
            ? `<button class="btnTbl" title="Eliminar" onclick='configCuerposDirectivos.eliminar( ${contador} )'><i class="fa fa-trash-alt"></i> </button>`
            :`<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`;

        tblCuerposDirectivos.row.add([
            contador, nombre, fechaRegistro, ultimaActualizacion,
            `<div class="tblButtonContainer"> ${button} </div>`
        ]).node().id = `trCuerposDirectivos${contador}`
    },
    eliminar( idFila )
    {
        const cuerpoDirectivo = this.cuerposDirectivos[idFila]

        Swal.fire({
            title: 'IMPORTANTE',
            text: `¿Quieres eliminar el cuerpo directivo "${ cuerpoDirectivo.nombre }" ?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            cancelButtonColor: '#f54343',
            confirmButtonText: `<i class="fa fa-thumbs-up"></i> Eliminar`,
            cancelButtonText: `<i class="fa fa-thumbs-down"></i> Cancelar`
        }).then( result => {

            if ( result.isConfirmed )
            {
                fetchActions.set({
                    modulo,
                    archivo: 'CuerposDirectivos/dietacoopEliminarCuerpoDirectivo',
                    datos: { id: cuerpoDirectivo.id, idApartado, tipoApartado }
                }).then( response => {

                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'El cuerpo directivo a sido eliminado con exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(i => {
                                tblCuerposDirectivos.row(`#trCuerposDirectivos${idFila}`).remove().draw()
                                delete this.cuerposDirectivos[idFila]
                            });
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }

                }).catch( generalMostrarError )
            }

        })

    }
}