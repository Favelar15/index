let tblCargos
const modulo = 'DIETACOOP'

window.onload = () => {

    initDatatble().then( () => {
        configCargos.cargarDatos()
        $('.preloader').fadeOut('fast')
    })
}

const initDatatble = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblCargos').length > 0) {
                tblCargos = $('#tblCargos').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                    },
                        {
                            targets: [2,3],
                            className: 'text-center',
                        },
                        {
                            targets: [4],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblCargos.columns.adjust().draw();
            }
            resolve();
        } catch (e) {
            reject(e.message);
        }
    });
}

const configCargos = {
    contador:0,
    idFila: 0,
    cargos: { },
    txtCargo: document.getElementById('txtCargo'),
    cargarDatos()
    {
        tblCargos.clear().draw()
        this.cargos = {}

        fetchActions.get( {
            modulo, archivo: 'Cargos/dietacoopObtenerCargos', params: { }
        }).then( ( { cargos } ) => {

            cargos.forEach( cargo => {
                this.contador++
                this.cargos[this.contador] = { ...cargo }
                this.agregarFila( this.contador, {... cargo })
            })

            tblCargos.columns.adjust().draw()

        }).catch( generalMostrarError )
    },
    mostrarModal( idFila = 0 )
    {
        formActions.clean('formCargos')
            .then( () => {

                this.idFila = idFila
                if ( this.idFila > 0 )
                {
                    this.txtCargo.value = this.cargos[this.idFila].cargo
                }

                $(`#modal-cargos`).modal('show')
            }).catch( generalMostrarError )

    },
    guardar()
    {
        formActions.validate('formCargos')
            .then( ( {errores } ) => {

                if( errores === 0 )
                {
                    let datos = formActions.getJSON('formCargos')

                    let cargoValido = true;

                    if ( Object.keys(this.cargos).length > 0 )
                    {
                        for ( const key in this.cargos )
                        {
                            if ( datos.txtCargo.value === this.cargos[key].cargo )
                            {
                                cargoValido = false
                                break
                            }
                        }
                    }

                    if( cargoValido )
                    {
                        fetchActions.set({
                            modulo, archivo: 'Cargos/dietacoopGuardarCargos',
                            datos: { id: this.idFila, ...datos, idApartado, tipoApartado }
                        }).then( response => {

                            switch (response.respuesta ) {
                                case 'EXITO':
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: '¡Bien hecho!',
                                        text: 'El cargo fue registrado con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then(() => {
                                        $(`#modal-cargos`).modal('hide')
                                        this.contador++
                                        this.cargos[this.contador] = { ...response.cargo }
                                        this.agregarFila( this.contador, { ...response.cargo } )
                                        tblCargos.columns.adjust().draw()
                                    })
                                    break;
                                default:
                                    generalMostrarError(response);
                                    break;
                            }
                        }).catch( generalMostrarError )
                    } else
                    {
                        toastr.warning(`El cargo "${datos.txtCargo.value}" ya existe`)
                    }
                }
            }).catch( generalMostrarError )
    },
    agregarFila( contador, { cargo, fechaRegistro, ultimaActualizacion } )
    {
        const button = ( tipoPermiso === 'ESCRITURA')
            ? `<button class="btnTbl" title="Eliminar" onclick='configCargos.eliminar( ${contador} )'><i class="fa fa-trash-alt"></i> </button>`
            :`<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`;

        tblCargos.row.add([
            contador, cargo, fechaRegistro, ultimaActualizacion,
            `<div class="tblButtonContainer"> ${button} </div>`
        ]).node().id = `trCargo${contador}`
    },
    eliminar( idFila )
    {
        const cargo = this.cargos[idFila]

        Swal.fire({
            title: 'IMPORTANTE',
            text: `¿Quieres eliminar el cargo "${ cargo.cargo }" ?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            cancelButtonColor: '#f54343',
            confirmButtonText: `<i class="fa fa-thumbs-up"></i> Eliminar`,
            cancelButtonText: `<i class="fa fa-thumbs-down"></i> Cancelar`
        }).then( result => {
            if (result.isConfirmed)
            {
                fetchActions.set({
                    modulo, archivo: 'Cargos/dietacoopEliminarCargos',
                    datos: { id: cargo.id, idApartado, tipoApartado }
                }).then( response => {

                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'El cargo ha sido eliminado con exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(i => {
                                tblCargos.row(`#trCargo${idFila}`).remove().draw()
                                delete this.cargos[idFila]
                            });
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }

                }).catch( generalMostrarError )
            }
        })

    }
}