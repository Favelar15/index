<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (Object)[];

session_start();

if ( isset($_SESSION["index"]) && ($_SESSION["index"]->locked) )
{
    include "../../code/connectionSqlServer.php";
    require_once  'Permiso.php';

    $solicitados = explode("@@@", $_GET["q"]);

    if (in_array('tiposDocumento', $solicitados) || in_array('all', $solicitados)) {
        require_once '../../code/Models/tipoDocumento.php';
        $tipoDocumento = new tipoDocumento();
        $tiposDocumento = $tipoDocumento->obtenerTiposDocumento();

        $respuesta->{"tiposDocumento"} = $tiposDocumento;
    }

    if (in_array('estadoCivil', $solicitados) || in_array('all', $solicitados)) {
        require_once '../../code/Models/estadoCivil.php';
        $estadoCivil = new estadoCivil();
        $estadosCiviles = $estadoCivil->obtenerEstados();

        $respuesta->{"estadosCiviles"} = $estadosCiviles;
    }

    if (in_array('actividadEconomica', $solicitados) || in_array('all', $solicitados)) {
        require_once '../../code/Models/actividadEconomica.php';
        $actividadEconomica = new actividadEconomica();
        $actividadesEconomicas = $actividadEconomica->obtenerActividades();

        $respuesta->{"actividadesEconomicas"} = $actividadesEconomicas;
    }

    if (in_array('agencias', $solicitados) || in_array('all', $solicitados)) {
        require_once '../../code/Models/agencia.php';
        $agencia = new agencia();
        $agencias = $agencia->getAgenciasCbo();

        $respuesta->{"agencias"} = $agencias;
    }

    if (in_array('geograficos', $solicitados) || in_array('all', $solicitados)) {
        require_once '../../code/Models/datosGeograficos.php';
        $geografico = new geografico();
        $datosGeograficos = $geografico->obtenerGeograficos();

        $respuesta->{"datosGeograficos"} = $datosGeograficos;
    }

    if ( in_array('cuerposDirectivos', $solicitados) || in_array('all', $solicitados) )
    {
        require_once 'CuerposDirectivos/Models/CuerpoDirectivo.php';

        $cuerpoDirectivo = new CuerpoDirectivo();
        $respuesta->{'cuerposDirectivos'} = $cuerpoDirectivo->getCuerposDirectivos();
    }

    if ( in_array('cargos', $solicitados) || in_array('all', $solicitados) )
    {
        require_once 'Cargos/Models/Cargo.php';

        $cargo = new Cargo();
        $respuesta->{'cargos'} = $cargo->getCargos();
    }

    $conexion = null;
}

echo json_encode( $respuesta );