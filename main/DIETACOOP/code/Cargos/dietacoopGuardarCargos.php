<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];


if (isset($_SESSION['index']) && $_SESSION['index']->locked)
{
    include "../../../code/connectionSqlServer.php";
    require_once '../Permiso.php';
    require_once 'Models/Cargo.php';

    $cargo = new Cargo();
    $cargo->id = $input['id'];
    $cargo->setIdApartado( base64_decode(urldecode($input['idApartado'])) );
    $cargo->setTipoApartado( $input['tipoApartado'] );
    $cargo->cargo = $input['txtCargo']['value'];
    $cargo->idUsuario = $_SESSION['index']->id;
    $cargo->ipOrdenador = generalObtenerIp();
    $respuestaDB = $cargo->save();
    $respuesta['status'] = $respuestaDB->status;
    $respuesta['cargo'] = $respuestaDB->cargo;
    $respuesta['respuesta'] = $respuestaDB->respuesta;

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );
