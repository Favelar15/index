<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['cargos' => [] ];

if( isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    include "../../../code/connectionSqlServer.php";
    require_once '../Permiso.php';
    require_once 'Models/Cargo.php';

    $cargo = new Cargo();
    $respuesta['cargos'] = $cargo->getCargos();

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );