<?php

//require_once '../Permiso.php';

class CuerpoDirectivo extends Permiso
{
    public $id, $nombre, $fechaRegistro, $ultimaActualizacion, $idUsuario, $ipOrdenador;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        parent::__construct();

        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime( $this->fechaRegistro ));
        !empty($this->ultimaActualizacion) && $this->ultimaActualizacion = date('d-m-Y h:i a', strtotime( $this->ultimaActualizacion ));
    }

    function getCuerposDirectivos()
    {
        $query = "SELECT * FROM dietacoop_viewCatCuerpoDirectorio ORDER BY fechaRegistro DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function save()
    {
        $respuesta = null; $idDb = null;

        $query = 'EXEC dietacoop_spGuardarCuerpoDirectivo :id, :nombre, :idUsuario, :idApartado, :tipoApartado, :ipOrdenador, :idDB, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':nombre', $this->nombre, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idDB', $idDb, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ( $respuesta == 'EXITO')
        {
            return (object) [
                'status' => true,
                'respuesta' => $respuesta,
                'cuerpoDirectivo' => [
                    'id' => $idDb,
                    'nombre' => $this->nombre,
                    'fechaRegistro' => date('d-m-Y H:s a'),
                    'ultimaActualizacion' => date('d-m-Y H:s a'),
                ]
            ];
        }
        return (object) [ 'status' => false, 'cargo' => null, 'respuesta' => $respuesta ];
    }

    function eliminar()
    {
        $respuesta = null;

        $query = 'EXEC dietacoop_spEliminarCuerpoDirectivo :id, :idUsuario, :idApartado, :tipoApartado, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ( $respuesta == 'EXITO')
        {
            return (object) [ 'status' => true, 'respuesta' => $respuesta ];
        }

        return (object) [ 'status' => false, 'respuesta' => $respuesta ];
    }

    public function setIdApartado ($idApartado)
    {
        $this->idApartado = $idApartado;
    }

    public function setTipoApartado ($tipoApartado)
    {
        $this->tipoApartado = $tipoApartado;
    }
}