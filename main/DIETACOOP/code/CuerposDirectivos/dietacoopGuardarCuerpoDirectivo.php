<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked)
{
    include "../../../code/connectionSqlServer.php";
    require_once '../Permiso.php';
    require_once 'Models/CuerpoDirectivo.php';

    $cuerpoDirectivo = new CuerpoDirectivo();
    $cuerpoDirectivo->id = $input['id'];
    $cuerpoDirectivo->setIdApartado( base64_decode(urldecode($input['idApartado'])) );
    $cuerpoDirectivo->setTipoApartado( $input['tipoApartado'] );
    $cuerpoDirectivo->nombre = $input['txtNombre']['value'];
    $cuerpoDirectivo->idUsuario = $_SESSION['index']->id;
    $cuerpoDirectivo->ipOrdenador = generalObtenerIp();

    $respuestaDB = $cuerpoDirectivo->save();
    $respuesta['status'] = $respuestaDB->status;
    $respuesta['cuerpoDirectivo'] = $respuestaDB->cuerpoDirectivo;
    $respuesta['respuesta'] = $respuestaDB->respuesta;

}  else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );