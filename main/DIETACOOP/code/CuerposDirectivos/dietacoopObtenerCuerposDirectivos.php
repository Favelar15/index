<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['cuerposDirectivos' => []];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {

    include "../../../code/connectionSqlServer.php";
    require_once '../Permiso.php';
    require_once 'Models/CuerpoDirectivo.php';

    $cuerpoDirectivo = new CuerpoDirectivo();
    $respuesta['cuerposDirectivos'] = $cuerpoDirectivo->getCuerposDirectivos();

} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);