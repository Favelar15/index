<?php


header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = [ ];

if (isset($_SESSION['index']) && $_SESSION['index']->locked)
{
    include "../../../code/connectionSqlServer.php";
    require_once '../Permiso.php';
    require_once 'Models/Directivo.php';

    $directivo = new Directivo();
    $directivo->id = isset( $_GET['id'] ) && $_GET['id'] ? $_GET['id'] : null;
    $respuesta['directivos'] = $directivo->obtenerDirectivos();

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );