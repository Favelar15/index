<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked)
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../Permiso.php';
    //require_once '../Cargos/Models/Cargo.php';
    require_once 'Models/Directivo.php';

    $comites = $input['comites'];
    $comitesDB = '';

    if ( count( $comites ) > 0  )
    {
        $temp = [];

        foreach ($comites as $comite )
        {
            array_push( $temp, $comite['id'].'%%%'.$comite['idComite'].'%%%'.$comite['idCargo']);
        }
        $comitesDB = implode( '@@@', $temp );
    }

    $directivo = new Directivo();
    $directivo->id = $input['id'];
    $directivo->setIdApartado( base64_decode(urldecode($input['idApartado'])) );
    $directivo->setTipoApartado( $input['tipoApartado'] );
    $directivo->codigoCliente = $input['txtCodigoCliente']['value'];
    $directivo->numeroDeCuenta = $input['txtNumeroCuenta']['value'];
    $directivo->contribuyente = $input['cboContribuyente']['value'];
    $directivo->idUsuario = $_SESSION['index']->id;
    $directivo->ipOrdenador = generalObtenerIp();
    $respuestaDB = $directivo->guardar($comitesDB);
    $respuesta['respuesta'] = $respuestaDB->respuesta;

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );