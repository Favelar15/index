<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if( isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    include "../../../code/connectionSqlServer.php";
    require_once '../Permiso.php';
    require_once 'Models/Directivo.php';

    $directivo = new Directivo();
    $directivo->id = $input['id'];
    $directivo->setIdApartado( base64_decode(urldecode($input['idApartado'])) );
    $directivo->setTipoApartado( $input['tipoApartado'] );
    $directivo->idUsuario = $_SESSION['index']->id;
    $respuestaDB = $directivo->eliminar();

    $respuesta['respuesta'] = $respuestaDB->respuesta;
} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );