<?php

class Directivo extends Permiso
{
    public $id, $codigoCliente, $numeroDeCuenta, $contribuyente, $idUsuario, $fechaRegistro, $ipOrdenador;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        parent::__construct();

        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
    }

    function obtenerDirectivos()
    {
        if ( $this->id )
        {
            $query = "SELECT * FROM dietacoop_viewDatosDirectivos WHERE id = :id";
            $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $result->bindParam(':id', $this->id, PDO::PARAM_INT);

        } else {
            $query = "SELECT id, codigoCliente, nombresAsociado, apellidosAsociado, numeroDocumentoPrimario, fechaRegistro FROM dietacoop_viewDatosDirectivos ORDER BY fechaRegistro DESC";
            $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        }

        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            if( $this->id  )
            {
                $directivo = $result->fetch(PDO::FETCH_OBJ);
                $directivo->comites = $this->obtenerDetComites();
                return $directivo;
            }
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function guardar( $comites )
    {
        $respuesta = null;

        $query = 'EXEC controles_spGuardarDirectivo :id, :idApartado, :tipoApartado, :codigoCliente, :numeroDeCuenta, :contribuyente, :comites, :idUsuario, :ipOrdenador, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':numeroDeCuenta', $this->numeroDeCuenta, PDO::PARAM_STR);
        $result->bindParam(':contribuyente', $this->contribuyente, PDO::PARAM_STR);
        $result->bindParam(':comites', $comites, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ( $respuesta == 'EXITO')
        {
            return (object) [
                'status' => true,
                'respuesta' => $respuesta,
            ];
        }

        return (object) [
            'status' => true,
            'respuesta' => $respuesta,
        ];
    }

    private function obtenerDetComites()
    {
        $query = "SELECT * FROM dietacoop_viewDetComites WHERE idDirectivo = :id";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);

        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_OBJ);;
        }

        $this->conexion = null;
        return [];
    }

    function eliminar()
    {
        $respuesta = null;

        $query = 'EXEC dietacoop_spEliminarDirectivo :id, :idUsuario, :idApartado, :tipoApartado, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ( $respuesta == 'EXITO')
        {
            return (object) [ 'status' => true, 'respuesta' => $respuesta ];
        }

        return (object) [ 'status' => false, 'respuesta' => $respuesta ];
    }

    public function setIdApartado ($idApartado)
    {
        $this->idApartado = $idApartado;
    }

    public function setTipoApartado ($tipoApartado)
    {
        $this->tipoApartado = $tipoApartado;
    }
}