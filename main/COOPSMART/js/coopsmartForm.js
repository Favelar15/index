window.onload = () => {
    document.getElementById("frmContrato").reset();
    fetchActions.getCats({
        modulo: 'COOPSMART',
        archivo: 'coopsmartGetCats',
        solicitados: ['geograficos', 'tiposDocumento', 'agencias']
    }).then(({
        datosGeograficos: geograficos,
        tiposDocumento: tiposDoc,
        agencias
    }) => {
        let opciones = [`<option selected disabled value="">seleccione</option>`],
            cbosTiposDocumento = document.querySelectorAll("select.cboTipoDocumento"),
            cbosPais = document.querySelectorAll("select.cboPais"),
            cbosAgencia = document.querySelectorAll("select.cboAgencia"),
            tmpOpciones = [...opciones];

        tiposDoc.forEach(tipo => tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`));
        cbosTiposDocumento.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

        tmpOpciones = [...opciones];
        datosGeograficos = {
            ...geograficos
        };

        for (let key in datosGeograficos) {
            tmpOpciones.push(`<option value="${datosGeograficos[key].id}">${datosGeograficos[key].nombrePais}</option>`);
        }
        cbosPais.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

        tmpOpciones = [...opciones];
        agencias.forEach(agencia => tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`));
        cbosAgencia.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));
    }).catch(generalMostrarError);
}

const configContrato = {
    id: 0,
    campoCodigoCliente: document.getElementById("txtCodigoCliente"),
    campoEmail: document.getElementById("txtEmail"),
    codigoCliente: 0,
    intentos: 5,
    spanIntentos: document.querySelector("#mdlGeneracion .intentos-restantes"),
    search: function () {
        formActions.validate('frmContrato').then(({
            errores
        }) => {
            if (errores == 0) {
                fetchActions.get({
                    modulo: "COOPSMART",
                    archivo: "coopsmartValidarAsociado",
                    params: {
                        q: this.campoCodigoCliente.value
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                this.campoCodigoCliente.readOnly = true;
                                this.campoCodigoCliente.parentNode.querySelector("button.input-group-text").remove();
                                document.getElementById("frmContrato").action = 'javascript:configContrato.save();';

                                let tmp = {
                                    ...datosRespuesta.datosAsociado
                                };

                                document.getElementById("cboTipoDocumento").value = tmp.tipoDocumentoPrimario;
                                document.getElementById("txtNumeroDocumento").value = tmp.numeroDocumentoPrimario;
                                document.getElementById("txtNIT").value = tmp.numeroDocumentoSecundario;
                                document.getElementById("txtNombres").value = tmp.nombresAsociado;
                                document.getElementById("txtApellidos").value = tmp.apellidosAsociado;
                                document.getElementById("cboPais").value = tmp.pais;
                                $("#cboPais").trigger("change");
                                document.getElementById("cboDepartamento").value = tmp.departamento;
                                $("#cboDepartamento").trigger("change");
                                document.getElementById("cboMunicipio").value = tmp.municipio;
                                document.getElementById("cboAgencia").value = tmp.agencia;
                                document.getElementById("txtEmail").value = tmp.email;
                                document.getElementById("txtDireccion").value = tmp.direccionCompleta;

                                this.campoEmail.readOnly = false;
                                let botones = document.querySelectorAll("#mainButtons button");

                                botones.forEach(boton => boton.disabled = false);
                                break;
                            case "Generado":
                                Swal.fire({
                                    title: "¡Información!",
                                    text: "¡Ups! el contrato de este asociado ya fué generado",
                                    icon: "info"
                                });
                                break;
                            case "NoAsociado":
                                Swal.fire({
                                    title: "¡Atención!",
                                    html: "Asociado no registrado o retirado",
                                    icon: "warning"
                                });
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    save: function () {
        formActions.validate('frmContrato').then(({
            errores
        }) => {
            if (errores == 0) {
                this.spanIntentos.innerHTML = this.intentos;
                $("#mdlGeneracion").modal("show");
            }
        }).catch(generalMostrarError);
    },
    saveDB: function () {
        let datos = formActions.getJSON('frmContrato');
        fetchActions.set({
            modulo: "COOPSMART",
            archivo: 'coopsmartGuardarActivacion',
            datos: {
                ...datos,
                idApartado,
                tipoApartado,
                id: this.id
            }
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta.trim()) {
                    case "EXITO":
                        this.intentos--;
                        this.id = datosRespuesta.id;
                        userActions.abrirArchivo('COOPSMART', datosRespuesta.nombresArchivos.nombreHoja, 'Hojas');
                        userActions.abrirArchivo('COOPSMART', datosRespuesta.nombresArchivos.nombreContrato, 'Contratos');
                        this.spanIntentos.innerHTML = this.intentos;
                        if (this.intentos == 0) {
                            window.location.reload();
                        }
                        $("#mdlGeneracion").modal("hide");
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Salir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.reload();
            }
        });
    }
}