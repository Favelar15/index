let tblContratos;

window.onload = () => {
    document.getElementById("frmReporte").reset();
    document.getElementById('btnExcel').disabled = true;
    initTables().then(() => {
        fetchActions.getCats({
            modulo: "COOPSMART",
            archivo: 'coopsmartGetCats',
            solicitados: ['agencias']
        }).then(({
            agencias
        }) => {
            let cbosAgencia = document.querySelectorAll("select.cboAgencia"),
                opciones = [`<option selected value="0">Todas las agencias</option>`];

            agencias.forEach(agencia => opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`));
            cbosAgencia.forEach(cbo => {
                cbo.innerHTML = opciones.join("");
                cbo.className.includes("selectpicker") && ($("#" + cbo.id).selectpicker("refresh"));
            });
        }).catch(generalMostrarError);
    });
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 250;
            if ($("#tblContratos").length) {
                tblContratos = $("#tblContratos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2, 3, 4, 5, 6],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de contrato';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblContratos.columns.adjust().draw();
                resolve();
            }
        } catch (err) {
            console.error(err.message);
            reject();
        }
    });
}

const configContratos = {
    id: 0,
    cnt: 0,
    datos: {},
    cboAgencia: document.getElementById('cboAgencia'),
    campoInicio: document.getElementById('txtInicio'),
    campoFin: document.getElementById('txtFin'),
    btnExcel: document.getElementById("btnExcel"),
    search: function () {
        formActions.validate('frmReporte').then(({
            errores
        }) => {
            if (errores == 0) {
                fetchActions.get({
                    modulo: "COOPSMART",
                    archivo: 'coopsmartGetContratos',
                    params: {
                        agencia: this.cboAgencia.value,
                        inicio: this.campoInicio.value,
                        fin: this.campoFin.value
                    }
                }).then(this.init).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                configContratos.datos = {};
                tblContratos.clear().draw();

                tmpDatos.forEach(contrato => {
                    configContratos.cnt++;
                    configContratos.datos[configContratos.cnt] = {
                        ...contrato
                    };
                    configContratos.addRowTbl({
                        ...contrato,
                        nFila: configContratos.cnt
                    });
                });

                tblContratos.columns.adjust().draw();
                this.btnExcel.disabled = false;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    addRowTbl: function ({
        nFila,
        codigoCliente,
        nombres,
        apellidos,
        email,
        nombreAgenciaOrigen,
        nombreAgencia,
        nombreUsuario,
        fechaRegistro
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblContratos.row.add([
                    nFila,
                    codigoCliente,
                    `${nombres} ${apellidos}`,
                    email,
                    nombreAgenciaOrigen,
                    nombreAgencia,
                    nombreUsuario,
                    fechaRegistro,
                    '<div class="tblButtonContainer">' +
                    '<span class="btnTbl" title="Reimprimir contrato" onclick="configContratos.print(' + nFila + ');">' +
                    '<i class="fas fa-print"></i>' +
                    '</span>' +
                    '</div>'
                ]).node().id = 'trContrato' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    print: function (id) {
        fetchActions.get({
            modulo: "COOPSMART",
            archivo: 'coopsmartPrintDocs',
            params: {
                id: this.datos[id].id
            }
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta.trim()) {
                    case "EXITO":
                        userActions.abrirArchivo('COOPSMART', datosRespuesta.nombresArchivos.nombreHoja, 'Hojas');
                        userActions.abrirArchivo('COOPSMART', datosRespuesta.nombresArchivos.nombreContrato, 'Contratos');
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    generarExcel: function () {
        if (Object.keys(this.datos).length > 0) {
            fetchActions.set({
                modulo: "COOPSMART",
                archivo: 'coopsmartExcelReporteGeneral',
                datos: {
                    datos: {
                        ...this.datos
                    },
                    inicio: this.campoInicio.value,
                    fin: this.campoFin.value
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta.trim()) {
                        case "EXITO":
                            window.open('./main/COOPSMART/docs/Reportes/'+datosRespuesta.nombreArchivo, '_blank');
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            Swal.fire({
                title: "¡Atención!",
                icon: "info",
                html: "No hay información para procesar"
            });
        }
    }
}