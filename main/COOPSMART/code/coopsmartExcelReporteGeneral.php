<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    $filename  = "rpt-" . $input['inicio'] . "-hasta-" . $input['fin'] . ".xlsx";

    $respuesta->{"nombreArchivo"} = $filename;

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'N°');
    $sheet->setCellValue('B1', 'Código de cliente');
    $sheet->setCellValue('C1', 'DUI');
    $sheet->setCellValue('D1', 'NIT');
    $sheet->setCellValue('E1', 'Nombre asociado');
    $sheet->setCellValue('F1', 'Correo electrónico');
    $sheet->setCellValue('G1', 'Agencia origen');
    $sheet->setCellValue('H1', 'Agencia de generación');
    $sheet->setCellValue('I1', 'Usuario');
    $sheet->setCellValue('J1', 'Fecha');
    $spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

    $nFila = 1;
    foreach ($input["datos"] as $contrato) {
        $nFila++;
        $sheet->setCellValue('A' . $nFila, ($nFila - 1));
        $sheet->setCellValue('B' . $nFila, $contrato["codigoCliente"]);
        $sheet->setCellValue('C' . $nFila, $contrato["numeroDocumento"]);
        $sheet->setCellValue('D' . $nFila, $contrato["NIT"]);
        $sheet->setCellValue('E' . $nFila, $contrato["nombres"] . ' ' . $contrato["apellidos"]);
        $sheet->setCellValue('F' . $nFila, $contrato["email"]);
        $sheet->setCellValue('G' . $nFila, $contrato["nombreAgenciaOrigen"]);
        $sheet->setCellValue('H' . $nFila, $contrato["nombreAgencia"]);
        $sheet->setCellValue('I' . $nFila, $contrato["nombreUsuario"]);
        $sheet->setCellValue('J' . $nFila, $contrato["fechaRegistro"]);
    }
    $writer = new Xlsx($spreadsheet);
    $writer->save(__DIR__ . '/../docs/Reportes/' . $filename);
    $respuesta->{"respuesta"} = "EXITO";
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
