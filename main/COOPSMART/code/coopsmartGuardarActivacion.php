<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    include '../../code/connectionSqlServer.php';

    require_once './Models/contrato.php';

    $idUsuario = $_SESSION["index"]->id;
    $tipoApartado = $input["tipoApartado"];
    $idApartado = base64_decode(urldecode($input["idApartado"]));

    $contrato = new contrato();
    $contrato->id = $input["id"];
    $contrato->codigoCliente = $input["txtCodigoCliente"]["value"];
    $contrato->tipoDocumento = $input["cboTipoDocumento"]["value"];
    $contrato->numeroDocumento = $input["txtNumeroDocumento"]["value"];
    $contrato->NIT = $input["txtNIT"]["value"];
    $contrato->nombres = $input["txtNombres"]["value"];
    $contrato->apellidos = $input["txtApellidos"]["value"];
    $contrato->pais = $input["cboPais"]["value"];
    $contrato->departamento = $input["cboDepartamento"]["value"];
    $contrato->municipio = $input["cboMunicipio"]["value"];
    $contrato->agenciaOrigen = $input["cboAgencia"]["value"];
    $contrato->agencia = $_SESSION["index"]->idAgenciaActual;
    $contrato->email = $input["txtEmail"]["value"];
    $contrato->direccionCompleta = $input["txtDireccion"]["value"];

    $guardar = $contrato->guardarContrato($idUsuario, $ipActual, $tipoApartado, $idApartado);

    if (isset($guardar["respuesta"])) {
        $respuesta->{"respuesta"} = $guardar["respuesta"];
        isset($guardar["id"]) && $respuesta->{"id"} = $guardar["id"];
        if ($guardar["respuesta"] == "EXITO") {
            $contrato->id = $guardar["id"];
            $generacion = $contrato->generarDocumentos();

            if (isset($generacion["respuesta"]) && $generacion["respuesta"] == "EXITO") {
                $respuesta->{"nombresArchivos"} = ["nombreHoja" => $generacion["nombreHoja"], "nombreContrato" => $generacion["nombreContrato"]];
            }
        }
    } else {
        $respuesta->{"respuesta"} = "Error al conectar con la base de datos";
    }

    $conexion = null;
} else {
    $respuesta->{"respuesta"} = 'SESION';
}

echo json_encode($respuesta);
