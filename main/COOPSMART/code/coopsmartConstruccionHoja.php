<?php

class MYPDF2 extends TCPDF {

    public function Header() {
        global $nombreAgencia;

        $this->SetFont('dejavusans', 'B', 8);

        $tabla_header='<table cellpadding="0" cellspacing="1" border="0" style="text-align:center; margin-top:60px;"><tr><td rowspan="6" style="width:150px;"><img src="../../img/logoAcacypac.png" border="0" height="65" width="65" /></td><td style="width:500px;"></td></tr><tr><td style="width:400px;">ACACYPAC N.C. DE R.L.</td></tr><tr><td>AGENCIA '.$nombreAgencia.'</td></tr><tr><td>AFILIADA AL SISTEMA COOPERATIVA FINANCIERO FEDECACES DE R.L.</td></tr><tr><td></td></tr><tr><td>HOJA DE ACEPTACIÓN DE TERMINOS DE USO DE APLICACIÓN MÓVIL COOPSMART.</td></tr></table>';
        $this->writeHTML($tabla_header, true, false, true, false, '');
    }

    // Page footer
    public function Footer() {
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

$pdf = new MYPDF2(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Departamento de Informática');
$pdf->SetTitle('Hoja de aceptación');
$pdf->SetSubject('ACACYPAC de R.L.');
$pdf->SetKeywords('TCPDF, PDF');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(15, 50, 15);
$pdf->SetHeaderMargin(14);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 35);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->SetFont('dejavusans', '', 9);

$pdf->AddPage();


$parte1='<table nobr="true" border="0"><tr><td style="width:40px;"></td><td style="width:580px; text-align:justify; line-height:15px;">Yo, '.$nombreAsociado.', mayor de edad, del domicilio de '.$nombreMunicipio.', departamento de '.$nombreDepartamento.', con DUI: '.$dui.', NIT: '.$nit.', actuando en calidad de asociado de Cooperativa ACACYPAC DE RL, tengo a bien aceptar los términos de uso de la aplicación móvil COOPSMART.<br/><br />Entiendo y acepto que para seguridad en el resguardo de información y para propiciar el éxito en la realización de trámites por internet, éstos se dan bajo los siguientes términos:<br /><ol style="text-align:justify;"><li><strong>CLAVE DE ACCESO:</strong></li><ul><li>La clave de acceso habilita el uso de la aplicación móvil COOPSMART para cualquiera de los servicios disponibles en la aplicación móvil.</li><li>Al momento del registro se creará una clave de acceso propia del usuario.</li><li>El uso de la clave de acceso es personal y por lo tanto, no se deberá revelar o dar a conocer a cualquier persona.</li><li>La clave de acceso hace las veces de la firma autógrafa.</li><li>La información enviada con el uso de la clave de acceso es considerada presentada y transmitida por el usuario ante la Administración de La Cooperativa.</li></ul><li><strong>RED DE COMUNICACIÓN:</strong></li><ul><li>El servicio de comunicación por datos proveído por la empresa privada seleccionada y los efectos en sus fallas serán de responsabilidad mutua entre el usuario y dicha empresa.</li><li></li>El servicio de comunicación por datos seleccionado por el usuario debe estar en óptimas condiciones al momento de realizar los trámites por a través de la aplicación móvil COOPSMART, lo cual debe ser verificado por el mismo.</ul><li><strong>PRESTACIÓN DE SERVICIOS:</strong></li><ul><li>A través de la aplicación móvil COOPSMART, se tendrá acceso a realizar los diferentes trámites y consultas que la Administración de La Cooperativa tenga a disposición, los cuales estarán relacionados con:<ul><li>Consulta de cuentas y movimientos.</li><li>Transferencia de fondos entre cuentas propias.</li><li>Transferencia de fondos a cuentas de terceros de La Cooperativa</li><li>Pago de préstamos</li><li>Otros que la Administración incorpore a futuro.</li></ul></li><li>Los trámites que se ponen a disposición, se brindarán bajo las condiciones del servicio.</li><li>La Administración de La Cooperativa establecerá los servicios que serán habilitados en la aplicación móvil y hará las verificaciones que considere pertinentes antes de su habilitación.</li></ul><li><strong>PAGO DE PRÉSTAMOS:</strong></li><ul><li>El horario del pago de préstamos en línea a través de la aplicación móvil COOSPSMART, dependerá de la hora de cierre de que la Administración de La Cooperativa estipule.</li></ul><li><strong>COMPROBACIÓN DE REALIZACIÓN DEL SERVICIO:</strong></li><ul><li>Para la comprobación de realización de servicios, se generará, un mensaje o comprobante electrónico, confirmando la aplicación de cada servicio realizado.</li></ul></ol></td></tr></table>';
$pdf->writeHTML($parte1, true, false, true, false, '');
$pdf->AddPage();
$parte2='<table><tr><td style="width:80;"></td><td style="text-align:justify; width:540px; line-height:15px;">6. <strong>CLAUSULAS ESPECIALES:</strong><ul style="text-align:justify;"><li>Por este medio doy por aceptada la activación de los servicios que la Administración de La Cooperativa habilite dentro de la aplicación móvil COOPSMART a futuro.</li><li>Así mismo el correo que autorizo a partir de esta fecha en el registro de la aplicación móvil y para los usos de recibir notificaciones de transacciones u otra información relacionada con La Cooperativa: <strong>'.$email.'</strong>.</li></ul></td></tr></table><br /><br /><br /><br /><br /><br /><br /><br /><table nobr="true" border="0"><tr><td style="width:40px;" rowspan="5"></td><td style="width:580px; text-align:justify; line-height:18px;">FIRMA: _____________________________</td></tr><tr><td>'.$nombreAsociado.'</td></tr><tr><td></td></tr><tr><td>'.$fechaGeneracion.'</td></tr><tr><td>'.$nombreUsuario.'</td></tr></table>';
$pdf->writeHTML($parte2, true, false, true, false, '');


//ob_clean();
$mypdf=__DIR__.'/../docs/Hojas/'.$nombreHoja;

$pdf->Output($mypdf,'F');