<?php
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        $this->SetFont('helvetica', 'B', 7);
    }

    // Page footer
    public function Footer() {
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Departamento de Informática');
$pdf->SetTitle('COOPSMART');
$pdf->SetSubject('ACACYPAC de R.L.');
$pdf->SetKeywords('TCPDF, PDF');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(25, 30, 25);
$pdf->SetHeaderMargin(14);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 35);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->SetFont('dejavusans', '', 9);

$pdf->AddPage();

$pdf->Write(0, 'CONTRATO DEL USO DE SERVICIOS FINANCIEROS POR MEDIO DE COOPSMART'."\n", '', 0, 'C', true, 0, false, false, 0);
$pdf->SetFont('dejavusans', '', 8);
$pdf->Write(0, "\n".'LAS PARTES CONTRATANTES aquí nominadas, por medio de este documento, convenimos en celebrar el CONTRATO DE PRESTACIÓN DEL SERVICIO DENOMINADO “COOPSMART”, cuyas estipulaciones y términos son los siguientes:'."\n", '', 0, 'J', true, 0, false, false, 0);
$pdf->Write(0, "\n".'La ASOCIACION COOPERATIVA DE AHORRO CREDITO Y PRODUCCION AGROPECUARIA COMUNAL DE NUEVA CONCEPCION DE RESPONSABILIDAD LIMITADA que se abrevia ACACYPAC, N.C. de R.L., que en este instrumento se nombrara como “LA COOPERATIVA” Domicilio de Nueva Concepción Chalatenango, con número de identificación tributaria cero cuatrocientos dieciséis – doscientos cincuenta mil cuatrocientos setenta y uno – cero cero uno - ocho y '.$nombreAsociado.' “EL ASOCIADO o USUARIO” Domicilio de '.$nombreMunicipio.', departamento de '.$nombreDepartamento.', DUI '.$dui.', NIT '.$nit.'.'."\n", '', 0, 'J', true, 0, false, false, 0);
$pdf->Write(0, "\n1. ANTECEDENTES Y OBJETO\n \nEl asociado declara que es titular de servicios y/o productos activos, pasivos y neutros, ya contratados previamente o que en el futuro contrate, conforme los términos y condiciones que constan en los respectivos contratos, y de los cuales puede disponer por medio de distintos canales físicos y/o electrónicos tales como: Agencias y Cajeros Automáticos.
\nQue con el objeto de brindar un canal adicional a los ya mencionados y, para disponer de tales servicios y productos, que, mediante el uso de equipos, dispositivos móviles y sistemas automatizados, permitan al cliente, visualizar, consultar y operar transacciones, LA COOPERATIVA pone a disposición del asociado, por medio del presente contrato, la prestación de servicios financieros a través de canales electrónicos de COOPSMART.
\n2. PRESTACIÓN DE LOS SERVICIOS
\na) APLICACIÓN MÓVIL COOPSMART es un servicio electrónico que LA COOPERATIVA ofrece a sus asociados a través del cual suministra información, permite interactuar y realizar transacciones de los Productos y Servicios que LA COOPERATIVA brinda.
\nAPLICACIÓN MOVIL COOPSMART.
\nb) El acceso a COOPSMART es exclusivo para operaciones financieras con productos de LA COOPERATIVA, por medio de dispositivos móviles, mediante descarga de la aplicación y contratación del servicio de datos con la compañía telefónica de su preferencia.
\nc) Correo electrónico a dispositivos móviles que LA COOPERATIVA enviará sobre información relacionada a los servicios y productos que LA COOPERATIVA ofrece.
\nd) Geolocalización es un servicio por medio del cual, le permite a EL ASOCIADO ubicar todos los puntos de atención de LA COOPERATIVA en un mapa satelital, incluyendo las horas de servicio y dirección exacta.
\n3. TIPOS DE ACCESO PARA APLICACIÓN MÓVIL COOPSMART
\na) EL ASOCIADO podrá acceder a las diferentes transacciones financieras de los diversos productos o servicios financieros que éste tenga o tuviera en un futuro, contratadas con aquél, a través de APLICACIÓN MÓVIL COOPSMART. Para Depósitos a plazo fijo no aplican transacciones financieras.
\nb) La solicitud de activación de COOPSMART móvil será enviada a LA COOPERATIVA en el momento que EL ASOCIADO ingrese a la app móvil por medio de su dispositivo móvil y acepte los términos y condiciones de uso y la política de privacidad, mediante el ingreso de su usuario y clave de acceso es de uso exclusivo DE EL ASOCIADO. La activación se realizará de manera presencial en cualquiera de las sucursales de LA COOPERATIVA en horarios autorizados.
\n4. COOPSMART MOVIL
\nEl servicio COOPSMART Móvil permitirá a EL ASOCIADO una comunicación directa con LA COOPERATIVA por medio de un dispositivo móvil, con el fin de permitirle la realización de operaciones y transacciones financieras habilitadas por LA COOPERATIVA y a solicitud de EL ASOCIADO.
\nMediante el servicio COOPSMART, EL ASOCIADO, siempre y cuando estuviese habilitado para ello, podrá efectuar las operaciones mencionadas a continuación:
\na) Registro del servicio COOPSMART.
\nb) Consultas de cuentas de aportaciones, cuentas de ahorro, depósitos a plazo fijo, pago de préstamos, y cualquier producto que en el futuro EL ASOCIADO contrate con LA COOPERATIVA
\nc) Operaciones financieras que se podrán realizar. Transferir fondos entre cuentas propias y a terceros abiertas en LA COOPERATIVA; realizar pagos a préstamos; y cualquier otro servicio que en el futuro LA COOPERATIVA ofrezca a sus asociados.
\nTodas las operaciones anteriores podrán realizarse también en Agencias de LA COOPERATIVA, salvo aquellos otros servicios que en el futuro LA COOPERATIVA ofrezca a sus asociados y que solo puedan realizarse a través de COOPSMART MOVIL, tales como: recargas de celular, etc.
\n5. TECNOLOGIA REQUERIDA
\nPara poder acceder al servicio, EL ASOCIADO debe contar con un dispositivo móvil apto para la tecnología requerida en su momento por LA COOPERATIVA y deberá poseer activo el servicio de telefonía celular, navegación a internet, plan de datos, Wi-Fi o cualquier otro requisito necesario al momento de la aceptación del presente contrato, así como mantener actualizados ante LA COOPERATIVA los datos básicos necesarios para el suministro del servicio, en especial el número de celular y la cuenta de correo electrónico. En este sentido, LA COOPERATIVA no se hace responsable por la suspensión o deshabilitación del servicio por incumplimiento de ésta obligación; toda mejora efectuada al sistema o adición de funcionalidades en el servicio de la APP MOVIL, LA COOPERATIVA actualizará con las herramientas y/o recursos disponibles.
\n5.1 USO PERSONAL
\nLa información que EL ASOCIADO recibirá es de uso estrictamente personal, por lo cual no deberá revelar a terceros dicha información. EL ASOCIADO deberá adoptar todas las precauciones pertinentes para que la información que LA COOPERATIVA envíe a su dispositivo móvil sea únicamente visualizada y conocida por él.
\n5.2 PROVEEDORES DEL ASOCIADO
\nEL ASOCIADO podrá elegir, por cuenta propia y según su criterio, los proveedores de las tecnologías y equipos requeridos para acceder a la aplicación de COOPSMART. Los términos y condiciones de APP MOVIL no incluyen las condiciones de contratación con estas empresas, las cuales serán acordadas directamente por EL ASOCIADO, sin intervención ni responsabilidad alguna por parte de LA COOPERATIVA.
\n5.3 MATERIAL DE INFORMACION
\nLA COOPERATIVA ofrecerá una presentación del servicio, la cual puede ser accedida desde el portal de Internet y/o los demás canales de información y/o atención al público que LA COOPERATIVA habilite. A tal efecto EL ASOCIADO tendrá a su disposición y acceso a dicho material, el que estará obligado a conocer, bajo su responsabilidad, para operar apropiadamente a través de APLICACIÓN MÓVIL COOPSMART.
\n5.4 RESPONSABILIDAD DE LA COOPERATIVA
\na) LA COOPERATIVA es responsable de garantizar la seguridad de la infraestructura tecnológica para brindar el servicio.
\nb) LA COOPERATIVA es responsable de garantizar la continuidad del servicio, salvo casos fortuitos o de fuerza mayor debidamente comprobables. En todo caso LA COOPERATIVA comunicará oportunamente en los medios que disponga.
\nc) LA COOPERATIVA es responsable del funcionamiento adecuado de los sistemas tecnológicos y de gestión de EL ASOCIADO que son requeridos para brindar el servicio de APLICACIÓN MÓVIL COOPSMART, pero el proceso posterior de envío de información desde estas empresas hasta EL ASOCIADO no es responsabilidad de LA COOPERATIVA.
\nd) LA COOPERATIVA sólo atenderá consultas que EL ASOCIADO tenga sobre la operatividad de APLICACIÓN MÓVIL COOPSMART, siempre y cuando estas correspondan a los procesos operativos de los cuales LA COOPERATIVA es responsable.
\ne) LA COOPERATIVA no asume ninguna responsabilidad por los inconvenientes que EL ASOCIADO tuviera con el equipo, hardware y proveedor utilizado para acceder a este servicio.
\n5.5 RESPONSABILIDAD DE EL ASOCIADO
\na) EL ASOCIADO asume la responsabilidad por el uso indebido o inadecuado del sistema, que no sea parte del Sistema tecnológico y de gestión de LA COOPERATIVA, por lo tanto EL ASOCIADO se hace cargo de todos los daños y perjuicios correspondientes sin que ello obste a la facultad de LA COOPERATIVA para actualizar, suspender y/o interrumpir el servicio, previo aviso al cliente, por  razones de seguridad del mismo cliente y protección al servicio, en situaciones tales como los casos de: phishing, préstamo de claves a terceros, etc.
\nb) Ocurrida una interrupción por cualquier causa, EL ASOCIADO siempre tendrá la alternativa de solicitar la información u ordenar las transacciones requeridas mediante los medios tradicionales en las sucursales de LA COOPERATIVA.
\nc) Es responsabilidad exclusiva de EL ASOCIADO elegir las empresas de servicios de Internet y telefonía celular que le resulten confiables para permitirle acceder al servicio y recibir mensajes de texto y/o correo electrónico.
\nd) Es responsabilidad de EL ASOCIADO el descargar la aplicación y las actualizaciones que se den en el futuro, así como el uso y consumo del plan de datos requeridos para tal efecto.
\ne) EL ASOCIADO es responsable de contar con el espacio suficiente en memoria del dispositivo para almacenar la aplicación y sus actualizaciones.
\n5.6 OPERACIONES
\na) Al ingresar al sistema y realizar el procedo de activación, EL ASOCIADO quedará habilitado para realizar todas las operaciones que LA COOPERATIVA determine y así lo aceptará EL ASOCIADO.
\nb) Las operaciones cursadas a través de este sistema serán tomadas 'en firme'; se entiende que una operación es firme cuando ha sido confirmada por el asociado y por tanto es irrevocable, sea monetaria ó no, tales como: un cargo y un abono a cuentas propias o de terceros, pagos a préstamos, etc.
\n5.7 SUSPENSION DEL SERVICIO
\na) Si LA COOPERATIVA se viera obligada a realizar suspensiones temporales del servicio por razones de mantenimiento o mejoras al servicio las mismas serán notificadas oportunamente según sea predecible el evento que motiva la suspensión. También LA COOPERATIVA podrá suspender el servicio de forma temporal por motivos de fuerza mayor y causas fortuitas.
\nb) LA COOPERATIVA podría suspender el servicio de APLICACIÓN MÓVIL COOPSMART a aquellos clientes que infrinjan estos Términos y Condiciones o cualquiera de las restantes normas de gestión vigentes para los servicios de LA COOPERATIVA.
\nc) La suspensión del servicio APLICACIÓN MÓVIL COOPSMART, en forma definitiva, puede ser decidida por LA COOPERATIVA y será comunicada previa y oportunamente a sus clientes, restando así cualquier tipo de responsabilidad a la cooperativa por los daños y perjuicios que la suspensión entrañe. La suspensión definitiva será comunicada previamente con al menos 3 días de anticipación.
\nd) Las suspensiones temporales o definitivas que realice el operador de telefonía celular contratada por EL ASOCIADO de su servicio de mensajes de texto, o bien el proveedor de servicios de internet, o bien las realizadas por cualquier otra empresa responsable de transmitir la información; no son competencia de LA COOPERATIVA, ya que las mismas dependen de las políticas de gestión de tales empresas.
\ne) El uso de la aplicación móvil COOPSMART no es obligatorio, aun cuando esté incorporado en el presente contrato.
\nf) Cada servicio es ofrecido dentro de estos mismos Términos y Condiciones, los cuales EL ASOCIADO acepta al incorporarse por primera vez al servicio o renovar la suscripción que haya anulado.
\nLa suscripción de EL ASOCIADO al presente servicio no implica obligación para LA COOPERATIVA de enviarle información comercial.
\n6. AFILIACION
\ni. Todo cliente que solicite la activación del servicio de aplicación móvil COOPSMART mediante el Modelo de
Contrato de Firma Autógrafa, lo podrá realizar únicamente visitando cualquiera de las Agencias de LA COOPERATIVA en todo el país.
\nii. Para acceder al servicio de aplicación móvil COOPSMART deberá descargar la aplicación previamente, en dependencia del tipo de dispositivo y sistema operativo que el asociado posea.
\nLA COOPERATIVA entregará al Cliente una copia de contrato con firma autógrafa, conforme lo regulado en el artículo 22 de la Ley de Protección al consumidor.
\n7. CORREOS ELECTRONICOS. NOTIFICACIONES. TÉRMINOS Y CONDICIONES.
\nLos siguientes Términos y Condiciones resultan complementarios a los Términos y Condiciones del servicio aplicación móvil COOPSMART.
\na) LA COOPERATIVA podrá a su opción, SIN COSTO PARA EL ASOCIADO, enviarle a éste, a través de cualquier medio electrónico correo electrónico, en adelante EL CORREO, por cada evento de los siguientes:
\ni. Por transacciones y/u operaciones efectuadas por EL ASOCIADO en los canales electrónicos y/o en los medios que en un futuro implemente LA COOPERATIVA;
\nii. Promociones lanzadas por La Cooperativa en cualquiera de sus productos;
\niii. Cualquier otra consideración que pueda ser sujeta a comunicación;
\nb) Si ante inconvenientes en las comunicaciones, EL ASOCIADO no recibiera EL CORREO relacionados al romano i) literal a) inmediato anterior, podrá confirmar si la operación ha sido realizada a través de los diferentes canales electrónicos que LA COOPERATIVA tenga a disposición de EL ASOCIADO o en cualquiera de las agencias que LA COOPERATIVA posee en todo el país.
\nc) El dispositivo móvil deberá estar encendido y dentro del área de cobertura disponible por su proveedor de servicio de telefonía, siendo EL ASOCIADO responsable por el pago del costo del servicio de “roaming”, si aplicara.
\nd) En caso de solicitar la baja del dispositivo móvil, por robo, hurto, extravío o cambio de número telefónico, EL ASOCIADO se compromete a comunicar dicha modificación inmediatamente a LA COOPERATIVA a través de los canales que se establezcan para tales efectos, para que éste no continúe enviando la información a dicha línea. EL ASOCIADO será exclusivamente responsable por la falta de comunicación de este hecho, así como también por los reclamos que pudieran surgir por tal motivo, no siendo La Cooperativa responsable por la falta de comunicación de parte de EL ASOCIADO.
\ne) La suspensión del servicio de mensajería, en forma definitiva o temporal, puede ser decidida por LA COOPERATIVA o la empresa de telefonía móvil y será comunicada previa y oportunamente a EL ASOCIADO, restando así cualquier tipo de responsabilidad a LA COOPERATIVA por los daños y perjuicios que la suspensión pudiera generar. Igual política se adoptará si LA COOPERATIVA suspende el suministro del servicio de envío de mensajes de texto con alguna empresa de telefonía celular determinada o en alguna zona geográfica determinada.
\nf) LA COOPERATIVA, a su exclusivo costo, enviará a EL ASOCIADO y como consecuencia de la adhesión del servicio de envío de mensajes de texto, uno o más mensajes informándole el atraso en el cumplimiento de sus obligaciones, en cualquiera de los productos y/o servicios que mantenga contratado con LA COOPERATIVA.
\ng) NOTIFICACIONES: La Cooperativa también podrá notificar a petición de EL ASOCIADO y a costo de éste, servicios especiales tales como: la recepción de planillas de salario, de pensiones, saldo de cuentas, entre otros, a los cuales podrá suscribirse por medio de los canales que La Cooperativa ponga a su disposición.
\n8. GEOLOCALIZACIÓN
\na) A petición de EL ASOCIADO y en el momento que LA COOPERATIVA tenga a disposición el servicio de GEOLOCALIZACIÓN, podrá prestarlo bajo las condiciones y normas que éste establezca y que se harán de conocimiento de EL ASOCIADO a través de los medios que LA COOPERATIVA tenga a bien utilizar. En tal sentido, el acceso a los servicios a ser prestados bajo la denominación de Geolocalización, implica la aceptación de las condiciones expuestas a continuación:
\ni. Opción móvil que le permitirá ubicar todos los puntos de servicio de LA COOPERATIVA en un mapa satelital, incluyendo las horas de servicio y dirección exacta. 
\nii. Requiere tecnología GPS en sus dispositivos móviles y plan de datos contratado con la compañía de telefonía de su preferencia 
\niii. Podrá encontrar el punto de servicio más cercano a su ubicación siempre y cuando EL ASOCIADO así lo solicite.
\niv. A través de este servicio EL ASOCIADO podrá, geolocalizar sucursales, cajeros automáticos, búsqueda personalizada de puntos de servicio y cualquier alternativa que LA COOPERATIVA pueda en el futuro implementar.
\nb) LA COOPERATIVA podrá proporcionar a través del servicio, determinadas características y funciones que utilicen información basada en la localización del dispositivo, tecnología de GPS (en aquellas ubicaciones en las que dicha tecnología esté disponible), así como puntos de conexión Wi-Fi y repetidores de telefonía móvil. Para proporcionar tales características o funciones en las ubicaciones en las que estén disponibles,
\nLA COOPERATIVA podrá recoger, utilizar, transmitir, tratar y almacenar los datos geográficos relativos a EL ASOCIADO, la localización geográfica de su dispositivo.
\nc) EL ASOCIADO acepta y presta su consentimiento para que LA COOPERATIVA lleve a cabo dichas actividades de recopilación, utilización, transmisión, tratamiento y almacenamiento de tales datos geográficos y de cuenta para prestar y mejorar tales características o servicios.
\nd) EL ASOCIADO tiene derecho a revocar en cualquier momento su activación expresa en cualquiera de los servicios prestados como servicios de valor añadido basados en Geolocalización, y de este modo eliminar su consentimiento a que LA COOPERATIVA trate o, en su caso, acceda a su información de posición geográfica aproximada. Con esta revocación EL ASOCIADO no será localizado ni sus datos personales serán tratados por LA COOPERATIVA, por tanto, EL ASOCIADO no podrá acceder a la prestación del servicio ofrecido.
\ne) LA COOPERATIVA, sus sucursales y/o sus administradores, empleados y personal autorizado no serán responsables, salvo prueba en contrario, de cualquier tipo de perjuicio, pérdidas, reclamaciones o gastos de ningún tipo, tanto si proceden como si no del uso del servicio de Geolocalización, de la información adquirida o accedida por o a través de éste, de virus informáticos, de fallos operativos o de interrupciones en el servicio o transmisión, o fallos en la línea; en el uso del servicio de Geolocalización, tanto por conexión directa como por vínculo u otro medio, constituye un aviso a cualquier usuario de que estas posibilidades pueden ocurrir.
\nf) La Cooperativa será responsable frente al cliente por daños y perjuicios imputables en aspectos tecnológicos, siempre que no salgan de la esfera de control LA COOPERATIVA, excepto la responsabilidad penal o personal en que incurra el colaborador, empleado o funcionario de La Cooperativa, por el uso indebido o inadecuado de la información de EL ASOCIADO.
\n9. SERVICIO DE TRANSACCIONES ELECTRÓNICAS DE CRÉDITO Y/O DÉBITO
\na) OPERACIONES POR LIQUIDAR
\nEL ASOCIADO se obliga a restituir a LA COOPERATIVA cualquier diferencia a su favor que pudiera resultar en la operación del sistema o de tercera persona ligada en la operación.
\nb) CONFIRMACIÓN DE OPERACIONES
\nLas operaciones realizadas por EL ASOCIADO quedarán sujetas a su posterior verificación y serán válidas después de su registro definitivo, conforme al procedimiento que LA COOPERATIVA disponga.
\n10. PLAZO GENERAL DEL CONTRATO APLICACIÓN MOVIL COOPSMART
\nEl plazo del presente contrato será por tiempo indeterminado, sin perjuicio de darlo por terminado en cualquier momento, con 30 días calendario de anticipación a la fecha de su finalización.
\n11. CAUSAS DE TERMINACION Y CADUCIDAD DEL CONTRATO
\nTERMINACION:
\na) En caso de mora actual e histórica en cualquiera de los productos financieros con LA COOPERATIVA a cargo de la parte acreditada.
\nb) El plazo del presente contrato podrá terminar en cualquier momento, a opción de cualquiera de las partes.
\nCAUSALES DE CADUCIDAD:
\nc) Si EL ASOCIADO incurre en mora en uno o varios productos.
\nd) Por demanda y/o acción judicial en contra de EL ASOCIADO, iniciada por terceros o por LA COOPERATIVA, siempre y cuando afecte la credibilidad de EL ASOCIADO ante La Cooperativa 
\ne) Por inactividad de la cuenta de aportaciones o intervención judicial de la misma.
\n12. CLAUSULAS GENERALES
\na) MEJORAS AL SERVICIO. Toda mejora efectuada al sistema o adición de funcionalidades en el servicio, LA COOPERATIVA lo actualizará con las herramientas y/o recursos disponibles.
\nb) PROVEEDORES. LA COOPERATIVA, con la finalidad de brindar mejores servicios a EL ASOCIADO, podrá celebrar contratos con otras compañías o proveedores que coadyuven a ampliar aún más las opciones de servicios que por medio de este contrato realicen.
\nc) CARGOS EN CUENTA. Todas las operaciones, exceptuando los Depositos a plazo y donde explícitamente se indique otra cosa, será aplicada o cargada a la cuenta de EL ASOCIADO en donde se origine la transacción.
\nd) CUMPLIMIENTO DE CONDICIONES. EL ASOCIADO usará a su entera voluntad estos servicios cumpliendo todos los requerimientos y condiciones señaladas en este contrato y los que en el futuro establezca LA COOPERATIVA para la seguridad jurídica y operativa de las transacciones realizadas por EL ASOCIADO.
\ne) INCORPORACIÓN DE PRODUCTOS. EL ASOCIADO acepta que todos los productos que mantenga con LA COOPERATIVA serán incluidos en EL SERVICIO de manera automática al momento de contratarlo.
\nf) CUENTAS MANCOMUNADAS TIPO “Y”. Las cuentas de depósito mancomunadas tipo “Y”, en la cual dos o más titulares son copropietarios de los fondos, podrán ser afiliados a COOPSMART, pero solo podrán ser consultados por cualquiera de los titulares, mas no podrán ser debitados, salvo autorización escrita de todos los titulares de las cuentas.
\n13. REQUERIMIENTOS DEL HARDWARE Y SOFTWARE
\nLa Cooperativa no es responsable por la compatibilidad de los equipos de EL ASOCIADO por medio de los cuales se conectará a COOPSMART, para el acceso a la aplicación móvil COOPSMART, EL ASOCIADO deberá descargar una aplicación para su dispositivo móvil dependiendo de la plataforma que éste utilice, asimismo deberá poseer un teléfono compatible y tener suscrito bajo la modalidad de prepago o postpago el plan de datos, contratado con la empresa de telecomunicaciones que EL ASOCIADO prefiera. EL ASOCIADO reconoce desde ahora que todo el software y cualquier tipo de código usado en la aplicación móvil es propiedad de LA COOPERATIVA y/o de sus proveedores, y está protegido por leyes nacionales y tratados internacionales sobre la propiedad intelectual. Cualquier reproducción o redistribución está expresamente prohibida por la ley y puede conllevar sanciones civiles y penales.
\n14. 'CREDENCIALES DE AUTENTICACIÓN'.
\na) EL ASOCIADO acepta que será de uso obligatorio para el ingreso a la aplicación móvil COOPSMART el nombre de usuario, la clave personal definida por EL ASOCIADO y demás mecanismos de seguridad que disponga LA COOPERATIVA para el acceso a la aplicación móvil COOPSMART, en adelante denominada las 'CREDENCIALES DE AUTENTICACIÓN'. Lo anterior, sin perjuicio de los mecanismos de seguridad adicionales que LA COOPERATIVA llegare a establecer. Las Partes acuerdan que estos elementos identifican al ASOCIADO en las transacciones desarrolladas y/o establecidas a través de la aplicación móvil COOPSMART. 
\nb) La definición de las CREDENCIALES DE AUTENTICACIÓN será responsabilidad de EL ASOCIADO, así como su debida actualización de acuerdo con las políticas que LA COOPERATIVA establezca. EL ASOCIADO se obliga a custodiar y mantener en absoluta reserva las CREDENCIALES DE AUTENTICACIÓN y demás elementos y/o mecanismos de seguridad que LA COOPERATIVA llegue a establecer para la utilización de la aplicación móvil COOPSMART, a fin de que ninguna otra persona pueda hacer uso de las mismas o conocerlas por cualquier medio; por lo tanto, EL ASOCIADO no podrá ceder ni hacerse sustituir por terceros en el ejercicio de los derechos y compromisos que se le imponen y que en el evento de incumplirlos, EL ASOCIADO asume como propios ante LA COOPERATIVA y ante terceros el riesgo y la responsabilidad por cualquier uso indebido que se haga a causa del descuido en la custodia o reserva que asume. 
\n15. OTROS MEDIOS.
En el futuro LA COOPERATIVA podrá establecer otros medios de acceso y/o autenticación, tales como: lectores de huellas digitales, token, claves de uso único u otros sistemas y mecanismos de seguridad que LA COOPERATIVA considere oportuno su utilización en los presentes servicios. Otros medios de accesos y/o autenticación construidos dentro del sistema, LA COOPERATIVA los comunicará inmediatamente a EL ASOCIADO y para mantener la seguridad de las transacciones financieras de EL ASOCIADO, éste adquiere el compromiso de aceptar los otros medios de acceso y/o autenticación, para lo cual será oportunamente capacitado por LA COOPERATIVA. Si EL ASOCIADO no acepta los otros medios de acceso y/o autenticación propuestos por LA COOPERATIVA, éste podrá dar por caducado este contrato, salvo que EL ASOCIADO suscriba en forma electrónica nota en la que exprese que asume el riesgo por la no aceptación de los otros medios de acceso y/o autenticación propuestos u otro medio que LA COOPERATIVA estime conveniente.
\n16. VALIDEZ Y USO DE USUARIO Y CLAVE DE ACCESO.
\na) Las transacciones, operaciones y/o instrucciones realizadas por EL ASOCIADO a través de la aplicación móvil COOPSMART se sujetarán al perfil establecido y a las políticas, cuantías y horarios que establezca LA COOPERATIVA para la utilización de la aplicación móvil COOPSMART y para el uso de cada uno de los productos y/o servicios contratados por EL ASOCIADO con LA COOPERATIVA.
\nb) Es entendido que por el carácter electrónico de éstos servicios, las CREDENCIALES DE AUTENTICACIÓN, mediante el USUARIO Y CLAVE DE ACCESO sustituye la firma autógrafa de EL ASOCIADO, por tanto toda la información u operaciones derivadas del uso de esta identificación electrónica secreta se entenderá que ha sido firmada, aceptada y autorizada en forma legítima por EL ASOCIADO, con su expreso consentimiento puro y simple, siendo la constancia emitida por la bitácora o registros electrónicos de LA COOPERATIVA, prueba suficiente para demostrar y comprobar la existencia de toda consulta u operación financiera derivada de este contrato, salvo prueba en contrario, facultando a la cooperativa a cargar o abonar a las cuentas DEL ASOCIADO el valor de esos comprobantes de donde se originan tales servicios.
\nc) Serán del riesgo de EL ASOCIADO los errores que se presenten en el diligenciamiento de las órdenes de pago y/o transferencias efectuadas por EL ASOCIADO y se advierte que una vez realizadas las transferencias y/o pagos de acuerdo con las órdenes dadas por EL ASOCIADO a través de la aplicación móvil COOPSMART, LA COOPERATIVA no podrá hacer reversión de las sumas trasferidas y/o pagadas. La fecha del traslado y/o pago será la indicada por EL ASOCIADO a través de la aplicación móvil COOPSMART; sin embargo, si la fecha suministrada por EL ASOCIADO es sábado hasta las 12:00 m., domingo o día no hábil, LA COOPERATIVA podrá realizar el traslado y/o pago el día hábil siguiente a dicha fecha. EL ASOCIADO se obliga a mantener en LA COOPERATIVA el dinero disponible para efectuar las transferencias y/o pagos.
\nd) Las transferencias de fondos y/o utilizaciones ordenadas por EL ASOCIADO estarán sujetas a la verificación por parte de LA COOPERATIVA de que las cuentas y/o cupos de crédito a debitar tengan la suficiente provisión de fondos o que no presenten impedimento alguno que permitan realizar la transferencia. Si no existen fondos disponibles o si existe un impedimento, la transferencia ordenada por EL ASOCIADO será anulada y no ejecutada por LA COOPERATIVA
\n17. PRIVACIDAD Y RESPONSABILIDAD DE LA CLAVE DE ACCESO.
\na) Se conviene, salvo prueba en contrario, que EL ASOCIADO asume toda la responsabilidad de las operaciones, transacciones y/o instrucciones ordenadas a través de la aplicación móvil COOPSMART., pactándose expresamente que, si cualquier persona hace uso de la aplicación móvil COOPSMART con las CREDENCIALES DE AUTENTICACIÓN de EL ASOCIADO, EL ASOCIADO reconoce como suyas tales operaciones y/o transacciones siendo responsable ante LA COOPERATIVA y ante terceros por los perjuicios que ello genere, en base en los artículos 8 y 9 de la Ley Especial de Delitos Informáticos y Conexos y/o cualquier disposición que los adicione o modifique 
\nb) el usuario y la clave de acceso es de autenticación es propiedad de EL ASOCIADO e intransferible, individual, confidencial y de uso exclusivo de EL ASOCIADO.
\nc) Será única y absoluta responsabilidad de EL ASOCIADO la custodia, confidencialidad y buen uso de esta clave de acceso.
\nd) Toda transacción que se efectúe utilizando esta clave de acceso se entenderá que la ha autorizado y por tanto consentido por EL ASOCIADO; en tal sentido, cualquier mal uso que haga de este código es responsabilidad de EL ASOCIADO.
\ne) En caso de bloqueo del usuario, deberá acercarse a cualquiera de las sucursales de LA COOPERATIVA y en los medios que en un futuro implemente LA COOPERATIVA. Para cambiar la clave por medio de la misma aplicación. 
\n18. HORARIO DE SERVICIO
\na) Los 365 días del año las 24 horas, sin embargo, las operaciones realizadas posteriores a la hora de cierre de operaciones de LA COOPERATIVA, serán contabilizadas con fecha del día siguiente. Para este caso, se le advertirá a EL ASOCIADO por medio de un mensaje que su operación se realizará, pero bajo las condiciones antes descritas.
\n19. ACTUALIZACION DE VERSIONES E INTERRUPCION DEL SERVICIO
\na) LA COOPERATIVA podrá ampliar, restringir o suprimir parcial o totalmente los servicios regulados en este documento en forma temporal o definitiva, caso en el cual notificará previamente al ASOCIADO, en forma motivada, mediante escrito dirigido o mediante comunicación electrónica a los últimos datos de contacto registrados, la respectiva circunstancia o decisión. 
\nb) Así mismo, LA COOPERATIVA se reserva la facultad en todo tiempo, de negar el servicio, o de suspender y/o terminar el servicio en ejecución, de manera unilateral en los casos siguientes: a) por la detección de vulnerabilidades en los sistemas de operación y/o procesamiento de LA COOPERATIVA, b) por la inclusión del ASOCIADO en listados nacionales o internacionales como sospechoso de participar en actividades de lavado de activos; c) por estar vinculado EL ASOCIADO de cualquier otra forma a actividades delincuenciales o de apoyo a grupos al margen de la ley y d) por otras causas que impliquen riesgo respecto de los recursos de los que es titular EL ASOCIADO y/o que vinculen al ASOCIADO a actos contrarios a la ley. Ninguna de las decisiones que adopte LA COOPERATIVA al amparo de las facultades establecidas en la presente cláusula, dará lugar a indemnizaciones a favor del ASOCIADO por daños y perjuicios.
\nc) Se entiende que una actualización de versión es una mejora técnica y/o visual del servicio prestado a EL ASOCIADO, en tal sentido por dicha actualización EL ASOCIADO no incurrirá en costo por el uso de los canales.
\n20. NO RESPONSABILIDAD DE LA COOPERATIVA
\na) Queda expresamente pactado que LA COOPERATIVA no prestará el servicio si los dispositivos móviles de EL ASOCIADO no son compatibles con las especificaciones técnicas de los sistemas de LA COOPERATIVA 
\nb) No es responsabilidad de LA COOPERATIVA el consumo del plan de datos que requiera la aplicación de aplicación móvil COOPSMART para su descarga y uso.
\n21. CUSTODIA DE REGISTROS
\nEL ASOCIADO se obliga a cumplir con los protocolos de seguridad de cada servicio y a no intervenir en su funcionamiento, ni disponer en cualquier forma de los archivos, sistemas, programas, aplicaciones o cualquier otro elemento que LA COOPERATIVA con carácter exclusivo, reservado o propio de su actividad, utilice, posea o ponga a su disposición a efecto de llevar a cabo la prestación de sus servicios en internet, o en cualquier otra red informática, o desarrollo tecnológico que establezca a futuro. EL ASOCIADO se obliga a garantizar que los equipos que utilice estén libres de cualquier virus, a no acceder, ni usar o disponer indebidamente sin autorización de LA COOPERATIVA de los datos o la información incluida en los mismos programas, archivos, sistemas o aplicaciones. EL ASOCIADO se obliga a dar aviso inmediato a LA COOPERATIVA por un medio idóneo del conocimiento, disposición, uso o acceso, cualquiera que sea su causa, que tenga o haya tenido EL ASOCIADO o terceros, de tales archivos, datos, sistemas, programas aplicaciones o cualquier otro elemento de los anteriormente citados. EL ASOCIADO asume frente a LA COOPERATIVA y ante terceros la responsabilidad comprobada que se derive del incumplimiento de la presente cláusula.
\n22. DOMICILIO JUDICIAL
\nPara los efectos legales de este contrato las partes señalan un domicilio especial el de la ciudad de San Salvador, Departamento de San Salvador, a la jurisdicción de cuyos Tribunales expresamente se someten.
\n23. DERECHO DE APROBACIÓN DE SOLICITUD DE SERVICIO
\nLA COOPERATIVA se reserva el derecho de aprobación de afiliación y/o anulación de este contrato, cuando no reúna los requisitos establecidos por la institución financiera, tales como: clientes de alto riesgo, detección o sospecha de lavado de dinero, fraude electrónico y otra consideración que LA COOPERATIVA establezca.
\n24. ACEPTACION
\na) El manejo, uso de los recursos y el control de lo estipulado en este contrato, se efectuará por medio de operaciones electrónicas vía aceptación electrónica por medio del número de identificación personal, mediante el usuario y clave de acceso.
\nb) Todas las transferencias electrónicas descritas en el presente contrato y demás operaciones que efectúe EL ASOCIADO o USUARIO se harán mediante el uso de la aplicación móvil COOPSMART con el acceso del usuario y clave de acceso.
\nc) El uso de los medios de identificación que se establezca en sustitución de la firma autógrafa, producirá los mismos efectos que los que las Leyes otorgan a los documentos correspondientes y tendrán el mismo valor probatorio. EL ASOCIADO O USUARIO reconoce como válidas las operaciones electrónicas que así se efectúen, las cuales serán respaldadas por medio de la base electrónica de datos de LA COOPERATIVA, salvo prueba en contrario.
\nCualquier modificación a los términos y condiciones de este contrato, podrá aceptarse electrónicamente por parte de EL ASOCIADO, siempre que se cumplan los plazos previos de notificación.
\n25. RESPONSABILIDADES GENERALES DE LA COOPERATIVA
\na) Prestar los servicios dentro de los límites bajo su control. Situaciones ajenas a LA COOPERATIVA, que le imposibiliten brindar el servicio le excluyen de esta obligación.
\nb) Poner a disposición de EL ASOCIADO en la página web un folleto electrónico de seguridad relacionado al uso de canales electrónicos, en caso que EL ASOCIADO lo desee en físico, podrá solicitarlo en LA COOPERATIVA en cualquiera de las agencias de todo el país.
\nc) Probar por medio de Estados de Cuenta físicos o electrónicos, los saldos de las cuentas corrientes y/o de ahorros.
\nd) Respaldar y verificar las operaciones realizadas por EL ASOCIADO a través de sus sistemas.
\ne) Atender reclamos por inconsistencias o anomalías en las cuentas de EL ASOCIADO en un período no mayor a cuarenta y cinco (45) días hábiles posteriores a la fecha de la operación, pasado este período la operación se presume aceptada como bien efectuada salvo si existiere caso fortuito o fuerza mayor comprobable que no permitiese la atención del reclamo dentro del plazo establecido, lo anterior sin perjuicio de los reclamos mediante la vía administrativa y/o judicial. LA COOPERATIVA podrá notificar a EL ASOCIADO los errores que notare en la verificación y hacer las correcciones que a su juicio sean necesarias o convenientes realizar.
\n26. RESPONSABILIDADES GENERALES DE EL ASOCIADO
\na) Gestionar de forma personal a través de los medios que LA COOPERATIVA ponga a disposición, la activación del usuario COOPSMART. 
\nb) Notificar a LA COOPERATIVA en caso de robo o extravío del dispositivo móvil cuyo número se encuentre adscrito al presente servicio, a fin de proceder a su suspensión, o en todo caso, a su cambio a otro número de telefonía celular que a éstos efectos indique EL ASOCIADO, de forma tal que LA COOPERATIVA no se hace responsable por la información a la cual pudiesen acceder terceras personas en virtud del incumplimiento de esta obligación.
\nc) Digitar correctamente los datos para transferencias de fondos a cuentas propias o a cuenta de terceros, teniendo presente que los sistemas verifican que las cuentas sean válidas, mas no que pertenezcan a determinada persona. En caso de errores en las transferencias a terceros, EL ASOCIADO asume toda la responsabilidad por la pérdida de dichos valores. 
\nd) No realizar operaciones o transacciones indebidas que violen la legislación salvadoreña y a la buena práctica mercantil bancaria nacional e internacional.
\ne) EL ASOCIADO adquiere la obligación de registrar y mantener actualizada su información, dirección, domicilio, correo electrónico y celular para el envío de las notificaciones y/o comunicaciones que LA COOPERATIVA requiera, notificando oportunamente y por cualquier medio idóneo cualquier cambio. EL ASOCIADO puede consultar la POLÍTICA DE PRIVACIDAD en www.fedecaces.com. En todo caso, transcurrido tres (3) meses continuos de inactividad en el uso de la aplicación móvil COOPSMART, el Sistema solicitará nuevamente el registro de nuevas claves de acceso al CLIENTE.
\nf) Registrar en LA COOPERATIVA su domicilio, teléfono y dirección, tanto física como de correo electrónico y deberá notificar por escrito todo cambio de los mismos. Las notificaciones y correspondencia remitidas a tales direcciones por LA COOPERATIVA, tendrán plena validez.
\ng) Actualizar anualmente la información y datos básicos que varíen. El incumplimiento de esta obligación dará derecho a LA COOPERATIVA para cancelar los contratos sin que se genere indemnización alguna en beneficio de EL ASOCIADO.
\nh) EL ASOCIADO reconoce que el contenido (incluidos entre otros: texto, software, música, sonido, fotografías, vídeo, gráficos u otro material) ubicado bien sea en la publicidad de los anunciantes o en la información producida comercialmente y distribuida de forma electrónica y presentada al ASOCIADO por LA COOPERATIVA por sí mismo o por un tercero autorizado, está protegido por derechos de autor, marcas, patentes u otros bienes mercantiles o formas diferentes del derecho de propiedad. El usuario podrá hacer copia de este contenido exclusivamente para su uso personal, no comercial, siempre y cuando se mantengan intactos todos los avisos de derechos de autor y se cite la fuente. El usuario no podrá modificar, copiar, reproducir, volver a publicar, cargar, exponer, transmitir o distribuir de cualquier forma el contenido disponible a través de LA COOPERATIVA y los sitios vinculados, incluidos el código fuente y el software, so pena de incurrir en responsabilidad civil y penal, según las normas vigentes
\n27. ACEPTACION
\nLas Partes convienen que LA COOPERATIVA puede cancelar, suspender, bloquear, retirar, descontinuar, limitar, modificar, suprimir o adicionar los términos y condiciones de este documento, lo mismo que los privilegios y las condiciones de uso de la aplicación móvil COOPSMART, mediante aviso o publicación previa dada en tal sentido por cualquier medio idóneo que LA COOPERATIVA considere pertinente. Si pasados quince (15) días comunes de realizada la modificación EL ASOCIADO no se presentare a LA COOPERATIVA a cancelar el servicio, o notificare dicha decisión por los medios dispuestos al efecto, y/u opta por hacer uso del servicio de la aplicación móvil COOPSMART, tal hecho se entenderá como una aceptación expresa de las modificaciones introducidas. Se conviene que LA COOPERATIVA, a su criterio, podrá añadir nuevos servicios o retirar servicios que estén en operación, previa notificación con la oportunidad debida al ASOCIADO.
\nEL ASOCIADO al hacer uso de los servicios de LA COOPERATIVA acepta y autoriza las investigaciones y auditorías decretadas por éste y la utilización de la información del ASOCIADO para la prevención del fraude, determinación de su posible ocurrencia e investigación de incidentes o posibles vulnerabilidades. LA COOPERATIVA podrá contratar con terceras entidades el desarrollo de tales investigaciones, auditorías y visitas al ASOCIADO, el cual prestará su colaboración y asistencia, lo que implica extender su autorización, sobre las bases de debida confidencialidad, a la consulta o reporte de información concerniente a dichas operaciones, en los registros propios, todo esto, sin perjuicio de la observancia de las garantías previstas en las disposiciones legales vigentes.
\nEL ASOCIADO autoriza a LA COOPERATIVA para que reverse las transacciones y/o retenga, reintegre, debite o bloquee los recursos que se hayan acreditado en sus cuentas de manera errónea o fraudulenta por parte de LA COOPERATIVA u otro(s) ASOCIADO(S) de LA COOPERATIVA u otras COOPERATIVAS. En caso de que no sea posible el reintegro de los dineros por parte de EL ASOCIADO, éste se obliga a devolverlos a LA COOPERATIVA dentro de los cinco (5) días hábiles siguientes a la solicitud realizada por LA COOPERATIVA sin necesidad de requerimientos adicionales. El incumplimiento de lo previsto anteriormente, dará lugar a que LA COOPERATIVA inicie los procesos judiciales correspondientes con base en sus registros y lo dispuesto en el presente documento.
\nPrevio a la suscripción de este contrato, EL ASOCIADO declara, que ha leído, comprende, conoce y acepta las implicaciones y los efectos legales de este contrato.\n
En fe de lo anterior firmamos el presente contrato en la ciudad de ".$municipioAgencia.", departamento de ".$departamentoAgencia.", el día ".$fechaLetras.".\n
\n
\n
\n
\n_______________________________________________
\n".$nombreAsociado." (Asociado/a)\n
\n
\n_______________________________________________
\nAporado cliente (si aplica)\n
\n
\n_______________________________________________
\nFIRMANTE A RUEGO (en caso de no saber o no poder firmar)\n
\n
\n_______________________________________________
\nTESTIGO 1 (en caso de no videntes)\n", '', 0, 'J', true, 0, false, false, 0);


$mypdf=__DIR__.'/../docs/Contratos/'.$nombreContrato;

$pdf->Output($mypdf,'F');
