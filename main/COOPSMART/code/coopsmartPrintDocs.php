<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (!empty($_GET)) {
        include "../../code/connectionSqlServer.php";

        require_once './Models/contrato.php';

        $contrato = new contrato();
        $contrato->id = $_GET["id"];

        $generacion = $contrato->generarDocumentos();

        if (isset($generacion["respuesta"])) {
            $respuesta->{"respuesta"} = $generacion["respuesta"];
            if ($generacion["respuesta"] == "EXITO") {
                $respuesta->{"nombresArchivos"} = ["nombreHoja" => $generacion["nombreHoja"], "nombreContrato" => $generacion["nombreContrato"]];
            }
        } else {
            $respuesta->{"respuesta"} = "Ocurrió un error al conectar a la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
