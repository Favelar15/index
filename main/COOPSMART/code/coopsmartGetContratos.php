<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = [];

session_start();
if (!empty($_GET)) {
    include "../../code/connectionSqlServer.php";

    require_once './Models/contrato.php';

    $contrato = new contrato();
    $agencia = $_GET["agencia"];
    $inicio = date('Y-m-d',strtotime($_GET["inicio"])).' 01:00:00';
    $fin = date('Y-m-d',strtotime($_GET["fin"])).' 23:59:00';
    $respuesta = $contrato->getContratos($agencia, $inicio, $fin);

    $conexion = null;
}

echo json_encode($respuesta);
