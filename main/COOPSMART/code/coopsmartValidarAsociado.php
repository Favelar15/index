<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {
    if (!empty($_GET) && isset($_GET['q'])) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/contrato.php';

        $contrato = new contrato();
        $contrato->codigoCliente = $_GET["q"];

        $busqueda = $contrato->validarAsociado();

        if (isset($busqueda["respuesta"])) {
            $respuesta->{"respuesta"} = $busqueda["respuesta"];
            isset($busqueda["datosAsociado"]) && $respuesta->{"datosAsociado"} = $busqueda["datosAsociado"];
        } else {
            $respuesta->{"respuesta"} = "Error al conectar a la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
