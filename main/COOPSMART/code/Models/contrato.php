<?php
class contrato
{
    public $id;
    public $codigoCliente;
    public $tipoDocumento;
    public $numeroDocumento;
    public $NIT;
    public $nombres;
    public $apellidos;
    public $pais;
    public $departamento;
    public $municipio;
    public $agenciaOrigen;
    public $agencia;
    public $email;
    public $direccionCompleta;
    public $fechaRegistro;
    public $usuario;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
    }

    public function guardarContrato($idUsuario, $ipActual, $tipoApartado, $idApartado)
    {
        $response = null;
        $id = null;

        $query = 'EXEC coopsmart_spGuardarActivacion :id,:codigoCliente,:tipoDocumento,:numeroDocumento,:nit,:nombres,:apellidos,:pais,:departamento,:municipio,:agenciaOrigen,:agencia,:email,:direccion,:usuario,:ip,:tipoApartado,:idApartado,:response,:idActivacion;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':tipoDocumento', $this->tipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumento', $this->numeroDocumento, PDO::PARAM_STR);
        $result->bindParam(':nit', $this->NIT, PDO::PARAM_STR);
        $result->bindParam(':nombres', $this->nombres, PDO::PARAM_STR);
        $result->bindParam(':apellidos', $this->apellidos, PDO::PARAM_STR);
        $result->bindParam(':pais', $this->pais, PDO::PARAM_INT);
        $result->bindParam(':departamento', $this->departamento, PDO::PARAM_INT);
        $result->bindParam(':municipio', $this->municipio, PDO::PARAM_INT);
        $result->bindParam(':agenciaOrigen', $this->agenciaOrigen, PDO::PARAM_INT);
        $result->bindParam(':agencia', $this->agencia, PDO::PARAM_INT);
        $result->bindParam(':email', $this->email, PDO::PARAM_STR);
        $result->bindParam(':direccion', $this->direccionCompleta, PDO::PARAM_STR);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idActivacion', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            // $this->conexion = null;
            return ["respuesta" => $response, "id" => $id];
        }
        // $this->conexion = null;
        return ["respuesta" => "Error en coopsmart_spGuardarActivacion"];
    }

    public function generarDocumentos()
    {
        $query = "SELECT top(1) * FROM coopsmart_viewDatosActivaciones where id = :id;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);

        if ($result->execute() && $result->rowCount() > 0) {
            require_once 'C:\xampp\htdocs\vendor\autoload.php';
            $datos = $result->fetchAll(PDO::FETCH_OBJ)[0];
            $nombreHoja = "HojaAceptacion_" . $datos->codigoCliente . ".pdf";
            $nombreContrato = "Contrato_" . $datos->codigoCliente . ".pdf";

            $nombreAgencia = $datos->nombreAgencia;
            $nombreAsociado = $datos->nombres . ' ' . $datos->apellidos;
            $nombreDepartamento = $datos->nombreDepartamento;
            $nombreMunicipio = $datos->nombreMunicipio;
            $dui = $datos->numeroDocumento;
            $nit = $datos->NIT;
            $email = $datos->email;
            $fechaGeneracion = date('d/m/Y', strtotime($datos->fechaRegistro));
            $nombreUsuario = $datos->nombreUsuario;
            $fechaLetras = generalFechaLetras(date('d', strtotime($datos->fechaRegistro)), date('m', strtotime($datos->fechaRegistro)), date('Y', strtotime($datos->fechaRegistro)));
            $departamentoAgencia = $datos->departamentoAgencia;
            $municipioAgencia = $datos->municipioAgencia;
            include __DIR__ . '/../coopsmartConstruccionHoja.php';
            include __DIR__ . '/../coopsmartConstruccionContrato.php';

            $this->conexion = null;
            return ["respuesta" => "EXITO", "nombreHoja" => $nombreHoja, "nombreContrato" => $nombreContrato];
        }

        $this->conexion = null;
        return ["respuesta" => "No se encontraron datos"];
    }

    public function validarAsociado()
    {
        $query = "SELECT count(*) as cantidad FROM coopsmart_datosActivaciones where codigoCliente = :codigoCliente and habilitado = 'S';";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);

        $result->execute();
        $datos = $result->fetch(PDO::FETCH_ASSOC);

        if ($datos["cantidad"] == 0) {
            require_once __DIR__ . '/../../../code/Models/asociado.php';
            $asociado = new asociado();
            $asociado->codigoCliente = $this->codigoCliente;
            $datosAsociado = $asociado->buscarAsociado();

            $this->conexion = null;
            if (count($datosAsociado) > 0) {
                return ["respuesta" => "EXITO", "datosAsociado" => $datosAsociado[0]];
            } else {
                return ["respuesta" => "NoAsociado"];
            }
        }

        $this->conexion = null;
        return ["respuesta" => "Generado"];
    }

    public function getContratos($agencia, $inicio, $fin)
    {
        $condicion = $agencia > 0 ? ' AND agencia = ' . $agencia : '';
        $query = "SELECT * FROM coopsmart_viewDatosActivaciones where (fechaRegistro between :inicio and :fin)" . $condicion . " AND habilitado = 'S';";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':inicio', $inicio, PDO::PARAM_STR);
        $result->bindParam(':fin', $fin, PDO::PARAM_STR);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }
}
