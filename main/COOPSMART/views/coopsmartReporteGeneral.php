<div class="pagetitle">
    <h1>Reporte general de activaciones COOPSMART</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">COOPSMART</li>
            <li class="breadcrumb-item">Reportería</li>
            <li class="breadcrumb-item active">General</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <form action="javascript:configContratos.search();" id="frmReporte" name="frmReporte" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-1 col-xl-1"></div>
                    <div class="col-lg-10 col-xl-10">
                        <div class="row">
                            <div class="col-lg-4 col-xl-4">
                                <label class="form-label" for="cboAgencia">Agencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboAgencia" name="cboAgencia" class="selectpicker cboAgencia form-control" required title="">
                                        <option selected value="" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <label for="txtInicio" class="form-label">Inicio <span class="requerido">*</span></label>
                                <input type="text" id="txtInicio" name="txtInicio" class="form-control fechaFinLimitado readonly" required>
                                <div class="invalid-feedback">
                                    Ingrese la fecha de inicio de búsqueda
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <label for="txtFin" class="form-label">Fin <span class="requerido">*</span></label>
                                <input type="text" id="txtFin" name="txtFin" class="form-control fechaFinLimitado readonly" required>
                                <div class="invalid-feedback">
                                    Ingrese la fecha de fin de búsqueda
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-xl-1"></div>
                    <div class="col-lg-12 col-xl-12 mt-2" align="center">
                        <button type="submit" class="btn btn-sm btn-outline-primary">
                            <i class="fas fa-search"></i> Buscar
                        </button>
                        <button type="button" class="btn btn-sm btn-outline-success" id="btnExcel" disabled onclick="configContratos.generarExcel();">
                            <i class="fas fa-file-excel"></i> Excel
                        </button>
                    </div>
                </div>
            </form>
            <hr class="mb-0 mt-2">
        </div>
        <div class="col-lg-12 col-xl-12 mt-3">
            <table id="tblContratos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Código de cliente</th>
                        <th>Nombre completo</th>
                        <th>Email</th>
                        <th>Agencia origen</th>
                        <th>Agencia generación</th>
                        <th>Usuario</th>
                        <th>Fecha</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php
$_GET["js"] = ['coopsmartReporteGeneral'];
