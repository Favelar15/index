<div class="pagetitle">
    <h1>Generación de contrato - COOPSMART</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">COOPSMART</li>
            <li class="breadcrumb-item active">Generar contrato</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <form action="javascript:configContrato.search();" id="frmContrato" name="frmContrato" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
        <div class="row">
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='txtCodigoCliente'>Código de cliente <span class="requerido">*</span></label>
                <div class='input-group form-group has-validation'>
                    <input type="text" class="form-control" required id="txtCodigoCliente" name="txtCodigoCliente">
                    <button type="submit" class="input-group-text btn btn-outline-secondary">
                        <i class="fas fa-search"></i>
                    </button>
                    <div class="invalid-feedback">
                        Ingrese el código de cliente
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='cboTipoDocumento'>Tipo de documento <span class="requerido">*</span></label>
                <select class="form-select cboTipoDocumento" title="" id="cboTipoDocumento" name="cboTipoDocumento" onchange="generalMonitoreoTipoDocumento(this.value,'txtNumeroDocumento')" required disabled>
                    <option selected disabled value="">seleccione</option>
                </select>
                <div class="invalid-feedback">
                    Seleccione un tipo de documento
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='txtNumeroDocumento'>Número de documento <span class="requerido">*</span></label>
                <input type="text" class="form-control " id="txtNumeroDocumento" name="txtNumeroDocumento" onkeypress='return generaLetrasNumeros(event)' maxlength="18" required readonly>
                <div class='invalid-feedback'>
                    Ingrese el número de documento
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='txtNIT'>NIT <span class="requerido">*</span></label>
                <input type="text" class="form-control" id="txtNIT" name="txtNIT" readonly required>
                <div class='invalid-feedback'>
                    Ingrese el número de NIT
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='txtNombres'>Nombres <span class="requerido">*</span></label>
                <input type="text" class="form-control" id="txtNombres" name="txtNombres" readonly required>
                <div class='invalid-feedback'>
                    Ingrese los nombres
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='txtApellidos'>Apellidos <span class="requerido">*</span></label>
                <input type="text" class="form-control" id="txtApellidos" name="txtApellidos" readonly required>
                <div class='invalid-feedback'>
                    Ingrese los apellidos
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='cboPais'>País de residencia <span class="requerido">*</span></label>
                <select id="cboPais" name="cboPais" class="form-select cboPais" title="seleccione" disabled required onchange="generalMonitoreoPais(this.value,'cboDepartamento');">
                    <option value="" selected disabled>seleccione</option>
                </select>
                <div class="invalid-feedback">
                    Seleccione un país
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='cboDepartamento'>Departamento de residencia <span class="requerido">*</span></label>
                <select id="cboDepartamento" name="cboDepartamento" class="form-select" title="seleccione" disabled required onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');">
                    <option value="" selected disabled>seleccione</option>
                </select>
                <div class='invalid-feedback'>
                    Seleccione un departamento
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='cboMunicipio'>Municipio de residencia <span class="requerido">*</span></label>
                <select id="cboMunicipio" name="cboMunicipio" class="form-select" title="seleccione" disabled required>
                    <option value="" selected disabled>seleccione</option>
                </select>
                <div class='invalid-feedback'>
                    Seleccione un municipio
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='cboAgencia'>Agencia origen <span class="requerido">*</span></label>
                <select id="cboAgencia" name="cboAgencia" class="form-select cboAgencia" title="seleccione" disabled required>
                    <option value="" selected disabled>seleccione</option>
                </select>
                <div class='invalid-feedback'>
                    Ingrese la agencia origen
                </div>
            </div>
            <div class="col-xl-3 col-lg-3">
                <label class="form-label" for='txtEmail'>Correo electrónico <span class="requerido">*</span></label>
                <input type="email" class="form-control" placeholder="Escriba un correo electrónico" id="txtEmail" name="txtEmail" readonly required>
                <div class='invalid-feedback'>
                    Ingrese un correo electrónico
                </div>
            </div>
            <div class="col-sm-12">
                <label class="form-label" for='txtDireccion'>Dirección completa <span class="requerido">*</span></label>
                <textarea class="form-control" id="txtDireccion" name="txtDireccion" readonly></textarea>
                <div class='invalid-feedback'>
                    Ingrese la dirección completa
                </div>
            </div>
            <div class="col-sm-12 mt-3" align="center" id="mainButtons">
                <button type="submit" class="btn btn-outline-success btn-sm" disabled>
                    <i class="fas fa-file-pdf-o"></i> Generar contrato
                </button>
                <button type="button" class="btn btn-outline-danger btn-sm" onclick="configContrato.cancel();" disabled>
                    <i class="fas fa-close"></i> Cancelar
                </button>
            </div>
        </div>
    </form>
</section>
<div class="modal fade" id="mdlGeneracion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Generación de documentos</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-xl-12" align="center">
                        <button type="button" class="btn btn-sm btn-success" title="Generar documentos" onclick="configContrato.saveDB();">
                            <i class="fas fa-file-alt"> Generar documentos</i>
                        </button>
                    </div>
                    <div class="col-lg-12 col-xl-12 mt-2" align="center">
                        <span>Intentos restantes: <strong class="intentos-restantes"></strong></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<?php
$_GET["js"] = ["coopsmartForm"];
?>