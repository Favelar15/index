<div class="pagetitle">
    <h1>Administración de directorio interno</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Soporte</li>
            <li class="breadcrumb-item active">Directorio</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="contactos-tab" data-bs-toggle="tab" data-bs-target="#contactos" role="tab" aria-controls="contactos" aria-selected="true" onclick="generalShowHideButtonTab('buttonsContainer','botonesPrincipal')">
                Contactos
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="contacto-tab" data-bs-toggle="tab" data-bs-target="#contacto" role="tab" aria-controls="contacto" aria-selected="true" onclick="generalShowHideButtonTab('buttonsContainer','botonesContacto')">
                Datos de contacto
            </button>
        </li>
        <li class="nav-item ms-auto" id="buttonsContainer">
            <div class="tabButtonContainer" id="botonesPrincipal">
                <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configContacto.edit();">
                    <i class="fas fa-plus-circle"></i> Contacto
                </button>
            </div>
            <div class="tabButtonContainer" id="botonesContacto" style="display: none;">
                <button type="submit" form="frmContacto" class="btn btn-sm btn-outline-success ml-2">
                    <i class="fas fa-save"></i> Guardar
                </button>
                <button type="button" class="btn btn-sm btn-outline-danger ml-2" onclick="javascript:configContacto.cancel();">
                    <i class="fas fa-times"></i> Cancelar
                </button>
            </div>
        </li>
    </ul>
    <div class="tab-content pt-2">
        <div class="tab-pane fade show active" id="contactos" role="tabpanel" aria-labelledby="contactos-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblContactos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Nombre completo</th>
                                <th>Oficina</th>
                                <th>Cargo</th>
                                <th>Discord</th>
                                <th>Teléfonos</th>
                                <th>Correos</th>
                                <th>Fecha de registro</th>
                                <th>Última actualización</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="contacto" role="tabpanel" aria-labelledby="contacto-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <form id="frmContacto" name="frmContacto" class="needs-validation" novalidate method="POST" accept-charset="utf-8" action="javascript:configContacto.save();">
                        <div class="row">
                            <div class="col-lg-4 col-xl-4 mayusculas">
                                <label for="txtNombres" class="form-label">Nombres <span class="requerido">*</span></label>
                                <input type="text" id="txtNombres" name="txtNombres" placeholder="nombres" class="form-control" required>
                                <div class="invalid-feedback">
                                    Ingrese los nombres
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4 mayusculas">
                                <label for="txtApellidos" class="form-label">Apellidos <span class="requerido">*</span></label>
                                <input type="text" id="txtApellidos" name="txtApellidos" placeholder="nombres" class="form-control" required>
                                <div class="invalid-feedback">
                                    Ingrese los apellidos
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <label for="cboAgencia" class="form-label">Agencia <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboAgencia" name="cboAgencia" class="form-control selectpicker cboAgencia" data-live-search="true" required>
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xl-6">
                                <label for="cboCargo" class="form-label">Cargo <span class="requerido">*</span></label>
                                <div class="form-group has-validation">
                                    <select id="cboCargo" name="cboCargo" class="form-control selectpicker cboCargo" data-live-search="true" required>
                                        <option selected value="0" disabled>seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xl-6">
                                <label for="txtDiscord" class="form-label">ID de Discord </label>
                                <input type="text" id="txtDiscord" name="txtDiscord" placeholder="ID de discord" class="form-control">
                                <div class="invalid-feedback">
                                    Ingrese el ID de discord
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-12 col-xl-12 mt-2">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="row">
                                <div class="col d-flex align-items-end">
                                    <label class="form-label">Teléfonos</label>
                                </div>
                                <div class="col">
                                    <button class='btn btn-sm btn-outline-primary float-end' style="margin-right: 0;" type="button" onclick="configTelefono.edit();">
                                        <i class='fas fa-plus'></i> Teléfono
                                    </button>
                                </div>
                            </div>
                            <hr class="mt-0">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <table id="tblTelefonos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Teléfono</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="row">
                                <div class="col d-flex align-items-end">
                                    <label class="form-label">Correos electrónicos</label>
                                </div>
                                <div class="col">
                                    <button class='btn btn-sm btn-outline-primary float-end' style="margin-right: 0;" type="button" onclick="configEmail.edit();">
                                        <i class='fas fa-plus'></i> Email
                                    </button>
                                </div>
                            </div>
                            <hr class="mt-0">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <table id="tblEmails" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Dirección de correo electrónico</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="mdlTelefono" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmTelefono" class="needs-validation" novalidate name="frmTelefono" method="POST" action="javascript:configTelefono.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Teléfono</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for='txtTelefono' class="form-label">Teléfono <span class="requerido">*</span></label>
                            <input type="text" class="form-control telefono" id="txtTelefono" name="txtTelefono" required placeholder="xxxx-xxxx">
                            <div class="invalid-feedback">
                                Ingrese el número de teléfono
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlEmail" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmEmail" name="frmEmail" class="needs-validation" novalidate method="POST" action="javascript:configEmail.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Correo electrónico</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for='txtEmail' class="form-label">Dirección de correo <span class="requerido">*</span></label>
                            <input type="email" id="txtEmail" name="txtEmail" class="form-control" required placeholder="xxx@xxxx.xxx">
                            <div class="invalid-feedback">
                                Ingrese la dirección de correo electrónico
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php
$_GET["js"] = ["soporteAdministracionDirectorio"];
