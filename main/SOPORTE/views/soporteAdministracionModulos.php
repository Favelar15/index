<div class="pagetitle">
    <h1>Administración de módulos</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Soporte</li>
            <li class="breadcrumb-item active">Módulos</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="roles-tab" data-bs-toggle="tab" data-bs-target="#roles" type="button" role="tab" aria-controls="roles" aria-selected="true">
                Módulos
            </button>
        </li>
        <li class="nav-item ms-auto">
            <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configModulo.edit();">
                <i class="fas fa-plus-circle"></i> Módulo
            </button>
        </li>
    </ul>
    <div class="tab-content pt-2">
        <div class="tab-pane fade show active" id="roles" role="tabpanel" aria-labelledby="roles-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblModulos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Módulo</th>
                                <th>Descripción</th>
                                <th>Contenedor</th>
                                <th>Fecha de registro</th>
                                <th>Última modificación</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$_GET["js"] = ["soporteGenerales", "soporteAdministracionModulos"];
