<div class="pagetitle">
    <h1>Administración de usuarios</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Soporte</li>
            <li class="breadcrumb-item">Usuarios</li>
            <li class="breadcrumb-item active">Administración</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="users-tab" data-bs-toggle="tab" data-bs-target="#users" type="button" role="tab" aria-controls="users" aria-selected="true" onclick="generalShowHideButtonTab('userButtonsTabs','botonesPrincipal');">
                Usuarios
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="user-tab" data-bs-toggle="tab" data-bs-target="#user" type="button" role="tab" aria-controls="user" aria-selected="true" onclick="generalShowHideButtonTab('userButtonsTabs','botonesUsuario');">
                Datos de usuario
            </button>
        </li>
        <li class="nav-item ms-auto" id="userButtonsTabs">
            <div class="tabButtonContainer" id="botonesPrincipal">
                <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configUsuario.edit();">
                    <i class="fas fa-plus-circle"></i> Usuario
                </button>
            </div>
            <div class="tabButtonContainer" id="botonesUsuario" style="display: none;">
                <button type="submit" form="frmUsuario" class="btn btn-sm btn-outline-success ml-2">
                    <i class="fas fa-save"></i> Guardar
                </button>
                <button type="button" class="btn btn-sm btn-outline-danger ml-2" onclick="javascript:configUsuario.cancel();">
                    <i class="fas fa-times"></i> Cancelar
                </button>
            </div>
        </li>
    </ul>
    <div class="tab-content pt-2">
        <div class="tab-pane fade show active" id="users" role="tabpanel" aria-labelledby="users-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblUsuarios" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Foto</th>
                                <th>Username</th>
                                <th>Nombre completo</th>
                                <th>Oficina's</th>
                                <th>Roles</th>
                                <th>Último ingreso</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="user" role="tabpanel" aria-labelledby="user-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <form id="frmUsuario" name="frmUsuario" enctype="multipart/form-data" method="POST" class="needs-validation" novalidate accept-charset="utf-8" action="javascript:configUsuario.save();">
                        <div class="row">
                            <div class="col-lg-3 col-xl-3" style="padding-left: 40px;">
                                <div class="wrap-custom-file">
                                    <input type="file" name="userImage" id="userImage" accept=".gif, .jpg, .png .jpeg" />
                                    <label for="userImage" style="background-size: 100% 100%; background-image:url('./main/img/users/avatar.jpg');" id="labelUserImage">
                                        <span><i class="fas fa-download"></i> Subir foto </span>
                                    </label>
                                </div>
                                <p style="font-size: 12px;">*mínimo 260px x 260px</p>
                            </div>
                            <div class="col-lg-9 col-xl-9">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4">
                                        <label for="txtNombres" class="form-label">Nombres <span class="requerido">*</span></label>
                                        <input type="text" id="txtNombres" name="txtNombres" placeholder="" title="" class="form-control" required value="">
                                        <div class="invalid-feedback">
                                            Ingrese los nombres.
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-xl-4">
                                        <label for="txtApellidos" class="form-label">Apellidos <span class="requerido">*</span></label>
                                        <input type="text" id="txtApellidos" name="txtApellidos" placeholder="" title="" required class="form-control" value="">
                                        <div class="invalid-feedback">
                                            Ingrese los apellidos
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-xl-4">
                                        <label for="txtEmail" class="form-label">Correo electrónico <span class="requerido">*</span></label>
                                        <input type="email" id="txtEmail" name="txtEmail" placeholder="" required title="" class="form-control" value="">
                                        <div class="invalid-feedback">
                                            Ingrese el correo electrónico
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4">
                                        <label for="txtUsername" class="form-label">Username <span class="requerido">*</span></label>
                                        <div class="input-group has-validation">
                                            <input type="text" id="txtUsername" name="txtUsername" placeholder="" required title="" class="form-control" value="">
                                            <div class="input-group-append">
                                                <span class="btn btn-outline-dark" onclick="configUsuario.validateUsername();">
                                                    <i class="fas fa-search"></i>
                                                </span>
                                            </div>
                                            <div class="invalid-feedback">
                                                Ingrese el username
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-xl-8" id="passContainer">
                                        <div class="row">
                                            <div class="col-lg-6 col-xl-6">
                                                <label for="txtContrasenia" class="form-label">Contraseña <span class="requerido">*</span></label>
                                                <input type="password" id="txtContrasenia" name="txtContrasenia" placeholder="" title="" class="form-control" value="">
                                                <div class="invalid-feedback">
                                                    Ingrese la contraseña
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-xl-6">
                                                <label for="txtContraseniaConfirmacion" class="form-label">Confirmación de contraseña <span class="requerido">*</span> </label>
                                                <input type="password" id="txtContraseniaConfirmacion" name="txtContraseniaConfirmacion" placeholder="" title="" class="form-control" value="">
                                                <div class="invalid-feedback">
                                                    Confirme la contraseña
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-12 col-xl-12">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="row">
                                <div class="col-lg-6 col-xl-6">
                                    <label>Agencias</label>
                                </div>
                                <div class="col-lg-6 col-xl-6">
                                    <button class='btn btn-sm btn-outline-primary float-end' type="button" onclick="configAgencia.edit();">
                                        <i class='fas fa-plus'></i> Agencia
                                    </button>
                                </div>
                                <div class="col-lg-12 col-xl-12 mt-2">
                                    <table id="tblAgencias" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Agencia</th>
                                                <th>Principal</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="row">
                                <div class="col-lg-6 col-xl-6">
                                    <label>Roles</label>
                                </div>
                                <div class="col-lg-6 col-xl-6">
                                    <button class='btn btn-sm btn-outline-primary float-end' type="button" onclick="configRol.edit();">
                                        <i class='fas fa-plus'></i> Rol
                                    </button>
                                </div>
                                <div class="col-lg-12 col-xl-12 mt-2">
                                    <table id="tblRoles" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Rol</th>
                                                <th>Tipo</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="mdlAgencia" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmAgencia" class="needs-validation" name="frmAgencia" method="POST" novalidate action="javascript:configAgencia.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdl_title">Asignación de agencia</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for='cboAgencia' class="form-label">Agencia <span class="requerido">*</span></label>
                            <div class="input-group form-group has-validation">
                                <select id="cboAgencia" name="cboAgencia" class="selectpicker form-control" required data-live-search="true">
                                    <option selected value="">seleccione</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione una agencia
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Agregar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlRol" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmRol" name="frmRol" class="needs-validation" novalidate method="POST" action="javascript:configRol.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdl_title">Asignación de rol</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for='cboRol'>Rol <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboRol" name="cboRol" class="selectpicker form-control campoRequerido" required data-live-search="true">
                                    <option selected value="0" disabled>seleccione</option>
                                </select>
                                <div class="invalid-feedback">
                                    Selecciona un rol
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit"" class=" btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$_GET["js"] = ["soporteGenerales", "soporteAdministracionUsuarios"];
?>