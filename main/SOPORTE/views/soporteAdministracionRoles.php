<div class="pagetitle">
    <h1>Administración de roles de usuario</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Soporte</li>
            <li class="breadcrumb-item active">Roles</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="roles-tab" data-bs-toggle="tab" data-bs-target="#roles" type="button" role="tab" aria-controls="roles" aria-selected="true">
                Roles
            </button>
        </li>
        <li class="nav-item ms-auto">
            <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configRol.edit();">
                <i class="fas fa-plus-circle"></i> Rol
            </button>
        </li>
    </ul>
    <div class="tab-content pt-2">
        <div class="tab-pane fade show active" id="roles" role="tabpanel" aria-labelledby="roles-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblRoles" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Rol</th>
                                <th>Tipo</th>
                                <th>Fecha de registro</th>
                                <th>Última modificación</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="mdlRol" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmRol" name="frmRol" class="needs-validation" novalidate method="POST" action="javascript:configRol.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Rol - <strong>Nuevo</strong></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for="txtRol" class="form-label">Rol <span class="requerido">*</span></label>
                            <input type="text" id="txtRol" name="txtRol" placeholder="" title="" class="form-control" required value="">
                            <div class="invalid-feedback">
                                Ingrese el nombre del rol
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-12">
                            <label for="cboTipoRol" class="form-label">Tipo de rol <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboTipoRol" name="cboTipoRol" class="custom-select form-control" title="" required>
                                    <option selected value="0" disabled>seleccione</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione el tipo de rol
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$_GET["js"] = ["soporteGenerales", "soporteAdministracionRoles"];
