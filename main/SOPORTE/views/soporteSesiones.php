<div class="pagetitle">
    <h1>Sesiones activas</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">Soporte</li>
            <li class="breadcrumb-item">Usuarios</li>
            <li class="breadcrumb-item active">Sesiones</li>
        </ol>
    </nav>
</div>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <table id="tblUsuarios" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Foto</th>
                        <th>Username</th>
                        <th>Agencia actual</th>
                        <th>Nombre completo</th>
                        <th>Fecha de ingreso</th>
                        <th>Pantalla activa</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php
// echo json_encode($_SESSION['index']);
$_GET['js'] = ['soporteSesiones'];
