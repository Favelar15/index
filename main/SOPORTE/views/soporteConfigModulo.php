<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Configuración de módulo</h1>
        </div>
        <div class="col-3 topBtnContainer">
            <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configModulo.cancel();">
                <i class="fas fa-times"></i> <span>Cancelar</span>
            </button>
            <button class="btn btn-sm btn-outline-success float-end" title="Guardar" type="submit" form="frmModulo">
                <i class="fas fa-save"></i> <span>Guardar</span>
            </button>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Soporte</li>
            <li class="breadcrumb-item">Módulos</li>
            <li class="breadcrumb-item active">Configuración</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <form id="frmModulo" name="frmModulo" accept-charset="utf-8" action="javascript:configModulo.save()" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-3 col-xl-3 col-3 mayusculas">
                        <label for="txtTitulo" class="form-label">Título <span class="requerido">*</span></label>
                        <input type="text" id="txtTitulo" name="txtTitulo" class="form-control" required>
                        <div class="invalid-feedback">
                            Ingrese el título del módulo
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-6 col-6">
                        <label for="txtDescripcion" class="form-label">Descripción <span class="requerido">*</span></label>
                        <input type="text" id="txtDescripcion" name="txtDescripcion" class="form-control" required>
                        <div class="invalid-feedback">
                            Ingrese el título del módulo
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3 col-3 mayusculas">
                        <label for="txtContenedor" class="form-label">Contenedor <span class="requerido">*</span></label>
                        <input type="text" id="txtContenedor" name="txtContenedor" class="form-control" required>
                        <div class="invalid-feedback">
                            Ingrese el título del módulo
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-12 col-xl-12 mt-3">
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="apartados-tab" data-bs-toggle="tab" data-bs-target="#apartados" type="button" role="tab" aria-controls="apartados" aria-selected="true" onclick="configApartado.backApartados();">
                        Apartados
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link disabled" id="subApartados-tab" data-bs-toggle="tab" data-bs-target="#subApartados" type="button" role="tab" aria-controls="subApartados" aria-selected="true">
                        Sub-apartados
                    </button>
                </li>
            </ul>
            <div class="tab-content pt-2">
                <div class="tab-pane fade show active" id="apartados" role="tabpanel" aria-labelledby="apartados-tab">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="row">
                                <div class="col d-flex align-items-end">
                                    <label class="form-label">Todos los apartados</label>
                                </div>
                                <div class="col">
                                    <button type="button" class="btn btn-sm btn-outline-primary float-end" onclick="configApartado.edit({})">
                                        <i class="fas fa-plus"></i> Apartado
                                    </button>
                                </div>
                            </div>
                            <hr class="mt-0">
                            <div class="row">
                                <div class="col-12">
                                    <table id="tblApartados" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Tipo</th>
                                                <th>Título</th>
                                                <th>Descripción</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="row">
                                <div class="col d-flex align-items-end">
                                    <label class="form-label">Permisos - <strong id="stApartadoPermiso">(Selecciona un apartado)</strong></label>
                                </div>
                                <div class="col">
                                    <button type="button" id="btnPermisoApartado" class="btn btn-sm btn-outline-primary float-end" disabled onclick="configPermiso.edit();">
                                        <i class="fas fa-plus"></i> Permiso
                                    </button>
                                </div>
                            </div>
                            <hr class="mt-0">
                            <div class="row">
                                <div class="col-12">
                                    <table id="tblPermisosApartado" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Tipo</th>
                                                <th>Asignación</th>
                                                <th>Tipo de permiso</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="subApartados" role="tabpanel" aria-labelledby="subApartados-tab">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="row">
                                <div class="col d-flex align-items-end">
                                    <label class="form-label">Todos los sub-apartados</label>
                                </div>
                                <div class="col">
                                    <button type="button" class="btn btn-sm btn-outline-primary float-end" onclick="configApartado.edit({tipo:'Sub'})">
                                        <i class="fas fa-plus"></i> Sub-apartado
                                    </button>
                                </div>
                            </div>
                            <hr class="mt-0">
                            <div class="row">
                                <div class="col-12">
                                    <table id="tblSubApartados" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Tipo</th>
                                                <th>Título</th>
                                                <th>Descripción</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="row">
                                <div class="col d-flex align-items-end">
                                    <label class="form-label">Permisos - <strong id="stSubApartadoPermiso">(Selecciona un sub-apartado)</strong></label>
                                </div>
                                <div class="col-3">
                                    <button type="button" id="btnPermisoSubApartado" class="btn btn-sm btn-outline-primary float-end" disabled onclick="configPermiso.edit();">
                                        <i class="fas fa-plus"></i> Permiso
                                    </button>
                                </div>
                            </div>
                            <hr class="mt-0">
                            <div class="row">
                                <div class="col-12">
                                    <table id="tblPermisosSubApartado" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Tipo</th>
                                                <th>Asignación</th>
                                                <th>Tipo de permiso</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="mdlApartado" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <form id="frmApartado" class="needs-validation" name="frmApartado" method="POST" novalidate action="javascript:configApartado.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Configuración de apartado</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <label for='txtTituloApartado' class="form-label">Título <span class="requerido">*</span></label>
                            <input type="text" id="txtTituloApartado" name="txtTituloApartado" class="form-control" required>
                            <div class="invalid-feedback">
                                Ingrese el título
                            </div>
                        </div>
                        <div class="col-lg-8 col-xl-8">
                            <label for='txtDescripcionApartado' class="form-label">Descripción <span class="requerido">*</span></label>
                            <input type="text" id="txtDescripcionApartado" name="txtDescripcionApartado" class="form-control" required>
                            <div class="invalid-feedback">
                                Ingrese el título
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label for="cboTipoApartado" class="form-label">Tipo <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select name="cboTipoApartado" id="cboTipoApartado" class="form-select" required onchange="configApartado.evalTipo(this.value);">
                                    <option value="" selected disabled>seleccione</option>
                                    <option value="V">Vista</option>
                                    <option value="M">Manual</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione el tipo
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-xl-8">
                            <label for="txtSubNombreArchivo" class="form-label">Nombre de archivo <span class="requerido">*</span></label>
                            <input type="text" id="txtSubNombreArchivo" name="txtSubNombreArchivo" placeholder="" title="" class="form-control" required>
                            <div class="invalid-feedback">
                                Ingrese el nombre del archivo
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label for="txtClaseIcono" class="form-label">Clase CSS para icono <span class="requerido">*</span></label>
                            <input type="text" id="txtClaseIcono" name="txtClaseIcono" class="form-control" required onkeyup="configApartado.evalIcono(this.value);">
                            <div class="invalid-feedback">
                                Ingrese la clase CSS
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label class="form-label">Icono</label>
                            <div class="form-group">
                                <i class="" id="iconoApartado" style="font-size:32px;"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlPermiso" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <form id="frmPermiso" class="needs-validation" name="frmPermiso" method="POST" novalidate action="javascript:configPermiso.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Configuración de permiso</h5>
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item">Soporte</li>
                        <li class="breadcrumb-item">Módulos</li>
                        <li class="breadcrumb-item active">Configuración</li>
                    </ol>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <label for='cboTipoGrupo' class="form-label">Tipo de grupo <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboTipoGrupo" name="cboTipoGrupo" class="form-select" required onchange="configPermiso.evalGrupo(this.value);">
                                    <option value="" selected disabled>seleccione</option>
                                    <option value="ROL">Rol</option>
                                    <option value="USER">Usuario</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione el tipo de grupo
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label for='cboAsignacionPermiso' class="form-label">Asignación <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboAsignacionPermiso" name="cboAsignacionPermiso" class="form-control selectpicker" required data-live-search="true">
                                    <option value="" selected disabled>seleccione un grupo</option>
                                </select>
                                <!-- <div class="invalid-feedback">
                                    Seleccione la asignación
                                </div> -->
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label for="cboTipoPermiso" class="form-label">Tipo de permiso <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select name="cboTipoPermiso" id="cboTipoPermiso" class="form-select" required onchange="configApartado.evalTipo(this.value);">
                                    <option value="" selected disabled>seleccione</option>
                                    <option value="LECTURA">Lectura</option>
                                    <option value="ESCRITURA">Escritura</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione el tipo de permiso
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$_GET["js"] = ["soporteGenerales", "soporteConfigModulo"];
