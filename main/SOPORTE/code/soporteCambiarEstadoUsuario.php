<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = [];

if (count($input)) {
    include "../../code/connectionSqlServer.php";
    include "../../code/generalParameters.php";

    $id = $input["id"];
    $newState = $input["newState"];
    $query = "UPDATE sistema_datosUsuarios set bloqueado = :newState WHERE id = :identificador;";
    $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
    $result->bindParam(':newState', $newState, PDO::PARAM_STR);
    $result->bindParam(':identificador', $id, PDO::PARAM_INT);
    $result->execute();

    $respuesta['respuesta'] = 'EXITO';

    $conexion = null;
}

echo json_encode($respuesta);
