<?php
class modulo
{
    public $id;
    public $titulo;
    public $descripcion;
    public $contenedor;
    public $apartados;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function saveModulo($idUsuario, $ipActual)
    {
        $arrApartados = [];
        foreach ($this->apartados as $apartado) {
            $idApartado = $apartado["id"];
            $tituloApartado = $apartado["titulo"];
            $descripcionApartado = $apartado["descripcion"];
            $tipoApartado = $apartado["tipo"];
            $archivoApartado = $apartado["nombreArchivo"];
            $mantenimientoApartado = $apartado["mantenimiento"];
            $claseIcono = $apartado["claseIcono"];
            $subApartados = $apartado["subApartados"];
            $permisosApartado = $apartado["permisos"];

            $arrSubApartados = [];
            $arrPermisosApartado = [];

            foreach ($permisosApartado as $permisoApartado) {
                $idPermisoApartado = $permisoApartado["id"];
                $asignacion = $permisoApartado["asignacion"];
                $grupo = $permisoApartado["grupo"];
                $tipoPermiso = $permisoApartado["tipoPermiso"];

                array_push($arrPermisosApartado, $idPermisoApartado . '&&&' . $asignacion . '&&&' . $grupo . '&&&' . $tipoPermiso);
            }

            foreach ($subApartados as $sub) {
                $idDbSub = $sub["id"];
                $tituloSub = $sub["titulo"];
                $descripcionSub = $sub["descripcion"];
                $tipoSub = $sub["tipo"];
                $nombreArchivoSub = $sub["nombreArchivo"];
                $mantenimientoSub = $sub["mantenimiento"];
                $tmpPermisosSub = $sub["permisos"];
                $claseIconoSub = $sub["claseIcono"];
                $arrPermisosSub = [];

                foreach ($tmpPermisosSub as $permisoSub) {
                    $idDbPermisoSub = $permisoSub["id"];
                    $asignacion = $permisoSub["asignacion"];
                    $grupo = $permisoSub["grupo"];
                    $tipoPermiso = $permisoSub["tipoPermiso"];

                    array_push($arrPermisosSub, $idDbPermisoSub . '¿¿¿' . $asignacion . '¿¿¿' . $grupo . '¿¿¿' . $tipoPermiso);
                }

                $permisosSub = count($arrPermisosSub) > 0 ? implode("???", $arrPermisosSub) : 'null';

                array_push($arrSubApartados, $idDbSub . '&&&' . $tituloSub . '&&&' . $descripcionSub . '&&&' . $tipoSub . '&&&' . $nombreArchivoSub . '&&&' . $mantenimientoSub . '&&&' . $permisosSub . '&&&' . $claseIconoSub);
            }

            $permisosApartadoDb = count($arrPermisosApartado) > 0 ? implode("###", $arrPermisosApartado) : 'null';
            $subApartadosDb = count($arrSubApartados) > 0 ? implode("###", $arrSubApartados) : 'null';

            array_push($arrApartados, $idApartado . '%%%' . $tituloApartado . '%%%' . $descripcionApartado . '%%%' . $tipoApartado . '%%%' . $archivoApartado . '%%%' . $mantenimientoApartado . '%%%' . $permisosApartadoDb . '%%%' . $subApartadosDb . '%%%' . $claseIcono);
        }


        $apartadosDb = count($arrApartados) > 0 ? implode('@@@', $arrApartados) : '';
        $response = null;

        $query = "EXEC soporte_spGuardarModulo :id,:titulo,:descripcion,:contenedor,:apartados,:ip,:usuario,:response;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':titulo', $this->titulo, PDO::PARAM_STR);
        $result->bindParam(':descripcion', $this->descripcion, PDO::PARAM_STR);
        $result->bindParam(':contenedor', $this->contenedor, PDO::PARAM_STR);
        $result->bindParam(':apartados', $apartadosDb, PDO::PARAM_STR);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return ["respuesta" => $response];
        }

        $this->conexion = null;
        return ["respuesta" => "soporte_spGuardarModulo"];
    }

    public function getModulosAdmin()
    {
        $query = "SELECT * FROM soporte_viewModulosAdmin order by titulo;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        if ($result->execute()) {
            $datos = $result->fetchAll(PDO::FETCH_OBJ);
            $this->conexion = null;

            foreach ($datos as $key => $modulo) {
                $datos[$key]->fechaRegistro = date('d-m-Y h:i a', strtotime($datos[$key]->fechaRegistro));
                $datos[$key]->fechaActualizacion = date('d-m-Y h:i a', strtotime($datos[$key]->fechaActualizacion));
            }

            return $datos;
        }

        $this->conexion = null;
        return [];
    }

    public function getModulos(int $id = 0)
    {
        $condicion = $id > 0 ? ' WHERE id=' . $id : '';
        $query = "SELECT * FROM soporte_viewModulos" . $condicion . " ORDER BY titulo,tituloApartado;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $tmp = $result->fetchAll(PDO::FETCH_OBJ);
            $resultados = (object)[];

            foreach ($tmp as $row) {
                if (!isset($resultados->{$row->id})) {
                    $resultados->{$row->id} = (object)[];
                    $resultados->{$row->id}->id = $row->id;
                    $resultados->{$row->id}->titulo = $row->titulo;
                    $resultados->{$row->id}->descripcion = $row->descripcion;
                    $resultados->{$row->id}->contenedor = $row->contenedor;
                    $resultados->{$row->id}->apartados = [];
                }

                $apartado = (object)[];
                $apartado->id = $row->idApartado;
                $apartado->titulo = $row->tituloApartado;
                $apartado->descripcion = $row->descripcionApartado;
                $apartado->nombreArchivo = $row->nombreArchivo;
                $apartado->tipo = $row->tipo;
                $apartado->mantenimiento = $row->mantenimiento;
                $apartado->claseIcono = $row->iconoApartado;
                $apartado->permisos = (object)[];
                $apartado->subApartados = (object)[];

                $cntPermiso = 0;
                $arrPermisos = !empty($row->permisosApartado) ? explode('@@@', $row->permisosApartado) : [];

                foreach ($arrPermisos as $permiso) {
                    $cntPermiso++;
                    $tmpPermiso = (object)[];
                    $valores = explode('%%%', $permiso);

                    $tmpPermiso->id = $valores[0];
                    $tmpPermiso->grupo = $valores[1];
                    $tmpPermiso->asignacion = $valores[2];
                    $tmpPermiso->tipoPermiso = $valores[3];

                    $apartado->permisos->{$cntPermiso} = $tmpPermiso;
                }

                $cntSubApartado = 0;
                $arrSubApartados = !empty($row->subApartados) ? explode('@@@', $row->subApartados) : [];

                foreach ($arrSubApartados as $subApartado) {
                    $cntSubApartado++;
                    $tmpSubApartado = (object)[];
                    $valores = explode('%%%', $subApartado);

                    $tmpSubApartado->id = $valores[0];
                    $tmpSubApartado->titulo = $valores[1];
                    $tmpSubApartado->descripcion = $valores[2];
                    $tmpSubApartado->tipo = $valores[3];
                    $tmpSubApartado->nombreArchivo = $valores[4];
                    $tmpSubApartado->mantenimiento = $valores[5];
                    $tmpSubApartado->claseIcono = $valores[6];
                    $tmpSubApartado->permisos = (object)[];

                    $cntPermiso = 0;
                    $arrPermisos = strlen($valores[7]) > 0 ? explode('???', $valores[7]) : [];

                    foreach ($arrPermisos as $permiso) {
                        $cntPermiso++;
                        $tmpPermiso = (object)[];
                        $valores = explode('¿¿¿', $permiso);

                        if (count($valores) == 4) {
                            $tmpPermiso->id = $valores[0];
                            $tmpPermiso->grupo = $valores[1];
                            $tmpPermiso->asignacion = $valores[2];
                            $tmpPermiso->tipoPermiso = $valores[3];

                            $tmpSubApartado->permisos->{$cntPermiso} = $tmpPermiso;
                        }
                    }

                    $apartado->subApartados->{$cntSubApartado} = $tmpSubApartado;
                }

                $resultados->{$row->id}->apartados[] = $apartado;
            }

            $this->conexion = null;

            return $resultados;
        }

        $this->conexion = null;
        return new stdClass();
    }
}
