<?php
class contactoDirectorio
{
    public $id;
    public $nombres;
    public $apellidos;
    public $idAgencia;
    public $idCargo;
    public $discord;
    public $telefonos;
    public $emails;
    public $fechaRegistro;
    public $fechaActualizacion;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
        if (!empty($this->telefonos)) {
            $tmpTelefonos = explode('@@@', $this->telefonos);
            $this->telefonos = [];
            foreach ($tmpTelefonos as $telefono) {
                $valores = explode("%%%", $telefono);
                $this->telefonos[] = new telefonoContacto($valores[0], $valores[1]);
            }
        }

        if (!empty($this->emails)) {
            $tmpEmails = explode('@@@', $this->emails);
            $this->emails = [];
            foreach ($tmpEmails as $email) {
                $valores = explode("%%%", $email);
                $this->emails[] = new emailContacto($valores[0], $valores[1]);
            }
        }
    }

    public function saveContacto($idUsuario, $ipActual, $tipoApartado, $idApartado)
    {
        $response = null;
        $id = null;
        $query = "EXEC soporte_spGuardarContactoDirectorio :id,:nombres,:apellidos,:agencia,:cargo,:discord,:telefonos,:emails,:usuario,:ip,:tipoApartado,:idApartado,:response,:idContacto;";
        $tmpTelefonos = [];
        $tmpEmails = [];

        foreach ($this->telefonos as $telefono) {
            array_push($tmpTelefonos, $telefono["id"] . "%%%" . $telefono["telefono"]);
        }

        foreach ($this->emails as $email) {
            array_push($tmpEmails, $email["id"] . "%%%" . $email["email"]);
        }

        $telefonosDb = count($tmpTelefonos) > 0 ? implode("&&&", $tmpTelefonos) : '';
        $emailsDb = count($tmpEmails) > 0 ? implode("&&&", $tmpEmails) : '';

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':nombres', $this->nombres, PDO::PARAM_STR);
        $result->bindParam(':apellidos', $this->apellidos, PDO::PARAM_STR);
        $result->bindParam(':agencia', $this->idAgencia, PDO::PARAM_INT);
        $result->bindParam(':cargo', $this->idCargo, PDO::PARAM_INT);
        $result->bindParam(':discord', $this->discord, PDO::PARAM_STR);
        $result->bindParam(':telefonos', $telefonosDb, PDO::PARAM_STR);
        $result->bindParam(':emails', $emailsDb, PDO::PARAM_STR);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idContacto', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return ["respuesta" => $response, "id" => $id];
        }

        $this->conexion = null;
        return ["respuesta" => "Error en soporte_spGuardarContactoDirectorio"];
    }

    public function getContactos()
    {
        $query = "SELECT * FROM soporte_viewContactosDirectorio;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

    public function eliminarContactoDirectorio(int $idUsuario, string $tipoApartado,int $idApartado)
    {
        $response = null;
        $query = "EXEC soporte_spEliminarContacto :id,:usuario,:tipoApartado,:idApartado,:response;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return $response;
        }

        $this->conexion = null;
        return "Error en soporte_spEliminarContacto";
    }
}

class telefonoContacto
{
    public $id;
    public $telefono;

    function __construct($id, $telefono)
    {
        $this->id = $id;
        $this->telefono = $telefono;
    }
}

class emailContacto
{
    public $id;
    public $email;

    function __construct($id, $email)
    {
        $this->id = $id;
        $this->email = $email;
    }
}
