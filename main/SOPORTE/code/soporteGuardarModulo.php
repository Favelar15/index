<?php
require_once "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/modulo.php';

        $idUsuario = $_SESSION["index"]->id;

        $modulo = new modulo();
        $modulo->id = isset($input["id"]) ? $input["id"] : 0;
        $modulo->titulo = $input["titulo"];
        $modulo->descripcion = $input["descripcion"];
        $modulo->contenedor = $input["contenedor"];
        $modulo->apartados = $input["apartados"];

        $guardar = $modulo->saveModulo($idUsuario,$ipActual);

        if(isset($guardar["respuesta"])){
            $respuesta->{"respuesta"} = $guardar["respuesta"];
            isset($guardar["procesados"]) && $respuesta->{"procesados"} = $guardar["procesados"];
        }
        else{
            $respuesta->{"respuesta"} = "Error al conectar con la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
