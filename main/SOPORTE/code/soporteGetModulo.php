<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/modulo.php';

        $id = $_GET["q"];

        $modulo = new modulo($id);
        $datos = $modulo->getModulos();

        $respuesta->{"modulo"} = $datos;

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
