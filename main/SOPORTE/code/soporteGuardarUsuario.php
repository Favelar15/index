<?php
require_once '../../code/generalParameters.php';
$respuesta = (object)[];

if (!empty($_POST)) {
    session_start();
    if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
        include '../../code/connectionSqlServer.php';
        $usuario = new usuario();
        $usuario->id = $_POST["id"];
        $usuario->nombres = $_POST["nombres"];
        $usuario->apellidos = $_POST["apellidos"];
        $usuario->email = $_POST["email"];
        $usuario->username = $_POST["username"];
        $usuario->agencias = $_POST["agencias"];
        $usuario->roles = $_POST["roles"];

        $password = !empty($_POST['password']) ? hash('sha256', $_POST["password"]) : '';
        $detArchivo = isset($_FILES['imagen']) ? pathinfo($_FILES['imagen']['name']) : pathinfo('avatar.jpg');
        $extension = $detArchivo['extension'];
        $nombreFoto = $_POST['nombreImagen'] != 'avatar.jpg' ? $_POST['nombreImagen'] : 'avatar.jpg';

        $usuario->foto = $nombreFoto;

        $guardar = $usuario->saveUsuario($password, $extension);

        if (isset($guardar["respuesta"])) {
            $respuesta->{"respuesta"} = $guardar["respuesta"];
            if ($guardar["respuesta"] == "EXITO" && isset($_FILES["imagen"])) {
                $nombreArchivo = $guardar["nombreArchivo"];
                if (file_exists('../../img/users/' . $nombreArchivo)) {
                    unlink('../../img/users/' . $nombreArchivo);
                }

                if (!move_uploaded_file($_FILES['imagen']['tmp_name'], '../../img/users/' . $nombreArchivo)) {
                    $respuesta->{'respuesta'} = 'ERROR_UPLOAD';
                }
            }
        } else {
            $respuesta->{"respuesta"} = "Error al conectar con la base de datos";
        }

        $conexion = null;
    } else {
        $respuesta->{"respuesta"} = "SESION";
    }
}

echo json_encode($respuesta);
