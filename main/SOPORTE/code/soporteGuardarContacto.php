<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/contactoDirectorio.php';

        $idUsuario = $_SESSION["index"]->id;
        $tipoApartado = $input["tipoApartado"];
        $idApartado = base64_decode(urldecode($input["idApartado"]));

        $contacto = new contactoDirectorio();
        $contacto->id = $input["id"];
        $contacto->nombres = $input["nombres"];
        $contacto->apellidos = $input["apellidos"];
        $contacto->idAgencia = $input["idAgencia"];
        $contacto->idCargo = $input["idCargo"];
        $contacto->discord = $input["discord"];
        $contacto->telefonos = $input["telefonos"];
        $contacto->emails = $input["emails"];

        $guardar = $contacto->saveContacto($idUsuario, $ipActual, $tipoApartado, $idApartado);

        if (isset($guardar["respuesta"])) {
            $respuesta->{"respuesta"} = $guardar["respuesta"];
            isset($guardar["id"]) && $respuesta->{"id"} = $guardar["id"];
        } else {
            $respuesta->{"respuesta"} = "Error al guardar en la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
