<?php
header("Content-type: application/json; charset=utf-8");
$respuesta = [];

if (!empty($_GET) && isset($_GET['q'])) {
    include '../../code/connectionSqlServer.php';
    include '../../code/generalParameters.php';

    $valores = explode('@@@', $_GET["q"]);

    $id = $valores[0];
    $username = $valores[1];

    $query = "SELECT COUNT(*) as total FROM sistema_viewDatosUsuarios where username = :username AND id != :id;";
    $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
    $result->bindParam(':id', $id, PDO::PARAM_INT);
    $result->bindParam(':username', $username, PDO::PARAM_STR);
    $result->execute();

    $data = $result->fetch(PDO::FETCH_ASSOC);
    if (intval($data['total']) > 0) {
        $respuesta['respuesta'] = 'ASIGNADO';
    } else {
        $respuesta['respuesta'] = 'DISPONIBLE';
    }

    $conexion = null;
}

echo json_encode($respuesta);
