<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";

        $query = 'SELECT * FROM sistema_viewSesionesUsuarios;';
        $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        $usuarios = $result->fetchAll(PDO::FETCH_OBJ);

        foreach ($usuarios as $key => $user) {
            $valores = explode('%%%', $user->fechaIngreso);
            $usuarios[$key]->pantallaActiva = 'MAIN';
            if (count($valores) == 2) {
                $usuarios[$key]->idIngreso = $valores[0];
                $usuarios[$key]->fechaIngreso = date('d-m-Y h:i a', strtotime($valores[1]));

                $queryUbicacion = 'SELECT top(1) * FROM sistema_detUbicacionUsuarios where idUsuario = :user and idIngreso = :ingreso order by fechaRegistro desc;';
                $resultUbicacion = $conexion->prepare($queryUbicacion, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultUbicacion->bindParam(':user', $user->id, PDO::PARAM_INT);
                $resultUbicacion->bindParam(':ingreso', $valores[0], PDO::PARAM_INT);
                $resultUbicacion->execute();
                if($resultUbicacion->rowCount() > 0){
                    $data = $resultUbicacion->fetchAll(PDO::FETCH_OBJ)[0];
                    $tipoApartado = $data->tipoApartado;
                    $idApartado = $data->idApartado;

                    $campo = $tipoApartado == 'Sub' ? 'idSubApartado' : 'idApartado';

                    $queryPantalla = 'SELECT top(1) * from soporte_viewModulosApartadosSubApartados where '.$campo.' = :idApartado';
                    $resultPantalla = $conexion->prepare($queryPantalla,[PDO::ATTR_CURSOR=>PDO::CURSOR_SCROLL]);
                    $resultPantalla->bindParam(':idApartado',$idApartado,PDO::PARAM_INT);
                    $resultPantalla->execute();

                    if($resultPantalla->rowCount() > 0){
                        $dataPantalla = $resultPantalla->fetchAll(PDO::FETCH_OBJ)[0];
                        $pantallaActual = $dataPantalla->titulo.'->'.$dataPantalla->tituloApartado;
                        $tipoApartado == 'Sub' && $pantallaActual.='->'.$dataPantalla->tituloSubApartado;

                        $usuarios[$key]->pantallaActiva = $pantallaActual;
                    }
                }
            }
        }

        $respuesta->{'usuarios'} = $usuarios;

        $conexion = null;
    }
} else {
    $respuesta->{'respuesta'} = 'EXITO';
}

echo json_encode($respuesta);
