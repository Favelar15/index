<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = [];

if (count($input) > 0) {
    include "../../code/connectionSqlServer.php";

    $id = $input['id'];

    $query = "UPDATE soporte_datosModulos set habilitado = 'N' WHERE id = :id;";

    $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
    $result->bindParam(':id', $id, PDO::PARAM_INT);
    $result->execute();

    $conexion = null;
}

echo json_encode($respuesta);