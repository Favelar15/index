<?php
require_once "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include "../../code/connectionSqlServer.php";
        require_once '../../code/Models/rol.php';

        $idUsuario = $_SESSION["index"]->id;
        $tipoApartado = $input["tipoApartado"];
        $idApartado = base64_decode(urldecode($input["idApartado"]));

        $rol = new rol();
        $rol->id = $input["id"];
        $rol->rol = $input["rol"];
        $rol->tipoRol = $input["tipoRol"];

        $guardar = $rol->saveRol($idUsuario, $tipoApartado, $idApartado);

        if (isset($guardar["respuesta"])) {
            $respuesta->{"respuesta"} = $guardar["respuesta"];
            isset($guardar["id"]) && $respuesta->{"id"} = $guardar["id"];
            isset($guardar["fechaRegistro"]) && $respuesta->{"fechaRegistro"} = $guardar["fechaRegistro"];
            isset($guardar["fechaActualizacion"]) && $respuesta->{"fechaActualizacion"} = $guardar["fechaActualizacion"];
        } else {
            $respuesta->{"respuesta"} = "Error al conectar con la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
