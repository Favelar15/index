<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";

        $solicitados = explode("@@@", $_GET["q"]);

        if (in_array('tiposRol', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/tipoRol.php';
            $tipoRol = new tipoRol();
            $tiposRol = $tipoRol->getTiposRol();

            $respuesta->{"tiposRol"} = $tiposRol;
        }

        if (in_array('roles', $solicitados) || in_array('rolesCbo', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/rol.php';
            $rol = new rol();
            if (in_array('rolesCbo', $solicitados)) {
                $roles = $rol->getRolesCbo();
            } else {
                $roles = $rol->getRoles();
            }

            $respuesta->{"roles"} = $roles;
        }


        if (in_array('agencias', $solicitados) || in_array('agenciasCbo', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/agencia.php';
            $agencia = new agencia();
            if (in_array('agenciasCbo', $solicitados)) {
                $agencias = $agencia->getAgenciasCbo();
            } else {
                //pendiente
            }

            $respuesta->{"agencias"} = $agencias;
        }

        if (in_array('usuariosCbo', $solicitados) || in_array('usuarios', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/usuario.php';

            $usuario = new usuario();
            if (in_array('usuariosCbo', $solicitados)) {
                $usuarios = $usuario->getUsersCbo();
            } else {
                $usuarios = $usuario->getUsers();
            }

            $respuesta->{"usuarios"} = $usuarios;
        }

        if (in_array('modulosAdmin', $solicitados)) {
            require_once './Models/modulo.php';
            $modulo = new modulo();
            $modulos = $modulo->getModulosAdmin();

            $respuesta->{"modulos"} = $modulos;
        }

        if (in_array('cargosDirectorio', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/cargoDirectorio.php';
            $cargo = new cargoDirectorio();
            $cargos = $cargo->getCargos();

            $respuesta->{"cargos"} = $cargos;
        }

        if (in_array('contactosDirectorio', $solicitados)) {
            require_once './Models/contactoDirectorio.php';
            $contacto = new contactoDirectorio();
            $contactos = $contacto->getContactos();

            $respuesta->{"contactosDirectorio"} = $contactos;
        }


        $conexion = null;
    }
}

echo json_encode($respuesta);
