let tblApartados,
    tblPermisosApartado,
    tblSubApartados,
    tblPermisosSubApartado;

window.onload = () => {
    initTables().then(() => {
        $('#tblApartados tbody').on('click', 'td', function () {
            if ($(this).closest('tr').attr("id") && $(this).index() != 4) {
                if ($(this).closest('tr').hasClass('selected')) {
                    $(this).closest('tr').removeClass('selected');
                    configPermiso.clean({
                        tipo: 'Apartado'
                    });
                } else {
                    tblApartados.$('tr.selected').removeClass('selected');
                    $(this).closest('tr').addClass('selected');
                    let id = $(this).closest('tr').attr("id").replace("trApartado", "");
                    configPermiso.init({
                        id,
                        tipo: 'Apartado'
                    });
                }
            }
        });

        $('#tblSubApartados tbody').on('click', 'td', function () {
            if ($(this).closest('tr').attr("id") && $(this).index() != 4) {
                if ($(this).closest('tr').hasClass('selected')) {
                    $(this).closest('tr').removeClass('selected');
                    configPermiso.clean({
                        tipo: 'Sub'
                    });
                } else {
                    tblSubApartados.$('tr.selected').removeClass('selected');
                    $(this).closest('tr').addClass('selected');
                    let id = $(this).closest('tr').attr("id").replace("trSubApartado", "");
                    configPermiso.init({
                        id,
                        tipo: 'Sub'
                    });
                }
            }
        });

        $(".preloader").fadeIn("fast");
        document.getElementById("frmModulo").reset();
        fetchActions.getCats({
            modulo: "SOPORTE",
            archivo: "soporteGetCats",
            solicitados: ['rolesCbo', 'usuariosCbo']
        }).then(({
            roles,
            usuarios
        }) => {
            roles.forEach(rol => configPermiso.roles[rol.id] = {
                ...rol
            });
            usuarios.forEach(usuario => configPermiso.usuarios[usuario.id] = {
                ...usuario
            });
            $(".preloader").fadeOut("fast");

            let idModulo = getParameterByName('id');
            idModulo.length > 0 && (configModulo.init(atob(decodeURIComponent(idModulo))));
        }).catch(generalMostrarError);
    });
}


function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 350;

            if ($("#tblApartados").length) {
                tblApartados = $("#tblApartados").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2],
                        "className": "text-start"
                    }],
                    paging: false,
                    sortable: true,
                    searching: false,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de apartado';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblApartados.columns.adjust().draw();
            }

            if ($("#tblPermisosApartado").length) {
                tblPermisosApartado = $("#tblPermisosApartado").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2],
                        "className": "text-start"
                    }],
                    sortable: true,
                    paging: false,
                    searching: false,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Parámetros de permiso';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblPermisosApartado.columns.adjust().draw();
            }

            if ($("#tblSubApartados").length) {
                tblSubApartados = $("#tblSubApartados").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2],
                        "className": "text-start"
                    }],
                    sortable: true,
                    paging: false,
                    searching: false,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de sub-apartado';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblSubApartados.columns.adjust().draw();
            }

            if ($("#tblPermisosSubApartado").length) {
                tblPermisosSubApartado = $("#tblPermisosSubApartado").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2],
                        "className": "text-start"
                    }],
                    sortable: true,
                    paging: false,
                    searching: false,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Parámetros de permiso';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblPermisosSubApartado.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            console.error(err.message);
            reject();
        }
    });
}

const configModulo = {
    id: 0,
    apartados: {},
    campoTitulo: document.getElementById('txtTitulo'),
    campoDescripcion: document.getElementById('txtDescripcion'),
    campoContenedor: document.getElementById('txtContenedor'),
    init: function (id = 0) {
        this.id = id;
        fetchActions.getCats({
            modulo: "SOPORTE",
            archivo: "soporteGetModulo",
            solicitados: [id]
        }).then(({
            modulo
        }) => {
            let tmp = {
                ...modulo[id]
            };
            this.campoTitulo.value = tmp.titulo;
            this.campoDescripcion.value = tmp.descripcion;
            this.campoContenedor.value = tmp.contenedor;
            configApartado.init([...tmp.apartados]);
        }).catch(generalMostrarError);
    },
    save: function () {
        formActions.validate().then(({
            errores
        }) => {
            if (errores == 0) {
                let datos = {
                    id: this.id,
                    titulo: this.campoTitulo.value,
                    descripcion: this.campoDescripcion.value,
                    contenedor: this.campoContenedor.value,
                    apartados: this.apartados,
                };

                fetchActions.set({
                    modulo: "SOPORTE",
                    archivo: 'soporteGuardarModulo',
                    datos
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                let urlModulo = encodeURIComponent(btoa('SOPORTE'));
                                window.location.href = '?page=soporteAdministracionModulos&mod=' + urlModulo;
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Salir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                let urlModulo = encodeURIComponent(btoa('SOPORTE'));
                window.location.href = `?page=soporteAdministracionModulos&mod=${urlModulo}`;
            }
        });
    }
}

const configApartado = {
    id: 0,
    idSub: 0,
    cnt: 0,
    tipo: 'Apartado',
    apartadosTab: document.getElementById('apartados-tab'),
    subApartadosTab: document.getElementById("subApartados-tab"),
    campoTitulo: document.getElementById("txtTituloApartado"),
    campoDescripcion: document.getElementById("txtDescripcionApartado"),
    cboTipoApartado: document.getElementById("cboTipoApartado"),
    campoNombreArchivo: document.getElementById("txtSubNombreArchivo"),
    campoClaseIcono: document.getElementById("txtClaseIcono"),
    tmpSubApartados: {},
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                this.cnt = 0;
                tmpDatos.forEach(apartado => {
                    this.cnt++;
                    configModulo.apartados[this.cnt] = {
                        ...apartado
                    };
                    this.addRowTbl({
                        ...apartado,
                        idFila: this.cnt
                    });
                });
                tblApartados.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function ({
        id = 0,
        idSub = 0,
        tipo = 'Apartado'
    }) {
        formActions.clean('frmApartado').then(() => {
            this.tipo = tipo;
            this.tipo == 'Apartado' ? (this.id = id) : (this.idSub = idSub);
            let tmpId = tipo == 'Apartado' ? this.id : this.idSub;

            if (tmpId > 0) {
                let tmp = this.tipo == 'Apartado' ? {
                    ...configModulo.apartados[this.id]
                } : {
                    ...this.tmpSubApartados[this.idSub]
                };
                this.campoTitulo.value = tmp.titulo;
                this.campoDescripcion.value = tmp.descripcion;
                this.cboTipoApartado.value = tmp.tipo;
                this.campoNombreArchivo.value = tmp.nombreArchivo;
                this.campoClaseIcono.value = tmp.claseIcono;
            }

            $("#" + this.campoClaseIcono.id).trigger("keyup");
            $("#mdlApartado").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmApartado').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.tipo == 'Apartado' ? (configModulo.apartados[this.id] ? configModulo.apartados[this.id].id : 0) : (configModulo.apartados[this.id].subApartados[this.idSub] ? configModulo.apartados[this.id].subApartados[this.idSub].id : 0),
                    titulo: this.campoTitulo.value,
                    descripcion: this.campoDescripcion.value,
                    tipo: this.cboTipoApartado.value,
                    nombreArchivo: this.campoNombreArchivo.value,
                    permisos: this.tipo == 'Apartado' ? (configModulo.apartados[this.id] ? configModulo.apartados[this.id].permisos : {}) : (configModulo.apartados[this.id].subApartados[this.idSub] ? configModulo.apartados[this.id].subApartados[this.idSub].permisos : {}),
                    mantenimiento: this.tipo == 'Apartado' ? (configModulo.apartados[this.id] ? configModulo.apartados[this.id].mantenimiento : 'N') : (configModulo.apartados[this.id].subApartados[this.idSub] ? configModulo.apartados[this.id].subApartados[this.idSub].mantenimiento : 'N'),
                    subApartados: this.tipo == 'Apartado' ? (configModulo.apartados[this.id] ? configModulo.apartados[this.id].subApartados : {}) : {},
                    claseIcono: this.campoClaseIcono.value
                };
                let guardando = new Promise((resolve, reject) => {
                    if (this.tipo == 'Apartado') {
                        if (this.id == 0) {
                            this.cnt++;
                            this.id = this.cnt;
                            this.addRowTbl({
                                ...tmp,
                                idFila: this.cnt
                            }).then(resolve).catch(reject);
                        } else {
                            let labelTipo = tmp.tipo == 'V' ? 'Vista' : 'Manual';
                            $("#trApartado" + this.id).find("td:eq(1)").html(labelTipo);
                            $("#trApartado" + this.id).find("td:eq(2)").html(tmp.titulo);
                            $("#trApartado" + this.id).find("td:eq(3)").html(tmp.descripcion);
                            $("#trApartado" + this.id).find("td:eq(3)").trigger('click');
                            resolve();
                        }
                    } else {
                        if (this.idSub == 0) {
                            this.configSubApartados.cnt++;
                            this.idSub = this.configSubApartados.cnt;
                            this.addRowTbl({
                                ...tmp,
                                idFila: this.configSubApartados.cnt
                            }).then(resolve).catch(reject);
                        } else {
                            let labelTipo = tmp.tipo == 'V' ? 'Vista' : 'Manual';
                            $("#trSubApartado" + this.idSub).find("td:eq(1)").html(labelTipo);
                            $("#trSubApartado" + this.idSub).find("td:eq(2)").html(tmp.titulo);
                            $("#trSubApartado" + this.idSub).find("td:eq(3)").html(tmp.descripcion);
                            $("#trSubApartado" + this.idSub).find("td:eq(3)").trigger('click');
                            resolve();
                        }
                    }
                });

                guardando.then(() => {
                    if (this.tipo == 'Apartado') {
                        configModulo.apartados[this.id] = {
                            ...tmp
                        };
                        tblApartados.columns.adjust().draw();
                    } else {
                        this.tmpSubApartados[this.idSub] = {
                            ...tmp
                        }

                        configModulo.apartados[this.id].subApartados = {
                            ...this.tmpSubApartados
                        };
                        tblSubApartados.columns.adjust().draw();
                    }
                    $("#mdlApartado").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        tipo,
        titulo,
        descripcion,
        mantenimiento
    }) {
        return new Promise((resolve, reject) => {
            try {
                let labelTipo = tipo == 'M' ? 'Manual' : 'Vista',
                    iconoMantenimiento = mantenimiento == 'N' ? 'fa-tools' : 'fa-check',
                    tituloMantenimiento = mantenimiento == 'N' ? 'Iniciar mantenimiento' : 'Terminar mantenimiento';
                if (this.tipo == 'Apartado') {
                    tblApartados.row.add([
                        idFila,
                        labelTipo,
                        titulo,
                        descripcion,
                        "<div class='tblButtonContainer'>" +
                        "<span class='btnTbl' title='Editar' onclick='configApartado.edit({id:" + idFila + "});'>" +
                        "<i class='fas fa-edit'></i>" +
                        "</span>" +
                        "<span class='btnTbl' title='Configurar sub-apartados' onclick='configApartado.configSubApartados.init({id:" + idFila + "});'>" +
                        "<i class='fas fa-cogs'></i>" +
                        "</span>" +
                        "<span class='btnTbl' title='" + tituloMantenimiento + "' onclick='configApartado.mantenimiento(" + idFila + ",\"" + this.tipo + "\",this);'>" +
                        "<i class='fas " + iconoMantenimiento + "'></i>" +
                        "</span>" +
                        "<span class='btnTbl' title='Eliminar' onclick='configApartado.delete({id:" + idFila + "});'>" +
                        "<i class='fas fa-trash'></i>" +
                        "</span>" +
                        "</div>"
                    ]).node().id = 'trApartado' + idFila;
                } else {
                    tblSubApartados.row.add([
                        idFila,
                        labelTipo,
                        titulo,
                        descripcion,
                        "<div class='tblButtonContainer'>" +
                        "<span class='btnTbl' title='Editar' onclick='configApartado.edit({idSub:" + idFila + ",tipo:\"Sub\"});'>" +
                        "<i class='fas fa-edit'></i>" +
                        "</span>" +
                        "<span class='btnTbl' title='" + tituloMantenimiento + "' onclick='configApartado.mantenimiento(" + idFila + ",\"" + this.tipo + "\",this);'>" +
                        "<i class='fas " + iconoMantenimiento + "'></i>" +
                        "</span>" +
                        "<span class='btnTbl' title='Eliminar' onclick='configApartado.delete({id:" + idFila + ",tipo:\"Sub\"});'>" +
                        "<i class='fas fa-trash'></i>" +
                        "</span>" +
                        "</div>"
                    ]).node().id = 'trSubApartado' + idFila;
                }
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function ({
        id = 0,
        tipo = 'Apartado'
    }) {
        let label = tipo == 'Apartado' ? configModulo.apartados[id].titulo : configModulo.apartados[this.id].subApartados[id].titulo,
            labelTipo = tipo == 'Apartado' ? 'Apartado' : 'Sub-apartado';
        Swal.fire({
            title: `Eliminar ${labelTipo}`,
            icon: 'warning',
            html: `¿Seguro/a que quieres <b>eliminar</b> el ${labelTipo} <strong>${label}</strong>?.`,
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                if (tipo == 'Apartado') {
                    tblApartados.row('#trApartado' + id).remove().draw();
                    delete configModulo.apartados[id];
                    configPermiso.clean({});
                } else {
                    tblSubApartados.row('#trSubApartado' + id).remove().draw();
                    delete configModulo.apartados[this.id].subApartados[id];
                    configPermiso.clean({
                        tipo: 'Sub'
                    });
                }
            }
        });
    },
    evalTipo: function (value) {
        this.campoNombreArchivo.required = value == 'M' ? false : true;
    },
    mantenimiento: function (id = 0, tipo = 'Apartado', spn) {
        let tmp = tipo == 'Apartado' ? configModulo.apartados[id] : configModulo.apartados[this.id].subApartados[id],
            newState = tmp.mantenimiento == 'S' ? 'N' : 'S',
            newIcon = tmp.mantenimiento == 'N' ? 'fas fa-check' : 'fas fa-tools',
            newTitle = tmp.mantenimiento == 'N' ? 'Terminar mantenimiento' : 'Iniciar mantenimiento';

        if (tipo == 'Apartado') {
            configModulo.apartados[id].mantenimiento = newState;
        } else {
            configModulo.apartados[this.id].subApartados[id].mantenimiento = newState;
        }

        spn.querySelector('i').className = newIcon;
        spn.title = newTitle;
    },
    configSubApartados: {
        cnt: 0,
        init: function ({
            id = 0
        }) {
            configApartado.id = id;
            this.cnt = 0;
            tblSubApartados.clear().draw();
            configPermiso.clean({
                tipo: 'Sub'
            });
            configApartado.tmpSubApartados = {};
            configApartado.subApartadosTab.classList.remove('disabled');
            $("#" + configApartado.subApartadosTab.id).trigger("click");

            let tmp = {
                ...configModulo.apartados[configApartado.id].subApartados
            };

            configApartado.tipo = 'Sub';

            for (let key in tmp) {
                this.cnt++;
                configApartado.tmpSubApartados[this.cnt] = {
                    ...tmp[key]
                };
                configApartado.addRowTbl({
                    ...tmp[key],
                    idFila: this.cnt
                });
            }

            tblSubApartados.columns.adjust().draw();
        },
    },
    backApartados: function () {
        if ($("#" + this.subApartadosTab.id).is(":visible")) {
            this.subApartadosTab.classList.add('disabled');
            this.id = 0;
        }
    },
    evalIcono: function (clase) {
        document.getElementById("iconoApartado").className = clase;
    }
}

const configPermiso = {
    id: 0,
    idSub: 0,
    idPermiso: 0,
    cnt: 0,
    tipo: 'Apartado',
    roles: {},
    usuarios: {},
    labelApartado: document.getElementById("stApartadoPermiso"),
    labelSubApartado: document.getElementById("stSubApartadoPermiso"),
    btnPermiso: document.getElementById("btnPermisoApartado"),
    btnSubPermiso: document.getElementById("btnPermisoSubApartado"),
    cboGrupo: document.getElementById("cboTipoGrupo"),
    cboAsignacion: document.getElementById("cboAsignacionPermiso"),
    cboTipoPermiso: document.getElementById("cboTipoPermiso"),
    tmpPermisos: {},
    tmpAsignacion: 0,
    init: function ({
        id = 0,
        tipo = 'Apartado'
    }) {
        this.cnt = 0;
        this.tmpPermisos = {};
        this.tipo = tipo;
        if (tipo == 'Apartado') {
            this.id = id;
            tblPermisosApartado.clear().draw();
            let tmp = {
                ...configModulo.apartados[id]
            };
            this.btnPermiso.disabled = false;
            this.labelApartado.innerHTML = `(${tmp.titulo})`;


            for (let key in tmp.permisos) {
                this.cnt++;
                let labelAsignacion = tmp.permisos[key].grupo == 'ROL' ? this.roles[tmp.permisos[key].asignacion].rol : this.usuarios[tmp.permisos[key].asignacion].nombreCompleto;
                this.tmpPermisos[this.cnt] = {
                    ...tmp.permisos[key],
                    labelAsignacion
                };

                this.addRowTbl({
                    ...tmp.permisos[key],
                    idFila: this.cnt,
                    labelAsignacion
                });
            }

            tblPermisosApartado.columns.adjust().draw();
        } else {
            this.idSub = id;
            tblPermisosSubApartado.clear().draw();
            let tmp = {
                ...configModulo.apartados[configApartado.id].subApartados[id]
            };

            this.btnSubPermiso.disabled = false;
            this.labelSubApartado.innerHTML = `(${tmp.titulo})`;

            for (let key in tmp.permisos) {
                this.cnt++;
                let labelAsignacion = tmp.permisos[key].grupo == 'ROL' ? this.roles[tmp.permisos[key].asignacion].rol : this.usuarios[tmp.permisos[key].asignacion].nombreCompleto;
                this.tmpPermisos[this.cnt] = {
                    ...tmp.permisos[key],
                    labelAsignacion
                };

                this.addRowTbl({
                    ...tmp.permisos[key],
                    idFila: this.cnt,
                    labelAsignacion
                });
            }

            tblPermisosSubApartado.columns.adjust().draw();
        }
    },
    edit: function (id = 0) {
        formActions.clean('frmPermiso').then(() => {
            this.idPermiso = id;
            this.tmpAsignacion = 0;

            if (this.idPermiso > 0) {
                let tmp = this.tmpPermisos[this.idPermiso];
                this.tmpAsignacion = tmp.asignacion;

                this.cboGrupo.value = tmp.grupo;
                $("#" + this.cboGrupo.id).trigger("change");
                this.cboAsignacion.value = tmp.asignacion;
                $("#" + this.cboAsignacion.id).selectpicker("refresh");
                this.cboTipoPermiso.value = tmp.tipoPermiso;
            }

            $("#mdlPermiso").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmPermiso').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.tipo == 'Apartado' ? (this.tmpPermisos[this.idPermiso] ? this.tmpPermisos[this.idPermiso].id : 0) : (this.tmpPermisos[this.idPermiso] ? this.tmpPermisos[this.idPermiso].id : 0),
                    grupo: this.cboGrupo.value,
                    asignacion: this.cboAsignacion.value,
                    labelAsignacion: this.cboAsignacion.options[this.cboAsignacion.options.selectedIndex].text,
                    tipoPermiso: this.cboTipoPermiso.value,
                };

                let guardando = new Promise((resolve, reject) => {
                    if (this.idPermiso == 0) {
                        this.cnt++;
                        this.idPermiso = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            idFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        if (this.tipo == 'Apartado') {
                            $("#trPermisoApartado" + this.idPermiso).find("td:eq(1)").html(tmp.grupo);
                            $("#trPermisoApartado" + this.idPermiso).find("td:eq(2)").html(tmp.labelAsignacion);
                            $("#trPermisoApartado" + this.idPermiso).find("td:eq(3)").html(tmp.tipoPermiso);
                            resolve();
                        } else {
                            $("#trPermisoSubApartado" + this.idPermiso).find("td:eq(1)").html(tmp.grupo);
                            $("#trPermisoSubApartado" + this.idPermiso).find("td:eq(2)").html(tmp.labelAsignacion);
                            $("#trPermisoSubApartado" + this.idPermiso).find("td:eq(3)").html(tmp.tipoPermiso);
                            resolve();
                        }
                    }

                });

                guardando.then(() => {
                    this.tmpPermisos[this.idPermiso] = {
                        ...tmp
                    };
                    if (this.tipo == 'Apartado') {
                        configModulo.apartados[this.id].permisos = {
                            ...this.tmpPermisos
                        };
                        tblPermisosApartado.columns.adjust().draw();
                    } else {
                        configModulo.apartados[configApartado.id].subApartados[this.idSub].permisos = {
                            ...this.tmpPermisos
                        };
                        tblPermisosSubApartado.columns.adjust().draw();
                    }
                    $("#mdlPermiso").modal("hide");
                });
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        grupo,
        labelAsignacion,
        tipoPermiso
    }) {
        return new Promise((resolve, reject) => {
            try {
                if (this.tipo == 'Apartado') {
                    tblPermisosApartado.row.add([
                        idFila,
                        grupo,
                        labelAsignacion,
                        tipoPermiso,
                        "<div class='tblButtonContainer'>" +
                        "<span class='btnTbl' title='Editar' onclick='configPermiso.edit(" + idFila + ");'>" +
                        "<i class='fas fa-edit'></i>" +
                        "</span>" +
                        "<span class='btnTbl' title='Eliminar' onclick='configPermiso.delete({id:" + idFila + "});'>" +
                        "<i class='fas fa-trash'></i>" +
                        "</span>" +
                        "</div>"
                    ]).node().id = 'trPermisoApartado' + idFila;
                } else {
                    tblPermisosSubApartado.row.add([
                        idFila,
                        grupo,
                        labelAsignacion,
                        tipoPermiso,
                        "<div class='tblButtonContainer'>" +
                        "<span class='btnTbl' title='Editar' onclick='configPermiso.edit(" + idFila + ");'>" +
                        "<i class='fas fa-edit'></i>" +
                        "</span>" +
                        "<span class='btnTbl' title='Eliminar' onclick='configPermiso.delete({id:" + idFila + ",tipo:\"Sub\"});'>" +
                        "<i class='fas fa-trash'></i>" +
                        "</span>" +
                        "</div>"
                    ]).node().id = 'trPermisoSubApartado' + idFila;
                }
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function ({
        id = 0,
        tipo = 'Apartado'
    }) {
        Swal.fire({
            title: `Eliminar permiso`,
            icon: 'warning',
            html: `¿Seguro/a que quieres <b>eliminar</b> el permiso?.`,
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                if (tipo == 'Apartado') {
                    tblPermisosApartado.row('#trPermisoApartado' + id).remove().draw();
                    delete configModulo.apartados[configPermiso.id].permisos[id];
                } else {
                    tblPermisosSubApartado.row('#trPermisoSubApartado' + id).remove().draw();
                    delete configModulo.apartados[configApartado.id].subApartados[configPermiso.idSub].permisos[id];
                }
            }
        });
    },
    clean: function ({
        tipo = 'Apartado'
    }) {
        if (tipo == 'Apartado') {
            tblPermisosApartado.clear().draw();
            this.btnPermiso.disabled = true;
            this.labelApartado.innerHTML = `(Selecciona un apartado)`;
        } else {
            tblPermisosSubApartado.clear().draw();
            this.btnSubPermiso.disabled = true;
            this.labelSubApartado.innerHTML = `(Selecciona un sub-apartado)`;
        }
    },
    evalGrupo: function (grupo) {
        let defaultMessage = grupo.length == 0 ? 'seleccione un grupo' : 'seleccione',
            opciones = [`<option value="" disabled selected>${defaultMessage}</option>`],
            incluidos = [];

        for (let key in this.tmpPermisos) {
            if (this.tmpPermisos[key].grupo == grupo && this.tmpPermisos[key].asignacion != this.tmpAsignacion) {
                incluidos.push(this.tmpPermisos[key].asignacion);
            }
        }

        if (grupo == 'ROL') {
            for (let key in this.roles) {
                if (!incluidos.includes(this.roles[key].id)) {
                    opciones.push(`<option value="${this.roles[key].id}">${this.roles[key].rol}</option>`);
                }
            }
        } else if (grupo == 'USER') {
            for (let key in this.usuarios) {
                if (!incluidos.includes(this.usuarios[key].id)) {
                    opciones.push(`<option value="${this.usuarios[key].id}">${this.usuarios[key].nombreCompleto}</option>`);
                }
            }
        }

        this.cboAsignacion.innerHTML = opciones.join("");
        this.cboAsignacion.className.includes("selectpicker") && ($("#" + this.cboAsignacion.id).selectpicker("refresh"));
    }
}