let tblUsuarios,
    tblAgencias,
    tblRoles;

window.onload = () => {
    const tablesInit = initTables();
    const gettingTabs = tablesInit.then(() => {
        return new Promise((resolve, reject) => {
            const getting = fetchActions.getCats({
                modulo: "SOPORTE",
                archivo: "soporteGetCats",
                solicitados: ['agenciasCbo', 'rolesCbo', 'usuarios']
            }).then(({
                agencias,
                roles,
                usuarios
            }) => {
                $(".preloader").fadeIn("fast");
                agencias.forEach(agencia => configAgencia.cat[agencia.id] = {
                    ...agencia
                });
                roles.forEach(rol => configRol.cat[rol.id] = {
                    ...rol
                });

                configUsuario.init([...usuarios]);
                resolve();
            });
        });
    });

    gettingTabs.then(() => {
        $(".preloader").fadeOut("fast");
    });
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 350;
            if ($("#tblUsuarios").length) {
                tblUsuarios = $("#tblUsuarios").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [2, 3, 4],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblUsuarios.columns.adjust().draw();
            }

            if ($("#tblAgencias").length) {
                tblAgencias = $("#tblAgencias").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    paging: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblAgencias.columns.adjust().draw();
            }

            if ($("#tblRoles").length) {
                tblRoles = $("#tblRoles").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblRoles.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            console.error(err.message);
            reject();
        }
    });
}

const configUsuario = {
    id: 0,
    cnt: 0,
    contenedorPass: document.getElementById('passContainer'),
    campoContrasenia: document.getElementById('txtContrasenia'),
    campoConfirmacion: document.getElementById('txtContraseniaConfirmacion'),
    campoNombres: document.getElementById('txtNombres'),
    campoApellidos: document.getElementById('txtApellidos'),
    campoEmail: document.getElementById('txtEmail'),
    campoUsername: document.getElementById('txtUsername'),
    labelImagen: document.getElementById('labelUserImage'),
    campoImagen: document.getElementById("userImage"),
    usuarios: {},
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tblUsuarios.clear().draw();
                tmpDatos.forEach(usuario => {
                    this.cnt++;
                    this.usuarios[usuario.id] = usuario;
                    this.addRowTbl({
                        ...usuario,
                        idFila: this.cnt
                    });
                });
                tblUsuarios.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmUsuario').then(() => {
            $("#labelUserImage").css("background-image", "url('./main/img/users/avatar.jpg')");

            this.campoContrasenia.required = true;
            this.campoConfirmacion.required = true;

            $("#" + this.contenedorPass.id).fadeIn("fast");
            tblAgencias.clear().draw();
            tblRoles.clear().draw();
            cntAgencias = 0;
            cntRoles = 0;
            tmpAgencias = {};
            tmpRoles = [];
            filaUsuario = 0;
            this.id = id;
            configRol.id = 0;
            configRol.cnt = 0;
            configRol.roles = [];
            configAgencia.id = 0;
            configAgencia.cnt = 0;
            configAgencia.agencias = {};
            if (id > 0) {
                this.campoContrasenia.required = false;
                this.campoConfirmacion.required = false;
                this.contenedorPass.style.display = 'none';

                this.campoNombres.value = this.usuarios[id].nombres;
                this.campoApellidos.value = this.usuarios[id].apellidos;
                this.campoEmail.value = this.usuarios[id].email;
                this.campoUsername.value = this.usuarios[id].username;
                $("#" + this.labelImagen.id).css('background-image', "url('./main/img/users/" + this.usuarios[id].foto + "')");

                configAgencia.init({
                    ...this.usuarios[id].agencias
                });

                configRol.init({
                    ...this.usuarios[id].roles
                });
            }

            $("#user-tab").removeClass('disabled');
            $("#user-tab").trigger('click');
            $("#users-tab").addClass('disabled');
        });
    },
    save: function () {
        formActions.validate('frmUsuario').then(({
            errores
        }) => {
            if (errores == 0) {
                let datosCompletos = new FormData(),
                    nombresUsuario = this.campoNombres.value,
                    apellidosUsuario = this.campoApellidos.value,
                    email = this.campoEmail.value,
                    username = this.campoUsername.value,
                    password = this.campoContrasenia.value,
                    passConfirm = this.campoConfirmacion.value,
                    nombreImagen = this.campoImagen.files[0] ? $("#" + this.campoImagen.id).val().split('\\').pop() : (this.usuarios[filaUsuario] ? this.usuarios[filaUsuario].foto : "avatar.jpg");

                if ($("#passContainer").is(":visible")) {
                    errores = generalValidarFormatoContrasenia(password);
                    if (errores == 0) {
                        if (password != passConfirm) {
                            errores++;
                            toastr.warning("La contraseña y su confirmación no son iguales");
                        }
                    }
                }

                if (errores == 0) {
                    datosCompletos.append('id', this.id);
                    datosCompletos.append('nombres', nombresUsuario);
                    datosCompletos.append('apellidos', apellidosUsuario);
                    datosCompletos.append('email', email);
                    datosCompletos.append('username', username);
                    datosCompletos.append('password', password);
                    datosCompletos.append('agencias', JSON.stringify(configAgencia.agencias));
                    datosCompletos.append('roles', JSON.stringify(configRol.roles));
                    datosCompletos.append('nombreImagen', nombreImagen);

                    if (this.campoImagen.files[0]) {
                        datosCompletos.append("imagen", this.campoImagen.files[0]);
                    }

                    fetchActions.setWFiles({
                        modulo: "SOPORTE",
                        archivo: "soporteGuardarUsuario",
                        datos: datosCompletos
                    }).then((datosRespuesta) => {
                        if (datosRespuesta.respuesta) {
                            switch (datosRespuesta.respuesta.trim()) {
                                case "EXITO":
                                    location.reload();
                                    break;
                                default:
                                    generalMostrarError(datosRespuesta);
                                    break;
                            }
                        } else {
                            generalMostrarError(datosRespuesta);
                        }
                    }).catch(generalMostrarError);
                }
            }
        });
    },
    addRowTbl: function ({
        idFila,
        username,
        nombres,
        apellidos,
        agencias,
        roles,
        ultimoIngreso,
        bloqueado,
        foto
    }) {
        return new Promise((resolve, reject) => {
            try {
                let clase = bloqueado == 'N' ? 'tblBtnDanger' : 'tblBtnSuccess',
                    title = bloqueado == 'N' ? 'Bloquear' : 'Desbloquear',
                    icono = bloqueado == 'N' ? 'fas fa-times' : 'fas fa-check',
                    nombresRoles = [];

                ultimoIngreso == null && (ultimoIngreso = 'Sin registros');

                for (let key in roles) {
                    nombresRoles.push(configRol.cat[roles[key].id].rol);
                }

                let tmpAgAsignadas = [];
                for (let key in agencias) {
                    if (agencias[key].principal == 'S') {
                        tmpAgAsignadas.push(`<span class="badge bg-warning text-dark" style="font-size:10px;">${agencias[key].id}</span>`);
                    } else {
                        tmpAgAsignadas.push(`${agencias[key].id}`);
                    }
                }
                let tmpAgencias = tmpAgAsignadas.length > 0 ? tmpAgAsignadas.join(', ') : 'Sin asignación',
                    tmpRoles = nombresRoles.length > 0 ? nombresRoles.join(', ') : 'Sin asignación',
                    nombreCompleto = `${nombres} ${apellidos}`;
                tblUsuarios.row.add([
                    idFila,
                    "<div class='contenedorImgTbl'>" +
                    "<img src='./main/img/users/" + foto + "' />" +
                    "</div>",
                    username,
                    nombreCompleto,
                    tmpAgencias,
                    tmpRoles,
                    ultimoIngreso,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configUsuario.edit(" + idFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='" + title + "' onclick='configUsuario.changeState(" + idFila + ",$(this));'>" +
                    "<i class='" + icono + "'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = 'trUsuario' + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    changeState: function (id, spn) {
        let tmpDatos = this.usuarios[id],
            newTitle = tmpDatos.bloqueado == "N" ? "Desbloquear" : "Bloquear",
            newIcon = tmpDatos.bloqueado == "N" ? "fas fa-check" : "fas fa-times",
            newState = tmpDatos.bloqueado == "S" ? "N" : "S",
            idDb = tmpDatos.id;

        fetchActions.set({
            modulo: "SOPORTE",
            archivo: "soporteCambiarEstadoUsuario",
            datos: {
                id: idDb,
                newState
            }
        }).then(() => {
            spn.attr("title", newTitle);
            spn.find("i").attr("class", newIcon);
            this.usuarios[id].bloqueado = newState;
        }).catch(generalMostrarError);
    },
    validateUsername: function () {
        let username = this.campoUsername.value;

        if (username.length > 0) {
            let tmpId = filaUsuario;
            fetchActions.getCats({
                modulo: "SOPORTE",
                archivo: 'soporteValidarUsername',
                solicitados: [tmpId, username]
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta.trim()) {
                        case "DISPONIBLE":
                            toastr.success("Username disponible");
                            break;
                        case "ASIGNADO":
                            toastr.error("Username asignado");
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            toastr.warning("Debe ingresar un username");
        }
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                $("#users-tab").removeClass('disabled');
                $("#users-tab").trigger('click');
                $("#user-tab").addClass('disabled');
            }
        });
    }
};

const configRol = {
    id: 0,
    cnt: 0,
    cat: {},
    roles: [],
    cboRol: document.getElementById("cboRol"),
    init: function (tmpDatos) {
        for (let key in tmpDatos) {
            this.roles.push(tmpDatos[key].id);
            this.cnt++;
            this.addRowTbl({
                ...tmpDatos[key],
                idFila: this.cnt,
                nombreRol: this.cat[tmpDatos[key].id].rol,
                tipo: this.cat[tmpDatos[key].id].labelTipoRol
            });
        }

        tblRoles.columns.adjust().draw();
    },
    edit: function (id = 0) {
        formActions.clean('frmRol').then(() => {
            let incluidos = [],
                opciones = [`<option value="" selected disabled>seleccione</option>`];

            this.roles.forEach(rol => incluidos.push(rol));

            for (let key in this.cat) {
                if (!incluidos.includes(this.cat[key].id)) {
                    opciones.push(`<option value="${this.cat[key].id}">${this.cat[key].rol} - ${this.cat[key].labelTipoRol}</option>`);
                }
            }

            this.cboRol.innerHTML = opciones.join("");
            this.cboRol.className.includes("selectpicker") && ($("#" + this.cboRol.id).selectpicker("refresh"));
            $("#mdlRol").modal("show");
        });
    },
    save: function () {
        formActions.validate("frmRol").then(() => {
            let tmp = {
                id: this.cboRol.value,
                nombreRol: this.cat[this.cboRol.value].rol,
                tipo: this.cat[this.cboRol.value].labelTipoRol
            };

            this.cnt++;
            this.addRowTbl({
                ...tmp,
                idFila: this.cnt
            }).then(() => {
                tblRoles.columns.adjust().draw();
                this.roles.push(tmp.id);
                $("#mdlRol").modal("hide");
            }).catch(generalMostrarError);
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        id,
        nombreRol,
        tipo
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblRoles.row.add([
                    idFila,
                    nombreRol,
                    tipo,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configRol.delete(" + idFila + "," + id + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = 'trRol' + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (idFila = 0, id = 0) {
        tblRoles.row('#trRol' + idFila).remove().draw();
        tblRoles.columns.adjust().draw();
        // for (let i = 0; i < this.roles.length; i++) {
        //     if (this.roles[i].id == id) {
        //         tmpRoles.splice(i, 1);
        //     }
        // }

        this.roles = [...this.roles.filter(rol => parseInt(rol) != id)];
    }
}

const configAgencia = {
    id: 0,
    cnt: 0,
    cat: {},
    agencias: {},
    cboAgencia: document.getElementById("cboAgencia"),
    init: function (tmpDatos) {
        this.agencias = {
            ...tmpDatos
        };
        for (let key in tmpDatos) {
            this.addRowTbl({
                ...tmpDatos[key],
                nombreAgencia: this.cat[key].nombreAgencia
            });
        }

        tblAgencias.columns.adjust().draw();
    },
    edit: function (id = 0) {
        formActions.clean('frmAgencia').then(() => {
            let incluidos = [],
                opciones = [`<option value="" selected disabled>seleccione</option>`];

            for (let key in this.agencias) {
                incluidos.push(this.agencias[key].id);
            }

            for (let key in this.cat) {
                if (!incluidos.includes(this.cat[key].id)) {
                    opciones.push(`<option value="${this.cat[key].id}">${this.cat[key].nombreAgencia}</option>`);
                }
            }

            this.cboAgencia.innerHTML = opciones.join("");
            this.cboAgencia.className.includes("selectpicker") && ($("#" + this.cboAgencia.id).selectpicker("refresh"));
            $("#mdlAgencia").modal("show");
        });
    },
    save: function () {
        formActions.validate("frmAgencia").then(() => {
            let tmp = {
                id: this.cboAgencia.value,
                nombreAgencia: this.cboAgencia.options[this.cboAgencia.options.selectedIndex].text,
                principal: 'N'
            };

            this.addRowTbl({
                ...tmp
            }).then(() => {
                tblAgencias.columns.adjust().draw();
                this.agencias[tmp.id] = {
                    ...tmp
                };
                $("#mdlAgencia").modal("hide");
            }).catch(generalMostrarError);
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        id,
        nombreAgencia,
        principal
    }) {
        return new Promise((resolve, reject) => {
            try {
                let checked = principal == 'S' ? 'checked' : '';
                tblAgencias.row.add([
                    id,
                    nombreAgencia,
                    "<div style='display:flex; justify-content:center;'>" +
                    "<div class='custom-control custom-radio'>" +
                    "<input type='radio' id='ag" + id + "' class='custom-control-input' name='rbAgencia' " + checked + " onchange='configAgencia.evalPrincipal(" + id + ")'>" +
                    "<label class='custom-control-label' for='ag" + id + "'></label>" +
                    "</div>" +
                    "</div>",
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configAgencia.delete(" + id + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = 'trAgencia' + id;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        tblAgencias.row('#trAgencia' + id).remove().draw();
        delete this.agencias[id];
    },
    evalPrincipal: function (id) {
        for (let key in this.agencias) {
            if (key == id) {
                this.agencias[key].principal = 'S';
            } else {
                this.agencias[key].principal = 'N';
            }
        }
    }
}