let tblRoles;

window.onload = () => {
    const gettingCats = initTables().then(() => {
        fetchActions.getCats({
            modulo: "SOPORTE",
            archivo: "soporteGetCats",
            solicitados: ["tiposRol", "roles"]
        }).then(({
            tiposRol,
            roles
        }) => {
            $(".preloader").fadeIn("fast");
            let opciones = [`<option value="" selected disabled>seleccione</option>`],
                cboTipoRol = document.getElementById("cboTipoRol");

            tiposRol.forEach(tipo => opciones.push(`<option value="${tipo.id}">${tipo.tipo}</option>`));
            cboTipoRol.innerHTML = opciones.join("");
            cboTipoRol.className.includes("selectpicker") && ($("#" + cboTipoRol.id).selectpicker("refresh"));

            configRol.init([
                ...roles
            ]).then(() => {
                $(".preloader").fadeOut("fast");
            }).catch(generalMostrarError);
        });
    });
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 350;

            if ($("#tblRoles").length) {
                tblRoles = $("#tblRoles").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblRoles.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            console.error(err.message);
            reject();
        }
    });
}

const configRol = {
    id: 0,
    cnt: 0,
    campoRol: document.getElementById("txtRol"),
    cboTipoRol: document.getElementById('cboTipoRol'),
    roles: {},
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(rol => {
                    this.cnt++;
                    this.roles[this.cnt] = {
                        ...rol
                    };
                    this.addRowTbl({
                        ...rol,
                        idFila: this.cnt
                    });
                });
                tblRoles.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('mdlRol').then(() => {
            let bloqueado = false;
            this.id = id;
            $("#mdlRol .modal-title strong").html('Nuevo');
            if (id != '0') {
                $("#mdlRol .modal-title strong").html('Edición');
                let rol = this.roles[id].rol,
                    tipoRol = this.roles[id].tipoRol;
                if (rol == 'ROOT') {
                    bloqueado = true;
                    toastr.warning("Este rol no puede ser modificado");
                } else {
                    this.campoRol.value = rol;
                    this.cboTipoRol.value = tipoRol;
                }
            }

            if (!bloqueado) {
                $("#mdlRol").modal("show");
            }
        });
    },
    save: function () {
        formActions.validate('frmRol').then(({
            errores
        }) => {
            if (errores == 0) {
                let id = this.id > 0 ? this.roles[this.id].id : this.id,
                    rol = this.campoRol.value,
                    tipoRol = this.cboTipoRol.value;

                fetchActions.set({
                    modulo: "SOPORTE",
                    archivo: 'soporteGuardarRol',
                    datos: {
                        id,
                        rol,
                        tipoRol,
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    switch (datosRespuesta.respuesta.trim()) {
                        case "EXITO":
                            let tmpDatos = {
                                id: datosRespuesta.id,
                                rol,
                                tipoRol,
                                labelTipoRol: this.cboTipoRol.options[this.cboTipoRol.options.selectedIndex].text,
                                fechaRegistro: datosRespuesta.fechaRegistro,
                                fechaActualizacion: datosRespuesta.fechaActualizacion,
                                habilitado: this.id > 0 ? this.roles[this.id].habilitado : 'S'
                            }
                            if (this.id == 0) {
                                this.cnt++;
                                this.roles[this.cnt] = {
                                    ...tmpDatos
                                };
                                this.addRowTbl({
                                    ...tmpDatos,
                                    idFila: this.cnt
                                }).then(() => {
                                    tblRoles.columns.adjust().draw();
                                    $("#mdlRol").modal("hide");
                                });
                            } else {
                                this.roles[this.id] = {
                                    ...tmpDatos
                                };
                                $("#trRol" + this.id).find("td:eq(1)").html(tmpDatos.rol);
                                $("#trRol" + this.id).find("td:eq(2)").html(tmpDatos.labelTipoRol);
                                $("#trRol" + this.id).find("td:eq(4)").html(tmpDatos.fechaActualizacion);
                                $("#mdlRol").modal("hide");
                            }
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
        // let {
        //     cantidadErrores
        // } = generalRevisarContenedor('frmRol');
        // if (cantidadErrores == 0) {


        //     const guardarRol = new Promise((resolve, reject) => {
        //         (async () => {
        //             try {
        //                 let datos = {
        //                         id,
        //                         rol,
        //                         tipoRol,
        //                         tipoApartado,
        //                         idApartado
        //                     },
        //                     init = {
        //                         method: "POST",
        //                         headers: {
        //                             "Content-type": "application/json"
        //                         },
        //                         body: JSON.stringify(datos)
        //                     },
        //                     response = await fetch(`./main/${modulo}/code/soporteGuardarRol.php`, init);
        //                 if (response.ok) {
        //                     let datosRespuesta = await response.json();
        //                     // console.log(datosRespuesta);
        //                     resolve(datosRespuesta);
        //                 } else {
        //                     throw new Error(response.statusText);
        //                     reject();
        //                 }
        //             } catch (err) {
        //                 console.error(err.message);
        //                 reject();
        //             }
        //         })();
        //     });

        //     guardarRol.then((datosRespuesta) => {
        //        
        //     }).catch(() => {
        //         console.error("Ocurrió un error al guardar el rol");
        //     });
        // }
    },
    addRowTbl: function ({
        idFila,
        rol,
        labelTipoRol: tipoRolImp,
        fechaRegistro,
        fechaActualizacion,
        habilitado
    }) {
        return new Promise((resolve, reject) => {
            let title = habilitado == 'S' ? 'Deshabilitar' : 'Habilitar',
                icono = habilitado == 'S' ? 'fas fa-times' : 'fas fa-check';
            tblRoles.row.add([
                idFila,
                rol,
                tipoRolImp,
                fechaRegistro,
                fechaActualizacion,
                "<div class='tblButtonContainer'>" +
                "<span class='btnTbl' title='Editar' onclick='configRol.edit(" + idFila + ");'>" +
                "<i class='fas fa-edit'></i>" +
                "</span>" +
                "<span class='btnTbl' title='" + title + "' onclick='configRol.changeState(" + idFila + ",$(this));'>" +
                "<i class='" + icono + "'></i>" +
                "</span>" +
                "</div>"
            ]).node().id = 'trRol' + idFila;
            resolve();
        });
    },
    changeState: function (idFila, spn) {
        let tmpDatos = this.roles[idFila],
            newTitle = tmpDatos.habilitado == "S" ? "Habilitar" : "Deshabilitar",
            newIcon = tmpDatos.habilitado == "S" ? "fas fa-check" : "fas fa-times",
            newState = tmpDatos.habilitado == "S" ? "N" : "S",
            id = tmpDatos.id;

        if (tmpDatos.rol != 'ROOT') {
            fetchActions.set({
                modulo: "SOPORTE",
                archivo: "soporteCambiarEstadoRol",
                datos: {
                    id,
                    newState
                }
            }).then(() => {
                spn.attr("title", newTitle);
                spn.find("i").attr("class", newIcon);
                this.roles[idFila].habilitado = newState;
            }).catch(generalMostrarError);
        } else {
            toastr.warning("No es posible cambiar el estado de este rol");
        }
    }
};