let tblModulos;

window.onload = () => {
    initTables().then(() => {
        fetchActions.getCats({
            modulo: "SOPORTE",
            archivo: "soporteGetCats",
            solicitados: ['modulosAdmin']
        }).then(({
            modulos
        }) => {
            $(".preloader").fadeIn("fast");
            configModulo.init([...modulos]).then(() => {
                $(".preloader").fadeOut("fast");
            });
        }).catch(generalMostrarError);
    });
}

const configModulo = {
    cnt: 0,
    datos: {},
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                this.datos = {};
                tmpDatos.forEach(modulo => {
                    this.cnt++;
                    this.datos[this.cnt] = {
                        ...modulo
                    };
                    this.addRowTbl({
                        ...modulo,
                        idFila: this.cnt
                    });
                });
                tblModulos.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        let moduloUrl = encodeURIComponent(btoa('SOPORTE'));
        if (id > 0) {
            let urlId = encodeURIComponent(btoa(this.datos[id].id));
            window.location.href = `?page=soporteConfigModulo&mod=${moduloUrl}&id=${urlId}`;
        } else {
            window.location.href = `?page=soporteConfigModulo&mod=${moduloUrl}`;
        }
    },
    addRowTbl: function ({
        idFila,
        titulo,
        descripcion,
        contenedor,
        fechaRegistro,
        fechaActualizacion,
        mantenimiento
    }) {
        return new Promise((resolve, reject) => {
            try {
                let iconoMantenimiento = mantenimiento == 'N' ? 'fa-tools' : 'fa-check',
                    tituloMantenimiento = mantenimiento == 'N' ? 'Iniciar mantenimiento' : 'Terminar mantenimiento';
                tblModulos.row.add([
                    idFila,
                    titulo,
                    descripcion,
                    contenedor,
                    fechaRegistro,
                    fechaActualizacion,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configModulo.edit(" + idFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='" + tituloMantenimiento + "' onclick='configModulo.mantenimiento(" + idFila + ",this);'>" +
                    "<i class='fas " + iconoMantenimiento + "'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configModulo.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = 'trModulo' + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let label = this.datos[id].titulo;
        Swal.fire({
            title: `Eliminar módulo`,
            icon: 'warning',
            html: `¿Seguro/a que quieres <b>eliminar</b> el módulo <strong>${label}</strong>?.`,
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: "SOPORTE",
                    archivo: "soporteEliminarModulo",
                    datos: {
                        id:this.datos[id].id
                    }
                }).then(() => {
                    tblModulos.row('#trModulo' + id).remove().draw();
                    delete this.datos[id];
                }).catch(generalMostrarError);
            }
        });
    },
    mantenimiento: function (id = 0, spn) {
        let tmp = this.datos[id],
            newState = tmp.mantenimiento == 'S' ? 'N' : 'S',
            newIcon = tmp.mantenimiento == 'N' ? 'fas fa-check' : 'fas fa-tools',
            newTitle = tmp.mantenimiento == 'N' ? 'Terminar mantenimiento' : 'Iniciar mantenimiento';

        fetchActions.set({
            modulo: "SOPORTE",
            archivo: "soporteMantenimientoModulo",
            datos: {
                id: this.datos[id].id,
                newState
            }
        }).then(() => {
            this.datos[id].mantenimiento = newState;
            spn.querySelector('i').className = newIcon;
            spn.title = newTitle;
        }).catch(generalMostrarError);
    }
}


function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 350;

            if ($("#tblModulos").length) {
                tblModulos = $("#tblModulos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblModulos.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            console.error(err.message);
            reject();
        }
    });
}