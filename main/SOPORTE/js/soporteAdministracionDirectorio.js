let tblContactos,
    tblTelefonos,
    tblEmails;
window.onload = () => {
    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "SOPORTE",
                archivo: 'soporteGetCats',
                solicitados: ['agenciasCbo', 'cargosDirectorio', 'contactosDirectorio']
            }).then((datosRespuesta) => {
                $(".preloader").fadeIn("fast");
                const opciones = [`<option selected disabled value="">seleccione</option>`];
                if (datosRespuesta.agencias) {
                    let tmpOpciones = [...opciones],
                        cbos = document.querySelectorAll("select.cboAgencia");

                    datosRespuesta.agencias.forEach(agencia => {
                        configContacto.agencias[agencia.id] = {
                            ...agencia
                        };
                        tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`);
                    });

                    cbos.forEach(cbo => {
                        cbo.innerHTML = tmpOpciones.join("");
                    });
                }
                if (datosRespuesta.cargos) {
                    let tmpOpciones = [...opciones],
                        cbos = document.querySelectorAll("select.cboCargo");

                    datosRespuesta.cargos.forEach(cargo => {
                        configContacto.cargos[cargo.id] = {
                            ...cargo
                        };
                        tmpOpciones.push(`<option value="${cargo.id}">${cargo.cargo}</option>`);
                    });

                    cbos.forEach(cbo => {
                        cbo.innerHTML = tmpOpciones.join("");
                    });
                }

                if (datosRespuesta.contactosDirectorio) {
                    configContacto.init([
                        ...datosRespuesta.contactosDirectorio
                    ]);
                }
                resolve();
            }).catch(reject);
        });
    }).catch(generalMostrarError);

    gettingCats.then(() => {
        let selectpickerCbos = document.querySelectorAll("select.selectpicker");
        selectpickerCbos.forEach(cbo => {
            $("#" + cbo.id).selectpicker("refresh");
        });
        $(".preloader").fadeOut("fast");
        $(".preloader").find("span").html("");
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 250;
            if ($("#tblContactos").length) {
                tblContactos = $("#tblContactos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2, 3, 4],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblContactos.columns.adjust().draw();
            }

            if ($("#tblTelefonos").length) {
                tblTelefonos = $("#tblTelefonos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de fiador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblTelefonos.columns.adjust().draw();
            }

            if ($("#tblEmails").length) {
                tblEmails = $("#tblEmails").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de producto';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblEmails.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configContacto = {
    id: 0,
    cnt: 0,
    agencias: {},
    cargos: {},
    contactos: {},
    tabContactos: document.getElementById("contactos-tab"),
    tabContacto: document.getElementById("contacto-tab"),
    campoNombres: document.getElementById("txtNombres"),
    campoApellidos: document.getElementById("txtApellidos"),
    cboAgencia: document.getElementById("cboAgencia"),
    cboCargo: document.getElementById("cboCargo"),
    campoDiscord: document.getElementById("txtDiscord"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(contacto => {
                    this.cnt++;
                    this.contactos[this.cnt] = {
                        ...contacto
                    };
                    this.addRowTbl({
                        ...contacto,
                        nombreAgencia: this.agencias[contacto.idAgencia].nombreAgencia,
                        nombreCargo: this.cargos[contacto.idCargo].cargo,
                        idFila: this.cnt
                    });
                });
                tblContactos.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmContacto').then(() => {
            let leyendo = new Promise((resolve, reject) => {
                tblTelefonos.clear().draw();
                tblEmails.clear().draw();
                this.id = id;
                configTelefono.telefonos = {};
                configTelefono.cnt = 0;
                configEmail.emails = {};
                configEmail.cnt = 0;
                if (this.id == 0) {
                    resolve();
                } else {
                    this.campoNombres.value = this.contactos[this.id].nombres;
                    this.campoApellidos.value = this.contactos[this.id].apellidos;
                    this.cboAgencia.value = this.contactos[this.id].idAgencia;
                    this.cboCargo.value = this.contactos[this.id].idCargo;
                    this.campoDiscord.value = this.contactos[this.id].discord;
                    configTelefono.init([
                        ...this.contactos[this.id].telefonos
                    ]).then().catch(generalMostrarError);
                    configEmail.init([
                        ...this.contactos[this.id].emails
                    ]).then().catch(generalMostrarError);
                    resolve();
                }
            });

            leyendo.then(() => {
                let selectpickerCbos = document.querySelectorAll("#frmContacto select.selectpicker");
                selectpickerCbos.forEach(cbo => {
                    $("#" + cbo.id).selectpicker("refresh");
                });
                this.tabContacto.classList.remove('disabled');
                $("#" + this.tabContacto.id).trigger("click");
                this.tabContactos.classList.add("disabled");
            }).catch(generalMostrarError);
        });
    },
    save: function () {
        formActions.validate('frmContacto').then(({
            errores
        }) => {
            if (errores == 0) {
                let currentDate = new Date(),
                    fechaActualizacion = ("0" + currentDate.getDate()).slice(-2) + "-" + ("0" + (currentDate.getMonth() + 1)).slice(-2) + "-" + currentDate.getFullYear() + " " + ("0" + currentDate.getHours()).slice(-2) + ":" + ("0" + currentDate.getMinutes()).slice(-2);
                let tmp = {
                    id: this.id > 0 ? this.contactos[this.id].id : 0,
                    nombres: this.campoNombres.value,
                    apellidos: this.campoApellidos.value,
                    idAgencia: this.cboAgencia.value,
                    nombreAgencia: this.cboAgencia.options[this.cboAgencia.options.selectedIndex].text,
                    idCargo: this.cboCargo.value,
                    nombreCargo: this.cboCargo.options[this.cboCargo.options.selectedIndex].text,
                    discord: this.campoDiscord.value,
                    telefonos: {
                        ...configTelefono.telefonos
                    },
                    emails: {
                        ...configEmail.emails
                    },
                    fechaRegistro: this.id > 0 ? this.contactos[this.id].fechaRegistro : fechaActualizacion,
                    fechaActualizacion
                }

                fetchActions.set({
                    modulo: "SOPORTE",
                    archivo: "soporteGuardarContacto",
                    datos: {
                        ...tmp,
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                Swal.fire({
                                    title: "¡Bien hecho!",
                                    icon: "success",
                                    html: "Datos guardados exitosamente."
                                }).then(() => {
                                    location.reload();
                                });
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        nombres,
        apellidos,
        nombreAgencia,
        nombreCargo,
        discord,
        telefonos,
        emails,
        fechaRegistro,
        fechaActualizacion
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configContacto.edit(" + idFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configContacto.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                let tmpTelefonos = [],
                    tmpEmails = [];

                    telefonos == null && (telefonos = []);
                    emails == null && (emails = []);

                telefonos.forEach(telefono => {
                    tmpTelefonos.push(telefono.telefono);
                });

                emails.forEach(email => {
                    tmpEmails.push(email.email);
                });

                tblContactos.row.add([
                    idFila,
                    `${nombres} ${apellidos}`,
                    nombreAgencia,
                    nombreCargo,
                    discord,
                    tmpTelefonos.join('<br/>'),
                    tmpEmails.join('<br />'),
                    fechaRegistro,
                    fechaActualizacion,
                    botones
                ]).node().id = 'trContacto' + idFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let nombreCompleto = this.contactos[id].nombres + ' ' + this.contactos[id].apellidos;
        Swal.fire({
            title: 'Eliminar contacto',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el contacto de <strong>' + nombreCompleto + '</strong>?.<br />Los datos no podrán recuperarse.',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: "SOPORTE",
                    archivo: "soporteEliminarContacto",
                    datos: {
                        id: this.contactos[id].id,
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                tblContactos.row('#trContacto' + id).remove().draw();
                                delete this.contactos[id];
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                this.showPrincipal();
            }
        });
    },
    showPrincipal: function () {
        this.tabContactos.classList.remove("disabled");
        $("#" + this.tabContactos.id).trigger('click');
        this.tabContacto.classList.add("disabled");
        return;
    }
}

const configTelefono = {
    id: 0,
    cnt: 0,
    telefonos: {},
    campoTelefono: document.getElementById("txtTelefono"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(telefono => {
                    this.cnt++;
                    this.telefonos[this.cnt] = {
                        ...telefono
                    };
                    this.addRowTbl({
                        ...telefono,
                        idFila: this.cnt
                    });
                });
                tblTelefonos.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmTelefono').then(() => {
            let leyendo = new Promise((resolve, reject) => {
                this.id = id;
                if (this.id == 0) {
                    resolve();
                } else {

                }
            });

            leyendo.then(() => {
                $("#mdlTelefono").modal("show");
            }).catch(generalMostrarError);
        });
    },
    save: function () {
        formActions.validate('frmTelefono').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.telefonos[this.id].id : 0,
                    telefono: this.campoTelefono.value
                }

                let guardando = new Promise((resolve, reject) => {
                    if (this.id > 0) {
                        $("#trTelefono" + this.id).find("td:eq(1)").html(tmp.telefono);
                        resolve();
                    } else {
                        this.cnt++;
                        this.id = this.cnt;
                        let agregar = this.addRowTbl({
                            ...tmp,
                            idFila: this.cnt
                        }).then(resolve).catch(reject);
                    }
                });

                guardando.then(() => {
                    this.telefonos[this.id] = {
                        ...tmp
                    };
                    tblTelefonos.columns.adjust().draw();
                    $("#mdlTelefono").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        telefono,
        idFila
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblTelefonos.row.add([
                    idFila,
                    telefono,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configTelefono.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trTelefono" + idFila;
                resolve();
            } catch (err) {
                reject(err.message)
            }
        });
    },
    delete: function (id = 0) {
        return new Promise((resolve, reject) => {
            try {
                tblTelefonos.row('#trTelefono' + id).remove().draw();
                tblTelefonos.columns.adjust().draw();
                delete this.telefonos[id];
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    }
}

const configEmail = {
    id: 0,
    cnt: 0,
    emails: {},
    campoEmail: document.getElementById("txtEmail"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(email => {
                    this.cnt++;
                    this.emails[this.cnt] = {
                        ...email
                    };
                    this.addRowTbl({
                        ...email,
                        idFila: this.cnt
                    });
                });
                tblEmails.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmEmail').then(() => {
            let leyendo = new Promise((resolve, reject) => {
                this.id = id;
                if (this.id == 0) {
                    resolve();
                } else {

                }
            });

            leyendo.then(() => {
                $("#mdlEmail").modal("show");
            }).catch(generalMostrarError);
        });
    },
    save: function () {
        formActions.validate('frmEmail').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.emails[this.id].id : 0,
                    email: this.campoEmail.value
                }

                let guardando = new Promise((resolve, reject) => {
                    if (this.id > 0) {
                        $("#trEmail" + this.id).find("td:eq(1)").html(tmp.email);
                        resolve();
                    } else {
                        this.cnt++;
                        this.id = this.cnt;
                        let agregar = this.addRowTbl({
                            ...tmp,
                            idFila: this.cnt
                        }).then(resolve).catch(reject);
                    }
                });

                guardando.then(() => {
                    this.emails[this.id] = {
                        ...tmp
                    };
                    tblEmails.columns.adjust().draw();
                    $("#mdlEmail").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        email,
        idFila
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblEmails.row.add([
                    idFila,
                    email,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configEmail.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trEmail" + idFila;
                resolve();
            } catch (err) {
                reject(err.message)
            }
        });
    },
    delete: function (id = 0) {
        return new Promise((resolve, reject) => {
            try {
                tblEmails.row('#trEmail' + id).remove().draw();
                tblEmails.columns.adjust().draw();
                delete this.emails[id];
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    }
}