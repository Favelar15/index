let tblUsuarios;

window.onload = () => {
    const gettingCats = initTables().then(() => {
        fetchActions.get({
            archivo: 'soporteGetSesiones',
            modulo: 'SOPORTE',
            params: {
                q: 'all'
            }
        }).then((datosRespuesta) => {
            configUsuarios.init(datosRespuesta.usuarios);
        }).catch(generalMostrarError);
    });
}


function initTables() {
    return new Promise((resolve, reject) => {
        try {
            let alturaTabla = 350;
            if ($("#tblUsuarios").length) {
                tblUsuarios = $("#tblUsuarios").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [2, 3, 4],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblUsuarios.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            console.error(err.message);
            reject();
        }
    });
}

const configUsuarios = {
    cnt: 0,
    usuarios: {},
    init: function (tmpDatos) {
        console.log(tmpDatos)
        tmpDatos.forEach(user => {
            this.cnt++;
            this.usuarios[this.cnt] = {
                ...user
            };
            this.addRowTbl({
                ...user,
                nFila: this.cnt
            });
        });
        tblUsuarios.columns.adjust().draw();
    },
    addRowTbl: function ({
        nFila,
        foto,
        username,
        idAgenciaActual,
        nombreUsuario,
        fechaIngreso,
        pantallaActiva
    }) {
        return new Promise((resolve, reject) => {
            try {
                tblUsuarios.row.add([
                    nFila,
                    "<div class='contenedorImgTbl'>" +
                    "<img src='./main/img/users/" + foto + "' />" +
                    "</div>",
                    username,
                    idAgenciaActual,
                    nombreUsuario,
                    fechaIngreso,
                    pantallaActiva,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Cerrar sesión' onclick='configUsuarios.cerrarSesion(" + nFila + ");'>" +
                    "<i class='fas fa-sign-out-alt'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = 'trUsuario' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    cerrarSesion: function (id) {
        let tmp = this.usuarios[id];
        fetchActions.set({
            archivo: 'soporteCerrarSesionRemota',
            modulo: 'SOPORTE',
            datos: tmp
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta) {
                    case 'EXITO':
                        tblUsuarios.row('#trUsuario' + id).remove().draw();
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    }
}