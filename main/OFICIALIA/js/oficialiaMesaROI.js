let tblFormularios,
    tblOperaciones,
    tblDocumentos,
    tblGestiones;

window.onload = () => {
    initTables().then(() => {
        fetchActions.getCats({
            modulo: 'OFICIALIA',
            archivo: 'oficialiaGetCats',
            solicitados: ['all']
        }).then(({
            tiposDocumento: documentos,
            agencias,
            datosGeograficos: geograficos,
            actividadesEconomicas,
            tiposOperacion,
            productosServicios,
            areas,
            colaboradores,
            tiposDocumentoAdjunto,
        }) => {
            let cbosTipoDocumento = document.querySelectorAll('select.cboTipoDocumento'),
                cbosAgencia = document.querySelectorAll('select.cboAgencia'),
                cbosPais = document.querySelectorAll('select.cboPais'),
                cbosActividadEconomica = document.querySelectorAll('select.cboActividadEconomica'),
                cbosColaborador = document.querySelectorAll('select.cboColaborador'),
                cbosTipoDocumentoAdjunto = document.querySelectorAll('select.cboTipoDocumentoAdjunto'),
                allSelectpickerCbos = document.querySelectorAll('select.selectpicker');

            const opciones = [`<option selected disabled value="">seleccione</option>`];
            let tmpOpciones = [...opciones];

            documentos.forEach(tipo => {
                tiposDocumento[tipo.id] = {
                    ...tipo
                };
                tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
            });

            cbosTipoDocumento.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

            datosGeograficos = {
                ...geograficos
            };
            tmpOpciones = [...opciones];
            for (let key in datosGeograficos) {
                tmpOpciones.push(`<option value="${datosGeograficos[key].id}">${datosGeograficos[key].nombrePais}</option>`);
            }

            cbosPais.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

            tmpOpciones = [...opciones];
            actividadesEconomicas.forEach(actividad => tmpOpciones.push(`<option value="${actividad.id}">${actividad.actividadEconomica}</option>`));
            cbosActividadEconomica.forEach(cbo => cbo.innerHTML = tmpOpciones);

            tmpOpciones = [...opciones];
            agencias.forEach(agencia => tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`));
            cbosAgencia.forEach(cbo => cbo.innerHTML = tmpOpciones);

            tiposOperacion.forEach(tipo => configOperaciones.tiposOperacion[tipo.id] = tipo);

            tmpOpciones = [...opciones];
            colaboradores.forEach(colaborador => {
                tmpOpciones.push(`<option value="${colaborador.id}">${colaborador.nombres} ${colaborador.apellidos}</option>`);
                configROI.colaboradores[colaborador.id] = {
                    ...colaborador
                };
            });
            cbosColaborador.forEach(cbo => cbo.innerHTML = tmpOpciones);

            areas.forEach(area => configOperaciones.catAreas[area.id] = area);

            tmpOpciones = [...opciones];
            // console.log(tiposDocumentoAdjunto)
            tiposDocumentoAdjunto.forEach(tipo => {
                configDocumentos.configCatalogo.cnt++;
                configDocumentos.catalogo[configDocumentos.configCatalogo.cnt] = {
                    ...tipo
                };
                tmpOpciones.push(`<option value="${tipo.id}">${tipo.documento}</option>`);
            });
            cbosTipoDocumentoAdjunto.forEach(cbo => cbo.innerHTML = tmpOpciones);

            productosServicios.forEach(producto => {
                configOperaciones.configTipoProducto.cnt++;
                configOperaciones.configTipoProducto.catalogo[configOperaciones.configTipoProducto.cnt] = {
                    ...producto
                };
            });

            allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));

            $('#btnFileGestion').on('click', function () {
                $('#fileGestion').trigger('click');
            });

            $("#fileGestion").on("change", function (e) {
                let file = e.target.files[0],
                    $file = $(this),
                    url = URL.createObjectURL(file),
                    filename = $file.val().split('\\').pop();
                document.getElementById("fileName").innerHTML = filename;
            });

            fetchActions.get({
                modulo: 'OFICIALIA',
                archivo: 'oficialiaGetROIAnalista',
                params: {
                    'q': 'all'
                }
            }).then((datosRespuesta) => {
                configFormulario.init(datosRespuesta);
            }).catch(generalMostrarError);
        }).catch(generalMostrarError);
    });
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblFormularios").length) {
                tblFormularios = $("#tblFormularios").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de formulario';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblFormularios.columns.adjust().draw();
            }

            if ($("#tblOperaciones").length) {
                tblOperaciones = $("#tblOperaciones").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2, 3, 4],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de operación';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblOperaciones.columns.adjust().draw();
            }

            if ($("#tblDocumentos").length) {
                tblDocumentos = $("#tblDocumentos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de documento';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblDocumentos.columns.adjust().draw();
            }

            if ($("#tblGestiones").length) {
                tblGestiones = $("#tblGestiones").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de documento';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblGestiones.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configFormulario = {
    id: 0,
    cnt: 0,
    resultados: {},
    init: function (datosRespuesta) {
        if (datosRespuesta.respuesta) {
            switch (datosRespuesta.respuesta) {
                case 'EXITO':
                    datosRespuesta.resultados.forEach(formulario => {
                        this.cnt++;
                        this.resultados[this.cnt] = {
                            ...formulario
                        };
                        this.addRowTbl({
                            ...formulario,
                            nFila: this.cnt
                        });
                    });
                    tblFormularios.rows().iterator('row', function (context, index) {
                        let node = $(this.row(index).node()),
                            clase = node.find('td:eq(8)').find('input[type="hidden"]').val();
                        // console.log(tmpFecha)
                        node.addClass('tr-' + clase);
                    });
                    tblFormularios.columns.adjust().draw();
                    break;
                default:
                    generalMostrarError(datosRespuesta);
                    break;
            }
        } else {
            generalMostrarError(datosRespuesta);
        }
    },
    edit: function (id = 0) {
        let tmpId = encodeURIComponent(btoa(this.resultados[id].id));
        fetchActions.get({
            modulo: 'OFICIALIA',
            archivo: 'oficialiaGetROIMesa',
            params: {
                id: tmpId
            }
        }).then(({
            data
        }) => {
            configROI.init(data.datosROI).then(() => {
                configOperaciones.init(data.operaciones).then(() => {
                    configDocumentos.init(data.documentosAdjuntos).then(() => {
                        let allSelectpickerCbos = document.querySelectorAll('select.selectpicker');
                        allSelectpickerCbos.forEach(cbo => $('#' + cbo.id).selectpicker('refresh'));
                    }).catch(generalMostrarError);
                }).catch(generalMostrarError);
            }).catch(generalMostrarError);
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        id,
        tipoDocumento,
        numeroDocumento,
        nombres,
        primerApellido,
        segundoApellido,
        apellidoCasada,
        cantidadOperaciones,
        montoOperaciones,
        nombreAgencia,
        colaborador,
        fechaRegistro,
        clase,
        nombreUsuario,
        ubicacion,
        analista
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar asignación' onclick='configFormulario.edit(" + nFila + ");'>" +
                    "<i class='fas fa-cogs'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Reimprimir' onclick='configFormulario.print(" + nFila + ");'>" +
                    "<i class='fas fa-print'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configFormulario.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                let nombreCompleto = `${nombres} ${primerApellido}`,
                    complementoNombre = segundoApellido.length > 0 ? segundoApellido : 'DE ' + apellidoCasada;
                nombreCompleto += ' ' + complementoNombre;
                tblFormularios.row.add([
                    nFila,
                    `ROI${id}`,
                    `${tipoDocumento}: ${numeroDocumento}`,
                    nombreCompleto,
                    nombreAgencia,
                    colaborador,
                    fechaRegistro + '<input type="hidden" value="' + clase + '">',
                    nombreUsuario,
                    ubicacion,
                    botones
                ]).node().id = 'trFormulario' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        let tmpId = this.resultados[id].id;
        Swal.fire({
            title: 'Eliminar formulario',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el formulario?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: 'OFICIALIA',
                    archivo: 'oficialiaEliminarROI',
                    datos: {
                        id: tmpId,
                        idApartado,
                        tipoApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta) {
                            case "EXITO":
                                tblFormularios.row('#trFormulario' + id).remove().draw();
                                delete this.resultados[id];
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    print: function (id = 0) {
        this.id = id;
        $("#mdlGeneracion").modal("show");
    },
    printFormulario() {
        window.open('./main/OFICIALIA/docs/ROI/ROI' + this.resultados[this.id].id + '.pdf');
    },
};

const configROI = {
    id: 0,
    colaboradores: {},
    datosGuardar: {},
    datosOriginales: {},
    cboTipoDocumento: document.getElementById('cboTipoDocumento'),
    campoNumeroDocumento: document.getElementById('txtNumeroDocumento'),
    campoNombres: document.getElementById('txtNombres'),
    campoPrimerApellido: document.getElementById('txtPrimerApellido'),
    campoSegundoApellido: document.getElementById('txtSegundoApellido'),
    campoApellidoCasada: document.getElementById('txtApellidoCasada'),
    campoProfesion: document.getElementById('txtProfesion'),
    cboActividadEconomica: document.getElementById('cboActividadEconomica'),
    cboPais: document.getElementById('cboPais'),
    cboDepartamento: document.getElementById('cboDepartamento'),
    cboMunicipio: document.getElementById('cboMunicipio'),
    campoDireccion: document.getElementById('txtDireccion'),
    strIntentos: document.getElementById('strIntentos'),
    campoAnalisis: document.getElementById('txtAntecedentes'),
    cboAgencia: document.getElementById('cboAgenciaDiligencia'),
    cboColaborador: document.getElementById('cboColaborador'),
    campoArea: document.getElementById('txtAreaColaborador'),
    campoCodigoJefe: document.getElementById('txtJefeInmediato'),
    intentos: 5,
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                this.id = tmpDatos.id;

                this.cboTipoDocumento.value = tmpDatos.idTipoDocumento;
                // $('#' + this.cboTipoDocumento.id).trigger('change');
                this.campoNumeroDocumento.value = tmpDatos.numeroDocumento;
                this.campoNombres.value = tmpDatos.nombres;
                this.campoPrimerApellido.value = tmpDatos.primerApellido;
                this.campoSegundoApellido.value = tmpDatos.segundoApellido;
                this.campoApellidoCasada.value = tmpDatos.apellidoCasada;
                this.campoProfesion.value = tmpDatos.profesion;
                this.cboActividadEconomica.value = tmpDatos.idActividadEconomica;
                this.cboPais.value = tmpDatos.idPais;
                $('#' + this.cboPais.id).trigger('change');
                this.cboDepartamento.value = tmpDatos.idDepartamento;
                $('#' + this.cboDepartamento.id).trigger('change');
                this.cboMunicipio.value = tmpDatos.idMunicipio;
                this.campoDireccion.value = tmpDatos.direccion;
                this.campoAnalisis.innerHTML = tmpDatos.analisisOperacion.replace('<br />', '\n');
                this.cboAgencia.value = tmpDatos.idAgencia;
                this.cboColaborador.value = tmpDatos.idColaborador;
                this.campoArea.value = tmpDatos.area;
                this.campoCodigoJefe.value = tmpDatos.codigoJefeInmediato;

                // this.datosOriginales = JSON.parse(JSON.stringify(this.getJSON()));
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },

}

const configOperaciones = {
    id: 0,
    cnt: 0,
    operaciones: {},
    tiposOperacion: {},
    catAreas: {},
    datosOriginales: {},
    cboAgencia: document.getElementById('cboAgenciaDiligencia'),
    configTipoProducto: {
        id: 0,
        cnt: 0,
        catalogo: {},
        init: function () {
            return new Promise((resolve, reject) => {
                try {
                    let opciones = [`<option selected disabled value="">seleccione</option><option value="AgregarProducto">Agregar nuevo</option><option disabled style="line-height:0.1;padding-top:0;">__________________________________</option>`];

                    for (let key in this.catalogo) {
                        opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].productoServicio}</option>`);
                    }

                    configOperaciones.cboTipoProducto.innerHTML = opciones.join("");

                    configOperaciones.cboTipoProducto.className.includes("selectpicker") && ($("#" + configOperaciones.cboTipoProducto.id).selectpicker("refresh"));
                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            });
        },
        add: function () {
            configOperaciones.configTipoProducto.cnt++;
            let tipoProducto = configOperaciones.campoTipoProducto.value,
                tmpId = "Pendiente" + configOperaciones.configTipoProducto.cnt;
            configOperaciones.configTipoProducto.catalogo[configOperaciones.configTipoProducto.cnt] = {
                id: tmpId,
                productoServicio: tipoProducto
            }

            return tmpId;
        }
    },
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(operacion => {
                    let labelTipoOperacion = this.tiposOperacion[operacion.tipoOperacion].tipoOperacion,
                        labelTipoProducto = '',
                        labelRelacion = 'Pendiente',
                        labelTipoRecepcion = operacion.tipoRecepcion,
                        labelAgencia = $('#' + this.cboAgencia.id + ' option[value="' + operacion.agencia + '"]').text(),
                        labelArea = this.catAreas[operacion.area].area,
                        labelEstado = operacion.estadoOperacion;

                    for (let key in this.configTipoProducto.catalogo) {
                        if (this.configTipoProducto.catalogo[key].id == operacion.productoServicio) {
                            labelTipoProducto = this.configTipoProducto.catalogo[key].productoServicio;
                        }
                    }
                    this.cnt++;
                    let tmp = {
                        id: operacion.id,
                        tipoOperacion: operacion.tipoOperacion,
                        labelTipoOperacion,
                        tipoProducto: operacion.productoServicio,
                        labelTipoProducto,
                        numeroProducto: operacion.numeroProducto,
                        relacion: operacion.tipoRelacion,
                        labelRelacion,
                        nombreReferente: operacion.nombreReferente,
                        monto: operacion.monto,
                        tipoRecepcion: operacion.tipoRecepcion,
                        labelTipoRecepcion,
                        agencia: operacion.agencia,
                        labelAgencia,
                        area: operacion.area,
                        labelArea,
                        fechaHora: operacion.fechaOperacion,
                        estado: operacion.estadoOperacion,
                        labelEstado
                    };
                    this.operaciones[this.cnt] = {
                        ...tmp
                    };
                    this.addRowTbl({
                        ...this.operaciones[this.cnt],
                        nFila: this.cnt
                    });
                });
                tblOperaciones.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    addRowTbl: function ({
        nFila,
        labelTipoOperacion,
        labelTipoProducto,
        numeroProducto,
        nombreReferente,
        monto,
        labelTipoRecepcion,
        labelAgencia,
        fechaHora,
        labelEstado
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configOperaciones.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configOperaciones.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblOperaciones.row.add([
                    nFila,
                    labelTipoOperacion,
                    labelTipoProducto,
                    numeroProducto,
                    nombreReferente,
                    "$" + new Intl.NumberFormat().format(parseFloat(monto).toFixed(2)),
                    labelTipoRecepcion,
                    labelAgencia,
                    fechaHora,
                    labelEstado,
                    // botones
                ]).node().id = 'trOperacion' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
}

const configDocumentos = {
    id: 0,
    cnt: 0,
    catalogo: {},
    documentos: {},
    datosOriginales: {},
    cboDocumento: document.getElementById('cboDocumentoAdjunto'),
    campoDocumento: document.getElementById('txtDocumentoAdjunto'),
    containerCbo: document.getElementById('documentoContainer'),
    containerCampo: document.getElementById('newDocumentoContainer'),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(documento => {
                    let tipoDocumento = '';
                    for (let key in this.catalogo) {
                        if (this.catalogo[key].id == documento.idTipoDocumento) {
                            tipoDocumento = this.catalogo[key].documento;
                        }
                    }
                    let tmp = {
                        id: documento.id,
                        idTipoDocumento: documento.idTipoDocumento,
                        tipoDocumento
                    }
                    this.cnt++;
                    this.documentos[this.cnt] = {
                        ...tmp
                    };
                    this.addRowTbl({
                        ...this.documentos[this.cnt],
                        nFila: this.cnt
                    });
                });
                tblDocumentos.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    addRowTbl: function ({
        nFila,
        tipoDocumento
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configDocumentos.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configDocumentos.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblDocumentos.row.add([
                    nFila,
                    tipoDocumento,
                    // botones
                ]).node().id = 'trDocumento' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    configCatalogo: {
        cnt: 0,
        add: function () {
            configDocumentos.configCatalogo.cnt++;
            configDocumentos.catalogo[configDocumentos.configCatalogo.cnt] = {
                id: 'Pendiente' + configDocumentos.configCatalogo.cnt,
                documento: configDocumentos.campoDocumento.value
            }

            return configDocumentos.catalogo[configDocumentos.configCatalogo.cnt].id;
        },
        init: function () {
            return new Promise((resolve, reject) => {
                try {
                    let opciones = [`<option selected disabled value="">seleccione</option><option value="AgregarDocumento">Agregar nuevo</option><option disabled style="line-height:0.1;padding-top:0;">__________________________________</option>`];

                    for (let key in configDocumentos.catalogo) {
                        opciones.push(`<option value="${configDocumentos.catalogo[key].id}">${configDocumentos.catalogo[key].documento}</option>`);
                    }

                    configDocumentos.cboDocumento.innerHTML = opciones.join("");

                    configDocumentos.cboDocumento.className.includes("selectpicker") && ($("#" + configDocumentos.cboDocumento.id).selectpicker("refresh"));
                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            });
        }
    },
    evalDocumento: function (opcion) {
        if (opcion == 'AgregarDocumento') {
            this.containerCbo.style.display = 'none';
            this.containerCampo.style.display = 'block';
            this.campoDocumento.required = true;
        } else {
            this.containerCampo.style.display = 'none';
            this.containerCbo.style.display = 'block';
            this.campoDocumento.required = false;
        }
    }
}

const configGestiones = {
    id: 0,
    cnt: 0,
    gestiones: {},
    datosOriginales: {},
    spnUsuarioActual: document.getElementById('spnNombreUsuario'),
    campoArchivo: document.getElementById('fileGestion'),
    nombreArchivo: document.getElementById('fileName'),
    campoGestion: document.getElementById('txtGestion'),
    solicitudArchivo: {},
    init: function () {},
    edit: function (id = 0) {
        formActions.clean('mdlGestion').then(() => {
            this.solicitudArchivo = {};
            this.id = id;

            if (this.id > 0) {

            }
            $('#mdlGestion').modal('show');
        });
    },
    save: function () {
        formActions.validate('frmGestion').then(({
            errores
        }) => {
            if (errores == 0) {
                let file = this.campoArchivo.files[0] ? this.campoArchivo.files[0] : (this.id > 0 ? this.gestiones[this.id].documentoAdjunto : null),
                    nombreAdjunto = this.campoArchivo.files[0] ? this.nombreArchivo.innerHTML : (this.id > 0 ? this.gestiones[this.id].nombreAdjunto : null),
                    fechaActual = new Date(),
                    fechaActualizacion = ("0" + fechaActual.getDate()).slice(-2) + "-" + ("0" + (fechaActual.getMonth() + 1)).slice(-2) + "-" + fechaActual.getFullYear() + " " + ("0" + fechaActual.getHours()).slice(-2) + ":" + ("0" + fechaActual.getMinutes()).slice(-2),
                    actualUser = new usuario();
                let tmp = {
                    id: this.id > 0 ? this.gestiones[this.id].id : 0,
                    gestion: this.campoGestion.value,
                    documentoAdjunto: file,
                    nombreAdjunto,
                    solicitudArchivo: {},
                    usuario: this.id > 0 ? this.gestiones[this.id].usuario : parseInt(localStorage.getItem('idUser')),
                    nombreUsuario: this.id > 0 ? this.gestiones[this.id].nombreUsuario : actualUser.actualUserName,
                    fechaRegistro: this.id > 0 ? this.gestiones[this.id].fechaRegistro : fechaActualizacion,
                    ultimaActualizacion: fechaActualizacion
                };

                let guardar = new Promise((resolve, reject) => {
                    try {
                        if (this.id == 0) {
                            this.cnt++;
                            this.gestiones[this.cnt] = {
                                ...tmp
                            }
                            this.addRowTbl({
                                ...tmp,
                                nFila: this.cnt
                            }).then(resolve).catch(reject);
                        } else {
                            let impAdjunto = tmp.nombreAdjunto == null ? 'Sin adjuntos' : nombreAdjunto;
                            $('#trGestion' + this.id).find('td:eq(1)').html(tmp.gestion);
                            $('#trGestion' + this.id).find('td:eq(3)').html(impAdjunto);
                            $('#trGestion' + this.id).find('td:eq(5)').html(tmp.ultimaActualizacion);
                            resolve();
                        }
                    } catch (err) {
                        reject(err.message);
                    }
                });

                guardar.then(() => {
                    tblGestiones.columns.adjust().draw();
                    $('#mdlGestion').modal('hide');
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    delete: function () {},
    addRowTbl: function ({
        nFila,
        gestion,
        usuario,
        nombreAdjunto,
        nombreUsuario,
        fechaRegistro,
        ultimaActualizacion
    }) {
        return new Promise((resolve, reect) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configGestiones.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configGestiones.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                let impAdjunto = nombreAdjunto == null ? 'Sin adjuntos' : nombreAdjunto;
                tblGestiones.row.add([
                    nFila,
                    gestion,
                    nombreUsuario,
                    impAdjunto,
                    fechaRegistro,
                    ultimaActualizacion,
                    botones
                ]).node().id = 'trGestion' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    configSolicitud: {
        id: 0,
        cnt: 0,
        cboDocumento: document.getElementById('cboDocumentoAdjunto'),
        cboAgencia: document.getElementById('cboAgenciaSolicitud'),
        campoDocumento: document.getElementById('txtDocumentoAdjunto'),
        edit: function () {
            formActions.clean('frmSolicitud').then(() => {
                configDocumentos.configCatalogo.init().then(() => {
                    // if (this.id > 0) {
                    //     this.cboDocumento.value = this.documentos[this.id].id;
                    //     this.cboDocumento.className.includes('selectpicker') && ($('#' + this.cboDocumento.id).selectpicker('refresh'));
                    // }

                    $('#mdlSolicitud').modal('show');
                });
            });
        },
        save: function () {
            formActions.validate('frmSolicitud').then(({
                errores
            }) => {
                if (errores == 0) {
                    let idDocumento = this.cboDocumento.value == 'AgregarDocumento' ? configDocumentos.configCatalogo.add() : this.cboDocumento.value;
                    let tmp = {
                        idDocumento,
                        tipoDocumento: this.cboDocumento.value == 'AgregarDocumento' ? this.campoDocumento.value : this.cboDocumento.options[this.cboDocumento.options.selectedIndex].text,
                        agencia: this.cboAgencia.value
                    };
                }
            }).catch(generalMostrarError);
        }
    }
};