let tblColaboradores;

window.onload = () => {
    initTables().then(() => {
        fetchActions.getCats({
            modulo: 'RRHH',
            archivo: 'rrhhGetCats',
            solicitados: ['agencias', 'cargos', 'gerencias']
        }).then(({
            agencias,
            cargos,
            gerencias
        }) => {
            const opciones = [`<option selected disabled value="">seleccione</option>`];
            let cbosAgencia = document.querySelectorAll('select.cboAgencia'),
                cbosGerencia = document.querySelectorAll('select.cboGerencia'),
                cbosCargo = document.querySelectorAll('select.cboCargo'),
                allSelectpickerCbos = document.querySelectorAll('select.selectpicker'),
                tmpOpciones = [...opciones];

            agencias.forEach(agencia => {
                tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`);
                configColaborador.agencias[agencia.id] = agencia;
            });
            cbosAgencia.forEach(cbo => cbo.innerHTML = tmpOpciones.join(''));

            tmpOpciones = [...opciones];
            gerencias.forEach(gerencia => {
                tmpOpciones.push(`<option value="${gerencia.id}">${gerencia.gerencia}</option>`);
                configColaborador.gerencias[gerencia.id] = gerencia;
            });
            cbosGerencia.forEach(cbo => cbo.innerHTML = tmpOpciones.join(''));

            tmpOpciones = [...opciones];
            cargos.forEach(cargo => {
                tmpOpciones.push(`<option value="${cargo.id}">${cargo.cargo}</option>`);
                configColaborador.cargos[cargo.id] = cargo;
            });
            cbosCargo.forEach(cbo => cbo.innerHTML = tmpOpciones.join(''));

            allSelectpickerCbos.forEach(cbo => $('#' + cbo.id).selectpicker('refresh'));
            fetchActions.get({
                modulo: 'RRHH',
                archivo: 'rrhhGetColaboradores',
                params: {
                    'solicitados': 'all'
                }
            }).then((datosRespuesta) => {
                configColaborador.init(datosRespuesta.colaboradores);
            }).catch(generalMostrarError);
        }).catch(generalMostrarError);
    });
};

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblColaboradores").length) {
                tblColaboradores = $("#tblColaboradores").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [2, 4, 5, 6],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblColaboradores.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configColaborador = {
    id: 0,
    cnt: 0,
    colaboradores: {},
    agencias: {},
    cargos: {},
    gerencias: {},
    campoNombres: document.getElementById('txtNombres'),
    campoApellidos: document.getElementById('txtApellidos'),
    cboAgencia: document.getElementById('cboAgencia'),
    cboGerencia: document.getElementById('cboGerencia'),
    cboCargo: document.getElementById('cboCargo'),
    campoCodigo: document.getElementById('txtCodigoOficialia'),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(colaborador => {
                    this.cnt++;
                    this.colaboradores[this.cnt] = {
                        ...colaborador,
                        labelAgencia: this.agencias[colaborador.agencia].nombreAgencia,
                        labelGerencia: this.gerencias[colaborador.gerencia].gerencia,
                        labelCargo: this.cargos[colaborador.cargo].cargo
                    }
                    this.addRowTbl({
                        ...this.colaboradores[this.cnt],
                        nFila: this.cnt
                    });
                });
                let indexes = tblColaboradores.rows().eq(0).filter(function (rowIdx) {
                    return tblColaboradores.cell(rowIdx, 7).data() == 'Sin asignar' ? true : false;
                });

                tblColaboradores.rows(indexes).nodes().to$().addClass('redRow');
                tblColaboradores.columns.adjust().draw();
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmColaborador').then(() => {
            this.id = id;

            if (this.id > 0) {
                let tmp = this.colaboradores[this.id];
                $("#labelUserImage").css("background-image", `url('./main/img/colaboradores/${tmp.foto}')`);
                this.campoNombres.value = tmp.nombres;
                this.campoApellidos.value = tmp.apellidos;
                this.cboAgencia.value = tmp.agencia;
                this.cboGerencia.value = tmp.gerencia;
                this.cboCargo.value = tmp.cargo;
                this.campoCodigo.value = tmp.codigoOficialia;

                document.querySelectorAll('#mdlColaborador select.selectpicker').forEach(cbo => $('#' + cbo.id).selectpicker('refresh'));
            }

            $("#mdlColaborador").modal('show');
        });
    },
    save: function () {
        formActions.validate('frmColaborador').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    ...this.colaboradores[this.id]
                };
                tmp.codigoOficialia = this.campoCodigo.value;
                fetchActions.set({
                    modulo: 'OFICIALIA',
                    archivo: 'oficialiaActualizarCodigoColaborador',
                    datos: {
                        ...tmp,
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta) {
                            case 'EXITO':
                                this.colaboradores[this.id] = {
                                    ...tmp
                                };

                                $('#trColaborador' + this.id).find('td:eq(7)').html(tmp.codigoOficialia);
                                // tblColaboradores.columns.adjust().draw();
                                $('#mdlColaborador').modal('hide');
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        foto,
        nombres,
        apellidos,
        agencia,
        labelAgencia,
        labelGerencia,
        labelCargo,
        codigoOficialia
    }) {
        return new Promise((resolve, reject) => {
            try {
                let impCodigo = codigoOficialia != null && codigoOficialia.length > 0 ? codigoOficialia : 'Sin asignar',
                    advertencia = impCodigo == 'Sin asignar' ? "fas fa-exclamation-triangle" : 'fas fa-edit';

                tblColaboradores.row.add([
                    nFila,
                    "<div class='contenedorImgTbl'>" +
                    "<img src='./main/img/colaboradores/" + foto + "' />" +
                    "</div>",
                    `${nombres} ${apellidos}`,
                    agencia,
                    labelAgencia,
                    labelGerencia,
                    labelCargo,
                    impCodigo,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configColaborador.edit(" + nFila + ");'>" +
                    "<i class='" + advertencia + "'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = 'trColaborador' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
}