let tblFormularios;

window.onload = () => {
    initTables().then(() => {
        fetchActions.getCats({
            modulo: 'OFICIALIA',
            archivo: 'oficialiaGetCats',
            solicitados: ['agencias', 'estados', 'analistas']
        }).then(({
            estados,
            agencias,
            analistas
        }) => {
            let cbosEstado = document.querySelectorAll('select.cboEstado'),
                cbosAgencia = document.querySelectorAll('select.cboAgencia'),
                cbosAnalista = document.querySelectorAll('select.cboAnalista'),
                allSelectpickerCbos = document.querySelectorAll('select.selectpicker'),
                opciones = [];

            estados.forEach(estado => opciones.push(`<option value="${estado.id}">${estado.estado}</option>`));
            cbosEstado.forEach(cbo => cbo.innerHTML = opciones.join(''));

            opciones = [`<option value="0">Todas las agencias</option>`];

            agencias.forEach(agencia => opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`));
            cbosAgencia.forEach(cbo => cbo.innerHTML = opciones.join(''));

            opciones = [`<option selected disabled value="">seleccione</option>`];

            analistas.forEach(analista => opciones.push(`<option value="${analista.id}">${analista.nombreCompleto}</option>`));
            cbosAnalista.forEach(cbo => cbo.innerHTML = opciones.join(''));

            allSelectpickerCbos.forEach(cbo => $('#' + cbo.id).selectpicker('refresh'));

            configBusqueda.search();
        }).catch(generalMostrarError);
    });
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblFormularios").length) {
                tblFormularios = $("#tblFormularios").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de formulario';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblFormularios.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configBusqueda = {
    id: 0,
    cnt: 0,
    resultados: {},
    cboEstado: document.getElementById('cboEstado'),
    cboAgencia: document.getElementById('cboAgencia'),
    cboAnalista: document.getElementById('cboAnalista'),
    search: function () {
        tblFormularios.clear().draw();
        this.cnt = 0;
        if (formActions.manualValidate('frmBusqueda')) {
            fetchActions.get({
                modulo: 'OFICIALIA',
                archivo: 'oficialiaGetROIGestion',
                params: {
                    tipoApartado,
                    idApartado,
                    estado: this.cboEstado.value,
                    agencia: this.cboAgencia.value
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta) {
                        case 'EXITO':
                            // console.log(datosRespuesta);
                            datosRespuesta.resultados.forEach(formulario => {
                                this.cnt++;
                                this.resultados[this.cnt] = {
                                    ...formulario
                                };
                                this.addRowTbl({
                                    ...formulario,
                                    nFila: this.cnt
                                });
                            });
                            tblFormularios.rows().iterator('row', function (context, index) {
                                let node = $(this.row(index).node()),
                                    clase = node.find('td:eq(8)').find('input[type="hidden"]').val();
                                // console.log(tmpFecha)
                                node.addClass('tr-' + clase);
                            });
                            tblFormularios.columns.adjust().draw();
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        }
    },
    addRowTbl: function ({
        nFila,
        id,
        tipoDocumento,
        numeroDocumento,
        nombres,
        primerApellido,
        segundoApellido,
        apellidoCasada,
        cantidadOperaciones,
        montoOperaciones,
        nombreAgencia,
        colaborador,
        fechaRegistro,
        clase,
        nombreUsuario,
        ubicacion,
        analista
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar asignación' onclick='configBusqueda.editAsignacion(" + nFila + ");'>" +
                    "<i class='fas fa-user-cog'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Reimprimir' onclick='configBusqueda.print(" + nFila + ");'>" +
                    "<i class='fas fa-print'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configBusqueda.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                let nombreCompleto = `${nombres} ${primerApellido}`,
                    complementoNombre = segundoApellido.length > 0 ? segundoApellido : 'DE ' + apellidoCasada,
                    impAnalista = analista != null && analista.length > 0 ? analista : 'Sin asignar';
                nombreCompleto += ' ' + complementoNombre;
                tblFormularios.row.add([
                    nFila,
                    `ROI${id}`,
                    `${tipoDocumento}: ${numeroDocumento}`,
                    nombreCompleto,
                    cantidadOperaciones,
                    montoOperaciones,
                    nombreAgencia,
                    colaborador,
                    fechaRegistro + '<input type="hidden" value="' + clase + '">',
                    nombreUsuario,
                    impAnalista,
                    ubicacion,
                    botones
                ]).node().id = 'trFormulario' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    print: function (id = 0) {
        this.id = id;
        $("#mdlGeneracion").modal("show");
    },
    printFormulario() {
        window.open('./main/OFICIALIA/docs/ROI/ROI' + this.resultados[this.id].id + '.pdf');
    },
    delete: function (id = 0) {
        let tmpId = this.resultados[id].id;
        Swal.fire({
            title: 'Eliminar formulario',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el formulario?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: 'OFICIALIA',
                    archivo: 'oficialiaEliminarROI',
                    datos: {
                        id: tmpId,
                        idApartado,
                        tipoApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta) {
                            case "EXITO":
                                tblFormularios.row('#trFormulario' + id).remove().draw();
                                delete this.resultados[id];
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    editAsignacion: function (id = 0) {
        formActions.clean('frmAsignacion').then(() => {
            $('div.dtr-bs-modal').find('.btn-close').trigger('click');
            let tmp = this.resultados[id];
            this.id = id;

            $('#mdlAsignacion').find('.modal-title').find('strong').html('ROI' + tmp.id);

            if (tmp.idAnalista != null) {
                this.cboAnalista.value = tmp.idAnalista;
                this.cboAnalista.className.includes('selectpicker') && ($('#' + this.cboAnalista.id).selectpicker('refresh'));
            }
            $('#mdlAsignacion').modal('show');
        });
    },
    saveAsignacion: function () {
        formActions.validate('frmAsignacion').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = this.resultados[this.id];
                fetchActions.set({
                    modulo: 'OFICIALIA',
                    archivo: 'oficialiaSetAsignacionROI',
                    datos: {
                        id: tmp.id,
                        analista: this.cboAnalista.value,
                        tipoApartado,
                        idApartado,
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta) {
                            case 'EXITO':
                                this.resultados[this.id].idAnalista = this.cboAnalista.value;
                                this.resultados[this.id].analista = this.cboAnalista.options[this.cboAnalista.options.selectedIndex].text;
                                $('#trFormulario' + this.id).find('td:eq(10)').html(this.resultados[this.id].analista);
                                $('#mdlAsignacion').modal('hide');
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    }
}