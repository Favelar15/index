let tblFormularios;

window.onload = () => {
    initTables().then(() => {
        fetchActions.get({
            modulo: 'OFICIALIA',
            archivo: 'oficialiaGetROISolicitudesEdicion',
            params: {
                'q': 'all'
            }
        }).then(({
            solicitudes
        }) => {
            configFormulario.init(solicitudes);
        }).catch(generalMostrarError);
    });
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblFormularios").length) {
                tblFormularios = $("#tblFormularios").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de formulario';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblFormularios.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configFormulario = {
    id: 0,
    cnt: 0,
    solicitudes: {},
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(solicitud => {
                    this.cnt++;
                    this.solicitudes[this.cnt] = {
                        ...solicitud
                    };
                    this.addRowTbl({
                        ...solicitud,
                        nFila: this.cnt
                    })
                });
                tblFormularios.columns.adjust().draw();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    addRowTbl: function ({
        nFila,
        tipoDocumento,
        numeroDocumento,
        nombres,
        primerApellido,
        segundoApellido,
        apellidoCasada,
        cantidadOperaciones,
        montoOperaciones,
        nombreAgencia,
        fechaRegistro,
        nombreUsuario,
        nombreSolicitante,
        fechaSolicitud
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configFormulario.evalResolucion(" + nFila + ");'>" +
                    "<i class='fas fa-spell-check'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                let nombreCompleto = `${nombres} ${primerApellido}`,
                    complementoNombre = segundoApellido.length > 0 ? segundoApellido : 'DE ' + apellidoCasada;
                nombreCompleto += ' ' + complementoNombre;
                tblFormularios.row.add([
                    nFila,
                    tipoDocumento + ':' + numeroDocumento,
                    nombreCompleto,
                    cantidadOperaciones + (cantidadOperaciones == 1 ? ' operación' : ' operaciones'),
                    montoOperaciones,
                    nombreAgencia,
                    fechaRegistro,
                    nombreUsuario,
                    nombreSolicitante,
                    fechaSolicitud,
                    botones
                ]).node().id = 'trFormulario' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    evalResolucion: function (tmpId) {
        let tmp = this.solicitudes[tmpId],
            id = tmp.id,
            nombreCompleto = `${tmp.nombres} ${tmp.primerApellido}`,
            complementoNombre = tmp.segundoApellido.length > 0 ? tmp.segundoApellido : 'DE ' + tmp.apellidoCasada;
        nombreCompleto += ' ' + complementoNombre;
        Swal.fire({
            title: 'Solicitud de edición',
            html: 'ROI a nombre de <br /><b>' + nombreCompleto + '</b>',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Aprobar',
            denyButtonText: `Denegar`,
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            let saveDB = false,
                resolucion = null;
            if (result.isConfirmed) {
                resolucion = 'APROBADA';
                saveDB = true;
            } else if (result.isDenied) {
                resolucion = 'DENEGADA';
                saveDB = true;
            }

            if (saveDB) {
                fetchActions.set({
                    modulo: 'OFICIALIA',
                    archivo: 'oficialiaSaveResolucionEdicionROI',
                    datos: {
                        id: id,
                        resolucion: resolucion,
                        idApartado,
                        tipoApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta) {
                            case 'EXITO':
                                tblFormularios.row('#trFormulario' + tmpId).remove().draw();
                                delete this.solicitudes[tmpId];
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        })
    }
}