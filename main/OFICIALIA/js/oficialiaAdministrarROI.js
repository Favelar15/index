let tblFormularios;

window.onload = () => {
    initTables().then(() => {
        fetchActions.getCats({
            modulo: 'OFICIALIA',
            archivo: 'oficialiaGetCats',
            solicitados: ['agencias']
        }).then(({
            agencias
        }) => {
            const opciones = [`<option selected disabled value="">seleccione</option>`];
            let allAgenciaCbos = document.querySelectorAll('select.cboAgencia'),
                allSelectpickerCbos = document.querySelectorAll('select.selectpicker'),
                tmpOpciones = [...opciones];

            agencias.forEach(agencia => tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`));
            allAgenciaCbos.forEach(cbo => cbo.innerHTML = tmpOpciones.join(''));

            allSelectpickerCbos.forEach(cbo => $('#' + cbo.id).selectpicker('refresh'));
        }).catch(generalMostrarError);
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblFormularios").length) {
                tblFormularios = $("#tblFormularios").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de formulario';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblFormularios.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configBusqueda = {
    id: 0,
    cboAgencia: document.getElementById('cboAgencia'),
    campoInicio: document.getElementById('txtInicio'),
    campoFin: document.getElementById('txtFin'),
    cnt: 0,
    resultados: {},
    search: function () {
        formActions.validate('frmBusqueda').then(({
            errores
        }) => {
            if (errores == 0) {
                tblFormularios.clear().draw();
                this.cnt = 0;
                this.resultados = {};
                fetchActions.get({
                    modulo: "OFICIALIA",
                    archivo: 'oficialiaGetROIAdministracion',
                    params: {
                        agencia: this.cboAgencia.value,
                        inicio: encodeURIComponent(this.campoInicio.value),
                        fin: encodeURIComponent(this.campoFin.value),
                        tipoApartado,
                        idApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                datosRespuesta.resultados.forEach(formulario => {
                                    this.cnt++;
                                    this.resultados[this.cnt] = {
                                        ...formulario
                                    };
                                    this.addRowTbl({
                                        ...formulario,
                                        nFila: this.cnt
                                    });
                                });
                                tblFormularios.columns.adjust().draw();
                                break
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        tipoDocumento,
        numeroDocumento,
        nombreAgencia,
        nombres,
        primerApellido,
        segundoApellido,
        apellidoCasada,
        profesion,
        colaborador,
        area,
        fechaRegistro,
        nombreUsuario,
        cantidadOperaciones,
        montoOperaciones
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configBusqueda.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Reimprimir' onclick='configBusqueda.print(" + nFila + ");'>" +
                    "<i class='fas fa-print'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configBusqueda.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                let nombreCompleto = `${nombres} ${primerApellido}`,
                    complementoNombre = segundoApellido.length > 0 ? segundoApellido : 'DE ' + apellidoCasada;
                nombreCompleto += ' ' + complementoNombre;
                tblFormularios.row.add([
                    nFila,
                    tipoDocumento + ':' + numeroDocumento,
                    nombreCompleto,
                    profesion,
                    cantidadOperaciones + (cantidadOperaciones == 1 ? ' operación' : ' operaciones'),
                    montoOperaciones,
                    nombreAgencia,
                    colaborador,
                    area,
                    fechaRegistro,
                    nombreUsuario,
                    botones
                ]).node().id = 'trFormulario' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        let tipo = 'Sub',
            tmpIdApartado = 'NTA%3D',
            tmpId = encodeURIComponent(btoa(this.resultados[id].id));
        fetchActions.get({
            modulo: 'OFICIALIA',
            archivo: 'oficialiaValidarPermisoEdicionROI',
            params: {
                tipoApartado: tipo,
                idApartado: tmpIdApartado,
                id: tmpId
            }
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta) {
                    case 'NoSolicitud':
                        Swal.fire({
                            title: 'Editar formulario',
                            icon: 'warning',
                            html: 'Actualmente no tienes permiso para editar este formulario.<br />¿Deseas solicitar permiso de edición a tu jefe inmediato?',
                            showCloseButton: false,
                            showCancelButton: true,
                            focusConfirm: false,
                            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Solicitar',
                            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                configBusqueda.crearSolicitud(tmpId, id);
                            }
                        });
                        break;
                    case 'PENDIENTE':
                        Swal.fire({
                            title: '¡Atención!',
                            icon: 'info',
                            html: 'Parece que tu solicitud de edición aún no tiene resolución.'
                        });
                        break;
                    case 'APROBADA':
                        this.openEdit(id, datosRespuesta.idSolicitud);
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    crearSolicitud: function (id, tmpId) {
        let tmp = this.resultados[tmpId],
            nombreCompleto = `${tmp.nombres} ${tmp.primerApellido}`,
            complementoNombre = tmp.segundoApellido.length > 0 ? tmp.segundoApellido : 'DE ' + tmp.apellidoCasada;
        nombreCompleto += ' ' + complementoNombre;
        fetchActions.set({
            modulo: 'OFICIALIA',
            archivo: 'oficialiaSaveSolicitudEdicionROI',
            datos: {
                id: id,
                idApartado,
                tipoApartado,
                nombreCompleto,
            }
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta) {
                    case 'EXITO':
                        Swal.fire({
                            icon: 'success',
                            html: 'Tu solicitud de edición se envió exitosamente y está a la espera de la resolución de un jefe inmediato.',
                            title: 'Bien hecho!'
                        });
                        break;
                    case 'PENDIENTE':
                        Swal.info({
                            title: '¡Atención!',
                            html: 'Parece que la solicitud de edición para este formulario aún no tiene resolución.',
                            icon: 'info'
                        });
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch(generalMostrarError);
    },
    openEdit: function (id = 0, idSolicitud = 0) {
        let tmp = this.resultados[id],
            urlId = encodeURIComponent(btoa(tmp.id)),
            urlIdSolicitud = encodeURIComponent(btoa(idSolicitud));
        window.top.location.href = `./?page=oficialiaROI&mod=T0ZJQ0lBTElB&id=${urlId}&auth=${urlIdSolicitud}`;
    },
    print: function (id = 0) {
        this.id = id;
        $("#mdlGeneracion").modal("show");
    },
    delete: function (id = 0) {
        let tmpId = this.resultados[id].id;
        Swal.fire({
            title: 'Eliminar formulario',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el formulario?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo: 'OFICIALIA',
                    archivo: 'oficialiaEliminarROI',
                    datos: {
                        id: tmpId,
                        idApartado,
                        tipoApartado
                    }
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta) {
                            case "EXITO":
                                tblFormularios.row('#trFormulario' + id).remove().draw();
                                delete this.resultados[id];
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    printFormulario() {
        window.open('./main/OFICIALIA/docs/ROI/ROI' + this.resultados[this.id].id + '.pdf');
    },
}