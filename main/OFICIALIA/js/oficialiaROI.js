let tblOperaciones,
    tblDocumentos;

window.onload = () => {
    document.getElementById('frmPrincipal').reset();
    document.getElementById('frmAntecedentes').reset();
    document.getElementById('frmDiligencia').reset();
    initTables().then(() => {
        fetchActions.getCats({
            modulo: "OFICIALIA",
            archivo: "oficialiaGetCats",
            solicitados: ['all']
        }).then(({
            tiposDocumento: documentos,
            agencias,
            datosGeograficos: geograficos,
            actividadesEconomicas,
            tiposOperacion,
            productosServicios,
            areas,
            colaboradores,
            tiposDocumentoAdjunto,
        }) => {
            let cbosTipoDocumento = document.querySelectorAll('select.cboTipoDocumento'),
                cbosAgencia = document.querySelectorAll('select.cboAgencia'),
                cbosPais = document.querySelectorAll('select.cboPais'),
                cbosActividadEconomica = document.querySelectorAll('select.cboActividadEconomica'),
                cbosTipoOperacion = document.querySelectorAll('select.cboTipoOperacion'),
                cbosArea = document.querySelectorAll('select.cboArea'),
                cbosColaborador = document.querySelectorAll('select.cboColaborador'),
                cbosTipoDocumentoAdjunto = document.querySelectorAll('select.cboTipoDocumentoAdjunto'),
                allSelectpickerCbos = document.querySelectorAll('select.selectpicker');

            const opciones = [`<option selected disabled value="">seleccione</option>`];
            let tmpOpciones = [...opciones];

            documentos.forEach(tipo => {
                tiposDocumento[tipo.id] = {
                    ...tipo
                };
                tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
            });

            cbosTipoDocumento.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

            datosGeograficos = {
                ...geograficos
            };
            tmpOpciones = [...opciones];
            for (let key in datosGeograficos) {
                tmpOpciones.push(`<option value="${datosGeograficos[key].id}">${datosGeograficos[key].nombrePais}</option>`);
            }

            cbosPais.forEach(cbo => cbo.innerHTML = tmpOpciones.join(""));

            tmpOpciones = [...opciones];
            actividadesEconomicas.forEach(actividad => tmpOpciones.push(`<option value="${actividad.id}">${actividad.actividadEconomica}</option>`));
            cbosActividadEconomica.forEach(cbo => cbo.innerHTML = tmpOpciones);

            tmpOpciones = [...opciones];
            agencias.forEach(agencia => tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`));
            cbosAgencia.forEach(cbo => cbo.innerHTML = tmpOpciones);

            tmpOpciones = [...opciones];
            tiposOperacion.forEach(tipo => tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoOperacion}</option>`));
            cbosTipoOperacion.forEach(cbo => cbo.innerHTML = tmpOpciones);

            tmpOpciones = [...opciones];
            colaboradores.forEach(colaborador => {
                tmpOpciones.push(`<option value="${colaborador.id}">${colaborador.nombres} ${colaborador.apellidos}</option>`);
                configROI.colaboradores[colaborador.id] = {
                    ...colaborador
                };
            });
            cbosColaborador.forEach(cbo => cbo.innerHTML = tmpOpciones);

            tmpOpciones = [...opciones];
            areas.forEach(area => tmpOpciones.push(`<option value="${area.id}">${area.area}</option>`));
            cbosArea.forEach(cbo => cbo.innerHTML = tmpOpciones);

            tmpOpciones = [...opciones];
            tiposDocumentoAdjunto.forEach(tipo => {
                configDocumentos.configCatalogo.cnt++;
                configDocumentos.catalogo[configDocumentos.configCatalogo.cnt] = {
                    ...tipo
                };
                tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
            });
            cbosTipoDocumentoAdjunto.forEach(cbo => cbo.innerHTML = tmpOpciones);

            productosServicios.forEach(producto => {
                configOperaciones.configTipoProducto.cnt++;
                configOperaciones.configTipoProducto.catalogo[configOperaciones.configTipoProducto.cnt] = {
                    ...producto
                };
            });

            allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));

            let urlId = getParameterByName('id'),
                urlAuth = getParameterByName('auth'),
                data = {};
            if (urlId) {
                let validateAuth = new Promise((resolve, reject) => {
                    try {
                        let granEdit = false;
                        if (urlAuth) {
                            fetchActions.get({
                                modulo: 'OFICIALIA',
                                archivo: 'oficialiaGetROI',
                                params: {
                                    id: urlId,
                                    auth: urlAuth
                                }
                            }).then((datosRespuesta) => {
                                if (datosRespuesta.respuesta) {
                                    switch (datosRespuesta.respuesta) {
                                        case 'APROBADA':
                                            granEdit = true;
                                            data = datosRespuesta.data;
                                            resolve(granEdit);
                                            break;
                                        default:
                                            generalMostrarError(datosRespuesta);
                                            resolve(granEdit);
                                            break;
                                    }
                                } else {
                                    generalMostrarError(datosRespuesta);
                                }
                            }).catch(generalMostrarError);
                        }
                    } catch (err) {
                        reject(err.message);
                    }
                });

                validateAuth.then((granEdit) => {
                    if (granEdit) {
                        $('.preloader').fadeIn('fast');
                        configROI.init(data.datosROI).then(() => {
                            configOperaciones.init(data.operaciones).then(() => {
                                configDocumentos.init(data.documentosAdjuntos).then(() => {
                                    let allSelectpickerCbos = document.querySelectorAll('select.selectpicker');
                                    allSelectpickerCbos.forEach(cbo => $('#' + cbo.id).selectpicker('refresh'));
                                    $('.preloader').fadeOut('fast');
                                });
                            });
                        });
                    } else {
                        Swal.fire({
                            title: '¡Atención!',
                            html: 'Parece que no tienes autorización para editar este documento',
                            icon: 'warning'
                        }).then(() => {
                            window.top.location.href = './';
                        });
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    });
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($("#tblOperaciones").length) {
                tblOperaciones = $("#tblOperaciones").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1, 2, 3, 4],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de operación';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblOperaciones.columns.adjust().draw();
            }

            if ($("#tblDocumentos").length) {
                tblDocumentos = $("#tblDocumentos").DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [1],
                        "className": "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de documento';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblDocumentos.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configROI = {
    id: 0,
    colaboradores: {},
    datosGuardar: {},
    datosOriginales: {},
    cboTipoDocumento: document.getElementById('cboTipoDocumento'),
    campoNumeroDocumento: document.getElementById('txtNumeroDocumento'),
    campoNombres: document.getElementById('txtNombres'),
    campoPrimerApellido: document.getElementById('txtPrimerApellido'),
    campoSegundoApellido: document.getElementById('txtSegundoApellido'),
    campoApellidoCasada: document.getElementById('txtApellidoCasada'),
    campoProfesion: document.getElementById('txtProfesion'),
    cboActividadEconomica: document.getElementById('cboActividadEconomica'),
    cboPais: document.getElementById('cboPais'),
    cboDepartamento: document.getElementById('cboDepartamento'),
    cboMunicipio: document.getElementById('cboMunicipio'),
    campoDireccion: document.getElementById('txtDireccion'),
    strIntentos: document.getElementById('strIntentos'),
    campoAnalisis: document.getElementById('txtAntecedentes'),
    cboAgencia: document.getElementById('cboAgenciaDiligencia'),
    cboColaborador: document.getElementById('cboColaborador'),
    campoArea: document.getElementById('txtAreaColaborador'),
    campoCodigoJefe: document.getElementById('txtJefeInmediato'),
    intentos: 5,
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                this.id = tmpDatos.id;

                this.cboTipoDocumento.value = tmpDatos.idTipoDocumento;
                $('#' + this.cboTipoDocumento.id).trigger('change');
                this.campoNumeroDocumento.value = tmpDatos.numeroDocumento;
                this.campoNombres.value = tmpDatos.nombres;
                this.campoPrimerApellido.value = tmpDatos.primerApellido;
                this.campoSegundoApellido.value = tmpDatos.segundoApellido;
                this.campoApellidoCasada.value = tmpDatos.apellidoCasada;
                this.campoProfesion.value = tmpDatos.profesion;
                this.cboActividadEconomica.value = tmpDatos.idActividadEconomica;
                this.cboPais.value = tmpDatos.idPais;
                $('#' + this.cboPais.id).trigger('change');
                this.cboDepartamento.value = tmpDatos.idDepartamento;
                $('#' + this.cboDepartamento.id).trigger('change');
                this.cboMunicipio.value = tmpDatos.idMunicipio;
                this.campoDireccion.value = tmpDatos.direccion;
                this.campoAnalisis.innerHTML = tmpDatos.analisisOperacion.replace('<br />', '\n');
                this.cboAgencia.value = tmpDatos.idAgencia;
                this.cboColaborador.value = tmpDatos.idColaborador;
                this.campoArea.value = tmpDatos.area;
                this.campoCodigoJefe.value = tmpDatos.codigoJefeInmediato;

                this.datosOriginales = JSON.parse(JSON.stringify(this.getJSON()));
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    prepareData: function () {
        let datos = {
            datosROI: JSON.parse(JSON.stringify(this.getJSON())),
            operaciones: JSON.parse(JSON.stringify(configOperaciones.operaciones)),
            documentosAdjuntos: JSON.parse(JSON.stringify(configDocumentos.documentos))
        };

        return {
            ...datos
        }
    },
    evalCambios: function () {
        return new Promise((resolve, reject) => {
            try {
                let incluyeCambios = false;
                configROI.datosGuardar = {
                    ...configROI.prepareData()
                };

                if (Object.keys(configROI.datosOriginales).length == 0) {
                    configROI.datosGuardar.datosROI.logs = {
                        descripcion: "Creación de formulario"
                    }
                    incluyeCambios = true;
                } else {
                    let detallesLog = [];
                    for (let key in configROI.datosOriginales) {
                        for (let key2 in configROI.datosGuardar.datosROI) {
                            if ((key == key2) && !key.includes('label')) {
                                if (configROI.datosOriginales[key] != configROI.datosGuardar.datosROI[key2]) {
                                    detallesLog.push({
                                        campo: key,
                                        valorAnterior: configROI.datosOriginales[key],
                                        nuevoValor: configROI.datosGuardar.datosROI[key2]
                                    });
                                }
                            }
                        }
                    }

                    if (detallesLog.length > 0) {
                        configROI.datosGuardar.datosROI.logs = {
                            descripcion: 'Edición de formulario',
                            detalles: detallesLog
                        }
                    }
                }

                if (Object.keys(configOperaciones.datosOriginales).length == 0) {
                    for (let key in configROI.datosGuardar.operaciones) {
                        configROI.datosGuardar.operaciones[key].logs = {
                            accion: 'Guardar',
                            descripcion: 'Creación de operación'
                        }
                    }
                    incluyeCambios = true;
                } else {
                    for (let key in configROI.datosGuardar.operaciones) {
                        for (let key2 in configOperaciones.datosOriginales) {
                            if (key == key2) {
                                let detallesLog = [];
                                for (let key3 in configROI.datosGuardar.operaciones[key]) {
                                    for (let key4 in configOperaciones.datosOriginales[key2]) {
                                        if (key3 == key4 && !key4.includes('label')) {
                                            if (configROI.datosGuardar.operaciones[key][key3] != configOperaciones.datosOriginales[key2][key4]) {
                                                detallesLog.push({
                                                    campo: key3,
                                                    valorAnterior: configOperaciones.datosOriginales[key2][key4],
                                                    nuevoValor: configROI.datosGuardar.operaciones[key][key3]
                                                });
                                            }
                                        }
                                    }
                                }

                                if (detallesLog.length > 0) {
                                    configROI.datosGuardar.operaciones[key].logs = {
                                        accion: 'Guardar',
                                        descripcion: 'Edición de operación',
                                        detalles: detallesLog
                                    }
                                }
                            }
                        }
                    }

                    let tmpOperaciones = Object.keys(configROI.datosGuardar.operaciones);
                    for (let key in configOperaciones.datosOriginales) {
                        if (!tmpOperaciones.includes(key)) {
                            configROI.datosGuardar.operaciones[key] = {
                                id: configOperaciones.datosOriginales[key].id,
                                logs: {
                                    accion: 'Eliminar',
                                    descripcion: 'Eliminación de operación'
                                }
                            }
                        }
                    }
                }

                if (Object.keys(configDocumentos.datosOriginales).length == 0) {
                    for (let key in configROI.datosGuardar.documentosAdjuntos) {
                        configROI.datosGuardar.documentosAdjuntos[key].logs = {
                            accion: 'Guardar',
                            descripcion: 'Creación de documento adjunto'
                        }
                    }
                    incluyeCambios = true;
                } else {
                    for (let key in configROI.datosGuardar.documentosAdjuntos) {
                        for (let key2 in configDocumentos.datosOriginales) {
                            if (key == key2) {
                                let detallesLog = [];
                                for (let key3 in configROI.datosGuardar.documentosAdjuntos[key]) {
                                    for (let key4 in configDocumentos.datosOriginales[key2]) {
                                        if (key3 == key4 && !key4.includes('label')) {
                                            if (configROI.datosGuardar.documentosAdjuntos[key][key3] != configDocumentos.datosOriginales[key2][key4]) {
                                                detallesLog.push({
                                                    campo: key3,
                                                    valorAnterior: configDocumentos.datosOriginales[key2][key4],
                                                    nuevoValor: configROI.datosGuardar.documentosAdjuntos[key][key3]
                                                });
                                            }
                                        }
                                    }
                                }

                                if (detallesLog.length > 0) {
                                    configROI.datosGuardar.documentosAdjuntos[key].logs = {
                                        accion: 'Guardar',
                                        descripcion: 'Edición de documento adjunto',
                                        detalles: detallesLog
                                    }
                                }
                            }
                        }
                    }

                    let tmpDocumentos = Object.keys(configROI.datosGuardar.documentosAdjuntos);
                    for (let key in configDocumentos.datosOriginales) {
                        if (!tmpDocumentos.includes(key)) {
                            configROI.datosGuardar.documentosAdjuntos[key] = {
                                id: configDocumentos.datosOriginales[key].id,
                                logs: {
                                    accion: 'Eliminar',
                                    descripcion: 'Eliminación de documento adjunto'
                                }
                            }
                        }
                    }
                }
                resolve(incluyeCambios);
            } catch (err) {
                reject(err.message);
            }
        });
    },
    save: function () {
        if (formActions.manualValidate('frmPrincipal')) {
            if (Object.keys(configOperaciones.operaciones).length > 0) {
                if (formActions.manualValidate('frmAntecedentes')) {
                    if (Object.keys(configDocumentos.documentos).length == 0 || Object.keys(configDocumentos.documentos).length > 0) {
                        if (formActions.manualValidate('frmDiligencia')) {
                            this.strIntentos.innerHTML = this.intentos;
                            $('#mdlGeneracion').modal('show');
                        } else {
                            document.getElementById('frmDiligencia').scrollIntoView();
                        }
                    }
                } else {
                    const id = 'frmAntecedentes';
                    const yOffset = -100;
                    const element = document.getElementById(id);
                    const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;

                    window.scrollTo({
                        top: y,
                        behavior: 'smooth'
                    });
                }
            } else {
                Swal.fire({
                    title: '¡Atención!',
                    html: 'Debe ingresar al menos una operación',
                    icon: 'warning'
                });
            }
        }
    },
    saveDB: function () {
        this.evalCambios().then((incluyeCambios) => {
            console.log(incluyeCambios)
            fetchActions.set({
                modulo: 'OFICIALIA',
                archivo: 'oficialiaSaveROI',
                datos: {
                    ...this.datosGuardar,
                    idApartado,
                    tipoApartado
                }
            }).then((datosRespuesta) => {
                console.log(datosRespuesta);
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta) {
                        case 'EXITO':
                            this.id = datosRespuesta.id;
                            for (let key in datosRespuesta.idsDocumentosAdjuntos) {
                                configDocumentos.documentos[key].id = datosRespuesta.idsDocumentosAdjuntos[key].id;
                                configDocumentos.documentos[key].idTipoDocumento = datosRespuesta.idsDocumentosAdjuntos[key].idTipoDocumento;
                            }

                            for (let key in datosRespuesta.idsOperaciones) {
                                configOperaciones.operaciones[key].id = datosRespuesta.idsOperaciones[key].id;
                                configOperaciones.operaciones[key].tipoProducto = datosRespuesta.idsOperaciones[key].idTipoProducto;
                            }

                            if (Object.keys(datosRespuesta.nuevosTiposDocumento).length > 0) {
                                for (let key in datosRespuesta.nuevosTiposDocumento) {
                                    for (let key2 in configDocumentos.catalogo) {
                                        if (configDocumentos.catalogo[key2].id == key) {
                                            configDocumentos.catalogo[key2].id = datosRespuesta.nuevosTiposDocumento[key];
                                        }
                                    }
                                }
                            }

                            if (Object.keys(datosRespuesta.nuevosTiposProductos).length > 0) {
                                for (let key in datosRespuesta.nuevosTiposProductos) {
                                    for (let key2 in configOperaciones.configTipoProducto.catalogo) {
                                        if (configOperaciones.configTipoProducto.catalogo[key2].id == key) {
                                            configOperaciones.configTipoProducto.catalogo[key2].id = datosRespuesta.nuevosTiposProductos[key];
                                        }
                                    }
                                }
                            }

                            configROI.datosOriginales = JSON.parse(JSON.stringify(configROI.getJSON()));
                            configOperaciones.datosOriginales = JSON.parse(JSON.stringify(configOperaciones.operaciones));
                            configDocumentos.datosOriginales = JSON.parse(JSON.stringify(configDocumentos.documentos));

                            window.open("./main/OFICIALIA/docs/ROI/" + datosRespuesta.nombreArchivo);
                            this.intentos--;
                            this.strIntentos.innerHTML = this.intentos;
                            if (this.intentos == 0) {
                                window.top.location.reload();
                            }
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        }).catch(generalMostrarError);
    },
    getNombreCompleto: function () {
        let nombreCompleto = this.campoNombres.value + ' ' + this.campoPrimerApellido.value;

        nombreCompleto += this.campoApellidoCasada.value ? ' DE ' + this.campoApellidoCasada.value : ' ' + this.campoSegundoApellido.value;

        return nombreCompleto;
    },
    getJSON: function () {
        return {
            id: this.id,
            tipoDocumento: this.cboTipoDocumento.value,
            labelTipoDocumento: this.cboTipoDocumento.options[this.cboTipoDocumento.options.selectedIndex].text,
            numeroDocumento: this.campoNumeroDocumento.value,
            nombres: this.campoNombres.value,
            primerApellido: this.campoPrimerApellido.value,
            segundoApellido: this.campoSegundoApellido.value,
            apellidoCasada: this.campoApellidoCasada.value,
            profesion: this.campoProfesion.value,
            actividadEconomica: this.cboActividadEconomica.value,
            labelActividadEconomica: this.cboActividadEconomica.options[this.cboActividadEconomica.options.selectedIndex].text,
            pais: this.cboPais.value,
            labelPais: this.cboPais.options[this.cboPais.options.selectedIndex].text,
            departamento: this.cboDepartamento.value,
            labelDepartamento: this.cboDepartamento.options[this.cboDepartamento.options.selectedIndex].text,
            municipio: this.cboMunicipio.value,
            labelMunicipio: this.cboMunicipio.options[this.cboMunicipio.options.selectedIndex].text,
            direccion: this.campoDireccion.value,
            analisis: this.campoAnalisis.value,
            agencia: this.cboAgencia.value,
            labelAgencia: this.cboAgencia.options[this.cboAgencia.options.selectedIndex].text,
            colaborador: this.cboColaborador.value,
            labelColaborador: this.cboColaborador.options[this.cboColaborador.options.selectedIndex].text,
            codigoColaborador: this.colaboradores[this.cboColaborador.value].codigoOficialia,
            area: this.campoArea.value,
            codigoJefeInmediato: this.campoCodigoJefe.value
        }
    }
}

const configOperaciones = {
    id: 0,
    cnt: 0,
    operaciones: {},
    datosOriginales: {},
    cboTipoOperacion: document.getElementById('cboTipoOperacion'),
    cboTipoProducto: document.getElementById('cboTipoProducto'),
    campoTipoProducto: document.getElementById('txtTipoProducto'),
    campoNumeroProducto: document.getElementById('txtNumeroProducto'),
    cboTipoRelacion: document.getElementById('cboTipoRelacion'),
    campoNombre: document.getElementById('txtNombreRelacion'),
    campoMonto: document.getElementById('numMonto'),
    cboTipoRecepcion: document.getElementById('cboTipoRecepcion'),
    cboAgencia: document.getElementById('cboAgencia'),
    cboArea: document.getElementById('cboArea'),
    campoFecha: document.getElementById('txtFechaOperacion'),
    cboEstado: document.getElementById('cboEstadoOperacion'),
    containerCboProducto: document.getElementById('containerCboProducto'),
    containerProducto: document.getElementById('containerProducto'),
    configTipoProducto: {
        id: 0,
        cnt: 0,
        catalogo: {},
        init: function () {
            return new Promise((resolve, reject) => {
                try {
                    let opciones = [`<option selected disabled value="">seleccione</option><option value="AgregarProducto">Agregar nuevo</option><option disabled style="line-height:0.1;padding-top:0;">__________________________________</option>`];

                    for (let key in this.catalogo) {
                        opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].productoServicio}</option>`);
                    }

                    configOperaciones.cboTipoProducto.innerHTML = opciones.join("");

                    configOperaciones.cboTipoProducto.className.includes("selectpicker") && ($("#" + configOperaciones.cboTipoProducto.id).selectpicker("refresh"));
                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            });
        },
        add: function () {
            configOperaciones.configTipoProducto.cnt++;
            let tipoProducto = configOperaciones.campoTipoProducto.value,
                tmpId = "Pendiente" + configOperaciones.configTipoProducto.cnt;
            configOperaciones.configTipoProducto.catalogo[configOperaciones.configTipoProducto.cnt] = {
                id: tmpId,
                productoServicio: tipoProducto
            }

            return tmpId;
        }
    },
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(operacion => {
                    let labelTipoOperacion = $('#' + this.cboTipoOperacion.id + ' option[value="' + operacion.tipoOperacion + '"]').text(),
                        labelTipoProducto = '',
                        labelRelacion = $('#' + this.cboTipoRelacion.id + ' option[value="' + operacion.tipoRelacion + '"]').text(),
                        labelTipoRecepcion = $('#' + this.cboTipoRecepcion.id + ' option[value="' + operacion.tipoRecepcion + '"]').text(),
                        labelAgencia = $('#' + this.cboAgencia.id + ' option[value="' + operacion.agencia + '"]').text(),
                        labelArea = $('#' + this.cboArea.id + ' option[value="' + operacion.area + '"]').text(),
                        labelEstado = $('#' + this.cboEstado.id + ' option[value="' + operacion.estadoOperacion + '"]').text();

                    for (let key in this.configTipoProducto.catalogo) {
                        if (this.configTipoProducto.catalogo[key].id == operacion.productoServicio) {
                            labelTipoProducto = this.configTipoProducto.catalogo[key].productoServicio;
                        }
                    }
                    this.cnt++;
                    let tmp = {
                        id: operacion.id,
                        tipoOperacion: operacion.tipoOperacion,
                        labelTipoOperacion,
                        tipoProducto: operacion.productoServicio,
                        labelTipoProducto,
                        numeroProducto: operacion.numeroProducto,
                        relacion: operacion.tipoRelacion,
                        labelRelacion,
                        nombreReferente: operacion.nombreReferente,
                        monto: operacion.monto,
                        tipoRecepcion: operacion.tipoRecepcion,
                        labelTipoRecepcion,
                        agencia: operacion.agencia,
                        labelAgencia,
                        area: operacion.area,
                        labelArea,
                        fechaHora: operacion.fechaOperacion,
                        estado: operacion.estadoOperacion,
                        labelEstado
                    };
                    this.operaciones[this.cnt] = {
                        ...tmp
                    };
                    this.addRowTbl({
                        ...this.operaciones[this.cnt],
                        nFila: this.cnt
                    });
                });
                tblOperaciones.columns.adjust().draw();
                this.datosOriginales = JSON.parse(JSON.stringify(configOperaciones.operaciones));
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmOperacion').then(() => {
            this.configTipoProducto.init();
            this.id = id;
            if (this.id > 0) {
                let tmp = this.operaciones[this.id],
                    allSelectpickerCbos = document.querySelectorAll('#frmOperacion select.selectpicker');

                this.cboTipoOperacion.value = tmp.tipoOperacion;
                this.cboTipoProducto.value = tmp.tipoProducto;
                $('#' + this.cboTipoProducto.id).trigger('change');
                this.campoNumeroProducto.value = tmp.numeroProducto;
                this.cboTipoRelacion.value = tmp.relacion;
                this.campoNombre.value = tmp.nombreReferente;
                this.campoMonto.value = tmp.monto;
                this.cboTipoRecepcion.value = tmp.tipoRecepcion;
                this.cboAgencia.value = tmp.agencia;
                this.cboArea.value = tmp.area;
                this.campoFecha.value = tmp.fechaHora;
                $("#" + this.campoFecha.id).datetimepicker("update", tmp.fechaHora);
                this.cboEstado.value = tmp.estado;

                allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));
            }

            $("#mdlOperacion").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmOperacion').then(({
            errores
        }) => {
            if (errores == 0) {
                let idTipoProducto = this.cboTipoProducto.value != 'AgregarProducto' ? this.cboTipoProducto.value : this.configTipoProducto.add();
                let tmp = {
                    id: this.id > 0 ? this.operaciones[this.id].id : 0,
                    tipoOperacion: this.cboTipoOperacion.value,
                    labelTipoOperacion: this.cboTipoOperacion.options[this.cboTipoOperacion.options.selectedIndex].text,
                    tipoProducto: idTipoProducto,
                    labelTipoProducto: this.cboTipoProducto.value != 'AgregarProducto' ? this.cboTipoProducto.options[this.cboTipoProducto.options.selectedIndex].text : this.campoTipoProducto.value,
                    numeroProducto: this.campoNumeroProducto.value,
                    relacion: this.cboTipoRelacion.value,
                    labelRelacion: this.cboTipoRelacion.options[this.cboTipoRelacion.options.selectedIndex].text,
                    nombreReferente: this.campoNombre.value,
                    monto: this.campoMonto.value,
                    tipoRecepcion: this.cboTipoRecepcion.value,
                    labelTipoRecepcion: this.cboTipoRecepcion.options[this.cboTipoRecepcion.options.selectedIndex].text,
                    agencia: this.cboAgencia.value,
                    labelAgencia: this.cboAgencia.options[this.cboAgencia.options.selectedIndex].text,
                    area: this.cboArea.value,
                    labelArea: this.cboArea.options[this.cboArea.options.selectedIndex].text,
                    fechaHora: this.campoFecha.value,
                    estado: this.cboEstado.value,
                    labelEstado: this.cboEstado.options[this.cboEstado.options.selectedIndex].text
                };

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $("#trOperacion" + this.id).find("td:eq(1)").html(tmp.labelTipoOperacion);
                        $("#trOperacion" + this.id).find("td:eq(2)").html(tmp.labelTipoProducto);
                        $("#trOperacion" + this.id).find("td:eq(3)").html(tmp.numeroProducto);
                        $("#trOperacion" + this.id).find("td:eq(4)").html(tmp.nombreReferente);
                        $("#trOperacion" + this.id).find("td:eq(5)").html("$" + new Intl.NumberFormat().format(parseFloat(tmp.monto).toFixed(2)));
                        $("#trOperacion" + this.id).find("td:eq(6)").html(tmp.labelTipoRecepcion);
                        $("#trOperacion" + this.id).find("td:eq(7)").html(tmp.labelAgencia);
                        $("#trOperacion" + this.id).find("td:eq(8)").html(tmp.fechaHora);
                        $("#trOperacion" + this.id).find("td:eq(9)").html(tmp.labelEstado);
                        resolve();
                    }
                });

                guardar.then(() => {
                    tblOperaciones.columns.adjust().draw();
                    this.operaciones[this.id] = {
                        ...tmp
                    };
                    $('#mdlOperacion').modal('hide');
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        labelTipoOperacion,
        labelTipoProducto,
        numeroProducto,
        nombreReferente,
        monto,
        labelTipoRecepcion,
        labelAgencia,
        fechaHora,
        labelEstado
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configOperaciones.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configOperaciones.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblOperaciones.row.add([
                    nFila,
                    labelTipoOperacion,
                    labelTipoProducto,
                    numeroProducto,
                    nombreReferente,
                    "$" + new Intl.NumberFormat().format(parseFloat(monto).toFixed(2)),
                    labelTipoRecepcion,
                    labelAgencia,
                    fechaHora,
                    labelEstado,
                    botones
                ]).node().id = 'trOperacion' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id) {
        Swal.fire({
            title: `Eliminar operación`,
            icon: 'warning',
            html: `¿Seguro/a que quieres <b>eliminar</b> la operación?.`,
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblOperaciones.row('#trOperacion' + id).remove().draw();
                delete this.operaciones[id];
            }
        });
    },
    evalTipoProducto: function (tipo = '') {
        if (tipo.length > 0) {
            this.campoNumeroProducto.readOnly = false;
            if (tipo == 'AgregarProducto') {
                this.containerCboProducto.style.display = 'none';
                this.containerProducto.style.display = 'block';
                this.campoTipoProducto.required = true;
            }
        } else {
            this.campoNumeroProducto.value = '';
            this.campoNumeroProducto.readOnly = true;
            this.containerProducto.style.display = 'none';
            this.containerCboProducto.style.display = 'block';
            this.campoTipoProducto.required = false;
        }
    },
    evalTipoRelacion: function (tipo = '') {
        if (tipo == 'titular') {
            this.campoNombre.value = configROI.getNombreCompleto();
            this.campoNombre.readOnly = true;
        } else if (tipo.length > 0) {
            this.campoNombre.readOnly = false;
        } else {
            this.campoNombre.value = '';
        }
    }
}

const configDocumentos = {
    id: 0,
    cnt: 0,
    catalogo: {},
    documentos: {},
    datosOriginales: {},
    cboDocumento: document.getElementById('cboDocumentoAdjunto'),
    campoDocumento: document.getElementById('txtDocumentoAdjunto'),
    containerCbo: document.getElementById('documentoContainer'),
    containerCampo: document.getElementById('newDocumentoContainer'),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(documento => {
                    let tipoDocumento = '';
                    for (let key in this.catalogo) {
                        if (this.catalogo[key].id == documento.idTipoDocumento) {
                            tipoDocumento = this.catalogo[key].documento;
                        }
                    }
                    let tmp = {
                        id: documento.id,
                        idTipoDocumento: documento.idTipoDocumento,
                        tipoDocumento
                    }
                    this.cnt++;
                    this.documentos[this.cnt] = {
                        ...tmp
                    };
                    this.addRowTbl({
                        ...this.documentos[this.cnt],
                        nFila: this.cnt
                    });
                });
                tblDocumentos.columns.adjust().draw();
                this.datosOriginales = JSON.parse(JSON.stringify(configDocumentos.documentos));
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('frmDocumento').then(() => {
            this.id = id;

            this.configCatalogo.init().then(() => {
                if (this.id > 0) {
                    this.cboDocumento.value = this.documentos[this.id].id;
                    this.cboDocumento.className.includes('selectpicker') && ($('#' + this.cboDocumento.id).selectpicker('refresh'));
                }
                $("#mdlDocumento").modal("show");
            });
        });
    },
    save: function () {
        formActions.validate('frmDocumento').then(({
            errores
        }) => {
            let idDocumento = this.cboDocumento.value == 'AgregarDocumento' ? this.configCatalogo.add() : this.cboDocumento.value;
            if (errores == 0) {
                let tmp = {
                    id: this.id > 0 ? this.documentos[this.id].id : 0,
                    idTipoDocumento: idDocumento,
                    tipoDocumento: this.cboDocumento.value == 'AgregarDocumento' ? this.campoDocumento.value : this.cboDocumento.options[this.cboDocumento.options.selectedIndex].text
                };

                let guardar = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        this.cnt++;
                        this.id = this.cnt;
                        this.addRowTbl({
                            ...tmp,
                            nFila: this.cnt
                        }).then(resolve).catch(reject);
                    } else {
                        $('#trDocumento' + this.id).find('td:eq(1)').html(tmp.tipoDocumento);
                        resolve();
                    }
                });

                guardar.then(() => {
                    this.documentos[this.id] = {
                        ...tmp
                    };
                    tblDocumentos.columns.adjust().draw();
                    $('#mdlDocumento').modal('hide');
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        tipoDocumento
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configDocumentos.edit(" + nFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configDocumentos.delete(" + nFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                tipoPermiso != 'ESCRITURA' && (botones = '');
                tblDocumentos.row.add([
                    nFila,
                    tipoDocumento,
                    botones
                ]).node().id = 'trDocumento' + nFila;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function () {},
    configCatalogo: {
        cnt: 0,
        add: function () {
            configDocumentos.configCatalogo.cnt++;
            configDocumentos.catalogo[configDocumentos.configCatalogo.cnt] = {
                id: 'Pendiente' + configDocumentos.configCatalogo.cnt,
                documento: configDocumentos.campoDocumento.value
            }

            return configDocumentos.catalogo[configDocumentos.configCatalogo.cnt].id;
        },
        init: function () {
            return new Promise((resolve, reject) => {
                try {
                    let opciones = [`<option selected disabled value="">seleccione</option><option value="AgregarDocumento">Agregar nuevo</option><option disabled style="line-height:0.1;padding-top:0;">__________________________________</option>`];

                    for (let key in configDocumentos.catalogo) {
                        opciones.push(`<option value="${configDocumentos.catalogo[key].id}">${configDocumentos.catalogo[key].documento}</option>`);
                    }

                    configDocumentos.cboDocumento.innerHTML = opciones.join("");

                    configDocumentos.cboDocumento.className.includes("selectpicker") && ($("#" + configDocumentos.cboDocumento.id).selectpicker("refresh"));
                    resolve();
                } catch (err) {
                    reject(err.message);
                }
            });
        }
    },
    evalDocumento: function (opcion) {
        if (opcion == 'AgregarDocumento') {
            this.containerCbo.style.display = 'none';
            this.containerCampo.style.display = 'block';
            this.campoDocumento.required = true;
        } else {
            this.containerCampo.style.display = 'none';
            this.containerCbo.style.display = 'block';
            this.campoDocumento.required = false;
        }
    }
}