<?php
class estado
{
    public $id;
    public $estado;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function getEstados()
    {
        $query = 'SELECT * FROM oficialia_viewEstados;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }
}
