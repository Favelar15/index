<?php
class tipoOperacion
{
    public $id;
    public $tipoOperacion;
    public $fechaRegistro;
    public $usuario;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
    }

    public function getTiposOperacion()
    {
        $query = 'SELECT * FROM oficialia_viewTiposOperacion';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }
}
