<?php
class tipoDocumentoAdjunto
{
    public $id;
    public $documento;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function getTiposDocumentoCbo()
    {
        $query = 'SELECT id,documento from oficialia_viewTiposDocumentoAdjunto;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_OBJ);
        }
        $this->conexion = null;
        return [];
    }
}
