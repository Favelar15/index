<?php
class analista
{
    public $id;
    public $nombres;
    public $apellidos;
    public $nombreCompleto;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
        $this->nombreCompleto = $this->nombres . ' ' . $this->apellidos;
    }

    public function getAnalistasCbo()
    {
        $query = 'SELECT * FROM oficialia_viewAnalistas';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }
}
