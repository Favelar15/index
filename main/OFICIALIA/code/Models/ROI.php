<?php
class ROI
{
    public $id;
    public $datosROI;
    public $operaciones;
    public $documentosAdjuntos;
    private $conexion;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    public function saveROI(int $idUsuario, string $ipActual, string $tipoApartado, string $idApartado)
    {
        $arrErrores = [];
        $id = null;
        $permitirGuardar = true;
        $idsDocumentosAdjuntos = [];
        $idsOperaciones = [];

        $arrIdsNuevosTiposDocumentos = [];
        $arrIdsNuevosProductos = [];

        $sendEmails = $this->id == 0 ? true : false;

        $responseFormulario = null;
        if (isset($this->datosROI['logs'])) {
            $descripcionLog = $this->datosROI['logs']['descripcion'];
            $arrDetallesLogs = [];

            if (isset($this->datosROI['logs']['detalles'])) {
                foreach ($this->datosROI['logs']['detalles'] as $detalle) {
                    $arrDetallesLogs[] = $detalle['campo'] . '%%%' . $detalle['valorAnterior'] . '%%%' . $detalle['nuevoValor'];
                }
            }

            $detallesLogs = count($arrDetallesLogs) > 0 ? implode('@@@', $arrDetallesLogs) : null;
            $analisis = nl2br($this->datosROI['analisis']);
            $query = 'EXEC oficialia_spGuardarROI :id,:agencia,:colaborador,:area,:codigoJefe,:tipoDocumento,:numeroDocumento,:nombres,:primerApellido,:segundoApellido,:apellidoCasada,:profesion,:actividadEconomica,:pais,:departamento,:municipio,:direccion,:analisis,:usuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idROI;';
            $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $result->bindParam(':id', $this->id, PDO::PARAM_INT);
            $result->bindParam(':agencia', $this->datosROI['agencia'], PDO::PARAM_INT);
            $result->bindParam(':colaborador', $this->datosROI['colaborador'], PDO::PARAM_INT);
            $result->bindParam(':area', $this->datosROI['area'], PDO::PARAM_STR);
            $result->bindParam(':codigoJefe', $this->datosROI['codigoJefeInmediato'], PDO::PARAM_STR);
            $result->bindParam(':tipoDocumento', $this->datosROI['tipoDocumento'], PDO::PARAM_INT);
            $result->bindParam(':numeroDocumento', $this->datosROI['numeroDocumento'], PDO::PARAM_STR);
            $result->bindParam(':nombres', $this->datosROI['nombres'], PDO::PARAM_STR);
            $result->bindParam(':primerApellido', $this->datosROI['primerApellido'], PDO::PARAM_STR);
            $result->bindParam(':segundoApellido', $this->datosROI['segundoApellido'], PDO::PARAM_STR);
            $result->bindParam(':apellidoCasada', $this->datosROI['apellidoCasada'], PDO::PARAM_STR);
            $result->bindParam(':profesion', $this->datosROI['profesion'], PDO::PARAM_STR);
            $result->bindParam(':actividadEconomica', $this->datosROI['actividadEconomica'], PDO::PARAM_INT);
            $result->bindParam(':pais', $this->datosROI['pais'], PDO::PARAM_INT);
            $result->bindParam(':departamento', $this->datosROI['departamento'], PDO::PARAM_INT);
            $result->bindParam(':municipio', $this->datosROI['municipio'], PDO::PARAM_INT);
            $result->bindParam(':direccion', $this->datosROI['direccion'], PDO::PARAM_STR);
            $result->bindParam(':analisis', $analisis, PDO::PARAM_STR);
            $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
            $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
            $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
            $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
            $result->bindParam(':descripcionLog', $descripcionLog, PDO::PARAM_STR);
            $result->bindParam(':detallesLog', $detallesLogs, PDO::PARAM_STR);
            $result->bindParam(':response', $responseFormulario, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
            $result->bindParam(':idROI', $id, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

            if ($result->execute()) {
                $responseFormulario != 'EXITO' && $arrErrores["Formulario"] = $responseFormulario;
                $responseFormulario != 'EXITO' && $permitirGuardar = false;
                $this->id = $id;
            }
        }

        if ($permitirGuardar) {
            foreach ($this->documentosAdjuntos as $key => $adjunto) {
                if (isset($adjunto['idTipoDocumento'])) {
                    if (strpos($adjunto['idTipoDocumento'], 'Pendiente') !== false) {
                        $responseTipoDocumento = null;
                        $idTipoDocumento = null;
                        $query = 'EXEC oficialia_spGuardarTipoDocumento :tipoDocumento,:usuario,:response,:id;';
                        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $result->bindParam(':tipoDocumento', $adjunto['tipoDocumento'], PDO::PARAM_STR);
                        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_STR);
                        $result->bindParam(':response', $responseTipoDocumento, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                        $result->bindParam(':id', $idTipoDocumento, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                        if ($result->execute()) {
                            $arrIdsNuevosTiposDocumentos[$adjunto['idTipoDocumento']] = $idTipoDocumento;
                        }
                    }
                }
            }

            foreach ($this->documentosAdjuntos as $key => $adjunto) {
                if (isset($adjunto['logs'])) {
                    $responseAdjunto = null;
                    $idAdjunto = null;
                    $arrDetallesLogsAdjunto = [];

                    if (isset($adjunto['logs']['detalles'])) {
                        foreach ($adjunto['logs']['detalles'] as $detalle) {
                            $arrDetallesLogsAdjunto[] = $detalle['campo'] . '%%%' . $detalle['valorAnterior'] . '%%%' . $detalle['nuevoValor'];
                        }
                    }

                    if ($adjunto['logs']['accion'] == 'Guardar') {
                        $detallesLogsAdjunto = count($arrDetallesLogsAdjunto) > 0 ? implode('@@@', $arrDetallesLogsAdjunto) : null;
                        $idTipoDocumento = isset($arrIdsNuevosTiposDocumentos[$adjunto['idTipoDocumento']]) ? $arrIdsNuevosTiposDocumentos[$adjunto['idTipoDocumento']] : $adjunto['idTipoDocumento'];
                        $query = 'EXEC oficialia_spGuardarAdjuntoROI :idROI,:id,:idTipoDocumento,:idUsuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idAdjunto;';
                        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $result->bindParam(':idROI', $this->id, PDO::PARAM_INT);
                        $result->bindParam(':id', $adjunto['id'], PDO::PARAM_INT);
                        $result->bindParam(':idTipoDocumento', $idTipoDocumento, PDO::PARAM_INT);
                        $result->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
                        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                        $result->bindParam(':descripcionLog', $adjunto['logs']['descripcion'], PDO::PARAM_STR);
                        $result->bindParam(':detallesLog', $detallesLogsAdjunto, PDO::PARAM_STR);
                        $result->bindParam(':response', $responseAdjunto, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                        $result->bindParam(':idAdjunto', $idAdjunto, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                        if ($result->execute()) {
                            $responseAdjunto != 'EXITO' && $arrErrores['Adjunto' . $key] = $responseAdjunto;
                            !isset($idsDocumentosAdjuntos[$key]) && $idsDocumentosAdjuntos[$key] = (object)[];
                            $idsDocumentosAdjuntos[$key]->id = $idAdjunto;
                            $idsDocumentosAdjuntos[$key]->idTipoDocumento = $idTipoDocumento;
                        }
                    } elseif ($adjunto['logs']['accion'] == 'Eliminar') {
                        $query = 'EXEC oficialia_spEliminarAdjuntoROI :id,:descripcion,:usuario,:ip,:tipoApartado,:idApartado,:response;';
                        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $result->bindParam(':id', $adjunto['id'], PDO::PARAM_INT);
                        $result->bindParam(':descripcion', $adjunto['logs']['descripcion'], PDO::PARAM_STR);
                        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                        $result->bindParam(':response', $responseAdjunto, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                        if ($result->execute()) {
                            $responseAdjunto != 'EXITO' && $arrErrores['Adjunto' . $key] = $responseAdjunto;
                        }
                    }
                }
            }

            foreach ($this->operaciones as $key => $operacion) {
                if (isset($operacion['tipoProducto'])) {
                    if (strpos($operacion['tipoProducto'], 'Pendiente') !== false) {
                        $responseTipoProducto = null;
                        $idTipoProducto = null;
                        $query = 'EXEC oficialia_spGuardarTipoProducto :tipoProducto,:usuario,:response,:id;';
                        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $result->bindParam(':tipoProducto', $operacion['labelTipoProducto'], PDO::PARAM_STR);
                        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_STR);
                        $result->bindParam(':response', $responseTipoProducto, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                        $result->bindParam(':id', $idTipoProducto, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                        if ($result->execute()) {
                            $arrIdsNuevosProductos[$operacion['tipoProducto']] = $idTipoProducto;
                        }
                    }
                }
            }

            foreach ($this->operaciones as $key => $operacion) {
                if (isset($operacion['logs'])) {
                    $responseOperacion = null;
                    $idOperacion = null;
                    $arrDetallesLogsOperacion = [];

                    if (isset($operacion['logs']['detalles'])) {
                        foreach ($operacion['logs']['detalles'] as $detalle) {
                            $arrDetallesLogsOperacion[] = $detalle['campo'] . '%%%' . $detalle['valorAnterior'] . '%%%' . $detalle['nuevoValor'];
                        }
                    }

                    if ($operacion['logs']['accion'] == 'Guardar') {
                        $detallesLogsOperacion = count($arrDetallesLogsOperacion) > 0 ? implode('@@@', $arrDetallesLogsOperacion) : null;
                        $idTipoProducto = isset($arrIdsNuevosProductos[$operacion['tipoProducto']]) ? $arrIdsNuevosProductos[$operacion['tipoProducto']] : $operacion['tipoProducto'];
                        $fechaOperacion = date('Y-m-d H:i', strtotime($operacion['fechaHora']));
                        $query = 'EXEC oficialia_spGuardarOperacionROI :idROI,:id,:idTipoOperacion,:idTipoProducto,:numeroProducto,:tipoRelacion,:referente,:monto,:tipoRecepcion,:agencia,:area,:fecha,:estado,:idUsuario,:ip,:tipoApartado,:idApartado,:descripcionLog,:detallesLog,:response,:idAdjunto;';
                        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $result->bindParam(':idROI', $this->id, PDO::PARAM_INT);
                        $result->bindParam(':id', $operacion['id'], PDO::PARAM_INT);
                        $result->bindParam(':idTipoOperacion', $operacion['tipoOperacion'], PDO::PARAM_INT);
                        $result->bindParam(':idTipoProducto', $idTipoProducto, PDO::PARAM_INT);
                        $result->bindParam(':numeroProducto', $operacion['numeroProducto'], PDO::PARAM_INT);
                        $result->bindParam(':tipoRelacion', $operacion['relacion'], PDO::PARAM_INT);
                        $result->bindParam(':referente', $operacion['nombreReferente'], PDO::PARAM_INT);
                        $result->bindParam(':monto', $operacion['monto'], PDO::PARAM_INT);
                        $result->bindParam(':tipoRecepcion', $operacion['tipoRecepcion'], PDO::PARAM_INT);
                        $result->bindParam(':agencia', $operacion['agencia'], PDO::PARAM_INT);
                        $result->bindParam(':area', $operacion['area'], PDO::PARAM_INT);
                        $result->bindParam(':fecha', $fechaOperacion, PDO::PARAM_INT);
                        $result->bindParam(':estado', $operacion['estado'], PDO::PARAM_INT);
                        $result->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
                        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                        $result->bindParam(':descripcionLog', $adjunto['logs']['descripcion'], PDO::PARAM_STR);
                        $result->bindParam(':detallesLog', $detallesLogsOperacion, PDO::PARAM_STR);
                        $result->bindParam(':response', $responseOperacion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
                        $result->bindParam(':idAdjunto', $idOperacion, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

                        if ($result->execute()) {
                            $responseOperacion != 'EXITO' && $arrErrores['Operacion' . $key] = $responseOperacion;
                            !isset($idsOperaciones[$key]) && $idsOperaciones[$key] = (object)[];
                            $idsOperaciones[$key]->id = $idOperacion;
                            $idsOperaciones[$key]->idTipoProducto = $idTipoProducto;
                        }
                    } elseif ($operacion['logs']['accion'] == 'Eliminar') {
                        $query = 'EXEC oficialia_spEliminarOperacionROI :id,:descripcion,:usuario,:ip,:tipoApartado,:idApartado,:response;';
                        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                        $result->bindParam(':id', $operacion['id'], PDO::PARAM_INT);
                        $result->bindParam(':descripcion', $operacion['logs']['descripcion'], PDO::PARAM_STR);
                        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
                        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                        $result->bindParam(':response', $responseOperacion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                        if ($result->execute()) {
                            $responseOperacion != 'EXITO' && $arrErrores['Operacion' . $key] = $responseOperacion;
                        }
                    }
                }
            }
        }

        $respuesta = 'EXITO';
        (!empty($responseFormulario) && $responseFormulario != 'EXITO') && $respuesta = $responseFormulario;
        $nombreArchivo = $this->construirPDF();
        if ($sendEmails) {
            $emailParameters = (object)[];
            $emailParameters->correlativo = 'ROI' . $this->id;
            $emailParameters->colaborador = $this->datosROI['labelColaborador'];
            $emailParameters->area = $this->datosROI['area'];
            $arrOperaciones = [];
            foreach ($this->operaciones as $operacion) {
                $operacion['monto'] = '$' . number_format($operacion['monto'], 2, '.', ',');
                $arrOperaciones[] = $operacion;
            }
            $emailParameters->operaciones = $arrOperaciones;
            $email = new oficialiaEmail();
            $email->setParams($emailParameters);
            $oficialiaEmails = $email->sendOficialia();
            $localEmail = $email->sendLocal();
        }

        $this->conexion = null;
        return ["respuesta" => $respuesta, "response" => $responseFormulario, "id" => $this->id, "idsDocumentosAdjuntos" => $idsDocumentosAdjuntos, 'idsOperaciones' => $idsOperaciones, "nuevosTiposDocumento" => $arrIdsNuevosTiposDocumentos, 'nuevosTiposProductos' => $arrIdsNuevosProductos, 'nombreArchivo' => $nombreArchivo, "Errores" => $arrErrores];
    }

    public function construirPDF()
    {
        $nombreArchivo = 'ROI' . $this->id . '.pdf';
        $pdf = new construccionPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
        $pdf->configuracionDocumento();
        $pdf->setMargin();

        $pdf->SetAutoPageBreak(TRUE, 35);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->AddPage();

        $pdf->setGenerales();
        $pdf->setDatosPersonales($this->datosROI);
        $pdf->setOperaciones($this->operaciones);
        $pdf->setAnalisis($this->datosROI);
        $pdf->setDiligencia($this->datosROI, $this->documentosAdjuntos);
        $pdf->setFirmas();

        $pdf->Output(__DIR__ . '/../../docs/ROI/' . $nombreArchivo, 'F');
        return $nombreArchivo;
    }

    public function getROIAdministracion($tipoRol, $usuario, $agencia, $inicio, $fin)
    {
        $condicion = $tipoRol == 'Administrativo' ? '' : ' AND idUsuario=' . $usuario;
        $query = "SELECT * FROM oficialia_viewROIAdministracion WHERE CAST(fechaRegistro as date) BETWEEN '{$inicio}' and '{$fin}' AND agencia = " . $agencia . "" . $condicion . ";";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $this->conexion = null;
            $datos = $result->fetchAll(PDO::FETCH_OBJ);

            foreach ($datos as $key => $formulario) {
                $datos[$key]->fechaRegistro = date('d-m-Y h:i a', strtotime($formulario->fechaRegistro));
                // $datos[$key]->fechaRegistro = date('d-m-Y h:i a', strtotime($formulario->fechaRegistro));
                $datos[$key]->montoOperaciones = "$" . number_format($formulario->montoOperaciones, 2, '.', ',');
                $datos[$key]->cantidadOperaciones = (int)$formulario->cantidadOperaciones;
            }

            return ["respuesta" => "EXITO", "resultados" => $datos];
        }

        $this->conexion = null;
        return [];
    }

    public function eliminarROI($idUsuario, $ipActual, $tipoApartado, $idApartado)
    {
        $response = null;
        $query = 'EXEC oficialia_spEliminarROI :id,:usuario,:ip,:tipoApartado,:idApartado,:response;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return $response;
        }

        $this->conexion = null;
        return [];
    }

    public function validarPermisoEdicion($usuario, $tipoApartado, $idApartado)
    {
        $response = null;
        $tipoPermiso = null;
        $query = 'EXEC general_spValidarPermiso :usuario,:tipoApartado,:idApartado,:response,:tipoPermiso;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':usuario', $usuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':tipoPermiso', $tipoPermiso, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            if ($response == 'EXITO' && $tipoPermiso == 'ESCRITURA') {
                $fecha = date('Y-m-d');

                $query = 'SELECT top(1) * FROM oficialia_viewSolicitudesEdicionROI WHERE idROI = :id and idSolicitante = :usuario and cast(fechaRegistro as date) = :fecha ORDER BY fechaRegistro DESC;';
                $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $result->bindParam(':id', $this->id, PDO::PARAM_INT);
                $result->bindParam(':usuario', $usuario, PDO::PARAM_INT);
                $result->bindParam(':fecha', $fecha, PDO::PARAM_STR);

                if ($result->execute()) {
                    if ($result->rowCount() > 0) {
                        $data = $result->fetchAll(PDO::FETCH_ASSOC)[0];
                        $resolucion = empty($data['resolucion']) ? 'PENDIENTE' : $data['resolucion'];
                        return ['respuesta' => $resolucion, 'idSolicitud' => $data['id']];
                    } else {
                        return ['respuesta' => 'NoSolicitud'];
                    }
                }

                $this->conexion = null;
                return ['respuesta' => 'Error en verificación de permiso de edición.'];
            }

            $this->conexion = null;
            return ['respuesta' => 'PERMISO'];
        }

        $this->conexion = null;
        return ['respuesta' => 'Error en general_spValidarPermiso'];
    }

    public function crearSolicitudEdicion(int $usuario, string $ipActual, string $tipoApartado, int $idApartado, string $nombreCompleto, string $solicitante)
    {
        $response = null;
        $idSolicitud = null;

        $query = 'EXEC oficialia_spGuardarSolicitudEdicionROI :id,:usuario,:ip,:tipoApartado,:idApartado,:response,:idSolicitud;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':usuario', $usuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idSolicitud', $idSolicitud, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            if ($response = 'EXITO') {
                $emailParameters = (object)[];
                $emailParameters->solicitante = $solicitante;
                $emailParameters->nombreCompleto = $nombreCompleto;

                $email = new oficialiaEmail();
                $email->setParams($emailParameters);
                $oficialiaEmails = $email->sendSolicitudEdicion();
            }
            return ['respuesta' => $response, 'idSolicitud' => $this->id];
        }
        $this->conexion = null;
        return ['respuesta' => 'Error en oficialia_spGuardarSolicitudEdicionROI'];
    }

    public function getSolicitudesEdicion($agencias)
    {
        $fecha = date('Y-m-d');
        $query = 'SELECT * FROM oficialia_viewAdministracionEdicionROI WHERE agencia in (' . implode(',', $agencias) . ') and cast(fechaSolicitud as date) = :fecha';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':fecha', $fecha, PDO::PARAM_STR);

        if ($result->execute()) {
            $this->conexion = null;

            $datos = $result->fetchAll(PDO::FETCH_OBJ);

            foreach ($datos as $key => $formulario) {
                $datos[$key]->fechaRegistro = date('d-m-Y h:i a', strtotime($formulario->fechaRegistro));
                $datos[$key]->fechaSolicitud = date('d-m-Y h:i a', strtotime($formulario->fechaSolicitud));
                $datos[$key]->montoOperaciones = "$" . number_format($formulario->montoOperaciones, 2, '.', ',');
                $datos[$key]->cantidadOperaciones = (int)$formulario->cantidadOperaciones;
            }

            return $datos;
        }
        $this->conexion = null;
        return [];
    }

    public function guardarResolucionEdicion($idSolicitud, $resolucion, $idUsuario, $ipActual, $tipoApartado, $idApartado)
    {
        $response = null;
        $query = 'EXEC oficialia_spGuardarResolucionEdicionROI :id,:resolucion,:usuario,:ip,:tipoApartado,:idApartado,:response;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $idSolicitud, PDO::PARAM_INT);
        $result->bindParam(':resolucion', $resolucion, PDO::PARAM_STR);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_STR);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return ['respuesta' => $response];
        }
        $this->conexion = null;
        return ['respuesta' => 'Error en oficialia_spGuardarResolucionEdicionROI'];
    }

    public function getROIEdicion($idSolicitud, $idUsuario)
    {
        $fecha = date('Y-m-d');
        $query = 'SELECT top(1) * FROM oficialia_viewSolicitudesEdicionROI where id = :idSolicitud and idROI = :id and idSolicitante = :usuario and cast(fechaRegistro as date) = :fecha ORDER BY fechaRegistro DESC;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $idSolicitud, PDO::PARAM_INT);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':fecha', $fecha, PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() > 0) {
            $data = $result->fetchAll(PDO::FETCH_OBJ)[0];
            if ($data->resolucion == 'APROBADA') {
                return $this->getROI();
            } else {
                $this->conexion = null;
                return ['respuesta' => 'PENDIENTE'];
            }
        }

        $this->conexion = null;
        return ['respuesta' => 'NoSolicitud'];
    }

    public function getROI()
    {
        $datosROI = (object)[];
        $queryROI = 'SELECT top(1) * FROM oficialia_datosROIs where id = :id;';
        $resultROI = $this->conexion->prepare($queryROI, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultROI->bindParam(':id', $this->id, PDO::PARAM_INT);
        $resultROI->execute();

        $datosROI->datosROI = $resultROI->rowCount() > 0 ? $resultROI->fetchAll(PDO::FETCH_OBJ)[0] : (object)[];

        $queryDocumentos = 'SELECT * FROM oficialia_viewDocumentosAdjuntos where idROI = :id;';
        $resultDocumentos = $this->conexion->prepare($queryDocumentos, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultDocumentos->bindParam(':id', $this->id, PDO::PARAM_INT);
        $resultDocumentos->execute();

        $datosROI->documentosAdjuntos = $resultDocumentos->fetchAll(PDO::FETCH_OBJ);

        $queryOperaciones = 'SELECT * FROM oficialia_viewOperacionesROI where idROI = :id;';
        $resultOperaciones = $this->conexion->prepare($queryOperaciones, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultOperaciones->bindParam(':id', $this->id, PDO::PARAM_INT);
        $resultOperaciones->execute();
        $operaciones = $resultOperaciones->fetchAll(PDO::FETCH_OBJ);

        foreach ($operaciones as $key => $operacion) {
            $operaciones[$key]->fechaOperacion = date('d-m-Y H:i', strtotime($operacion->fechaOperacion));
            $operaciones[$key]->monto = number_format($operacion->monto, 2, '.', '');
        }

        $datosROI->operaciones = $operaciones;

        $this->conexion = null;
        return ['respuesta' => 'APROBADA', 'data' => $datosROI];
    }

    public function getROIGestion($estado, $agencia)
    {
        $condicion = $agencia == 0 ? '' : ' AND agencia = ' . $agencia;
        $query = 'SELECT * FROM oficialia_viewGestionROI where estado = :estado' . $condicion . ' ORDER BY fechaRegistro ASC;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':estado', $estado, PDO::PARAM_INT);

        if ($result->execute()) {
            $this->conexion = null;
            $data = $result->fetchAll(PDO::FETCH_OBJ);

            foreach ($data as $key => $roi) {
                $data[$key]->clase = 'none';
                $fechaLimite = strtotime(sumarDiasHabiles(date('Y-m-d', strtotime($roi->fechaRegistro)), 15));
                $fechaAdvertencia = strtotime(sumarDiasHabiles(date('Y-m-d', strtotime($roi->fechaRegistro)), 10));
                $fechaActual = strtotime(date('Y-m-d'));

                if ($fechaActual >= $fechaAdvertencia && $fechaActual < $fechaLimite) {
                    $data[$key]->clase = 'warning';
                } elseif ($fechaActual >= $fechaLimite) {
                    $data[$key]->clase = 'danger';
                }
                $data[$key]->fechaRegistro = date('d-m-Y h:i a', strtotime($roi->fechaRegistro));
                $data[$key]->montoOperaciones = '$' . number_format($roi->montoOperaciones, 2, '.', ',');
            }

            return ['respuesta' => 'EXITO', 'resultados' => $data];
        }
        $this->conexion = null;
        return [];
    }

    public function getROIAnalista(int $estado, int $analista)
    {
        $query = 'SELECT * FROM oficialia_viewGestionROI where estado = :estado and idAnalista = :analista ORDER BY fechaRegistro ASC;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':estado', $estado, PDO::PARAM_INT);
        $result->bindParam(':analista', $analista, PDO::PARAM_INT);

        if ($result->execute()) {
            $this->conexion = null;
            $data = $result->fetchAll(PDO::FETCH_OBJ);

            foreach ($data as $key => $roi) {
                $data[$key]->clase = 'none';
                $fechaLimite = strtotime(sumarDiasHabiles(date('Y-m-d', strtotime($roi->fechaRegistro)), 15));
                $fechaAdvertencia = strtotime(sumarDiasHabiles(date('Y-m-d', strtotime($roi->fechaRegistro)), 10));
                $fechaActual = strtotime(date('Y-m-d'));

                if ($fechaActual >= $fechaAdvertencia && $fechaActual < $fechaLimite) {
                    $data[$key]->clase = 'warning';
                } elseif ($fechaActual >= $fechaLimite) {
                    $data[$key]->clase = 'danger';
                }
                $data[$key]->fechaRegistro = date('d-m-Y h:i a', strtotime($roi->fechaRegistro));
                $data[$key]->montoOperaciones = '$' . number_format($roi->montoOperaciones, 2, '.', ',');
            }

            return ['respuesta' => 'EXITO', 'resultados' => $data];
        }
        $this->conexion = null;
        return [];
    }

    public function setAnalista(int $idUsuario, string $ipActual, string $tipoApartado, int $idApartado, int $analista)
    {
        $response = null;
        $query = 'EXEC oficialia_spGuardarAnalistaROI :id,:analista,:usuario,:ip,:tipoApartado,:idApartado,:response;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':analista', $analista, PDO::PARAM_INT);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return ['respuesta' => $response];
        }
        $this->conexion = null;
        return ['respuesta' => 'Pendiente el nombre del SP'];
    }
}

class construccionPDF extends TCPDF
{
    function configuracionDocumento()
    {
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('Departamento IT');
        $this->SetTitle('Formulario ROI');
        $this->SetSubject('ACACYPAC');
        $this->SetKeywords('TCPDF, PDF, Formulario ROI');

        $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    }

    function setMargin()
    {
        $this->SetMargins(22, 15, 22, 15);
        $this->SetHeaderMargin(10);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);
    }

    function Header()
    {
        $bMargin = $this->getBreakMargin();
        $auto_page_break = $this->AutoPageBreak;

        $this->SetAutoPageBreak(false, 0);


        $marcaAgua = '../../img/confidencial.png';
        // $this->Image($marcaAgua, 0, 0, 223, 280, '', '', '', false, 300, '', false, false, 0);
        $this->Image('@' . file_get_contents($marcaAgua), 0, 0, 220, 275, '', '', '', false, 300);
        $this->SetAutoPageBreak($auto_page_break, $bMargin);

        $this->setPageMark();
    }

    function Footer()
    {
        $this->SetY(-20);
        // Set font
        $this->SetFont('helvetica', 'B', 8);
        // Page number
        $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . ' de ' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

    public function setGenerales()
    {
        $this->setFont('dejavusans', '', 10);
        $tblHeader = '<style>table{font-size:9.5px;}</style><table cellspacing="0" cellpadding="0" border="0">
        <tr>
        <td style="width:570px; text-align:center; font-weight:bold;">Asociación Cooperativa de Ahorro, Crédito y Producción Agropecuaria Comunal de Nueva Concepción de Responsabilidad Limitada.  ACACYPAC NC de R.L. <br /> Oficialía de Cumplimiento
        </td>
        <td style="width:40px;"><img src="../../img/logoAcacypac.png" /></td>
        </tr>
        <tr>
        <td style="width:610px; text-align:center; border:1px solid black;"><span style="font-weight:bold; font-size:13px;">F-ROI 01 Formulario para Reporte de Operaciones Irregulares</span><br />Formulario para el reporte de operaciones únicas o múltiples, consideradas inusuales o irregulares; en cumplimiento a las obligaciones establecidas en la Ley Contra el Lavado de Dinero y de Activos.
        </td>
        </tr>
        </table>';
        $this->writeHTML($tblHeader, true, false, true, false, '');
    }

    public function setDatosPersonales($datosROI)
    {
        $segundoApellido = empty($datosROI['apellidoCasada']) ? $datosROI['segundoApellido'] : $datosROI['apellidoCasada'];
        $this->setFont('helvetica', '', 8);
        $this->ln(2);

        $this->SetFillColor(0, 95, 85);
        $this->SetTextColor(255);

        $this->SetDrawColor(0, 95, 85);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B', 7);

        $this->Cell(172.2, 5, 'Datos personales de quien realiza la operación físicamente', 1, 0, 'C', 1);
        $this->ln();
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0);

        $this->SetFillColor(224, 235, 255);

        $this->SetFont('dejavusans', '', 6);
        $this->Cell(40, 5, $datosROI['primerApellido'], 'LR', 0, 'C', 0);
        $this->Cell(50, 5, $segundoApellido, 'LR', 0, 'C', 0);
        $this->Cell(82.2, 5, $datosROI['nombres'], 'LR', 0, 'C', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 6);
        $this->Cell(40, 5, 'Primer apellido', 'LRB', 0, 'C', 1);
        $this->Cell(50, 5, 'Segundo apellido / Apellido de casada', 'LRB', 0, 'C', 1);
        $this->Cell(82.2, 5, 'Nombres', 'LRB', 0, 'C', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 6);
        $this->Cell(30, 5, $datosROI['labelTipoDocumento'] . ':', 'LRB', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 6);
        $this->Cell(35, 5, $datosROI['numeroDocumento'], 'LRB', 0, 'L', 0);
        $this->SetFont('dejavusans', 'B', 6);
        $this->Cell(30, 5, 'Profesión u oficio:', 'LRB', 0, 'R', 1);
        $this->SetFont('dejavusans', '', 6);
        $this->Cell(77.2, 5, $datosROI['profesion'], 'LRB', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 6);
        $this->Cell(30, 5, 'Actividad económica:', 'LRB', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 6);
        $this->Cell(50, 5, $datosROI['labelActividadEconomica'], 'LRB', 0, 'L', 0);
        $this->SetFont('dejavusans', 'B', 6);
        $this->Cell(15, 5, 'Dirección:', 'LRB', 0, 'R', 1);
        $this->SetFont('dejavusans', '', 6);
        $this->Cell(77.2, 5, $datosROI['direccion'], 'LRB', 0, 'L', 0);
        $this->ln();
    }

    public function setOperaciones($operaciones = [])
    {
        $this->ln(5);
        $this->SetFillColor(0, 95, 85);
        $this->SetTextColor(255);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(172.2, 5, 'Detalle de la operación u operaciones objeto de reporte', 1, 0, 'C', 1);
        foreach ($operaciones as $key => $operacion) {
            $romano = aRomano($key);
            $this->ln();
            $this->SetFillColor(224, 235, 255);
            $this->SetTextColor(0);

            $this->SetFont('dejavusans', 'B', 5.2);
            $this->Cell(5, 5, $romano . '.', 'LR', 0, 'R', 1);
            // $this->MultiCell(5, 20, 'I. ', 1, 'L', 1, 0, '', '', true);
            $this->Cell(25, 5, 'Tipo de operación:', 'LRB', 0, 'L', 1);
            $this->SetFont('dejavusans', '', 5.2);
            $this->Cell(35, 5, $operacion['labelTipoOperacion'], 'LRB', 0, 'L', 0);
            $this->SetFont('dejavusans', 'B', 5.2);
            $this->Cell(25, 5, 'Producto/Servicio:', 'LRB', 0, 'R', 1);
            $this->SetFont('dejavusans', '', 5.2);
            $this->Cell(30, 5, $operacion['labelTipoProducto'], 'LRB', 0, 'L', 0);
            $this->SetFont('dejavusans', 'B', 5.2);
            $this->Cell(25, 5, 'Número/Referencia:', 'LRB', 0, 'R', 1);
            $this->SetFont('dejavusans', '', 5.2);
            $this->Cell(27.2, 5, $operacion['numeroProducto'], 'LRB', 0, 'L', 0);
            $this->ln();

            $this->SetFont('dejavusans', 'B', 5.2);
            $this->Cell(5, 5, '', 'LR', 0, 'R', 1);
            $this->Cell(35, 5, 'Titular/Beneficiario/Remitente:', 'LRB', 0, 'L', 1);
            $this->SetFont('dejavusans', '', 5.2);
            $this->Cell(80, 5, $operacion['nombreReferente'], 'LRB', 0, 'L', 0);
            $this->SetFont('dejavusans', 'B', 5.2);
            $this->Cell(25, 5, 'Tipo de recepción:', 'LRB', 0, 'R', 1);
            $this->SetFont('dejavusans', '', 5.2);
            $this->Cell(27.2, 5, $operacion['labelTipoRecepcion'], 'LRB', 0, 'L', 0);
            $this->ln();

            $this->SetFont('dejavusans', 'B', 5.2);
            $this->Cell(5, 5, '', 'LR', 0, 'R', 1);
            $this->Cell(35, 5, 'Agencia:', 'LRB', 0, 'L', 1);
            $this->SetFont('dejavusans', '', 5.2);
            $this->Cell(45, 5, $operacion['labelAgencia'], 'LRB', 0, 'L', 0);
            $this->SetFont('dejavusans', 'B', 5.2);
            $this->Cell(35, 5, 'Área en la que se efectuó:', 'LRB', 0, 'R', 1);
            $this->SetFont('dejavusans', '', 5.2);
            $this->Cell(52.2, 5, $operacion['labelArea'], 'LRB', 0, 'L', 0);
            $this->ln();

            $this->SetFont('dejavusans', 'B', 5.2);
            $this->Cell(5, 5, '', 'LRB', 0, 'R', 1);
            $this->Cell(13, 5, 'Monto:', 'LRB', 0, 'L', 1);
            $this->SetFont('dejavusans', '', 5.2);
            $this->Cell(17, 5, '$' . number_format($operacion['monto'], 2, '.', ','), 'LRB', 0, 'L', 0);
            $this->SetFont('dejavusans', 'B', 5.2);
            $this->Cell(25, 5, 'Fecha de la operación:', 'LRB', 0, 'R', 1);
            $this->SetFont('dejavusans', '', 5.2);
            $this->Cell(28, 5, date('d-m-Y h:i a', strtotime($operacion['fechaHora'])), 'LRB', 0, 'L', 0);
            $this->SetFont('dejavusans', 'B', 5.2);
            $this->Cell(32, 5, 'Estado de la operación:', 'LRB', 0, 'R', 1);
            $this->SetFont('dejavusans', '', 5.2);
            $this->Cell(52.2, 5, $operacion['labelEstado'], 'LRB', 0, 'L', 0);
        }
        $this->ln(5);
    }

    public function setAnalisis($analisis)
    {
        $this->setFont('helvetica', '', 8);
        $this->ln();

        $this->SetFillColor(0, 95, 85);
        $this->SetTextColor(255);

        $this->SetDrawColor(0, 95, 85);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B', 7);

        $this->Cell(172.2, 5, 'Análisis de la operación u operaciones reportadas (Antecedentes)', 1, 0, 'C', 1);
        $this->ln();
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0);

        $this->SetFillColor(224, 235, 255);
        $this->SetFont('dejavusans', '', 7);

        $this->setCellHeightRatio(1.5);
        $this->MultiCell(172.2, 20, $analisis['analisis'], 1, 'L', 0, 0, '', '', true);
        // $this->setCellHeightRatio(1.25);
        $this->ln();
    }

    public function setDiligencia($datosDiligencia, $documentosAdjuntos)
    {
        $arrDocumentos = [];

        foreach ($documentosAdjuntos as $key => $documento) {
            $arrDocumentos[] = '<li>' . $documento['tipoDocumento'] . '</li>';
        }

        $htmlAdjuntos = count($arrDocumentos) > 0 ? '<ul>' . implode('', $arrDocumentos) . '</ul>' : '';

        $this->SetMargins(21.8, 15, 22, 15);
        $this->setCellHeightRatio(1.25);
        $this->ln(5);
        $this->setFont('helvetica', '', 8);

        $this->SetFillColor(0, 95, 85);
        $this->SetTextColor(255);

        $this->SetDrawColor(255, 255, 255);
        $this->SetLineWidth(0.3);

        $this->Cell(86.2, 5, 'Documentos anexos al reporte', 'R', 0, 'C', 1);
        $this->Cell(86.1, 5, 'Datos sobre el diligenciamiento del reporte', 'L', 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 6);
        $this->SetDrawColor(0, 95, 85);
        $this->SetMargins(22, 15, 22, 15);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->MultiCell(86, 20, $htmlAdjuntos, 1, 'L', 0, 0, '', '', true, 0, true);
        $this->Cell(37, 5, 'Agencia:', 'LRB', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 6);
        $this->Cell(49, 5, $datosDiligencia['labelAgencia'], 'LRB', 0, 'L', 0);
        $this->SetMargins(108, 15, 22, 15);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 6);
        $this->Cell(37, 5, 'Área que reporta:', 'LRB', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 6);
        $this->Cell(49, 5, $datosDiligencia['area'], 'LRB', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 6);
        $this->Cell(37, 5, 'Código de colaborador:', 'LRB', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 6);
        $this->Cell(49, 5, $datosDiligencia['codigoColaborador'], 'LRB', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 6);
        $this->Cell(37, 5, 'Código Jefe inmediato:', 'LRB', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 6);
        $this->Cell(49, 5, $datosDiligencia['codigoJefeInmediato'], 'LRB', 0, 'L', 0);
        $this->SetMargins(22, 15, 22, 15);
        $this->ln();
    }

    public function setFirmas()
    {
        $this->ln(20);

        $this->setFont('dejavusans', 'B', 9);
        $this->Cell(66, 6, '______________________________________', 0, 0, 'C', 0);
        $this->Cell(40, 6, '', 0, 0, 'C', 0);
        $this->Cell(66.2, 6, '______________________________________', 0, 0, 'C', 0);
        $this->ln();

        $this->setFont('dejavusans', 'B', 8);
        $this->Cell(66, 6, 'Firma colaborador', false, 0, 'C', 0);
        $this->Cell(40, 6, 'Sello', false, 0, 'C', 0);
        $this->Cell(66.2, 6, 'Firma Jefe inmediato', false, 0, 'C', 0);
        $this->ln();
    }
}

class oficialiaEmail
{
    private $apiInstance;
    private $params;
    private $config;

    function __construct()
    {
        $this->config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');
        $this->apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(
            new GuzzleHttp\Client(),
            $this->config
        );
    }

    public function setParams($params)
    {
        $this->params = $params;
    }

    public function sendOficialia()
    {
        // echo json_encode($this->params);
        // $receptores = [['email' => 'gerencia.cumplimiento@acacypac.coop', 'name' => 'Lcda. Sindy Rivas'], ['email' => 'yesenia.lopez@acacypac.coop', 'name' => 'Lcda. Yesenia López']];
        $receptores = [['email' => 'francisco.avelar@acacypac.coop', 'name' => 'Francisco Avelar']];
        $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
        $sendSmtpEmail['to'] = $receptores;
        $sendSmtpEmail['cc'] = [['email' => 'informatica@acacypac.coop', 'name' => 'Ing. Erick Méndez']];
        $sendSmtpEmail['templateId'] = 18;
        $sendSmtpEmail['params'] = $this->params;
        $sendSmtpEmail['headers'] = array('X-Mailin-custom' => 'Departamento de IT - ACACYPAC');

        try {
            $result = $this->apiInstance->sendTransacEmail($sendSmtpEmail);
            return  print_r($result, true);
        } catch (Exception $e) {
            return 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ' . $e->getMessage();
        }
    }

    public function sendLocal()
    {
        require_once __DIR__ . '/../../../code/Models/agencia.php';
        $agenciaActual = new agencia();
        // $agenciaActual->id = $_SESSION['index']->agenciaActual->id;
        $agenciaActual->id = 701;
        $jefes = $agenciaActual->getJDAData();
        $receptores = [];
        if (count($jefes) > 0) {
            foreach ($jefes as $jda) {
                $receptores[] = ['email' => $jda->email, 'name' => ($jda->nombres . ' ' . $jda->apellidos)];
            }

            $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
            $sendSmtpEmail['to'] = $receptores;
            $sendSmtpEmail['templateId'] = 19;
            $sendSmtpEmail['params'] = $this->params;
            $sendSmtpEmail['headers'] = array('X-Mailin-custom' => 'Departamento de IT - ACACYPAC');

            try {
                $result = $this->apiInstance->sendTransacEmail($sendSmtpEmail);
                return  print_r($result, true);
            } catch (Exception $e) {
                return 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ' . $e->getMessage();
            }
        }
    }

    public function sendSolicitudEdicion()
    {
        require_once __DIR__ . '/../../../code/Models/agencia.php';
        $agenciaActual = new agencia();
        // $agenciaActual->id = $_SESSION['index']->agenciaActual->id;
        $agenciaActual->id = 701;
        $jefes = $agenciaActual->getJDAData();
        $receptores = [];
        if (count($jefes) > 0) {
            foreach ($jefes as $jda) {
                $receptores[] = ['email' => $jda->email, 'name' => ($jda->nombres . ' ' . $jda->apellidos)];
            }

            $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
            $sendSmtpEmail['to'] = $receptores;
            $sendSmtpEmail['templateId'] = 20;
            $sendSmtpEmail['params'] = $this->params;
            $sendSmtpEmail['headers'] = array('X-Mailin-custom' => 'Departamento de IT - ACACYPAC');

            try {
                $result = $this->apiInstance->sendTransacEmail($sendSmtpEmail);
                return  print_r($result, true);
            } catch (Exception $e) {
                return 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ' . $e->getMessage();
            }
        }
    }
}
