<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";
        require_once './Models/ROI.php';
        $ROI = new ROI();
        $agencias = [];

        foreach ($_SESSION['index']->agencias as $agencia) {
            $agencias[] = $agencia->id;
        }

        $respuesta->solicitudes = $ROI->getSolicitudesEdicion($agencias);

        $conexion = null;
    }
} else {
    $respuesta->respuesta = 'SESION';
}

echo json_encode($respuesta);
