<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    if (count($input) > 0) {
        include '../../code/connectionSqlServer.php';

        $idUsuario = $_SESSION['index']->id;
        $tipoApartado = $input['tipoApartado'];
        $idApartado = base64_decode(urldecode($input['idApartado']));
        require_once '../../RRHH/code/Models/colaborador.php';

        $colaborador = new colaborador();
        $colaborador->id = $input['id'];
        $colaborador->codigoOficialia = $input['codigoOficialia'];
        
        $actualizar = $colaborador->actualizarCodigoOficialia($idUsuario, $tipoApartado, $idApartado);

        if (isset($actualizar['respuesta'])) {
            $respuesta->respuesta = $actualizar['respuesta'];
        } else {
            $respuesta->respuesta = 'Error al conectar con la base de datos';
        }

        $conexion = null;
    }
} else {
    $respuesta->respuesta = 'SESION';
}

echo json_encode($respuesta);
