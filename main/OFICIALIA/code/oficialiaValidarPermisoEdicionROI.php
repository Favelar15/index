<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET['id']) && isset($_GET['tipoApartado']) && isset($_GET["idApartado"])) {
        require_once '../../code/connectionSqlServer.php';
        require_once './Models/ROI.php';

        $idUsuario = $_SESSION['index']->id;
        $tipoApartado = $_GET['tipoApartado'];
        $idApartado = base64_decode(urldecode($_GET['idApartado']));

        $ROI = new ROI();
        $ROI->id = base64_decode(urldecode($_GET['id']));

        $respuesta = $ROI->validarPermisoEdicion($idUsuario, $tipoApartado, $idApartado);

        $conexion = null;
    }
} else {
    $respuesta->respuesta = 'SESION';
}

echo json_encode($respuesta);
