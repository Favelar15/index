<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";

        $solicitados = explode("@@@", $_GET["q"]);

        if (in_array('tiposDocumento', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/tipoDocumento.php';
            $tipoDocumento = new tipoDocumento();
            $tiposDocumento = $tipoDocumento->obtenerTiposDocumento();

            $respuesta->{"tiposDocumento"} = $tiposDocumento;
        }

        if (in_array('agencias', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/agencia.php';
            $agencia = new agencia();
            $agencias = $agencia->getAgenciasCbo($_SESSION["index"]->agencias);

            $respuesta->{"agencias"} = $agencias;
        }

        if (in_array('geograficos', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/datosGeograficos.php';
            $geografico = new geografico();
            $datosGeograficos = $geografico->obtenerGeograficos();

            $respuesta->{"datosGeograficos"} = $datosGeograficos;
        }

        if (in_array('actividadEconomica', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/actividadEconomica.php';
            $actividadEconomica = new actividadEconomica();
            $actividadesEconomicas = $actividadEconomica->obtenerActividades();

            $respuesta->{"actividadesEconomicas"} = $actividadesEconomicas;
        }

        if (in_array('tiposOperacion', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/tipoOperacion.php';
            $tipoOperacion = new tipoOperacion();
            $tiposOperacion = $tipoOperacion->getTiposOperacion();

            $respuesta->{"tiposOperacion"} = $tiposOperacion;
        }

        if (in_array('productosServicios', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/productoServicio.php';
            $productoServicio = new productoServicio();
            $productosServicios = $productoServicio->getProductosServicios();

            $respuesta->{"productosServicios"} = $productosServicios;
        }

        if (in_array('areas', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/area.php';
            $area = new area();
            $areas = $area->getAreas();

            $respuesta->{"areas"} = $areas;
        }

        if (in_array('colaboradores', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../RRHH/code/Models/colaborador.php';
            $colaborador = new colaborador();
            $colaboradores = $colaborador->getColaboradoresCbo();

            $respuesta->{"colaboradores"} = $colaboradores;
        }

        if (in_array('tiposDocumentoAdjunto', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/tipoDocumentoAdjunto.php';
            $tipoDocumentoAdjunto = new tipoDocumentoAdjunto();
            $tiposDocumentoAdjunto = $tipoDocumentoAdjunto->getTiposDocumentoCbo();

            $respuesta->{"tiposDocumentoAdjunto"} = $tiposDocumentoAdjunto;
        }

        if (in_array('estados', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/estado.php';
            $estado = new estado();
            $estados = $estado->getEstados();

            $respuesta->{"estados"} = $estados;
        }

        if (in_array('analistas', $solicitados) || in_array('all', $solicitados)) {
            require_once './Models/analista.php';
            $analista = new analista();
            $analistas = $analista->getAnalistasCbo();

            $respuesta->{"analistas"} = $analistas;
        }

        $conexion = null;
    }
}

echo json_encode($respuesta);
