<?php
require_once "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        include '../../code/connectionSqlServer.php';
        require_once './Models/ROI.php';

        $idUsuario = $_SESSION['index']->id;
        $tipoApartado = $input['tipoApartado'];
        $idApartado = base64_decode(urldecode($input['idApartado']));
        $nombreCompleto = $input['nombreCompleto'];
        $solicitante = $_SESSION['index']->nombres . ' ' . $_SESSION['index']->apellidos;

        $ROI = new ROI();
        $ROI->id = base64_decode(urldecode($input['id']));

        $respuesta = $ROI->crearSolicitudEdicion($idUsuario, $ipActual, $tipoApartado, $idApartado, $nombreCompleto, $solicitante);
    }
} else {
    $respuesta->respuesta = 'SESION';
}

echo json_encode($respuesta);
