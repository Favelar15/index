<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["auth"]) && isset($_GET['id'])) {
        require_once '../../code/connectionSqlServer.php';
        require_once './Models/ROI.php';

        $idUsuario = $_SESSION['index']->id;
        $ROI = new ROI();
        $ROI->id = base64_decode(urldecode($_GET['id']));
        $idSolicitud = base64_decode(urldecode($_GET['auth']));

        $respuesta = $ROI->getROIEdicion($idSolicitud, $idUsuario);

        $conexion = null;
    }
} else {
    $respuesta->respuesta = 'SESION';
}

echo json_encode($respuesta);
