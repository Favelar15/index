<?php
require_once "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        require_once '../../code/connectionSqlServer.php';
        require_once './Models/ROI.php';

        $idUsuario = $_SESSION['index']->id;
        $tipoApartado = $input['tipoApartado'];
        $idApartado = base64_decode(urldecode($input['idApartado']));

        $idSolicitud = $input['id'];
        $resolucion = $input['resolucion'];

        $ROI = new ROI();

        $respuesta = $ROI->guardarResolucionEdicion($idSolicitud,$resolucion,$idUsuario,$ipActual,$tipoApartado,$idApartado);

        $conexion = null;
    }
} else {
    $respuesta->respuesta = 'SESION';
}

echo json_encode($respuesta);
