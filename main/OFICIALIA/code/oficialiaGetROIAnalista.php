<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET)) {
        include '../../code/connectionSqlServer.php';
        require_once './Models/ROI.php';

        $idUsuario = $_SESSION['index']->id;
        $estado = 1;

        $ROI = new ROI();

        $formularios = $ROI->getROIAnalista($estado, $idUsuario);

        if (isset($formularios["respuesta"])) {
            $respuesta->{"respuesta"} = $formularios["respuesta"];
            isset($formularios["resultados"]) && $respuesta->{"resultados"} = $formularios["resultados"];
        } else {
            $respuesta->{"respuesta"} = "Error al conectar con la base de datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = 'SESION';
}

echo json_encode($respuesta);
