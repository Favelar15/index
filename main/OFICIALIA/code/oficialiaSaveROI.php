<?php
require_once "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
$respuesta = (object)[];
session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (count($input)) {
        require_once '../../code/connectionSqlServer.php';
        require_once './Models/ROI.php';

        $idUsuario = $_SESSION['index']->id;
        $tipoApartado = $input['tipoApartado'];
        $idApartado = base64_decode(urldecode($input['idApartado']));

        $ROI = new ROI();

        $ROI->id = $input['datosROI']['id'];
        $ROI->datosROI = $input['datosROI'];
        $ROI->operaciones = $input['operaciones'];
        $ROI->documentosAdjuntos = $input['documentosAdjuntos'];

        $guardado = $ROI->saveROI($idUsuario, $ipActual, $tipoApartado, $idApartado);
        $respuesta = $guardado;

        $conexion = null;
    }
} else {
    $respuesta->{'respuesta'} = 'SESION';
}

echo json_encode($respuesta);
