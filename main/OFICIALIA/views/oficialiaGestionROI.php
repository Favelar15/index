<div class="pagetitle">
    <h1>Gestión de formularios ROI's</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">OFICIALIA</li>
            <li class="breadcrumb-item">Gestión</li>
            <li class="breadcrumb-item active">Formularios ROI</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <form id="frmBusqueda" name="frmBusqueda" accept-charset="utf-8" method="POST" action="javascript:configBusqueda.search();" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label for="cboEstado" class="form-label">Estado de formulario <span class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select id="cboEstado" name="cboEstado" class="form-control selectpicker cboEstado" title="" required data-live-search="true">
                                <option value="1">En gestión</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="cboAgencia" class="form-label">Agencia <span class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select id="cboAgencia" name="cboAgencia" class="form-control selectpicker cboAgencia" title="" required data-live-search="true">
                                <option value="0">Todas las agencias</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <div class="d-grid gap-2">
                            <button type="submit" class="btn btn-primary" style="margin-top: 28.5px;">
                                <i class="fas fa-search"></i> Buscar
                            </button>
                        </div>
                    </div>
                </div>
                <hr class="mb-2 mt-3">
            </form>
        </div>
        <div class="col-lg-12 col-xl-12">
            <table id="tblFormularios" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Correlativo</th>
                        <th>Documento</th>
                        <th>Nombre</th>
                        <th>Operaciones</th>
                        <th>Monto</th>
                        <th>Agencia</th>
                        <th>Colaborador</th>
                        <th>Fecha de registro</th>
                        <th>Usuario</th>
                        <th>Analista</th>
                        <th>Ubicación</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
<div class="modal fade" id="mdlGeneracion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Impresión de documentos</h5>
            </div>
            <div class="modal-body">
                <div class="row pt-2">
                    <div class="col-lg-2 col-xl-2">
                    </div>
                    <div class="col-lg-8 col-xl-8">
                        <div class="d-grid">
                            <button type="button" class="btn btn-success" title="Formulario ROI 1" onclick="configBusqueda.printFormulario();">
                                <i class="fas fa-spell-check"> Formulario ROI 01</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlAsignacion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Asignación de analista - Correlativo: <strong></strong></h5>
            </div>
            <div class="modal-body">
                <form id="frmAsignacion" name="frmAsignacion" class="needs-validation" novalidate method="POST" accept-charset="utf-8" action="javascript:configBusqueda.saveAsignacion();">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for="cboAnalista" class="form-label">Analista <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboAnalista" name="cboAnalista" title="" class="selectpicker form-control cboAnalista" required data-live-search="true">
                                    <option selected disabled value="">seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmAsignacion" class="btn btn-sm btn-primary">Guardar</button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<?php
$_GET['js'] = ['oficialiaGestionROI'];
