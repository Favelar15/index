<div class="pagetitle">
    <h1>Solicitudes de edición - Formulario ROI</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">OFICIALIA</li>
            <li class="breadcrumb-item">Administración</li>
            <li class="breadcrumb-item active">Solicitudes de edición ROI</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <table id="tblFormularios" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Documento</th>
                        <th>Nombre completo</th>
                        <th>Cantidad de operaciones</th>
                        <th>Monto total de operaciones</th>
                        <th>Agencia</th>
                        <th>Fecha de registro</th>
                        <th>Creador</th>
                        <th>Solicitante</th>
                        <th>Fecha de solicitud</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php
// echo json_encode($_SESSION['index']);
$_GET['js'] = ['oficialiaAdministrarEdicionROI'];
