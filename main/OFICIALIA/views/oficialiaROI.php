<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1 style="font-size:20px;">F-ROI 01 Formulario para Reporte de Operaciones Irregulares</h1>
        </div>
        <div class="col-4 topBtnContainer">
            <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configOID.cancel();">
                <i class="fas fa-times"></i> <span>Cancelar</span>
            </button>
            <button class="btn btn-sm btn-outline-success float-end" title="Guardar" type="button" onclick="configROI.save();">
                <i class="fas fa-save"></i> <span>Generar documento</span>
            </button>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">OFICIALÍA</li>
            <li class="breadcrumb-item">Generación</li>
            <li class="breadcrumb-item active">ROI 01</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <form id="frmPrincipal" name="frmPrincipal" accept-charset="utf-8" method="post" action="javascript:configROI.save();" novalidate class="needs-validation">
        <fieldset class="border p-1" style="padding-top: 0;">
            <legend class="w-auto float-none mb-0 pb-0">Datos personales</legend>
            <div class="row">
                <div class="col-lg-3 col-xl-3">
                    <label for="cboTipoDocumento" class="form-label">Tipo de documento <span class="requerido">*</span></label>
                    <select id="cboTipoDocumento" name="cboTipoDocumento" class="form-select cboTipoDocumento" title="Tipo de documento" required onchange="generalMonitoreoTipoDocumento(this.value,'txtNumeroDocumento');">
                        <option selected disabled value="">seleccione</option>
                    </select>
                    <div class="invalid-feedback">
                        Seleccione un tipo de documento
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="txtNumeroDocumento" class="form-label">Número de documento <span class="requerido">*</span></label>
                    <input type="text" class="form-control" required id="txtNumeroDocumento" name="txtNumeroDocumento" readonly>
                    <div class="invalid-feedback">
                        Ingrese el número de documento
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtNombres" class="form-label">Nombres <span class="requerido">*</span></label>
                    <input type="text" id="txtNombres" name="txtNombres" class="form-control" required placeholder="Ingrese los nombres">
                    <div class="invalid-feedback">
                        Ingrese los nombres
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtPrimerApellido" class="form-label">Primer apellido <span class="requerido">*</span></label>
                    <input type="text" id="txtPrimerApellido" name="txtPrimerApellido" class="form-control" required placeholder="Ingrese el primer apellido">
                    <div class="invalid-feedback">
                        Ingrese el primer apellido
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtSegundoApellido" class="form-label">Segundo apellido</label>
                    <input type="text" id="txtSegundoApellido" name="txtSegundoApellido" class="form-control" placeholder="Ingrese el segundo apellido">
                    <div class="invalid-feedback">
                        Ingrese el segundo apellido
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtApellidoCasada" class="form-label">Apellido de casada</label>
                    <input type="text" id="txtApellidoCasada" name="txtApellidoCasada" class="form-control" placeholder="Ingrese el apellido de casada">
                    <div class="invalid-feedback">
                        Ingrese el apellido de casada
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtProfesion" class="form-label">Profesión <span class="requerido">*</span></label>
                    <input type="text" id="txtProfesion" name="txtProfesion" class="form-control" placeholder="Ingrese la profesión" required>
                    <div class="invalid-feedback">
                        Ingrese la profesión
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="cboActividadEconomica" class="form-label">Actividad económica <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select id="cboActividadEconomica" name="cboActividadEconomica" class="form-control selectpicker cboActividadEconomica" data-live-search="true" required title="">
                            <option selected disabled value="">seleccione</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="cboPais" class="form-label">País de residencia <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select id="cboPais" name="cboPais" class="form-control selectpicker cboPais" data-live-search="true" required title="" onchange="generalMonitoreoPais(this.value,'cboDepartamento');">
                            <option selected disabled value="">seleccione</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="cboDepartamento" class="form-label">Departamento de residencia <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select id="cboDepartamento" name="cboDepartamento" class="form-control selectpicker" data-live-search="true" required title="" onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');">
                            <option selected disabled value="">seleccione un país</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="cboMunicipio" class="form-label">Municipio de residencia <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select id="cboMunicipio" name="cboMunicipio" class="form-control selectpicker" data-live-search="true" required title="">
                            <option selected disabled value="">seleccione un departamento</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtDireccion" class="form-label">Dirección <span class="requerido">*</span></label>
                    <input type="text" id="txtDireccion" name="txtDireccion" class="form-control" required placeholder="Ingrese la dirección">
                    <div class="invalid-feedback">
                        Ingrese la dirección
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
    <fieldset class="border p-1" style="padding-top: 0;">
        <legend class="w-auto float-none mb-0 pb-0">Detalle de las operaciones objeto de reporte</legend>
        <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
            <li class="nav-item">
                <button class="nav-link active" id="operaciones-tab" data-bs-toggle="tab" data-bs-target="#operaciones" type="button" role="tab" aria-controls="operaciones" aria-selected="true">
                    Operaciones
                </button>
            </li>
            <li class="nav-item ms-auto" id="mainButtonContainer">
                <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configOperaciones.edit();">
                    <i class="fas fa-plus-circle"></i> Operación
                </button>
            </li>
        </ul>
        <div class="tab-content pt-2">
            <div class="tab-pane fade show active" id="operaciones" role="tabpanel" aria-labelledby="operaciones-tab">
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <table id="tblOperaciones" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                            <thead>
                                <th>N°</th>
                                <th>Tipo de operación</th>
                                <th>Producto / Servicio</th>
                                <th>Número / Referencia</th>
                                <th>Titular / Beneficiario / Remitente</th>
                                <th>Monto</th>
                                <th>Recepción</th>
                                <th>Agencia</th>
                                <th>Fecha y hora</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <form id="frmAntecedentes" name="frmAntecedentes" action="javascript:void(0);" method="POST" accept-charset="utf-8" class="needs-validation" novalidate>
        <fieldset class="border p-1" style="padding-top: 0;">
            <legend class="w-auto float-none mb-0 pb-0">Análisis de la operación u operaciones reportadas (Antecedentes)</legend>
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <textarea name="txtAntecedentes" id="txtAntecedentes" cols="30" rows="5" class="form-control" required></textarea>
                </div>
            </div>
        </fieldset>
    </form>
    <fieldset class="border p-1" style="padding-top: 0;">
        <legend class="w-auto float-none mb-0 pb-0">Documentos anexos al reporte</legend>
        <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
            <li class="nav-item">
                <button class="nav-link active" id="documentos-tab" data-bs-toggle="tab" data-bs-target="#documentos" type="button" role="tab" aria-controls="documentos" aria-selected="true">
                    Documentos anexos
                </button>
            </li>
            <li class="nav-item ms-auto" id="mainButtonContainer">
                <button type="button" class="btn btn-sm btn-outline-primary ml-2" onclick="javascript:configDocumentos.edit();">
                    <i class="fas fa-plus-circle"></i> Documento
                </button>
            </li>
        </ul>
        <div class="tab-content pt-2">
            <div class="tab-pane fade show active" id="documentos" role="tabpanel" aria-labelledby="documentos-tab">
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <table id="tblDocumentos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                            <thead>
                                <th>N°</th>
                                <th>Documento</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <form id="frmDiligencia" name="frmDiligencia" accept-charset="utf-8" method="POST" novalidate class="needs-validation" action="javascript:void(0);">
        <fieldset class="border p-1" style="padding-top: 0;">
            <legend class="w-auto float-none mb-0 pb-0">Datos sobre el diligenciamiento del reporte</legend>
            <div class="row">
                <div class="col-lg-3 col-xl-3">
                    <label for="cboAgenciaDiligencia" class="form-label">Agencia <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select id="cboAgenciaDiligencia" name="cboAgenciaDiligencia" class="selectpicker form-control cboAgencia" title="" data-live-search="true" required>
                            <option selected disabled value="">seleccione</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="cboColaborador" class="form-label">Colaborador <span class="requerido">*</span></label>
                    <div class="form-group has-validation">
                        <select id="cboColaborador" name="cboColaborador" class="selectpicker form-control cboColaborador" title="" data-live-search="true" required>
                            <option selected disabled value="">seleccione</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <label for="txtAreaColaborador" class="form-label">Área <span class="requerido">*</span></label>
                    <input id="txtAreaColaborador" type="text" name="txtAreaColaborador" class="form-control" required title="" value="">
                </div>
                <div class="col-lg-3 col-xl-3 mayusculas">
                    <label for="txtJefeInmediato" class="form-label">Código de jefe inmediato <span class="requerido">*</span></label>
                    <input type="text" id="txtJefeInmediato" name="txtJefeInmediato" class="form-control" required title="Nombre de jefe inmediato">
                </div>
            </div>
        </fieldset>
    </form>
</section>
<div class="modal fade" id="mdlOperacion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Operación objeto de reporte</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configOperaciones.save();" id="frmOperacion" name="frmOperacion" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class='col-lg-3 col-xl-3'>
                            <label for='cboTipoOperacion' class="form-label">Tipo de operación <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboTipoOperacion" name="cboTipoOperacion" class="form-control selectpicker cboTipoOperacion" required data-live-search="true">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3' id="containerCboProducto">
                            <label for='cboTipoProducto' class="form-label">Tipo de producto / servicio <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboTipoProducto" name="cboTipoProducto" class="form-control selectpicker cboTipoProducto" data-live-search="true" required onchange="configOperaciones.evalTipoProducto(this.value);">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3' style="display: none;" id="containerProducto">
                            <label for='txtTipoProducto' class="form-label">Tipo de producto / servicio <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtTipoProducto' name='txtTipoProducto' value="">
                            <div class='invalid-feedback'>
                                Ingrese el tipo de producto / referencia
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='txtNumeroProducto' class="form-label">Número de producto / referencia <span class="requerido">*</span></label>
                            <input type='text' class='form-control' id='txtNumeroProducto' name='txtNumeroProducto' value="" required readonly>
                            <div class='invalid-feedback'>
                                Ingrese el número de producto / referencia
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='cboTipoRelacion' class="form-label">Relación <span class="requerido">*</span></label>
                            <select id="cboTipoRelacion" name="cboTipoRelacion" class="form-select" required onchange="configOperaciones.evalTipoRelacion(this.value);">
                                <option selected value="" disabled>seleccione</option>
                                <option value="titular">Titular</option>
                                <option value="beneficiario">Beneficiario</option>
                                <option value="remitente">Remitente</option>
                            </select>
                            <div class='invalid-feedback'>
                                Seleccione un tipo de relación
                            </div>
                        </div>
                        <div class='col-lg-6 col-xl-6 mayusculas'>
                            <label for='txtNombreRelacion' class="form-label">Nombre de referente</label>
                            <input type='text' class='form-control' id='txtNombreRelacion' name='txtNombreRelacion' value="" required>
                            <div class='invalid-feedback'>
                                Ingrese el nombre completo
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="numMonto" class="form-label">Monto de operación <span class="requerido">*</span> </label>
                            <input type="number" min="0" step="0.01" onkeypress="return generalSoloNumeros(event);" id="numMonto" name="numMonto" placeholder="" title="monto de operación" class="form-control" value="" required>
                            <div class="invalid-feedback">
                                Ingrese el monto de la operación
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboTipoRecepcion' class="form-label">Tipo de recepción <span class="requerido">*</span></label>
                            <select id="cboTipoRecepcion" name="cboTipoRecepcion" class="form-select" required>
                                <option selected value="" disabled>seleccione</option>
                                <option value="Efectivo">Efectivo</option>
                                <option value="Otros medios">Otros medios</option>
                            </select>
                            <div class='invalid-feedback'>
                                Seleccione un tipo de recepción
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='cboAgencia' class="form-label">Agencia <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboAgencia" name="cboAgencia" class="form-control selectpicker cboAgencia" required>
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class='col-lg-3 col-xl-3'>
                            <label for='cboArea' class="form-label">Área <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboArea" name="cboArea" class="form-control selectpicker cboArea" required>
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for="txtFechaOperacion" class="form-label">Fecha de operación <span class="requerido">*</span> </label>
                            <input type="text" id="txtFechaOperacion" name="txtFechaOperacion" placeholder="dd-mm-yyyy h:i" title="fecha de operación" class="form-control readonly fechaHoraAbierta" value="" required>
                            <div class="invalid-feedback">
                                seleccione la fecha
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3">
                            <label for='cboEstadoOperacion' class="form-label">Estado de la operación <span class="requerido">*</span></label>
                            <select id="cboEstadoOperacion" name="cboEstadoOperacion" class="form-select" required>
                                <option selected value="" disabled>seleccione</option>
                                <option value="Intentada">Intentada</option>
                                <option value="Ejecutada">Ejecutada</option>
                            </select>
                            <div class='invalid-feedback'>
                                Seleccione un estado
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmOperacion" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlDocumento" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Documento adjunto</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configDocumentos.save();" id="frmDocumento" name="frmDocumento" accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                    <div class="row">
                        <div class='col-lg-12 col-xl-12' id="documentoContainer">
                            <label for='cboDocumentoAdjunto' class="form-label">Documento <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboDocumentoAdjunto" name="cboDocumentoAdjunto" class="form-control selectpicker cboTipoDocumentoAdjunto" required data-live-search="true" onchange="configDocumentos.evalDocumento(this.value);">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class='col-lg-12 col-xl-12' id="newDocumentoContainer" style="display: none;">
                            <label for="txtDocumentoAdjunto" class="form-label">Documento <span class="requerido">*</span></label>
                            <input id="txtDocumentoAdjunto" name="txtDocumentoAdjunto" class="form-control" title="Documento adjunto" required>
                            <div class="invalid-feedback">
                                Ingrese el tipo de documento
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmDocumento" class="btn btn-sm btn-primary">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mdlGeneracion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Generación de documentos</h5>
            </div>
            <div class="modal-body">
                <div class="row pt-2">
                    <div class="col-lg-2 col-xl-2"></div>
                    <div class="col-lg-8 col-xl-8">
                        <div class="d-grid">
                            <button type="button" class="btn btn-success" title="ROI" onclick="configROI.saveDB();">
                                <i class="fas fa-file-pdf"> Formulario ROI 01</i>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mt-2" align="center">
                        <span> Intentos restantes: <strong id="strIntentos"></strong></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<?php
$_GET["js"] = ['oficialiaROI'];
