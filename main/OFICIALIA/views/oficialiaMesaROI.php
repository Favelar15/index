<div class="pagetitle">
    <h1>Mesa de trabajo - Formulario ROI</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">OFICIALIA</li>
            <li class="breadcrumb-item">Mesa de trabajo</li>
            <li class="breadcrumb-item active">Formularios ROI</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="asignacion-tab" data-bs-toggle="tab" data-bs-target="#asignacion" type="button" role="tab" aria-controls="asignacion" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesAsignacion');">
                Asignación
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="formulario-tab" data-bs-toggle="tab" data-bs-target="#formulario" type="button" role="tab" aria-controls="formulario" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','botonesFormulario');">
                Formulario
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="botonesAsignacion" style="display: none;"></div>
            <div class="tabButtonContainer" id="botonesFormulario" style="display: none;">
                <button type="button" class="btn btn-sm btn-outline-success ml-2" onclick="javascript:configFormulario.save();">
                    <i class="fas fa-save"></i> Guardar
                </button>
                <button type="button" class="btn btn-sm btn-outline-danger ml-2" onclick="javascript:configFormulario.cancel();">
                    <i class="fas fa-times"></i> Cancelar
                </button>
            </div>
        </li>
    </ul>
    <div class="tab-content pt-2">
        <div class="tab-pane fade show active" id="asignacion" role="tabpanel" aria-labelledby="asignacion-tab">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <table id="tblFormularios" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Correlativo</th>
                                <th>Documento</th>
                                <th>Nombre</th>
                                <th>Agencia</th>
                                <th>Colaborador</th>
                                <th>Fecha de registro</th>
                                <th>Usuario</th>
                                <th>Ubicación</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="formulario" role="tabpanel" aria-labelledby="formulario-tab">
            <div class="accordion accordion-flush" id="flush-formularioROI">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingDatosPersonales">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-datosPersonales" aria-expanded="false" aria-controls="flush-datosPersonales">
                            Datos personales
                        </button>
                    </h2>
                    <div id="flush-datosPersonales" class="accordion-collapse collapse" aria-labelledby="flush-headingDatosPersonales" data-bs-parent="#formularioROI">
                        <div class="accordion-body">
                            <form id="frmPrincipal" name="frmPrincipal" accept-charset="utf-8" method="post" action="javascript:configROI.save();" novalidate class="needs-validation">
                                <div class="row">
                                    <div class="col-lg-3 col-xl-3">
                                        <label for="cboTipoDocumento" class="form-label">Tipo de documento <span class="requerido">*</span></label>
                                        <select id="cboTipoDocumento" name="cboTipoDocumento" class="form-select cboTipoDocumento" title="Tipo de documento" required onchange="generalMonitoreoTipoDocumento(this.value,'txtNumeroDocumento');" disabled>
                                            <option selected disabled value="">seleccione</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Seleccione un tipo de documento
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for="txtNumeroDocumento" class="form-label">Número de documento <span class="requerido">*</span></label>
                                        <input type="text" class="form-control" required id="txtNumeroDocumento" name="txtNumeroDocumento" readonly>
                                        <div class="invalid-feedback">
                                            Ingrese el número de documento
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3 mayusculas">
                                        <label for="txtNombres" class="form-label">Nombres <span class="requerido">*</span></label>
                                        <input type="text" id="txtNombres" name="txtNombres" class="form-control" required placeholder="Ingrese los nombres" readonly>
                                        <div class="invalid-feedback">
                                            Ingrese los nombres
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3 mayusculas">
                                        <label for="txtPrimerApellido" class="form-label">Primer apellido <span class="requerido">*</span></label>
                                        <input type="text" id="txtPrimerApellido" name="txtPrimerApellido" class="form-control" required placeholder="Ingrese el primer apellido" readonly>
                                        <div class="invalid-feedback">
                                            Ingrese el primer apellido
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3 mayusculas">
                                        <label for="txtSegundoApellido" class="form-label">Segundo apellido</label>
                                        <input type="text" id="txtSegundoApellido" name="txtSegundoApellido" class="form-control" placeholder="Ingrese el segundo apellido" readonly>
                                        <div class="invalid-feedback">
                                            Ingrese el segundo apellido
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3 mayusculas">
                                        <label for="txtApellidoCasada" class="form-label">Apellido de casada</label>
                                        <input type="text" id="txtApellidoCasada" name="txtApellidoCasada" class="form-control" placeholder="Ingrese el apellido de casada" readonly>
                                        <div class="invalid-feedback">
                                            Ingrese el apellido de casada
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3 mayusculas">
                                        <label for="txtProfesion" class="form-label">Profesión <span class="requerido">*</span></label>
                                        <input type="text" id="txtProfesion" name="txtProfesion" class="form-control" placeholder="Ingrese la profesión" required readonly>
                                        <div class="invalid-feedback">
                                            Ingrese la profesión
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for="cboActividadEconomica" class="form-label">Actividad económica <span class="requerido">*</span></label>
                                        <div class="form-group has-validation">
                                            <select id="cboActividadEconomica" name="cboActividadEconomica" class="form-control selectpicker cboActividadEconomica" data-live-search="true" required title="" disabled>
                                                <option selected disabled value="">seleccione</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for="cboPais" class="form-label">País de residencia <span class="requerido">*</span></label>
                                        <div class="form-group has-validation">
                                            <select id="cboPais" name="cboPais" class="form-control selectpicker cboPais" data-live-search="true" required title="" onchange="generalMonitoreoPais(this.value,'cboDepartamento');" disabled>
                                                <option selected disabled value="">seleccione</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for="cboDepartamento" class="form-label">Departamento de residencia <span class="requerido">*</span></label>
                                        <div class="form-group has-validation">
                                            <select id="cboDepartamento" name="cboDepartamento" class="form-control selectpicker" data-live-search="true" required title="" onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');" disabled>
                                                <option selected disabled value="">seleccione un país</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for="cboMunicipio" class="form-label">Municipio de residencia <span class="requerido">*</span></label>
                                        <div class="form-group has-validation">
                                            <select id="cboMunicipio" name="cboMunicipio" class="form-control selectpicker" data-live-search="true" required title="" disabled>
                                                <option selected disabled value="">seleccione un departamento</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3 mayusculas">
                                        <label for="txtDireccion" class="form-label">Dirección <span class="requerido">*</span></label>
                                        <input type="text" id="txtDireccion" name="txtDireccion" class="form-control" required placeholder="Ingrese la dirección" readonly>
                                        <div class="invalid-feedback">
                                            Ingrese la dirección
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOperaciones">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-operaciones" aria-expanded="false" aria-controls="flush-operaciones">
                            Operaciones
                        </button>
                    </h2>
                    <div id="flush-operaciones" class="accordion-collapse collapse" aria-labelledby="flush-headingOperaciones" data-bs-parent="#formularioROI">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <table id="tblOperaciones" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <th>N°</th>
                                            <th>Tipo de operación</th>
                                            <th>Producto / Servicio</th>
                                            <th>Número / Referencia</th>
                                            <th>Titular / Beneficiario / Remitente</th>
                                            <th>Monto</th>
                                            <th>Recepción</th>
                                            <th>Agencia</th>
                                            <th>Fecha y hora</th>
                                            <th>Estado</th>
                                            <!-- <th>Acciones</th> -->
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingAdjuntos">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-adjuntos" aria-expanded="false" aria-controls="flush-adjuntos">
                            Documentos adjuntos
                        </button>
                    </h2>
                    <div id="flush-adjuntos" class="accordion-collapse collapse" aria-labelledby="flush-headingAdjuntos" data-bs-parent="#formularioROI">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <table id="tblDocumentos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <th>N°</th>
                                            <th>Documento</th>
                                            <!-- <th>Acciones</th> -->
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingDiligencia">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-diligencia" aria-expanded="false" aria-controls="flush-diligencia">
                            Diligencia
                        </button>
                    </h2>
                    <div id="flush-diligencia" class="accordion-collapse collapse" aria-labelledby="flush-headingDiligencia" data-bs-parent="#formularioROI">
                        <div class="accordion-body">
                            <form id="frmDiligencia" name="frmDiligencia" accept-charset="utf-8" method="POST" novalidate class="needs-validation" action="javascript:void(0);">
                                <div class="row">
                                    <div class="col-lg-12 col-xl-12">
                                        <label for="txtAntecedentes" class="form-label">Antecedentes <span class="requerido">*</span></label>
                                        <textarea name="txtAntecedentes" id="txtAntecedentes" cols="30" rows="5" class="form-control" required readonly></textarea>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for="cboAgenciaDiligencia" class="form-label">Agencia <span class="requerido">*</span></label>
                                        <div class="form-group has-validation">
                                            <select id="cboAgenciaDiligencia" name="cboAgenciaDiligencia" class="selectpicker form-control cboAgencia" title="" data-live-search="true" required disabled>
                                                <option selected disabled value="">seleccione</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for="cboColaborador" class="form-label">Colaborador <span class="requerido">*</span></label>
                                        <div class="form-group has-validation">
                                            <select id="cboColaborador" name="cboColaborador" class="selectpicker form-control cboColaborador" title="" data-live-search="true" required disabled>
                                                <option selected disabled value="">seleccione</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-xl-3">
                                        <label for="txtAreaColaborador" class="form-label">Área <span class="requerido">*</span></label>
                                        <input id="txtAreaColaborador" type="text" name="txtAreaColaborador" class="form-control" required title="" value="" readonly>
                                    </div>
                                    <div class="col-lg-3 col-xl-3 mayusculas">
                                        <label for="txtJefeInmediato" class="form-label">Código de jefe inmediato <span class="requerido">*</span></label>
                                        <input type="text" id="txtJefeInmediato" name="txtJefeInmediato" class="form-control" required title="Nombre de jefe inmediato" readonly>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingGestiones">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#flush-gestiones" aria-expanded="true" aria-controls="flush-gestiones">
                            Gestiones
                        </button>
                    </h2>
                    <div id="flush-gestiones" class="accordion-collapse collapse show" aria-labelledby="flush-headingGestiones" data-bs-parent="#formularioROI">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                                        <li class="nav-item ms-auto" id="mainButtonContainer">
                                            <button type="button" class="btn btn-sm btn-outline-primary mb-1" onclick="javascript:configGestiones.edit();">
                                                <i class="fas fa-plus-circle"></i> Gestión
                                            </button>
                                        </li>
                                    </ul>
                                    <table id="tblGestiones" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <th>N°</th>
                                            <th>Gestión</th>
                                            <th>Usuario responsable</th>
                                            <th>Adjuntos</th>
                                            <th>Fecha de registro</th>
                                            <th>Última actualización</th>
                                            <th>Acciones</th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End Accordion without outline borders -->

        </div>
    </div>
</section>
<div class="modal fade" id="mdlGeneracion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Impresión de documentos</h5>
            </div>
            <div class="modal-body">
                <div class="row pt-2">
                    <div class="col-lg-2 col-xl-2">
                    </div>
                    <div class="col-lg-8 col-xl-8">
                        <div class="d-grid">
                            <button type="button" class="btn btn-success" title="Formulario ROI 1" onclick="configFormulario.printFormulario();">
                                <i class="fas fa-spell-check"> Formulario ROI 01</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdlGestion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Gestión</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configGestiones.save();" id="frmGestion" name="frmGestion" class="needs-validation" enctype="multipart/form-data" novalidate accept-charset="utf-8" method="POST">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for="txtGestion" class="form-label">Gestión <span class="requerido">*</span></label>
                            <textarea name="txtGestion" id="txtGestion" cols="30" rows="5" class="form-control" required></textarea>
                            <div class="invalid-feedback">
                                Ingrese la gestión
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6 mt-2">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <div class="card mb-0 pb-0">
                                        <div class="card-body">
                                            <h5 class="card-title mb-1">Archivo adjunto</h5>
                                            <div class="row">
                                                <div class="col-lg-10 col-xl-10">
                                                    <input type="file" accept="application/pdf" id="fileGestion" name="fileGestion" style="display: none;" class="form-control">
                                                    <div class="d-grid gap-2">
                                                        <button type="button" class="btn btn-sm btn-primary" id="btnFileGestion">
                                                            <i class="fas fa-upload"></i> Subir archivo
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-xl-2">
                                                    <div class="d-grid gap-2">
                                                        <button type="button" class="btn btn-sm btn-primary" title="Ver archivo">
                                                            <i class="fas fa-eye"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <divl class="col-lg-12 col-xl-12">
                                                    <span id="fileName" class="mt-2">No hay archivo seleccionado</span>
                                                </divl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6 mt-2">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <div class="card mb-0 pb-0">
                                        <div class="card-body">
                                            <h5 class="card-title mb-1">Solicitar archivo a una agencia</h5>
                                            <div class="row">
                                                <div class="col-lg-10 col-xl-10">
                                                    <div class="d-grid gap-2">
                                                        <button type="button" class="btn btn-sm btn-primary" onclick="configGestiones.configSolicitud.edit();">
                                                            <i class="fas fa-hand-holding"></i> Solicitar archivo
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-xl-2">
                                                    <div class="d-grid gap-2">
                                                        <button type="button" class="btn btn-sm btn-primary" title="Ver archivo">
                                                            <i class="fas fa-eye"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <divl class="col-lg-12 col-xl-12">
                                                    <span id="fileName" class="mt-2">No hay solicitud de archivo</span>
                                                </divl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmGestion" class="btn btn-sm btn-primary">Guardar</button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade mt-5" id="mdlSolicitud" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Solicitud de archivo</h5>
            </div>
            <div class="modal-body">
                <form action="javascript:configGestiones.configSolicitud.save();" class="needs-validation" id="frmSolicitud" name="frmSolicitud" novalidate accept-charset="utf-8" method="POST">
                    <div class="row">
                        <div class='col-lg-12 col-xl-12' id="documentoContainer">
                            <label for='cboDocumentoAdjunto' class="form-label">Documento <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboDocumentoAdjunto" name="cboDocumentoAdjunto" class="form-control selectpicker cboTipoDocumentoAdjunto" required data-live-search="true" onchange="configDocumentos.evalDocumento(this.value);">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class='col-lg-12 col-xl-12' id="newDocumentoContainer" style="display: none;">
                            <label for="txtDocumentoAdjunto" class="form-label">Documento <span class="requerido">*</span></label>
                            <input id="txtDocumentoAdjunto" name="txtDocumentoAdjunto" class="form-control" title="Documento adjunto" required>
                            <div class="invalid-feedback">
                                Ingrese el tipo de documento
                            </div>
                        </div>
                        <div class='col-lg-12 col-xl-12'>
                            <label for='cboAgenciaSolicitud' class="form-label">Agencia <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboAgenciaSolicitud" name="cboAgenciaSolicitud" class="form-control selectpicker cboAgencia" required data-live-search="true">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmSolicitud" class="btn btn-sm btn-primary">Guardar</button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<?php
$_GET['js'] = ['oficialiaMesaROI'];
