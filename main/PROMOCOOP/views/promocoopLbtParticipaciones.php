<div class="pagetitle">
    <h1>PROMOCOOP - La Bolsa del Tersoro</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">PROMOCOOP</li>
            <li class="breadcrumb-item">Participaciones</li>
            <li class="breadcrumb-item active">La Bolsa del Tesoro</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="listadoCreditos-tab" data-bs-toggle="tab"
                    data-bs-target="#listadoCreditos" role="tab" aria-controls="Lista de creditos" aria-selected="true"
                    onclick="generalShowHideButtonTab('mainButtonContainer','primaryButtons');">
                Participaciones
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">

            <div class="tabButtonContainer" id="primaryButtons">
                <button type="button" id="btnGuardar" form="frmCredito" class="btn btn-sm btn-outline-success" disabled>
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button type="button" onclick="configCredito.cancel()" class="btn btn-sm btn-outline-danger">
                    <i class="fa fa-times"></i> Cancelar
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="addCredito" role="tabpanel" aria-labelledby="evaluacion-tab">
            <form id="frmParticipacionesLBT" name="frmCredito" accept-charset="utf-8" method="post"
                  action="javascript:configBolsaTesoro.save()" class="needs-validation" novalidate>

                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtCodigoCliente">Buscar asociado: <span
                                    class="requerido">*</span></label>
                        <div class="input-group form-group has-validation">
                            <input type="text" class="form-control" id="txtCodigoCliente"
                                   onkeypress="return generalSoloNumeros(event)" name="txtCodigoCliente"
                                   placeholder="Codigo del cliente" required>
                            <div class="input-group-append">
                                <button type="button" id="btnBuscarDatosAsociado" onclick="configBolsaTesoro.buscarDatosAsociado()" class="btn btn-outline-dark"><i class="fas fa-search"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el código de cliente
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNombres">Nombres: </label>
                        <input type="text" class="form-control" id="txtNombres" name="txtNombres" placeholder="Nombres"
                               readonly>
                        <div class="invalid-feedback">
                            Ingrese los nombres
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtApellidos">Apellidos: </label>
                        <input type="text" class="form-control" id="txtApellidos" name="txtApellidos"
                               placeholder="Apellidos" readonly>
                        <div class="invalid-feedback">
                            Ingrese los apellidos
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtEstatusAsociado">Estatus: </label>
                        <input type="text" class="form-control" id="txtEstatusAsociado" name="txtEstatusAsociado"
                               placeholder="Estatus" readonly>
                        <div class="invalid-feedback">

                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtDeudaAportaciones">Deuda en aportaciones: </label>
                        <input type="text" class="form-control" id="txtDeudaAportaciones" name="txtDeudaAportaciones"
                               placeholder="USD $0.00" readonly>
                        <div class="invalid-feedback">

                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtSellosAntesTransaccion">Sellos antes transacción: </label>
                        <input type="text" class="form-control txtSellosAntesTransaccion" id="txtSellosAntesTransaccion"
                               name="txtSellosAntesTransaccion" placeholder="Sellos" readonly required>
                        <div class="invalid-feedback">

                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtSellosAplicables">Sellos aplicables: </label>
                        <input type="text" class="form-control txtSellosAplicables" id="txtSellosAplicables"
                               name="txtSellosAplicables" placeholder="Sellos" readonly required>
                        <div class="invalid-feedback">

                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtAbono">Monto a abonar: </label>
                        <input type="number"  class="form-control" id="txtAbono" name="txtAbono"
                               min ="0" placeholder="USD $0.00" onblur="configBolsaTesoro.calcularSellosAplicables()" readonly>
                        <div class="invalid-feedback">

                        </div>
                    </div>
                </div>
                <br>
                <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                    <li class="nav-item">
                        <button type="button" class="nav-link active" id="DetalleCanjes-tab" data-bs-toggle="tab"
                                data-bs-target="#DetalleCanjes" role="tab" aria-controls="DetalleCanjes" aria-selected="true">
                            Detalle de Canjes
                        </button>
                    </li>
                    <li class="nav-item ms-auto" id="mainButtonContainer">
                        <div class="tabButtonContainer" id="primaryButtons">
                            <button type="button" id="btnAgregarArticulo" onclick="configBolsaTesoro.mdlMostrarArticulos()" class="btn btn-sm btn-outline-dark" disabled>
                                <i class="fa fa-cart-plus"></i> Agregar Artículo
                            </button>
                        </div>
                    </li>
                </ul>


            </form>
            <div class="row" id="divTblCanjes">
                <div class="tab-content" style="margin-top: 20px;">

                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblDetalleCanjes" class="table table-striped table-bordered"
                                   style="text-align:center; width:100% !important;">
                                <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Nombre Articulo</th>
                                    <th>Imagen</th>
                                    <th>Cantidad Artículos</th>
                                    <th>Sellos Canjeados</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<?php
include ('modals/promocoopLBTmostrarArticulo.php');
$_GET['js'] = ['promocoopLbtParticipaciones'];
?>