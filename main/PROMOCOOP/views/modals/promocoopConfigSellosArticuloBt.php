
<div class="modal fade " tabindex="-1" id="mdlPromocoopConfigSellos" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span id="spnTitulo">PROMOCOOP - Configurar sellos en articulos</span></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="frmConfigSellos" name="frmConfigSellos" accept-charset="utf-8" method="POST"
                          action="javascript:configAdministacionSellos.save();" class="needs-validation" novalidate>
                        <div class="row" style="padding-bottom: 15px;">
                            <input type="hidden" id="hdnIdConfiguracion" name="hdnIdConfiguracion" value="0">
                            <div class="col-md-4">
                                <div class="offset-1 text-center promocoopImgArticulo" id="divContenedorImagen">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xl-12">
                                        <label class="form-label">Selecione articulo <span
                                                class="requerido">*</span></label>
                                        <select class="form-control selectpicker cboArticuloSellos"  data-live-search="true"
                                                id="cboArticuloSellos" name="cboArticuloSellos"
                                                onchange="configAdministacionSellos.evalArtciculoSelected(this.value)" required>
                                            <option value="">Seleccione</option>
                                        </select>
                                        <div class="invalid-feedback">
                                           Seleccione
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xl-12">
                                        <label class="form-label">Cantidad Sellos <span
                                                class="requerido">*</span></label>
                                        <input class="form-control" type="text" id="numCantidadSellos"
                                               name="numCantidadSellos" onkeypress="return generalSoloNumeros(event)"
                                               maxlength="150" required>
                                        <div class="invalid-feedback">
                                           Seleccione
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-primary" form="frmConfigSellos">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>