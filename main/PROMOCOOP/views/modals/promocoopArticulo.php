<?php
?>
<div class="modal fade " tabindex="-1" id="mdlPromocoopArticulo" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span id="spnTitulo">PROMOCOOP - Registro de artículos</span></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="frmDatosArticulos" name="frmDatosArticulos" accept-charset="utf-8" method="POST"
                          action="javascript:configAdministracionArticulos.save();" class="needs-validation" novalidate>
                        <div class="row" style="padding-bottom: 15px;">
                            <input type="hidden" id="hdnIdArticulo" name="hdnIdArticulo" value="0">
                            <div class="col-md-5">
                                <div class="offset-2 text-center promocoopImgArticulo" id="divContenedorImagen">
                                </div>
                                <div class="has-validation offset-4">
                                    <input type="file" id="imgArticulo" name="imgArticulo" value=""
                                           onchange="configAdministracionArticulos.evalStatusImage();"
                                           style="display: none;" accept=".gif, .jpg, .png .jpeg">
                                    <button type="button" class="btn btn-primary btn-sm" title="Cargar nueva imagen"
                                            onclick="$('#imgArticulo').trigger('click');">
                                        <i class="bi bi-upload"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger btn-sm"
                                            title="Eliminar mi foto de perfil" onclick="configAdministracionArticulos.deleteImage();">
                                        <i class="bi bi-trash"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xl-12">
                                        <label class="form-label">Nombre artículo <span
                                                    class="requerido">*</span></label>
                                        <input class="form-control" type="text" id="txtNombreArticulo"
                                               name="txtNombreArticulo" onkeypress="return generaLetrasNumeros(event)"
                                               maxlength="150" required>
                                        <div class="invalid-feedback">
                                            Ingrese el tipo de garantía
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xl-12">
                                        <label class="form-label" for="cboLimiteDisponibilidad">¿Posee fecha límite de
                                            disponibilidad? <span class="requerido">*</span></label>
                                        <input type="text" id="txtFechaLimite" name="txtFechaLimite"
                                               class="form-control fechaAbierta readonly" placeholder="dd/mm/yyyy"
                                               >
                                        <div class="invalid-feedback">
                                            Ingrese la fecha limite
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-primary" form="frmDatosArticulos">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>