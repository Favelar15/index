<?php

?>
<div class="modal fade " tabindex="-1" id="mdlPromocoopLBTMostrarArticulos" data-bs-keyboard="false"
     data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span id="spnTitulo">PROMOCOOP - Registro de artículos</span></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="frmArticulosCanjeLBT" name="frmArticulosCanjeLBT" accept-charset="utf-8" method="POST"
                          action="javascript:configAdministracionArticulos.save();" class="needs-validation" novalidate>
                        <div class="row" style="padding-bottom: 15px;">
                            <input type="hidden" id="hdnIdArticulo" name="hdnIdArticulo" value="0">
                            <div class="col-md-5">
                                <div class="offset-2 text-center promocoopImgArticulo" id="divContenedorImagen">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xl-12">
                                        <label class="form-label">Nombre artículo <span
                                                    class="requerido">*</span></label>
                                        <select class=" cboArticuloLBT" id="cboArticuloLBT">
                                            <option selected>Seleccione Artículo</option>

                                        </select>
                                        <div class="invalid-feedback">
                                            Ingrese el tipo de garantía
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xl-12">
                                        <label class="form-label" for="">Cantidad<span
                                                    class="requerido">*</span></label>
                                        <select class="form-select" id="cboCantidadSellosArticuloLBT">
                                            <option selected>Seleccione Cantidad</option>

                                        </select>
                                        <div class="invalid-feedback">
                                            Ingrese la fecha limite
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-primary" form="frmDatosArticulos">
                    Guardar
                </button>
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>