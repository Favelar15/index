<link rel="stylesheet" href="./main/css/hipotecas.css">
<div class="pagetitle">
    <h1>Administración de Artículos</h1>
    <nav>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item">PROMOCOOP</li>
            <li class="breadcrumb-item active">Administración de Artículos.</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">


<section class="section">
    <form id="frmAdministracionArticulos" name="frmAdministracionArticulos" method="POST" accept-charset="utf-8" action="javascript:void(0);">
        <div class="bloques" id="bloquePrincipal">
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="deudor-tab" data-bs-toggle="tab" data-bs-target="#divfrmAdministracionHipotecas" role="tab" aria-controls="deudor" aria-selected="true">
                        Administración
                    </button>
                </li>
                <li class="nav-item ms-auto">
                    <button type="button" class="btn  btn-outline-dark btn-sm " onclick="javascript: configAdministracionArticulos.showModal()">
                        <i class="fas fa-plus-circle"></i> Nuevo Artículo
                    </button>
                </li>
            </ul>
            <div class="tab-content" style="margin-top: 20px;">
                <div class="tab-pane fade show active" id="divfrmAdministracionArticuls" role="tabpanel" aria-labelledby="fiadores-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblAdministracionArticulos" class="table table-striped table-bordered" style="text-align:center; width:100% !important;">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Nombre Articulo</th>
                                        <th>Imagen</th>
<!--                                        <th>Inclusiones</th>-->
                                        <th>Limite de disponibilidad</th>
                                        <th>Fecha de registro</th>
                                        <th>Ultima actualización</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
    <?php include ('modals/promocoopArticulo.php') ?>
</section>
<?php
$_GET["js"] = ['promocoopGenerales','promocoopAdministrarArticulos'];
