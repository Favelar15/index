<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";
session_start();
$respuesta = (object)[];
$respuesta->{'respuesta'} = "NO_ACCION";
$accion = "";


if ($_SESSION['index']) {
    if (!empty($_GET)) {
        $accion = isset($_GET['accion']) ? $_GET['accion'] : "";
    } else if (isset($input['accion'])) {
        $accion = isset($input['accion']) ? $input['accion'] : "prueba";
    }
    include "../../code/connectionSqlServer.php";
    include "./clases/sellos.php";

    $sellos = new sellos();

    switch ($accion) {
        case 'obtenerArticulosSinSellosCbo':
            $respuesta = $sellos->obtenerArticulosSinSellosCbo();
            break;
        case  'guardarConfiguracion':
            $sellos->id = (isset($input['id']) && $input['id'] != 0) ? $input['id'] : "0";
            $sellos->sellos = $input['numCantidadSellos']['value'];
            $sellos->idArticulo = $input['cboArticuloSellos']['value'];
            $sellos->idUsuario = $_SESSION['index']->id;
            $sellos->ipOrdenador = generalObtenerIp();
            $sellos->idApartado = base64_decode(urldecode($input['idApartado']));
            $sellos->tipoApartado = $input['tipoApartado'];
            $sellos->logsCambio = $input['logsCambios'];
            $sellos->idAgenciaActual = $_SESSION['index']->agenciaActual->id;
            $respuesta->{'respuesta'} = $sellos->guardarConfiguracionSellos();
            break;
        case 'obtenerArticulosConSellos':
            $respuesta = $sellos->obtenerArticulosConSellos();
            break;
        case 'eliminarConfiguracionSellos':
            $sellos->id = strval($input['id']);
            $sellos->idApartado = base64_decode(urldecode($input['idApartado']));
            $sellos->idUsuario = $_SESSION['index']->id;
            $sellos->idAgenciaActual = $_SESSION['index']->agenciaActual->id;
            $sellos->tipoApartado = $input['tipoApartado'];
            $sellos->ipOrdenador = generalObtenerIp();
            $sellos->logsCambio = "ELIMININADO%%%NO%%%SI";
            $respuesta->{'respuesta'} = $sellos->eliminarConfiguracionSellos();
            break;
    }


} else {
    $respuesta->{'respuesta'} = 'SESION';
}
echo json_encode($respuesta);