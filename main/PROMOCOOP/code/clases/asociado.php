<?php

class Asociado
{
    protected $conection;
    public $codigoCliente,
        $nombres,
        $apellidos,
        $estatus,
        $montoPendiente,
        $sellosDisponibles,
        $idApartado,
        $subApartado,
        $idUsuario;

    public function __construct()
    {
        global $conexion;
        $this->conection = $conexion;
    }

    function obtenerDatosAsociadosLBT()
    {

        $retorno = 'NO_DATOS';
        $query = "select * from promocoop_viewObtenerDatosAsociadoLBT where codigoCliente = $this->codigoCliente";

        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();
        if ($result->rowCount() > 0) {
            $retorno = $result->fetchAll(PDO::FETCH_ASSOC);
        }
        $this->conection = null;
        return $retorno;

    }

}