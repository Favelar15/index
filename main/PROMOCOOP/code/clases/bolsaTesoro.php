<?php
class  bolsaTesoro {
    protected $conection;
    public $id,
    $maximoAnualSellos,
    $maximoMensualSellos,
    $participacionInactividad,
    $permitirAbonos,
    $permitirCanjes,
    $idUsuario,
    $ipUsuario,
    $idApartado,
    $tipoApartado,
    $logs;
    public function __construct()
    {
        global $conexion;
        $this->conection = $conexion;
    }
    function obtenerConfiguracionesLBT (){
        $retorno = 'NO_DATOS';
        $query = "select * from promocoop_configBT where habilitado = 'S' ";

        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();
        if ($result->rowCount() > 0) {
            $retorno = $result->fetchAll(PDO::FETCH_ASSOC);
        }
        $this->conection = null;
        return $retorno;
    }
}