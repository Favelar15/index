<?php

class sellos
{
    protected $conection;
    public $id,
        $idArticulo,
        $sellos,
        $idUsuario,
        $ipOrdenador,
        $idApartado,
        $tipoApartado,
        $logsCambio,
        $idAgenciaActual;

    public function __construct()
    {
        global $conexion;
        $this->conection = $conexion;
    }

    function obtenerArticulosSinSellosCbo()
    {
        $retorno = 'NO_DATOS';
        $query = "select * from promocoo_viewObtenerArticulosSinSellosCbo";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();
        if ($result->rowCount() > 0) {
            $retorno = $result->fetchAll(PDO::FETCH_ASSOC);
        }
        $this->conection = null;
        return $retorno;
    }

    function obtenerArticulosConSellos()
    {
        $retorno = "NO_DATOS";
        $query = "select * from promocoop_viewObtenerArticulosConSellos";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();
        if ($result->rowCount() > 0) {
            $retorno = $result->fetchAll(PDO::FETCH_ASSOC);
        }
        $this->conection = null;
        return $retorno;
    }

    function guardarConfiguracionSellos()
    {

        $retorno = 'NO_DATOS';
        $response = null;
        $query = "EXEC promocoop_spGuardarConfiguracionSellos :id, :idArticulo, :sellos, :idUsuario, :ipOrdenador, :idApartado, :tipoApartado, :logsCambios, :idAgenciaActual, :response ";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idArticulo', $this->idArticulo, PDO::PARAM_INT);
        $result->bindParam(':sellos', $this->sellos, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':logsCambios', $this->logsCambio, PDO::PARAM_STR);
        $result->bindParam(':idAgenciaActual', $this->idAgenciaActual, PDO::PARAM_INT);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        if ($result->execute()) {
            $retorno = $response;
        }
        $this->conection = null;
        return $retorno;
    }

    function eliminarConfiguracionSellos()
    {
//        return $this;
//        die();
        $retorno = 'NO_DATOS';
        $response = null;
        $query = "EXEC promocoop_spEliminarconfiguracionSellos :id, :idApartado, :idUsuario, :idAgenciaActual, :tipoApartado, :ipOrdenador, :logsCambios, :response";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':idAgenciaActual', $this->idAgenciaActual, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':logsCambios', $this->logsCambio, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        if ($result->execute()) {
            $retorno = $response;
        }
        $this->conection = null;
        return $retorno;

    }

}