<?php

class Articulo
{
    protected $conection;
    public $id,
        $nombreArticulo,
        $nombreImagen,
        $fechaDisponibilidad,
        $cantidadSellos,
        $idApartado,
        $tipoApartado,
        $idUsuario,
        $ipActual,
        $idAgenciaActual,
        $logCambios;

    public function __construct()
    {
        global $conexion;
        $this->conection = $conexion;
    }

    function obtenerArticulosAdministracion()
    {
        $retorno = 'NO_DATOS';
        $query = "select * from promocoop_viewObtenerArticulosGeneral ";

        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();
        if ($result->rowCount() > 0) {
            $retorno = $result->fetchAll(PDO::FETCH_ASSOC);
        }
        $this->conection = null;
        return $retorno;
    }

    function obtenerArticulosPorSellos()
    {
        $retorno = 'NO_DATOS';
        $sellos =  $this->cantidadSellos;
        $query = "select * from promocoop_viewObtenerArticulosPorSellos where sellos <=  $sellos ";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();
        if ($result->rowCount() > 0) {
            $retorno = $result->fetchAll(PDO::FETCH_ASSOC);
        }
        $this->conection = null;
        return $retorno;
    }

    function guardarArticulo()
    {
        $retorno = 'NO_DATOS';
        $response = null;
        $query = "EXEC promocoop_spGuardarArticulo :id, :nombreArticulo, :nombreImagen, :fechaDisponibilidad, :idApartado, :tipoApartado, :idUsuario, :ipActual, :idAgenciaActual, :logsCambio, :response ";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':nombreArticulo', $this->nombreArticulo, PDO::PARAM_STR);
        $result->bindParam(':nombreImagen', $this->nombreImagen, PDO::PARAM_STR);
        $result->bindParam(':fechaDisponibilidad', $this->fechaDisponibilidad, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipActual', $this->ipActual, PDO::PARAM_STR);
        $result->bindParam(':idAgenciaActual', $this->idAgenciaActual, PDO::PARAM_INT);
        $result->bindParam(':logsCambio', $this->logCambios, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        if ($result->execute()) {
            if (($response == 'GUARDADO' || $response == 'ACTUALIZADO') && !empty($_FILES)) {
                if (file_exists('../images/articulos/' . $_FILES['imagen']["name"])) {
                    unlink('../images/articulos/' . $_FILES['imagen']["name"]);
                }
                move_uploaded_file($_FILES['imagen']["tmp_name"], '../images/articulos/' . $_FILES['imagen']["name"]);
            }
            $retorno = $response;
        } else {
            $retorno = "NO_EXCE";
        }
        $this->conection = null;
        return $retorno;
    }

    function eliminarArticulo()
    {
        $retorno = 'NO_EXCE';
        $response = null;
        $query = "EXEC promocoop_spEliminarArticulo :id, :idApartado, :tipoApartado, :idUsuario, :idAgenciaActual, :ipActual, :logsCambios, :response ";
        $result = $this->conection->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':idAgenciaActual', $this->idAgenciaActual, PDO::PARAM_INT);
        $result->bindParam(':ipActual', $this->ipActual, PDO::PARAM_STR);
        $result->bindParam(':logsCambios', $this->logCambios, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        if ($result->execute()) {
            $retorno = $response;
        }
        $this->conection = null;
        return $retorno;
    }

}