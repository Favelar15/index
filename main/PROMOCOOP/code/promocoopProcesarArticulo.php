<?php

include "../../code/generalParameters.php";
session_start();
$respuesta = (object)[];
$accion = null;
if (isset($_SESSION["index"])) {
    if (!empty($_GET)) {
        $accion = isset($_GET['accion']) ? $_GET['accion'] : "";
    } else if (isset($_POST['accion'])) {
        $accion = isset($_POST['accion']) ? $_POST['accion'] : "prueba";
    }

    include "../../code/connectionSqlServer.php";
    include './clases/articulo.php';
    $articulo = new Articulo();
    switch ($accion) {
        case 'guardarArticulo':

            $articulo->id = $_POST['id'];
            $articulo->nombreArticulo = $_POST['nombreArticulo'];
            $articulo->nombreImagen = $_POST['nombreImagen'];
            $articulo->idApartado = base64_decode(urldecode($_POST['idApartado']));
            $articulo->tipoApartado = $_POST['tipoApartado'];
            $articulo->fechaDisponibilidad = ($_POST['fechaDisponibilidad'] != null) ? date('Y-m-d', strtotime($_POST['fechaDisponibilidad'])) : null;
            $articulo->idUsuario = $_SESSION['index']->id;
            $articulo->idAgenciaActual = $_SESSION['index']->agenciaActual->id;

            $articulo->ipActual = generalObtenerIp();
            $articulo->logCambios = $_POST['logCambios'];

            $respuesta->{'respuesta'} = $articulo->guardarArticulo();
            break;
        case 'obtenerArticulosAdministracion':
            $respuesta = $articulo->obtenerArticulosAdministracion();
            break;

        case 'obtenerArticulosPorSellos':
            $articulo->cantidadSellos =  $_GET['cantidadSellos'];
            $respuesta = $articulo->obtenerArticulosPorSellos();
            break;

        case 'eliminarArticulo':
            $articulo->id = $_POST['id'];
            $articulo->idApartado = base64_decode(urldecode($_POST['idApartado']));
            $articulo->tipoApartado = $_POST['tipoApartado'];
            $articulo->idUsuario = $_SESSION['index']->id;
            $articulo->idAgenciaActual = $_SESSION['index']->agenciaActual->id;
            $articulo->ipActual = generalObtenerIp();
            $articulo->logCambios = "ELIMININADO%%%NO%%%SI";
            $respuesta->{'respuesta'} = $articulo->eliminarArticulo();
            break;
        default :
            $respuesta = $accion;
            break;
    }
} else {
    $respuesta->{'respuesta'} = "SESION";
}
echo json_encode($respuesta);