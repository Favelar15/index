<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";
session_start();
$respuesta = (object)[];
$respuesta->{'respuesta'} = "NO_ACCION";
$accion = "";
if ($_SESSION['index']) {
    if (!empty($_GET)) {
        $accion = isset($_GET['accion']) ? $_GET['accion'] : "";
    } else if (isset($input['accion'])) {
        $accion = isset($input['accion']) ? $input['accion'] : "prueba";
    }
    include "../../code/connectionSqlServer.php";
    include "./clases/bolsaTesoro.php";

    $bolsaTesoro = new bolsaTesoro();
    switch ($accion) {
        case 'obtenerConfiguracionesLBT':
            $respuesta->{'respuesta'} = $bolsaTesoro->obtenerConfiguracionesLBT();
            break;
        default:
            $respuesta->{'respuesta'} = "NO_ACCION";
            break;
    }

} else {
    $respuesta->{'respuesta'} = 'SESION';
}

echo json_encode($respuesta);

