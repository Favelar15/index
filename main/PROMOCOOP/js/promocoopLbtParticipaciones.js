var tblDetalleCanjes = null,
    valorSelloLBT = 0;
window.onload = () => {
    configBolsaTesoro.initializeDatatable().then(() => {
        formActions.clean('frmParticipacionesLBT').then(() => {
            $(".preloader").fadeOut("fast");
        })
    });
}
const configBolsaTesoro = {
    id: 0,
    txtCodigoCliente: document.getElementById('txtCodigoCliente'),
    txtAbono: document.getElementById('txtAbono'),
    txtSellosAntesTransaccion: document.getElementById('txtSellosAntesTransaccion'),
    txtSellosAplicables : document.getElementById('txtSellosAplicables'),
    btnAgregarArticulo: document.getElementById('btnAgregarArticulo'),
    btnBuscarDatosAsociado: document.getElementById('btnBuscarDatosAsociado'),
    btnGuardar: document.getElementById('btnGuardar'),

    buscarDatosAsociado: function () {
        return new Promise((resolve, reject) => {
            try {
                if (formActions.manualValidate('frmParticipacionesLBT')) {
                    fetchActions.set({
                        modulo: 'PROMOCOOP',
                        archivo: 'promocoopLbtPartipaciones',
                        datos: {
                            codigoCliente: this.txtCodigoCliente.value,
                            accion: 'consultarDatosAsociado'
                        }
                    }).then((datos) => {
                        if (Array.isArray(datos.respuesta)) {
                            fetchActions.set({
                                modulo: 'PROMOCOOP',
                                archivo: 'promocoopLBTConfiguracionesGenerales',
                                datos: {
                                    accion: 'obtenerConfiguracionesLBT'
                                }
                            }).then((configuraciones) => {
                                console.log();
                                console.log(datos.respuesta)
                                document.getElementById('txtNombres').value = datos.respuesta[0]['nombresAsociado']
                                document.getElementById('txtApellidos').value = datos.respuesta[0]['apellidosAsociado']
                                document.getElementById('txtEstatusAsociado').value = datos.respuesta[0]['estatus']
                                document.getElementById('txtEstatusAsociado').style.color = datos.respuesta[0]['estatus'] == 'ACTIVO' ? 'green' : "red";
                                document.getElementById('txtDeudaAportaciones').value = (datos.respuesta[0]['montoPendiente'] == null || datos.respuesta[0]['montoPendiente'] == 0) ? 0.00 : (Math.round(datos.respuesta[0]['montoPendiente'] * 100) / 100).toFixed(2);
                                document.getElementById('txtSellosAntesTransaccion').value = (datos.respuesta[0]['sellosDisponibles'] == null) ? 0 : datos.respuesta[0]['sellosDisponibles'];
                                document.getElementById('txtSellosAplicables').value = datos.respuesta[0]['sellosDisponibles'] == null ? 0 : datos.respuesta[0]['sellosDisponibles'];

                                this.txtCodigoCliente.readOnly = true;
                                this.txtAbono.readOnly = configuraciones.respuesta[0]['permitirAbonos'] == 'S' ? false : true;
                                this.btnBuscarDatosAsociado.disabled = true;
                                this.btnGuardar.disabled = false;
                                this.btnAgregarArticulo.disabled = configuraciones.respuesta[0]['permitirCanjes'] == 'S' ? false : true;
                                this.txtAbono.step = configuraciones.respuesta[0]['valorSello'];
                                valorSelloLBT = configuraciones.respuesta[0]['valorSello'];

                            }).catch(generalMostrarError);
                        } else {
                            if (datos.respuesta == 'NO_DATOS') {
                                toastr.info("No se econtró ningun dato", "Información")
                            } else {
                                console.log("aqui");
                                generalMostrarError(datos.respuesta);
                            }

                        }
                    }).catch((error) => {
                        generalMostrarError(error);
                    })

                } else {
                    toastr.warning("complete el código de cliente", "Atención");
                }
            } catch (e) {
                reject(e);
            }
        });
    },

    mdlMostrarArticulos: function () {
        if (this.txtSellosAplicables.value == 0 ){
            toastr.info("No posees sellos suficiente para realizar este proceso", "Atención");
        }else {
            fetchActions.get({
                modulo : 'PROMOCOOP',
                archivo : 'promocoopProcesarArticulo',
                params : {
                    accion : 'obtenerArticulosPorSellos',
                    cantidadSellos : this.txtSellosAplicables.value
                }
            }).then((articulos)=>{
                if (!articulos.respuesta) {
                    const opcionesCbo = [`<option selected disabled value="">seleccione</option>`];
                    articulos.forEach(articulo =>{
                        opcionesCbo.push(`<option value="${articulo.id}">${articulo.nombre} - ${articulo.sellos} </option>`);
                    });
                    let cboArticulo = document.querySelectorAll("select.cboArticuloLBT");
                    cboArticulo.forEach(cbo => {
                        cbo.innerHTML = opcionesCbo.join("");
                    });
                    $("#cboArticuloLBT").selectpicker("refresh");

                }
                $("#mdlPromocoopLBTMostrarArticulos").modal("show");
            }).catch(generalMostrarError);
        }
    },
    calcularSellosAplicables : function (){
        let montoAbona = this.txtAbono.value,
            sellosAntesTransaccion =  this.txtSellosAntesTransaccion.value,
            sellosAplicables = 0;
        sellosAplicables =  parseInt(sellosAntesTransaccion + (montoAbona / valorSelloLBT));

        this.txtSellosAplicables.value =  sellosAplicables;

    },

    cargarConfiguracionesLBT: function () {
        return new Promise((resolve, reject) => {
            fetchActions.get({
                modulo: 'PROMOCOOP',
                archivo: 'promocoopValidarConfiguracionesLBDT'

            }).then((resultado) => {
                console.log("configuraciones: " + resultado);
                resolve(resultado);

            }).catch(generalMostrarError);

        });

    },

    initializeDatatable: function () {
        return new Promise((resolve, reject) => {
            try {
                if ($("#tblDetalleCanjes").length) {
                    tblDetalleCanjes = $("#tblDetalleCanjes").DataTable({
                        drawCallBack: function () {
                            $("[data-toggle='tooltip']").tooltip("dispose");
                            $("[data-toggle='tooltip']").tooltip();
                        },
                        dateFormat: 'uk',
                        sortable: true,
                    });
                    tblDetalleCanjes.columns.adjust().draw();
                }
                resolve();

            } catch (error) {
                reject(error);
            }
        })

    }

}