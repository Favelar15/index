var tblAdministracionArticulos = "";
window.onload = () => {
    $(".preloader").fadeOut("fast");
    configAdministracionArticulos.initializeDatable().then(() => {
        configAdministracionArticulos.init();
    })
}

const configAdministracionArticulos = {
    id: 0,
    cnt: 0,
    articulos: {},
    nombreArticulo: document.getElementById('txtNombreArticulo'),
    campoImagen: document.getElementById('imgArticulo'),
    fechaDisponibilidad: document.getElementById('txtFechaLimite'),
    tmpIdArticulo: document.getElementById('hdnIdArticulo'),
    nombreImagenTmp: null,
    init: function () {
        fetchActions.get({
            modulo: 'PROMOCOOP',
            archivo: 'promocoopProcesarArticulo',
            params: {
                accion: 'obtenerArticulosAdministracion'
            }
        }).then((articulos) => {
            if (!articulos.respuesta) {
                let nFila = 1;
                articulos.forEach(articulo => {
                    this.id = articulo.id;
                    this.cnt = nFila;
                    this.articulos[this.id] = {
                        ...articulo
                    };
                    this.addRowTbl({nFila: this.cnt, ...articulo})
                    tblAdministracionArticulos.columns.adjust().draw();
                    nFila++;
                });


            } else {
                generalMostrarError(articulos);
            }
        }).catch((generalMostrarError));

    },
    save: function () {
        formActions.validate('frmDatosArticulos').then(({errores}) => {
            if (errores == 0) {
                this.id = document.getElementById('hdnIdArticulo').value;
                let datosArticulos = formActions.getJSON('frmDatosArticulos'),
                    camposRequeridos = document.querySelectorAll("#frmDatosArticulos .form-control");
                camposRequeridos = [...camposRequeridos, ...document.querySelectorAll("#frmDatosArticulos .form-select")];
                const {
                    contadorEditados,
                    camposEditados
                } = promocoopGenerarStringCambios(camposRequeridos);
                let imagen = (this.nombreImagenTmp != null) ? (this.campoImagen.files[0] == undefined) ? this.nombreImagenTmp : this.campoImagen.files[0].name : 'no_image.jpg';


                let datosGuardar = new FormData();
                datosGuardar.append('id', this.id);
                datosGuardar.append('imagen', (this.campoImagen.files[0] != undefined) ? this.campoImagen.files[0] : 'no_image.jpg');
                datosGuardar.append('nombreArticulo', this.nombreArticulo.value);
                datosGuardar.append('nombreImagen', imagen);
                datosGuardar.append('accion', 'guardarArticulo');
                datosGuardar.append('fechaDisponibilidad', this.fechaDisponibilidad.value);
                datosGuardar.append('idApartado', idApartado);
                datosGuardar.append('tipoApartado', tipoApartado);
                datosGuardar.append('logCambios', camposEditados);
                fetchActions.setWFiles({
                    modulo: 'PROMOCOOP',
                    archivo: 'promocoopProcesarArticulo',
                    datos: datosGuardar
                }).then((resultado) => {
                    console.log(resultado.respuesta);
                    switch (resultado.respuesta) {
                        case "GUARDADO":
                            promocoop_generalmostrarAlertas(resultado);
                            break;
                        case "ACTUALIZADO":
                            $("#trArticulo" + this.id).find("td:eq(1)").html(this.nombreArticulo.value);
                            $("#trArticulo" + this.id).find("td:eq(2)").html("<div class='contenedorImgTbl'>" +
                                "<img src='./main/PROMOCOOP/images/articulos/" + imagen + "' /> </div>");
                            $("#trArticulo" + this.id).find("td:eq(3)").html(this.fechaDisponibilidad.value);

                            let tmp = {
                                id: this.id,
                                nombre: this.nombreArticulo.value,
                                nombreImagen: imagen,
                                limiteDisponibilidad: this.fechaDisponibilidad.value,
                                fechaRegistro: $("#trArticulo" + this.id).find("td:eq(4)").html(),
                                fechaActualizacion: $("#trArticulo" + this.id).find("td:eq(5)").html(),
                            }
                            delete this.articulos[this.id];
                            this.articulos[this.id] = {
                              ...tmp
                            };

                            tblAdministracionArticulos.columns.adjust().draw();
                            $("#mdlPromocoopArticulo").modal("hide");
                            toastr.success("","Actualizado Correctamente")
                            break;
                        default:
                            generalMostrarError(resultado);
                            break;
                    }

                }).catch((generalMostrarError));
            }
        });
    },
    edit: function (id = 0) {
        this.id = id;
        if (id > 0) {
            let tmp = {
                ...this.articulos[this.id]
            };

            this.nombreArticulo.value = tmp.nombre;
            this.fechaDisponibilidad.value = tmp.limiteDisponibilidad;
            this.tmpIdArticulo.value = tmp.id;
            this.nombreImagenTmp = tmp.nombreImagen;
            let contenedor = $("#divContenedorImagen");
            contenedor.css('background-image', 'url("./main/PROMOCOOP/images/articulos/' + tmp.nombreImagen + '")');

            let todosCampos = document.querySelectorAll("#frmDatosArticulos .form-control");
            todosCampos = [...todosCampos, ...document.querySelectorAll("#frmDatosArticulos .form-select")];
            todosCampos.forEach(campo => {
                if (campo.id) {
                    if (campo.type == 'select-one') {
                        $("#" + campo.id).data("valorInicial", campo.options[campo.options.selectedIndex].text);
                    } else {
                        $("#" + campo.id).data("valorInicial", campo.value);
                    }

                    if (campo.className.includes("selectpicker")) {
                        $("#" + campo.id).selectpicker("refresh");
                    }
                }
            });
        }
        $("#mdlPromocoopArticulo").modal("show");
    },
    delete: function (id = 0) {
        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información ingresada.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO'
        }).then((result) => {
            if (result.isConfirmed) {
                let formdata = new FormData();
                this.id = id;
                formdata.append('id', this.id);
                formdata.append('accion', 'eliminarArticulo')
                formdata.append('idApartado', idApartado);
                formdata.append('tipoApartado', tipoApartado);
                fetchActions.setWFiles({
                    modulo: 'PROMOCOOP',
                    archivo: 'promocoopProcesarArticulo',
                    datos: formdata
                }).then((resultado) => {
                    if (resultado.respuesta == 'ELIMINADO') {
                        toastr.success("", "Articulo Eliminado Exitosamente");
                        tblAdministracionArticulos.row('#trArticulo' + id).remove().draw();
                    } else {
                        generalMostrarError(resultado);
                    }

                });
            }
        });
    },
    showModal: function () {
        formActions.clean('mdlPromocoopArticulo').then(() => {
            $("#mdlPromocoopArticulo").modal("show");
            this.tmpIdArticulo.value = 0;
        });
    },
    evalStatusImage: function () {
        if (this.campoImagen.files[0]) {
            let url = URL.createObjectURL(this.campoImagen.files[0]);
            let contenedor = $("#divContenedorImagen");
            contenedor.css('background-image', 'url("' + url + '")');
        }
    },
    deleteImage: function () {
        this.campoImagen.value = '';
        let contenedor = $("#divContenedorImagen");
        contenedor.css('background-image', 'url("./main/PROMOCOOP/images/articulos/no_image.jpg")');

    },
    evalDisponibilidadArticulo: function (value) {
        switch (value.trim()) {
            case 'S':
                document.getElementById('txtFechaLimite').value = '';
                document.getElementById('txtFechaLimite').disabled = false;
                document.getElementById('txtFechaLimite').required = true;
                break;
            case 'N':
                document.getElementById('txtFechaLimite').value = '';
                document.getElementById('txtFechaLimite').disabled = true;
                document.getElementById('txtFechaLimite').required = false;
                break;
        }
    },
    addRowTbl({nFila, id, nombre, nombreImagen, limiteDisponibilidad, fechaRegistro, fechaActualizacion}) {
        return new Promise((resolve, reject) => {
            try {
                tblAdministracionArticulos.row.add([
                    nFila,
                    nombre,
                    "<div class='contenedorImgTbl'>" +
                    "<img src='./main/PROMOCOOP/images/articulos/" + nombreImagen + "' />" +
                    "</div>",

                    limiteDisponibilidad,
                    fechaRegistro,
                    fechaActualizacion,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' data-toggle='tooltip' onclick='configAdministracionArticulos.edit(" + id + ")'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' data-toggle='tooltip' onclick='configAdministracionArticulos.delete(" + id + ")'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"
                ]).node().id = "trArticulo" + id;
                resolve();
            } catch (e) {
                reject(e.message);
            }

        });
    },
    initializeDatable: function () {
        return new Promise((resolve, reject) => {
            try {
                let alturaTabla = 300;
                if ($("#tblAdministracionArticulos").length) {
                    tblAdministracionArticulos = $("#tblAdministracionArticulos").DataTable({
                        drawCallBack: function () {
                            $("[data-toggle='tooltip']").tooltip("dispose");
                            $("[data-toggle='tooltip']").tooltip();
                        },
                        dateFormat: 'uk',
                        sortable: true,
                    });
                    tblAdministracionArticulos.columns.adjust().draw();
                }
                if ($("#tblInclusionPromociones").length) {
                    tblAdministracionArticulos = $("#tblInclusionPromociones").DataTable({
                        drawCallBack: function () {
                            $("[data-toggle='tooltip']").tooltip("dispose");
                            $("[data-toggle='tooltip']").tooltip();
                        },
                        dateFormat: 'uk',
                        sortable: true,
                    });
                    tblAdministracionArticulos.columns.adjust().draw();
                }
                resolve();
            } catch (e) {
                reject(e.message);
            }
        });

    }
}