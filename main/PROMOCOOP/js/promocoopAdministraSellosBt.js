var tblAdministracionSellos = "";
window.onload = () => {
    $(".preloader").fadeOut("fast");
    configAdministacionSellos.initializeDatatable().then(() => {
        configAdministacionSellos.init();
    });

}
const configAdministacionSellos = {
    id: 0,
    idArticulo: 0,
    cnt: 0,
    tmpNombreArticulo: null,
    nombreImagen: null,
    tmpNombreImagen: null,
    sellos: document.getElementById('numCantidadSellos'),
    articulosSellos: {},
    init: function () {
        fetchActions.get({
            modulo: 'PROMOCOOP',
            archivo: 'promocoopConfigSellosArticulos',
            params: {
                accion: 'obtenerArticulosConSellos'
            }
        }).then((articulos) => {
            if (!articulos.respuesta) {
                let nFila = 1;
                articulos.forEach(articulo => {
                    this.id = articulo.id;
                    this.cnt = nFila;
                    this.articulosSellos[this.id] = {
                        ...articulo
                    };
                    this.addRowTbl({nFila: this.cnt, ...articulo})
                    tblAdministracionSellos.columns.adjust().draw();
                    nFila++;
                });
            } else {
                generalMostrarError(articulos);
            }

        }).catch(generalMostrarError);
    },
    save: function () {
        if (formActions.manualValidate('frmConfigSellos')) {
            let datos = formActions.getJSON('frmConfigSellos'),
                todosCampos = document.querySelectorAll('#frmConfigSellos .form-control');
            todosCampos = [...todosCampos, ...document.querySelectorAll("#frmConfigSellos .form-select")];
            const {contadorEditados, camposEditados} = promocoopGenerarStringCambios(todosCampos);
            fetchActions.set({
                modulo: 'PROMOCOOP',
                archivo: 'promocoopConfigSellosArticulos',
                datos: {
                    id: this.id,
                    accion: 'guardarConfiguracion',
                    ...datos,
                    idApartado: idApartado,
                    tipoApartado: tipoApartado,
                    logsCambios: camposEditados
                }
            }).then((resultado) => {
                switch (resultado.respuesta) {
                    case 'GUARDADO':
                        Swal.fire({
                            title: "¡Registro Exitoso!",
                            text: "El registro ha guardado satisfactoriamente",
                            icon: "success"
                        }).then(function () {
                            self.location.reload();
                        });
                        break;
                    case 'ACTUALIZADO':
                        console.log("actuaalizado");
                        $("#trArticuloConSello" + this.id).find("td:eq(3)").html(this.sellos.value);

                        this.articulosSellos[this.id].sellos =  this.sellos.value;


                        tblAdministracionSellos.columns.adjust().draw();
                        $("#mdlPromocoopConfigSellos").modal("hide");
                        toastr.success("","Actualizado Correctamente")
                        break;
                    default:
                        promocoop_generalmostrarAlertas();
                        break;
                }

            }).catch(promocoop_generalmostrarAlertas);


        } else {
            console.log("nel perro")
        }
    },
    edit: function (id = 0) {
        this.id = id;
        if (id > 0) {
            let tmp = {
                ...this.articulosSellos[this.id]
            };
            this.id =  tmp.id;
            this.idArticulo = tmp.idArticulo;
            this.sellos.value = tmp.sellos;
            this.tmpNombreArticulo = tmp.nombreArticulo;
            this.tmpNombreImagen = tmp.nombreImagen;
            let contenedor = $("#divContenedorImagen");
            contenedor.css('background-image', 'url("./main/PROMOCOOP/images/articulos/' + tmp.nombreImagen + '")');

            let todosCampos = document.querySelectorAll("#frmConfigSellos .form-control");
            todosCampos = [...todosCampos, ...document.querySelectorAll("#frmConfigSellos .form-select")];
            todosCampos.forEach(campo => {
                if (campo.id) {
                    if (campo.type == 'select-one') {
                        $("#" + campo.id).data("valorInicial", campo.options[campo.options.selectedIndex].text);
                    } else {
                        $("#" + campo.id).data("valorInicial", campo.value);
                    }

                    if (campo.className.includes("selectpicker")) {
                        $("#" + campo.id).selectpicker("refresh");
                    }
                }
            });
        }
        let opcionesCbo =[`<option selected disabled value="${this.articulosSellos[this.id].id}">${this.articulosSellos[this.id].nombreArticulo}</option>`];
        let cboArticulo = document.querySelectorAll("select.cboArticuloSellos");
        cboArticulo.forEach(cbo => {
            cbo.innerHTML = opcionesCbo.join("");
        });
        $("#cboArticuloSellos").selectpicker("refresh");
        $("#mdlPromocoopConfigSellos").modal("show");
    },
    delete: function (id = 0) {
        Swal.fire({
            title: "¿Estás seguro/a?",
            html: "No se podrá recuperar la información ingresada.",
            icon: "warning",
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO'
        }).then((result) => {
            if (result.isConfirmed) {
                this.id = id;

                fetchActions.set({
                    modulo: 'PROMOCOOP',
                    archivo: 'promocoopConfigSellosArticulos',
                    datos :{
                        id: this.id,
                        idApartado: idApartado,
                        tipoApartado: tipoApartado,
                        accion: 'eliminarConfiguracionSellos'
                    }
                }).then((resultado) => {
                    if (resultado.respuesta == 'ELIMINADO') {
                        toastr.success("", "Articulo Eliminado Exitosamente");
                        tblAdministracionSellos.row('#trArticuloSellos' + id).remove().draw();
                    } else {
                        generalMostrarError(resultado);
                    }

                });
            }
        });
    },
    addRowTbl: function ({nFila, id, idArticulo, nombreArticulo, nombreImagen, sellos, limiteDisponibilidad}) {
        return new Promise((resolve, reject) => {
            try {
                tblAdministracionSellos.row.add([
                    nFila,
                    nombreArticulo,
                    "<div class='contenedorImgTbl'>" +
                    "<img src='./main/PROMOCOOP/images/articulos/" + nombreImagen + "' />" +
                    "</div>",
                    sellos,
                    limiteDisponibilidad,
                    "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' data-toggle='tooltip' onclick='configAdministacionSellos.edit(" + id + ", " + idArticulo + ")'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' data-toggle='tooltip' onclick='configAdministacionSellos.delete(" + id + ")'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>"

                ]).node().id = 'trArticuloConSello' + id;
                resolve();
            } catch (error) {
                reject(error.message);
            }
        })
    },
    evalArtciculoSelected: function (id = 0) {

        if (id.length > 0) {
            let contenedor = $("#divContenedorImagen");
            contenedor.css('background-image', 'url("./main/PROMOCOOP/images/articulos/' + this.articulosSellos[id].nombreImagen + '")');
        }

    },
    showModal: function () {
        formActions.clean('mdlPromocoopConfigSellos').then(() => {
            $(".preloader").fadeIn("fast");
            fetchActions.get({
                modulo: 'PROMOCOOP',
                archivo: 'promocoopConfigSellosArticulos',
                params: {
                    accion: 'obtenerArticulosSinSellosCbo'
                }
            }).then((resultado) => {
                return new Promise((resolve, reject) => {
                    const opcionesCbo = [`<option selected disabled value="">seleccione</option>`];
                    if (!resultado.respuesta) {
                        resultado.forEach(articulo => {
                            this.articulosSellos[articulo.id] = {
                                ...articulo
                            };
                            opcionesCbo.push(`<option value="${articulo.id}">${articulo.nombreArticulo}</option>`)
                        });
                        let cboArticulo = document.querySelectorAll("select.cboArticuloSellos");
                        cboArticulo.forEach(cbo => {
                            cbo.innerHTML = opcionesCbo.join("");
                        });
                        $("#cboArticuloSellos").selectpicker("refresh");
                    }
                    resolve()
                }).then(() => {
                    $(".preloader").fadeOut("fast");
                     this.id = 0;
                    $("#divContenedorImagen").css('background-image', 'url("./main/PROMOCOOP/images/articulos/no_image.jpg")');
                    $("#mdlPromocoopConfigSellos").modal("show");
                });
            }).catch(generalMostrarError);

        });

    },
    initializeDatatable: function () {
        return new Promise((resolve, reject) => {
            try {
                if ($("#tblAdministracionSellos").length) {
                    tblAdministracionSellos = $("#tblAdministracionSellos").DataTable({
                        drawCallBack: function () {
                            $("[data-toggle='tooltip']").tooltip("dispose");
                            $("[data-toggle='tooltip']").tooltip();
                        },
                        dateFormat: 'uk',
                        sortable: true,
                    });
                    tblAdministracionSellos.columns.adjust().draw();
                }
                resolve();

            } catch (error) {
                reject(error.message);
            }
        })
    }

}