function promocoopGenerarStringCambios(arregloCampos) {
    let respuesta = {
        contadorEditados: 0,
        camposEditados: ""
    };
    let tmpEditados = [];


    arregloCampos.forEach(campo => {
        if (typeof campo.id != "undefined" && campo.id.length > 0) {

            let campoActual = campo.type == 'select-one' ? campo.options[campo.selectedIndex].text : $("#" + campo.id).val();

            if (campoActual != $("#" + campo.id).data("valorInicial")) {

                let valorAnterior = $("#" + campo.id).data("valorInicial") == null || $("#" + campo.id).data("valorInicial").length == 0 ? "No definido" : $("#" + campo.id).data("valorInicial"),
                    valorActual = "",
                    tituloCampo = $("#" + campo.id).parent().find("label").length > 0 ? $("#" + campo.id).parent().find("label").text() : ($("#" + campo.id).parent().parent().find("label").length > 0 ? $("#" + campo.id).parent().parent().find("label").text() : "Indefinido");
                if (campo.type == 'select-one') {
                    valorActual = campo.options[campo.selectedIndex].text;
                } else {
                    valorActual = $("#" + campo.id).val();
                }
                tituloCampo = (tituloCampo.replace("*", "")).trim();
                tmpEditados.push(tituloCampo + "$$$" + valorAnterior + "$$$" + valorActual);
                respuesta.contadorEditados ++;
            }
        }
    });
    respuesta.camposEditados = tmpEditados.length > 0 ? tmpEditados.join("%%%") : "";

    return respuesta;
}

function promocoop_generalmostrarAlertas(mensaje) {
    $(".preloader").fadeOut("fast");
    let mensajeFinal =  (mensaje['respuesta'] != null) ? mensaje['respuesta'] : (mensaje.respuesta != null) ? mensaje.respuesta : ""
    switch (mensajeFinal) {
        case 'EXITO':
            Swal.fire({
                title: "¡Registro Exitoso!",
                text: "El registro ha sido completado satisfactoriamente",
                icon: "success"
            }).then(function () {
                self.location.reload();
            });
            break;
            case 'GUARDADO':
            Swal.fire({
                title: "¡Registro Exitoso!",
                text: "El registro ha guardado satisfactoriamente",
                icon: "success"
            }).then(function () {
                self.location.reload();
            });
            break;
        case 'EXISTE':
            Swal.fire({
                title: "¡Registro Existente!",
                text: "Al parecer los datos que ingresaste ya existen en nuestras bases de datos",
                icon: "info"
            });
            break;
        case 'LLENAR_CAMPOS':
            Swal.fire({
                title: "¡Complete todos los campos!",
                html: "<h4>Completa los campos marcados con <span style='color:red;'>*</span></h4>",
                icon: "info"
            });
            break;
        case 'ACTUALIZADO':
            Swal.fire({
                title: "¡Registro Actualizado!",
                text: "El registro ha sido actualizado existosamente",
                icon: "success"
            }).then(function () {
                window.location.href = `page=promocoopAdministrarArticulos&mod=UFJPTU9DT09Q`;
            });
            break;

        case 'NO_USUARIO':
            Swal.fire({
                title: "¡Usuario no válido!",
                text: "No puedes realizar la transacción con este usuario",
                icon: "error"
            }).then(function () {
                window.location.reload();
            });
            break;
        case 'ELIMINADO':
            Swal.fire({
                title: "¡Registro Eliminado!",
                text: "Se ha eliminado el registro exitosamente",
                icon: "success"
            }).then(function () {
                window.location.reload();
            });
            break;

        default:
           generalMostrarError(mensaje);
           break;
    }
}
