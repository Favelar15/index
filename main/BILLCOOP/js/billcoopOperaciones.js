
let tblOperaciones
const modulo = 'BILLCOOP'

window.onload = () => {

    initDataTable().then( () => {
        return new Promise( (resolve, reject ) => {
            fetchActions.set({
                modulo, archivo:'billcoopObtenerOperaciones', datos:{ idApartado, tipoApartado }
            }).then( ( {operaciones } ) => {

                operaciones.forEach( operacion => {
                    configOperaciones.contador++
                    configOperaciones.operaciones[configOperaciones.contador] = {
                        ...operacion
                    }
                    configOperaciones.agregarFila( {contador: configOperaciones.contador, ...operacion})
                })
                tblOperaciones.columns.adjust().draw()

                resolve()

            }).catch( reject )
        })
        $('.preloader').fadeOut('fast')
    })
}


const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblOperaciones').length > 0) {
                tblOperaciones = $('#tblOperaciones').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            className: 'text-center',
                            width: '8%'
                        },
                        {
                            targets: [6],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblOperaciones.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const configOperaciones = {
    contador: 0,
    operaciones : {},
    agregarFila( { contador, nombres, tipoDocumento, numeroDocumento, persona, agencia, monto, edicion, eliminacion })
    {
        let btneditar = ``
        let eliminar = ``
        let imprimir = ``

        if ( edicion =='S' && tipoPermiso == 'ESCRITURA')
        {
            btneditar = `<button type="button" class="btnTbl" onclick="configOperaciones.editar(${contador})"><i class="fas fa-edit"></i></button>`
        }

        if ( eliminacion == 'S' && tipoPermiso == 'ESCRITURA')
        {
            eliminar = `<button type="button" class="btnTbl" onclick="configOperaciones.eliminar(${contador})"><i class="fas fa-trash"></i></button>`
        }

        if (tipoPermiso == 'ESCRITURA')
        {
             imprimir = `<button type="button" class="btnTbl" onclick="construirDeclracionPDF(${contador})"><i class="fas fa-print"></i></button>`
        } else {
            btneditar = `<h6><span class="badge bg-primary">Modo solo lectura</span></h6>`
        }

        tblOperaciones.row.add([
            contador, nombres, `${tipoDocumento} - ${numeroDocumento}`, persona, agencia, `$${monto}`,
            `<div class="tblButtonContainer">${btneditar} ${imprimir} ${eliminar}</div>`
        ]).node().id = `trOperacion${contador}`
    },
    editar( contador )
    {
        if ( tipoPermiso == 'ESCRITURA')
        {
            const idOperacion = this.operaciones[contador].id
            window.location = `?page=billcoopFormulario&mod=${btoa(modulo)}&idOperacion=${btoa(idOperacion)}`
        } else {
            toastr.warning(`Modo solo lectura, no tiene permiso para editar la decaración`)
        }
    },
    eliminar( idFila )
    {
        const operacion = this.operaciones[idFila];

        Swal.fire({
            title: 'IMPORTANTE',
            text: `¿Quieres anular la declaración a nombre de ${operacion.nombres}, realizada el ${operacion.fechaRegistro}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            confirmButtonText: `<i class="fas fa-thumbs-up"></i> Eliminar`,
            cancelButtonText: `<i class="fas fa-thumbs-up"></i> Cancelar`
        }).then( result => {

            if (result.isConfirmed) {
                fetchActions.set({
                    modulo, archivo: 'billcoopEliminarOperacion',
                    datos : {
                        idApartado, tipoApartado, idOperacion: operacion.id
                    }
                }).then( response => {

                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                html: 'Se anulo correctamente la activación',
                                showConfirmButton: false,
                                timer: 1500
                            }).then( () => {
                                delete this.operaciones[idFila]
                                tblOperaciones.row(`#trOperacion${idFila}`).remove().draw(false)
                            });
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }
                }).catch( generalMostrarError )
            }
        }).catch()
    },
    crearDeclaracion()
    {
        if ( tipoPermiso == 'ESCRITURA')
        {
            window.location = `?page=billcoopFormulario&mod=${btoa(modulo)}`
        } else {
            toastr.warning(`Modo solo lectura, no tiene permiso para editar la decaración`)
        }
    }
}


const construirDeclracionPDF = ( contador ) => {

    const idOperacion = configOperaciones.operaciones[contador].id;

    fetchActions.get({
        modulo, archivo: 'billcoopConstruirDeclaracionPDF', params : { idOperacion }
    }).then( response => {

        switch (response.respuesta.trim()) {
            case 'EXITO':
                userActions.abrirArchivo(modulo, response.nombreArchivo, 'declaraciones', 'S');
                break;
            default:
                generalMostrarError(response);
                break;
        }
    }).catch( generalMostrarError)

}
