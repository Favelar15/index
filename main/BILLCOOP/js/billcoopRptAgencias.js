
let tblAgencias, tblTipoProdcuto
const modulo = 'BILLCOOP'

window.onload = () => {

    const cboAgencia = document.getElementById('cboAgencia')

    document.getElementById('formRptAgencia').reset()

    const inicializando = initDataTable().then( () => {
        return new Promise( (resolve, reject ) => {
            try {

                fetchActions.getCats({
                    modulo, archivo: 'billcoopGetCats', solicitados: ['agenciasAsignadas']
                }).then( ( { agenciasAsignadas = [] }) => {

                    let opciones = [`<option selected  disabled value="">Seleccione</option>`]

                    opciones.push(`<option value="all">Todas las agencias</option>`)

                    agenciasAsignadas.forEach( agencia => {
                        if ( agencia.id != 700 )
                        {
                            opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                        }
                    })

                    cboAgencia.innerHTML = opciones.join('')
                    $(`#${cboAgencia.id}`).selectpicker('refresh')

                }).catch( reject )

                resolve()

            } catch (e) {
                reject(e.message)
            }
        })
    })

    inicializando.then ( () => {
        $('.preloader').fadeOut('fast')
    })

}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblAgencias').length > 0) {
                tblAgencias = $('#tblAgencias').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [2,3,4,5,6],
                            className: 'text-center',
                        },
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblAgencias.columns.adjust().draw();
            }

            if ($('#tblTipoProdcuto').length > 0) {
                tblTipoProdcuto = $('#tblTipoProdcuto').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [2,3,4,5,6],
                            className: 'text-center',
                        },
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblTipoProdcuto.columns.adjust().draw();
            }

            resolve()

        } catch (e) {
            reject(e.message)
        }
    })
}


const configRptAgencia = {
    contador: 0,
    agencias: {},
    btnExcel:  document.getElementById('btnExportar'),
    btnExportarProductos: document.getElementById('btnExportarProductos'),
    txtInicio: document.getElementById('txtInicio'),
    txtFin: document.getElementById('txtFin'),
    cboAgencia : document.getElementById('cboAgencia'),
    generar()
    {
        formActions.validate('formRptAgencia')
            .then( ( { errores } ) => {

                if ( errores == 0 )
                {
                    tblAgencias.clear()
                    tblTipoProdcuto.clear()

                    this.contador = 0
                    this.agencias = {}

                    fetchActions.set({
                        modulo, archivo:'Reporte/billcoopRptAgencia', datos: {
                            ...formActions.getJSON('formRptAgencia'), accion: 'GENERAR'
                        }
                    }).then( ( { consolidados: { agencias, productos } } ) => {

                        if ( Object.keys(agencias).length > 0 && Object.keys(productos).length > 0 )
                        {
                            this.btnExcel.classList.remove('disabled')

                            this.txtInicio.disabled = true
                            this.txtFin.disabled = true
                            this.cboAgencia.disabled = true
                            $(`#cboAgencia`).selectpicker('refresh')

                            for ( const key in agencias )
                            {
                                this.contador++
                                this.agencias[key] = { ...agencias[key] }
                                this.agregarFila({
                                    contador: this.contador, ...agencias[key]
                                })
                            }

                            for ( const key in productos )
                            {
                                configProductos.contador++
                                configProductos.productos[key] = {
                                    ...productos[key]
                                }
                                configProductos.agregarFila({
                                    contador: configProductos.contador, ...productos[key]
                                })
                            }

                        } else
                        {
                            toastr.warning(`No hay datos para mostrar`)
                        }

                        tblAgencias.columns.adjust().draw()
                        tblTipoProdcuto.columns.adjust().draw()

                    }).catch( generalMostrarError )
                }
            })
    },
    exportar()
    {
        if ( Object.keys(this.agencias).length > 0 )
        {
            fetchActions.set({
                modulo, archivo: 'Reporte/billcoopRptAgencia', datos: {
                    ...formActions.getJSON('formRptAgencia'),
                    accion: 'EXPORTAR', agencias: { ...this.agencias }, productos: configProductos.productos
                }
            }).then( response => {

                switch ( response.respuesta.trim() )
                {
                    case 'EXITO':
                        window.open('./main/BILLCOOP/docs/Excel/' + response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }

            }).catch( generalMostrarError )
        } else
        {
            toastr.warning(`No hay datos para exportar`)
        }
    },
    agregarFila( { contador, agencia, billetesCincuenta, billetesCien, totalCincuenta, totalCien } )
    {
        tblAgencias.row.add([
            contador, agencia, billetesCincuenta,
            `$${ new Intl.NumberFormat().format(parseFloat(totalCincuenta).toFixed(2)) }`,
            billetesCien, `$${ new Intl.NumberFormat().format(parseFloat(totalCien).toFixed(2)) }`,
            `$${  new Intl.NumberFormat().format( parseFloat(Number(totalCincuenta) + Number(totalCien) ).toFixed(2)) }`
        ]).node().id = `trAgencia${contador}`
    },
    limpiar()
    {
        formActions.clean('formRptAgencia')
            .then( () => {

                this.contador = 0
                tblAgencias.clear().draw()
                this.agencias = {}

                tblTipoProdcuto.clear().draw()
                configProductos.contador = 0
                configProductos.productos = {}

                this.btnExcel.classList.add('disabled')
                this.txtInicio.disabled = false
                this.txtFin.disabled = false

                this.cboAgencia.disabled = false
                $(`#cboAgencia`).selectpicker('refresh')

            }).catch( generalMostrarError )
    },
    cambioFecha()
    {
        $("#txtFin").datepicker('destroy')
        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        document.getElementById('txtFin').value = ''
        $("#txtFin").datepicker('update')
    }
}

const configProductos = {
    productos: {},
    contador: 0,
    agregarFila( { contador, tipoProducto,  billetesCincuenta, billetesCien, totalCincuenta, totalCien } )
    {
        tblTipoProdcuto.row.add([
            contador, tipoProducto, billetesCincuenta,
            `$${ new Intl.NumberFormat().format(parseFloat(totalCincuenta).toFixed(2)) }`,
            billetesCien,
            `$${ new Intl.NumberFormat().format(parseFloat(totalCien).toFixed(2)) }`,
            `$${  new Intl.NumberFormat().format( parseFloat(Number(totalCincuenta) + Number(totalCien) ).toFixed(2)) }`
        ]).node().id = `trProducto${contador}`
    }
}