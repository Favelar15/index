
let tblOperaciones
const modulo = 'BILLCOOP'

window.onload = () => {

    const cboAgencia = document.getElementById('cboAgencia')

    const inicializando = initDataTable().then( () => {
        return new Promise( ( resolve, reject ) => {
            try {

                fetchActions.getCats({
                    modulo, archivo: 'billcoopGetCats', solicitados: ['agenciasAsignadas']
                }).then( ( { agenciasAsignadas = [] }) => {

                    let opciones = [`<option disabled selected value="">Seleccione</option>`]

                    opciones.push(`<option value="all">Todas las agencias</option>`)

                    agenciasAsignadas.forEach( agencia => {
                        if ( agencia.id != 700 )
                        {
                            opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                        }
                    })

                    cboAgencia.innerHTML = opciones.join('')
                    $(`#${cboAgencia.id}`).selectpicker('refresh')

                }).catch( reject )

                resolve()

            } catch (e) {
                reject(e.message)
            }
        })
    })

    inicializando.then( () => {
        $('.preloader').fadeOut('fast')
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblOperaciones').length > 0) {
                tblOperaciones = $('#tblOperaciones').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [5,6],
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [4],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblOperaciones.columns.adjust().draw();
            }


            resolve()

        } catch (e) {
            reject(e.message)
        }
    })
}

const configRptGeneral = {
    contador: 0,
    operaciones: {},
    btnExcel: document.getElementById('btnExportar'),
    txtInicio : document.getElementById('txtInicio'),
    txtFin: document.getElementById('txtFin'),
    cboAgencia: document.getElementById('cboAgencia'),
    generar()
    {
        formActions.validate('formRptGeneral')
            .then( ( { errores } ) => {

                if ( errores == 0)
                {
                    fetchActions.set({
                        modulo, archivo: 'Reporte/billcoopRptGeneral', datos: {
                            ...formActions.getJSON('formRptGeneral'),
                            accion: 'GENERAR'
                        }
                    }).then( ( { operaciones } ) => {

                        tblOperaciones.clear()
                        this.contador = 0
                        this.operaciones = {}

                        if ( operaciones.length == 0 )
                        {
                            toastr.warning(`No hay registros para mostrar`)
                        } else
                        {
                            this.btnExcel.classList.remove('disabled')
                            this.txtInicio.readOnly = true
                            this.txtFin.readOnly = true

                            this.cboAgencia.disabled = true
                            $(`#${this.cboAgencia.id}`).selectpicker('refresh')

                            operaciones.forEach( operacion => {
                                this.contador++
                                this.operaciones[this.contador] = { ...operacion }
                                this.agregarFila({
                                    contador: this.contador, ...operacion
                                })
                            })
                        }

                        tblOperaciones.columns.adjust().draw()
                    }).catch( generalMostrarError )
                }
            }).catch( generalMostrarError )
    },
    exportar()
    {
        if ( Object.keys(this.operaciones).length > 0 )
        {
            fetchActions.set({
                modulo, archivo: 'Reporte/billcoopRptGeneral', datos: {
                    accion: 'EXPORTAR',
                    operaciones: { ...this.operaciones },
                    ...formActions.getJSON('formRptGeneral')
                }
            }).then( response => {

                switch ( response.respuesta.trim() )
                {
                    case 'EXITO':
                        window.open('./main/BILLCOOP/docs/Excel/' + response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }

            }).catch( generalMostrarError )
        } else
        {
            toastr.warning(`No hay datos para exportar`)
        }
    },
    agregarFila( { contador, nombres, apellidos, tipoDocumento, numeroDocumento, agencia, monto, billetesCincuenta, billetesCien, fechaRegistro } )
    {
        tblOperaciones.row.add([
            contador, `${nombres} ${apellidos}`, `${tipoDocumento} / ${numeroDocumento}`, agencia, `$${parseFloat(monto).toFixed(2)}`,
            billetesCincuenta, billetesCien, fechaRegistro
        ]).node().id = `trOperaciones${contador}`
    },
    limpiar()
    {
        formActions.clean('formRptGeneral')
            .then( () => {

                this.btnExcel.classList.add('disabled')
                this.txtInicio.readOnly = false
                this.txtFin.readOnly = false
                this.cboAgencia.disabled = false
                $(`#${this.cboAgencia.id}`).selectpicker('refresh')

                tblOperaciones.clear().draw()
                this.contador = 0
                this.operaciones = {}
            })
    },
    cambioFecha()
    {
        $("#txtFin").datepicker('destroy')
        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        this.txtFin.value = ''
        $("#txtFin").datepicker('update')
    }
}