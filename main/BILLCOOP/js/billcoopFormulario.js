let tblTitulares, tblBilletes
const modulo = 'BILLCOOP'

window.onload = () => {

    const cboTipoProducto = document.getElementById('cboTipoProducto')
    const cboPais = document.getElementById('cboPais')
    const cboTipoDocumento = document.querySelectorAll('select.tipoPrimario')
    const cboActividadEconomica = document.querySelectorAll('select.cboActividadEconomica')


    const inicializando = initDataTable().then(() => {
        return new Promise((resolve, reject) => {
            try {

                fetchActions.getCats({
                    modulo,
                    archivo: 'billcoopGetCats',
                    solicitados: ['productos', 'tiposDocumento', 'actividadEconomica', 'geograficos']
                }).then(({
                             productos = [],
                             tiposDocumento: documentos,
                             datosGeograficos: geograficos,
                             actividadesEconomicas = []
                         }) => {

                    let opciones = [`<option value="" selected disabled>Seleccione</option>`]

                    productos.forEach(producto => {
                        configFormulario.contador++
                        configFormulario.catProductos[configFormulario.contador] = {...producto}
                        opciones.push(`<option value="${producto.id}">${producto.tipoProducto}</option>`)
                    })

                    cboTipoProducto.innerHTML = opciones.join()
                    $(`#${cboTipoProducto.id}`).selectpicker('refresh')


                    if (cboTipoDocumento.length > 0) {

                        opciones = [`<option value="" selected disabled>Seleccione</option>`]

                        documentos.forEach(documento => {
                            tiposDocumento[documento.id] = {
                                ...documento
                            }

                            if (documento.tipoDocumento != 'NIT') {
                                opciones.push(` <option value="${documento.id}">${documento.tipoDocumento}</option> `)
                            }
                        });

                        cboTipoDocumento.forEach(cbo => {
                            cbo.innerHTML = opciones.join('');
                            $(`#${cbo.id}`).selectpicker('refresh');
                        })

                    }

                    if (cboPais) {
                        datosGeograficos = {
                            ...geograficos
                        };

                        opciones = ['<option selected value="" disabled>Seleccione</option>'];

                        for (let key in geograficos) {
                            opciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
                        }


                        cboPais.innerHTML = opciones.join('');
                        $(`#${cboPais.id}`).selectpicker('refresh');

                    }

                    if (cboActividadEconomica.length > 0) {
                        opciones = [`<option value="" selected disabled >Seleccione</option>`]

                        actividadesEconomicas.forEach(actividad => {
                            opciones.push(`<option value="${actividad.id}">${actividad.actividadEconomica}</option>`)
                        })

                        cboActividadEconomica.forEach(cbo => {
                            cbo.innerHTML = opciones.join()
                            $(`#${cbo.id}`).selectpicker('refresh')
                        })
                    }

                    resolve()
                }).catch(reject)

            } catch (e) {
                reject(e.message)
            }
        })
    })

    inicializando.then(editar)
}

const editar = () => {
    return new Promise((resolve, reject) => {
        try {
            const id = atob(getParameterByName('idOperacion'))

            if (id && parseInt(id) > 0) {
                fetchActions.set({
                    modulo, archivo: 'billcoopObtenerOperaciones', datos: {
                        idOperacion: id
                    }
                }).then(({operacion = {}}) => {


                    document.getElementById('frmAsociado').action = 'javascript:configFormulario.guardar();';

                    const cboTipoDocumento = document.getElementById('cboTipoDocumento')
                    cboTipoDocumento.value = operacion.idTipoDocumento

                    document.getElementById('txtDocumentoPrimario').value = operacion.numeroDocumento

                    document.getElementById('txtCodigoCliente').value = operacion.codigoCliente

                    document.getElementById('txtNombres').value = operacion.nombres
                    document.getElementById('txtApellidos').value = operacion.apellidos

                    document.getElementById('txtTelefonoFijo').value = operacion.telefonoFijo;
                    document.getElementById('txtTelefonoMovil').value = operacion.telefonoMovil;

                    document.getElementById('cboActividadEconomica').value = operacion.idActividadEconomica
                    document.getElementById('cboPais').value = operacion.idPais;
                    $("#cboPais").trigger("change");
                    document.getElementById('cboDepartamento').value = operacion.idDepartamento
                    $("#cboDepartamento").trigger("change");
                    document.getElementById('cboMunicipio').value = operacion.idMunicipio
                    document.getElementById('txtDireccion').value = operacion.direccion

                    configFormulario.habilitarFormulario('frmAsociado', false)
                    cboTipoDocumento.disabled = true

                    configFormulario.idOperacion = operacion.id

                    document.getElementById('cboTipoProducto').value = operacion.idProducto

                    document.getElementById('txtTipoOperacion').value = operacion.tipoOperacion
                    document.getElementById('txtProcedencia').value = operacion.procedencia
                    document.getElementById('txtMonto').value = operacion.monto
                    document.getElementById('txtMontoRecibido').value = operacion.montoRecibido

                    document.getElementById('txtCincuenta').value = operacion.billetesCincuenta
                    document.getElementById('txtCien').value = operacion.billetesCien
                    $('#txtCien').trigger('change')

                    configFormulario.changeTipoProducto(document.getElementById('cboTipoProducto').value)
                        .then( () => {

                            document.getElementById('txtNumeroCuenta').value = operacion.numeroCuenta

                            if ( operacion.tipoProducto != 'Envío de remesa') {

                                document.getElementById('cboEsTitular').value = operacion.isTitular

                                if (operacion.isTitular == 'N') {

                                    document.getElementById('cboTipoDocumentoTitular').disabled = true
                                    document.getElementById('cboTipoDocumentoTitular').value = operacion.TidTipoDocumento
                                    $(`#cboTipoDocumentoTitular`).selectpicker('refresh')

                                    document.getElementById('txtDocumentoPrimarioTitular').value = operacion.TnumeroPrimario
                                    document.getElementById('txtCodigoClienteTitular').value = operacion.TcodigoCliente

                                    const nombresT = document.getElementById('txtNombresTitular')
                                    nombresT.value = operacion.Tnombres
                                    nombresT.readOnly = false

                                    const apellidosT = document.getElementById('txtApellidosTitular')
                                    apellidosT.value = operacion.Tapellidos
                                    apellidosT.readOnly = false

                                    document.getElementById('cboActividadEconomicaTitular').value = operacion.TidActividadEconomica
                                    document.getElementById('cboActividadEconomicaTitular').disabled = false
                                    $(`#cboActividadEconomicaTitular`).selectpicker('refresh')

                                    document.getElementById('cboEsTitular').value = operacion.isTitular

                                } else {
                                    $("#cboEsTitular").trigger("change");
                                }
                            } else
                            {
                                document.getElementById('txtNombreBeneficiario').value = operacion.beneficiario
                                document.getElementById('txtPaisDestino').value = operacion.paisDestino
                                document.getElementById('txtDestino').value = operacion.destino
                            }

                            let cbos = document.querySelectorAll('form#frmAsociado select.selectpicker')
                            cbos = [...cbos, ...document.querySelectorAll('form#formTransaccion select.selectpicker')]

                            cbos.forEach(cbo => {
                                $(`#${cbo.id}`).selectpicker('refresh')
                            })

                            configFormulario.habilitarFormulario('formTransaccion', false)
                            logsFormulario.setLogs('formTransaccion')
                            logsFormulario.setLogs('frmAsociado')
                            logsFormulario.setLogs('formTitular')
                            logsFormulario.setLogs('formBeneficiario')



                        }).catch( console.error )

                    resolve()

                }).catch(reject)
            }
            resolve()

        } catch (e) {
            reject(e.message)
        }
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblTitulares').length > 0) {
                tblTitulares = $('#tblTitulares').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0, 1],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [4],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblTitulares.columns.adjust().draw();
            }

            if ($('#tblBilletes').length > 0) {
                tblBilletes = $('#tblBilletes').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [1, 2, 3],
                            "orderable": true,
                            className: 'text-center',
                        },
                        {
                            "targets": [0],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblBilletes.columns.adjust().draw();
            }
            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const buscardoPersonas = (cboTipoDocumento, txtDocumentoPrimario) => {
    return new Promise((resolve, reject) => {
        try {

            fetchActions.set({
                modulo, archivo: 'billcoopBuscarPersona', datos: {
                    tipoDocumento: cboTipoDocumento.value,
                    numeroDocumento: txtDocumentoPrimario.value
                }
            }).then((response) => {

                resolve(response)

            }).catch(() => {
                throw new Error()
            })
        } catch (e) {
            reject(e.message)
        }
    })
}

const buscarPersona = {
    btnBuscarPersona: document.getElementById('btnBuscarPersona'),
    btnBuscarTitular: document.getElementById('btnBuscarTitular'),
    cboTipoDocumento: document.getElementById('cboTipoDocumento'),
    buscarPersona() {
        const {
            cboTipoDocumento, txtDocumentoPrimario
        } = formActions.getJSON('frmAsociado')

        if (cboTipoDocumento.value && txtDocumentoPrimario.value) {
            buscardoPersonas(cboTipoDocumento, txtDocumentoPrimario).then(({respuesta}) => {

                const {existe, Persona} = respuesta

                document.getElementById('frmAsociado').action = 'javascript:configFormulario.guardar();';

                if (existe) {
                    configFormulario.limpiar().then(() => {

                        this.btnBuscarPersona.classList.add('disabled')


                        document.getElementById('txtCodigoCliente').value = Persona.codigoCliente
                        document.getElementById('cboTipoDocumento').value = Persona.idTipoDocumento ? Persona.idTipoDocumento : Persona.tipoDocumentoPrimario
                        document.getElementById('txtDocumentoPrimario').value = Persona.numeroDocumento ? Persona.numeroDocumento : Persona.numeroDocumentoPrimario

                        document.getElementById('txtNombres').value = Persona.nombres ? Persona.nombres : Persona.nombresAsociado;
                        document.getElementById('txtApellidos').value = Persona.apellidos ? Persona.apellidos : Persona.apellidosAsociado;

                        document.getElementById('txtTelefonoFijo').value = Persona.telefonoFijo;
                        document.getElementById('txtTelefonoMovil').value = Persona.telefonoMovil;

                        document.getElementById('cboActividadEconomica').value = Persona.actividadPrimaria;

                        document.getElementById('cboPais').value = Persona.pais;
                        $("#cboPais").trigger("change");
                        document.getElementById('cboDepartamento').value = Persona.departamento;
                        $("#cboDepartamento").trigger("change");
                        document.getElementById('cboMunicipio').value = Persona.municipio;
                        document.getElementById('txtDireccion').value = Persona.direccionCompleta;

                        configFormulario.habilitarFormulario('frmAsociado', false)
                        configFormulario.habilitarFormulario('formTransaccion', false)

                        this.cboTipoDocumento.disabled = true
                        $(`#${this.cboTipoDocumento.id}`).selectpicker('refresh')

                        logsFormulario.setLogs('frmAsociado')

                    })
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Información del sistema',
                        html: `No se encontro una personas con el número de ${cboTipoDocumento.labelOption} [ <strong>${txtDocumentoPrimario.value}</strong> ]; Procede a llenar el formulario manual`,
                    }).then(() => {
                        configFormulario.limpiar().then(() => {
                            document.getElementById('cboTipoDocumento').value = cboTipoDocumento.value
                            document.getElementById('txtDocumentoPrimario').value = txtDocumentoPrimario.value
                            document.getElementById('txtDocumentoPrimario').readOnly = true
                            document.getElementById('cboTipoDocumento').disabled = true

                            configFormulario.habilitarFormulario('frmAsociado', false)
                            configFormulario.habilitarFormulario('formTransaccion', false)
                            $(`#cboTipoDocumento`).selectpicker('refresh')
                        })
                    })
                }

            }).catch(generalMostrarError)
        } else {
            toastr.warning(`Tipo de documento y número de documento son invalidos`)
        }
    },
    selectTipoDocumento(tipoDocumento) {
        this.btnBuscarPersona.classList.remove('disabled')
        generalMonitoreoTipoDocumento(tipoDocumento, 'txtDocumentoPrimario')
    },
    selectTipoDocumentoTitular(tipoDocumento) {
        if (tipoDocumento !== '') {
            this.btnBuscarTitular.classList.remove('disabled')
            generalMonitoreoTipoDocumento(tipoDocumento, 'txtDocumentoPrimarioTitular')
        }
    },
    buscarTitular() {
        const {
            cboTipoDocumentoTitular, txtDocumentoPrimarioTitular
        } = formActions.getJSON('formTitular')

        if (cboTipoDocumentoTitular.value && txtDocumentoPrimarioTitular.value) {
            buscardoPersonas(cboTipoDocumentoTitular, txtDocumentoPrimarioTitular)
                .then(({respuesta}) => {

                    const {existe, Persona} = respuesta

                    if (existe) {
                        formActions.clean('formTitular')
                            .then(() => {

                                document.getElementById('txtCodigoClienteTitular').value = Persona.codigoCliente
                                document.getElementById('cboTipoDocumentoTitular').value = Persona.idTipoDocumento ? Persona.idTipoDocumento : Persona.tipoDocumentoPrimario
                                document.getElementById('txtDocumentoPrimarioTitular').value = Persona.numeroDocumento ? Persona.numeroDocumento : Persona.numeroDocumentoPrimario
                                document.getElementById('cboTipoDocumentoTitular').disabled = true
                                document.getElementById('txtDocumentoPrimarioTitular').readOnly = true
                                document.getElementById('btnBuscarTitular').classList.add('disabled')

                                document.getElementById('txtCodigoClienteTitular').value = Persona.codigoCliente
                                document.getElementById('txtNombresTitular').value = Persona.nombres ? Persona.nombres : Persona.nombresAsociado;
                                document.getElementById('txtApellidosTitular').value = Persona.apellidos ? Persona.apellidos : Persona.apellidosAsociado;
                                document.getElementById('cboActividadEconomicaTitular').value = Persona.actividadPrimaria;

                                logsFormulario.setLogs('formTitular')

                                $(`#cboTipoDocumentoTitular`).selectpicker('refresh')
                                $(`#cboActividadEconomicaTitular`).selectpicker('refresh')


                            }).catch(generalMostrarError)
                    } else {

                        Swal.fire({
                            icon: 'warning',
                            title: 'Información del sistema',
                            html: `No se encontro una personas con el número de ${cboTipoDocumento.labelOption} [ <strong>${txtDocumentoPrimario.value}</strong> ]; Procede a llenar los datos del titular manualmente`,
                        }).then(() => {
                            formActions.clean('formTitular').then(() => {
                                document.getElementById('cboTipoDocumentoTitular').value = cboTipoDocumentoTitular.value
                                document.getElementById('txtDocumentoPrimarioTitular').value = txtDocumentoPrimarioTitular.value
                                document.getElementById('txtDocumentoPrimarioTitular').readOnly = true
                                document.getElementById('cboTipoDocumentoTitular').disabled = true

                                document.getElementById('txtNombresTitular').readOnly = false
                                document.getElementById('txtApellidosTitular').readOnly = false
                                document.getElementById('cboActividadEconomicaTitular').disabled = false

                                $(`#cboTipoDocumentoTitular`).selectpicker('refresh')
                                $(`#cboActividadEconomicaTitular`).selectpicker('refresh')
                            })
                        })

                    }

                }).catch(generalMostrarError)
        } else {
            toastr.warning(`Tipo de documento y número de documento son invalidos`)
        }
    }
}

const configFormulario = {
    catProductos: {},
    contador: 0,
    idOperacion: 0,
    guardar() {

        let formularioValido = true

        let datosOperacionBillcoop = {}

        if (formActions.manualValidate('frmAsociado')) {
            if (formActions.manualValidate('formTransaccion')) {
                if (formActions.manualValidate('formTitular') && document.getElementById('txtNombresTitular').value ) {
                    datosOperacionBillcoop = {
                        persona: formActions.getJSON('frmAsociado'),
                        operacion: {idOperacion: this.idOperacion, ...formActions.getJSON('formTransaccion'), ...formActions.getJSON('formBeneficiario')},
                        titular: {...formActions.getJSON('formTitular')}
                    }

                    if (datosOperacionBillcoop.operacion.cboTipoProducto.labelOption === 'Envío de remesa') {
                        if (!formActions.manualValidate('formBeneficiario')) {
                            toastr.warning(`¡Complete los datos del beneficiario de la remesa!`)
                            formularioValido = false
                            document.getElementById('contenedorTitular').scrollIntoView({behavior: "smooth", block: "center", inline: "end"})
                        }
                    }
                } else
                {
                    formularioValido = false
                    toastr.warning(`¡Complete los datos del titular!`)
                    document.getElementById('contenedorTitular').scrollIntoView({behavior: "smooth", block: "center", inline: "end"})
                }
            } else {
                formularioValido = false
                document.getElementById('formTransaccion').scrollIntoView({behavior: "smooth", block: "center", inline: "end"})
                toastr.warning(`¡Datos de la operación incompletos!`)
            }
        } else {
            formularioValido = false
            toastr.warning('¡Complete los datos de la persona que realizar fisicamente la operación!')
        }


        if (formularioValido) {
            if (parseInt(configBilletes.billetes['total'].total) > 0) {

                const montoOperación = Number(datosOperacionBillcoop.operacion.txtMonto.value)
                const montoRecibido = Number(datosOperacionBillcoop.operacion.txtMontoRecibido.value)

                if (montoOperación > montoRecibido) {
                    toastr.warning(`El monto de la operación no puede ser mayor al monto recibido`)
                    document.getElementById('formTransaccion').scrollIntoView({behavior: "smooth", block: "center", inline: "end"})
                    document.getElementById('txtMontoRecibido').focus({preventScroll: true})

                } else if (montoRecibido < parseInt(configBilletes.billetes['total'].total)) {
                    toastr.warning(`Total de monto por denominación es mayor al monto recibido`)
                    //document.getElementById('txtMontoRecibido').focus()
                    document.getElementById('formTransaccion').scrollIntoView({behavior: "smooth", block: "center", inline: "end"})
                    document.getElementById('txtMontoRecibido').focus({preventScroll: true})
                } else {
                    fetchActions.set({
                        modulo, archivo: 'billcoopGuardarOperacion', datos: {
                            idApartado, tipoApartado,
                            billetes: {...datosOperacionBillcoop, ...configBilletes.billetes},
                            logsPersona: [...logsFormulario.getLogs('frmAsociado')],
                            logsTransaccion: [...logsFormulario.getLogs('formTransaccion'), ...logsFormulario.getLogs('formBeneficiario')]
                        }
                    }).then(response => {
                        switch (response.respuesta.trim()) {
                            case 'EXITO':

                                Swal.fire({
                                    title: "¡Bien hecho!",
                                    html: "Operación registrada con exito.<br />¿Desea salir del formulario?",
                                    icon: "success",
                                    showCancelButton: true,
                                    showDenyButton: true,
                                    focusConfirm: false,
                                    allowOutsideClick: false,
                                    confirmButtonText: '<i class="fa fa-edit"></i> Nueva operación',
                                    cancelButtonText: '<i class="fa fa-print"></i> Imprimir declaración',
                                    denyButtonText: '<i class="fa fa-times"></i> Salir del formulario',
                                }).then(result => {

                                    if (result.isDenied) {
                                        window.location = `?page=billcoopOperaciones&mod=${btoa('BILLCOOP')}`
                                    } else if (!result.isConfirmed) {
                                        construirDeclracionPDF(response.id)
                                    } else {
                                        window.location = `?page=billcoopFormulario&mod=${btoa('BILLCOOP')}`
                                    }
                                })
                                break;
                            default:
                                generalMostrarError(response);
                                break;
                        }
                    })
                }

            } else {
                document.getElementById('tblBilletes').scrollIntoView({behavior: "smooth", block: "center", inline: "end"})
                document.getElementById('txtCincuenta').focus({preventScroll: true})
                //document.getElementById('txtCincuenta').focus()
                toastr.warning(`Ingrese el detalle de billetes de alta denominación`)
            }
        }

        // if (formActions.manualValidate('frmAsociado'))
        // {
        //     if (formActions.manualValidate('formTransaccion'))
        //     {
        //         if ( formActions.manualValidate('formTitular') )
        //         {
        //             if ( parseInt( configBilletes.billetes['total'].total ) > 0 )
        //             {
        //                 const datosOperacionBillcoop = {
        //                     persona: formActions.getJSON('frmAsociado'),
        //                     operacion: { idOperacion: this.idOperacion, ...formActions.getJSON('formTransaccion')},
        //                     titular: formActions.getJSON('formTitular')
        //                 }
        //
        //                 const montoOperación = Number(datosOperacionBillcoop.operacion.txtMonto.value)
        //                 const montoRecibido = Number(datosOperacionBillcoop.operacion.txtMontoRecibido.value)
        //
        //                 if ( montoOperación > montoRecibido )
        //                 {
        //                     toastr.warning(`El monto de la operación no puede ser mayor al monto recibido`)
        //                     document.getElementById('txtMontoRecibido').focus()
        //
        //                 } else if ( montoRecibido < parseInt( configBilletes.billetes['total'].total ) )
        //                 {
        //                     toastr.warning(`Total de monto por denominación es mayor al monto recibido`)
        //                     document.getElementById('txtMontoRecibido').focus()
        //                 } else {
        //

        //
        //                 }
        //             } else {
        //                 toastr.warning(`Revise el número de billestes  ingresado`)
        //                 document.getElementById('txtCincuenta').focus()
        //             }
        //         }
        //     }
        // }
    },
    salir() {
        Swal.fire({
            title: '¿Estas seguro de cancelar el registro?',
            text: "¡ Los datos del formulario serán eliminados !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            cancelButtonColor: '#f54343',
            confirmButtonText: `<i class="fas fa-sign-out-alt"></i> Salir`,
            cancelButtonText: '<i class="fas fa-trash-alt"></i> Limpiar'
        }).then((result) => {
            if (result.isConfirmed) {
                window.location = `?page=billcoopOperaciones&mod=${btoa('BILLCOOP')}`

            } else if (result.dismiss === Swal.DismissReason.cancel) {
                window.location = `?page=billcoopFormulario&mod=${btoa('BILLCOOP')}`
            }
        })
    },
    limpiar() {
        return new Promise((resolve, reject) => {

            const limpiandoFormAsociado = formActions.clean('frmAsociado')
            const limpiandoFormTransaccion = formActions.clean('formTransaccion')
            const limpiarFormTitular = formActions.clean('formTitular')

            const txtCincuenta = document.getElementById('txtCincuenta')
            const txtCien = document.getElementById('txtCien')

            txtCincuenta.value = ''
            txtCien.value = ''
            $(`#${txtCien.id}`).trigger('change')

            this.habilitarFormulario('frmAsociado')
            this.habilitarFormulario('formTransaccion')
            this.habilitarFormulario('formTitular')


            buscarPersona.btnBuscarPersona.classList.add('disabled')
            buscarPersona.cboTipoDocumento.disabled = false
            $(`#${buscarPersona.cboTipoDocumento.id}`).selectpicker('refresh')

            Promise.all([limpiandoFormAsociado, limpiandoFormTransaccion, limpiarFormTitular])
                .then(resolve).catch(reject)
        })
    },
    txtNumeroCuenta: document.getElementById('txtNumeroCuenta'),
    changeTipoProducto(tipoProducto) {
        return new Promise((resolve, reject) => {
            try {

                if (tipoProducto.length > 0) {
                    this.txtNumeroCuenta.value = ''

                    let isReadOnly = false;

                    if (this.catProductos[tipoProducto].numeroCuenta == 'S') {
                        if (this.catProductos[tipoProducto].tipo === 'PRODUCTO') {
                            $("#txtNumeroCuenta").mask('000-000-00000000000000000');
                        } else {
                            $("#txtNumeroCuenta").unmask();
                        }
                    } else {
                        isReadOnly = true
                    }
                    this.txtNumeroCuenta.readOnly = isReadOnly


                    const limpiandoTitular = formActions.clean('formTitular')
                    const limpiandoBeneficiario = formActions.clean('formBeneficiario')

                    Promise.all([limpiandoTitular, limpiandoBeneficiario])
                        .then(() => {

                            if (this.catProductos[tipoProducto].tipoProducto === 'Envío de remesa') {
                                document.getElementById('titular-tab').classList.add('disabled')
                                document.getElementById('beneficiario-tab').classList.remove('disabled')
                                $(`#beneficiario-tab`).trigger('click')

                                document.getElementById('cboEsTitular').value = 'N'
                                document.getElementById('cboEsTitular').disabled = true
                                //$(`#cboEsTitular`).selectpicker('refresh')
                                configTitular.isTitular('S')

                            } else {
                                document.getElementById('beneficiario-tab').classList.add('disabled')
                                document.getElementById('titular-tab').classList.remove('disabled')
                                $(`#titular-tab`).trigger('click')

                                if( !configFormulario.idOperacion )
                                {
                                    document.getElementById('cboEsTitular').value = ''
                                }
                                document.getElementById('cboEsTitular').disabled = false
                            }
                        })
                }

                resolve()

            } catch (e) {
                reject(e.message)
            }
        })
    },
    seleccionTipoProducto(tipoProducto) {

        this.changeTipoProducto(tipoProducto).then(() => {

        })
    },
    habilitarFormulario(idFormulario, estado = true) {
        let campos = document.querySelectorAll(`form#${idFormulario} .form-control`)

        campos.forEach(campo => {
            if (campo.id) {
                if (campo.type == 'select-one') {
                    campo.disabled = estado
                    if (campo.className.includes('selectpicker')) {
                        $(`#${campo.id}`).selectpicker('refresh')
                    }
                } else {
                    if (campo.id !== 'txtDocumentoPrimario' && campo.id !== 'txtCodigoCliente' && campo.id !== 'txtNumeroCuenta') {
                        campo.readOnly = estado
                    }
                }
            }
        });

    }
}

const configTitular = {
    isTitular(isTitular) {

        const valido = formActions.manualValidate('frmAsociado')

        if (valido) {
            if (isTitular == 'S') {

                //console.log('Si es el titular')

                document.getElementById('btnBuscarTitular').classList.add('disabled')
                this.habilitar(isTitular)

                const {
                    txtCodigoCliente,
                    cboTipoDocumento,
                    txtDocumentoPrimario,
                    txtNombres,
                    txtApellidos,
                    cboActividadEconomica
                } = formActions.getJSON('frmAsociado')

                document.getElementById('txtCodigoClienteTitular').value = txtCodigoCliente.value
                document.getElementById('cboTipoDocumentoTitular').value = cboTipoDocumento.value
                document.getElementById('txtDocumentoPrimarioTitular').value = txtDocumentoPrimario.value
                document.getElementById('txtNombresTitular').value = txtNombres.value
                document.getElementById('txtApellidosTitular').value = txtApellidos.value
                document.getElementById('cboActividadEconomicaTitular').value = cboActividadEconomica.value

            } else if (isTitular != '') {
                this.habilitar(isTitular, false)
            }
        } else if (isTitular != '') {
            cboTitular = document.getElementById('cboEsTitular')
            cboTitular.value = ''
            toastr.warning('Completa los datos de la persona que realiza la operación')
        }

        $(`#cboActividadEconomicaTitular`).selectpicker('refresh')
        $(`#cboTipoDocumentoTitular`).selectpicker('refresh')

        const form = document.getElementById('frmAsociado')

        if (form.className.includes('was-validated')) {
            form.classList.remove('was-validated')
        }
    },
    habilitar(isTitular, estado = true) {
        formActions.clean('formTitular')
            .then(() => {

                const cboTipoDocumento = document.getElementById('cboTipoDocumentoTitular')
                const campos = document.querySelectorAll('form#formTitular .form-control')

                if (isTitular == "S") {
                    campos.forEach(campo => {
                        if (campo.id) {
                            if (campo.type == 'select-one') {
                                campo.disabled = estado

                                $(`#${campo.id}`).selectpicker('refresh')

                            } else {
                                campo.readOnly = estado
                            }
                        }
                    })
                } else {
                    cboTipoDocumento.disabled = false
                    $(`#${cboTipoDocumento.id}`).selectpicker('refresh')
                }
            })
    }
}

const configBilletes = {
    txt50: document.getElementById('txtCincuenta'),
    txt100: document.getElementById('txtCien'),
    billetes: {50: {cantidad: 0, total: 0.0}, 100: {cantidad: 0, total: 0.0}, total: {total: 0}},
    valorBilletes() {
        const fila1 = parseFloat(this.txt50.value == '' ? 0 : this.txt50.value * 50).toFixed(2)
        const fila2 = parseFloat(this.txt100.value == '' ? 0 : this.txt100.value * 100).toFixed(2)

        this.billetes[50].cantidad = this.txt50.value == '' ? 0 : this.txt50.value
        this.billetes[50].total = fila1

        this.billetes[100].cantidad = this.txt100.value == '' ? 0 : this.txt100.value
        this.billetes[100].total = fila2
        this.billetes['total'].total = parseFloat(Number(fila1) + Number(fila2)).toFixed(2)

        $("#trBilletes50").find("td:eq(3)").html(`$${fila1}`);
        $("#trBilletes100").find("td:eq(3)").html(`$${fila2}`);
        $("#trBilletesTotal").find("td:eq(3)").html(`$${parseFloat(Number(fila1) + Number(fila2)).toFixed(2)}`);

    }
}

const logsFormulario = {
    setLogs(idFormulario) {
        let campos = document.querySelectorAll(`form#${idFormulario} .form-control`);

        if (idFormulario === 'formTransaccion') {
            campos = [...campos, document.getElementById("txtCincuenta"), document.getElementById("txtCien")];
        }

        campos.forEach(campo => {
            if (campo.id) {
                if (campo.type == 'select-one') {
                    const value = campo.value ? campo.options[campo.selectedIndex].text : ''
                    $("#" + campo.id).data("valorInicial", value);
                } else {
                    $("#" + campo.id).data("valorInicial", campo.value);
                }
            }
        });
    },
    getLogs(idFormulario) {
        let camposEditados = [];
        let campos = document.querySelectorAll(`form#${idFormulario} .form-control`);

        if (idFormulario === 'formTransaccion') {
            campos = [...campos, document.getElementById("txtCincuenta"), document.getElementById("txtCien")];
        }

        campos.forEach(campo => {
            if (campo.id) {
                let valorActual = campo.type == 'select-one' ? campo.options[campo.selectedIndex].text : $("#" + campo.id).val();

                if ($("#" + campo.id).data("valorInicial") || $("#" + campo.id).data("valorInicial") == '') {
                    let valorAnterior = $("#" + campo.id).data("valorInicial");

                    if (valorActual != valorAnterior) {

                        let label

                        if (campo.id !== 'txtCincuenta' && campo.id !== 'txtCien') {
                            label = $("#" + campo.id).parent().find("label").length > 0 ? $("#" + campo.id).parent().find("label:eq(0)").text() : ($("#" + campo.id).parent().parent().parent().find("label").length > 0 ? $("#" + campo.id).parent().parent().parent().find("label:eq(0)").text() : 'undefined');
                            label = (label.replace("*", "")).trim();
                        } else {
                            if (campo.id === 'txtCincuenta') {
                                label = 'Billetes de Cincuenta'
                            }

                            if (campo.id === 'txtCien') {
                                label = 'Billetes de Cien'
                            }
                        }
                        camposEditados.push({
                            campo: label,
                            valorAnterior,
                            valorNuevo: valorActual
                        })
                    }
                }
            }
        });
        return camposEditados;
    }
}

const construirDeclracionPDF = (idOperacion) => {

    fetchActions.get({
        modulo, archivo: 'billcoopConstruirDeclaracionPDF', params: {idOperacion}
    }).then(response => {

        switch (response.respuesta.trim()) {
            case 'EXITO':
                userActions.abrirArchivo(modulo, response.nombreArchivo, 'declaraciones', 'S');
                break;
            default:
                generalMostrarError(response);
                break;
        }
    }).catch(generalMostrarError)

}


