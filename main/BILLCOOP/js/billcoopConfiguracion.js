const modulo = 'BILLCOOP'
let tblConfiguracion

window.onload = () => {

    const inicializando =  initDataTable().then( () => {
        return new Promise( ( resolve, reject ) => {
            try {
                fetchActions.getCats({
                    modulo, archivo: 'billcoopGetCats', solicitados: ['roles', 'motivosRetiro', 'rolesCbo']
                }).then( ( {  roles = [] } ) => {
                    configRoles.catRoles = [ ...roles ]

                    resolve()

                }).catch( reject )
            } catch (e) {
                reject( e.message )
            }
        })
    })

    inicializando.then( () => {
        fetchActions.get({
            modulo, archivo: 'billcoopObtenerConfiguracion', params: {}
        }).then( ( { configuracion = [] } ) => {
            configuracion.forEach( config => {
                configRoles.contador++
                configRoles.roles[configRoles.contador] = { ...config }
                configRoles.agregarFila({
                    contador:configRoles.contador, ...config
                })
            })

            tblConfiguracion.columns.adjust().draw()

        }).catch( generalMostrarError )


        $('.preloader').fadeOut('fast')
    }).catch( generalMostrarError )
}
const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblConfiguracion').length > 0) {
                tblConfiguracion = $('#tblConfiguracion').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            className: 'text-center',
                            width: '8%'
                        },
                        {
                            targets: [2,3,4],
                            className: 'text-center',
                            width: '15%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblConfiguracion.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const configRoles = {
    contador : 0,
    idFila: 0,
    roles: {},
    catRoles: {},
    cboRoles: document.getElementById('cboRoles'),
    cboEdicion: document.getElementById('cboEdicion'),
    cboEliminacion: document.getElementById('cboEliminacion'),
    mostrarModal( idFila = 0)
    {
        this.idFila = idFila

        formActions.clean('formConfiguracion')
            .then( () => {

                this.cboRoles.disabled = false
                let opciones = [`<option selected disabled value="">Seleccione</option>`]
                let idsIncluidos = []

                if (this.idFila == 0) {
                    for (const key in this.roles) {
                        idsIncluidos.push(this.roles[key].id)
                    }
                }

                this.catRoles.forEach( rol => {
                    if (!idsIncluidos.includes( rol.id ) ) {
                        opciones.push(`<option value="${rol.id}">${rol.rol}</option>`)
                    }
                })

                this.cboRoles.innerHTML = opciones.join('')
                $(`#${this.cboRoles.id}`).selectpicker('refresh')
                //this.cboRoles.value = 1

                if ( this.idFila > 0 )
                {
                    this.cboRoles.value =  `${this.roles[this.idFila].id}`
                    this.cboRoles.disabled = true
                    this.cboEdicion.value = this.roles[this.idFila].edicion
                    this.cboEliminacion.value = this.roles[this.idFila].eliminacion
                }

                $(`#${this.cboRoles.id}`).selectpicker('refresh')
                $(`#${this.cboEdicion.id}`).selectpicker('refresh')
                $(`#${this.cboEliminacion.id}`).selectpicker('refresh')

                $('#modal-configuracion').modal('show')

            }).catch( generalMostrarError )
    },
    agregarFila( { contador, rol, edicion, eliminacion } )
    {
        const btnEditar = `<button type="button" class="btnTbl" onclick="configRoles.mostrarModal(${contador})"><i class="fas fa-edit"></i></button>`
        const btnEliminar = `<button type="button" class="btnTbl" onclick="configRoles.eliminar(${contador})"><i class="fas fa-times-circle"></i></button>`

        edicion  =  edicion == 'S' ? 'SI':'NO'
        eliminacion  =  eliminacion == 'S' ? 'SI':'NO'

        tblConfiguracion.row.add([
            contador, rol, edicion, eliminacion,
            `<div class="tblButtonContainer">${btnEditar} ${btnEliminar}</div>`
        ]).node().id = `trRol${contador}`
    },
    agregar()
    {
        formActions.validate('formConfiguracion')
            .then( ( { errores } ) => {

                if ( errores == 0)
                {
                    const {
                        cboRoles: {
                            value: id,
                            labelOption: rol
                        },
                        cboEdicion: { value: edicion },
                        cboEliminacion : { value: eliminacion }
                    } = formActions.getJSON('formConfiguracion')

                    if ( this.idFila == 0)
                    {
                        this.contador++
                        this.roles[this.contador] = { id, rol, edicion, eliminacion }
                        this.agregarFila({
                            contador: this.contador, ...this.roles[this.contador]
                        })
                        tblConfiguracion.columns.adjust().draw()
                    } else {

                        this.roles[this.idFila].edicion = edicion
                        this.roles[this.idFila].eliminacion = eliminacion

                        this.editar(this.idFila, {   ...this.roles[this.idFila] })
                    }

                    $(`#modal-configuracion`).modal('hide')
                }
            }).catch( generalMostrarError )
    },
    editar( contador, { edicion, eliminacion} )
    {
        edicion  =  edicion == 'S' ? 'SI':'NO'
        eliminacion  =  eliminacion == 'S' ? 'SI':'NO'

        $(`#trRol${contador}`).find('td:eq(2)').html(edicion)
        $(`#trRol${contador}`).find('td:eq(3)').html(eliminacion)
    },
    eliminar( contador )
    {
        const datosRol = this.roles[contador]

        Swal.fire({
            title: 'IMPORTANTE',
            html: ` ¿Quieres eliminar el rol : <strong>${datosRol.rol}</strong> de la lista de permisos?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            confirmButtonText: `<i class="fas fa-thumbs-up"></i> Eliminar`,
            cancelButtonText: `<i class="fas fa-thumbs-down"></i> Cancelar`
        }).then( result => {

            if ( result.isConfirmed )
            {
                tblConfiguracion.row(`#trRol${contador}`).remove().draw(false);
                delete this.roles[contador];
            }
        })
    },
    guardar()
    {
        if ( Object.keys( this.roles).length == 0 )
        {
            Swal.fire({
                title: 'IMPORTANTE',
                html: `No se agregaron roles para <strong>edición y eliminación de declaraciones BILLCOOP</strong>, nadie podra editar ni anular <br> ¿Quieres guardar la configuración?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#46b1c3',
                confirmButtonText: `<i class="fas fa-save"></i> Guardar`,
                cancelButtonText: `<i class="fas fa-times-circle"></i> Cancelar`
            }).then( result => {

                if ( result.isConfirmed )
                {
                    this.save()
                }
            })
        } else
        {
            this.save()
        }

    },
    save()
    {
        fetchActions.set({
            modulo, archivo: 'billcoopGuardarConfiguracion', datos : {
                idApartado, tipoApartado, roles: { ...this.roles }
            }
        }).then( response => {

            switch (response.respuesta.trim()) {
                case 'EXITO':
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: '¡Bien hecho!',
                        html: 'Se actualizó correctamente la configuración',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(() => {
                        window.location = `?page=billcoopConfiguracion&mod=${btoa('BILLCOOP')}`
                    });
                    break;
                default:
                    generalMostrarError(response);
                    break;
            }
        })
    },
    cancelar()
    {
        Swal.fire({
            title: 'IMPORTANTE',
            html: `¿Quieres cancelar el proceso y cargos los datos iniciales?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            confirmButtonText: `<i class="fas fa-thumbs-up"></i> SI`,
            cancelButtonText: `<i class="fas fa-thumbs-down"></i> NO`
        }).then( result => {

            if ( result.isConfirmed )
            {
                window.location = `?page=billcoopConfiguracion&mod=${btoa(modulo)}`
            }
        })


    }
}