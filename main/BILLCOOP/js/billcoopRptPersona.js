let tblOperaciones, tblBilletes
const modulo = 'BILLCOOP'

window.onload = () => {

    const cboTipoDocumento = document.querySelectorAll('select.tipoPrimario')

    document.getElementById('formRptPersona').reset()

    const inicializando = initDataTable().then( () => {
        return new Promise( ( resolve, reject ) => {
            try {

                fetchActions.getCats({
                    modulo,
                    archivo: 'billcoopGetCats',
                    solicitados: ['tiposDocumento']
                }).then( ( { tiposDocumento: documentos }) => {

                    let opciones = [`<option value="" selected disabled>Seleccione</option>`]

                    if (cboTipoDocumento.length > 0) {

                        documentos.forEach(documento => {
                            tiposDocumento[documento.id] = {
                                ...documento
                            }

                            if (documento.tipoDocumento != 'NIT') {
                                opciones.push(` <option value="${documento.id}">${documento.tipoDocumento}</option> `)
                            }
                        });

                        cboTipoDocumento.forEach(cbo => {
                            cbo.innerHTML = opciones.join('');
                            $(`#${cbo.id}`).selectpicker('refresh');
                        })
                    }

                }).catch( reject )
            } catch (e) {
                reject(e.message)
            }
        })
    })

    inicializando.then( () => {
        $(`.preloader`).fadeOut('fast')
    })
}


const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblOperaciones').length > 0) {
                tblOperaciones = $('#tblOperaciones').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                          targets: [5,6],
                          className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [4],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblOperaciones.columns.adjust().draw();
            }

            if ($('#tblBilletes').length > 0) {
                tblBilletes = $('#tblBilletes').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0, 1, 2, 3,4,5],
                            "orderable": true,
                            className: 'text-center',
                        },
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblBilletes.columns.adjust().draw();
            }

            resolve()

        } catch (e) {
            reject(e.message)
        }
    })
}

const rptPersona = {
    txtInicio: document.getElementById('txtInicio'),
    txtFin: document.getElementById('txtFin'),
    txtDocumentoPrimario: document.getElementById('txtDocumentoPrimario'),
    cboTipoDocumento : document.getElementById('cboTipoDocumento'),
    cambioFecha()
    {
        $("#txtFin").datepicker('destroy')
        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        document.getElementById('txtFin').value = ''
        $("#txtFin").datepicker('update')
    },
    generarReporte()
    {
        formActions.validate('formRptPersona')
            .then( ( { errores } ) => {
                if( errores == 0)
                {
                    tblOperaciones.clear()
                    configTblOperaciones.contador = 0
                    configTblOperaciones.operaciones = {}

                    fetchActions.set({
                        modulo, archivo: 'Reporte/billcoopRptPersona', datos: {
                            ...formActions.getJSON('formRptPersona'), accion: 'GENERAR'
                        }
                    }).then( ( {operaciones} ) => {


                        if (operaciones.operaciones.length == 0)
                        {
                            toastr.warning(`No hay operaciones`)
                        } else {

                            document.getElementById('btnExportar').classList.remove('disabled')
                            this.txtInicio.disabled = true
                            this.txtFin.disabled = true
                            this.txtDocumentoPrimario.readOnly = true
                            this.cboTipoDocumento.disabled = true
                            $(`#${this.cboTipoDocumento.id}`).selectpicker('refresh')

                            tblBiletes.cargar('trBilletesCincuenta', {
                                cantidadTitular : operaciones.billetesCincuenta.cantidadTitular,
                                totalTitular : operaciones.billetesCincuenta.totalTitular,
                                cantidadRecibidos : operaciones.billetesCincuenta.cantidadRecibidos,
                                totalRecibidos : operaciones.billetesCincuenta.totalRecibidos,
                                cantidadBilletesEnviados: operaciones.billetesCincuenta.cantidadBilletesEnviados,
                                totalBilletesEnviados: operaciones.billetesCincuenta.totalBilletesEnviados
                            });

                            tblBiletes.cargar('trBilletesCien', {
                                cantidadTitular : operaciones.billetesCien.cantidadTitular,
                                totalTitular : operaciones.billetesCien.totalTitular,
                                cantidadRecibidos : operaciones.billetesCien.cantidadRecibidos,
                                totalRecibidos : operaciones.billetesCien.totalRecibidos,
                                cantidadBilletesEnviados: operaciones.billetesCien.cantidadBilletesEnviados,
                                totalBilletesEnviados: operaciones.billetesCien.totalBilletesEnviados
                            });

                            tblBiletes.setTotal( operaciones.totalTitular, operaciones.totalRecibidos, operaciones.totalEnviados )

                            operaciones.operaciones.forEach( operacion => {
                                configTblOperaciones.contador++
                                configTblOperaciones.agregarFila({
                                    contador: configTblOperaciones.contador,
                                    ...operacion
                                })
                            })

                            configTblOperaciones.operaciones = { ...operaciones }

                        }

                        tblOperaciones.columns.adjust().draw()

                    }).catch( generalMostrarError )
                }
            })
    },
    exportarReporte()
    {
        if ( configTblOperaciones.operaciones.operaciones.length > 0 )
        {
            fetchActions.set({
                modulo, archivo: 'Reporte/billcoopRptPersona', datos: {
                    ...formActions.getJSON('formRptPersona'), accion: 'EXPORTAR',
                    operaciones: {... configTblOperaciones.operaciones}
                }
            }).then( response => {

                switch (response.respuesta.trim())
                {
                    case 'EXITO':
                        window.open('./main/BILLCOOP/docs/Excel/' + response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }
            })
        } else
        {
            toastr.warning(`No hay datos para exportar`)
        }
    },
    limpiar()
    {
        formActions.clean('formRptPersona')
            .then( () => {

                tblBiletes.cargar('trBilletesCincuenta', {
                    cantidadTitular : 0,
                    totalTitular : '0.00',
                    cantidadRecibidos : 0,
                    totalRecibidos : '0.00',
                    cantidadBilletesEnviados: 0,
                    totalBilletesEnviados: '0.00'
                });

                tblBiletes.cargar('trBilletesCien', {
                    cantidadTitular : 0,
                    totalTitular : '0.00',
                    cantidadRecibidos : 0,
                    totalRecibidos : '0.00',
                    cantidadBilletesEnviados: 0,
                    totalBilletesEnviados: '0.00'
                });

                tblBiletes.setTotal( '0.00', '0.00', '0.00' )

                configTblOperaciones.contador = 0
                configTblOperaciones.operaciones = {}

                tblOperaciones.clear().draw()

                document.getElementById('btnExportar').classList.add('disabled')
                this.txtInicio.disabled = false
                this.txtFin.disabled = false
                this.txtDocumentoPrimario.readOnly = true
                this.cboTipoDocumento.disabled = false
                $(`#${this.cboTipoDocumento.id}`).selectpicker('refresh')

            })
    }
}

const tblBiletes = {
    cargar( identificador, { totalTitular, cantidadTitular, totalRecibidos, cantidadRecibidos, totalBilletesEnviados, cantidadBilletesEnviados  } )
    {
        $(`#${identificador}`).find("td:eq(1)").html(`${cantidadTitular}`);
        $(`#${identificador}`).find("td:eq(2)").html(`$${totalTitular}`);
        $(`#${identificador}`).find("td:eq(3)").html(`${cantidadRecibidos}`);
        $(`#${identificador}`).find("td:eq(4)").html(`$${totalRecibidos}`);
        $(`#${identificador}`).find("td:eq(5)").html(`${cantidadBilletesEnviados}`);
        $(`#${identificador}`).find("td:eq(6)").html(`$${totalBilletesEnviados}`);
    },
    setTotal( totalTitular, totalRecibidos, totalEnviados )
    {
        $("#trTotalBilletes").find("td:eq(2)").html(`$${totalTitular}`);
        $("#trTotalBilletes").find("td:eq(4)").html(`$${totalRecibidos}`);
        $("#trTotalBilletes").find("td:eq(6)").html(`$${totalEnviados}`);
    }
}

const configTblOperaciones = {
    operaciones : {},
    contador: 0,
    agregarFila( { contador, nombres, apellidos, tipoDocumento, numeroDocumento, agencia, monto, billetesCincuenta, billetesCien, fechaRegistro } )
    {
        tblOperaciones.row.add([
            contador, `${nombres} ${apellidos}`, `${tipoDocumento} / ${numeroDocumento}`, agencia, `$${parseFloat(monto).toFixed(2)}`,
            billetesCincuenta, billetesCien, fechaRegistro
        ]).node().id = `trOperaciones${contador}`
    }
}