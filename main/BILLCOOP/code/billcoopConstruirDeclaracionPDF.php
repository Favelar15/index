<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
session_start();
$respuesta = [];

if ( isset($_SESSION['index'])  && $_SESSION['index']->locked )
{
    if ( isset( $_GET['idOperacion'] ))
    {
        require_once '../../code/connectionSqlServer.php';
        require_once 'Models/Permiso.php';
        require_once 'Models/Persona.php';

        $declaracion = new OperacionBillcoop();
        $declaracion->id = (int) $_GET['idOperacion'];
        $respuesta = $declaracion->construirPDF();

    } else {
        $respuesta = ["respuesta" => "Error al construir archivo", "nombreArchivo" => null];
    }
} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );