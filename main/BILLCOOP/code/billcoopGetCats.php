<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (Object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"]))
    {
        include "../../code/connectionSqlServer.php";

        $solicitados = explode("@@@", $_GET["q"]);

        if (in_array('productos', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Models/Productos.php';
            $producto = new Productos();
            $respuesta->{"productos"} = $producto->obtenerProductos();
        }

        if (in_array('tiposDocumento', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/tipoDocumento.php';
            $tipoDocumento = new tipoDocumento();
            $tiposDocumento = $tipoDocumento->obtenerTiposDocumento();

            $respuesta->{"tiposDocumento"} = $tiposDocumento;
        }

        if (in_array('actividadEconomica', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/actividadEconomica.php';
            $actividadEconomica = new actividadEconomica();
            $actividadesEconomicas = $actividadEconomica->obtenerActividades();

            $respuesta->{"actividadesEconomicas"} = $actividadesEconomicas;
        }

        if (in_array('geograficos', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/datosGeograficos.php';
            $geografico = new geografico();
            $datosGeograficos = $geografico->obtenerGeograficos();

            $respuesta->{"datosGeograficos"} = $datosGeograficos;
        }

        if (in_array('roles', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/rol.php';
            $rol = new rol();
            if (in_array('rolesCbo', $solicitados)) {
                $roles = $rol->getRolesCbo();
            } else {
                $roles = $rol->getRoles();
            }

            $respuesta->{"roles"} = $roles;
        }

        if (in_array('agenciasAsignadas', $solicitados) || in_array('all', $solicitados))
        {
            require_once '../../code/Models/agencia.php';

            $agencia = new agencia();
            $agencias = $agencia->getAgenciasCbo();

            $respuesta->{"agenciasAsignadas"} =$agencias;
        }

    }
}

echo json_encode($respuesta);