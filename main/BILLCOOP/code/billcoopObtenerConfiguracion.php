<?php

header("Content-type: application/json; charset=utf-8");
include "../../code/generalParameters.php";
$input = json_decode(file_get_contents("php://input"), true);

session_start();

$respuesta = [ 'configuracion' => [] ];

if ( isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../code/connectionSqlServer.php';
    require_once 'Models/Permiso.php';
    require_once 'Models/Persona.php';

    $operacion = new OperacionBillcoop();
    $respuesta['configuracion'] = $operacion->obtenerConfiguracion();

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta);


