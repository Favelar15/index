<?php
include "../../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


session_start();

$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {

    require_once '../../../code/connectionSqlServer.php';
    require_once '../Models/Permiso.php';
    require_once '../Models/Persona.php';


    $nombreArchivo = 'rpt-billcoopOperaciones-' . $input['txtInicio']['value'] . '-' . $input['txtFin']['value'] . '.xlsx';
    $idAgencias = [];

    if ($input['cboAgencia']['value'] == 'all') {
        foreach ($_SESSION['index']->agencias as $agencia) {
            array_push($idAgencias, (int)$agencia->id);
        }
    } else {
        $nombreArchivo = 'rpt-billcoopOperaciones-' . $input['cboAgencia']['value'] . '-' . $input['txtInicio']['value'] . '-' . $input['txtFin']['value'] . '.xlsx';
        array_push($idAgencias, (int)$input['cboAgencia']['value']);
    }

    $desde = date('Y-m-d', strtotime($input['txtInicio']['value']));
    $hasta = date('Y-m-d', strtotime($input['txtFin']['value']));
    $accion = $input['accion'];


    if ($accion == 'GENERAR') {
        $operacion = new OperacionBillcoop();
        $operacion->setAgencias($idAgencias);
        $respuesta['operaciones'] = $operacion->rptGeneral($desde, $hasta);

    } else {

        $reporte = new ExcelGeneral();
        $reporte->operaciones = $input['operaciones'];
        $reporte->agencias = $idAgencias;
        $reporte->nombreArchivo = "rptBillcoop-General-".$desde."-".$hasta.'.xlsx';
        $respuesta = $reporte->crearExcel();
    }

} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);

class ExcelGeneral
{
    public $operaciones;
    public $nombreArchivo;
    public $agencias;

    function crearExcel()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $idAgencias = implode(',', $this->agencias);

        $sheet->setCellValue('A1', "Operaciones BILLCOOP de agencias ($idAgencias)");
        $spreadsheet->getActiveSheet()->mergeCells("A1:V1");
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

        $spreadsheet->getActiveSheet()->getStyle('A1:V1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:V1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $sheet->setCellValue('A2', 'N°');
        $sheet->setCellValue('B2', 'Código Cliente');
        $sheet->setCellValue('C2', 'nombres');
        $sheet->setCellValue('D2', 'Tipo Documento');
        $sheet->setCellValue('E2', 'Número documento');
        $sheet->setCellValue('F2', 'Actividad ecónomica');
        $sheet->setCellValue('G2', 'Producto/servicio');
        $sheet->setCellValue('H2', 'Numero de referencia');
        $sheet->setCellValue('I2', 'Tipo de Operación');
        $sheet->setCellValue('J2', 'Procedencia');
        $sheet->setCellValue('K2', 'Monto');
        $sheet->setCellValue('L2', 'Monto recibido');
        $sheet->setCellValue('M2', '$ 50');
        $sheet->setCellValue('N2', '$ 100');

        $sheet->setCellValue('O2', 'Codigo Cliente');
        $sheet->setCellValue('P2', 'Nombres');
        $sheet->setCellValue('Q2', 'Tipo documento');
        $sheet->setCellValue('R2', 'Número documento');
        $sheet->setCellValue('S2', 'Actividad ecónomica');

        $sheet->setCellValue('T2', 'Agencia');
        $sheet->setCellValue('U2', 'Usuario');
        $sheet->setCellValue('V2', 'fechaRegistro');

        $spreadsheet->getActiveSheet()->getStyle('A2:N2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:N2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $spreadsheet->getActiveSheet()->getStyle('O2:S2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('O2:S2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('EF6D6D');

        $spreadsheet->getActiveSheet()->getStyle('T2:V2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('T2:V2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $contador = 2;

        foreach ($this->operaciones as $operacion)
        {
            $contador++;
            $sheet->setCellValue("A$contador", ($contador - 2) );
            $sheet->setCellValue("B$contador", $operacion['codigoCliente']);
            $sheet->setCellValue("C$contador", $operacion['nombres'].' '.$operacion['apellidos']);
            $sheet->setCellValue("D$contador", $operacion['tipoDocumento']);
            $sheet->setCellValue("E$contador", $operacion['numeroDocumento']);
            $sheet->setCellValue("F$contador", $operacion['actividadEconomica']);
            $sheet->setCellValue("G$contador", $operacion['tipoProducto']);
            $sheet->setCellValue("H$contador", $operacion['numeroCuenta']);
            $sheet->setCellValue("I$contador", $operacion['tipoOperacion']);
            $sheet->setCellValue("J$contador", $operacion['procedencia']);
            $sheet->setCellValue("K$contador", '$'.$operacion['monto']);
            $sheet->setCellValue("L$contador", '$'.$operacion['montoRecibido']);
            $sheet->setCellValue("M$contador", $operacion['billetesCincuenta']);
            $sheet->setCellValue("N$contador", $operacion['billetesCien']);

            if ( $operacion['isTitular'] == 'N')
            {
                $sheet->setCellValue("O$contador", $operacion['TcodigoCliente']);
                $sheet->setCellValue("P$contador", $operacion['Tnombres']);
                $sheet->setCellValue("Q$contador", $operacion['TtipoDocumento']);
                $sheet->setCellValue("R$contador", $operacion['TnumeroPrimario']);
                $sheet->setCellValue("S$contador", $operacion['TactividadEconomica']);
            }

            $sheet->setCellValue("T$contador", $operacion['agencia']);
            $sheet->setCellValue("U$contador", $operacion['usuario']);
            $sheet->setCellValue("V$contador", $operacion['fechaRegistro']);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '../../../docs/Excel/'.$this->nombreArchivo);

        return (Object) [
            'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];

    }
}
