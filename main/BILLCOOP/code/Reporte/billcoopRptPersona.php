<?php
include "../../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


// SEPARAR EL CONSOLIDADO DE BILLETES
// CONSUMIDOS POR EL TITRULAR O HECHOS A TERCERAS PERSONAS

// detalle de configuracion

session_start();

$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked)
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../Models/Permiso.php';
    require_once '../Models/Persona.php';

    $desde = date('Y-m-d', strtotime( $input['txtInicio']['value'] ));
    $hasta =  date('Y-m-d', strtotime( $input['txtFin']['value'] ));
    $accion = $input['accion'];

    if ( $accion == 'GENERAR')
    {
        $operacion = new OperacionBillcoop();
        $operacion->tipoDocumento = (int) $input['cboTipoDocumento']['value'];
        $operacion->numeroPrimario = $input['txtDocumentoPrimario']['value'];
        $operacion->TtipoDocumento = (int) $input['cboTipoDocumento']['value'];
        $operacion->TnumeroPrimario = $input['txtDocumentoPrimario']['value'];
        $respuesta['operaciones'] = $operacion->rptPersona( $desde, $hasta );

    } else if ( $accion === 'EXPORTAR')
    {
        $archivo = new ExcelPersona();
        $archivo->nombreArchivo = "rptBillcoop-".$desde."-".$hasta.'.xlsx';
        $archivo->operaciones = $input['operaciones']['operaciones'];
        $billetes = new stdClass();
        $billetes->BilletesCincuenta = $input['operaciones']['billetesCincuenta'];
        $billetes->BilletesCien = $input['operaciones']['billetesCien'];
        $billetes->totalTitular = $input['operaciones']['totalTitular'];
        $billetes->totalRecibidos = $input['operaciones']['totalRecibidos'];
        $billetes->totalEnviados = $input['operaciones']['totalEnviados'];
        $archivo->billetes = $billetes;
        $respuesta = $archivo->crearExcel();
    }
} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );


class ExcelPersona
{
    public $data;
    public $operaciones = [];
    public $nombreArchivo;
    public $billetes;

    function __construct()
    {
        $this->billetes = new stdClass();
    }

    function crearExcel()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', "Consolidado de billetes por denominación");
        $spreadsheet->getActiveSheet()->mergeCells("A1:G1");
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

        $spreadsheet->getActiveSheet()->getStyle('A1:C1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $sheet->setCellValue('A2', "Denominación");
        $sheet->setCellValue('B2', "Hechas por el titular de la cuenta");
        $sheet->setCellValue('D2', "Recibidos de terceros");
        $sheet->setCellValue('F2', "Enviados a terceros");

        $sheet->getStyle('A2')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('D2')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->mergeCells("B2:C2");
        $spreadsheet->getActiveSheet()->mergeCells("D2:E2");
        $spreadsheet->getActiveSheet()->mergeCells("F2:G2");
        $spreadsheet->getActiveSheet()->getStyle('A2:G2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:C2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');
        $spreadsheet->getActiveSheet()->getStyle('D2:E2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('EF6D6D');
        $spreadsheet->getActiveSheet()->getStyle('F2:G2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('02ACA6');


        $sheet->setCellValue('B3', 'N°');
        $sheet->setCellValue('C3', 'SubTotal');

        $sheet->setCellValue('D3', 'N°');
        $sheet->setCellValue('E3', 'SubTotal');

        $sheet->setCellValue('F3', 'N°');
        $sheet->setCellValue('G3', 'SubTotal');


        $spreadsheet->getActiveSheet()->getStyle('A3:G3')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A3:C3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');
        $spreadsheet->getActiveSheet()->getStyle('D3:E3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('EF6D6D');
        $spreadsheet->getActiveSheet()->getStyle('F3:G3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('02ACA6');



        $sheet->setCellValue('A4', '$50');
        $sheet->setCellValue('B4', $this->billetes->BilletesCincuenta['cantidadTitular']);
        $sheet->setCellValue('C4', '$'.$this->billetes->BilletesCincuenta['totalTitular']);
        $sheet->setCellValue('D4', $this->billetes->BilletesCincuenta['cantidadRecibidos']);
        $sheet->setCellValue('E4', "$".$this->billetes->BilletesCincuenta['totalRecibidos']);
        $sheet->setCellValue('F4', $this->billetes->BilletesCincuenta['cantidadBilletesEnviados']);
        $sheet->setCellValue('G4', "$".$this->billetes->BilletesCincuenta['totalBilletesEnviados']);

        $sheet->setCellValue('A5', '$100');
        $sheet->setCellValue('B5', $this->billetes->BilletesCien['cantidadTitular']);
        $sheet->setCellValue('C5', '$'.$this->billetes->BilletesCien['totalTitular']);
        $sheet->setCellValue('D5', $this->billetes->BilletesCien['cantidadRecibidos']);
        $sheet->setCellValue('E5', "$".$this->billetes->BilletesCien['totalRecibidos']);
        $sheet->setCellValue('F5', $this->billetes->BilletesCien['cantidadBilletesEnviados']);
        $sheet->setCellValue('G5', "$".$this->billetes->BilletesCien['totalBilletesEnviados']);


        $sheet->setCellValue('A6', '');
        $sheet->setCellValue('B6', 'Total');
        $sheet->setCellValue('C6', '$'.$this->billetes->totalTitular);
        $sheet->setCellValue('D6', 'Total');
        $sheet->setCellValue('E6', '$'.$this->billetes->totalRecibidos);
        $sheet->setCellValue('F6', 'Total');
        $sheet->setCellValue('G6', '$'.$this->billetes->totalEnviados);


        $sheet->setCellValue('A8', "Reporte de operaciones BILLCOOP");
        $spreadsheet->getActiveSheet()->mergeCells("A8:Y8");
        $sheet->getStyle('A8')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A9', 'N°');
        $sheet->setCellValue('B9', 'Código Cliente');
        $sheet->setCellValue('C9', 'nombres');
        $sheet->setCellValue('D9', 'Tipo Documento');
        $sheet->setCellValue('E9', 'Número documento');
        $sheet->setCellValue('F9', 'Actividad ecónomica');
        $sheet->setCellValue('G9', 'Producto/servicio');
        $sheet->setCellValue('H9', 'Numero de referencia');
        $sheet->setCellValue('I9', 'Tipo de Operación');
        $sheet->setCellValue('J9', 'Procedencia');
        $sheet->setCellValue('K9', 'Monto');
        $sheet->setCellValue('L9', 'Monto recibido');
        $sheet->setCellValue('M9', '$ 50');
        $sheet->setCellValue('N9', '$ 100');

        $sheet->setCellValue('O9', 'Codigo Cliente');
        $sheet->setCellValue('P9', 'Nombres');
        $sheet->setCellValue('Q9', 'Tipo documento');
        $sheet->setCellValue('R9', 'Número documento');
        $sheet->setCellValue('S9', 'Actividad ecónomica');

        $sheet->setCellValue('T9', 'Beneficiario');
        $sheet->setCellValue('U9', 'Pais de destino');
        $sheet->setCellValue('V9', 'Destino de los fondos');

        $sheet->setCellValue('W9', 'Agencia');
        $sheet->setCellValue('X9', 'Usuario');
        $sheet->setCellValue('Y9', 'fechaRegistro');


        $spreadsheet->getActiveSheet()->getStyle('A8:V8')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A8:V8')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $spreadsheet->getActiveSheet()->getStyle('A9:N9')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A9:N9')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $spreadsheet->getActiveSheet()->getStyle('O9:S9')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('O9:S9')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('EF6D6D');

        $spreadsheet->getActiveSheet()->getStyle('T9:V9')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('T9:V9')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('02ACA6');

        $spreadsheet->getActiveSheet()->getStyle('W9:Y9')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('W9:Y9')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');


        $contador = 9;

        foreach ( $this->operaciones as $operacion )
        {
            $contador++;
            $sheet->setCellValue("A$contador", ($contador - 9) );
            $sheet->setCellValue("B$contador", $operacion['codigoCliente']);
            $sheet->setCellValue("C$contador", $operacion['nombres'].' '.$operacion['apellidos']);
            $sheet->setCellValue("D$contador", $operacion['tipoDocumento']);
            $sheet->setCellValue("E$contador", $operacion['numeroDocumento']);
            $sheet->setCellValue("F$contador", $operacion['actividadEconomica']);
            $sheet->setCellValue("G$contador", $operacion['tipoProducto']);
            $sheet->setCellValue("H$contador", $operacion['numeroCuenta']);
            $sheet->setCellValue("I$contador", $operacion['tipoOperacion']);
            $sheet->setCellValue("J$contador", $operacion['procedencia']);
            $sheet->setCellValue("K$contador", '$'.$operacion['monto']);
            $sheet->setCellValue("L$contador", '$'.$operacion['montoRecibido']);
            $sheet->setCellValue("M$contador", $operacion['billetesCincuenta']);
            $sheet->setCellValue("N$contador", $operacion['billetesCien']);

            if ( $operacion['isTitular'] == 'N' && $operacion['tipoProducto'] != 'Envío de remesa' )
            {
                $sheet->setCellValue("O$contador", $operacion['TcodigoCliente']);
                $sheet->setCellValue("P$contador", $operacion['Tnombres']);
                $sheet->setCellValue("Q$contador", $operacion['TtipoDocumento']);
                $sheet->setCellValue("R$contador", $operacion['TnumeroPrimario']);
                $sheet->setCellValue("S$contador", $operacion['TactividadEconomica']);
            }

            $sheet->setCellValue("T$contador", $operacion['beneficiario']);
            $sheet->setCellValue("U$contador", $operacion['paisDestino']);
            $sheet->setCellValue("V$contador", $operacion['destino']);

            $sheet->setCellValue("W$contador", $operacion['agencia']);
            $sheet->setCellValue("X$contador", $operacion['usuario']);
            $sheet->setCellValue("Y$contador", $operacion['fechaRegistro']);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '../../../docs/Excel/'.$this->nombreArchivo);

        return (Object) [
            'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];
    }
}