<?php
include "../../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


session_start();

$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked)
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../Models/Permiso.php';
    require_once '../Models/Persona.php';


    $nombreArchivo = 'rpt-billcoopOperacionesPorAgencia-' . $input['txtInicio']['value'] . '-' . $input['txtFin']['value'] . '.xlsx';
    $idAgencias = [];

    if ($input['cboAgencia']['value'] == 'all') {
        foreach ($_SESSION['index']->agencias as $agencia) {
            array_push($idAgencias, (int)$agencia->id);
        }
    } else {
        $nombreArchivo = 'rpt-billcoopOperacionesPorAgencia-' . $input['cboAgencia']['value'] . '-' . $input['txtInicio']['value'] . '-' . $input['txtFin']['value'] . '.xlsx';
        array_push($idAgencias, (int)$input['cboAgencia']['value']);
    }

    $desde = date('Y-m-d', strtotime( $input['txtInicio']['value'] ));
    $hasta =  date('Y-m-d', strtotime( $input['txtFin']['value'] ));
    $accion = $input['accion'];

    if ( $accion == 'GENERAR')
    {
        $operacion = new OperacionBillcoop();
        $operacion->setAgencias( $idAgencias );
        $respuesta['consolidados'] = $operacion->rptAgencias( $desde, $hasta );

    } else
    {
        $archivo = new ExcelAgencia();
        $archivo->nombreArchivo = $nombreArchivo;
        $archivo->agencias = $input['agencias'];
        $archivo->productos = $input['productos'];
        $respuesta = $archivo->crearExcel();
    }

} else
{
    $respuesta['respuesta'] = 'SESION';
}


echo json_encode( $respuesta );

class ExcelAgencia
{
    public $nombreArchivo;
    public $agencias;
    public $productos;


    function crearExcel()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', "Detalle de billetes por agencia");
        $spreadsheet->getActiveSheet()->mergeCells("A1:G1");
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

        $spreadsheet->getActiveSheet()->getStyle('A1:G1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:G1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $sheet->setCellValue('A2', 'N°');
        $sheet->setCellValue('B2', 'Agencia');
        $sheet->setCellValue('C2', 'Cantidad ($50)');
        $sheet->setCellValue('D2', 'Subtotal ($50)');
        $sheet->setCellValue('E2', 'Cantidad ($100)');
        $sheet->setCellValue('F2', 'SubTotal ($100)');
        $sheet->setCellValue('G2', 'Total por agencia');

        $spreadsheet->getActiveSheet()->getStyle('A2:G2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:G2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $contador = 2;

        foreach ( $this->agencias as $agencia )
        {
            $contador++;
            $sheet->setCellValue("A${contador}", ($contador - 2 ) );
            $sheet->setCellValue("B${contador}", $agencia['agencia'] );
            $sheet->setCellValue("C${contador}", $agencia['billetesCincuenta']);
            $sheet->setCellValue("D${contador}", "$".number_format($agencia['totalCincuenta'] , 2, '.', ','));
            $sheet->setCellValue("E${contador}", $agencia['billetesCien'] );
            $sheet->setCellValue("F${contador}", "$".number_format($agencia['totalCien'], 2, '.', ',') );
            $sheet->setCellValue("G${contador}", "$".number_format(( floatval( $agencia['totalCincuenta'] ) + floatval( $agencia['totalCien'] ) ), 2, '.', ',') );
        }

        $contador+=2;

        $sheet->setCellValue("A$contador", "Detalle de billetes por Producto");
        $spreadsheet->getActiveSheet()->mergeCells("A$contador:G$contador");
        $sheet->getStyle("A$contador")->getAlignment()->setHorizontal('center');

        $spreadsheet->getActiveSheet()->getStyle("A$contador:G$contador")->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle("A$contador:G$contador")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $contador++;
        $sheet->setCellValue("A$contador", 'N°');
        $sheet->setCellValue("B$contador", 'Tipo de producto');
        $sheet->setCellValue("C$contador", 'Cantidad ($50)');
        $sheet->setCellValue("D$contador", 'Subtotal ($50)');
        $sheet->setCellValue("E$contador", 'Cantidad ($100)');
        $sheet->setCellValue("F$contador", 'SubTotal ($100)');
        $sheet->setCellValue("G$contador", 'Total por agencia');

        $contadorProducto = 0;

        foreach ( $this->productos as $producto )
        {
            $contador++; $contadorProducto++;
            $sheet->setCellValue("A${contador}", $contadorProducto);
            $sheet->setCellValue("B${contador}", $producto['tipoProducto'] );
            $sheet->setCellValue("C${contador}", $producto['billetesCincuenta']);
            $sheet->setCellValue("D${contador}", "$".number_format($producto['totalCincuenta'] , 2, '.', ','));
            $sheet->setCellValue("E${contador}", $producto['billetesCien'] );
            $sheet->setCellValue("F${contador}", "$".number_format($producto['totalCien'], 2, '.', ',') );
            $sheet->setCellValue("G${contador}", "$".number_format(( floatval( $producto['totalCincuenta'] ) + floatval( $producto['totalCien'] ) ), 2, '.', ',') );
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '../../../docs/Excel/'.$this->nombreArchivo);

        return (Object) [
            'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];
    }
}