<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

session_start();
$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked)
{

    require_once '../../code/connectionSqlServer.php';
    require_once 'Models/Permiso.php';
    require_once 'Models/Persona.php';

    $operacion = new OperacionBillcoop();
    $operacion->setIdApartado((int)base64_decode(urldecode($input['idApartado'])));
    $operacion->setTipoApartado($input['tipoApartado']);
    $operacion->idUsuario = $_SESSION['index']->id;
    $operacion->ipOrdenador = generalObtenerIp();

    $configDB = '';

    if (count($input['roles']) > 0) {
        $temp = [];

        foreach ($input['roles'] as $log) {
            array_push($temp, $log['id'] . '%%%' . $log['edicion'] . '%%%' . $log['eliminacion']);
        }
        $configDB = implode('@@@', $temp);
    }

    $respuesta['respuesta'] = $operacion->guardarConfiguracion( $configDB );

} else
{
    $respuesta['respuesta'] = 'SESION';
}


echo json_encode( $respuesta );
