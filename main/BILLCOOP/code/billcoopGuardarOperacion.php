<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
require_once($_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php');

session_start();
$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    require_once '../../code/connectionSqlServer.php';
    require_once 'Models/Permiso.php';
    require_once 'Models/Persona.php';

    $datosPersona = $input['billetes']['persona'];

    $operacion = new OperacionBillcoop();
    $operacion->codigoCliente = $datosPersona['txtCodigoCliente']['value'];
    $operacion->tipoDocumento = (int)$datosPersona['cboTipoDocumento']['value'];
    $operacion->numeroPrimario = $datosPersona['txtDocumentoPrimario']['value'];
    $operacion->nombres = $datosPersona['txtNombres']['value'];
    $operacion->apellidos = $datosPersona['txtApellidos']['value'];
    $operacion->telefonoFijo = $datosPersona['txtTelefonoFijo']['value'];
    $operacion->telefonoMovil = $datosPersona['txtTelefonoMovil']['value'];
    $operacion->idActividadEconomica = $datosPersona['cboActividadEconomica']['value'];
    $operacion->idPais = $datosPersona['cboPais']['value'];
    $operacion->idDepartamento = $datosPersona['cboDepartamento']['value'];
    $operacion->idMunicipio = $datosPersona['cboMunicipio']['value'];
    $operacion->direccion = $datosPersona['txtDireccion']['value'];

    $datosOperacion = $input['billetes']['operacion'];

    $producto = new stdClass();
    $producto->id = (int)$datosOperacion['cboTipoProducto']['value'];

    $operacion->idOperacion = $datosOperacion['idOperacion'];
    $operacion->Producto = $producto;
    $operacion->numeroCuenta = $datosOperacion['txtNumeroCuenta']['value'];
    $operacion->tipoOperacion = $datosOperacion['txtTipoOperacion']['value'];
    $operacion->procedencia = $datosOperacion['txtProcedencia']['value'];
    $operacion->monto = $datosOperacion['txtMonto']['value'];
    $operacion->montoRecibido = $datosOperacion['txtMontoRecibido']['value'];
    $isTitular = $datosOperacion['cboEsTitular']['value'];
    $operacion->isTitular = $isTitular;


    if ( $datosOperacion['cboTipoProducto']['labelOption']  == 'Envío de remesa')
    {
        $operacion->beneficiario = $datosOperacion['txtNombreBeneficiario']['value'];
        $operacion->paisDestino = $datosOperacion['txtPaisDestino']['value'];
        $operacion->destino = $datosOperacion['txtDestino']['value'];
    }

    $datosTitular = $input['billetes']['titular'];
    $operacion->TtipoDocumento = $datosTitular['cboTipoDocumentoTitular']['value'];
    $operacion->TcodigoCliente = $datosTitular['txtCodigoClienteTitular']['value'];
    $operacion->TnumeroPrimario = $datosTitular['txtDocumentoPrimarioTitular']['value'];
    $operacion->Tnombres = $datosTitular['txtNombresTitular']['value'];
    $operacion->Tapellidos = $datosTitular['txtApellidosTitular']['value'];
    $operacion->TidActividadEconomica = $datosTitular['cboActividadEconomicaTitular']['value'];


    $billetes = $input['billetes'];
    $operacion->billetesCincuenta = (int)$billetes['50']['cantidad'];
    $operacion->billetesCien = (int)$billetes['100']['cantidad'];

    $operacion->idAgencia = $_SESSION['index']->agenciaActual->id;
    $operacion->idUsuario = $_SESSION['index']->id;
    $operacion->ipOrdenador = generalObtenerIp();

    $operacion->setIdApartado((int)base64_decode(urldecode($input['idApartado'])));
    $operacion->setTipoApartado($input['tipoApartado']);

    $logsPersonasDB = '';

    if (count($input['logsPersona']) > 0) {
        $temp = [];

        foreach ($input['logsPersona'] as $log) {
            if (empty($log['valorAnterior'])) {
                $log['valorAnterior'] = 'NULL';
            }

            if (empty($log['valorNuevo'])) {
                $log['valorNuevo'] = 'NULL';
            }

            array_push($temp, $log['campo'] . '%%%' . $log['valorAnterior'] . '%%%' . $log['valorNuevo']);
        }
        $logsPersonasDB = implode('@@@', $temp);
    }

    $logsOperaciones = '';

    if (count($input['logsTransaccion']) > 0) {
        $temp = [];

        foreach ($input['logsTransaccion'] as $log) {

            if (empty($log['valorAnterior'])) {
                $log['valorAnterior'] = 'NULL';
            }

            if (empty($log['valorNuevo'])) {
                $log['valorNuevo'] = 'NULL';
            }

            array_push($temp, $log['campo'] . '%%%' . $log['valorAnterior'] . '%%%' . $log['valorNuevo']);
        }
        $logsOperaciones = implode('@@@', $temp);
    }

    $response = $operacion->guardarOperacion($logsPersonasDB, $logsOperaciones);
    $respuesta['respuesta'] = $response->respuesta;
    $respuesta['id'] = $response->id;
    $respuesta['nombreArchivo'] = $response->nombreArchivo;
    $respuesta['temp'] = $response;

} else {
    $respuesta['respuesta'] = 'SESION';
}


echo json_encode($respuesta);
