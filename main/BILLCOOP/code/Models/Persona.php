<?php

class Persona extends Permiso
{
    public $idPersona, $codigoCliente,
        $numeroPrimario,
        $tipoDocumento,
        $nombres, $apellidos, $telefonoFijo, $telefonoMovil, $idActividadEconomica,
        $idPais, $idDepartamento, $idMunicipio, $direccion;

    private $conexion;

    function __construct()
    {
        parent::__construct();
        global $conexion;
        $this->conexion = $conexion;
    }

    private function buscarAsociadoPorDUI()
    {
        $query = "SELECT * FROM general_viewDatosAsociados WHERE tipoDocumentoPrimario = :tipoDocumento AND numeroDocumentoPrimario = :numeroDocumento AND habilitado = 'S';";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':tipoDocumento', $this->tipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumento', $this->numeroPrimario, PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_CLASS, 'asociado')[0];
        }
        return false;
    }

    function buscarPersona()
    {
        $query = "SELECT * FROM billcoop_viewDatosPersonas WHERE idTipoDocumento = :tipoDocumento AND numeroDocumento = :numeroPrimario";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':numeroPrimario', $this->numeroPrimario, PDO::PARAM_STR);
        $result->bindParam(':tipoDocumento', $this->tipoDocumento, PDO::PARAM_INT);
        $result->execute();

        if ($result->rowCount() > 0) {
            $this->conexion = null;
            $persona = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__)[0];

            return (object)[
                'Persona' => $persona,
                'existe' => true
            ];
        } else {
            $persona = $this->buscarAsociadoPorDUI();

            if ($persona) {
                return (object)[
                    'Persona' => $persona,
                    'existe' => true
                ];
            }
        }

        $this->conexion = null;
        return (object)[
            'Persona' => (object)[],
            'existe' => false
        ];
    }

    function setIdApartado($idApartado)
    {
        $this->idApartado = $idApartado;
    }

    function setTipoApartado($tipoApartado)
    {
        $this->tipoPermiso = $tipoApartado;
    }

}

class OperacionBillcoop extends Persona
{
    public $idOperacion, $Producto, $numeroCuenta, $tipoOperacion, $procedencia, $monto, $montoRecibido, $isTitular,
        $TnumeroPrimario,
        $TtipoDocumento,
        $TcodigoCliente, $Tnombres, $Tapellidos, $TidActividadEconomica,
        $billetesCincuenta, $billetesCien, $fechaRegistro,
        $beneficiario, $paisDestino, $destino;

    public $idAgencia, $idUsuario, $ipOrdenador, $nombreArchivo;
    public $edicion;

    private $path = __DIR__ . '\..\..\docs\declaraciones\/';
    private $conexion;
    private $agenciasAsignadas = [];

    function setAgencias($agencias)
    {
        $this->agenciasAsignadas = $agencias;
    }

    function __construct()
    {
        parent::__construct();
        global $conexion;
        $this->conexion = $conexion;

        $this->Producto = new stdClass();

        !(empty($this->fechaRegistro)) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->monto) && $this->monto = sprintf('%0.2f', $this->monto);
        !empty($this->montoRecibido) && $this->montoRecibido = sprintf('%0.2f', $this->montoRecibido);

    }

    function guardarOperacion($logsPersonas, $logsOperaciones)
    {
        $respuesta = null;
        $numeroOperaciones = 0;
        $numeroOperacionesMensuales = 0;
        $idDb = 0;

        $query = 'EXEC billcoop_spGuardarOperacion :codigoCliente, :tipoDocumento, :numeroDocumento, :nombres, :apellidos, :telefonoFijo, :telefonoMovil, :idActividadEconomica, :idPais, :idDepartamento, :idMunicipio, :direccion, 
        :idOperacion, :idProducto, :numeroCuenta, :tipoOperacion, :procedencia, :monto, :montoRecibido, :billetesCincuenta, :billetesCien, :isTitular, :beneficiario, :paisDestino, :destino, :TtipoDocumento, 
        :TnumeroDocumento, :TcodigoCliente, :Tnombres, :Tapellidos, :TidActividadEconomica, :idAgencia, :idUsuario, :ipOrdenador, :idApartado, :tipoApartado, :logsPersonas, :logsOperaciones, :respuesta, :idDBOperacion, :nombreArchivo, :operacionesDiarias, :operacionesMensuales';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':tipoDocumento', $this->tipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumento', $this->numeroPrimario, PDO::PARAM_STR);
        $result->bindParam(':nombres', $this->nombres, PDO::PARAM_STR);
        $result->bindParam(':apellidos', $this->apellidos, PDO::PARAM_STR);
        $result->bindParam(':telefonoFijo', $this->telefonoFijo, PDO::PARAM_STR);
        $result->bindParam(':telefonoMovil', $this->telefonoMovil, PDO::PARAM_STR);
        $result->bindParam(':idActividadEconomica', $this->idActividadEconomica, PDO::PARAM_INT);
        $result->bindParam(':idPais', $this->idPais, PDO::PARAM_INT);
        $result->bindParam(':idDepartamento', $this->idDepartamento, PDO::PARAM_INT);
        $result->bindParam(':idMunicipio', $this->idMunicipio, PDO::PARAM_INT);
        $result->bindParam(':direccion', $this->direccion, PDO::PARAM_STR);
        $result->bindParam(':idOperacion', $this->idOperacion, PDO::PARAM_INT);
        $result->bindParam(':idProducto', $this->Producto->id, PDO::PARAM_INT);
        $result->bindParam(':numeroCuenta', $this->numeroCuenta, PDO::PARAM_STR);
        $result->bindParam(':tipoOperacion', $this->tipoOperacion, PDO::PARAM_STR);
        $result->bindParam(':procedencia', $this->procedencia, PDO::PARAM_STR);
        $result->bindParam(':monto', $this->monto, PDO::PARAM_INT);
        $result->bindParam(':montoRecibido', $this->montoRecibido, PDO::PARAM_INT);
        $result->bindParam(':billetesCincuenta', $this->billetesCincuenta, PDO::PARAM_INT);
        $result->bindParam(':billetesCien', $this->billetesCien, PDO::PARAM_INT);
        $result->bindParam(':isTitular', $this->isTitular, PDO::PARAM_STR);
        $result->bindParam(':beneficiario', $this->beneficiario, PDO::PARAM_STR);
        $result->bindParam(':paisDestino', $this->paisDestino, PDO::PARAM_STR);
        $result->bindParam(':destino', $this->destino, PDO::PARAM_STR);
        $result->bindParam(':TtipoDocumento', $this->TtipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':TnumeroDocumento', $this->TnumeroPrimario, PDO::PARAM_STR);
        $result->bindParam(':TcodigoCliente', $this->TcodigoCliente, PDO::PARAM_INT);
        $result->bindParam(':Tnombres', $this->Tnombres, PDO::PARAM_STR);
        $result->bindParam(':Tapellidos', $this->Tapellidos, PDO::PARAM_STR);
        $result->bindParam(':TidActividadEconomica', $this->TidActividadEconomica, PDO::PARAM_INT);
        $result->bindParam(':idAgencia', $this->idAgencia, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoPermiso, PDO::PARAM_STR);
        $result->bindParam(':logsPersonas', $logsPersonas, PDO::PARAM_STR);
        $result->bindParam(':logsOperaciones', $logsOperaciones, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idDBOperacion', $idDb, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':nombreArchivo', $this->nombreArchivo, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':operacionesDiarias', $numeroOperaciones, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':operacionesMensuales', $numeroOperacionesMensuales, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        $response = (object)[];

        if ( $respuesta == 'EXITO')
        {
            if ( (int) $this->monto >= 10000  )
            {
                $email = new EnviarEmail(14);
                $body = [
                    'id' => str_pad($idDb, 7, '0', STR_PAD_LEFT),
                    'persona' => $this->nombres.' '.$this->apellidos,
                    'agencia' => $_SESSION['index']->agenciaActual->nombreAgencia,
                    'monto' => '$'.sprintf('%0.2f', $this->monto),
                    'fechaRegistro' =>  date("d-m-Y h:i:s a"),
                    'usuario' => $_SESSION['index']->nombres.' '.$_SESSION['index']->apellidos
                ];
                $email->setBody( $body );
                $emailResponse = $email->alertaPorMonto();
                $response->email = !empty( $emailResponse) ? true : false;
                $response->reporte = 'monto'.$this->monto;
            }

            if ( $numeroOperacionesMensuales > 1 || $numeroOperaciones > 1 )
            {
                $tipo = '';

                if ( $numeroOperacionesMensuales > 1)
                {
                    $tipo = 'mes';
                    $operaciones = $this->obtenerOperacionesPorPersona();
                } else if ( $numeroOperaciones > 1 )
                {
                    $tipo = 'dia';
                    $operaciones = $this->obtenerOperacionesPorPersona(true);
                }

                $email = new EnviarEmail(15);
                $body = [
                    'persona' => $this->nombres.' '.$this->apellidos,
                    'tipo' => $tipo,
                    'operaciones' => $operaciones
                ];
                $email->setBody( $body );
                $emailResponse = $email->alertPorOperaciones();
                $response->email = !empty( $emailResponse) ? true : false;
            }
        }

        $response->respuesta = $respuesta;
        $response->nombreArchivo = $this->nombreArchivo;
        $response->id = $idDb;

        return $response;
    }

    private function obtenerOperacionesPorPersona ( $porDia = false )
    {
        if ( $porDia )
        {
            $query = 'SELECT id, nombres, agencia, monto, fechaRegistro FROM billcoop_viewDetOperaciones WHERE numeroDocumento = :numeroDocumento  AND CAST(fechaRegistro AS DATE) = CAST(GETDATE() AS DATE)';
        } else
        {
            $query = 'SELECT id, nombres, agencia, monto, fechaRegistro FROM billcoop_viewDetOperaciones WHERE  numeroDocumento = :numeroDocumento AND MONTH(fechaRegistro) =  MONTH(GETDATE())';
        }

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':numeroDocumento', $this->numeroPrimario, PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() > 0)
        {
            $operaciones = $result->fetchAll(PDO::FETCH_OBJ);

            foreach ( $operaciones as $operacion )
            {
                $operacion->fechaRegistro = date('d-m-Y h:i a', strtotime($operacion->fechaRegistro));
                $operacion->monto = number_format($operacion->monto,2,'.',',') ;
            }
            return $operaciones;
        }
        return [];
    }

    function obtenerOperaciones($roles)
    {
        $this->isAdministrativo($roles);

        if (!$this->administrativo) {
            $query = "SELECT * FROM billcoop_viewDetOperaciones WHERE CAST( fechaRegistro AS DATE )  <= DATEADD(month, 1, cast(fechaRegistro as date)) AND  idUsuario =" . $this->idUsuario . " AND idAgencia IN (" . implode(',', $this->agenciasAsignadas) . ") ORDER BY fechaRegistro DESC";
        } else {
            $query = "SELECT * FROM billcoop_viewDetOperaciones WHERE CAST( fechaRegistro AS DATE ) <= DATEADD(month, 1, cast(fechaRegistro as date)) AND  idAgencia IN (" . implode(',', $this->agenciasAsignadas) . ") ORDER BY fechaRegistro DESC";
        }

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {

            $operaciones = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
            $permisosRoles = $this->obtenerConfiguracion();

            $idsRoles = [];
            foreach ( $roles as $rol )
            {
                array_push( $idsRoles, $rol->id);
            }

            $permisoFinal = new stdClass();
            $permisoFinal->edicion = null;
            $permisoFinal->eliminacion = null;

            foreach ( $permisosRoles as $key => $permiso )
            {
                if( ! in_array($permiso->id, $idsRoles) )
                {
                    unset( $permisosRoles[$key] );

                } else {

                    if ( $permisoFinal->edicion != 'S')
                    {
                        $permisoFinal->edicion = $permiso->edicion;
                    }

                    if ( $permisoFinal->eliminacion != 'S')
                    {
                        $permisoFinal->eliminacion = $permiso->eliminacion;
                    }
                }
            }

            foreach ( $operaciones as $operacion )
            {
                if ( $operacion->edicion === 'S')
                {
                    $operacion->edicion = $permisoFinal->edicion;
                }

                if ( $operacion->eliminacion === 'S')
                {
                    $operacion->eliminacion = $permisoFinal->eliminacion;
                }

                if (true)
                {

                }
            }

            return $operaciones;
        }

        $this->conexion = null;
        return [];
    }

    function obtenerDetalleOperacion()
    {
        $query = 'SELECT * FROM billcoop_viewDetOperacion where id = :id';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__)[0];
        }

        $this->conexion = null;
        return [];
    }

    function construirPDF()
    {
        $pdf = new declaracionPDF();

        $operacion = $this->obtenerDetalleOperacion();

        $pdf->setDetalleOperacion($operacion);

        $pdf->configuracionDocumento();
        $pdf->setMargin();
        $pdf->SetAutoPageBreak(TRUE, 35);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();

        $pdf->setFont('dejavusans', 'B', 10);
        $pdf->Write(0, 'Formulario para operaciones con billetes de alta demoninación', '', 0, 'C', true, 0, false, false, 0);
        $pdf->Write(0, 'Legitimidad de la fuente de procedencia de fondos', '', 0, 'C', true, 0, false, false, 0);
        $pdf->writeHTML("<hr>", true, false, false, false, '');


        $pdf->datosPersonales();
        $pdf->datosOperacion();
        $pdf->declaracion();
        $pdf->firmas();


        $pdf->Output($this->path . $operacion->nombreArchivo, 'F');

        $this->conexion = null;

        if (file_exists($this->path . $operacion->nombreArchivo)) {
            return (object)["respuesta" => "EXITO", "nombreArchivo" => $operacion->nombreArchivo];
        }

        return (object)["respuesta" => "Error al construir archivo", "nombreArchivo" => null];

        //$pdf->Output('MyPDF_File.pdf', 'I');
    }

    function eliminar()
    {
        $respuesta = null;

        $query = 'EXEC billcoop_spEliminarOperación :idApartado, :tipoApartado, :idOperacion, :idUsuario, :ipOrdenador, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoPermiso, PDO::PARAM_STR);
        $result->bindParam(':idOperacion', $this->idOperacion, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        return $respuesta;
    }

    function guardarConfiguracion( $roles )
    {
        $respuesta = null;

        $query = 'EXEC billcoop_spGuardarConfiguracion :idApartado, :tipoApartado, :roles, :idUsuario, :ipOrdenador, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoPermiso, PDO::PARAM_STR);
        $result->bindParam(':roles', $roles, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        return $respuesta;
    }

    function obtenerConfiguracion()
    {
        $query = 'SELECT * FROM billcoop_viewConfiguracion';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_OBJ);
        }

        $this->conexion = null;
        return [];
    }

    function rptPersona( $desde, $hasta )
    {
        $query = 'SELECT * FROM billcoop_viewRptPersona 
                        WHERE ( idTipoDocumento = :idTipoDocumento AND numeroDocumento = :numeroDocumento ) 
                        OR ( idTipoDocumento = :TidTipoDocumento AND TnumeroPrimario = :TnumeroDocumento ) 
                        AND CAST(fechaRegistro AS DATE) BETWEEN :desde AND :hasta ORDER BY fechaRegistro DESC';

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idTipoDocumento', $this->tipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumento', $this->numeroPrimario, PDO::PARAM_STR);
        $result->bindParam(':TidTipoDocumento', $this->tipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':TnumeroDocumento', $this->numeroPrimario, PDO::PARAM_STR);
        $result->bindParam(':desde', $desde, PDO::PARAM_STR);
        $result->bindParam(':hasta', $hasta, PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() > 0) {
            $operaciones = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);

            $totalBilletesCincientaTitular = 0; $cantidadBilletesCincuentaTitular = 0;
            $totalBilletesCienTitular = 0; $cantidadBilletesCienTitular = 0;

            $totalBilletesCiencuentaEnviados = 0; $cantidadBilletesCincuentaEnviados = 0;
            $totalBilletesCienEnviados = 0; $cantidadBilletesCienEnviados = 0;

            $totalBilletesCincuentaRecibidos = 0; $cantidadBilletesCincuentaRecibidos = 0;
            $totalBilletesCienRecibidos = 0; $cantidadBilletesCienRecibidos = 0;

            foreach ( $operaciones as $operacion )
            {
                if ( $this->numeroPrimario == $operacion->TnumeroPrimario && $this->numeroPrimario == $operacion->numeroDocumento && $operacion->tipoProducto != 'Envío de remesa')
                {
                    $cantidadBilletesCincuentaTitular += (int) $operacion->billetesCincuenta;
                    $cantidadBilletesCienTitular += (int) $operacion->billetesCien;

                } else if ($this->numeroPrimario == $operacion->numeroDocumento && $this->numeroPrimario != $operacion->TnumeroPrimario)
                {
                    $cantidadBilletesCincuentaRecibidos += (int) $operacion->billetesCincuenta;
                    $cantidadBilletesCienRecibidos += (int) $operacion->billetesCien;
                } else {
                    $cantidadBilletesCincuentaEnviados += (int) $operacion->billetesCincuenta;
                    $cantidadBilletesCienEnviados += (int) $operacion->billetesCien;
                }
            }

            $totalBilletesCincientaTitular = number_format( $cantidadBilletesCincuentaTitular * 50, 2, '.', ',' );
            $totalBilletesCienTitular = number_format( $cantidadBilletesCienTitular *  100, 2, '.', ',' );

            $totalBilletesCincuentaRecibidos = number_format( $cantidadBilletesCincuentaRecibidos * 50, 2, '.', ',' );
            $totalBilletesCienRecibidos = number_format( $cantidadBilletesCienRecibidos *  100, 2, '.', ',' );

            $totalBilletesCiencuentaEnviados = number_format( $cantidadBilletesCincuentaEnviados * 50, 2, '.', ',' );
            $totalBilletesCienEnviados = number_format( $cantidadBilletesCienEnviados *  100, 2, '.', ',' );

            return (Object) [
                'operaciones' => $operaciones,
                'billetesCincuenta' => [
                    'totalTitular' => $totalBilletesCincientaTitular,
                    'cantidadTitular' => $cantidadBilletesCincuentaTitular,
                    'totalRecibidos' => $totalBilletesCincuentaRecibidos,
                    'cantidadRecibidos' => $cantidadBilletesCincuentaRecibidos,
                    'totalBilletesEnviados' => $totalBilletesCiencuentaEnviados,
                    'cantidadBilletesEnviados' => $cantidadBilletesCincuentaEnviados
                ],
                'billetesCien' => [
                    'totalTitular' => $totalBilletesCienTitular,
                    'cantidadTitular' => $cantidadBilletesCienTitular,
                    'totalRecibidos' => $totalBilletesCienRecibidos,
                    'cantidadRecibidos' => $cantidadBilletesCienRecibidos,
                    'totalBilletesEnviados' => $totalBilletesCienEnviados,
                    'cantidadBilletesEnviados' => $cantidadBilletesCienEnviados
                ],
                'totalTitular' => number_format( ($cantidadBilletesCincuentaTitular * 50) + ($cantidadBilletesCienTitular *  100), 2, '.', ','),
                'totalRecibidos' => number_format( ($cantidadBilletesCincuentaRecibidos * 50) + ($cantidadBilletesCienRecibidos *  100), 2, '.', ','),
                'totalEnviados' => number_format( ($cantidadBilletesCincuentaEnviados * 50) + ($cantidadBilletesCienEnviados *  100), 2, '.', ',')
            ];
        }

        return (Object) [
            'operaciones' => [],
            'billetesCincuenta' => [
                'totalTitular' => 0,
                'cantidadTitular' => 0,
                'totalRecibidos' => 0,
                'cantidadRecibidos' => 0,
                'totalBilletesEnviados' => 0,
                'cantidadBilletesEnviados' => 0
            ],
            'billetesCien' => [
                'totalTitular' => 0,
                'cantidadTitular' => 0,
                'totalRecibidos' => 0,
                'cantidadRecibidos' => 0,
                'totalBilletesEnviados' => 0,
                'cantidadBilletesEnviados' => 0
            ],
            'totalTitular' =>  0,
            'totalRecibidos' => 0,
            'totalEnviados' =>  0
        ];
    }

    function rptAgencias( $desde, $hasta )
    {
        $query = 'SELECT * FROM billcoop_viewRptPersona WHERE idAgencia IN('.implode(',', $this->agenciasAsignadas).') AND CAST(fechaRegistro AS DATE) BETWEEN :desde AND :hasta';

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':desde', $desde, PDO::PARAM_STR);
        $result->bindParam(':hasta', $hasta, PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() > 0)
        {
            $operaciones = $result->fetchAll(PDO::FETCH_OBJ);
            $operacionesTemp = (Object)[];
            $productos = (Object)[];

            foreach ( $operaciones as $operacion )
            {
                if ( property_exists($operacionesTemp, $operacion->idAgencia ) )
                {
                    $operacionesTemp->{$operacion->idAgencia}->billetesCincuenta += (int) $operacion->billetesCincuenta;
                    $operacionesTemp->{$operacion->idAgencia}->billetesCien += (int) $operacion->billetesCien;

                    $operacionesTemp->{$operacion->idAgencia}->totalCincuenta += (int) $operacion->billetesCincuenta * 50;
                    $operacionesTemp->{$operacion->idAgencia}->totalCien += (int) $operacion->billetesCien * 100;

                } else
                {
                    $operacionesTemp->{$operacion->idAgencia} = (Object)[
                        'idAgencia' => $operacion->idAgencia,
                        'agencia' => $operacion->agencia,
                        'billetesCincuenta' => (int) $operacion->billetesCincuenta,
                        'totalCincuenta' => ( (int) $operacion->billetesCincuenta * 50 ),
                        'billetesCien' => (int) $operacion->billetesCien,
                        'totalCien' => ( (int) $operacion->billetesCien * 100 )
                    ];
                }

                if ( property_exists( $productos, $operacion->idProducto ) )
                {
                    $productos->{$operacion->idProducto}->billetesCincuenta += (int) $operacion->billetesCincuenta;
                    $productos->{$operacion->idProducto}->billetesCien += (int) $operacion->billetesCien;

                    $productos->{$operacion->idProducto}->totalCincuenta += (int) $operacion->billetesCincuenta * 50;
                    $productos->{$operacion->idProducto}->totalCien += (int) $operacion->billetesCien * 100;
                } else
                {
                    $productos->{$operacion->idProducto} = (Object) [
                        'idProducto' => $operacion->idProducto,
                        'tipoProducto' => $operacion->tipoProducto,
                        'billetesCincuenta' => (int) $operacion->billetesCincuenta,
                        'totalCincuenta' => ( (int) $operacion->billetesCincuenta * 50 ),
                        'billetesCien' => (int) $operacion->billetesCien,
                        'totalCien' => ( (int) $operacion->billetesCien * 100 )
                    ];
                }
            }
            return (Object) [ 'agencias' => $operacionesTemp, 'productos' => $productos ];
        }

        return (Object)[ 'agencias' => [], 'productos' => [] ];
    }

    function rptGeneral( $desde, $hasta )
    {
        $idAgencias = implode(',', $this->agenciasAsignadas );

        $query = "SELECT * FROM billcoop_viewRptPersona WHERE idAgencia IN ( $idAgencias ) AND CAST(fechaRegistro AS DATE) BETWEEN :desde AND :hasta ORDER BY fechaRegistro DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':desde', $desde, PDO::PARAM_STR);
        $result->bindParam(':hasta', $hasta, PDO::PARAM_STR);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        return [];
    }
}


class declaracionPDF extends TCPDF
{
    private $operacion;

    function setDetalleOperacion($detOperacion)
    {
        $this->operacion = $detOperacion;
    }

    function configuracionDocumento()
    {
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('Departamento IT');
        $this->SetTitle('Solicitud de afiliación PEP');
        $this->SetSubject('ACACYPAC');
        $this->SetKeywords('TCPDF, PDF, Solicitud de afiliación PEP');

        $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    }

    function setMargin()
    {
        $this->SetMargins(15, 15, 15, 15);
        $this->SetHeaderMargin(4);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);
    }

    function Header()
    {
        $this->setFont('dejavusans', 'B', 10);
        $this->Cell(185, 6, 'Formulario N° ' . str_pad($this->operacion->id, 7, '0', STR_PAD_LEFT), 0, 0, 'R', 0);
    }

    function Footer()
    {
        $this->setFont('dejavusans', 'B', 10);
        $this->Write(0, $this->operacion->agencia . ' | ' . $this->operacion->fechaRegistro, '', 0, 'R', true, 0, false, false, 0);
    }

    function datosPersonales()
    {
        $this->ln(5);

        $this->SetFillColor(0, 128, 57);
        $this->SetTextColor(255);

        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B', 7);

        $this->Cell(175, 7, 'Datos personales de quién realiza la operación físicamente', 1, 0, 'C', 1);
        $this->Cell(10, 7, '', 1, 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Nombre', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->operacion->nombres . ' ' . $this->operacion->apellidos, 'LR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Código de cliente', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->operacion->codigoCliente, 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Documento de identificación', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->operacion->tipoDocumento . ' : ' . $this->operacion->numeroDocumento, 'LR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Contacto', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);

        $telefono = '';

        if (!empty($this->operacion->telefonoFijo)) {
            $telefono = $this->operacion->telefonoFijo;
        }

        if (!empty($telefono) && !empty($this->operacion->telefonoMovil)) {
            $telefono = $telefono . ' / ' . $this->operacion->telefonoMovil;

        } else if (empty($telefono) && !empty($this->operacion->telefonoMovil)) {
            $telefono = $this->operacion->telefonoMovil;
        }

        $this->Cell(125, 7, $telefono, 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 9, 'Dirección', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->writeHTMLCell(125, 9, '', '', '<p style="text-align: justify">' . $this->operacion->direccion . ', ' . $this->operacion->municipio . ',' . $this->operacion->departamento . '</p>', 'LR', 1, 0, true, 'L', 1);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Actividad económica', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->operacion->actividadEconomica, 'LR', 0, 'L', 1);
        $this->ln();

        if ( $this->operacion->tipoProducto != 'Envío de remesa')
        {
            $this->Cell(60, 7, "¿Es el titular del ".$this->operacion->tipo." ?", 'TLR', 0, 'C', 0);
            $this->Cell(125, 7, $this->operacion->isTitular == 'S' ? 'SI' : 'NO', 'TR', 0, 'L', 0);
        } else
        {
            $this->Cell(60, 7, "¿El remitente es el mismo beneficiario?", 'TLR', 0, 'C', 0);
            $this->Cell(125, 7, $this->operacion->isTitular == 'S' ? 'SI' : 'NO', 'TR', 0, 'L', 0);
        }
        $this->ln();

        if ($this->operacion->isTitular == 'N' && $this->operacion->tipoProducto != 'Envío de remesa') {
            $this->SetFillColor(0, 128, 57);
            $this->SetTextColor(255);

            $this->SetDrawColor(16, 126, 20);
            $this->SetLineWidth(0.3);
            $this->SetFont('dejavusans', 'B', 7);

            $this->Cell(175, 7, 'Datos del titular', 1, 0, 'C', 1);
            $this->Cell(10, 7, '', 1, 0, 'C', 1);
            $this->ln();

            $this->SetFillColor(224, 235, 255);
            $this->SetTextColor(0);

            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(60, 7, 'Nombre', 'TLR', 0, 'L', 0);
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(125, 7, $this->operacion->Tnombres.' '.$this->operacion->Tapellidos, 'TLR', 0, 'L', 0);
            $this->ln();

            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(60, 7, 'Código de cliente', 'LR', 0, 'L', 1);
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(125, 7, $this->operacion->TcodigoCliente, 'LR', 0, 'L', 1);
            $this->ln();

            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(60, 7, 'Documento de identificación', 'LR', 0, 'L', 0);
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(125, 7, $this->operacion->TtipoDocumento . ' ' . $this->operacion->TnumeroPrimario, 'LR', 0, 'L', 0);
            $this->ln();

            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(60, 7, 'Actividad económica', 'LR', 0, 'L', 1);
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(125, 7, $this->operacion->TactividadEconomica, 'LR', 0, 'L', 1);
            $this->ln();

        } else if ( $this->operacion->isTitular == 'N' && $this->operacion->tipoProducto == 'Envío de remesa')
        {
            $this->SetFillColor(0, 128, 57);
            $this->SetTextColor(255);

            $this->SetDrawColor(16, 126, 20);
            $this->SetLineWidth(0.3);
            $this->SetFont('dejavusans', 'B', 7);

            $this->Cell(175, 7, 'Datos del beneficiario', 1, 0, 'C', 1);
            $this->Cell(10, 7, '', 1, 0, 'C', 1);
            $this->ln();

            $this->SetFillColor(224, 235, 255);
            $this->SetTextColor(0);

            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(60, 7, 'Nombre completo', 'TLR', 0, 'L', 0);
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(125, 7, $this->operacion->beneficiario, 'TLR', 0, 'L', 0);
            $this->ln();

            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(60, 7, 'País de destino', 'LR', 0, 'L', 1);
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(125, 7, $this->operacion->paisDestino, 'LR', 0, 'L', 1);
            $this->ln();

            $this->SetFont('dejavusans', 'B', 7);
            $this->Cell(60, 7, 'Destino de los fondos', 'LR', 0, 'L', 0);
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(125, 7, $this->operacion->destino, 'LR', 0, 'L', 0);
            $this->ln();

        }

        $this->Cell(185, 0, '', 'T');
        $this->ln();
    }

    function datosOperacion()
    {
        $this->ln(3);

        $this->SetFillColor(0, 128, 57);
        $this->SetTextColor(255);

        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B', 7);

        $this->Cell(175, 7, 'Datos de la operación', 1, 0, 'C', 1);
        $this->Cell(10, 7, '', 1, 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(41, 7, 'Producto/Servicio', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(52, 7, $this->operacion->tipoProducto, 'LR', 0, 'C', 0);
        $this->SetFont('dejavusans', 'B', 7);

        if ( $this->operacion->tipo == 'PRODUCTO' )
        {
            $cuenta = 'Producto';
        } else
        {
            $cuenta = 'referencia';
        }

        $this->Cell(41, 7, "Número de $cuenta", 'LR', 0, 'C', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(51, 7, $this->operacion->numeroCuenta, 'LR', 0, 'C', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(41, 7, 'Tipo de operación', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(144, 7, $this->operacion->tipoOperacion, 'TLR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(41, 7, 'Procedencia de los fondos', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(144, 7, $this->operacion->procedencia, 'TLR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(41, 7, 'Monto de la operación', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(51, 7, '$' . $this->operacion->monto, 'TLR', 0, 'C', 0);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(41, 7, 'Monto de recibido', 'TLR', 0, 'C', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(52, 7, '$' . $this->operacion->montoRecibido, 'TLR', 0, 'C', 0);
        $this->ln();

        $this->SetFillColor(0, 128, 57);
        $this->SetTextColor(255);

        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B', 7);

        $this->Cell(185, 7, 'Detalle de billetes de alta demoninación incluidos en la operación', 1, 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(62, 6, 'Número de billetes', 'TLR', 0, 'C', 0);
        $this->Cell(62, 6, 'Denominación', 'TLR', 0, 'C', 0);
        $this->Cell(61, 6, 'Monto por denominación', 'TLR', 0, 'C', 0);
        $this->ln();

        $totalCincuenta = (int)$this->operacion->billetesCincuenta * 50;

        $this->SetFont('dejavusans', '', 7);
        $this->Cell(62, 6, $this->operacion->billetesCincuenta, 'TLR', 0, 'C', 0);
        $this->Cell(62, 6, '$50', 'TLR', 0, 'C', 0);
        $this->Cell(61, 6, '$' . number_format( ($totalCincuenta), 2, '.', ','), 'TLR', 0, 'C', 0);
        $this->ln();


        $totalCien = (int)$this->operacion->billetesCien * 100;

        $this->Cell(62, 6, $this->operacion->billetesCien, 'LR', 0, 'C', 0);
        $this->Cell(62, 6, '$100', 'LR', 0, 'C', 0);
        $this->Cell(61, 6, '$' . number_format( ($totalCien), 2, '.', ','), 'LR', 0, 'C', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(124, 6, 'Monto total', 'TLR', 0, 'R', 0);
        $this->Cell(61, 6, '$' . number_format( ($totalCincuenta + $totalCien), 2, '.', ','), 'TLR', 0, 'C', 0);
        $this->ln();

        $this->Cell(185, 0, '', 'T');

    }

    function declaracion()
    {
        $declaracion = 'Declaro que toda información proporcionada es fidedigna; así como, me comprometo a proporcionar toda
        la documentación e información requerida por ACACYPAC DE R.L. para comprobar el origen o destino de los fondos,
        así como cualquier otro dato de interés para propósitos de prevención del lavado de dinero y de activos,
        financiación al terrorismo y a la proliferación de armas de destrucción masiva.';

        $this->ln(10);

        $this->SetFont('dejavusans', '', 7);
        $this->writeHTMLCell(185, 15, '', '', '<p style="text-align: justify">' . $declaracion . '</p>', '', 1, 0, true, 'L', 0);

    }

    function firmas()
    {
        $this->ln(20);

        $this->setFont('dejavusans', 'B', 9);
        $this->Cell(92, 6, '______________________________________', false, 0, 'C', 0);
        $this->Cell(92, 6, '______________________________________', false, 0, 'C', 0);
        $this->ln();

        $this->setFont('dejavusans', 'B', 6);
        $this->Cell(92, 6, $this->operacion->usuario, false, 0, 'C', 0);
        $this->Cell(92, 6, 'Firma del cliente', false, 0, 'C', 0);
        $this->ln();
    }
}


class EnviarEmail
{
    private $config;
    private $apiInstance;
    private $receptores = [  ['email' => 'jonathan.alvarado@acacypac.coop', 'name' => 'Jonathan Alfonso Alvarado'] ];
    private $cc = [ [ 'email' => 'miguel.calles@acacypac.coop', 'name' => 'Miguel Calles' ] ];
    private $idTemplate;
    private $params = [];

    function __construct( $idTemplate )
    {
        $this->config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');

        $this->apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(
            new GuzzleHttp\Client(),
            $this->config
        );
        $this->idTemplate = $idTemplate;
    }

    function alertaPorMonto()
    {
        $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
        $sendSmtpEmail['to'] = $this->receptores;
        //$sendSmtpEmail['cc'] = $this->cc;
        $sendSmtpEmail['templateId'] = $this->idTemplate;
        $sendSmtpEmail['params'] = $this->params;
        $sendSmtpEmail['headers'] = array('X-Mailin-custom' => 'Departamento de IT - ACACYPAC');

        try {
            $result = $this->apiInstance->sendTransacEmail($sendSmtpEmail);
            return  print_r($result, true);
        } catch (Exception $e) {
           return 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ' . $e->getMessage();
        }
    }

    function alertPorOperaciones ()
    {
        $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
        $sendSmtpEmail['to'] = $this->receptores;
        $sendSmtpEmail['cc'] = $this->cc;
        $sendSmtpEmail['templateId'] = $this->idTemplate;
        $sendSmtpEmail['params'] = $this->params;
        $sendSmtpEmail['headers'] = array('X-Mailin-custom' => 'Departamento de IT - ACACYPAC');

        try {
            $result = $this->apiInstance->sendTransacEmail($sendSmtpEmail);
            return print_r($result, true);
        } catch (Exception $e) {
            return 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ' . $e->getMessage();
        }
    }

    function setBody( $params )
    {
        $this->params = $params;
    }
}


