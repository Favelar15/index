<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";
session_start();

$respuesta = [];


if ( isset($_SESSION['index'])  && $_SESSION['index']->locked )
{

    if ( !empty( $input['numeroDocumento']) && !empty( $input['tipoDocumento'] ))
    {
        require_once "../../code/connectionSqlServer.php";
        require_once 'Models/Permiso.php';
        require_once  '../../code/Models/asociado.php';
        require_once 'Models/Persona.php';

        $persona = new Persona();
        $persona->numeroPrimario = $input['numeroDocumento'];
        $persona->tipoDocumento = $input['tipoDocumento'];
        $respuesta['respuesta'] = $persona->buscarPersona();
    } else
    {
        $respuesta['respuesta'] = 'Tipo de documento ó número de documento invalido';
    }

} else {
    $respuesta["respuesta"] = "SESION";
}

echo json_encode( $respuesta );