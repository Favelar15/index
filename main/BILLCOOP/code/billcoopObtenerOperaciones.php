<?php

header("Content-type: application/json; charset=utf-8");
include "../../code/generalParameters.php";
$input = json_decode(file_get_contents("php://input"), true);

session_start();

$respuesta = [ 'operaciones' => [] ];

if ( isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../code/connectionSqlServer.php';
    require_once 'Models/Permiso.php';
    require_once 'Models/Persona.php';

    if ( !empty( $input['idOperacion'] ) )
    {
        $operacion = new OperacionBillcoop();
        $operacion->id = (int) $input['idOperacion'];
        $respuesta['operacion'] = $operacion->obtenerDetalleOperacion();
    } else
    {
        $idAgencias = [];

        foreach ( $_SESSION['index']->agencias as $agencia )
        {
            array_push( $idAgencias, (int) $agencia->id);
        }

        $roles = (array) $_SESSION["index"]->roles;

        $operacion = new OperacionBillcoop();
        $operacion->setIdApartado( (int) base64_decode( urldecode( $input['idApartado'] )) );
        $operacion->setTipoApartado( $input['tipoApartado'] );
        $operacion->setAgencias( $idAgencias );
        $operacion->idUsuario = $_SESSION['index']->id;
        $respuesta['operaciones'] = $operacion->obtenerOperaciones( $roles );
    }
}

echo json_encode( $respuesta );

