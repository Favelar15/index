<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Reporte de operaciones BILLCOOP</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">BILLCOOP</li>
            <li class="breadcrumb-item">Reporte</li>
            <li class="breadcrumb-item active">Por persona</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">

    <form action="javascript:rptPersona.generarReporte()" id="formRptPersona" name="formRptPersona"
          accept-charset="utf-8" method="POST" class="needs-validation" novalidate>

        <div class="row">

            <div class="col-lg-3 col-xl-3">
                <label for="cboTipoDocumento" class="form-label">Tipo de documento: <span
                            class="requerido">*</span></label>
                <select onchange="generalMonitoreoTipoDocumento(this.value, 'txtDocumentoPrimario')"
                        class="form-control selectpicker cboTipoDocumento tipoPrimario" id="cboTipoDocumento" required>
                    <option selected value=''>Seleccione</option>
                </select>
                <div class="invalid-feedback">
                    Seleccione el tipo de documento
                </div>
            </div>

            <div class="col-lg-3 col-xl-3">
                <label for="txtDocumentoPrimario" class="form-label">Número de documento <span
                            class="requerido">*</span></label>
                <div class="input-group has-validation">
                    <input onkeypress="return generalSoloNumeros(event);" type="text" class="form-control"
                           id="txtDocumentoPrimario" name="txtDocumentoPrimario" placeholder="Ingrese el numero"
                           required readonly>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label class="form-label" for="txtInicio">Desde <span class="requerido">*</span> </label>
                <input type="text" class="form-control fechaFinLimitado readonly" onchange="rptPersona.cambioFecha()"
                       id="txtInicio" name="txtInicio" placeholder="Fecha de inicio" required>
                <div class="invalid-feedback">
                    Seleccione la fecha de inicio
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label class="form-label" for="txtFinG">Hasta <span class="requerido">*</span></label>
                <input type="text" class="form-control fechaFinLimitado readonly" id="txtFin" name="txtFin"
                       placeholder="Fecha final" required>
                <div class="invalid-feedback">
                    Seleccione la fecha final
                </div>
            </div>

            <div class="col-lg- col-xl- mt-3" align="center">
                <button type="submit" class="btn btn-sm btn-outline-primary">
                    <i class="fas fa-search"></i> Generar reporte
                </button>
                <button type="button" class="btn btn-sm btn-outline-danger" id="btnCancelar" onclick="rptPersona.limpiar()">
                    <i class="fas fa-times-circle"></i> Limpiar
                </button>
            </div>

        </div>
    </form>

    <hr class="mb-1 mt-3">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="solicitudesPep-tab" data-bs-toggle="tab" data-bs-target="#operaciones"
                    role="tab" aria-controls="Solicitudes de PEP" aria-selected="true">
                Billetes por denominación
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="btnExcelPep">
                <button type="button" id="btnExportar" onclick="rptPersona.exportarReporte()" class="btn btn-sm btn-outline-success disabled">
                    <i class="fa fa-file-excel"></i> Exportar
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="operaciones" role="tabpanel" aria-labelledby="operaciones-tab">

            <table class="table table-striped table-bordered" id="tblBilletes">
                <thead>
                    <tr class="text-center">
                        <th>Denominación</th>
                        <th colspan="2">Hechas por Titular dela cuenta</th>
                        <th colspan="2">Recibidos de terceros</th>
                        <th colspan="2">Enviados a terceros</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>N°</th>
                        <th>SubTotal</th>
                        <th>N°</th>
                        <th>SubTotal</th>
                        <th>N°</th>
                        <th>SubTotal</th>
                    </tr>
                </thead>
                <body>
                    <tr id="trBilletesCincuenta" >
                        <td>Billetes de $50</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td>0</td>
                        <td>$0.00</td>
                    </tr>

                    <tr id="trBilletesCien" >
                        <td>Billetes de $100</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td>0</td>
                        <td>$0.00</td>
                    </tr>
                    <tr id="trTotalBilletes">
                        <td></td>
                        <td>Total:</td>
                        <td>$0.00</td>
                        <td>Total:</td>
                        <td>$0.00</td>
                        <td>Total:</td>
                        <td>$0.00</td>
                    </tr>
                </body>
            </table>
        </div>
    </div>

    <br>
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="solicitudesPep-tab" data-bs-toggle="tab" data-bs-target="#operaciones"
                    role="tab" aria-controls="Solicitudes de PEP" aria-selected="true">
                Operaciones BILLCOOP
            </button>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="operaciones" role="tabpanel" aria-labelledby="operaciones-tab">
            <table class="table table-bordered table-striped" id="tblOperaciones">
                <thead>
                <th>N°</th>
                <th>Nombre del titular</th>
                <th>Documento de identificación</th>
                <th>Agencia</th>
                <th>Monto</th>
                <th>Billetes de $50</th>
                <th>Billetes de $100</th>
                <th>Fecha de registro</th>
                </thead>
            </table>
        </div>
    </div>

</section>

<?php
$_GET['js'] = ['billcoopRptPersona'];