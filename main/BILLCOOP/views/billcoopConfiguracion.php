<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Configuración BILLCOOP</h1>
        </div>
        <div class="col-3 topBtnContainer">
            <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configRoles.cancelar()">
                <i class="fas fa-times"></i> <span>Cancelar</span>
            </button>
            <button class="btn btn-sm btn-outline-success float-end" onclick="configRoles.guardar()" title="Guardar" type="button">
                <i class="fas fa-save"></i> <span>Guardar</span>
            </button>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">BILLCOOP</li>
            <li class="breadcrumb-item active">Configuración</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">


    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="listadoCreditos-tab" data-bs-toggle="tab" data-bs-target="#listadoCreditos" role="tab" aria-controls="Lista de creditos" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','primaryButtons');">
                Configuración de permisos
            </button>
        </li>
        <li class="nav-item ms-auto" >
            <div class="tabButtonContainer" id="primaryButtons">
                <button type="button" onclick="configRoles.mostrarModal()" class="btn btn-sm btn-outline-dark">
                    <i class="fa fa-plus"></i> Agregar Rol
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active " id="listadoCreditos" role="tabpanel" aria-labelledby="evaluacion-tab">
            <table class="table-striped table table-bordered" id="tblConfiguracion">
                <thead>
                    <th>N°</th>
                    <th>Rol</th>
                    <th>Edición</th>
                    <th>Eliminación</th>
                    <th>Acciones</th>
                </thead>
                <body></body>
            </table>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-configuracion" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Configuración de roles</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <form action="javascript:configRoles.agregar()" id="formConfiguracion"
                  accept-charset="utf-8" method="POST"
                  name="formConfiguracion" class="needs-validation" novalidate>

                <div class="modal-body">

                    <div class="row">

                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for="cboRoles">
                               Seleccione un rol: <span class="requerido">*</span> </label>
                            <select class="selectpicker form-control cboRoles"
                                    id="cboRoles" title="seleccione" required
                                    name="cboRoles" data-live-search="true">
                                <option value="" selected disabled>Seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione el rol
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for="cboEdicion">
                                ¿Puede editar?: <span class="requerido">*</span> </label>
                            <select class="selectpicker form-control cboEdicion"
                                    id="cboEdicion" required
                                    name="cboEdicion">
                                <option value="" disabled selected>Seleccione</option>
                                <option value="S">SI</option>
                                <option value="N">NO</option>
                            </select>
                            <div class="invalid-feedback">
                               Seleccione la acción
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for="cboEliminacion">
                                ¿Puede eliminar?: <span class="requerido">*</span> </label>
                            <select class="selectpicker form-control cboEliminacion"
                                    id="cboEliminacion" required name="cboEliminacion">
                                <option value="" selected disabled>Seleccione</option>
                                <option value="S">SI</option>
                                <option value="N">NO</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione la acción
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-outline-primary btn-sm">
                        <i class="fa fa-save"></i> Guardar
                    </button>

                    <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>

                </div>
            </form>
        </div>
    </div>
</div>


<?php

$_GET['js'] = ['billcoopConfiguracion'];
