<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Formulario BILLCOOP</h1>
        </div>
        <div class="col-4 topBtnContainer">
            <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar"
                    onclick="configFormulario.salir()">
                <i class="fas fa-times"></i> <span>Cancelar</span>
            </button>
            <button class="btn btn-sm btn-outline-success float-end" form="frmAsociado" title="Guardar" type="submit">
                <i class="fas fa-save"></i> <span>Guardar</span>
            </button>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">BILLCOOP</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Formulario BILLCOOP</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">

    <form action="javascript:buscarPersona.buscarPersona()" class="needs-validation" novalidate id="frmAsociado"
          name="frmAsociado" accept-charset="utf-8" method="post">

        <div class="row">

            <div class="col-lg-3 col-xl-3">
                <label for="cboTipoDocumento" class="form-label">Tipo de documento: <span
                            class="requerido">*</span></label>
                <select onchange="buscarPersona.selectTipoDocumento(this.value)"
                        class="form-control selectpicker cboTipoDocumento tipoPrimario" id="cboTipoDocumento" required>
                    <option selected value=''>Seleccione</option>
                </select>
                <div class="invalid-feedback">
                    Seleccione el tipo de documento
                </div>
            </div>

            <div class="col-lg-3 col-xl-3">
                <label for="txtDocumentoPrimario" class="form-label">Número de documento <span
                            class="requerido">*</span></label>
                <div class="input-group has-validation">
                    <input onkeypress="return generalSoloNumeros(event);" type="text" class="form-control"
                           id="txtDocumentoPrimario" name="txtDocumentoPrimario" placeholder="Número de identificación"
                           required readonly>
                    <div class="input-group-append">
                        <button class="input-group-text btn btn-outline-dark disabled" onclick="buscarPersona.buscarPersona()"
                                type="button"
                                id="btnBuscarPersona"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-xl-3">
                <label for="txtCodigoCliente" class="form-label">Código de cliente</label>
                <input type="text" placeholder="Código de cliente" id="txtCodigoCliente" name="txtCodigoCliente"
                       class="form-control" readonly>
            </div>
            <div class="col-lg-3 col-xl-3 mayusculas">
                <label for="txtNombres" class="form-label">Nombres: <span class="requerido">*</span></label>
                <input type="text" id="txtNombres" name="txtNombres" class="form-control" placeholder="Nombres"
                       required readonly>
                <div class="invalid-feedback">
                    Ingrese los nombres
                </div>
            </div>
            <div class="col-lg-3 col-xl-3 mayusculas">
                <label for="txtApellidos" class="form-label">Apellidos: <span class="requerido">*</span></label>
                <input type="text" id="txtApellidos" name="txtApellidos" class="form-control" placeholder="Apellidos"
                       required readonly>
                <div class="invalid-feedback">
                    Ingrese los apellidos
                </div>
            </div>
            <div class="col-lg-6 col-xl-6">
                <label for="cboActividadEconomica" class="form-label">Actividad ecónomica primaria: <span
                            class="requerido">*</span></label>
                <div class="form-group has-validation">
                    <select disabled title="Seleccione" class="form-control selectpicker cboActividadEconomica"
                            data-live-search="true" id="cboActividadEconomica"
                            name="cboActividadEconomica" required data-live-search="true">
                        <option value="" selected>Seleccione</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtTelefonoFijo" class="form-label">Teléfono fijo:</label>
                <input type="text" placeholder="0000-0000" id="txtTelefonoFijo" name="txtTelefonoFijo"
                       class="form-control telefono" readonly>
                <div class="invalid-feedback">
                    Ingrese el teléfono fijo
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtTelefonoMovil" class="form-label">Teléfono movil:</label>
                <input type="text" placeholder="0000-0000" id="txtTelefonoMovil" name="txtTelefonoMovil"
                       class="form-control telefono" readonly>
                <div class="invalid-feedback">
                    Ingrese el teléfono móvil
                </div>
            </div>

            <div class="col-lg-3 col-xl-3">
                <label for="cboPais" class="form-label">País: <span class="requerido">*</span></label>
                <select disabled title="Seleccione" class="form-control selectpicker cboPais" data-live-search="true"
                        id="cboPais" name="cboPais" onchange="generalMonitoreoPais(this.value,'cboDepartamento');"
                        required>
                    <option value="" selected disabled>Seleccione</option>
                </select>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboDepartamento" class="form-label">Departamento de residencia: <span
                            class="requerido">*</span> </label>

                <select disabled title="Seleccione" class="form-control selectpicker" data-live-search="true"
                        id="cboDepartamento" name="cboDepartamento"
                        onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');" required
                        data-live-search="true">
                    <option value="" selected disabled>seleccione un país</option>
                </select>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboMunicipio" class="form-label">Municipio de residencia: <span
                            class="requerido">*</span></label>
                <div class="form-group has-validation">
                    <select disabled title="Seleccione" class="form-control selectpicker" data-live-search="true"
                            id="cboMunicipio" name="cboMunicipio" required data-live-search="true">
                        <option selected value="" disabled>seleccione un departamento</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-12 col-xl-12 mayusculas">
                <label for="txtDireccion" class="form-label">Dirección completa: <span class="requerido">*</span>
                </label>
                <input readonly type="text" class="form-control" id="txtDireccion" name="txtDireccion"
                       placeholder="Detalle la dirección" required/>
                <div class="invalid-feedback">
                    Ingrese la dirección completa
                </div>
            </div>
        </div>
    </form>

    <br>
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="datosTransaccion-tab" data-bs-toggle="tab"
                    data-bs-target="#datosTransaccion" role="tab" aria-controls="Datos de la transacción"
                    aria-selected="true">
                Datos de la transacción
            </button>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="datosTransaccion" role="tabpanel"
             aria-labelledby="evaluacion-tab">
            <form action="javascript:configFormulario.guardar()" class="needs-validation" novalidate id="formTransaccion"
                  name="formTransaccion"
                  accept-charset="utf-8" method="post">
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label for="cboTipoProducto" class="form-label">Productos/Servicio: <span
                                    class="requerido">*</span></label>
                        <select disabled onchange="configFormulario.seleccionTipoProducto(this.value)"
                                class="form-control selectpicker cboTipoProducto" id="cboTipoProducto" required>
                            <option selected disabled value=''>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un producto ó servicio
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label for="txtNumeroCuenta" class="form-label">Número de cuenta/referencia: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtNumeroCuenta" name="txtNumeroCuenta"
                               class="form-control"
                               placeholder="Ingrese el número" required readonly>
                        <div class="invalid-feedback">
                            Ingrese el numero de cuenta/referencia
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label for="txtTipoOperacion" class="form-label">Tipo de operación: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtTipoOperacion" name="txtTipoOperacion" class="form-control"
                               placeholder="Descripción de la transacción" required readonly>
                        <div class="invalid-feedback">
                            Seleccione el tipo de operación
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label for="txtProcedencia" class="form-label">Procedencia de los fondos: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtProcedencia" name="txtProcedencia" class="form-control"
                               placeholder="Detalle la procedencia" required readonly>
                        <div class="invalid-feedback">
                            Ingrese la procedencia de los fondos
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label for="txtMonto" class="form-label">Monto de la operación: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtMonto" name="txtMonto" class="form-control" placeholder="USD"
                               required readonly onkeypress="return generalSoloNumeros(event);">
                        <div class="invalid-feedback">
                            Ingrese el monto total
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label for="txtMontoRecibido" class="form-label">Monto recibido: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtMontoRecibido" name="txtMontoRecibido" class="form-control"
                               placeholder="USD" onkeypress="return generalSoloNumeros(event);"
                               required readonly>
                        <div class="invalid-feedback">
                            Ingrese el monto recibido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="cboEsTitular" class="form-label">¿Es el titular? <span
                                    class="requerido">*</span></label>
                        <select onchange="configTitular.isTitular(this.value)"
                                class="form-select cboEsTitular" id="cboEsTitular" disabled required>
                            <option selected disabled value=''>seleccione</option>
                            <option value='S'>SI</option>
                            <option value='N'>NO</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione
                        </div>
                    </div>
                </div>
            </form>

            <br>
            <table class="table table-striped table-bordered" id="tblBilletes">
                <thead>
                <th>N°</th>
                <th>Número de billetes</th>
                <th>Denomicación</th>
                <th>Monto por denominación</th>
                </thead>
                <body>
                    <tr id="trBilletes50">
                        <td>1</td>
                        <td>
                            <input type="text" onkeypress="return generalSoloNumeros(event);" onkeyup="configBilletes.valorBilletes()"  class="form-control form-control-sm" placeholder="Número de billetes" name="txtCincuenta" id="txtCincuenta">
                        </td>
                        <td>$50.00</td>
                        <td> $ 0.00</td>
                    </tr>
                    <tr id="trBilletes100">
                        <td>2</td>
                        <td>
                            <input type="text" onkeypress="return generalSoloNumeros(event);" onkeyup="configBilletes.valorBilletes()" class="form-control form-control-sm" placeholder="Número de billetes" name="txtCien" id="txtCien">
                        </td>
                        <td>$100.00</td>
                        <td> $ 0.00</td>
                    </tr>
                    <tr id="trBilletesTotal">
                        <td></td>
                        <td></td>
                        <td> <strong>Monto total:</strong></td>
                        <td> $ 0.00</td>
                    </tr>
                </body>
            </table>
        </div>
    </div>

    <br>
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="titular-tab" data-bs-toggle="tab"
                    data-bs-target="#titular" role="tab" aria-controls="Transacción BILLCOOP"
                    aria-selected="true">
                Datos del titular
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="beneficiario-tab" data-bs-toggle="tab"
                    data-bs-target="#beneficiario" role="tab" aria-controls="Transacción BILLCOOP"
                    aria-selected="true">
                Datos del beneficiario
            </button>
        </li>
    </ul>

    <div class="tab-content" id="contenedorTitular" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="titular" role="tabpanel"
             aria-labelledby="evaluacion-tab">
            <form action="javascript:configFormulario.guardar();" class="needs-validation" novalidate id="formTitular"
                  name="formTitular"
                  accept-charset="utf-8" method="post">
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label for="cboTipoDocumentoTitular" class="form-label">Tipo de documento: <span
                                    class="requerido">*</span> </label>
                        <select disabled required onchange="buscarPersona.selectTipoDocumentoTitular(this.value)"
                                class="form-control cboTipoDocumentoTitular tipoPrimario" id="cboTipoDocumentoTitular">
                            <option selected disabled value=''>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el tipo de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtDocumentoPrimarioTitular" class="form-label">Número de documento <span
                                    class="requerido">*</span></label>
                        <div class="input-group has-validation">
                            <input onkeypress="return generalSoloNumeros(event);" type="text" class="form-control"
                                   id="txtDocumentoPrimarioTitular" name="txtDocumentoPrimarioTitular" placeholder="Número de identificación"
                                   required readonly>
                            <div class="input-group-append">
                                <button class="input-group-text btn btn-outline-dark disabled" onclick="buscarPersona.buscarTitular()"
                                        type="button"
                                        id="btnBuscarTitular"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtCodigoClienteTitular" class="form-label">Código de cliente</label>
                        <input type="text" placeholder="Código de cliente" id="txtCodigoClienteTitular" name="txtCodigoClienteTitular"
                               class="form-control" readonly>
                    </div>
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label for="txtNombresTitular" class="form-label">Nombres: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtNombresTitular" readonly name="txtNombresTitular" class="form-control" placeholder="Nombres"
                               required>
                        <div class="invalid-feedback">
                            Ingrese los nombres
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label for="txtApellidosTitular" class="form-label">Apellidos: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtApellidosTitular" readonly name="txtApellidosTitular" class="form-control" placeholder="Apellidos"
                               required>
                        <div class="invalid-feedback">
                            Ingrese los apellidos
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-6">
                        <label for="cboActividadEconomicaTitular" class="form-label">Actividad ecónomica primaria: <span
                                    class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select class="form-control selectpicker cboActividadEconomica"
                                    data-live-search="true" id="cboActividadEconomicaTitular"
                                    name="cboActividadEconomica" disabled required data-live-search="true">
                                <option value="" selected>Seleccione</option>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="tab-pane fade" id="beneficiario" role="tabpanel"
             aria-labelledby="evaluacion-tab">

            <form action="javascript:configFormulario.guardar();" class="needs-validation" novalidate id="formBeneficiario"
                  name="formTitular"
                  accept-charset="utf-8" method="post">

                <div class="row">

                    <div class="col-lg-6 col-xl-6 mayusculas">
                        <label for="txtNombreBeneficiario" class="form-label">Nombre completo: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtNombreBeneficiario" name="txtNombreBeneficiario" class="form-control" placeholder="Ingrese el nombre completo"
                               required>
                        <div class="invalid-feedback">
                            Ingrese el nombre de beneficiario de la remesa
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-6 mayusculas">
                        <label for="txtPaisDestino" class="form-label">País de destino de los fondos: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtPaisDestino" name="txtPaisDestino" class="form-control" placeholder="Nombre del país"
                               required>
                        <div class="invalid-feedback">
                            Detalle el nombre del país de destino de los fondos
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mayusculas">
                        <label for="txtDestino" class="form-label">Destino de los fondos: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtDestino" name="txtDestino" class="form-control" placeholder="Ingrese el destino de los fondos">
                        <div class="invalid-feedback">
                            Detalle el motivo del envío de los fondos
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


</section>

<?php
$_GET['js'] = ['billcoopFormulario'];