<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Operaciones con billetes de alta denominación</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">BILLCOOP</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Operaciones</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="listOperaciones-tab" data-bs-toggle="tab" data-bs-target="#listOperaciones" role="tab" aria-controls="Listado de operaciones" aria-selected="true"">
                Operaciones
            </button>
        </li>
        <li class="nav-item ms-auto">
            <div class="tabButtonContainer">
                <button type="button" onclick="configOperaciones.crearDeclaracion()" class="btn btn-sm btn-outline-dark">
                    <i class="fa fa-plus-circle"></i> Formulario
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active " id="listOperaciones" role="tabpanel" aria-labelledby="evaluacion-tab">
            <table class="table table-bordered table-striped" id="tblOperaciones">
                <thead>
                <th>N°</th>
                <th>Nombres</th>
                <th>Número documento</th>
                <th>Titular/Beneficiario</th>
                <th>Agencia</th>
                <th>Monto</th>
                <th>Acciones</th>
                </thead>
            </table>
        </div>
    </div>



</section>

<?php
$_GET['js'] = ['billcoopOperaciones'];