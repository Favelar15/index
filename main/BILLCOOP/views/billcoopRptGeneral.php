<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Reporte de operaciones BILLCOOP</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">BILLCOOP</li>
            <li class="breadcrumb-item">Reporte</li>
            <li class="breadcrumb-item active">Operaciones BILLCOOP</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">

    <form action="javascript:configRptGeneral.generar()" id="formRptGeneral" name="formRptGeneral"
          accept-charset="utf-8" method="POST" class="needs-validation" novalidate>

        <div class="row justify-content-center">

            <div class="col-lg-3 col-xl-3">
                <label class="form-label" for="cboAgencia">Agencia</label>
                <div class="form-group has-validation">
                    <select class="selectpicker form-control cboAgencia"
                            id="cboAgencia" name="cboAgencia">
                        <option selected disabled value="">Seleccione</option>
                    </select>
                </div>
            </div>

            <div class="col-lg-3 col-xl-3">
                <label class="form-label" for="txtInicio">Desde <span class="requerido">*</span> </label>
                <input type="text" class="form-control fechaFinLimitado readonly" onchange="configRptGeneral.cambioFecha()"
                       id="txtInicio" name="txtInicio" placeholder="Fecha de inicio" required>
                <div class="invalid-feedback">
                    Seleccione la fecha de inicio
                </div>
            </div>

            <div class="col-lg-3 col-xl-3">
                <label class="form-label" for="txtFinG">Hasta <span class="requerido">*</span></label>
                <input type="text" class="form-control fechaFinLimitado readonly" id="txtFin" name="txtFin"
                       placeholder="Fecha final" required>
                <div class="invalid-feedback">
                    Seleccione la fecha final
                </div>
            </div>

            <div class="col-lg- col-xl- mt-3" align="center">
                <button type="submit" class="btn btn-sm btn-outline-primary">
                    <i class="fas fa-search"></i> Generar reporte
                </button>
                <button type="button" class="btn btn-sm btn-outline-danger" id="btnCancelar" onclick="configRptGeneral.limpiar()">
                    <i class="fas fa-times-circle"></i> Limpiar
                </button>
            </div>

        </div>
    </form>

    <hr class="mb-1 mt-3">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="demominacion-tab" data-bs-toggle="tab" data-bs-target="#agencias"
                    role="tab" aria-controls=" Billetes por denominación por agencia" aria-selected="true">
                Operaciones BILLCOOP
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="btnExcel">
                <button type="button" id="btnExportar" onclick="configRptGeneral.exportar()" class="btn btn-sm btn-outline-success disabled">
                    <i class="fa fa-file-excel"></i> Exportar
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="operaciones" role="tabpanel" aria-labelledby="operaciones-tab">
            <table class="table table-bordered table-striped" id="tblOperaciones">
                <thead>
                <th>N°</th>
                <th>Nombre</th>
                <th>Documento de identificación</th>
                <th>Agencia</th>
                <th>Monto</th>
                <th>Billetes de $50</th>
                <th>Billetes de $100</th>
                <th>Fecha de registro</th>
                </thead>
            </table>
        </div>
    </div>


</section>



<?php
$_GET['js'] = ['billcoopRptGeneral'];