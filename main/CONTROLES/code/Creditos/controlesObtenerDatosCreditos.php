<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['creditos' => [] ];

if ( isset($_SESSION['index']) ) {

    $idAgencias = [];
    $agenciasAsignadas = $_SESSION['index']->agencias;

    foreach ( $agenciasAsignadas as $agencia )
    {
        array_push( $idAgencias, (int) $agencia->id);
    }

    require_once '../../../code/connectionSqlServer.php';
    require_once 'Models/Empresa.php';
    require_once 'Models/Prestamos.php';
    require_once 'Models/Credito.php';

    $credito = new Credito();
    $credito->setAgencias( $idAgencias );
    $respuesta['creditos'] = $credito->obtenerCreditos();
    //$respuesta['mes'] = data('');
}

echo json_encode( $respuesta );