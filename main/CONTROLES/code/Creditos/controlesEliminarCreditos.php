<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";
session_start();

$respuesta = [];

if ( isset( $_SESSION['index'] ) && $_SESSION['index']->locked )
{
    include "../../../code/connectionSqlServer.php";

    require_once 'Models/Credito.php';

    $idApartado = (int) base64_decode(urldecode($input['idApartado']));
    $tipoApartado = $input['tipoApartado'];

    $credito = new Credito();
    $credito->id = (int) $input['id'];
    $credito->idUsuario = $_SESSION['index']->id;
    $credito->ipOrdenador = generalObtenerIp();
    $respuesta = $credito->eliminarCreedito( $idApartado, $tipoApartado );
}  else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );