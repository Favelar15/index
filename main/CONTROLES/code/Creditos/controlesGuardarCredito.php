<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];


if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    include "../../../code/connectionSqlServer.php";

    $agencia = new stdClass();
    $agencia->id = $_SESSION['index']->agenciaActual->id;

    $id = $input['id'];
    $codigoCliente = $input['txtCodigoClienteCredito']['value'];
    $idEmpresa = $input['cboEmpresas']['value'];
    $nombreEmpresa = $input['cboEmpresas']['labelOption'];
    $idTipoPrestamo = $input['cboTipoDePrestamo']['value'];
    $numeroDePrestamo = $input['txtNumeroDePrestamo']['value'];
    $monto = $input['txtMonto']['value'];
    $idApartado = base64_decode(urldecode($input['idApartado']));
    $tipoApartado = $input['tipoApartado'];
    $logs = $input['logs'];

    $logsDB = '';

    if (count($logs) > 0) {
        $logTemp = [];

        foreach ($logs as $log) {
            $campo = $log['campo'];
            $valorAntiguo = $log['valorAnterior'];
            $valorNuevo = $log['valorNuevo'];
            $temp = $campo . '%%%' . $valorAntiguo . '%%%' . $valorNuevo;
            array_push($logTemp, $temp);
        }
        $logsDB = implode("@@@", $logTemp);
    } else {
        $logsDB = 'NULL';
    }

    require_once 'Models/Empresa.php';
    require_once 'Models/Prestamos.php';

    $empresa = new Empresa();
    $empresa->id = $idEmpresa;
    $empresa->empresa = $nombreEmpresa;

    $prestamos = new Prestamos();
    $prestamos->id = $idTipoPrestamo;

    require_once 'Models/Credito.php';

    $credito = new Credito();
    $credito->id = $id;
    $credito->codigoCliente = $codigoCliente;
    $credito->Empresa = $empresa;
    $credito->Prestamo = $prestamos;
    $credito->numeroDePrestamo = $numeroDePrestamo;
    $credito->monto = (float) $monto;

    $credito->Agencia = $agencia;
    $credito->idUsuario = $_SESSION['index']->id;
    $credito->ipOrdenador = generalObtenerIp();

    $respuestaBD = $credito->guardarRemesa($logsDB, $idApartado, $tipoApartado);

    if ($respuestaBD->status) {
        $respuesta['respuesta'] = $respuestaBD->respuesta;
        $respuesta['id'] = $respuestaBD->id;
    } else {
        $respuesta = $respuestaBD->respuesta;
    }

} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);
