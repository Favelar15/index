<?php

class Credito
{
    public $id, $fechaRegistro, $codigoCliente, $Empresa, $Prestamo, $numeroDePrestamo, $monto, $Agencia, $idUsuario, $ipOrdenador;
    public $propietario;
    private $idPrestamo, $prestamo;
    private $idEmpresa, $empresa;
    private $idAgencia, $agencia;
    private $conexion;
    private $agenciasAsignadas = [];

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        $this->Agencia = new stdClass();

        if ( !empty( $this->fechaRegistro ) )
        {
            $this->fechaRegistro = date('d-m-Y', strtotime( $this->fechaRegistro ));
        }

        !empty($this->monto) && $this->monto = sprintf('%0.2f', $this->monto);

        if ( !empty($this->idEmpresa) && !empty($this->empresa) )
        {
            $this->Empresa = new Empresa();
            $this->Empresa->id = $this->idEmpresa;
            $this->Empresa->empresa = $this->empresa;
        }

        if ( !empty($this->idPrestamo) && !empty($this->prestamo) )
        {
            $this->Prestamo = new Prestamos();
            $this->Prestamo->id = $this->idPrestamo;
            $this->Prestamo->prestamo = $this->prestamo;
        }

        if (!empty( $this->idAgencia) && !empty($this->agencia) )
        {
            $this->Agencia->id = $this->idAgencia;
            $this->Agencia->agencia = $this->agencia;
        }

        if ( !empty( $this->idUsuario ) )
        {
            $this->propietario = ( $this->idUsuario == $_SESSION['index']->id ) ? 'S' : 'N';
        }
    }

    function obtenerCreditos ()
    {
        $values = str_repeat('?,',count( $this->agenciasAsignadas) - 1 ).'?';

        $query = "SELECT * FROM controles_viewDatosCreditos WHERE idAgencia IN ($values) AND ( CAST(fechaRegistro AS DATE )) > ( CAST (DATEADD (MONTH, -1, GETDATE() ) AS DATE )) ORDER BY fechaRegistro DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute( $this->agenciasAsignadas );

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function reporteCreditos( $desde, $hasta )
    {
        $query = "SELECT * FROM controles_viewDatosCreditos WHERE idAgencia IN (implode(',', $this->agenciasAsignadas)) AND ( CAST( fechaRegistro AS DATE ) ) BETWEEN $desde AND $hasta ORDER BY fechaRegistro DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function guardarRemesa ( $logs, $idApartado, $tipoApartado )
    {
        $respuesta = null; $dbId = 0;

        $query = 'EXEC controles_spGuardarCreditos :id, :codigoCliente, :idEmpresa, :empresa, :idTipoCredito, :numeroDePrestamo, :monto, :idAgencia, :idUsuario, :ipOrdenador, :idApartado, :tipoApartado, :logs, :dbId, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':idEmpresa', $this->Empresa->id, PDO::PARAM_INT);
        $result->bindParam(':empresa', $this->Empresa->empresa, PDO::PARAM_STR);
        $result->bindParam(':idTipoCredito', $this->Prestamo->id, PDO::PARAM_INT);
        $result->bindParam(':numeroDePrestamo', $this->numeroDePrestamo, PDO::PARAM_INT);
        $result->bindParam(':monto', $this->monto, PDO::PARAM_INT);

        $result->bindParam(':idAgencia', $this->Agencia->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);

        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);

        $result->bindParam(':logs', $logs, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':dbId', $dbId, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        $result->execute();


        if ( $respuesta == 'EXITO')
        {
            return (object)['status' => true, 'respuesta' => $respuesta,  'id' => $dbId];
        } else
        {
            return (object)['status' => false, 'respuesta' => $respuesta, 'id' => $dbId ];
        }
    }

    function eliminarCreedito( $idApartado, $tipoApartado )
    {
        $respuestaDB = '';

        $query = 'EXEC controles_spEliminarCreditos :id, :idUsuario, :ipOrdenador, :idApartado, :tipoApartado, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuestaDB, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        //return [ $this->id , $this->idUsuario, $this->ipOrdenador, $idApartado, $tipoApartado, $respuestaDB ];

        if ( $respuestaDB == 'EXITO')
        {
            return (object)['status' => true, 'respuesta' => $respuestaDB ];
        }

        return (object)['status' => false, 'respuesta' => $respuestaDB ];

    }

    function setAgencias( $agencias )
    {
        $this->agenciasAsignadas = $agencias;
    }
}