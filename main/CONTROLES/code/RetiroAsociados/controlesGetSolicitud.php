<?php
include "../../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../../code/connectionSqlServer.php";
        require_once '../../../code/Models/asociado.php';
        require_once "retiroAsociado.php";

        $idUsuario = $_SESSION["index"]->id;
        $params = explode("@@@", $_GET["q"]);
        if ($params[0] == "all") {
            $idApartado = base64_decode(urldecode($params[2]));

            $vista = $params[1] ? 'general_viewRolesSubApartados' : 'general_viewRolesApartados';
            $queryRoles = "SELECT top(1) * from " . $vista . " WHERE id = " . $idApartado;
            $resultRoles = $conexion->prepare($queryRoles, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $resultRoles->execute();
            $dataRoles = $resultRoles->fetch(PDO::FETCH_ASSOC);
            $roles = explode('@@@', $dataRoles["roles"]);
            $agencias = [];

            $tipoPermiso = 'Local';

            foreach ($_SESSION["index"]->roles as $rol) {
                if (in_array($rol->id, $roles)) {
                    if ($rol->tipo == 'Administrativo') {
                        $tipoPermiso = 'Administrativo';
                        break;
                    }

                    if ( $rol->label == 'Cajero')
                    {
                        $tipoPermiso = 'Cajero';
                        break;
                    }
                }
            }

            foreach ($_SESSION["index"]->agencias as $agencia) {
                array_push($agencias, $agencia->id);
            }

            // PREPARANDO ARRAY DE ROLES PARA VALIDAR CONFIGURACION DE RESOLUCIÓN
            $rolesIds = [];

            foreach ( $_SESSION['index']->roles as $rol )
            {
                array_push( $rolesIds, (int) $rol->id );
            }

            $solicitud = new solicitudRetiro();
            $solicitudes = $solicitud->obtenerSolicitudes($tipoPermiso, $idUsuario, $agencias, $rolesIds);

            $respuesta->{"solicitudes"} = $solicitudes;
        } else {
            $id = base64_decode(urldecode($params[0]));
            $solicitud = new solicitudRetiro();
            if (!isset($params[1])) {
                $datosSolicitud = $solicitud->obtenerSolicitud($id, $idUsuario);
                $respuesta->{"solicitud"} = $datosSolicitud;
            } else {
                $documento = $solicitud->construirPDF($id, $idUsuario);
                $respuesta->{"respuesta"} = $documento["respuesta"];
                $respuesta->{"nombreArchivo"} = $documento["nombreArchivo"];
            }
        }

        $conexion = null;
    }
}

echo json_encode($respuesta);
