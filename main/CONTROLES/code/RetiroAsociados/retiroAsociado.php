<?php
class retiroAsociado extends asociado
{
    function __construct()
    {
        parent::__construct();
        global $conexion;
        $this->conexion = $conexion;
    }

    public function validarSolicitudesActivas()
    {
    }

    public function buscarAsociadoRetiro()
    {
        $datosAsociado = $this->buscarAsociado();

        if (count($datosAsociado) > 0) {

            $query = "SELECT TOP(1) * FROM controles_viewSolicitudesDatosMinimos WHERE codigoCliente = " . $this->codigoCliente . " AND pagado = 'N' AND (fechaResolucion is null OR cast(getdate() as date) <= DATEADD(month, 6, cast(fechaResolucion as date))) AND (resolucion is null OR resolucion = 'APROBADA')";
            $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $result->execute();
            if ($result->rowCount() > 0) {
                $data = $result->fetch(PDO::FETCH_ASSOC);
                $estado = new stdClass();
                $estado->{"nombreCompleto"} = $data["nombreCompleto"];
                $estado->{"estado"} = $data["estado"];
                return ["respuesta" => "PROCESO", "estado" => $estado];
            } else {
                return ["respuesta" => "EXITO", "datosAsociado" => $datosAsociado[0]];
            }
        } else {
            return ["respuesta" => "NoAsociado"];
        }
    }
}

class solicitudRetiro
{
    public $id;
    public $codigoCliente;
    public $tipoDocumento;
    public $numeroDocumento;
    public $NIT;
    public $nombresAsociado;
    public $apellidosAsociado;
    public $profesion;
    public $agencia;
    public $fechaAfiliacion;
    public $telefonoFijo;
    public $telefonoMovil;
    public $email;
    public $tarjetaDebito;
    public $pais;
    public $departamento;
    public $municipio;
    public $direccionCompleta;
    public $montoPrestamos;
    public $montoCuentasCobrar;
    public $solicitudResuelta;
    public $nombreArchivo;
    public $motivosRetiro;
    public $productos;
    public $deudores;
    public $gestiones;
    public $fechaRegistro;
    public $fechaActualizacion;
    public $resolucion;
    public $usuario;
    public $nombreUsuario;
    public $propietario;
    public $fechaResolucion;


    public $monto, $fechaPago;

    private $conexion;
    private $path;
    private $agenciaPorIP;

    function __construct()
    {

        $this->agenciaPorIP = (Object)[];
        $this->agenciaPorIP->{'15'} = array('700', 'Oficina Central');
        $this->agenciaPorIP->{'144'} = array('701', 'Nueva Concepción' );
        $this->agenciaPorIP->{'13'} = array('702', 'Apopa' );
        $this->agenciaPorIP->{'16'} = array('703', 'Dulce Nombre de María' );
        $this->agenciaPorIP->{'16'} = array('704', 'La palma' );
        $this->agenciaPorIP->{'6'} = array('705', 'Chalatenango' );
        $this->agenciaPorIP->{'47'} = array ('706', 'El Paraíso' );
        $this->agenciaPorIP->{'48'} = array ('707', 'La Laguna' );
        $this->agenciaPorIP->{'46'} = array('708', 'Concepción Quezaltepeque' );
        $this->agenciaPorIP->{'87'} = array('709', 'Perícentro Apopa' );
        $this->agenciaPorIP->{'97'} = array('710', 'Metrocentro San Salvador' );


        global $conexion;
        $this->conexion = $conexion;
        $this->path = __DIR__ . '\..\..\docs\solicitudRetiro\/';

        !empty($this->fechaAfiliacion) && $this->fechaAfiliacion = date('d-m-Y', strtotime($this->fechaAfiliacion));
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
        !empty($this->fechaPago) && $this->fechaPago = date('d-m-Y h:i a', strtotime($this->fechaPago));
        !empty($this->monto) && $this->monto = sprintf('%0.2f', $this->monto);
        !empty($this->fechaResolucion) && $this->fechaResolucion = date('d-m-Y', strtotime($this->fechaResolucion));

        if (!empty($this->tarjetaDebito)) {
            $motivo = new motivoRetiroSolicitud();
            $this->motivosRetiro = $motivo->obtenerMotivos($this->id);

            $producto = new productoSolicitud();
            $this->productos = $producto->obtenerProductos($this->id);

            $deudor = new deudorSolicitud();
            $this->deudores = $deudor->obtenerDeudores($this->id);

            $gestion = new gestionSolicitud();
            $this->gestiones = $gestion->obtenerGestiones($this->id);
        } else if (!empty($this->estado) && $this->resolucion == 'APROBADA') {
            $producto = new productoSolicitud();
            $this->productos = $producto->obtenerProductos($this->id);
        }
    }

    public function guardarSolicitud($tipoApartado, $idApartado, $idUsuario, $ipActual)
    {
        $arrMotivosRetiro = [];
        $arrProductos = [];
        $arrDeudores = [];
        $arrGestiones = [];

        !empty($this->fechaAfiliacion) && $this->fechaAfiliacion = date('Y-m-d', strtotime($this->fechaAfiliacion));

        foreach ($this->motivosRetiro as $motivo) {
            array_push($arrMotivosRetiro, $motivo["id"] . "%%%" . $motivo["idMotivo"] . "%%%" . $motivo["explicacion"]);
        }

        foreach ($this->productos as $producto) {
            array_push($arrProductos, $producto["id"] . "%%%" . $producto["idProducto"] . "%%%" . $producto["monto"]);
        }

        foreach ($this->deudores as $deudor) {
            array_push($arrDeudores, $deudor["id"] . "%%%" . $deudor["nombreDeudor"]);
        }

        foreach ($this->gestiones as $gestion) {
            array_push($arrGestiones, $gestion["id"] . "%%%" . $gestion["area"] . "%%%" . $gestion["gestion"]);
        }

        $motivosRetiroDb = count($arrMotivosRetiro) > 0 ? implode('@@@', $arrMotivosRetiro) : '';
        $productosDb = count($arrProductos) > 0 ? implode('@@@', $arrProductos) : '';
        $deudoresDb = count($arrDeudores) > 0 ? implode('@@@', $arrDeudores) : '';
        $gestionesDb = count($arrGestiones) > 0 ? implode('@@@', $arrGestiones) : '';

        $response = null;
        $idSolicitud = null;

        $query = "EXEC controles_spGuardarSolicitudRetiro :tipoApartado,:idApartado,:id,:codigoCliente,:tipoDocumento,:numeroDocumento,:NIT,:nombres,:apellidos,:profesion,:agencia,:fechaAfiliacion,:telefonoFijo,:telefonoMovil,:email,:tarjetaDebito,:pais,:departamento,:municipio,:direccion,:montoPrestamos,:montoCuentasCobrar,:motivos,:productos,:deudores,:gestiones,:usuario,:ip,:response,:idSolicitud;";

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_STR);
        $result->bindParam(':tipoDocumento', $this->tipoDocumento, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumento', $this->numeroDocumento, PDO::PARAM_STR);
        $result->bindParam(':NIT', $this->NIT, PDO::PARAM_STR);
        $result->bindParam(':nombres', $this->nombresAsociado, PDO::PARAM_STR);
        $result->bindParam(':apellidos', $this->apellidosAsociado, PDO::PARAM_STR);
        $result->bindParam(':profesion', $this->profesion, PDO::PARAM_STR);
        $result->bindParam(':agencia', $this->agencia, PDO::PARAM_INT);
        $result->bindParam(':fechaAfiliacion', $this->fechaAfiliacion, PDO::PARAM_STR);
        $result->bindParam(':telefonoFijo', $this->telefonoFijo, PDO::PARAM_STR);
        $result->bindParam(':telefonoMovil', $this->telefonoMovil, PDO::PARAM_STR);
        $result->bindParam(':email', $this->email, PDO::PARAM_STR);
        $result->bindParam(':tarjetaDebito', $this->tarjetaDebito, PDO::PARAM_STR);
        $result->bindParam(':pais', $this->pais, PDO::PARAM_INT);
        $result->bindParam(':departamento', $this->departamento, PDO::PARAM_INT);
        $result->bindParam(':municipio', $this->municipio, PDO::PARAM_INT);
        $result->bindParam(':direccion', $this->direccionCompleta, PDO::PARAM_STR);
        $result->bindParam(':montoPrestamos', $this->montoPrestamos, PDO::PARAM_STR);
        $result->bindParam(':montoCuentasCobrar', $this->montoCuentasCobrar, PDO::PARAM_STR);
        $result->bindParam(':motivos', $motivosRetiroDb, PDO::PARAM_STR);
        $result->bindParam(':productos', $productosDb, PDO::PARAM_STR);
        $result->bindParam(':deudores', $deudoresDb, PDO::PARAM_STR);
        $result->bindParam(':gestiones', $gestionesDb, PDO::PARAM_STR);
        $result->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idSolicitud', $idSolicitud, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);

        $result->execute();

        if ($response) {
            $this->conexion = null;
            return ["respuesta" => $response, "idSolicitud" => $idSolicitud];
        } else {
            return ["respuesta" => "Error en controles_spGuardarSolicitudRetiro"];
            $this->conexion = null;
        }

        $this->conexion = null;
        return ["respuesta" => false];
    }

    public function obtenerSolicitud($id, $idUsuario)
    {
        $query = "SELECT * FROM controles_viewDatosSolicitudRetiro WHERE id = :id;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        if ($result->execute()) {
            return $result->fetchAll(PDO::FETCH_CLASS, 'solicitudRetiro')[0];
        }

        $this->conexion = null;
        return [];
    }

    public function construirPDF($id, $idUsuario)
    {
        $solicitud = $this->obtenerSolicitud($id, $idUsuario);

        $queryDepartamento = "SELECT TOP(1) nombreDepartamento from general_datosDepartamentos WHERE id = :id;";
        $resultDepartamento = $this->conexion->prepare($queryDepartamento, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultDepartamento->bindParam(':id', $solicitud->departamento, PDO::PARAM_INT);
        $resultDepartamento->execute();
        $dataDepartamento = $resultDepartamento->fetch(PDO::FETCH_ASSOC);

        $solicitud->nombreDepartamento = mb_strtoupper($dataDepartamento["nombreDepartamento"], 'utf-8');

        $queryMunicipio = "SELECT TOP(1) nombreMunicipio from general_datosMunicipios WHERE id = :id;";
        $resultMunicipio = $this->conexion->prepare($queryMunicipio, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $resultMunicipio->bindParam(':id', $solicitud->municipio, PDO::PARAM_INT);
        $resultMunicipio->execute();
        $dataMunicipio = $resultMunicipio->fetch(PDO::FETCH_ASSOC);

        $solicitud->nombreMunicipio = mb_strtoupper($dataMunicipio["nombreMunicipio"], 'utf-8');

        require_once 'Motivos.php';
        $tmpMotivo = new Motivos();
        $motivos = $tmpMotivo->obtenerMotivos();
        $detallesMotivos = [];

        require_once 'Producto.php';
        $tmpProducto = new Producto();
        $productos = $tmpProducto->obtenerProductos();

        $pdf = new construccionPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', false);
        $pdf->codigoCliente = $solicitud->codigoCliente;
        $pdf->configuracionDocumento();
        $pdf->setMargin();

        $pdf->SetAutoPageBreak(TRUE, 35);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->AddPage();
        $pdf->setFont('dejavusans', 'B', 12);
        $pdf->Write(0, 'SOLICITUD DE RETIRO DE ASOCIADO/A', '', 0, 'C', true, 0, false, false, 0);
        $pdf->ln(3);
        $pdf->setFont('dejavusans', 'B', 9);
        $pdf->Write(0, 'Señores:', '', 0, '', true, 0, false, false, 0);
        $pdf->ln(1);
        $pdf->Write(0, 'Asociación Cooperativa de Ahorro y Crédito', '', 0, '', true, 0, false, false, 0);
        $pdf->ln(1);
        $pdf->Write(0, 'Presente', '', 0, '', true, 0, false, false, 0);

        $pdf->ln(3);

        $pdf->Write(0, 'Estimados Señores:', '', 0, '', true, 0, false, false, 0);
        $pdf->ln(1);
        $pdf->Write(0, 'Por medio de la presente comunico a ustedes que he decidido retirarme de la Cooperativa, por el motivo siguiente:', '', 0, '', true, 0, false, false, 0);
        $pdf->ln(5);


        $pdf->setFont('dejavusans', '', 9);


        foreach ($motivos as $motivo) {
            foreach ($solicitud->motivosRetiro as $motivoIncluido) {

                if ($motivoIncluido->idMotivo == $motivo->id) {

                    $motivo->seleccionado = true;

                    if (!empty($motivoIncluido->explicacion)) {
                        array_push($detallesMotivos, $motivoIncluido->explicacion);
                    }
                    break;
                } else {
                    $motivo->seleccionado = false;
                }
            }
        }

        $pdf->setTblMotivos($motivos, $detallesMotivos);
        $pdf->ln(5);
        $pdf->setFont('dejavusans', 'B', 9);
        $pdf->Write(0, 'Agradeceré a ustedes disponer de la liquidación de mi cuenta lo más pronto posible.', '', 0, 'C', true, 0, false, false, 0);
        $pdf->setFont('');

        $pdf->setDatosPersonales($solicitud);

        $pdf->firmaDelAsociado();

        $pdf->AddPage();
        $pdf->setFont('dejavusans', 'B', 12);
        $pdf->Write(0, 'INFORME DEL EJECUTIVO QUE ATIENDE', '', 0, 'C', true, 0, false, false, 0);
        $pdf->ln(3);
        $pdf->writeHTML("<hr>", true, false, false, false, '');

        $pdf->setFont('dejavusans', '', 8);
        $pdf->Write(0, 'El estado de cuenta del Señor(a) ' . $solicitud->nombresAsociado . ' ' . $solicitud->apellidosAsociado . ' es el siguiente:', '', 0, '', true, 0, false, false, 0);

        $pdf->setInformeFinanciero($solicitud, $productos);

        $pdf->setGestion($solicitud->gestiones);
        $pdf->setGestionVacia();

        $pdf->setFont('dejavusans', 'B', 10);
        $pdf->Write(0, 'RESOLUCIÓN DEL CONSEJO', '', 0, 'C', true, 0, false, false, 0);
        $pdf->ln(2);


        $pdf->setResolucion();

        $pdf->Output($this->path . $solicitud->nombreArchivo, 'F');

        $this->conexion = null;

        if (file_exists($this->path . $solicitud->nombreArchivo)) {
            return ["respuesta" => "EXITO", "nombreArchivo" => $solicitud->nombreArchivo];
        }

        return ["respuesta" => "Error al crear archivo", "nombreArchivo" => null];
    }

    public function obtenerSolicitudes($tipoPermiso, $idUsuario, $agencias, $roles )
    {
        // $query = "SELECT * FROM controles_viewSolicitudesDatosMinimos WHERE usuario = " . $idUsuario . " AND pagado = 'N' AND ( fechaResolucion is null OR cast(getdate() as date) <= DATEADD(month, 6, cast(fechaResolucion as date)) )";
        $query = "SELECT * FROM controles_viewSolicitudesDatosMinimos WHERE pagado = 'N' AND ( fechaResolucion is null OR cast(getdate() as date) <= DATEADD(month, 6, cast(fechaResolucion as date)) )";
        $tipoPermiso == 'Cajero' && $query = "SELECT * FROM controles_viewSolicitudesDatosMinimos WHERE estado = 'RESUELTA'  AND pagado = 'N' AND ( fechaResolucion is null OR cast(getdate() as date) <= DATEADD(month, 6, cast(fechaResolucion as date)) )";
        // $tipoPermiso == 'Administrativo' && $query = "SELECT * FROM controles_viewSolicitudesDatosMinimos WHERE agencia IN (" . implode(',', $agencias) . ") AND pagado = 'N' AND (fechaResolucion is null OR cast(getdate() as date) <= DATEADD(month, 6, cast(fechaResolucion as date)))";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $solicitudes = $result->fetchAll(PDO::FETCH_CLASS, 'solicitudRetiro');

            $rolesDB = $this->getConfiguracion( $roles );

            $allMotivos = [];

            foreach ( $rolesDB as $rolDB )
            {
                if ( count( $rolDB->motivos) > 0 )
                {
                    foreach ( $rolDB->motivos as $motivo )
                    {
                        if( !in_array( (int) $motivo->idMotivo, $allMotivos ) )
                        {
                            array_push( $allMotivos, (int) $motivo->idMotivo );
                        }
                    }
                }
            }

            foreach ( $solicitudes as $solicitud )
            {
                $motivoSolicitud = explode('@@@', $solicitud->motivos );

                foreach ( $motivoSolicitud as $motivo )
                {
                    $solicitud->aprueba = 'N';
                    $temp = explode('???', $motivo);
                    $id = $temp[0];

                    if (in_array((int) $id, $allMotivos ))
                    {
                        $solicitud->aprueba = 'S';
                        break;
                    }
                }
            }
            return $solicitudes;
        }
        $this->conexion = null;
        return [];
    }

    public function eliminarSolicitud($idUsuario, $tipoApartado, $idApartado, $ipActual)
    {
        $response = null;
        $query = "EXEC controles_spEliminarSolicitudRetiro :id,:idUsuario,:tipoApartado,:idApartado,:ip,:response;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return $response;
        }

        $this->conexion = null;
        return "Error en controles_spEliminarSolicitudRetiro";
    }

    public function setResolucionSolicitud($idUsuario, $idResolucion, $observaciones, $tipoApartado, $idApartado, $ipActual, $fechaResolucion)
    {
        $response = null;
        $query = "EXEC controles_spResolucionSolicitudRetiro :id,:idUsuario,:tipoApartado,:idApartado,:ip,:idResolucion,:observaciones, :fechaResolucion, :response;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':idResolucion', $idResolucion, PDO::PARAM_INT);
        $result->bindParam(':observaciones', $observaciones, PDO::PARAM_STR);
        $result->bindParam(':fechaResolucion', $fechaResolucion, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return $response;
        }

        $this->conexion = null;
        return $response;
    }

    public function setPagoSolicitud($idUsuario, $tipoApartado, $idApartado, $ipActual)
    {
        $response = null;
        $tmpProductos = [];

        foreach ($this->productos as $producto) {
            array_push($tmpProductos, $producto["id"] . "%%%" . $producto["monto"]);
        }

        $productosDb = count($tmpProductos) > 0 ? implode('@@@', $tmpProductos) : '';
        $query = "EXEC controles_spPagoSolicitudRetiro :id,:idUsuario,:tipoApartado,:idApartado,:ip,:productos,:response;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':ip', $ipActual, PDO::PARAM_STR);
        $result->bindParam(':productos', $productosDb, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ($result->execute()) {
            $this->conexion = null;
            return $response;
        }

        $this->conexion = null;
        return "Error en controles_spPagoSolicitudRetiro";
    }

    function guardarConfiguracion($config, $idUsuario, $idApartado, $tipoApartado )
    {
        $response = null;
        $query = "EXEC controles_spConfigResolucionSolicitudRetiro :configRoles, :idApartado,:tipoApartado, :idUsuario, :respuesta;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':configRoles', $config, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':respuesta', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        if ( $result->execute() )
        {
            return (Object)[ 'status' => true, 'respuesta' => $response];
        }

        return (Object)[ 'status' => false, 'respuesta' => $response];
    }

    function getConfiguracion( $roles = [] )
    {
        if ( count( $roles ) > 0 )
        {
            $values = str_repeat('?,',count( $roles ) - 1 ).'?';

            $query = "SELECT * FROM controles_configSolicitudRetiro WHERE idRol IN ( $values )";
            $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $result->execute( $roles);
        } else
        {
            $query = "SELECT * FROM controles_configSolicitudRetiro";
            $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $result->execute();
        }


        if ( $result->rowCount() > 0 )
        {
            $datos = [];
            $this->conexion = null;
            $roles = $result->fetchAll(PDO::FETCH_OBJ);

            foreach ($roles as $rol )
            {
                $motivos = explode('@@@', $rol->motivos );
                $motivoRol = [];

                foreach ( $motivos as $motivoRetiro )
                {
                    $tempMotivo = explode('%%%', $motivoRetiro );
                    $motivo = new stdClass();
                    $motivo->idMotivo = $tempMotivo[0];
                    $motivo->motivo = $tempMotivo[1];
                    array_push( $motivoRol, $motivo );
                }
                $rolClass = new stdClass();
                $rolClass->idRol = $rol->idRol;
                $rolClass->rol = $rol->rol;
                $rolClass->motivos = $motivoRol;

                array_push($datos, $rolClass );
            }
            return $datos;
        }
        $this->conexion = null;
        return [];
    }

    function reporteSolicitudesPagadas( $agencias, $desde, $hasta )
    {
        // $query = "SELECT * FROM controles_viewReporteSolicitudesRetiroPagadas WHERE idAgencia IN (".implode(',', $agencias).") AND cast(fechaPago as date) >= '".$desde."' AND cast(fechaPago as date) <= '".$hasta."' ORDER BY fechaPago DESC";
        $query = "SELECT * FROM controles_viewReporteSolicitudesRetiroPagadas WHERE cast(fechaPago as date) >= '".$desde."' AND cast(fechaPago as date) <= '".$hasta."' ORDER BY fechaPago DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {

            $solicitudes = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);

            foreach ( $solicitudes as $key => $solicitud )
            {
                if ( !empty($solicitud->ipOrdenador ) )
                {
                    $segmentos = explode('.', $solicitud->ipOrdenador);
                    $segmento = $segmentos[2];

                    $segmento = isset( $this->agenciaPorIP->{$segmento}) ? $segmento : 15;
                    $solicitud->agencia = $this->agenciaPorIP->{$segmento}[1];

                    if ( !in_array( (int) $this->agenciaPorIP->{$segmento}[0], $agencias) )
                    {

                        unset( $solicitudes[$key] );
                    }
                }

            }
            return $solicitudes;
        }
        $this->conexion = null;
        return [];
    }

    function reporteSolicitudesAprobadas( $estado, $agencias, $desde, $hasta )
    {
        //$query = "SELECT * FROM controles_viewReporteSolicitudesAprobadas WHERE idAgencia IN (".implode(',', $agencias).") AND cast(fechaResolucion as date) >= '".$desde."' AND cast(fechaResolucion as date) <= '".$hasta."' ORDER BY fechaResolucion DESC";
        $query = "SELECT * FROM controles_viewReporteSolicitudesAprobadas WHERE pagado = 'N' AND CAST(fechaResolucion AS DATE) >= '".$desde."' AND CAST(fechaResolucion AS DATE) <= '".$hasta."' ORDER BY fechaResolucion DESC";
        if ( $estado == 'PENDIENTE' )
        {
            //$query = "SELECT * FROM controles_viewReporteSolicitudesAprobadas WHERE idAgencia IN (".implode(',', $agencias).") AND CAST(fechaRegistro AS DATE) >= '".$desde."' AND cast(fechaRegistro AS DATE) <= '".$hasta."' ORDER BY fechaRegistro DESC";
            $query = "SELECT * FROM controles_viewReporteSolicitudesAprobadas WHERE pagado = 'N' AND CAST(fechaRegistro AS DATE) >= '".$desde."' AND cast(fechaRegistro AS DATE) <= '".$hasta."' ORDER BY fechaRegistro DESC";
        }
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);

        if ($result->execute()) {
            $solicitudes =  $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);

            foreach ( $solicitudes as $key => $solicitud  )
            {
                $valido = true;

                if ( !empty($solicitud->ipOrdenador ) )
                {
                    $segmentos = explode('.', $solicitud->ipOrdenador);
                    $segmento = $segmentos[2];

                    $segmento = isset( $this->agenciaPorIP->{$segmento}[0]) ? $segmento : 15;

                    $solicitud->agencia = $this->agenciaPorIP->{$segmento}[1];

                    if ( !in_array( (int) $this->agenciaPorIP->{$segmento}[0], $agencias) )
                    {
                        $valido = false;
                        unset( $solicitudes[$key] );
                    }
                }

                if( $valido )
                {
                    $motivosArray = [];
                    $motivosDb = explode('@@@', $solicitud->motivos );

                    $productosArray = [];
                    $productosDb = explode('@@@', $solicitud->productos );

                    foreach ( $motivosDb as $motivo )
                    {
                        $motivoTemp = explode('???', $motivo );
                        $descripcion = !empty($motivoTemp[2]) ? ' ( '.$motivoTemp[2].' )' : "";

                        array_push( $motivosArray,
                            $motivoTemp[1].' '.$descripcion
                        );
                    }
                    $solicitud->motivos = implode(",", $motivosArray);


                    $sumaAportaciones = 0;
                    $sumaAhorros = 0;

                    foreach ( $productosDb as $producto )
                    {
                        $productoTemp = explode('???', $producto );

                        if ( $productoTemp[1] == 'Aportaciones')
                        {
                            $sumaAportaciones += floatval($productoTemp[2]);
                        } else {
                            $sumaAhorros += floatval($productoTemp[2]);
                        }
                    }
                    $solicitud->productos = [
                        'Aportaciones' => sprintf('%0.2f',  $sumaAportaciones), 'Ahorros' => sprintf('%0.2f',  $sumaAhorros)
                    ];

                    if( empty( $solicitud->resolucion ) )
                    {
                        $solicitud->resolucion = 'PENDIENTE';
                        $solicitud->fechaResolucion = 'PENDIENTE';
                    }
                }
            }

            return $solicitudes;
        }
        $this->conexion = null;
        return [];
    }
}

class motivoRetiroSolicitud
{
    public $id;
    public $idMotivo;
    public $explicacion;
    public $fechaRegistro;
    public $fechaActualizacion;
    public $usuario;
    public $propietario;
    private $conexion;

    function __construct()
    {
        global $conexion;
        global $_SESSION;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
        $this->propietario = (isset($_SESSION["indexAccess"]) && $_SESSION["indexAccess"]->id == $this->usuario) ? 'S' : 'N';
    }

    public function obtenerMotivos($idSolicitud)
    {
        $query = "SELECT * FROM controles_viewMotivosRetiroSolicitud WHERE idSolicitud = :idSolicitud;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $idSolicitud, PDO::PARAM_INT);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'motivoRetiroSolicitud');
        }

        $this->conexion = null;
        return [];
    }
}

class productoSolicitud
{
    public $id;
    public $idProducto;
    public $monto;
    public $fechaRegistro;
    public $fechaActualizacion;
    public $usuario;
    public $propietario;
    private $conexion;

    function __construct()
    {
        global $conexion;
        global $_SESSION;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
        $this->propietario = (isset($_SESSION["indexAccess"]) && $_SESSION["indexAccess"]->id == $this->usuario) ? 'S' : 'N';
    }

    public function obtenerProductos($idSolicitud)
    {
        $query = "SELECT * FROM controles_viewProductosSolicitud WHERE idSolicitud = :idSolicitud;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $idSolicitud, PDO::PARAM_INT);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'productoSolicitud');
        }

        $this->conexion = null;
        return [];
    }
}

class deudorSolicitud
{
    public $id;
    public $nombreDeudor;
    public $fechaRegistro;
    public $fechaActualizacion;
    public $usuario;
    public $propietario;
    private $conexion;

    function __construct()
    {
        global $conexion;
        global $_SESSION;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
        $this->propietario = (isset($_SESSION["indexAccess"]) && $_SESSION["indexAccess"]->id == $this->usuario) ? 'S' : 'N';
    }

    public function obtenerDeudores($idSolicitud)
    {
        $query = "SELECT * FROM controles_viewDeudoresSolicitud WHERE idSolicitud = :idSolicitud;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $idSolicitud, PDO::PARAM_INT);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'deudorSolicitud');
        }

        $this->conexion = null;
        return [];
    }
}

class gestionSolicitud
{
    public $id;
    public $area;
    public $gestion;
    public $fechaRegistro;
    public $fechaActualizacion;
    public $usuario;
    public $propietario;
    private $conexion;

    function __construct()
    {
        global $conexion;
        global $_SESSION;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
        $this->propietario = (isset($_SESSION["indexAccess"]) && $_SESSION["indexAccess"]->id == $this->usuario) ? 'S' : 'N';
    }

    public function obtenerGestiones($idSolicitud)
    {
        $query = "SELECT * FROM controles_viewGestionesSolicitud WHERE idSolicitud = :idSolicitud;";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $idSolicitud, PDO::PARAM_INT);

        if ($result->execute()) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, 'gestionSolicitud');
        }

        $this->conexion = null;
        return [];
    }
}

class construccionPDF extends TCPDF
{
    public $codigoCliente;
    function configuracionDocumento()
    {
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('Departamento IT');
        $this->SetTitle('Solicitud de retiro');
        $this->SetSubject('ACACYPAC');
        $this->SetKeywords('TCPDF, PDF, Solicitud de retiro');

        $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    }

    function setMargin()
    {
        $this->SetMargins(15, 15, 15, 15);
        $this->SetHeaderMargin(4);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);
    }

    function Header()
    {
        $this->setFont('dejavusans', 'B', 10);
        $this->Write(0, 'N° de cliente: ' . $this->codigoCliente, '', 0, 'R', true, 0, false, false, 0);
    }

    function Footer()
    {
    }

    function setTblMotivos($motivos = [], $detallesMotivos = [])
    {
        $this->SetFillColor(16, 126, 20);
        $this->SetTextColor(255);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('', 'B');

        $w = array(40, 35, 40, 45);
        $this->Cell(140, 7, 'Marcar con una X el motivo de retiro:', 1, 0, 'C', 1);
        $this->Cell(40, 7, '', 1, 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        $fill = false;

        $seleccionOtros = false;

        foreach ($motivos as $motivo) {

            if ($motivo->motivoRetiro != 'Otros') {
                $this->Cell(140, 7, $motivo->motivoRetiro, 'LR', 0, 'L', $fill);

                if ($motivo->seleccionado) {
                    $this->writeHTMLCell(40, 7, '', '', '<h3>X</h3>', 'R', 1, $fill, true, 'C', 1);
                } else {
                    $this->writeHTMLCell(40, 7, '', '', '', 'R', 1, $fill, true, 'C', 1);
                }
                $fill = !$fill;
            } else {
                $seleccionOtros = $motivo->seleccionado;
            }
        }

        $this->Cell(140, 7, $motivo->motivoRetiro, 'LR', 0, 'L', $fill);
        if ($seleccionOtros) {
            $this->writeHTMLCell(40, 7, '', '', '<h3>X</h3>', 'R', 1, $fill, true, 'C', 1);
        } else {
            $this->writeHTMLCell(40, 7, '', '', '', 'R', 1, $fill, true, 'C', 1);
        }
        $this->Cell(180, 0, '', 'T');

        $this->ln(2);
        $this->SetFont('dejavusans', 'B', 8);
        $this->Write(0, 'Explique:', '', 0, '', true, 0, false, false, 0);
        $this->ln(3);
        $this->SetFont('dejavusans', '', 8);

        if (count($detallesMotivos)) {
            $this->writeHTML("<p style='text-align: justify'>" . implode(', ', $detallesMotivos) . "</p>", false, false, false, false, '');
        } else {
            $this->writeHTML("<hr>", true, false, false, false, '');
        }
    }

    function setDatosPersonales($datosPersonales = null)
    {
        $this->ln(8);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->ln();


        $this->SetFont('dejavusans', 'B', 10);
        $this->Cell(45, 8, 'Nombre de asociado:', 0, 0, 'L', 0);
        $this->SetFont('dejavusans', '', 10);
        $this->Cell(135, 8, $datosPersonales->nombresAsociado . ' ' . $datosPersonales->apellidosAsociado, 0, 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 10);
        $this->Cell(45, 8, 'N° DUI:', 0, 0, 'L', 0);
        $this->SetFont('dejavusans', '', 10);
        $this->Cell(135, 8, $datosPersonales->numeroDocumento, 0, 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 10);
        $this->Cell(45, 8, 'N° NIT:', 0, 0, 'L', 0);
        $this->SetFont('dejavusans', '', 10);
        $this->Cell(135, 8, $datosPersonales->NIT, 0, 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 10);
        $this->Cell(45, 8, 'Telefono:', 0, 0, 'L', 0);
        $this->SetFont('dejavusans', '', 10);
        $this->Cell(135, 8, "Teléfono fijo: " . $datosPersonales->telefonoFijo . ' / Teléfono móvil: ' . $datosPersonales->telefonoMovil, 0, 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 10);
        $this->Cell(45, 8, 'Email:', 0, 0, 'L', 0);
        $this->SetFont('dejavusans', '', 10);
        $this->Cell(135, 8, $datosPersonales->email, 0, 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 10);
        $this->Cell(45, 8, 'Dirección:', 0, 0, 'L', 0);
        $this->SetFont('dejavusans', '', 10);
        $this->writeHTMLCell(135, 6, '', '', '<p style="text-align: justify">' . $datosPersonales->direccionCompleta . ', ' . $datosPersonales->nombreMunicipio . ', ' . $datosPersonales->nombreDepartamento . '</p>', 0, 1, 1, true, 'L', 0);


        $this->SetFont('dejavusans', 'B', 10);
        $this->Cell(45, 8, 'Fecha:', 0, 0, 'L', 0);
        $this->SetFont('dejavusans', '', 10);
        $this->Cell(135, 8, date('d-m-Y', strtotime($datosPersonales->fechaRegistro)), 0, 0, 'L', 1);
        $this->ln();
    }

    function setInformeFinanciero($solicitud = null, $productos = [])
    {
        $this->ln(3);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);

        $totalProductos = 0;
        $tblIzquierda = [];
        $tblDerecha = [];

        foreach ($productos as $producto) {
            $tmp = new stdClass();
            $tmp->label = $producto->tipoCuenta;
            $monto = 0;
            foreach ($solicitud->productos as $productoIncluido) {
                if ($producto->id == $productoIncluido->idProducto) {
                    $monto = floatval($productoIncluido->monto);
                }
            }

            $tmp->valor = "$" . number_format($monto, 2, '.', ',');
            $totalProductos += $monto;
            array_push($tblIzquierda, $tmp);
        }

        $deudores = [];
        foreach ($solicitud->deudores as $deudor) {
            array_push($deudores, $deudor->nombreDeudor);
        }
        $montoPrestamos = !empty($solicitud->montoPrestamos) ? "$" . number_format(floatval($solicitud->montoPrestamos), 2, '.', ',') : "$0.00";
        $montoCuentasCobrar = !empty($solicitud->montoCuentasCobrar) ? "$" . number_format(floatval($solicitud->montoCuentasCobrar), 2, '.', ',') : "$0.00";

        array_push($tblIzquierda, (object)["label" => 'Total de ahorros', "valor" => '$' . number_format($totalProductos, 2, '.', ',')]);
        array_push($tblIzquierda, (object)["label" => 'Fecha de ingreso', "valor" => $solicitud->fechaAfiliacion]);

        array_push($tblDerecha, (object)["label" => "Tarjeta de débito", "valor" => ($solicitud->tarjetaDebito == 'S') ? 'SI' : 'NO']);
        array_push($tblDerecha, (object)["label" => "Préstamos", "valor" => $montoPrestamos]);
        array_push($tblDerecha, (object)["label" => "Cuentas por cobrar", "valor" => $montoCuentasCobrar]);
        array_push($tblDerecha, (object)["label" => "Es fiador de un asociado", "valor" => count($solicitud->deudores) > 0 ? 'SI' : 'NO']);
        array_push($tblDerecha, (object)["label" => "Si su respuesta es si, indique el nombre", "valor" => count($deudores) > 0 ? implode(", ", $deudores) : '']);

        $cnt = count($tblIzquierda);
        count($tblDerecha) > $cnt && $cnt = count($tblDerecha);
        for ($a = 0; $a < $cnt; $a++) {
            if (isset($tblIzquierda[$a])) {
                $this->SetFont('dejavusans', 'B', 6);
                $this->Cell(50, 6, $tblIzquierda[$a]->label . ':', 0, 0, 'C', 0);
                $this->SetFont('dejavusans', '', 6);
                $this->Cell(40, 6, $tblIzquierda[$a]->valor, 0, 0, 'C', 1);
            } else {
                $this->Cell(90, 6, '', 0, 0, 'C', 0);
            }
            if (isset($tblDerecha[$a])) {
                $this->SetFont('dejavusans', 'B', 6);
                $this->Cell(50, 6, $tblDerecha[$a]->label . ':', 0, 0, 'C', 0);
                $this->SetFont('dejavusans', '', 6);
                $this->Cell(40, 6, $tblDerecha[$a]->valor, 0, 0, 'C', 1);
            } else {
                $this->Cell(90, 6, '', 0, 0, 'C', 0);
            }

            $this->ln();
        }
    }

    function setGestion($gestiones)
    {
        $gestion = '';

        foreach ($gestiones as $tmp) {
            if ($tmp->area == '1') {
                $gestion = $tmp->gestion;
            }
        }

        $html = '<p style="text-align:justify; border-bottom:1px solid black;">' . $gestion . '</p>';

        $this->ln(4);
        $this->setFont('dejavusans', 'B', 9);
        $this->Write(0, 'Gestión realizada por Ejecutivo de atención al cliente:', '', 0, 'L', true, 0, false, false, 0);
        $this->ln(2);
        $this->setFont('dejavusans', '', 7);
        $this->writeHTMLCell(180, 7, '', '', $html, 'C', 0, 0, 0, '', 1);

        $this->ln(8);
        $this->firma('');
    }

    function setGestionVacia()
    {
        $this->ln(3);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(36, 6, 'Gestión realizada:', 0, 0, 'C', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Operaciones', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(36, 6, '__________________', 0, 0, 'C', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Negocios', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(36, 6, '__________________', 0, 0, 'C', 0);
        $this->ln();

        $this->ln(10);
        $this->setFont('dejavusans', '', 7);
        $this->writeHTML("<hr>", true, true, true, true, '');
        $this->ln(3);
        $this->writeHTML("<hr>", true, true, true, true, '');
        $this->ln(3);
        $this->writeHTML("<hr>", true, true, true, true, 0);

        $this->firma();
    }

    function setResolucion()
    {
        $this->ln(3);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Aprobada', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(24, 6, '__________________', 0, 0, 'C', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Pendiente', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(24, 6, '__________________', 0, 0, 'C', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Denegada', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(24, 6, '__________________', 0, 0, 'C', 0);
        $this->ln();

        $this->ln(4);
        $this->setFont('dejavusans', 'B', 9);
        $this->Write(0, 'Observaciones:', '', 0, 'L', true, 0, false, false, 0);
        $this->ln(5);
        $this->setFont('dejavusans', '', 7);
        $this->writeHTML("<hr>", true, true, true, true, '');
        $this->ln(3);
        $this->writeHTML("<hr>", true, true, true, true, '');
        $this->ln(3);
        $this->writeHTML("<hr>", true, true, true, true, '');

        $this->ln(11);

        $this->SetFont('dejavusans', 'B', 10);
        $this->Cell(180, 8, '____________________________________', 0, 0, 'C', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 8);
        $this->Cell(180, 8, 'Firma de autorización', 0, 0, 'C', 0);
    }

    function firmaDelAsociado()
    {
        $this->setFont('dejavusans', 'B', 9);
        $this->ln(10);
        $this->Write(0, '______________________________________________________________', '', 0, 'C', true, 0, false, false, 0);
        $this->Write(0, 'Firma del asociado/a', '', 0, 'C', true, 0, false, false, 0);
    }

    function firma($nombre = '')
    {
        $nombre = strlen($nombre) == 0 ? '____________________________________' : $nombre;

        $this->ln(7);
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(90, 8, $nombre, 0, 0, 'C', 0);
        $this->SetFont('dejavusans', 'B', 8);
        $this->Cell(90, 8, '____________________________________', 0, 0, 'C', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 6);
        $this->Cell(90, 5, 'Nombre:', 0, 0, 'C', 0);
        $this->Cell(90, 5, 'Firma:', 0, 0, 'C', 0);
        $this->ln();
    }
}
