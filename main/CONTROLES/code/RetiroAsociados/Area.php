<?php
class Area
{
    private $conexion;
    public $id, $area;

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    function obtenerAreas()
    {
        $query = 'SELECT * FROM controles_viewAreas;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }
}
