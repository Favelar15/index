<?php

class EstadoSolicitud {

    private $conexion;
    public $id, $estado;

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    function obtenerEstadosDeSolicitud()
    {
        $query = 'SELECT * FROM controles_view_estadoSolicitudes;';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__ );
        }

        $this->conexion = null;
        return [];
    }
}
