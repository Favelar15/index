<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

$respuesta = (object)[];

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {

    $now = date('d-m-Y');
    $nombreArchivo = "rptSolicitudesPagadas-".$now.".xlsx";

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'N°');
    $sheet->setCellValue('B1', 'Código de cliente');
    $sheet->setCellValue('C1', 'Nombre asociado');
    $sheet->setCellValue('D1', 'Agencia de Afiliación');
    $sheet->setCellValue('E1', 'Monto');
    $sheet->setCellValue('F1', 'Agencia');
    $sheet->setCellValue('G1', 'Fecha de registro');
    $sheet->setCellValue('H1', 'Fecha de resolución');
    $sheet->setCellValue('I1', 'Fecha de pago');

    $spreadsheet->getActiveSheet()->getStyle('A1:I1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $spreadsheet->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

    $solicitudes = $input['solicitudes'];

    $contador = 1;

    foreach ( $solicitudes as $solicitud )
    {
        $contador++;
        $sheet->setCellValue("A$contador", ($contador - 1) );
        $sheet->setCellValue("B$contador", $solicitud['codigoCliente']);
        $sheet->setCellValue("C$contador", $solicitud['nombresAsociado']);
        $sheet->setCellValue("D$contador", $solicitud['agenciaAfiliacion']);
        $sheet->setCellValue("E$contador", "$".$solicitud['monto']);
        $sheet->setCellValue("F$contador", $solicitud['agencia']);
        $sheet->setCellValue("G$contador", $solicitud['fechaRegistro']);
        $sheet->setCellValue("H$contador", $solicitud['fechaResolucion']);
        $sheet->setCellValue("I$contador", $solicitud['fechaPago']);
    }

    $writer = new Xlsx($spreadsheet);
    $writer->save(__DIR__ . '/../docs/solicitudRetiro/Excel/'.$nombreArchivo);
    $respuesta->{"respuesta"} = "EXITO";
    $respuesta->{'nombreArchivo'} = $nombreArchivo;

} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
