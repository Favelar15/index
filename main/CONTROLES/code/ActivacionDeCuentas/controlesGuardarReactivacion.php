<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if ( isset( $_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../Models/Permiso.php';
    require_once 'Models/ActivacionCuenta.php';

    $aplicaciones = $input['aplicaciones'];
    $aplicacionesDB = '';

    if ( count( $aplicaciones ) > 0  )
    {
        $subAplicacion = [];

        foreach ( $aplicaciones as $aplicacion )
        {
            array_push( $subAplicacion, $aplicacion['idAgencia'].'%%%'.$aplicacion['idSubAplicacion'].'%%%'.$aplicacion['numeroCuenta']);
        }
        $aplicacionesDB = implode('@@@', $subAplicacion);
    }

    $activacion = new ActivacionCuenta();
    $activacion->setIdApartado( (int) base64_decode( urldecode( $input['idApartado'] )) );
    $activacion->setTipoApartado( $input['tipoApartado'] );
    $activacion->codigoCliente = (int) $input['codigoCliente'];
    $activacion->aplicaciones = $aplicacionesDB;
    $activacion->idUsuario = (int) $_SESSION['index']->id;
    $activacion->idAgencia = (int) $_SESSION['index']->agenciaActual->id;
    $activacion->ipOrdenador = generalObtenerIp();
    $respuestaDb = $activacion->guardar();

    $respuesta['respuesta'] = $respuestaDb->respuesta;
    $respuesta['idSolicitud'] = $respuestaDb->idSolicitud;
    $respuesta['nombreArchivo'] = $respuestaDb->nombreArchivo;

} else
{
    $respuesta['respuesta'] = 'SESION';
}


echo json_encode( $respuesta );
