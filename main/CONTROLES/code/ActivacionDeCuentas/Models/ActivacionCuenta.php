<?php

class ActivacionCuenta extends Permiso
{

    private $path = __DIR__ . '\..\..\..\docs\solicitudesActivacion\/';
    private $conexion;
    public $idActivacion;
    public $codigoCliente, $idUsuario, $ipOrdenador, $aplicaciones, $nombreArchivo, $idAgencia, $fechaRegistro;

    private $agenciasAsignadas = [];

    function setAgencias($agencias)
    {
        $this->agenciasAsignadas = $agencias;
    }

    function __construct()
    {
        parent::__construct();
        global $conexion;

        $this->conexion = $conexion;

        if (!empty($this->fechaRegistro)) {
            $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        }
    }

    function guardar()
    {
        $respuesta = null;
        $idDb = 0;

        $query = 'EXEC controles_guardarActivacion :idApartado, :tipoApartado, :codigoCliente, :aplicaciones, :idUsuario, :ipOrdenador, :agenciaOrigen, :nombreArchivo, :idActivacion, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoPermiso, PDO::PARAM_STR);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':aplicaciones', $this->aplicaciones, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':agenciaOrigen', $this->idAgencia, PDO::PARAM_INT);
        $result->bindParam(':nombreArchivo', $this->nombreArchivo, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':idActivacion', $idDb, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ($respuesta == 'EXITO') {
            return (object)['respuesta' => $respuesta, 'nombreArchivo' => $this->nombreArchivo, 'idSolicitud' => $idDb];
        }
        return (object)['respuesta' => $respuesta, 'nombreArchivo' => null, 'idSolicitud' => null];
    }

    public function setIdApartado($idApartado)
    {
        $this->idApartado = $idApartado;
    }

    public function setTipoApartado($tipoApartado)
    {
        $this->tipoPermiso = $tipoApartado;
    }

    function generarPDF()
    {
        $activacion = $this->obtenerActivacion();

        $pdf = new solicitudActivacion(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', true);

        $pdf->setDatosActivacion($activacion);

        $pdf->configuracionDocumento();
        $pdf->setMargin();

        $pdf->SetAutoPageBreak(TRUE, 35);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();

        $pdf->setFont('dejavusans', 'B', 10);
        $pdf->Write(0, 'SOLICITUD DE ACTIVACIÓN DE CUENTAS', '', 0, 'C', true, 0, false, false, 0);
        $pdf->writeHTML("<hr>", true, false, false, false, '');
        $pdf->ln(3);

        $pdf->setFont('dejavusans', 'B', 9);
        $pdf->Write(0, 'Señores:', '', 0, '', true, 0, false, false, 0);
        $pdf->ln(1);
        $pdf->Write(0, 'Asociación Cooperativa de Ahorro y Crédito', '', 0, '', true, 0, false, false, 0);
        $pdf->ln(1);
        $pdf->Write(0, 'Presente', '', 0, '', true, 0, false, false, 0);

        $pdf->ln(3);

        $pdf->Write(0, 'Estimados Señores:', '', 0, '', true, 0, false, false, 0);
        $pdf->ln(1);
        $pdf->Write(0, 'Por este medio solícito la reactivación de mis cuenta, segun detalle a continuación.', '', 0, '', true, 0, false, false, 0);
        $pdf->ln(5);

        $pdf->cuentas();
        $pdf->datosPersonsales();
        $pdf->firma();

        $pdf->Output($this->path . $activacion->nombreArchivo, 'F');

        $this->conexion = null;

        if (file_exists($this->path . $activacion->nombreArchivo)) {
            return (object)["respuesta" => "EXITO", "nombreArchivo" => $activacion->nombreArchivo];
        }

        return (object)["respuesta" => "Error al construir archivo", "nombreArchivo" => null];

        // $pdf->Output('MyPDF_File.pdf', 'I');
    }

    function obtenerActivacion()
    {
        $query = "SELECT * FROM controles_datosActivacionCuentas WHERE id = :id";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->idActivacion, PDO::PARAM_INT);
        $result->execute();

        if ($result->rowCount() > 0) {
            $datosActivacion = $result->fetch(PDO::FETCH_OBJ);
            $datosActivacion->cuentas = $this->getDetalleCuentas();

            $datosActivacion->fechaRegistro = date('d-m-Y h:i a', strtotime($datosActivacion->fechaRegistro));
            return $datosActivacion;
        }
        $this->conexion = null;
        return [];
    }

    function obtenerActivaciones($roles)
    {
        $this->isAdministrativo($roles);

        if (!$this->administrativo) {
            $query = "SELECT * FROM controles_viewListadoActivacionCuentas WHERE idUsuario = " . $this->idUsuario . " AND idAgencia IN (" . implode(',', $this->agenciasAsignadas) . ") AND ( fechaRegistro is null OR cast(getdate() as date) <= DATEADD(month, 1, cast(fechaRegistro as date)) ) ORDER BY fechaRegistro DESC";
        } else {
            $query = "SELECT * FROM controles_viewListadoActivacionCuentas WHERE idAgencia IN (" . implode(',', $this->agenciasAsignadas) . ") AND ( fechaRegistro is null OR cast(getdate() as date) <= DATEADD(month, 1, cast(fechaRegistro as date)) ) ORDER BY fechaRegistro DESC";
        }

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function getDetalleCuentas()
    {
        $query = "SELECT * FROM controles_viewDetActivacionCuentas WHERE idActivacion = :idActivacion";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idActivacion', $this->idActivacion, PDO::PARAM_INT);

        $result->execute();

        if ($result->rowCount() > 0) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_OBJ);
        }
        $this->conexion = null;
        return [];

    }

    function eliminar()
    {
        $respuesta = null;

        $query = 'EXEC controles_spEliminarActivacion :idApartado, :tipoApartado, :idActivacion, :idUsuario, :ipOrdenador, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoPermiso, PDO::PARAM_STR);
        $result->bindParam(':idActivacion', $this->idActivacion, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        return $respuesta;
    }

    function reporteActivacionCuentas( $desde, $hasta )
    {
        $query = "SELECT * FROM controles_viewRptActivaciones WHERE idAgencia IN (" . implode(',', $this->agenciasAsignadas) . ") AND  CAST(fechaRegistro AS DATE) BETWEEN '$desde' AND '$hasta'";

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            $activaciones = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);

            foreach ( $activaciones as $activacion )
            {
                $subAplicaciones = [];
                $subAplicacionesDB = explode('@@@', $activacion->subAplicaciones );

                foreach ( $subAplicacionesDB as $sub )
                {
                    $temp = explode('%%%', $sub);
                    array_push( $subAplicaciones, "$temp[0]-$temp[1] [$temp[3]]");
                }

                $activacion->subAplicaciones = implode(', ', $subAplicaciones );
            }

            return $activaciones;
        }
        $this->conexion = null;
        return [];
    }
}

class solicitudActivacion extends TCPDF
{
    private $activacion;

    function setDatosActivacion($datos)
    {
        $this->activacion = $datos;
    }

    function configuracionDocumento()
    {
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('Departamento IT');
        $this->SetTitle('Solicitud de activación de cuenta');
        $this->SetSubject('ACACYPAC');
        $this->SetKeywords('TCPDF, PDF, Solicitud de activación de cuenta');

        $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    }

    function setMargin()
    {
        $this->SetMargins(15, 15, 15, 15);
        $this->SetHeaderMargin(4);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);
    }

    function Header()
    {
        $this->setFont('dejavusans', 'B', 10);
        $this->Cell(185, 6, 'N° de solicitud: ' . str_pad($this->activacion->id, 7, '0', STR_PAD_LEFT), 0, 0, 'R', 0);
    }

    function Footer()
    {
        $this->setFont('dejavusans', 'B', 10);
        $this->Write(0, $this->activacion->agencia . ' | ' . $this->activacion->fechaRegistro, '', 0, 'R', true, 0, false, false, 0);
    }

    function cuentas()
    {
        $this->SetFillColor(0, 128, 57);
        $this->SetTextColor(255);

        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B', 7);

        $this->SetFont('', 'B');

        $this->Cell(20, 6, 'N°', 'LTR', 0, 'C', 1);
        $this->Cell(100, 6, 'Tipo de cuenta', 'TLR', 0, 'C', 1);
        $this->Cell(65, 6, 'Numéro de cuenta', 'LTR', 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        $fill = false;

        if (count($this->activacion->cuentas) > 0) {
            $contador = 0;
            foreach ($this->activacion->cuentas as $cuenta) {
                $contador++;
                $this->Cell(20, 6, $contador, 'LR', 0, 'C', $fill);
                $this->Cell(100, 6, $cuenta->idSubAplicacion . ' - ' . $cuenta->tipoCuenta, 'LR', 0, 'C', $fill);
                $this->Cell(65, 6, $cuenta->numeroCuenta, 'LR', 0, 'C', $fill);
                $this->ln();

                $fill = !$fill;
            }
        }


        $this->Cell(185, 0, '', 'T');
    }

    function datosPersonsales()
    {
        $this->ln(5);

        $this->SetFont('dejavusans', 'B', 9);
        $this->Cell(185, 7, 'Atentamente,', 0, 0, 'L', 0);
        $this->ln();

        $this->ln(2);

        $this->SetFillColor(0, 128, 57);
        $this->SetTextColor(255);

        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B', 7);

        $this->Cell(185, 6, 'Datos personales del asociado', 1, 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(40, 6, 'Nombre del asociado', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(145, 6, $this->activacion->nombresAsociado . ' ' . $this->activacion->apellidosAsociado, 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(40, 6, 'Código de cliente', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(145, 6, $this->activacion->codigoCliente, 'LR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(40, 6, 'DUI', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(145, 6, $this->activacion->numeroDocumentoPrimario, 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(40, 6, 'Dirección', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->writeHTMLCell(145, 6, '', '', '<p style="text-align: justify">' . $this->activacion->direccionCompleta . ' ' . $this->activacion->municipio . ' ' . $this->activacion->departamento . '</p>', 'LR', 1, 0, true, 'L', 1);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(40, 6, 'Agencia de Afiliación', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(145, 6, $this->activacion->agenciaAfiliacion, 'LR', 0, 'L', 1);
        $this->ln();


        $this->Cell(185, 0, '', 'T');
    }

    function firma()
    {
        $this->ln(20);

        $this->setFont('dejavusans', 'B', 9);
        $this->Cell(60, 6, '____________________________________', false, 0, 'L', 0);
        $this->ln();
        $this->setFont('dejavusans', '', 9);
        $this->Cell(55, 6, 'Firma del asociado', false, 0, 'C', 0);
        $this->ln();

        $this->ln(5);
        $this->setFont('dejavusans', 'B', 9);
        $this->Cell(185, 6, 'Exclusico para ACACYPAC NC DE RL', false, 0, 'L', 0);
        $this->ln();
        $this->Cell(185, 6, '____________________________________________________________________________________________________________________', false, 0, 'L', 0);
        $this->ln();
        $this->Cell(185, 6, '____________________________________________________________________________________________________________________', false, 0, 'L', 0);
        $this->ln();

        $this->ln(20);

        $this->setFont('dejavusans', 'B', 9);
        $this->Cell(61, 6, '____________________________', false, 0, 'C', 0);
        $this->Cell(61, 6, '____________________________', false, 0, 'C', 0);
        $this->Cell(61, 6, '____________________________', false, 0, 'C', 0);
        $this->ln();

        $this->setFont('dejavusans', '', 9);
        $this->Cell(61, 6, $this->activacion->agencia, false, 0, 'C', 0);
        $this->Cell(61, 6, 'Sello', false, 0, 'C', 0);
        $this->Cell(61, 6, 'Firma de Autorización', false, 0, 'C', 0);
        $this->ln();


    }

}