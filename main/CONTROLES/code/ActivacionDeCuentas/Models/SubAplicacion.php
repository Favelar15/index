<?php

class SubAplicacion extends Permiso
{
    public $idSubAplicacion, $tipoCuenta, $fechaRegistro,$fechaActualizacion, $idUsuario, $ipOrdenador;
    private $conexion;

    function __construct()
    {
        parent::__construct();

        global $conexion;
        $this->conexion = $conexion;
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
        !empty($this->fechaActualizacion) && $this->fechaActualizacion = date('d-m-Y h:i a', strtotime($this->fechaActualizacion));
    }

    function obtenerSubAplicaciones( $cat = false )
    {
        $query = $cat ? "SELECT idSubAplicacion, tipoCuenta FROM controles_viewCatSubAplicaciones" : "SELECT * FROM controles_viewCatSubAplicaciones";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

    function guardar()
    {
        $respuesta = null; $accion = null;

        $query = 'EXEC controles_spGuardarCatSubAplicacion :idApartado, :tipoApartado, :idSubAplicacion, :tipoCuenta, :idUsuario, :respuesta, :accion';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idSubAplicacion', $this->idSubAplicacion, PDO::PARAM_INT);
        $result->bindParam(':tipoCuenta', $this->tipoCuenta, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':accion', $accion, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ( $respuesta == 'EXITO')
        {
            if ( $accion == 'INSERT')
            {
                return (object)['status' => true,'respuesta' => $respuesta, 'fechaRegistro' => date('d-m-Y h:s a'), 'fechaActualizacion' => date('d-m-Y h:s a') ];
            } else
            {
                return (object)['status' => true,'respuesta' => $respuesta, 'fechaRegistro' => null, 'fechaActualizacion' => date('d-m-Y h:s a') ];
            }
        }

        return (object)['status' => false, 'respuesta' => $respuesta, 'fechaRegistro' => null ];
    }

    function eliminar()
    {
        $respuesta = null;

        $query = 'EXEC controles_eliminarSubApartado :idApartado, :tipoApartado, :idSubAplicacion, :idUsuario, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idSubAplicacion', $this->idSubAplicacion, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        return $respuesta;
    }

    public function setIdApartado ($idApartado)
    {
        $this->idApartado = $idApartado;
    }

    public function setTipoApartado ($tipoApartado)
    {
        $this->tipoApartado = $tipoApartado;
    }
}