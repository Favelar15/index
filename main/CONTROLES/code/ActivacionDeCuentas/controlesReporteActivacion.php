<?php
include "../../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

$respuesta = [];

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {

    $idAgencias = [];
    $nombreArchivo = 'rpt-activacionDeCuentas-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';

    if ( $input['cboAgencia']['value'] == 'all')
    {
        $agenciasAsignadas = $_SESSION['index']->agencias;

        foreach ( $agenciasAsignadas as $agencia )
        {
            array_push( $idAgencias, (int) $agencia->id);
        }
    } else
    {
        $nombreArchivo = 'rpt-activacionDeCuentas-'.$input['cboAgencia']['value'].'-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';
        array_push( $idAgencias, (int) $input['cboAgencia']['value']);
    }

    if ($input['accion'] == 'EXPORTAR')
    {
        $activacion = new ExcelActivacion();
        $activacion->agencias = $idAgencias;
        $activacion->nombreArchivo = $nombreArchivo;
        $activacion->activaciones = $input['activaciones'];
        $respuesta = $activacion->crearReporte();

    } else
    {
        require_once '../../../code/connectionSqlServer.php';
        require_once '../Models/Permiso.php';
        require_once 'Models/ActivacionCuenta.php';

        $desde = date('Y-m-d', strtotime( $input['txtInicio']['value'] ));
        $hasta =  date('Y-m-d', strtotime( $input['txtFin']['value'] ));

        $activacion = new ActivacionCuenta();
        $activacion->setAgencias( $idAgencias );
        $respuesta = ['activaciones' => $activacion->reporteActivacionCuentas( $desde, $hasta ) ];

    }
}

echo json_encode($respuesta);

class ExcelActivacion
{
    public $nombreArchivo;
    public $agencias = [];
    public $activaciones = [];

    function crearReporte()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $agencias = implode(',', $this->agencias );

        $sheet->setCellValue('A1', "Reporte de activación de cuentas de agencias ($agencias)");
        $spreadsheet->getActiveSheet()->mergeCells("A1:H1");
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A2', 'N°');
        $sheet->setCellValue('B2', 'Código cliente');
        $sheet->setCellValue('C2', 'Asociado');
        $sheet->setCellValue('D2', 'Número de documento');
        $sheet->setCellValue('E2', 'Agencia');
        $sheet->setCellValue('F2', 'Cuentas');
        $sheet->setCellValue('G2', 'Usuario');
        $sheet->setCellValue('H2', 'Fecha de registro');

        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $spreadsheet->getActiveSheet()->getStyle('A2:H2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:H2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $contador = 2;

        foreach ( $this->activaciones as $activacion )
        {
            $contador++;
            $sheet->setCellValue("A$contador", ($contador - 2) );
            $sheet->setCellValue("B$contador", $activacion['codigoCliente']);
            $sheet->setCellValue("C$contador", $activacion['nombresAsociado']);
            $sheet->setCellValue("D$contador", $activacion['numeroDocumentoPrimario']);
            $sheet->setCellValue("E$contador", $activacion['agencia'] );
            $sheet->setCellValue("F$contador", $activacion['subAplicaciones']);
            $sheet->setCellValue("G$contador",  $activacion['usuario']);
            $sheet->setCellValue("H$contador", $activacion['fechaRegistro']);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '../../../docs/solicitudesActivacion/Excel/'.$this->nombreArchivo);
        return (Object)[
            'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];
    }
}
