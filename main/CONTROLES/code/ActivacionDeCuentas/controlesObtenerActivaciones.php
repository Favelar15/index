<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['activaciones' => []];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    require_once '../../../code/connectionSqlServer.php';
    require_once '../Models/Permiso.php';
    require_once 'Models/ActivacionCuenta.php';

    $agenciasAsignadas = $_SESSION['index']->agencias;
    $roles = (array) $_SESSION['index']->roles;

    $idAgencias = [];

    foreach ($agenciasAsignadas as $agencia) {
        array_push($idAgencias, (int)$agencia->id);
    }

    $activacion = new ActivacionCuenta();
    $activacion->idUsuario = $_SESSION['index']->id;
    $activacion->setIdApartado(  base64_decode(urldecode($_GET["idApartado"])) );
    $activacion->setTipoApartado(  $_GET['tipoApartado'] );
    $activacion->setAgencias( $idAgencias );

    $respuesta['activaciones'] = $activacion->obtenerActivaciones( $roles );

} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);