<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = [];

if ( isset($_SESSION['index'])  && $_SESSION['index']->locked )
{
    if ( isset( $_GET['idSolicitud'] ))
    {
        require_once '../../../code/connectionSqlServer.php';
        require_once '../Models/Permiso.php';
        require_once 'Models/ActivacionCuenta.php';

        $activacion = new ActivacionCuenta();
        $activacion->idActivacion = $_GET['idSolicitud'];
        $respuestaTemporal = $activacion->generarPDF();
        $respuesta['respuesta'] = $respuestaTemporal->respuesta;
        $respuesta['nombreArchivo'] = $respuestaTemporal->nombreArchivo;

    } else {
        $respuesta['respuesta'] = 'La solicitud no existe';
        $respuesta['nombreArchivo'] = null;
    }
} else
{
    $respuesta['respuesta'] = 'SESION';
}
echo json_encode( $respuesta  );
