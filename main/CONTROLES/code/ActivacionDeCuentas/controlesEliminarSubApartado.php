<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if( isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../Models/Permiso.php';
    require_once 'Models/SubAplicacion.php';

    $subAplicacion = new SubAplicacion();
    $subAplicacion->idSubAplicacion = $input['idSubAplicacion'];
    $subAplicacion->idUsuario = $_SESSION['index']->id;
    $subAplicacion->setIdApartado( base64_decode(urldecode($input['idApartado'])) );
    $subAplicacion->setTipoApartado( $input['tipoApartado'] );

    $respuesta['respuesta'] = $subAplicacion->eliminar();
} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );