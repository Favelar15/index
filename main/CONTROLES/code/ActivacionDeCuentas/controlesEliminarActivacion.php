<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    require_once '../../../code/connectionSqlServer.php';
    require_once '../Models/Permiso.php';
    require_once 'Models/ActivacionCuenta.php';

    $activacion = new ActivacionCuenta();
    $activacion->setIdApartado( base64_decode(urldecode($input['idApartado'])) );
    $activacion->setTipoApartado($input['tipoApartado']);
    $activacion->idActivacion = $input['idActivacion'];
    $activacion->idUsuario = $_SESSION['index']->id;
    $activacion->ipOrdenador = generalObtenerIp();

    $respuesta['respuesta'] = $activacion->eliminar();
} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);