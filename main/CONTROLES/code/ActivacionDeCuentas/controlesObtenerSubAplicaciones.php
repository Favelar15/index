<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['subAplicaciones' => [] ];

if ( isset( $_SESSION['index']) && $_SESSION['index']->locked )
{
    require_once '../../../code/connectionSqlServer.php';
    require_once '../Models/Permiso.php';
    require_once 'Models/SubAplicacion.php';

    $subAplicacion = new SubAplicacion();
    $respuesta['subAplicaciones'] = $subAplicacion->obtenerSubAplicaciones();
} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );