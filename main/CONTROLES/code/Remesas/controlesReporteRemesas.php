<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();

$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {

    $idAgencias = [];
    $nombreArchivo = 'rpt-remesas-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';

    if ( $input['cboAgencia']['value'] == 'all')
    {
        $agenciasAsignadas = $_SESSION['index']->agencias;

        foreach ( $agenciasAsignadas as $agencia )
        {
            array_push( $idAgencias, (int) $agencia->id);
        }
    } else
    {
        $nombreArchivo = 'rpt-remesas-'.$input['cboAgencia']['value'].'-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';
        array_push( $idAgencias, (int) $input['cboAgencia']['value']);
    }

    if( $input['accion'] === 'EXPORTAR')
    {
        $reporte = new ExcelRemesas();
        $reporte->agencias = $idAgencias;
        $reporte->remesas = $input['remesas'];
        $reporte->nombreArchivo = $nombreArchivo;
        $respuesta = $reporte->crearReporte();

    } else {

        require_once '../../../code/connectionSqlServer.php';
        require_once 'Models/Remesas.php';
        include 'Models/Transacion.php';
        include 'Models/Remesador.php';

        $remesa = new Remesas();
        $remesa->setAgencias( $idAgencias );
        $desde = date('Y-m-d', strtotime( $input['txtInicio']['value'] ));
        $hasta =  date('Y-m-d', strtotime( $input['txtFin']['value'] ));

        $respuesta = ['remesas' => $remesa->reporteRemesas( $desde, $hasta ) ];
    }
}

echo json_encode($respuesta);


class ExcelRemesas
{
    public $agencias = [];
    public $nombreArchivo;
    public $remesas = [];


    function crearReporte()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $agencias = implode(',', $this->agencias );
        $sheet->setCellValue('A1', "Reporte de envío y pago de remesas de agencias ($agencias)");
        $spreadsheet->getActiveSheet()->mergeCells("A1:H1");
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A2', 'N°');
        $sheet->setCellValue('B2', 'Fecha de transacción');
        $sheet->setCellValue('C2', 'Tipo de operación');
        $sheet->setCellValue('D2', 'Remesador');
        $sheet->setCellValue('E2', 'Numero de operaciones');
        $sheet->setCellValue('F2', 'Monto');
        $sheet->setCellValue('G2', 'Agencia');
        $sheet->setCellValue('H2', 'Usuario');


        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $spreadsheet->getActiveSheet()->getStyle('A2:H2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:H2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');


        $contador = 2;

        foreach ( $this->remesas as $remesa )
        {
            $contador++;
            $sheet->setCellValue("A$contador", ($contador - 2) );
            $sheet->setCellValue("B$contador", $remesa['fecha']);
            $sheet->setCellValue("C$contador", $remesa['Transaccion']['transaccion']);
            $sheet->setCellValue("D$contador", $remesa['Remesador']['remesador']);
            $sheet->setCellValue("E$contador", $remesa['numeroDeOperaciones']);
            $sheet->setCellValue("F$contador", '$'.$remesa['monto']);
            $sheet->setCellValue("G$contador",  $remesa['Agencia']['agencia']);
            $sheet->setCellValue("H$contador", $remesa['usuario']);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '../../../docs/remesas/Excel/'.$this->nombreArchivo);
        return (Object) [
          'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];
    }
}

