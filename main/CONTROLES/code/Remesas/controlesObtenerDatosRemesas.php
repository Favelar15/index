<?php
header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['remesas' => []];;

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {

    $idAgencias = [];

    $agenciasAsignadas = $_SESSION['index']->agencias;

    foreach ( $agenciasAsignadas as $agencia )
    {
        array_push( $idAgencias, (int) $agencia->id);
    }

    require_once '../../../code/connectionSqlServer.php';
    include 'Models/Remesas.php';
    include 'Models/Transacion.php';
    include 'Models/Remesador.php';

    $idAgencia = (int) $_SESSION['index']->agenciaActual->id;
    $idUsuario = (int) $_SESSION['index']->id;

    $remesa = new Remesas();

    $remesa->setAgencias( $idAgencias );
    $remesas = $remesa->obtenerRemesas();

    $respuesta = ['remesas' => $remesas];
}

echo json_encode($respuesta);


