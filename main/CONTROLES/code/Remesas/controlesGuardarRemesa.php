<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";


session_start();
$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    include "../../../code/connectionSqlServer.php";
    $idAgencia = $_SESSION['index']->agenciaActual->id;
    $idUsuario = $_SESSION['index']->id;
    $ipOrdenador = generalObtenerIp();

    $idApartado = base64_decode(urldecode($input['idApartado']));
    $tipoApartado = $input['tipoApartado'];

    $respuestaDb = null;

    $logsDB = '';
    $datosRemesas = $input['datosRemesas'];
    $remesaDB = '';

    if (count($datosRemesas)) {
        $tempRemesa = [];
        $logTemp = [];

        foreach ($datosRemesas as $remesa) {
            $id = $remesa['id'];
            $fecha = date('Y-m-d', strtotime($remesa['fecha']));
            $idTransacion = $remesa['Transaccion']['id'];
            $idRemesador = $remesa['Remesador']['id'];
            $numeroDeOperaciones = $remesa['numeroDeOperaciones'];
            $monto = $remesa['monto'];
            $logs = $remesa['logs'];

            if (count($logs)) {
                foreach ($logs as $log) {
                    $campo = $log['campo'];
                    $valorAntiguo = $log['valorAnterior'];
                    $valorNuevo = $log['valorNuevo'];
                    $temp = $campo . '¿¿¿' . $valorAntiguo . '¿¿¿' . $valorNuevo;
                    array_push($logTemp, $temp);
                }
                $logsDB = implode("&&&", $logTemp);
            } else {
                $logsDB = 'NULL';
            }

            $temp = $id . "%%%" . $fecha . '%%%' . $idTransacion . '%%%' . $idRemesador . '%%%' . $numeroDeOperaciones . '%%%' . $monto . '%%%' . $logsDB;
            array_push($tempRemesa, $temp);
        }
        $remesaDB = implode("@@@", $tempRemesa);
    }

    include 'Models/Remesas.php';

    $agencia = new stdClass();
    $agencia->id = $idAgencia;

    $remesaModel = new Remesas();
    $remesaModel->ipOrdenador = $ipOrdenador;
    $remesaModel->idUsuario = $idUsuario;
    $remesaModel->Agencia = $agencia;
    $respuestaDb =  $remesaModel->guardarRemesa($remesaDB, $idApartado, $tipoApartado);

    if ($respuestaDb->status) {
        $respuesta['respuesta'] = $respuestaDb->respuesta;
    } else {
        $respuesta = $respuestaDb->respuesta;
    }

    $conexion = null;
} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);
