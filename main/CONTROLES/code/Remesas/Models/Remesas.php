<?php

class Remesas
{
    public $id, $fecha, $numeroDeOperaciones;
    private $idAgencia, $agencia, $idTransaccion, $transaccion, $idRemesador, $remesador;
    public $Agencia, $Transaccion, $Remesador, $propietario;
    public $ipOrdenador, $idUsuario;
    private $conexion;
    private $agenciasAsignadas = [];

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        $this->Agencia = new stdClass();

        if ( !empty( $this->fecha ) )
        {
            $this->fecha = date('d-m-Y', strtotime( $this->fecha ));
        }

        !empty($this->monto) && $this->monto = sprintf('%0.2f', $this->monto);


        if( !empty($this->idAgencia) && !empty($this->agencia))
        {
            $this->Agencia->id = $this->idAgencia;
            $this->Agencia->agencia = $this->agencia;
        }

        if ( !empty($this->idTransaccion) && !empty($this->transaccion) )
        {
            $this->Transaccion = new Transacion();
            $this->Transaccion->id = $this->idTransaccion;
            $this->Transaccion->transaccion = $this->transaccion;
        }

        if ( !empty($this->idRemesador) && !empty($this->remesador) )
        {
            $this->Remesador = new Remesador();
            $this->Remesador->id = $this->idRemesador;
            $this->Remesador->remesador = $this->remesador;
        }

        if( !empty( $this->idUsuario ) )
        {
            $this->propietario = ( $this->idUsuario == $_SESSION['index']->id ) ? 'S' : 'N';
        }

    }

    function obtenerRemesas ()
    {
        $values = str_repeat('?,',count( $this->agenciasAsignadas) - 1 ).'?';

        $query = "SELECT * FROM controles_viewDatosRemesas WHERE idAgencia IN (".$values .") AND fecha > DATEADD (MONTH, -1,  CAST ( GETDATE() AS DATE )) ORDER BY fecha DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute( $this->agenciasAsignadas );

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function reporteRemesas( $desde, $hasta )
    {
        $query = "SELECT * FROM controles_viewDatosRemesas WHERE idAgencia IN(".implode(',', $this->agenciasAsignadas).") AND fecha BETWEEN '".$desde."' AND '".$hasta."' ORDER BY fecha DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function guardarRemesa ( $datosRemesas, $idApartado, $tipoApartado )
    {
        $respuesta = '';

        $query = 'EXEC controles_SpGuardarRemesas :datosRemesa, :idUsuario, :idAgencia, :ipOrdenador, :tipoApartado, :idApartado, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':datosRemesa', $datosRemesas, PDO::PARAM_STR);
        $result->bindParam(':idAgencia', $this->Agencia->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        // return (object)[$datosRemesas, $this->Agencia->id, $this->idUsuario, $this->ipOrdenador, $tipoApartado, $idApartado];

        $result->execute();

        if ( $respuesta == 'EXITO' )
        {
            return (object)['status' => true, 'respuesta' => $respuesta ];
        }
        return (object)['status' => false, 'respuesta' => $respuesta ];
    }

    function eliminarRemesa ( $idApartado, $tipoApartado )
    {
        $respuesta = null;

        $query = 'EXEC controles_spEliminarRemesa :id, :idUsuario, :ipOrdenador, :tipoApartado, :idApartado, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ( $respuesta == 'EXITO' )
        {
            return (object)['status' => true, 'respuesta' => $respuesta ];
        }
        return (object)['status' => false, 'respuesta' => $respuesta ];
    }

    function setAgencias ( $agencias ) {
        $this->agenciasAsignadas = $agencias;
    }
}