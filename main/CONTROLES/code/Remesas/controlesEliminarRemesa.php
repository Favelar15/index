<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";
session_start();

$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    include "../../../code/connectionSqlServer.php";

    $idUsuario = $_SESSION['index']->id;
    $ipOrdenador = generalObtenerIp();

    $idApartado = base64_decode(urldecode($input['idApartado']));
    $tipoApartado = $input['tipoApartado'];
    $id = $input['id'];

    $respuestaDb = null;

    include 'Models/Remesas.php';

    $remesaModal = new Remesas();
    $remesaModal->id = $id;
    $remesaModal->idUsuario = $idUsuario;
    $remesaModal->ipOrdenador = $ipOrdenador;
    $respuestaDb = $remesaModal->eliminarRemesa($idApartado, $tipoApartado);

    if ($respuestaDb->status) {
        $respuesta['respuesta'] = $respuestaDb->respuesta;
    } else {
        $respuesta = 'error' . $respuestaDb->respuesta;
    }
}

echo json_encode($respuesta);
