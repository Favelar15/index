<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && ($_SESSION["index"]->locked)) {
    if (!empty($_GET) && isset($_GET["q"])) {
        include "../../code/connectionSqlServer.php";

        $solicitados = explode("@@@", $_GET["q"]);

        if (in_array('tiposDocumento', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/tipoDocumento.php';
            $tipoDocumento = new tipoDocumento();
            $tiposDocumento = $tipoDocumento->obtenerTiposDocumento();

            $respuesta->{"tiposDocumento"} = $tiposDocumento;
        }

        if (in_array('estadoCivil', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/estadoCivil.php';
            $estadoCivil = new estadoCivil();
            $estadosCiviles = $estadoCivil->obtenerEstados();

            $respuesta->{"estadosCiviles"} = $estadosCiviles;
        }

        if (in_array('actividadEconomica', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/actividadEconomica.php';
            $actividadEconomica = new actividadEconomica();
            $actividadesEconomicas = $actividadEconomica->obtenerActividades();

            $respuesta->{"actividadesEconomicas"} = $actividadesEconomicas;
        }

        if (in_array('agencias', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/agencia.php';
            $agencia = new agencia();
            $agencias = $agencia->getAgenciasCbo();

            $respuesta->{"agencias"} = $agencias;
        }

        if (in_array('agenciasAsignadas', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/agencia.php';
            require_once 'Pagadurias/Models/Asesor.php';

            $agencia = new agencia();
            $agencias = $agencia->getAgenciasCbo();


            $asesor = new Asesor();
            $asesor->agenciasAsignadas = $_SESSION['index']->agencias;
            $asesorPorAgencia = $asesor->obtenerAgencias();

            $agenciasResponse = [];

            foreach ( $asesorPorAgencia as $key => $ag )
            {
                foreach ($agencias as $agencia ) {

                    if( $key === $agencia->id )
                    {
                        $agencia->asesores = $ag->asesores;
                        array_push($agenciasResponse, $agencia);
                    }
                }
            }

            $respuesta->{"agenciasAsignadas"} =  $agenciasResponse;
        }

        if (in_array('geograficos', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/datosGeograficos.php';
            $geografico = new geografico();
            $datosGeograficos = $geografico->obtenerGeograficos();

            $respuesta->{"datosGeograficos"} = $datosGeograficos;
        }

        if (in_array('motivosRetiro', $solicitados) || in_array('all', $solicitados)) {
            require_once 'RetiroAsociados/Motivos.php';
            $motivo = new Motivos();
            $respuesta->{"motivosRetiro"} = $motivo->obtenerMotivos();
        }

        if (in_array('productos', $solicitados) || in_array('all', $solicitados)) {
            require_once 'RetiroAsociados/Producto.php';
            $producto = new Producto();
            $respuesta->productos = $producto->obtenerProductos();
        }

        if (in_array('areas', $solicitados) || in_array('all', $solicitados)) {
            require_once 'RetiroAsociados/Area.php';
            $area = new Area();
            $respuesta->areas = $area->obtenerAreas();
        }

        if (in_array('estadosDeSolicitud', $solicitados) || in_array('all', $solicitados)) {
            require_once 'RetiroAsociados/EstadoSolicitud.php';
            $estado = new EstadoSolicitud();
            $respuesta->estadosSolicitud = $estado->obtenerEstadosDeSolicitud();
        }

        if (in_array('resoluciones', $solicitados) || in_array('all', $solicitados)) {
            require_once 'RetiroAsociados/Resolucion.php';
            $resolucion = new Resolucion();
            $respuesta->resoluciones = $resolucion->obtenerResolucion();
        }

        if (in_array('transacciones', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Remesas/Models/Transacion.php';
            $transaccion = new Transacion();
            $respuesta->transacciones = $transaccion->obtenerTransacciones();
        }

        if (in_array('remesadores', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Remesas/Models/Remesador.php';
            $remesador = new Remesador();
            $respuesta->remesadores = $remesador->obtenerRemesadores();
        }

        if (in_array('prestamos', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Creditos/Models/Prestamos.php';
            $prestamo = new Prestamos();
            $respuesta->prestamos = $prestamo->obtenerPrestamos();
        }

        if (in_array('empresas', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Creditos/Models/Empresa.php';
            $empresa = new Empresa();
            $respuesta->empresas = $empresa->obtenerEmpresas();
        }

        if (in_array('acciones', $solicitados) || in_array('all', $solicitados)) {
            require_once 'PYP/Models/Acciones.php';
            $accion = new Acciones();
            $respuesta->acciones = $accion->obtenerAcciones();
        }

        if (in_array('tiposSolicitudes', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Peps/Models/TipoSolicitud.php';
            $tipoSolicitud = new TipoSolicitud();
            $respuesta->tiposSolicitudes = $tipoSolicitud->obtenerTipoSolicitud();
        }

        if (in_array('tiposAfiliacion', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Peps/Models/TipoAfiliacion.php';
            $tipoAfiliacion = new TipoAfiliacion();
            $respuesta->tiposAfiliacion = $tipoAfiliacion->obtenerTiposAfiliacion();
        }

        if (in_array('cargosPoliticos', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Peps/Models/CargoPolitico.php';
            $cargosPoliticos = new CargoPolitico();
            $respuesta->cargosPoliticos = $cargosPoliticos->obtenerCargosPoliticos();
        }

        if (in_array('parentezcos', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Peps/Models/Parentezco.php';
            $parentezco = new Parentezco();
            $respuesta->parentezcos = $parentezco->obtenerParentezcos();
        }

        if (in_array('productosServicios', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Peps/Models/ProductoServicio.php';
            $servicio = new ProductoServicio();
            $respuesta->productosServicios = $servicio->obtenerProductosServicios();
        }

        if (in_array('entidades', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Peps/Models/Entidad.php';
            $entidad = new Entidad();
            $respuesta->entidades = $entidad->obtenerEntidades();
        }

        if (in_array('tipoRelacion', $solicitados) || in_array('all', $solicitados)) {
            require_once 'Peps/Models/TipoRelacion.php';
            $tipoRelacion = new TipoRelacion();
            $respuesta->tipoRelacion = $tipoRelacion->obtenerTipoRelacion();
        }

        if (in_array('roles', $solicitados) || in_array('all', $solicitados)) {
            require_once '../../code/Models/rol.php';
            $rol = new rol();
            if (in_array('rolesCbo', $solicitados)) {
                $roles = $rol->getRolesCbo();
            } else {
                $roles = $rol->getRoles();
            }

            $respuesta->{"roles"} = $roles;
        }

        if ( in_array('subAplicaciones', $solicitados) || in_array('all', $solicitados))
        {
            require_once 'Models/Permiso.php';
            require_once 'ActivacionDeCuentas/Models/SubAplicacion.php';
            $subAplicacion = new SubAplicacion();
            $respuesta->subAplicaciones = $subAplicacion->obtenerSubAplicaciones(true);
        }

        if ( in_array('programasBandesal', $solicitados) || in_array('all', $solicitados))
        {
            require_once 'Models/Permiso.php';
            require_once 'GarantiaBandesal/Models/Programa.php';
            $programa = new Programa();
            $respuesta->programasBandesal = $programa->obtenerProgramasBandesal();
        }

        if ( in_array('tiposGarantias', $solicitados) || in_array('all', $solicitados))
        {
            require_once 'Models/Permiso.php';
            require_once 'GarantiaBandesal/Models/TipoGarantia.php';
            $garantia = new TipoGarantia();
            $respuesta->tiposGarantias = $garantia->obtenerTiposGarantias();
        }

        $conexion = null;
    }
}

echo json_encode($respuesta);
