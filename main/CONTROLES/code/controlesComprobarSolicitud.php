<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";

$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    if (!empty($_GET) && isset($_GET['q'])) {
        include "../../code/connectionSqlServer.php";
        require_once '../../code/Models/asociado.php';
        require_once 'RetiroAsociados/retiroAsociado.php';
        $codigoCliente = base64_decode(urldecode($_GET["q"]));

        $solicitud = new retiroAsociado();
        $solicitud->codigoCliente = (int) $codigoCliente;
        $response = $solicitud->buscarAsociadoRetiro();

        if (count($response) > 0) {
            $respuesta->{"respuesta"} = $response["respuesta"];
            if (isset($response["datosAsociado"])) {
                $respuesta->{"datosAsociado"} = $response["datosAsociado"];
            }
            if (isset($response["estado"])) {
                $respuesta->{"estado"} = $response["estado"];
            }
        } else {
            $respuesta->{"respuesta"} = "Sin datos";
        }

        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
