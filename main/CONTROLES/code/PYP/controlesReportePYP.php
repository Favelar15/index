<?php
include "../../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

$respuesta = [];

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();

if (isset($_SESSION['index']) && $_SESSION["index"]->locked)
{
    if ( $input['accion'] == 'EXPORTAR')
    {
        $reporte = new ExcelPYP();
        $reporte->nombreArchivo = 'rpt-PYP-'.$input['cboAgencia']['labelOption'].'-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';
        $reporte->movimientos = $input['movimientos'];
        $reporte->agencia = $input['cboAgencia']['labelOption'];
        $respuesta = $reporte->crearReporte();

    } else {

        require_once '../../../code/connectionSqlServer.php';
        require_once 'Models/Inventario.php';

        $inventario = new Inventario();
        $inventario->id = (int) $input['cboAgencia']['value'];
        $desde = date('Y-m-d', strtotime( $input['txtInicio']['value'] ));
        $hasta =  date('Y-m-d', strtotime( $input['txtFin']['value'] ));
        $respuesta['movimientos'] = $inventario->reporteMovimiento($desde, $hasta);
    }
} else {
    $respuesta['SESION'] = 'SESION';
}
echo json_encode($respuesta);

class ExcelPYP
{
    public $nombreArchivo;
    public $agencia;
    public $movimientos = [];

    function crearReporte()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        $sheet->setCellValue('A1', "Detalle de movimientos de agencia: $this->agencia");
        $spreadsheet->getActiveSheet()->mergeCells("A1:E1");
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');


        $sheet->setCellValue('A2', 'N°');
        $sheet->setCellValue('B2', 'Agencia');
        $sheet->setCellValue('C2', 'Acción');
        $sheet->setCellValue('D2', 'Pines');
        $sheet->setCellValue('E2', 'Plásticos');

        $spreadsheet->getActiveSheet()->getStyle('A1:E1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $spreadsheet->getActiveSheet()->getStyle('A2:E2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:E2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $contador = 2;

        foreach ( $this->movimientos as $movimiento )
        {
            $contador++;
            $sheet->setCellValue("A$contador", ($contador - 2) );
            $sheet->setCellValue("B$contador", $movimiento['Agencia']['agencia']);
            $sheet->setCellValue("C$contador", $movimiento['Accion']['accion']);
            $sheet->setCellValue("D$contador", $movimiento['stockPines']);
            $sheet->setCellValue("E$contador", $movimiento['stockPlastico']);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '../../../docs/PYP/Excel/'.$this->nombreArchivo);
        return (Object)[
            'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];
    }
}
