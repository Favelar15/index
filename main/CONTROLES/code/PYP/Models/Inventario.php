<?php

require_once('Acciones.php');
//require_once ("/../../Models/Permiso.php");


class Inventario {

    public $id, $Accion, $Agencia, $stockPines, $stockPlastico, $idUsuario, $ipOrdenador, $fechaRegistro;
    public $idMovimiento, $nuevoStockPines, $nuevoStockPlastico;

    private $idApartado, $tipoApartado;

    private $idAgencia, $agencia, $idAccion, $accion;
    private $conexion;
    private $agenciasAsignadas = [];

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        if ( !empty( $this->fechaRegistro ) )
        {
            $this->fechaRegistro = date('d-m-Y h:m A', strtotime( $this->fechaRegistro ));
        }

        if ( !empty( $this->idAgencia ) && !empty( $this->agencia) )
        {
            $this->Agencia = new stdClass();
            $this->Agencia->id = $this->idAgencia;
            $this->Agencia->agencia = $this->agencia;
        }

        if ( !empty( $this->idAccion ) && !empty( $this->accion) )
        {
            $this->Accion = new Acciones();
            $this->Accion->id = $this->idAccion;
            $this->Accion->accion = $this->accion;
        }
    }

    function obtenerInventario ()
    {
        $values = str_repeat('?,',count( $this->agenciasAsignadas ) - 1 ).'?';

        $query = "SELECT * FROM controles_viewInventario WHERE idAgencia IN ( $values )";

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute( $this->agenciasAsignadas );


        if ( $result->rowCount() > 0 )
        {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__ );
        }
        return [];
    }

    function obtenerOneInventario() {

        $query = "SELECT * FROM controles_viewInventario WHERE id = :id";

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->execute();


        if ( $result->rowCount() > 0 )
        {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__ );
        }
        return [];
    }

    function guardarMovimiento ( $logs )
    {
        $respuesta = false;

        $query = 'EXEC controles_spGuardarInventario :id, :idInventario, :idAccion, :cantidadPines, :cantidadPlasticos, :idAgencia, :idUsuario, :ipOrdenador, :idApartado, :tipoApartado, :logs, :nuevoStockPines, :nuevoStockPlastico, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->idMovimiento, PDO::PARAM_INT);
        $result->bindParam(':idInventario', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idAccion', $this->Accion->id, PDO::PARAM_INT );
        $result->bindParam(':cantidadPines', $this->stockPines, PDO::PARAM_INT );
        $result->bindParam(':cantidadPlasticos', $this->stockPlastico, PDO::PARAM_INT );
        $result->bindParam(':idAgencia', $this->Agencia->id, PDO::PARAM_INT );
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT );
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT );
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':logs', $logs, PDO::PARAM_STR);
        $result->bindParam(':nuevoStockPines', $this->nuevoStockPines, PDO::PARAM_INT );
        $result->bindParam(':nuevoStockPlastico', $this->nuevoStockPlastico, PDO::PARAM_INT );
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

//        return (object) [   'idMovimiento'  => $this->idMovimiento,
//                            'idInventario' => $this->id,
//                            'logs' => $logs ];

        if( $respuesta == 'EXITO')
        {
            return (Object)[ 'status' => true, 'respuesta' => $respuesta ];
        }
        return (Object)[ 'status' => false, 'respuesta' => $respuesta ];
    }

    function obtenerMovimientos ()
    {
        $query = 'SELECT * FROM controles_viewDetMovimientosInventario WHERE id= :id';

        $result = $this->conexion->prepare( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL] );
        $result->bindParam(':id', $this->id, PDO::PARAM_INT );
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__ );
        }
        return [];
    }

    function obtenerAllMovimientos()
    {
        $query = 'SELECT * FROM controles_viewDetMovimientos WHERE idInventario = :id AND fechaRegistro > DATEADD (MONTH, -1, GETDATE() ) ORDER BY fechaRegistro DESC';

        $result = $this->conexion->prepare( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL] );
        $result->bindParam(':id', $this->id, PDO::PARAM_INT );
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__ );
        }
        return [];
    }

    function reporteMovimiento ( $desde, $hasta )
    {
        $query = "SELECT * FROM controles_viewDetMovimientos WHERE idInventario = $this->id AND CAST(fechaRegistro AS DATE) BETWEEN '$desde' AND '$hasta' ORDER BY fechaRegistro DESC";

        $result = $this->conexion->prepare( $query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL] );
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $movimientos = $result->fetchAll(PDO::FETCH_CLASS, __CLASS__ );

            $temp = (Object)[];

            foreach ( $movimientos as $movimiento )
            {
                if (property_exists($temp, $movimiento->idAgencia))
                {
                    foreach ( $temp->{$movimiento->idAgencia} as $row )
                    {
                        if( $row->Accion->accion == $movimiento->Accion->accion )
                        {
                            $row->stockPines += (int) $movimiento->stockPines;
                            $row->stockPlastico += (int) $movimiento->stockPlastico;

                        } else {
                            $temp->{$movimiento->idAgencia}[] = $movimiento;
                        }
                    }
                }else
                {
                    $temp->{$movimiento->idAgencia} = [];
                    $temp->{$movimiento->idAgencia} = [$movimiento];
                }
            }

            return $temp;
        }
        return [];
    }

    function setAgenciasAsignadas ( $agencias )
    {
        $this->agenciasAsignadas = $agencias;
    }

    function setIdApartado( $idApartado ) {
        $this->idApartado = base64_decode( urldecode( $idApartado ) );
    }

    function setTipoApartado( $tipoApartado )
    {
        $this->tipoApartado = $tipoApartado;
    }
}

//class PYP {
//    public $Inventario;
//
//    public function __construct ()
//    {
//        $this->Inventario = new Agen
//    }
//}
//
//class agenciaPYP {
//    public $agencia, $idAgencia;
//}

//701 {
//  agencia: nueva concepcion
//    detalle: {
//      NUEVA CONCEPCION: {
//           entradas: 100,
//          salidas: 100
//      }
//}