<?php

class Acciones
{
    public $id, $accion;
    private $conexion;

    function __construct ()
    {
        global $conexion;
        $this->conexion = $conexion;
    }


    function obtenerAcciones ()
    {
        $query = 'SELECT * FROM controles_viewCatAccionesInventarios';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }
}