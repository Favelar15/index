<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['inventarios' => [] ];

if ( isset( $_SESSION['index']) && $_SESSION['index']->locked )
{
    include '../../../code/connectionSqlServer.php';

    $idAgencias = [];

    $agenciasAsignadas = $_SESSION['index']->agencias;

    foreach ( $agenciasAsignadas as $agencia )
    {
        array_push( $idAgencias, (int) $agencia->id);
    }

    require_once 'Models/Inventario.php';

    $inventario = new Inventario();
    $inventario->setAgenciasAsignadas( $idAgencias );
    $respuesta['inventarios'] = $inventario->obtenerInventario();

} else
{
    $respuesta['sesion'] = 'SESION';
}

echo json_encode( $respuesta );