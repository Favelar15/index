<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['movimientos' => [] ];

if ( isset( $_SESSION['index']) && $_SESSION['index']->locked )
{

    $detalle = filter_var($_GET['detalle'], FILTER_VALIDATE_BOOLEAN);;

    if ( isset( $_GET['idInventario'] ) && $_GET['idInventario'] != 0 )
    {
        $idInventario = (int) $_GET['idInventario'] ;

        include '../../../code/connectionSqlServer.php';
        require_once 'Models/Inventario.php';

        $inventario = new Inventario();
        $inventario->id = $idInventario;

        if ( $detalle )
        {
            $respuesta['movimientos'] = $inventario->obtenerAllMovimientos() ;
            $respuesta['inventario'] = $inventario->obtenerOneInventario()[0];
        } else
        {
            $respuesta['movimientos'] = $inventario->obtenerMovimientos();
        }
    }
} else
{
    $respuesta['sesion'] = 'SESION';
}

echo json_encode( $respuesta );