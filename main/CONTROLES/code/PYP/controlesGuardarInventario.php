<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";
include "../../../code/connectionSqlServer.php";

session_start();
$respuesta = [];

if ( isset( $_SESSION['index']) && $_SESSION['index']->locked )
{
    $idUsuario = $_SESSION['index']->id;
    $ipOrdenador = generalObtenerIp();

    $idApartado =  $input['idApartado'] ;
    $tipoApartado = $input['tipoApartado'];

    $id = $input['idInventario'];
    $idMovimiento = $input['id'];
    $idAccion = (int) $input['cboAcciones']['value'];
    $idAgencia = (int) $input['cboAgencia']['value'];
    $cantidadPines = (int) $input['txtCantidadPines']['value'];
    $cantidadPlasticos = (int) $input['txtCantidadPlastico']['value'];

    $nuevoStockPines = !empty( $input['nuevoStockPines'] ) ?  (int) $input['nuevoStockPines'] : null;
    $nuevoStockPlastico = !empty( $input['nuevoStockPlastico'] ) ? (int) $input['nuevoStockPlastico'] : null ;

    $logs = $input['logs'];
    $logsDB = '';

    if ( count( $logs ) > 0 )
    {
        $logTemp = [];

        foreach ( $logs as $log )
        {
            $campo = $log['campo'];
            $valorAntiguo = $log['valorAnterior'];
            $valorNuevo = $log['valorNuevo'];
            $temp = $campo.'%%%'.$valorAntiguo.'%%%'.$valorNuevo;
            array_push( $logTemp, $temp );
        }
        $logsDB = implode("@@@", $logTemp);
    } else {
        $logsDB = 'NULL';
    }

    require_once('Models/Inventario.php'); require_once('Models/Acciones.php');
    $accion = new Acciones();
    $accion->id = $idAccion;

    $agencia = new stdClass();
    $agencia->id = $idAgencia;

    $inventario = new Inventario();
    $inventario->idMovimiento = $idMovimiento;
    $inventario->id = $id;
    $inventario->Accion = $accion;
    $inventario->stockPines = $cantidadPines;
    $inventario->stockPlastico = $cantidadPlasticos;
    $inventario->Agencia = $agencia;
    $inventario->idUsuario = $idUsuario;
    $inventario->ipOrdenador = $ipOrdenador;
    $inventario->nuevoStockPlastico = $nuevoStockPlastico;
    $inventario->nuevoStockPines = $nuevoStockPines;
    $inventario->setIdApartado( $idApartado );
    $inventario->setTipoApartado( $tipoApartado );

    $respuesta = $inventario->guardarMovimiento( $logsDB );

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );