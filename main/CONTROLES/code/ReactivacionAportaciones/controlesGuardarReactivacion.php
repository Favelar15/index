<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if ( isset( $_SESSION['index']) && $_SESSION['index']->locked )
{
    include "../../../code/connectionSqlServer.php";
    require_once '../Peps/Permiso.php';

    require_once 'Models/ReactivarCuenta.php';
    $reactivar = new ReactivarCuenta();
    $reactivar->codigoCliente = (int) $input['txtCodigoCliente']['value'];
    $reactivar->montoPendiente = (float) $input['txtMontoPendiente']['value'];
    $reactivar->abono = (float) $input['txtAbono']['value']  ;
    $reactivar->setIdApartado( (int) base64_decode( urldecode( $input['idApartado'] )) );
    $reactivar->setTipoApartado( $input['tipoApartado'] );
    $reactivar->idUsuario = (int) $_SESSION['index']->id;
    $reactivar->ipOrdenador = generalObtenerIp();
    $respuestaDb = $reactivar->guardar();

    $respuesta['respuesta'] = $respuestaDb->respuesta;

} else
{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );