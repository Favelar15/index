<?php


class ReactivarCuenta extends Permiso
{
    public $idUsuario, $ipOrdenador, $codigoCliente, $montoPendiente, $abono;
    private $conexion;

    function __construct()
    {
        parent::__construct();

        global $conexion;
        $this->conexion = $conexion;
    }

    function guardar()
    {
        $respuesta = null;

        $query = 'EXEC controles_guardarReactivacionAportaciones :idApartado, :tipoApartado, :codigoCliente, :montoPendiente, :abono, :idUsuario, :ipOrdenador, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':montoPendiente', $this->montoPendiente, PDO::PARAM_INT);
        $result->bindParam(':abono', $this->abono, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ( $respuesta == 'EXITO')
        {
            return (object)['status' => true,'respuesta' => $respuesta];
        }

        return (object)['status' => false, 'respuesta' => $respuesta ];
    }

    public function setIdApartado ($idApartado)
    {
        $this->idApartado = $idApartado;
    }

    public function setTipoApartado ($tipoApartado)
    {
        $this->tipoApartado = $tipoApartado;
    }

}