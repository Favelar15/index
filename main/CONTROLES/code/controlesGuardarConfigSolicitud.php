<?php

include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

session_start();
$respuesta = (object)[];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    include "../../code/connectionSqlServer.php";
    require_once '../../code/Models/asociado.php';
    require_once 'RetiroAsociados/retiroAsociado.php';

    $idApartado = (int)base64_decode(urldecode($input['idApartado']));
    $tipoApartado = $input['tipoApartado'];
    $idUsuario = $_SESSION['index']->id;

    $roles = $input['roles'];
    $rolesDb = '';

    if (count($roles) > 0)
    {
        $rolesTemp = [];

        foreach ($roles as $rol)
        {
            $motivosTemp = [];

            if ( count( $rol['motivos'] ) > 0 )
            {
                foreach ($rol['motivos'] as $motivo)
                {
                    array_push($motivosTemp, $motivo['idMotivo']);
                }
                array_push($rolesTemp, $rol['idRol'] . '%%%' . implode('###', $motivosTemp));
            } else
            {
                array_push($rolesTemp, $rol['idRol'] . '%%%NULL');
            }
        }
        $rolesDb = implode('@@@', $rolesTemp);
    }

    $solicitud = new solicitudRetiro();
    //$respuesta = $rolesDb;
    $respuesta  = $solicitud->guardarConfiguracion($rolesDb, $idUsuario, $idApartado, $tipoApartado);

} else {
    $respuesta->{'respuesta'} = 'SESION';
}

echo json_encode($respuesta);
