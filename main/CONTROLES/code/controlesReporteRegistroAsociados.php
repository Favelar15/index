<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";

session_start();

$respuesta = [ 'registros' => [] ];

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {

    $agencia = $input['idAgencia'];
    $idAgencias = [];

    if ($agencia == 'all') {
        $agenciasAsignadas = $_SESSION['index']->agencias;

        foreach ($agenciasAsignadas as $agencia) {
            array_push($idAgencias, (int)$agencia->id);
        }

    } else {
        array_push($idAgencias, (int)$agencia);
    }

    require_once '../../code/connectionSqlServer.php';
    require_once '../../code/Models/asociado.php';

    $desde = date('Y-m-d', strtotime( $input['txtInicio']['value'] ));
    $hasta =  date('Y-m-d', strtotime( $input['txtFin']['value'] ));

    $asociado = new asociado();
    $respuesta['registros'] = $asociado->reporteRegistroAsociados($idAgencias, $desde, $hasta);
}

echo json_encode($respuesta);


