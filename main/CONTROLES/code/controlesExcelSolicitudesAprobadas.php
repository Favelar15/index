<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

$respuesta = (object)[];

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked)
{
    $nombreArchivo = 'rpt-solicitudAprobadas-'.$input['desde'].'-'.$input['hasta'].'.xlsx';

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'N°');
    $sheet->setCellValue('B1', 'Código de cliente');
    $sheet->setCellValue('C1', 'Nombre asociado');
    $sheet->setCellValue('D1', 'Agencia Afiliación');
    $sheet->setCellValue('E1', 'Monto aportaciones');
    $sheet->setCellValue('F1', 'Ahorro');
    $sheet->setCellValue('G1', 'Total');
    $sheet->setCellValue('H1', 'Motivos por el cual se retiran');
    $sheet->setCellValue('I1', 'Agencia');
    $sheet->setCellValue('J1', 'Fecha de Aprobacion');
    $sheet->setCellValue('K1', 'Fecha de registro');

    $spreadsheet->getActiveSheet()->getStyle('A1:K1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $spreadsheet->getActiveSheet()->getStyle('A1:K1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

    $solicitudes = $input['solicitudes'];

    $contador = 1;

    foreach ( $solicitudes as $solicitud )
    {
        $contador++;
        $sheet->setCellValue("A$contador", ($contador - 1) );
        $sheet->setCellValue("B$contador", $solicitud['codigoCliente']);
        $sheet->setCellValue("C$contador", $solicitud['nombresAsociado']);
        $sheet->setCellValue("D$contador", $solicitud['agenciaAfiliacion']);
        $sheet->setCellValue("E$contador", $solicitud['productos']['Aportaciones']);
        $sheet->setCellValue("F$contador", $solicitud['productos']['Ahorros']);
        $sheet->setCellValue("G$contador",  sprintf('%0.2f', $solicitud['productos']['Aportaciones'] + $solicitud['productos']['Ahorros'] ) );
        $sheet->setCellValue("H$contador", $solicitud['motivos']);
        $sheet->setCellValue("I$contador", $solicitud['agencia']);
        $sheet->setCellValue("J$contador", $solicitud['fechaResolucion']);
        $sheet->setCellValue("K$contador", $solicitud['fechaRegistro']);
    }

    $writer = new Xlsx($spreadsheet);
    $writer->save(__DIR__ . '/../docs/solicitudRetiro/Excel/'.$nombreArchivo);
    $respuesta->{"respuesta"} = "EXITO";
    $respuesta->{'nombreArchivo'} = $nombreArchivo;

} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);

