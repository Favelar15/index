<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";

session_start();

$respuesta = ['solicitudes' => [] ];

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {

    $agencia = $input['idAgencia'];
    $idAgencias = [];

    if( $agencia == 'all')
    {
        $agenciasAsignadas = $_SESSION['index']->agencias;

        foreach ($agenciasAsignadas as $agencia) {
            array_push($idAgencias, (int)$agencia->id);
        }
    } else {
        array_push($idAgencias, (int) $agencia );
    }

    require_once '../../code/connectionSqlServer.php';
    require_once '../../code/Models/asociado.php';
    require_once 'RetiroAsociados/retiroAsociado.php';

    $solicitudes = new solicitudRetiro();
    if( !empty( $input['accion'] ))
    {
        $estado = $input['estado'];
        $desde = date('Y-m-d', strtotime( $input['txtInicioG']['value'] ));
        $hasta =  date('Y-m-d', strtotime( $input['txtFinG']['value'] ));
        $respuesta['solicitudes'] = $solicitudes->reporteSolicitudesAprobadas( $estado, $idAgencias, $desde, $hasta);
    } else
    {
        $desde = date('Y-m-d', strtotime( $input['txtFechaInicio']['value'] ));
        $hasta =  date('Y-m-d', strtotime( $input['txtFechaFin']['value'] ));
        $respuesta['solicitudes'] = $solicitudes->reporteSolicitudesPagadas( $idAgencias, $desde, $hasta);
    }
}

echo json_encode($respuesta);

