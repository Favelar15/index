<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$respuesta = (object)[];
$respuesta->{'respuesta'} = false;

session_start();

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {
    if (!empty($_GET) && isset($_GET['codigoCliente']) && $_GET["tipo"]) {
        include '../../code/connectionSqlServer.php';
        require_once '../../code/Models/asociado.php';

        $asociado = new asociado();
        $asociado->codigoCliente = urldecode($_GET["codigoCliente"]);
        $tipo = urldecode($_GET["tipo"]);

        $asociados = $asociado->buscarAsociadoRegistroActualizacion();

        $estado = 'EXITO';

        if (count($asociados) > 0) {
            $datosAsociado = $asociados[0];

            if ($datosAsociado->habilitado == 'N') {
                $estado = 'Retirado';
            } else {
                if ($tipo == 'Register') {
                    $estado = 'Registrado';
                } else {
                    $respuesta->{"datosAsociado"} = $datosAsociado;
                }
            }
        } elseif ($tipo == 'Update') {
            $estado = 'NoAsociado';
        }

        $respuesta->{'respuesta'} = $estado;
        $conexion = null;
    }
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
