<?php


class Persona {

    public $idActividadEconomicaPrimaria, $actividadPrimaria;
    public $idActividadEconomicaSecundaria, $actividadSecundaria;
    public $agencia;
    public $apellidosAsociado;
    public $codigoCliente;
    public $idDepartamento, $departamento;
    public $direccionCompleta;
    public $edad;
    public $email;
    public $estadoCivil;
    public $fechaAfiliacion;
    public $fechaNacimiento;
    public $genero;
    public $hojaLegal;
    public $idMunicipio, $municipio;
    public $nombreCompleto;
    public $nombresAsociado;
    public $idDocumentoPrimario, $numeroDocumentoPrimario;
    public $idDocumentoSecundario, $numeroDocumentoSecundario;
    public $idPais, $pais;
    public $profesion;
    public $telefonoFijo;
    public $telefonoMovil;
    public $tipoDocumentoPrimario;
    public $tipoDocumentoSecundario;
    public $usuario;

}