<?php

class Asesor
{
    private $conexion;
    public $nombre, $apellido, $asesor;
    public $agenciasAsignadas;

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

    }

    function obtenerAgencias()
    {
        $query = "select * from hp_viewObtenerEjecutivosAgenciasaCbo";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {

            $usuarios = $result->fetchAll(PDO::FETCH_OBJ);

            $agenciasR = (Object) [];

            foreach ($this->agenciasAsignadas as $idAgencia => $ag) {

                $agencia = new stdClass();
                $agencia->asesores = [];

                foreach ($usuarios as $key => $ejecutivo) {
                    $agencias = explode(",", $ejecutivo->agencias);
                    $roles = explode(",", $ejecutivo->roles);

                    if ( in_array($idAgencia, $agencias) && (in_array(37, $roles) || in_array(19, $roles))) {
                        array_push($agencia->asesores, $usuarios[$key] );
                    }
                }
                $agenciasR->{$idAgencia} = $agencia;
            }
            $this->conexion = null;
            return $agenciasR;
        }
    }
}