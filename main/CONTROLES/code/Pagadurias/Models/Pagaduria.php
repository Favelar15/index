<?php

class Pagaduria extends Permiso
{
    private $conexion;

    public $id,
        $codigoCliente,
        $numeroCredito,
        $monto,
        $cuota,
        $tipoGarantia,
        $institucion,
        $fechaDesembolso,
        $idAgencia, $idAsesor, $idUsuario, $ipOrdenador, $fechaRegistro;

    private $agenciasAsignadas = [];

    function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        !empty($this->monto) && $this->monto = sprintf('%0.2f', $this->monto);
        !empty($this->cuota) && $this->cuota = sprintf('%0.2f', $this->cuota);
        !empty($this->fechaDesembolso) && $this->fechaDesembolso = date('d-m-Y', strtotime( $this->fechaDesembolso ));
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:s a', strtotime( $this->fechaRegistro ));
    }

    function guardar( $logs )
    {
        $idDB = null;

        $query = 'EXEC controles_spGuardarPagadurias :idApartado, :tipoApartado, :id, :codigoCliente, :numeroCredito, :monto, :cuota, :tipoGarantia, :instituciones, :fechaDesembolso, :idAgencia, :idAsesor, :idUsuario, :ipOrdenador, :logs, :idDb, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':numeroCredito', $this->numeroCredito, PDO::PARAM_STR);
        $result->bindParam(':monto', $this->monto, PDO::PARAM_INT);
        $result->bindParam(':cuota', $this->cuota, PDO::PARAM_INT);
        $result->bindParam(':tipoGarantia', $this->tipoGarantia, PDO::PARAM_STR);
        $result->bindParam(':instituciones', $this->institucion, PDO::PARAM_STR);
        $result->bindParam(':fechaDesembolso', $this->fechaDesembolso, PDO::PARAM_STR);
        $result->bindParam(':idAgencia', $this->idAgencia, PDO::PARAM_INT);
        $result->bindParam(':idAsesor', $this->idAsesor, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':logs', $logs, PDO::PARAM_STR);
        $result->bindParam(':idDb',$idDB , PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        //return (object)[ 'USUARIO' => $this->idUsuario, 'tipoApartado' => $this->tipoApartado, 'idApartado' => $this->idApartado];

        $result->execute();

        if ( $respuesta == 'EXITO' )
        {
            return (object)['status' => true, 'respuesta' => $respuesta, 'id' => $idDB ];
        }
        return (object)['status' => false, 'respuesta' => $respuesta, 'id' => $idDB ];
    }

    function setAgencias ( $agencias ) {
        $this->agenciasAsignadas = $agencias;
    }

    function obtenerPagadurias()
    {
        $values = str_repeat('?,',count( $this->agenciasAsignadas) - 1 ).'?';

        $query = "SELECT * FROM controles_viewDatosPagadurias WHERE idAgencia IN (".$values .") AND fechaDesembolso > DATEADD (MONTH, -1,  CAST ( GETDATE() AS DATE )) ORDER BY fechaDesembolso DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute( $this->agenciasAsignadas );

        if ( $result->rowCount() > 0 )
        {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function reportePagadurias( $desde, $hasta )
    {
        $agencias = implode(',', $this->agenciasAsignadas );

        $query = "SELECT * FROM controles_viewDatosPagadurias WHERE idAgencia IN ($agencias) AND fechaDesembolso BETWEEN '$desde' AND '$hasta' ORDER BY fechaDesembolso DESC";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute( $this->agenciasAsignadas );

        if ( $result->rowCount() > 0 )
        {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function eliminarPagaduria()
    {
        $respuesta = null;

        $query = 'EXEC controles_eliminarPagaduria :idApartado, :tipoApartado, :id, :idUsuario, :ipOrdenador, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        return $respuesta;
    }

    public function setIdApartado ($idApartado)
    {
        $this->idApartado = $idApartado;
    }

    public function setTipoApartado ($tipoApartado)
    {
        $this->tipoApartado = $tipoApartado;
    }
}