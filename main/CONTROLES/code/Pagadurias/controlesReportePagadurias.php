<?php
include "../../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

$respuesta = [];

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();


if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {

    $idAgencias = [];
    $nombreArchivo = 'rpt-pagadurias-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';

    if ( $input['cboAgencia']['value'] == 'all')
    {
        $agenciasAsignadas = $_SESSION['index']->agencias;

        foreach ( $agenciasAsignadas as $agencia )
        {
            array_push( $idAgencias, (int) $agencia->id);
        }
    } else
    {
        $nombreArchivo = 'rpt-pagadurias-'.$input['cboAgencia']['value'].'-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';
        array_push( $idAgencias, (int) $input['cboAgencia']['value']);
    }

    if ( $input['accion'] == 'EXPORTAR')
    {
        $pagaduria = new ExcelPagaduria();
        $pagaduria->agencias = $idAgencias;
        $pagaduria->nombreArchivo = $nombreArchivo;
        $pagaduria->pagadurias = $input['pagadurias'];
        $respuesta = $pagaduria->crearReporte();

    } else
    {
        require_once '../../../code/connectionSqlServer.php';
        require_once '../Models/Permiso.php';
        require_once 'Models/Pagaduria.php';

        $pagaduria = new Pagaduria();
        $pagaduria->setAgencias( $idAgencias );

        $desde = date('Y-m-d', strtotime( $input['txtInicio']['value'] ));
        $hasta =  date('Y-m-d', strtotime( $input['txtFin']['value'] ));

        $respuesta['pagadurias'] = $pagaduria->reportePagadurias($desde, $hasta );
    }
}

echo json_encode($respuesta);

class ExcelPagaduria {

    public $nombreArchivo;
    public $agencias = [];
    public $pagadurias = [];

    function crearReporte()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $agencias = implode(',', $this->agencias);

        $sheet->setCellValue('A1', "Reporte de pagadurías de agencias ($agencias)");
        $spreadsheet->getActiveSheet()->mergeCells("A1:M1");
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A2', 'N°');
        $sheet->setCellValue('B2', 'Fecha de desembolso');
        $sheet->setCellValue('C2', 'Número de crédito');
        $sheet->setCellValue('D2', 'Código de cliente');
        $sheet->setCellValue('E2', 'Nombres del asociado');
        $sheet->setCellValue('F2', 'Tipo de garantía');
        $sheet->setCellValue('G2', 'Institución');
        $sheet->setCellValue('H2', 'Monto');
        $sheet->setCellValue('I2', 'cuota');
        $sheet->setCellValue('J2', 'Agencia');
        $sheet->setCellValue('K2', 'Ejecutivo de Créditos');
        $sheet->setCellValue('L2', 'Ejecutivo de Atención al Cliente');
        $sheet->setCellValue('M2', 'Fecha de registro');

        $spreadsheet->getActiveSheet()->getStyle('A1:M1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:M1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');
        $spreadsheet->getActiveSheet()->getStyle('A2:M2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:M2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $contador = 2;

        foreach ( $this->pagadurias as $pagaduria )
        {
            $contador++;
            $sheet->setCellValue("A$contador", ($contador - 2) );
            $sheet->setCellValue("B$contador", $pagaduria['fechaDesembolso']);
            $sheet->setCellValue("C$contador", $pagaduria['numeroCredito']);
            $sheet->setCellValue("D$contador", $pagaduria['codigoCliente']);
            $sheet->setCellValue("E$contador", $pagaduria['nombresAsociados']);
            $sheet->setCellValue("F$contador", $pagaduria['tipoGarantia']);
            $sheet->setCellValue("G$contador", $pagaduria['institucion'] );
            $sheet->setCellValue("H$contador", "$".number_format($pagaduria['monto'], 2, '.', ','));
            $sheet->setCellValue("I$contador", "$".number_format($pagaduria['cuota'], 2, '.', ','));
            $sheet->setCellValue("J$contador", $pagaduria['agencia']);
            $sheet->setCellValue("k$contador", $pagaduria['asesor']);
            $sheet->setCellValue("L$contador", $pagaduria['usuario']);
            $sheet->setCellValue("M$contador", $pagaduria['fechaRegistro']);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '/../../docs/Pagadurias/Excel/'.$this->nombreArchivo );
        return (Object)[
            'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];
    }
}
