<?php
header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['pagadurias' => [] ];

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {

    $idAgencias = [];

    $agenciasAsignadas = $_SESSION['index']->agencias;

    foreach ( $agenciasAsignadas as $agencia )
    {
        array_push( $idAgencias, (int) $agencia->id);
    }

    require_once '../../../code/connectionSqlServer.php';
    require_once '../Models/Permiso.php';
    require_once 'Models/Pagaduria.php';

    $pagaduria = new Pagaduria();
    $pagaduria->setAgencias( $idAgencias );
    $pagadurias = $pagaduria->obtenerPagadurias();

    $respuesta = ['pagadurias' => $pagadurias ];
}

echo json_encode($respuesta);


