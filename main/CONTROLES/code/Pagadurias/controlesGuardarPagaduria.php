<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {

    include "../../../code/connectionSqlServer.php";
    require_once '../Models/Permiso.php';
    require_once 'Models/Pagaduria.php';


    $logs = $input['logs'];
    $logsDB = '';

    if ( count($logs) > 0 )
    {
        $logTemp = [];

        foreach ($logs as $log) {
            $campo = $log['campo'];
            $valorAntiguo = $log['valorAnterior'];
            $valorNuevo = $log['valorNuevo'];
            $temp = $campo . '%%%' . $valorAntiguo . '%%%' . $valorNuevo;
            array_push($logTemp, $temp);
        }
        $logsDB = implode("@@@", $logTemp);
    }

    $pagaduria = new Pagaduria();
    $pagaduria->id = $input['id'];
    $pagaduria->codigoCliente = $input['codigoCliente'];
    $pagaduria->numeroCredito = $input['txtNumeroCredito']['value'];
    $pagaduria->monto = $input['txtMonto']['value'];
    $pagaduria->cuota = $input['txtCuota']['value'];
    $pagaduria->tipoGarantia = $input['txtTipoGarantia']['value'];
    $pagaduria->institucion = $input['txtInstitucion']['value'];
    $pagaduria->fechaDesembolso = date('Y-m-d', strtotime( $input['txtFechaDesembolso']['value'] ));
    $pagaduria->idAgencia = $input['cboAgencia']['value'];
    $pagaduria->idAsesor = $input['cboAsesor']['value'];
    $pagaduria->idUsuario = (int) $_SESSION['index']->id;
    $pagaduria->ipOrdenador = generalObtenerIp();

    $pagaduria->setIdApartado( (int) base64_decode(urldecode($input['idApartado'])) );
    $pagaduria->setTipoApartado(  $input['tipoApartado'] );

    $respuestaDb = null;

    $respuestaDb = $pagaduria->guardar( $logsDB );

    $respuesta['respuesta'] = $respuestaDb->respuesta;
    $respuesta['id'] = $respuestaDb->id;

    $conexion = null;

} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);
