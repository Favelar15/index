<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";
$respuesta = (object)[];

session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    include "../../code/connectionSqlServer.php";
    require_once '../../code/Models/asociado.php';
    include "./RetiroAsociados/retiroAsociado.php";

    $tipoApartado = $input["tipoApartado"];
    $idApartado = base64_decode(urldecode($input["idApartado"]));
    $idSolicitud = $input["idSolicitud"];
    $datosPersonales = $input["datosPersonales"];
    $motivosRetiro = $input["motivosRetiro"];
    $productos = $input["productos"];
    $detalleCreditos = $input["detalleCreditos"];
    $gestiones = $input["gestiones"];
    $idUsuario = $_SESSION["index"]->id;

    $solicitudRetiro = new solicitudRetiro();
    $solicitudRetiro->id = $idSolicitud;
    $solicitudRetiro->codigoCliente = $datosPersonales["txtCodigoCliente"]["value"];
    $solicitudRetiro->tipoDocumento = $datosPersonales["cboTipoDocumento"]["value"];
    $solicitudRetiro->numeroDocumento = $datosPersonales["txtDocumentoPrimario"]["value"];
    $solicitudRetiro->NIT = $datosPersonales["txtNIT"]["value"];
    $solicitudRetiro->nombresAsociado = $datosPersonales["txtNombres"]["value"];
    $solicitudRetiro->apellidosAsociado = $datosPersonales["txtApellidos"]["value"];
    $solicitudRetiro->profesion = $datosPersonales["txtProfesion"]["value"];
    $solicitudRetiro->agencia = $datosPersonales["cboAgencia"]["value"];
    $solicitudRetiro->fechaAfiliacion = $datosPersonales["txtFechaAfiliacion"]["value"];
    $solicitudRetiro->telefonoFijo = $datosPersonales["txtTelefonoFijo"]["value"];
    $solicitudRetiro->telefonoMovil = $datosPersonales["txtTelefonoMovil"]["value"];
    $solicitudRetiro->email = $datosPersonales["txtEmail"]["value"];
    $solicitudRetiro->tarjetaDebito = $datosPersonales["cboTarjeta"]["value"];
    $solicitudRetiro->pais = $datosPersonales["cboPais"]["value"];
    $solicitudRetiro->departamento = $datosPersonales["cboDepartamento"]["value"];
    $solicitudRetiro->municipio = $datosPersonales["cboMunicipio"]["value"];
    $solicitudRetiro->direccionCompleta = $datosPersonales["txtDireccion"]["value"];
    $solicitudRetiro->montoPrestamos = $detalleCreditos["montoPrestamos"];
    $solicitudRetiro->montoCuentasCobrar = $detalleCreditos["montoCuentasCobrar"];
    $solicitudRetiro->motivosRetiro = $motivosRetiro;
    $solicitudRetiro->productos = $productos;
    $solicitudRetiro->deudores = $detalleCreditos["deudores"];
    $solicitudRetiro->gestiones = $gestiones;

    $guardarDB = $solicitudRetiro->guardarSolicitud($tipoApartado, $idApartado, $idUsuario, $ipActual);

    // include "./controlesConstruccionSolicitudRetiro.php";
    // $archivo = new GenerarPDFSolicitudRetiro($idSolicitud, $nombreArchivo, $idUsuario);
    // $archivo->construirArchivo();
    if (isset($guardarDB["respuesta"])) {
        $respuesta->{"respuesta"} = $guardarDB["respuesta"];
        $respuesta->{"idSolicitud"} = $guardarDB["idSolicitud"];
    } else {
        $respuesta->{"respuesta"} = "Error al guardar en la base de datos";
    }

    $conexion = null;
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
