<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

session_start();
$respuesta = (object)[];

if (isset($_SESSION['index'])) {

    if (count($input) > 0) {

        include "../../code/connectionSqlServer.php";

        $codigoCliente = $input['txtCodigoAsociado']['value'];
        $tipoDocumentoPrimario = $input['cboTipoDocumento']['value'];
        $numeroDocumentoPrimario = $input['txtDocumentoPrimario']['value'];
        $tipoDocumentoSecundario = 2;
        $numeroDocumentoSecundario = $input['txtNit']['value'];
        $nombresAsociado = $input['txtNombres']['value'];
        $apellidosAsociado = $input['txtApellidos']['value'];
        $fechaNacimiento = isset($input['txtFechaNacimiento']) ? date('Y-m-d', strtotime($input['txtFechaNacimiento']['value'])) : null;
        $edad = isset($input['txtEdad']) ? $input['txtEdad']['value'] : null;
        $telefonoFijo = $input['txtTelefonoFijo']['value'];
        $telefonoMovil = $input['txtTelefonoMovil']['value'];
        $email = $input["txtEmail"]["value"];
        $genero = isset($input['cboGenero']) ? $input['cboGenero']['value'] : null;
        $idEstadoCivil = $input['cboEstadoCivil']['value'];
        $profesion = $input['txtProfesion']['value'];
        $idPais = $input['cboPais']['value'];
        $idDepartamento = $input['cboDepartamento']['value'];
        $idMunicipio = $input['cboMunicipio']['value'];
        $direccionCompleta = $input['txtDireccion']['value'];
        $hojaLegal = isset($input['txtHojaLegal']) ? $input['txtHojaLegal']['value'] : null;
        $idActividadPrimaria = $input['cboActividadEconomicaPrincipal']['value'];
        $idActividadSecundaria = $input['cboActividadEconomicaSecundaria']['value'];
        $idAgencia = $input['cboAgencia']['value'];
        $fechaAfilicacion = date('Y-m-d', strtotime($input['txtFechaAfiliacion']['value']));
        $idUsuario = $_SESSION['index']->id;
        $accion = isset($input["accion"]) ? $input["accion"] : 'Nuevo';
        $ipOrdenador = generalObtenerIp();
        $logs = isset($input['logs']) ? $input['logs'] : [];

        $logsDB = '';



        $response = null;

        $query = 'EXEC controles_spGuardarAsociado :codigoCliente, :idTipoDocumentoPrimario, :numeroDocumentoPrimario, :idTipoDocumentoSecundario, :numeroDocumentoSecundario, :nombresAsociado, :apellidosAsociado, :fechaNacimiento, :edad, :telefonoFijo, :telefonoMovil,:email, :genero, :idEstadoCivil, :profesion, :idPais, :idDepartamento, :idMunicipio, :direccionCompleta, :hojaLegal, :idActividadEconomicaPrimaria, :idActividadEconomicaSecundaria, :idAgencia, :fechaAfilicacion, :idUsuario, :ipOrdenador,:accion, :logs, :response;';
        $result = $conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':codigoCliente', $codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':idTipoDocumentoPrimario', $tipoDocumentoPrimario, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumentoPrimario', $numeroDocumentoPrimario, PDO::PARAM_STR);
        $result->bindParam(':idTipoDocumentoSecundario', $tipoDocumentoSecundario, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumentoSecundario', $numeroDocumentoSecundario, PDO::PARAM_STR);
        $result->bindParam(':nombresAsociado', $nombresAsociado, PDO::PARAM_STR);
        $result->bindParam(':apellidosAsociado', $apellidosAsociado, PDO::PARAM_STR);
        $result->bindParam(':fechaNacimiento', $fechaNacimiento, PDO::PARAM_STR);
        $result->bindParam(':edad', $edad, PDO::PARAM_INT);
        $result->bindParam(':telefonoFijo', $telefonoFijo, PDO::PARAM_STR);
        $result->bindParam(':telefonoMovil', $telefonoMovil, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':genero', $genero, PDO::PARAM_STR);
        $result->bindParam(':idEstadoCivil', $idEstadoCivil, PDO::PARAM_INT);
        $result->bindParam(':profesion', $profesion, PDO::PARAM_STR);
        $result->bindParam(':idPais', $idPais, PDO::PARAM_INT);
        $result->bindParam(':idDepartamento', $idDepartamento, PDO::PARAM_INT);
        $result->bindParam(':idMunicipio', $idMunicipio, PDO::PARAM_INT);
        $result->bindParam(':direccionCompleta', $direccionCompleta, PDO::PARAM_STR);
        $result->bindParam(':hojaLegal', $hojaLegal, PDO::PARAM_INT);
        $result->bindParam(':idActividadEconomicaPrimaria', $idActividadPrimaria, PDO::PARAM_INT);
        $result->bindParam(':idActividadEconomicaSecundaria', $idActividadSecundaria, PDO::PARAM_INT);
        $result->bindParam(':idAgencia', $idAgencia, PDO::PARAM_INT);
        $result->bindParam(':fechaAfilicacion', $fechaAfilicacion, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':accion', $accion, PDO::PARAM_STR);
        $result->bindParam(':logs', $logsDB, PDO::PARAM_STR);
        $result->bindParam(':response', $response, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ($response) {
            if ($response == 'EXITO') {
                $responseLog = null;
                $tipoApartado = $input["tipoApartado"];
                $idApartado = base64_decode(urldecode($input["idApartado"]));
                $arrLogs = [];

                foreach ($input["detallesLog"] as $detalleLog) {
                    $arrLogs[] = $detalleLog["campo"] . '%%%' . $detalleLog["valorAnterior"] . '%%%' . $detalleLog["valorNuevo"];
                }

                $detallesLog = count($arrLogs) > 0 ? implode('@@@', $arrLogs) : null;

                $queryLog = 'EXEC controles_spGuardarLogsAsociado :codigoCliente,:descripcion,:detalles,:usuario,:ip,:tipoApartado,:idApartado,:response;';
                $resultLog = $conexion->prepare($queryLog, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $resultLog->bindParam(':codigoCliente', $codigoCliente, PDO::PARAM_INT);
                $resultLog->bindParam(':descripcion', $input["descripcionLog"], PDO::PARAM_STR);
                $resultLog->bindParam(':detalles', $detallesLog, PDO::PARAM_STR);
                $resultLog->bindParam(':usuario', $idUsuario, PDO::PARAM_INT);
                $resultLog->bindParam(':ip', $ipOrdenador, PDO::PARAM_STR);
                $resultLog->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
                $resultLog->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
                $resultLog->bindParam(':response', $responseLog, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

                $resultLog->execute();
            }
            $respuesta->{"respuesta"} = $response;
        } else {
            $respuesta->{"respuesta"} = "Error en controles_spGuardarAsociado";
        }
        $conexion = null;
    }
} else {
    $respuesta->{'respuesta'} = 'SESION';
}

echo json_encode($respuesta);
