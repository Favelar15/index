<?php

header("Content-type: application/json; charset=utf-8");
include "../../code/generalParameters.php";
session_start();

$respuesta = [];


if ( isset($_SESSION['index'])  && $_SESSION['index']->locked )
{

    include "../../code/connectionSqlServer.php";
    include  '../../code/Models/asociado.php';
    include 'RetiroAsociados/retiroAsociado.php';

    $solicitud = new solicitudRetiro();
    $respuesta['configuracion'] = $solicitud->getConfiguracion();
}

echo json_encode( $respuesta );
