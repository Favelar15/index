<?php
include "../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

$respuesta = (object)[];

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();
if (isset($_SESSION["index"]) && $_SESSION["index"]->locked)
{
    $nombreArchivo = 'rpt-registrosAsociados-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';

    $idAgencias = [];

    if ( $input['cboAgencia']['value'] == 'all')
    {
        $agenciasAsignadas = $_SESSION['index']->agencias;
        foreach ( $agenciasAsignadas as $agencia )
        {
            array_push( $idAgencias, (int) $agencia->id);
        }
    } else
    {
        $nombreArchivo = 'rpt-registrosAsociados-'.$input['cboAgencia']['value'].'-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';
        array_push( $idAgencias, (int) $input['cboAgencia']['value']);
    }


    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    $agencias = implode(',', $idAgencias);

    $sheet->setCellValue('A1', "Registro de asociados: $agencias");
    $spreadsheet->getActiveSheet()->mergeCells("A1:H1");
    $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

    $sheet->setCellValue('A2', 'N°');
    $sheet->setCellValue('B2', 'Fecha de afiliación');
    $sheet->setCellValue('C2', 'Código de cliente');
    $sheet->setCellValue('D2', 'Nombre asociado');
    $sheet->setCellValue('E2', 'Agencia');
    $sheet->setCellValue('F2', 'Género');
    $sheet->setCellValue('G2', 'Hoja legal');
    $sheet->setCellValue('H2', 'Tipo de afiliación');

    $spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

    $spreadsheet->getActiveSheet()->getStyle('A2:H2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $spreadsheet->getActiveSheet()->getStyle('A2:H2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');


    $solicitudes = $input['registros'];

    $contador = 2;

    foreach ( $solicitudes as $solicitud )
    {
        $contador++;
        $sheet->setCellValue("A$contador", ($contador - 2) );
        $sheet->setCellValue("B$contador", $solicitud['fechaAfiliacion']);
        $sheet->setCellValue("C$contador", $solicitud['codigoCliente']);
        $sheet->setCellValue("D$contador", $solicitud['nombresAsociado']);
        $sheet->setCellValue("E$contador", $solicitud['agencia']);
        $sheet->setCellValue("F$contador", $solicitud['genero']);
        $sheet->setCellValue("G$contador",  $solicitud['hojaLegal']);
        $sheet->setCellValue("H$contador", $solicitud['tipoAfiliacion']);
    }

    $writer = new Xlsx($spreadsheet);
    $writer->save(__DIR__ . '/../docs/registrosAsociados/Excel/'.$nombreArchivo);
    $respuesta->{"respuesta"} = "EXITO";
    $respuesta->{'nombreArchivo'} = $nombreArchivo;

} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);

