<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";
$respuesta = (object)[];

session_start();

if (isset($_SESSION["index"]) && $_SESSION["index"]->locked) {
    include "../../code/connectionSqlServer.php";
    require_once '../../code/Models/asociado.php';
    require_once "./RetiroAsociados/retiroAsociado.php";

    $idSolicitud = $input["id"];
    $tipoApartado = $input["tipoApartado"];
    $idApartado = base64_decode(urldecode($input["idApartado"]));
    $idUsuario = $_SESSION["index"]->id;
    $solicitud = new solicitudRetiro();
    $solicitud->id = $idSolicitud;
    $eliminar = $solicitud->eliminarSolicitud($idUsuario, $tipoApartado, $idApartado, $ipActual);

    $respuesta->{"respuesta"} = $eliminar;

    $conexion = null;
} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);
