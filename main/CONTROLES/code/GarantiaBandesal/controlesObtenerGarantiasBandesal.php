<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = [ 'garantiasBandesal' => [] ];

if ( isset($_SESSION['index'])  && $_SESSION['index']->locked )
{
    include "../../../code/connectionSqlServer.php";
    require_once '../Models/Permiso.php';
    require_once 'Models/GarantiaBandesal.php';

    if ( !empty( $_GET['idGarantia']) )
    {
        $garantias = new GarantiaBandesal();
        $garantias->id = (int) $_GET['idGarantia'];
        $respuesta['garantiasBandesal'] = $garantias->obtenerGarantiaBandesal();
    } else
    {
        $agencias = (array) $_SESSION['index']->agencias;
        $roles = (array) $_SESSION["index"]->roles;

        $garantias = new GarantiaBandesal();
        $garantias->setIdApartado( base64_decode(urldecode($_GET["idApartado"])) );
        $garantias->setTipoApartado( $_GET['tipoApartado'] );
        $garantias->setAgencias( $agencias );
        $garantias->idUsuario = $_SESSION['index']->id;

        $respuesta['garantiasBandesal'] = $garantias->obtenerGarantiasBandesal( $roles );
    }

} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );