<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {

    include "../../../code/connectionSqlServer.php";
    require_once '../Models/Permiso.php';
    require_once 'Models/GarantiaBandesal.php';


    $proyectos = $input['proyectos'];
    $logs = $input['logs'];

    $proyectosDB = '';

    if (count($proyectos) > 0) {
        $proyectoTemp = [];

        foreach ($proyectos as $proyecto ) {
            $id = $proyecto['id'];
            $descripcion = !empty($proyecto['descripcion']) ? $proyecto['descripcion'] :'NULL';
            $idDepartamento = (int) $proyecto['idDepartamento'];
            $idMunicipio = (int) $proyecto['idMunicipio'];
            $direccion = $proyecto['direccion'];

            $temp = $id . '%%%' . $descripcion . '%%%' . $idDepartamento. '%%%'. $idMunicipio.'%%%'.$direccion;
            array_push($proyectoTemp, $temp);
        }
        $proyectosDB = implode("@@@", $proyectoTemp);
    }

    $logsDB = '';

    if( count( $logs ) > 0 )
    {
        $logsTemp = [];

        foreach ( $logs as $log )
        {
            array_push($logsTemp, $log['campo'].'%%%'.$log['valorAnterior'].'%%%'.$log['valorNuevo']);
        }
        $logsDB = implode('@@@', $logsTemp);
    }

    $garantia = new GarantiaBandesal();
    $garantia->setIdApartado( (int) base64_decode(urldecode($input['idApartado']))  );
    $garantia->setTipoApartado( $input['tipoApartado'] );
    $garantia->id = $input['id'];
    $garantia->codigoCliente = $input['codigoCliente'];
    $garantia->comprobante= $input['txtComprobante']['value'];
    $garantia->numeroPrestamo= $input['txtNumeroPrestamo']['value'];
    $garantia->monto= $input['txtMonto']['value'];
    $garantia->porcentajeGarantizado= $input['txtPorcentajeGarantizado']['value'];
    $garantia->montoGarantizado = $input['txtMontoGarantizado']['value'];
    $garantia->montoAdicional = $input['txtMontoAdicional']['value'];
    $garantia->fechaDesembolso = date('Y-m-d', strtotime( $input['txtFechaDesembolso']['value'] ));
    $garantia->fechaVencimiento = date('Y-m-d', strtotime( $input['txtFechaVencimiento']['value'] ));
    $garantia->porcentajeDescontado = $input['txtPorcentajeDescontado']['value'];
    $garantia->valorDescontado = $input['txtValorDescontado']['value'];
    $garantia->añosDeExperiencia= $input['txtAñosExperiencia']['value'];
    $garantia->idTipoGarantia = $input['cboTipoGarantia']['value'];
    $garantia->idPrograma = $input['cboPrograma']['value'];
    $garantia->idLineaPrestamo = $input['cboLineaPrestamo']['value'];
    $garantia->tipoPrestamo = $input['cboTipoPrestamo']['value'];
    $garantia->vencimientoLineaRotativa =  date('Y-m-d', strtotime( $input['txtFechaVencimientoRotativa']['value'] ) );
    $garantia->tipoPrograma = $input['cboEsProgara']['value'];
    $garantia->diasAtrasados = $input['txtDiasAtrasados']['value'];
    $garantia->clasificacionRiesgo = $input['txtRiesgo']['value'];
    $garantia->idAgencia = $input['cboAgencia']['value'];
    $garantia->idAsesor = $input['cboAsesor']['value'];
    $garantia->protectos = $proyectosDB;
    $garantia->idUsuario = $_SESSION['index']->id;
    $garantia->ipOrdenador= generalObtenerIp();

    $respuestaDB = $garantia->guardar($logsDB);

    $respuesta['respuesta'] = $respuestaDB->respuesta;

    $conexion = null;

} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);
