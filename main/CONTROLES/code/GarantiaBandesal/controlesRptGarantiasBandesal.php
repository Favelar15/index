<?php
include "../../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

$respuesta = [];

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {

    $idAgencias = [];

    $nombreArchivo = 'rpt-garatiasBandesal-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';



    if ( $input['cboAgencia']['value'] == 'all')
    {
        foreach ( $_SESSION['index']->agencias as $agencia )
        {
            array_push( $idAgencias, (int) $agencia->id);
        }
    } else
    {
        $nombreArchivo = 'rpt-garatiasBandesal-'.$input['cboAgencia']['value'].'-'.$input['txtInicio']['value'].'-'.$input['txtFin']['value'].'.xlsx';
        array_push( $idAgencias, (int) $input['cboAgencia']['value']);
    }

    if ( $input['accion'] == 'EXPORTAR')
    {
        $reporte = new ExportarGarantias();
        $reporte->agencias = $idAgencias;
        $reporte->nombreArchivo = $nombreArchivo;
        $reporte->garantiasBandesal = $input['garantiasBandesal'];
        $respuesta = $reporte->crear();

    } else
    {
        require_once '../../../code/connectionSqlServer.php';
        require_once '../Models/Permiso.php';
        require_once 'Models/GarantiaBandesal.php';

        $desde = date('Y-m-d', strtotime( $input['txtInicio']['value'] ));
        $hasta =  date('Y-m-d', strtotime( $input['txtFin']['value'] ));

        $garantiaBandesal = new GarantiaBandesal();
        $garantiaBandesal->setAgencias($idAgencias);
        $respuesta['garantiasBandesal'] = $garantiaBandesal->rptGarantiasBandesal( $desde, $hasta );
    }
}


echo json_encode( $respuesta );


class ExportarGarantias
{
    public $nombreArchivo;
    public $agencias = [];
    public $garantiasBandesal = [];


    function crear()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', "Reporte de garantías bandesal por agencias (". implode(',', $this->agencias ).")");
        $spreadsheet->getActiveSheet()->mergeCells("A1:AA1");
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

        $spreadsheet->getActiveSheet()->getStyle('A1:AA1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:AA1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');


        $sheet->setCellValue('A2', 'N°');
        $sheet->setCellValue('B2', 'Número de reserva');
        $sheet->setCellValue('C2', 'NIT');
        $sheet->setCellValue('D2', 'Nombre de cliente');
        $sheet->setCellValue('E2', 'Agencia');
        $sheet->setCellValue('F2', 'Dirección del cliente');
        $sheet->setCellValue('G2', 'Código de departamento');
        $sheet->setCellValue('H2', 'Código de municipio');
        $sheet->setCellValue('I2', 'Referencia préstamo');
        $sheet->setCellValue('J2', 'Monto del préstamo');
        $sheet->setCellValue('K2', 'Porcentaje a garantizar');
        $sheet->setCellValue('L2', 'Monto a garantizar');
        $sheet->setCellValue('M2', 'Fecha de contratación');
        $sheet->setCellValue('N2', 'Fecha de vencimiento');
        $sheet->setCellValue('O2', 'Dirección del proyecto');
        $sheet->setCellValue('P2', 'Departamento del proyecto');
        $sheet->setCellValue('Q2', 'Municipio del proyecto');
        $sheet->setCellValue('R2', 'Años de experiencia');
        $sheet->setCellValue('S2', 'Código tipo de garantía');
        $sheet->setCellValue('T2', 'Monto de la garantía adicional');
        $sheet->setCellValue('U2', 'Clasificación de riesgo');
        $sheet->setCellValue('V2', 'Dias de mora');
        $sheet->setCellValue('W2', 'Tipo de prestamo');
        $sheet->setCellValue('X2', 'Fecha vencimiento Linea Rotativa');
        $sheet->setCellValue('Y2', 'Asesor');
        $sheet->setCellValue('Z2', 'Usuario');
        $sheet->setCellValue('AA2', 'Fecha de registro');

        $spreadsheet->getActiveSheet()->getStyle('A2:AA2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A2:AA2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $contador = 2;

        foreach ( $this->garantiasBandesal as $garantia )
        {
            $contador++;
            $sheet->setCellValue('A'.$contador, ( $contador - 1 ) );
            $sheet->setCellValue('B'.$contador, $garantia['comprobante']);
            $sheet->setCellValue('C'.$contador, $garantia['nit']);
            $sheet->setCellValue('D'.$contador, $garantia['nombreAsociados']);
            $sheet->setCellValue('E'.$contador, $garantia['idAgencia']);
            $sheet->setCellValue('F'.$contador, $garantia['direccionCompleta']);
            $sheet->setCellValue('G'.$contador, str_pad( $garantia['codigoDepartamento'], 2, '0', STR_PAD_LEFT ) );
            $sheet->setCellValue('H'.$contador, str_pad( $garantia['codigoMunicipio'], 2, '0', STR_PAD_LEFT));
            $sheet->setCellValue('I'.$contador, $garantia['numeroPrestamo']);
            $sheet->setCellValue('J'.$contador, "$".number_format($garantia['monto'], 2, '.', ','));
            $sheet->setCellValue('K'.$contador, $garantia['porcentajeGarantizado']);
            $sheet->setCellValue('L'.$contador, "$".number_format($garantia['montoGarantizado'], 2, '.', ','));
            $sheet->setCellValue('M'.$contador, $garantia['fechaDesembolso']);
            $sheet->setCellValue('N'.$contador, $garantia['fechaVencimiento']);
            $sheet->setCellValue('O'.$contador, $garantia['proyectos'][0]['direccion']);
            $sheet->setCellValue('P'.$contador, str_pad($garantia['proyectos'][0]['codigoDepartamento'], 2, '0', STR_PAD_LEFT) );
            $sheet->setCellValue('Q'.$contador, str_pad($garantia['proyectos'][0]['codigoMunicipio'], 2, '0', STR_PAD_LEFT));
            $sheet->setCellValue('R'.$contador, $garantia['añosDeExperiencia']);
            $sheet->setCellValue('S'.$contador, $garantia['codigo']);
            $sheet->setCellValue('T'.$contador, "$". number_format($garantia['montoAdicional'], 2, '.', ','));
            $sheet->setCellValue('U'.$contador, $garantia['clasificacionRiesgo']);
            $sheet->setCellValue('V'.$contador, $garantia['diasAtrasados']);
            $sheet->setCellValue('W'.$contador, $garantia['tipoPrestamo']);
            $sheet->setCellValue('X'.$contador, $garantia['vencimientoLineaRotativa']);
            $sheet->setCellValue('Y'.$contador, $garantia['asesor']);
            $sheet->setCellValue('Z'.$contador, $garantia['usuario']);
            $sheet->setCellValue('AA'.$contador, $garantia['fechaRegistro']);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '/../../docs/garantiasBandesal/Excel/'.$this->nombreArchivo );
        return (Object)[
            'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];

    }

}