<?php

class GarantiaBandesal extends Permiso
{
    public $id, $codigoCliente, $comprobante, $numeroPrestamo, $monto, $porcentajeGarantizado, $montoGarantizado, $montoAdicional,
        $fechaDesembolso, $fechaVencimiento, $porcentajeDescontado, $valorDescontado, $añosDeExperiencia, $idTipoGarantia, $idPrograma, $idLineaPrestamo,
        $tipoPrestamo, $vencimientoLineaRotativa, $tipoPrograma, $diasAtrasados, $clasificacionRiesgo,
        $idAgencia, $idAsesor, $idUsuario, $ipOrdenador, $protectos;
    private $conexion;

    private $agenciasAsignadas = [];

    function __construct()
    {
        parent::__construct();

        global $conexion;
        $this->conexion = $conexion;

        !empty($this->monto) && $this->monto = sprintf('%0.2f', $this->monto);
        !empty($this->montoGarantizado) && $this->montoGarantizado = sprintf('%0.2f', $this->montoGarantizado);
        !empty($this->montoAdicional) && $this->montoAdicional = sprintf('%0.2f', $this->montoAdicional);
        !empty($this->valorDescontado) && $this->valorDescontado = sprintf('%0.2f', $this->valorDescontado);
        !empty($this->fechaDesembolso) && $this->fechaDesembolso = date('d-m-Y', strtotime( $this->fechaDesembolso ));
        !empty($this->fechaVencimiento) && $this->fechaVencimiento = date('d-m-Y', strtotime( $this->fechaVencimiento ));
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime( $this->fechaRegistro ));
        !empty($this->vencimientoLineaRotativa) && $this->vencimientoLineaRotativa = date('d-m-Y', strtotime( $this->vencimientoLineaRotativa ));
    }

    function eliminar()
    {

        $respuesta = null;

        $query = 'EXEC controles_spEliminarGarantiaBandesal :idApartado, :tipoApartado, :idGarantia, :idUsuario, :ipOrdenador, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoPermiso, PDO::PARAM_STR);
        $result->bindParam(':idGarantia', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        return $respuesta;
    }

    function guardar( $logs )
    {
        $query = 'EXEC controles_spGuardarGarantiaBandesal :idApartado, :tipoApartado, :id, :codigoCliente, :comprobante, 
        :numeroPrestamo, :monto, :porcentajeGarantizado, :montoGarantizado, :montoAdicional, :fechaDesembolso, :fechaVencimiento, 
                        :porcentajeDescontado, :valorDescontado, :experiencia, :idTipoGarantia, :idPrograma, 
                        :idLineaPrestamo, :tipoPrestamo, :vencimientoLineaRotativa,:tipoPrograma, :diasAtrasados, :clasificacionRiesgo, :idAgencia, :idAsesor, 
                        :idUsuario, :ipOrdenador, :proyectos, :logs, :respuesta';

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoPermiso, PDO::PARAM_STR);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':codigoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':comprobante', $this->comprobante, PDO::PARAM_INT);
        $result->bindParam(':numeroPrestamo', $this->numeroPrestamo, PDO::PARAM_STR);
        $result->bindParam(':monto', $this->monto, PDO::PARAM_INT);
        $result->bindParam(':porcentajeGarantizado', $this->porcentajeGarantizado, PDO::PARAM_INT);
        $result->bindParam(':montoGarantizado', $this->montoGarantizado, PDO::PARAM_INT);
        $result->bindParam(':montoAdicional', $this->montoAdicional, PDO::PARAM_INT);
        $result->bindParam(':fechaDesembolso', $this->fechaDesembolso, PDO::PARAM_STR);
        $result->bindParam(':fechaVencimiento', $this->fechaVencimiento, PDO::PARAM_STR);
        $result->bindParam(':porcentajeDescontado', $this->porcentajeGarantizado, PDO::PARAM_INT);
        $result->bindParam(':experiencia', $this->añosDeExperiencia, PDO::PARAM_INT);
        $result->bindParam(':valorDescontado', $this->valorDescontado, PDO::PARAM_INT);
        $result->bindParam(':idTipoGarantia', $this->idTipoGarantia, PDO::PARAM_STR);
        $result->bindParam(':idPrograma', $this->idPrograma, PDO::PARAM_INT);
        $result->bindParam(':idLineaPrestamo', $this->idLineaPrestamo, PDO::PARAM_INT);
        $result->bindParam(':tipoPrestamo', $this->tipoPrestamo, PDO::PARAM_STR);
        $result->bindParam(':vencimientoLineaRotativa', $this->vencimientoLineaRotativa, PDO::PARAM_STR);
        $result->bindParam(':tipoPrograma', $this->tipoPrograma, PDO::PARAM_STR);
        $result->bindParam(':diasAtrasados', $this->diasAtrasados, PDO::PARAM_INT);
        $result->bindParam(':clasificacionRiesgo', $this->clasificacionRiesgo, PDO::PARAM_STR);
        $result->bindParam(':idAgencia', $this->idAgencia, PDO::PARAM_INT);
        $result->bindParam(':idAsesor', $this->idAsesor, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':proyectos', $this->protectos, PDO::PARAM_STR);
        $result->bindParam(':logs', $logs, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        $result->execute();

        if ($respuesta == 'EXITO') {
            return (object)['status' => true, 'respuesta' => $respuesta];
        }

        return (object)['status' => false, 'respuesta' => $respuesta];
    }

    function obtenerGarantiasBandesal($roles)
    {
        $this->isAdministrativo( $roles );

        if (!$this->administrativo) {
            $query = "SELECT * FROM controles_viewDatosGarantiasBandesal WHERE idUsuario = " . $this->idUsuario . " AND idAgencia IN (" .implode(',', $this->getIdAgencias()  ). ") AND fechaDesembolso > DATEADD (MONTH, -1,  CAST ( GETDATE() AS DATE )) ORDER BY fechaRegistro DESC";
        } else {
            $query = "SELECT * FROM controles_viewDatosGarantiasBandesal WHERE idAgencia IN (".implode(',', $this->getIdAgencias()  ).") AND fechaDesembolso > DATEADD (MONTH, -1,  CAST ( GETDATE() AS DATE )) ORDER BY fechaRegistro DESC";
        }

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }

    function obtenerGarantiaBandesal()
    {
        $query = "SELECT * FROM controles_viewObtenerDatosGarantia WHERE id = :id";

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 ) {
            $garantia =  $result->fetchAll(PDO::FETCH_CLASS, __CLASS__)[0];
            $garantia->proyectos = $this->obtenerProyectos();
            $this->conexion = null;
            return $garantia;
        }
        $this->conexion = null;
        return [];
    }

    public function setIdApartado($idApartado)
    {
        $this->idApartado = $idApartado;
    }

    public function setTipoApartado($tipoApartado)
    {
        $this->tipoPermiso = $tipoApartado;
    }

    function setAgencias($agencias)
    {
        $this->agenciasAsignadas = $agencias;
    }

    private function getIdAgencias()
    {
        $idsAgencias = [];

        foreach ($this->agenciasAsignadas as $agencia) {
            array_push($idsAgencias, (int)$agencia->id);
        }
        return $idsAgencias;
    }

    private function obtenerProyectos()
    {
        $query = "SELECT * FROM controles_viewDetProyectos WHERE idGarantia = :idGarantia";

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idGarantia', $this->id, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 ) {
            return $result->fetchAll(PDO::FETCH_OBJ);
        }
        return [];
    }

    function rptGarantiasBandesal ( $desde, $hasta )
    {
        $query = "SELECT * FROM controles_viewRptGarantiasBandesal WHERE idAgencia IN (".implode(',', $this->agenciasAsignadas)." ) AND CAST(fechaRegistro AS DATE) BETWEEN '$desde' AND '$hasta'";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ($result->rowCount() > 0) {
            $garantiasBandesal =  $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);

            foreach ( $garantiasBandesal as $garantia )
            {
                $this->id = $garantia->id;
                $garantia->proyectos = $this->obtenerProyectos();
            }
            return $garantiasBandesal;
        }
        $this->conexion = null;
        return [];
    }

}