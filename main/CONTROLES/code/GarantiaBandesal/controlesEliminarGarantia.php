<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked) {
    require_once '../../../code/connectionSqlServer.php';
    require_once '../Models/Permiso.php';
    require_once 'Models/GarantiaBandesal.php';

    $garantia = new GarantiaBandesal();
    $garantia->setIdApartado( base64_decode(urldecode($input['idApartado'])) );
    $garantia->setTipoApartado( $input['tipoApartado'] );
    $garantia->id = $input['id'];
    $garantia->idUsuario = $_SESSION['index']->id;
    $garantia->ipOrdenador = generalObtenerIp();

    $respuesta['respuesta'] = $garantia->eliminar();

} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode($respuesta);