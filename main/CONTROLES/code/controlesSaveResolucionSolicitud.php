<?php
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../code/generalParameters.php";

session_start();

$respuesta = (object)[];

if (isset($_SESSION['index']) && $_SESSION["index"]->locked) {
    if (count($input) > 0) {
        include "../../code/connectionSqlServer.php";
        require_once '../../code/Models/asociado.php';
        require_once './RetiroAsociados/retiroAsociado.php';

        $idUsuario = $_SESSION['index']->id;
        $idSolicitud = $input['id'];
        $idResolucion = $input['idResolucion'];
        $observaciones = $input['observaciones'];
        $tipoApartado = $input["tipoApartado"];
        $fechaResolucion = date('Y-m-d', strtotime( $input["fechaResolucion"] ));
        $idApartado = base64_decode(urldecode($input["idApartado"]));

        $solicitud = new solicitudRetiro();
        $solicitud->id = $idSolicitud;

        $resolucion = $solicitud->setResolucionSolicitud($idUsuario, $idResolucion, $observaciones, $tipoApartado, $idApartado, $ipActual, $fechaResolucion);

        $respuesta->{"respuesta"} = $resolucion;

        $conexion = null;
    }
} else {
    $respuesta->{'respuesta'} = 'SESION';
}

echo json_encode($respuesta);
