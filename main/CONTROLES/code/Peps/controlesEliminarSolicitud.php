<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();

$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    include "../../../code/connectionSqlServer.php";
    require_once "./Models/SolicitudPep.php";

    $solicitud = new SolicitudPep();
    $solicitud->id = $input['id'];
    $solicitud->setIdApartado((int)base64_decode(urldecode($input['idApartado'])));
    $solicitud->setTipoApartado($input['tipoApartado']);
    $solicitud->idUsuario = $_SESSION['index']->id;
    $solicitud->ipOrdenador = generalObtenerIp();
    $respuesta = $solicitud->eliminarSolicitud();

} else {
    $respuesta = 'SESION';
}

echo json_encode($respuesta);
