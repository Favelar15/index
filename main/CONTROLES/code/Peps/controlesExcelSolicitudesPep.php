<?php
include "../../../code/generalParameters.php";
header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);

$respuesta = (object)[];

require_once 'C:\xampp\htdocs\vendor\autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

session_start();

if (isset($_SESSION["index"]) && $_SESSION["index"]->locked)
{
    $reporte = new ReporteExcel();
    $desde = $input['txtInicio']['value'];
    $hasta = $input['txtFin']['value'];
    $reporte->solicitudes = $input['solicitudes'];

    $respuesta = $input['accion'] == 'PEP' ? $reporte->exportarPep( $desde, $hasta) : $reporte->exportarRelacionados($desde, $hasta);

} else {
    $respuesta->{"respuesta"} = "SESION";
}

echo json_encode($respuesta);

class ReporteExcel {

    private $nombreArchivo;
    public $solicitudes;

    function exportarPep( $desde, $hasta )
    {
        $this->nombreArchivo = 'rpt-solicitudesPep-'.$desde.'-'.$hasta.'.xlsx';

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'N°');
        $sheet->setCellValue('B1', 'Fecha de registro');
        $sheet->setCellValue('C1', 'Número de cliente');
        $sheet->setCellValue('D1', 'Tipo de solicitud');
        $sheet->setCellValue('E1', 'Agencia');
        $sheet->setCellValue('F1', 'Nombre PEP');
        $sheet->setCellValue('G1', 'Documento');
        $sheet->setCellValue('H1', 'Institución donde labora');
        $sheet->setCellValue('I1', 'Cargo');
        $sheet->setCellValue('J1', 'Del');
        $sheet->setCellValue('K1', 'AL');
        $sheet->setCellValue('L1', 'Actividades economicas');
        $sheet->setCellValue('M1', 'Sociedades con las tiene relación');
        $sheet->setCellValue('N1', 'Estado de la solicitud');

        $spreadsheet->getActiveSheet()->getStyle('A1:N1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:N1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $contador = 1;

        foreach ( $this->solicitudes as $solicitud )
        {
            $contador++;
            $sheet->setCellValue("A$contador", ($contador - 1) );
            $sheet->setCellValue("B$contador", $solicitud['fechaRegistro'] );
            $sheet->setCellValue("C$contador", $solicitud['codigoCliente'] );
            $sheet->setCellValue("D$contador", $solicitud['tipoSolicitud'] );
            $sheet->setCellValue("E$contador", $solicitud['agencia'] );
            $sheet->setCellValue("F$contador", $solicitud['nombres'] );
            $sheet->setCellValue("G$contador", $solicitud['numeroDocumentoPrimario'] );
            $sheet->setCellValue("H$contador", $solicitud['organismo'] );
            $sheet->setCellValue("I$contador", $solicitud['cargo'] );
            $sheet->setCellValue("J$contador", $solicitud['inicioGestion'] );
            $sheet->setCellValue("K$contador", $solicitud['finGestion'] );
            $sheet->setCellValue("L$contador", $solicitud['activadesEconomicas'] );
            $sheet->setCellValue("M$contador", $solicitud['sociedades'] );
            $sheet->setCellValue("N$contador", $solicitud['resolucion'] );
        }


        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '../../../docs/solicitudesPeps/Excel/'.$this->nombreArchivo);
        return (Object)[
          'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];
    }

    function exportarRelacionados($desde, $hasta)
    {
        $this->nombreArchivo = 'rpt-solicitudesRelacionadoPep-'.$desde.'-'.$hasta.'.xlsx';

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'N°');
        $sheet->setCellValue('B1', 'Fecha de registro');
        $sheet->setCellValue('C1', 'Número de cliente');
        $sheet->setCellValue('D1', 'Tipo de solicitud');
        $sheet->setCellValue('E1', 'Agencia');
        $sheet->setCellValue('F1', 'Nombre (Persona relacionada  a PEP)');
        $sheet->setCellValue('G1', 'Documento');
        $sheet->setCellValue('H1', 'Actividades economicas');
        $sheet->setCellValue('I1', 'Parentesco');
        $sheet->setCellValue('J1', 'Nombre del PEP (Relacionado)');
        $sheet->setCellValue('K1', 'Cargo');
        $sheet->setCellValue('L1', 'Periodo');
        $sheet->setCellValue('M1', 'Estado de la solcitud');

        $spreadsheet->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('005f55');

        $contador = 1;

        foreach ( $this->solicitudes as $solicitud )
        {
            $contador++;
            $sheet->setCellValue("A$contador", ($contador - 1) );
            $sheet->setCellValue("B$contador", $solicitud['fechaRegistro'] );
            $sheet->setCellValue("C$contador", $solicitud['codigoCliente'] );
            $sheet->setCellValue("D$contador", $solicitud['tipoSolicitud'] );
            $sheet->setCellValue("E$contador", $solicitud['agencia'] );
            $sheet->setCellValue("F$contador", $solicitud['nombres'] );
            $sheet->setCellValue("G$contador", $solicitud['numeroDocumentoPrimario'] );
            $sheet->setCellValue("H$contador", $solicitud['activadesEconomicas'] );
            $sheet->setCellValue("I$contador", $solicitud['parantescoPep'] );
            $sheet->setCellValue("J$contador", $solicitud['nombrePep'] );
            $sheet->setCellValue("K$contador", $solicitud['cargosPep'] );
            $sheet->setCellValue("L$contador", $solicitud['gestionesPep'] );
            $sheet->setCellValue("M$contador", $solicitud['resolucion'] );
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__ . '../../../docs/solicitudesPeps/Excel/'.$this->nombreArchivo);
        return (Object)[
            'respuesta' => 'EXITO', 'nombreArchivo' => $this->nombreArchivo
        ];
    }

}