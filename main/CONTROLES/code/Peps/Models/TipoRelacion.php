<?php

class TipoRelacion
{
    public $id, $tipoRelacion;
    private $conexion;

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    function obtenerTipoRelacion()
    {
        $query = "SELECT * FROM controles_viewTipoRelacionPep";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }
        $this->conexion = null;
        return [];
    }
}