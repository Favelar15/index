<?php

require_once 'Permiso.php';

class SolicitudPep extends Permiso
{
    public $id, $idTipoSolicitud, $idTipoAfiliacion;
    public $codigoCliente, $nombres, $apellidos, $idTipoDocumentoPrimario, $numeroDocumentoPrimario, $fechaExpedicion,
        $lugarExpedicion, $telefono, $email, $fechaNacimiento, $lugarDeNacimiento, $idEstadoCivil, $idPais, $idDepartamento,
        $idMunicipio, $direccion, $profesion, $idActividadEconomicaPrimaria, $idActividadEconomicaSecundaria, $lugarDeTrabajo,
        $otraNacionalidad, $fondosPoliticos, $cuentaDeTerceros, $remuneracionMensual, $detalleRemuneracion, $otrosIngresos,
        $detalleOtrosIngresos, $idEstado, $solicitudAprobada, $nombreArchivo, $idUsuario, $idAgencia, $ipOrdenador;
    public $nacionalidad;
    public $residencias, $personasRelacionadas, $sociedades, $otrasCuentas, $productos;

    public $Antecedente;

    // PROPIEDAD QUE ALMACENA LOS DATOS DEL REPRESENTANTE
    public $representante;

    // NUEVAS PROPIEDADES
    public $fechaRegistro, $propietario;

    public $fechaResolucion;

    private $agenciasAsignadas = [];
    private $path;

    public function __construct()
    {
        parent::__construct();

        global $conexion;
        $this->conexion = $conexion;

        $this->path =  __DIR__ . '\..\..\..\docs\solicitudesPeps\/';

        if ( !empty( $this->idUsuario ))
        {
            $this->propietario = $this->idUsuario == $_SESSION['index']->id ? 'S': 'N';
        }

        !empty($this->otrosIngresos) && $this->otrosIngresos = sprintf('%0.2f', $this->otrosIngresos);
        !empty($this->remuneracionMensual) && $this->remuneracionMensual = sprintf('%0.2f', $this->remuneracionMensual);
        //!empty($this->otrosIngresos) && $this->otrosIngresos = number_format( $this->otrosIngresos, 2);
        //!empty($this->remuneracionMensual) && $this->remuneracionMensual = number_format( $this->remuneracionMensual, 2);
        !empty($this->fechaResolucion) && $this->fechaResolucion = date('d-m-Y', strtotime($this->fechaResolucion));
        !empty($this->fechaNacimiento) && $this->fechaNacimiento = date('d-m-Y', strtotime($this->fechaNacimiento));
        !empty($this->fechaExpedicion) && $this->fechaExpedicion = date('d-m-Y', strtotime($this->fechaExpedicion));
        !empty($this->fechaRegistro) && $this->fechaRegistro = date('d-m-Y h:i a', strtotime($this->fechaRegistro));
    }

    public function obtenerDatosSolicitudPep ( $roles )
    {
        $values = str_repeat('?,',count( $this->getIdAgencias() ) - 1 ).'?';

        $this->isAdministrativo( $roles );

        if ( !$this->administrativo )
        {
            // echo 'asdhkjhasdkjhasd';
            $query = "SELECT * FROM controles_viewListadoSolicitudesPeps WHERE estado = 'PENDIENTE' AND idUsuario =". $this->idUsuario ." AND idAgencia IN ($values) ORDER BY fechaRegistro DESC";
        } else
        {
            $query = "SELECT * FROM controles_viewListadoSolicitudesPeps WHERE estado = 'PENDIENTE' AND idAgencia IN ($values) ORDER BY fechaRegistro DESC";
        }

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute( $this->getIdAgencias() );

        if ( $result->rowCount() > 0 ) {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        return [];
    }

    public function obtenerSolicitudesAprobadas ( $roles, $desde )
    {
        $values = str_repeat('?,',count( $this->getIdAgencias()) - 1 ).'?';
        $this->isAdministrativo( $roles );

        if ( !$this->administrativo )
        {
            $query = "SELECT * FROM controles_viewListadoSolicitudesPeps WHERE estado = 'RESUELTA' AND (fechaResolucion IS NULL OR cast( GETDATE() AS DATE ) <= DATEADD(MONTH,1, CAST(fechaResolucion AS DATE)) ) AND  idUsuario =". $this->idUsuario ." AND idAgencia IN ($values) ORDER BY fechaRegistro DESC";
        } else
        {
            $query = "SELECT * FROM controles_viewListadoSolicitudesPeps WHERE estado = 'RESUELTA' AND (fechaResolucion IS NULL OR cast( GETDATE() AS DATE ) <= DATEADD(MONTH,1, CAST(fechaResolucion AS DATE)) ) AND  idAgencia IN ($values) ORDER BY fechaRegistro DESC";
        }

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute( $this->getIdAgencias() );

        if ( $result->rowCount() > 0 ) {
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

    function guardarSolicitud ( $idApartado, $tipoApartado )
    {
        $idDb = 0;
        $respuesta = '';

        $temporal = $this->Antecedente->formatDb();

        //return (Object)[ 'RRELACIONADOS' => $this->personasRelacionadas ];

        //return (Object)[ 'NACIONALIDAD' => $this->nacionalidad ];
        //return (Object)[ 'AntecedentesPep' => $temporal ];
        //return (Object)[ 'Representante' => $this->representante ];

        $query = 'EXEC controles_spGuardarSolicitudPep :id, :idApartado, :tipoApartado, :idTipoSolicitud, :idTipoAfiliacion, :codidoCliente, :nombres, :apellidos, :idTipoDocumentoPrimario, :numeroDocumentoPrimario, :fechaExpedicion, :lugarExpedicion, :telefono, :email, :fechaNacimiento, :lugarNacimiento, :idEstadoCivil, :nacionalidad, :idPais, :idDepartamento, :idMunicipio, :direccion, :profesion, :idActividadEconomicaPrincipal, :idActividadEconomicaSecundaria, :lugarDeTrabajo, :otraNacionalidad, :fondosPoliticos, :cuentasDeTerceros, :remuneracionMensual, :detalleRemuneracion, :otrosIngresos, :detalleOtrosIngresos, :idUsuario, :idAgencia, :ipOrdenador, :residencias,:antecedentes, :personasRelacionadas, :sociedades, :otrasCuentas, :productos, :representante, :idSolicitud, :nombreArchivo, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idApartado', $idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $tipoApartado, PDO::PARAM_STR);
        $result->bindParam(':idTipoSolicitud', $this->idTipoSolicitud, PDO::PARAM_INT);
        $result->bindParam(':idTipoAfiliacion', $this->idTipoAfiliacion, PDO::PARAM_INT);
        $result->bindParam(':codidoCliente', $this->codigoCliente, PDO::PARAM_INT);
        $result->bindParam(':nombres', $this->nombres, PDO::PARAM_STR);
        $result->bindParam(':apellidos', $this->apellidos, PDO::PARAM_STR);
        $result->bindParam(':idTipoDocumentoPrimario', $this->idTipoDocumentoPrimario, PDO::PARAM_INT);
        $result->bindParam(':numeroDocumentoPrimario', $this->numeroDocumentoPrimario, PDO::PARAM_STR);
        $result->bindParam(':fechaExpedicion', $this->fechaExpedicion, PDO::PARAM_STR);
        $result->bindParam(':lugarExpedicion', $this->lugarExpedicion, PDO::PARAM_STR);
        $result->bindParam(':telefono', $this->telefono, PDO::PARAM_STR);
        $result->bindParam(':email', $this->email, PDO::PARAM_STR);
        $result->bindParam(':fechaNacimiento', $this->fechaNacimiento, PDO::PARAM_STR);
        $result->bindParam(':lugarNacimiento', $this->lugarDeNacimiento, PDO::PARAM_STR);
        $result->bindParam(':idEstadoCivil', $this->idEstadoCivil, PDO::PARAM_INT);
        $result->bindParam(':nacionalidad', $this->nacionalidad, PDO::PARAM_INT);
        $result->bindParam(':idPais', $this->idPais, PDO::PARAM_INT);
        $result->bindParam(':idDepartamento', $this->idDepartamento, PDO::PARAM_INT);
        $result->bindParam(':idMunicipio', $this->idMunicipio, PDO::PARAM_INT);
        $result->bindParam(':direccion', $this->direccion, PDO::PARAM_STR);
        $result->bindParam(':profesion', $this->profesion, PDO::PARAM_STR);
        $result->bindParam(':idActividadEconomicaPrincipal', $this->idActividadEconomicaPrimaria, PDO::PARAM_INT);
        $result->bindParam(':idActividadEconomicaSecundaria', $this->idActividadEconomicaSecundaria, PDO::PARAM_INT);
        $result->bindParam(':lugarDeTrabajo', $this->lugarDeTrabajo, PDO::PARAM_STR);
        $result->bindParam(':otraNacionalidad', $this->otraNacionalidad, PDO::PARAM_STR);
        $result->bindParam(':fondosPoliticos', $this->fondosPoliticos, PDO::PARAM_STR);
        $result->bindParam(':cuentasDeTerceros', $this->cuentaDeTerceros, PDO::PARAM_STR);
        $result->bindParam(':remuneracionMensual', $this->remuneracionMensual, PDO::PARAM_INT);
        $result->bindParam(':detalleRemuneracion', $this->detalleRemuneracion, PDO::PARAM_STR);
        $result->bindParam(':otrosIngresos', $this->otrosIngresos, PDO::PARAM_INT);
        $result->bindParam(':detalleOtrosIngresos', $this->detalleOtrosIngresos, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':idAgencia', $this->idAgencia, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':residencias', $this->residencias, PDO::PARAM_STR);
        $result->bindParam(':antecedentes', $temporal, PDO::PARAM_STR);
        $result->bindParam(':personasRelacionadas', $this->personasRelacionadas, PDO::PARAM_STR);
        $result->bindParam(':sociedades', $this->sociedades, PDO::PARAM_STR);
        $result->bindParam(':otrasCuentas', $this->otrasCuentas, PDO::PARAM_STR);
        $result->bindParam(':productos', $this->productos, PDO::PARAM_STR);
        $result->bindParam(':representante', $this->representante, PDO::PARAM_STR);
        $result->bindParam(':idSolicitud', $idDb, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':nombreArchivo', $this->nombreArchivo, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);

        //return (Object)[ $this->residencias, $this->personasRelacionadas, $this->sociedades, $this->otrasCuentas, $this->productos];

        //return (Object)[ $temporal];

        $result->execute();

        if ( $respuesta == 'EXITO')
        {
            return (object)['status' => true,  'idSolicitud' => $idDb, 'respuesta' => $respuesta, 'nombreArchivo' => $this->nombreArchivo ];
        } else
        {
            return (object)['status' => false, 'idSolicitud' => $idDb,  'respuesta' => $respuesta, 'nombreArchivo' => null ];
        }
    }

    function setAgencias( $agencias )
    {
        $this->agenciasAsignadas = $agencias;
    }

    private function getIdAgencias ()
    {
        $idsAgencias = [];

        foreach ( $this->agenciasAsignadas as $agencia )
        {
            array_push( $idsAgencias, (int) $agencia->id  );
        }
        return $idsAgencias;
    }

    public function setIdApartado ($idApartado)
    {
        $this->idApartado = $idApartado;
    }

    public function setTipoApartado ($tipoApartado)
    {
        $this->tipoPermiso = $tipoApartado;
    }

    public function obtenerDatosSolicitud ( )
    {
        $solicitud = $this->obtenerSolicitud();

        if ( $solicitud->otraNacionalidad == 'S' )
        {
            $residencia = new Residencia();
            $residencia->idSolicitud = $this->id;
            $solicitud->residencias = $residencia->obtenerNacionalidades();
        }

        if ( (int) $solicitud->idTipoAfiliacion == 1 )
        {
            $antecedentes = new AntecedentePep();
            $antecedentes->idSolicitud = $this->id;
            $solicitud->Antecedente = $antecedentes->obtenerAntecedentes();
        }

        if ( $solicitud->cuentaDeTerceros == 'S' )
        {
            $cuenta = new OtrasCuentas();
            $cuenta->idSolicitud = $this->id;
            $solicitud->otrasCuentas = $cuenta->obtenerOtrasCuentas();
        }

        $relacionado = new PersonasRelacionadas();
        $relacionado->idSolicitud = $this->id;

        $sociedad = new Sociedades();
        $sociedad->idSolicitud = $this->id;

        $productoContratado = new ProductosContratados();
        $productoContratado->idSolicitud = $this->id;

        $representante = new Representante();
        $representante->idSolicitud = $this->id;
        $solicitud->representante = $representante->obtenerRepresentante();

        $solicitud->cboRepresentante = !empty( (array) $solicitud->representante ) ? 'S' :  'N';

        $solicitud->personasRelacionadas = $relacionado->obtenerRelacionados();
        $solicitud->sociedades = $sociedad->obtenerSociedades();
        $solicitud->productos = $productoContratado->obtenerProductos();

        return $solicitud;
    }

    private function obtenerSolicitud ()
    {
        $query = "SELECT * FROM controles_viewDatosSolicitudPep WHERE id = :id ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__)[0];
        }

        $this->conexion = null;
        return (object)[];
    }

    public function guardarResolucion ( $idArea, $idResolucion, $fechaResolucion, $observacion )
    {
        $respuesta = null;

        $query = 'EXEC controles_spGuardarResolucionPep :idApartado, :tipoApartado, :idSolicitud, :idArea, :idResolucion, :fechaResolucion, :observacion, :idUsuario, :ipOrdenador, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoPermiso, PDO::PARAM_STR);
        $result->bindParam(':idSolicitud', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idArea', $idArea, PDO::PARAM_INT);
        $result->bindParam(':idResolucion', $idResolucion, PDO::PARAM_INT);
        $result->bindParam(':fechaResolucion', $fechaResolucion, PDO::PARAM_STR);
        $result->bindParam(':observacion', $observacion, PDO::PARAM_STR);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();


        if ( $respuesta == 'EXITO')
        {
            return (object)['status' => true, 'respuesta' => $respuesta];
        } else
        {
            return (object)['status' => false, 'respuesta' => $respuesta];
        }
    }

    public function eliminarSolicitud()
    {
        $respuesta = null;

        $query = 'EXEC controles_spEliminarSolicitudPep :id, :idApartado, :tipoApartado, :idUsuario, :ipOrdenador, :respuesta';
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idApartado', $this->idApartado, PDO::PARAM_INT);
        $result->bindParam(':tipoApartado', $this->tipoPermiso, PDO::PARAM_STR);
        $result->bindParam(':id', $this->id, PDO::PARAM_INT);
        $result->bindParam(':idUsuario', $this->idUsuario, PDO::PARAM_INT);
        $result->bindParam(':ipOrdenador', $this->ipOrdenador, PDO::PARAM_STR);
        $result->bindParam(':respuesta', $respuesta, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $result->execute();

        if ( $respuesta == 'EXITO')
        {
            return (object)['status' => true, 'respuesta' => $respuesta];
        } else
        {
            return (object)['status' => false, 'respuesta' => $respuesta];
        }
    }

    public function generarPdf()
    {
        $solicitud = $this->obtenerDatosSolicitud();

        //return $solicitud;

//        if ( file_exists($this->path . $solicitud->nombreArchivo))
//        {
//            return ["respuesta" => "EXITO", "nombreArchivo" => $solicitud->nombreArchivo];
//        }

        $producto = new ProductoServicio();
        $productosAll = $producto->obtenerProductosServicios();

        $pdf = new solicitudPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'UTF-8', true);

        $pdf->setSolicitud( $solicitud );
        $pdf->setProductosAll( $productosAll );

        $pdf->configuracionDocumento();
        $pdf->setMargin();

        $pdf->SetAutoPageBreak(TRUE, 35);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();

        $pdf->setFont('dejavusans', 'B', 10);
        $pdf->Write(0, 'SOLICITUD DE AFILIACIÓN PARA PERSONAS EXPUESTAS POLÍTICAMENTE (PEP) O RELACIONADAS A PEP', '', 0, 'C', true, 0, false, false, 0);
        $pdf->writeHTML("<hr>", true, false, false, false, '');
        $pdf->ln(3);

        $pdf->generalidades();
        $pdf->datosPersonales();
        $pdf->Antecedentes();

        $pdf->SetAutoPageBreak(TRUE, 35);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();

        $pdf->personaRelacionada();
        $pdf->sociedades();
        $pdf->cuentasTercero();
        $pdf->fuenteDeIngreso();
        $pdf->productosContratados();

        $pdf->SetAutoPageBreak(TRUE, 35);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();

        $pdf->representante();
        $pdf->manifiesto();
        $pdf->firmaRepresentante();

        $pdf->SetAutoPageBreak(TRUE, 35);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();

        $pdf->resolución();


        $pdf->Output( $this->path . $solicitud->nombreArchivo, 'F');

        $this->conexion = null;

        if ( file_exists($this->path . $solicitud->nombreArchivo)) {
            return ["respuesta" => "EXITO", "nombreArchivo" => $solicitud->nombreArchivo];
        }

        return ["respuesta" => "Error al construir archivo", "nombreArchivo" => null];

        // VISUALIZAR PDF SIN DESCARGAR
        //$pdf->Output('MyPDF_File.pdf', 'I');

    }

    function rptSolicitudesPep( $accion, $desde, $hasta )
    {
        // antes llamaba a la funcion getIds()
        $agencias = implode(',', $this->agenciasAsignadas );

        $query = "SELECT * FROM controles_rptSolicitudesPep WHERE idAgencia IN ( $agencias ) AND CAST(fechaResolucion AS DATE) BETWEEN '$desde' AND '$hasta' ORDER BY fechaResolucion DESC";

        if ( $accion == 'PENDIENTE')
        {
            $query = "SELECT * FROM controles_rptSolicitudesPep  WHERE idAgencia IN ( $agencias ) AND CAST(fechaRegistro AS DATE) BETWEEN '$desde' AND '$hasta' ORDER BY fechaRegistro DESC";
        }

        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            $solicitudes = $result->fetchAll(PDO::FETCH_OBJ);

            foreach ( $solicitudes as $solicitud )
            {
                $sociedades = [];
                $sociedadesDB = explode('@@@', $solicitud->sociedades);

                foreach ( $sociedadesDB as $sociedad )
                {
                    if (!empty( $sociedad))
                    {
                        $sociedadTemp = explode('%%%', $sociedad);
                        array_push( $sociedades, $sociedadTemp[1].'('.$sociedadTemp[2].'%)' );
                    }
                }
                $solicitud->sociedades = implode(",", $sociedades);

                $personasRelacionadas = [];
                $personasRelacionadasDB = explode('@@@', $solicitud->personasRelacionadas );

                $nombres = [];
                $parentescos = [];
                $cargos = [];
                $gestiones = [];

                foreach ($personasRelacionadasDB as $persona )
                {
                    if ( !empty( $persona ) )
                    {
                        $personaTemp = explode('%%%', $persona);
                        array_push( $nombres, $personaTemp[1] );
                        array_push( $parentescos, $personaTemp[2]);
                        array_push($cargos, $personaTemp[3] );
                        array_push($gestiones, $personaTemp[4].' hasta '.$personaTemp[5].' ');
                    }
                }
                $solicitud->personasRelacionadas = null;
                $solicitud->nombrePep = implode(" / ", $nombres);
                $solicitud->parantescoPep = implode('/', $parentescos);
                $solicitud->cargosPep = implode('/', $cargos);
                $solicitud->gestionesPep = implode('/', $gestiones);

                if( empty( $solicitud->resolucion ) )
                {
                    $solicitud->resolucion = 'PENDIENTE';
                    $solicitud->fechaResolucion = 'PENDIENTE';
                } else {
                    $solicitud->fechaResolucion = date('d-m-Y h:i a', strtotime($solicitud->fechaResolucion));;
                }
                $solicitud->fechaRegistro = date('d-m-Y h:i a', strtotime($solicitud->fechaRegistro));;
            }

            return $solicitudes;
        }
        $this->conexion = null;
        return [];
    }
}

class AntecedentePep {

    private $conexion;

    public $idSolicitud;
    public $id, $entidad, $nombre, $ejercicio, $inicioGestion, $finGestion;

    public CargoPolitico $Cargo;
    public CargoPolitico $CargoReemplazante;


    private $idCargo, $cargo;
    private $idCargoReemplazante, $cargoReemplazante;

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        $this->Cargo = new CargoPolitico();
        $this->CargoReemplazante = new CargoPolitico();

        if ( !empty( $this->idCargo) || !empty( $this->cargo ) )
        {

            $this->Cargo->id = (int) $this->idCargo;
            $this->Cargo->cargo = $this->cargo;
        }


        if ( !empty( $this->idCargoReemplazante ) || !empty($this->cargoReemplazante) )
        {
            $this->CargoReemplazante->id = (int) $this->idCargoReemplazante;
            $this->CargoReemplazante->cargo = $this->cargoReemplazante;
        }

        !empty($this->inicioGestion) && $this->inicioGestion = date('d-m-Y', strtotime($this->inicioGestion));
        !empty($this->finGestion) && $this->finGestion = date('d-m-Y', strtotime($this->finGestion));
    }

    public function obtenerAntecedentes ()
    {
        $query = "SELECT * FROM controles_viewDetAntecedentes WHERE idSolicitud = :idSolicitud ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $this->idSolicitud, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__)[0];
        }

        $this->conexion = null;
        return (object)[];
    }

    public function formatDb()
    {
        if ( $this->Cargo->id != '' && $this->entidad != '' )
        {
            $this->inicioGestion = $this->inicioGestion ? date('Y-m-d', strtotime($this->inicioGestion)) : 'NULL';
            $this->finGestion = $this->finGestion ? date('Y-m-d', strtotime( $this->finGestion )) : 'NULL';
            $this->nombre = !empty( $this->nombre ) ? $this->nombre : 'NULL';
            $this->Cargo->id =!empty( $this->Cargo->id ) ? $this->Cargo->id : 0;
            $this->ejercicio = !empty( $this->ejercicio) ? $this->ejercicio : 'NULL';

            return $this->id.'@@@'.$this->Cargo->id.'@@@'.$this->entidad.'@@@'.$this->inicioGestion.'@@@'.$this->finGestion.'@@@'.$this->nombre.'@@@'.$this->ejercicio.'@@@'.$this->CargoReemplazante->id;

        } else {
            return  '';
        }

    }
}

class Residencia {

    private $conexion;
    private $idUsuario;

    public $idPais, $pais, $idSolicitud, $idRepresentante;
    public $propietario;

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        if ( !empty( $this->idUsuario ) )
        {
            $this->propietario = ( $this->idUsuario == $_SESSION['index']->id ) ? 'S' : 'N';
        }
    }

    public function obtenerNacionalidades ()
    {
        $query = "SELECT * FROM controles_viewDetNacionalidadesPep WHERE idSolicitud = :idSolicitud ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $this->idSolicitud, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            //$this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

    function obtenerNacionalidadesRepresentante()
    {
        $query = "SELECT * FROM controles_viewDetNacionalidadesRepresentante WHERE idRepresentante = :idRepresentante ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idRepresentante', $this->idRepresentante, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }

}

class PersonasRelacionadas {

    private $conexion;

    public $idSolicitud, $nombre, $inicioGestion, $finGestion;
    public Vinculo $Vinculo;
    public Parentezco $Parentesco;
    public CargoPolitico $Cargo;
    private $idUsuario;
    public $propietario;

    private $idVinculo, $vinculo, $idParentesco, $parentesco, $idCargo, $cargo;

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        if ( !empty( $this->idVinculo ) || !empty(!empty( $this->vinculo) ) )
        {
            $this->Vinculo = new Vinculo();
            $this->Vinculo->id = $this->idVinculo;
            $this->Vinculo->vinculo = $this->vinculo;
        }

        $this->Parentesco = new Parentezco();

        if ( !empty( $this->idParentesco ) || !empty(!empty( $this->parentesco) ) )
        {
            $this->Parentesco->id = $this->idParentesco;
            $this->Parentesco->parentesco = $this->parentesco;
        } else {
            $this->Parentesco->id = 0;
            $this->Parentesco->parentesco = null;
        }

        if ( !empty( $this->idCargo ) || !empty(!empty( $this->cargo) ) )
        {
            $this->Cargo = new CargoPolitico();
            $this->Cargo->id = $this->idCargo;
            $this->Cargo->cargo = $this->cargo;
        }

        !empty( $this->idUsuario ) && $this->propietario = ( $this->idUsuario == $_SESSION['index']->id ) ? 'S' : 'N';
        !empty($this->inicioGestion) && $this->inicioGestion = date('d-m-Y', strtotime($this->inicioGestion));
        !empty($this->finGestion) && $this->finGestion = date('d-m-Y', strtotime($this->finGestion));
    }

    function obtenerRelacionados ()
    {
        $query = "SELECT * FROM controles_viewDetRelacionadosPep WHERE idSolicitud = :idSolicitud ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $this->idSolicitud, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return (object)[];
    }

}

class Sociedades {
    public $id, $idSolicitud, $sociedad, $porcentaje, $propietario;
    private $idUsuario;
    private $conexion;

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        !empty( $this->idUsuario ) && $this->propietario = ( $this->idUsuario == $_SESSION['index']->id ) ? 'S' : 'N';
    }

    function obtenerSociedades ()
    {
        $query = "SELECT * FROM controles_viewDetSociedades WHERE idSolicitud = :idSolicitud ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $this->idSolicitud, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return (object)[];
    }

}

class OtrasCuentas {

    public $idSolicitud, $nombre, $motivo;
    public Parentezco $Parentesco;
    public $propietario;
    private $idUsuario;

    private $conexion;

    private $idParentesco, $parentesco;

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        !empty( $this->idUsuario ) && $this->propietario = ( $this->idUsuario == $_SESSION['index']->id ) ? 'S' : 'N';

        if ( !empty( $this->idParentesco ) && !empty( $this->parentesco) )
        {
            $this->Parentesco = new Parentezco();
            $this->Parentesco->id = $this->idParentesco;
            $this->Parentesco->parentesco = $this->parentesco;
        }
    }

    public function obtenerOtrasCuentas ()
    {
        $query = "SELECT * FROM controles_viewDetOtrasCuentas WHERE idSolicitud = :idSolicitud ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $this->idSolicitud, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return (object)[];
    }

}

class ProductosContratados {

    private $conexion;
    public $id, $idSolicitud, $proposito;
    public ProductoServicio $Servicio;
    public $propietario;
    private $idUsuario;

    private $idServicio, $nombre;


    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;

        !empty( $this->idUsuario ) && $this->propietario = ( $this->idUsuario == $_SESSION['index']->id ) ? 'S' : 'N';

        if ( !empty( $this->idServicio) && !empty( $this->nombre ) )
        {
            $this->Servicio = new ProductoServicio();
            $this->Servicio->id = $this->idServicio;
            $this->Servicio->nombre = $this->nombre;
        }
    }

    public function obtenerProductos ()
    {
        $query = "SELECT * FROM controles_viewDetProductosContratados WHERE idSolicitud = :idSolicitud ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $this->idSolicitud, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return (object)[];
    }
}

class Vinculo {
    public $id, $vinculo;
}

class Representante {

    private $conexion;

    public $id, $idSolicitud, $nombres, $apellido, $idTipoDocumentoPrimario, $numeroDocumentoPrimario,
        $telefono, $email, $idPais, $idDepartamento, $idMunicipio, $direccion;

    public $residencias = [];

    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    function obtenerRepresentante()
    {
        $query = "SELECT * FROM controles_viewDetRepresentantesPep WHERE idSolicitud = :idSolicitud ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':idSolicitud', $this->idSolicitud, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;

            $representante =  $result->fetchAll(PDO::FETCH_CLASS, __CLASS__)[0];

            $residencia = new Residencia();
            $residencia->idRepresentante = $representante->id;

            $cargos = new CargoRepresentante();
            $cargos->idRepresentante = $representante->id;


            $representante->residencias = $residencia->obtenerNacionalidadesRepresentante();
            $representante->cargosPoliticos = $cargos->obtenerCargosRepresentante();

            return $representante;
        }

        $this->conexion = null;
        return (object)[];
    }

}

class CargoRepresentante {

    public $id, $idRepresentante, $inicioGestion, $finGestion, $Cargo;
    private $idCargo, $cargo;
    private $conexion;

    function __construct ()
    {

        global $conexion;
        $this->conexion = $conexion;


        $this->Cargo = new CargoPolitico();

        if ( !empty( $this->idCargo ) || !empty(!empty( $this->cargo) ) )
        {
            $this->Cargo->id = $this->idCargo;
            $this->Cargo->cargo = $this->cargo;
        }

        !empty($this->inicioGestion) && $this->inicioGestion = date('d-m-Y', strtotime($this->inicioGestion));
        !empty($this->finGestion) && $this->finGestion = date('d-m-Y', strtotime($this->finGestion));
    }

    function obtenerCargosRepresentante ()
    {
        $query = "SELECT * FROM controles_viewDetCargosPublicosRepresentante WHERE idRepresentante = :id ";
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->bindParam(':id', $this->idRepresentante, PDO::PARAM_INT);
        $result->execute();

        if ( $result->rowCount() > 0 )
        {
            $this->conexion = null;
            return $result->fetchAll(PDO::FETCH_CLASS, __CLASS__);
        }

        $this->conexion = null;
        return [];
    }
}

class solicitudPdf extends TCPDF
{
    private $solicitud;
    private $productos = [];

    function setSolicitud ( $solicitud )
    {
        $this->solicitud = $solicitud;
    }

    function setProductosAll( $productos )
    {
        $this->productos = $productos;
    }

    function configuracionDocumento()
    {
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('Departamento IT');
        $this->SetTitle('Solicitud de afiliación PEP');
        $this->SetSubject('ACACYPAC');
        $this->SetKeywords('TCPDF, PDF, Solicitud de afiliación PEP');

        $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    }

    function setMargin()
    {
        $this->SetMargins(15, 15, 15, 15);
        $this->SetHeaderMargin(4);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);
    }

    function Header()
    {
        $this->setFont('dejavusans', 'B', 10);
        $this->Cell(185, 6, 'N° de solicitud: ' . $this->solicitud->id, 0, 0, 'R', 0);
    }

    function Footer()
    {
        $this->setFont('dejavusans', 'B', 10);
        $this->Write(0, $this->solicitud->agencia .' | '.$this->solicitud->fechaRegistro, '', 0, 'R', true, 0, false, false, 0);
    }

    function generalidades ()
    {
        $this->ln(1);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(36, 6, 'Tipo de afiliación:', 0, 0, 'C', 0);
        $this->SetFont('dejavusans', '', 7);

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);

        $this->Cell(36, 6, 'PEP', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);

        $pep = '__________________'; $relacionado = '__________________';

        if ( $this->solicitud->idTipoAfiliacion == 1 )
        {
            $pep = '________X_________';
        } else
        {
            $relacionado = '________X_________';
        }

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->Cell(36, 6, $pep, 0, 0, 'C', 0);

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);

        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Relacionado a PEP', 0, 0, 'C', 1);

        $this->SetFont('dejavusans', 'B', 7);
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->Cell(36, 6, $relacionado, 0, 0, 'C', 0);
        $this->ln();

        $this->ln(3);
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(36, 6, 'Tipo de Solicitud:', 0, 0, 'C', 0);

        $nuevo = '__________________'; $recurrente = '__________________';

        if ($this->solicitud->idTipoSolicitud == 1 )
        {
            $nuevo = '________X_________';
        } else
        {
            $recurrente = '________X_________';
        }

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Nuevo ingreso', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->Cell(36, 6, $nuevo, 0, 0, 'C', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);
        $this->Cell(36, 6, 'Recurrente', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->Cell(36, 6, $recurrente, 0, 0, 'C', 0);
        $this->ln();

    }

    function datosPersonales ()
    {
        $this->ln(5);

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);

        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B', 7);

        $this->Cell(175, 7, 'Datos Personales', 1, 0, 'C', 1);
        $this->Cell(10, 7, '', 1, 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Nombre del aspirante', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->solicitud->nombres.' '.$this->solicitud->apellidos, 'LR', 0, 'L', 1);
        $this->ln();


        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'DUI/PASAPORTE', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->solicitud->numeroDocumentoPrimario, 'LR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Fecha y lugar de expedición', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->solicitud->fechaExpedicion.' / '.$this->solicitud->lugarExpedicion , 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Teléfono', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->solicitud->telefono, 'LR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Email', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->solicitud->email , 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Estado civil', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->solicitud->estadoCivil, 'LR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Fecha y lugar de nacimiento', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->solicitud->fechaNacimiento.' / '.$this->solicitud->lugarDeNacimiento, 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 9, 'Dirección', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->writeHTMLCell(125, 9, '', '', '<p style="text-align: justify">'.$this->solicitud->direccion.', '.$this->solicitud->municipio.', '.$this->solicitud->departamento.'</p>', 'LR', 1, 0, true, 'L', 1);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 9, 'Actividad económica primaria', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->writeHTMLCell(125, 9, '', '', '<p style="text-align: justify">'.$this->solicitud->actividadEconomicaPrimaria.'</p>', 'LR', 1, 1, true, 'L', 1);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 9, 'Actividad económica secundaria', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->writeHTMLCell(125, 9, '', '', '<p style="text-align: justify">'.$this->solicitud->actividadEconomicaSecundaria.'</p>', 'LR', 1, 0, true, 'L', 1);
        //$this->Cell(125, 7, $this->solicitud->actividadEconomicaSecundaria, 'LR', 0, 'L', 0);
        //$this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Profesión u oficio', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->solicitud->profesion, 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Lugar de trabajo', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->solicitud->lugarDeTrabajo, 'LR', 0, 'L', 0);
        $this->ln();


        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, '¿Tiene otra nacionalidad?', 'TLR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, $this->solicitud->otraNacionalidad == 'S' ? 'SI' : 'NO', 'TLR', 0, 'L', 1);
        $this->ln();

        $this->Cell(185, 7, 'En caso que SÍ, liste los países en los que tiene otra nacionalidad', 'TRL', 0, 'C', 0);
        $this->ln();

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B');

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(20, 6, 'N°', 'TLR', 0, 'C', 1);
        $this->Cell(60, 6, 'País', 'TLR', 0, 'C', 1);
        $this->Cell(105, 6, 'Dirección de residencia', 'TLR', 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);

        if ( count( (array) $this->solicitud->residencias ) > 0 )
        {
            $contador = 0;
            foreach ( $this->solicitud->residencias as $residencia )
            {
                $contador++;
                $this->SetFont('dejavusans', '', 7);
                $this->Cell(20, 7, $contador, 'TLR', 0, 'C', 0);
                $this->Cell(60, 7, $residencia->pais, 'TLR', 0, 'C', 0);
                $this->Cell(105, 7, $residencia->direccion, 'TLR', 0, 'C', 0);
                $this->ln();
            }
        } else
        {
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(185, 6, 'No hay nacionalidades', 'TLR', 0, 'C', 0);
            $this->ln();
        }
        $this->Cell(185, 0, '', 'T');

    }

    function Antecedentes ()
    {
        $antecedente = $this->solicitud->Antecedente ?? NULL ;

        $this->ln(5);

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(185, 7, 'ANTECEDENTES DEL CARGO PÚBLICO DEL PEP', 'LR', 0, 'C', 1);
        $this->ln();


        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(16, 126, 20);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(70, 6, 'Cargo que desempeña como PEP', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(115, 6, $antecedente ? $antecedente->Cargo->cargo : '', 'LR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(70, 6, 'Organismo donde se desempeña', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(115, 6, $antecedente ? $antecedente->entidad : '', 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(70, 6, 'Periodo de gestión', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(115, 6, $antecedente ? $antecedente->inicioGestion.' - '.$antecedente->finGestion : '', 'LR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(185, 6, 'Datos de la persona reemplazante en sus funciones del cargo público', 'TRL', 0, 'C', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(70, 6, 'Nombre completo:', 'TLR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 9);
        $this->Cell(115, 6, $antecedente ? $antecedente->nombre : '', 'TLR', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(70, 6, 'Cargo', 'LR', 0, 'L', 1);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(115, 6, $antecedente ? $antecedente->CargoReemplazante->cargo : '', 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(70, 6, 'Ejercicio', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(115, 6, $antecedente ? $antecedente->ejercicio : '', 'LR', 0, 'L', 0);
        $this->ln();
        $this->Cell(185, 0, '', 'T');



    }

    function personaRelacionada()
    {
        $personaRelacionada = $this->solicitud->personasRelacionadas ?? NULL ;

        $this->ln(3);

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(185, 6, 'PERSONA RELACIONADA A PEP', 'LR', 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(16, 126, 20);

        $this->SetFont('dejavusans', '', 7);
        $this->Cell(100, 6, '¿Es o a sido una persona relacionada a PEP?', '', 0, 'L', 0);
        $this->Cell(85, 6, $personaRelacionada ? count( (array) $personaRelacionada) > 0 ? 'SI' : 'NO' : 'NO' , '', 0, 'L', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(185, 6, 'Datos de las personas relacionadas', '', 0, 'C', 1);
        $this->ln();

        if( $personaRelacionada && count( (array ) $personaRelacionada ) > 0 )
        {
            $this->ln(2);
            foreach ( $personaRelacionada as $persona )
            {
                $this->SetFont('dejavusans', 'B', 7);
                $this->Cell(60, 6, 'VINCULO', 'TL', 0, 'C', 0);
                $this->Cell(60, 6, 'NOMBRE DEL RELACIONADO', 'T', 0, 'C', 0);
                $this->Cell(65, 6, 'PARENTESCO' , 'TR', 0, 'C', 0);
                $this->ln();

                $this->SetFont('dejavusans', '', 7);
                $this->Cell(60, 6, $persona->Vinculo->vinculo, 'L', 0, 'C', 1);
                $this->Cell(60, 6, $persona->nombre, 0, 0, 'C', 1);
                $this->Cell(65, 6, $persona->Parentesco->parentesco , 'R', 0, 'C', 1);
                $this->ln();

                $this->SetFont('dejavusans', 'B', 7);
                $this->Cell(100, 6, 'CARGO POLÍTICO DEL RELACIONADO', 'L', 0, 'C', 0);
                $this->Cell(85, 6, 'PERIODO DE GESTIÓN', 'R', 0, 'C', 0);
                $this->ln();

                $this->SetFont('dejavusans', '', 7);
                $this->Cell(100, 6, $persona->Cargo->cargo, 'L', 0, 'C', 0);
                $this->Cell(85, 6, $persona->inicioGestion.' - '.$persona->finGestion, 'R', 0, 'C', 0);
                $this->ln();
                $this->Cell(185, 0, '', 'T');
                $this->ln();
            }
        } else
        {
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(185, 6, 'No hay personas relacionadas', '', 0, 'C', 0);
            $this->ln();
        }

    }

    function sociedades()
    {
        $sociedades = $this->solicitud->sociedades;

        $this->ln(2);

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(185, 6, 'SOCIEDADES CON LAS QUE TIENE RELACIÓN PATRIMONIAL', 'LR', 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(16, 126, 20);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(20, 6, 'N°', 'LR', 0, 'L', 1);
        $this->Cell(100, 6, 'Razón social', '', 0, 'L', 1);
        $this->Cell(65, 6, 'Porcentaje de participación' , 'LR', 0, 'L', 1);
        $this->ln();

        if ( $sociedades && count( (array) $sociedades ) > 0 )
        {
            $contador = 0;

            foreach ( $sociedades as $sociedad )
            {
                $contador++;

                $this->SetFont('dejavusans', '', 7);
                $this->Cell(20, 6, $contador, 'TLR', 0, 'C', 0);
                $this->Cell(100, 6, $sociedad->sociedad, 'TLR', 0, 'L', 0);
                $this->Cell(65, 6, $sociedad->porcentaje , 'TLR', 0, 'C', 0);
                $this->ln();
            }
        } else
        {
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(185, 6, 'No hay sociedades', 'TLR', 0, 'C', 0);
            $this->ln();
        }
        $this->Cell(185, 0, '', 'T');

    }

    function cuentasTercero()
    {
        $cuentas = $this->solicitud->otrasCuentas;

        $this->ln(3);

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(185, 6, 'CUENTAS DE TERCEROS', 'LR', 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(16, 126, 20);

        $this->SetFont('dejavusans', '', 7);
        $this->Cell(100, 6, '¿Maneja cuentas de terceros?', '', 0, 'C', 0);
        $this->Cell(85, 6, count( (array) $cuentas) > 0 ? 'SI': 'NO', '', 0, 'C', 0);
        $this->ln();

//        $this->SetFont('dejavusans', 'B', 7);
//        $this->Cell(185, 6, 'DETALLE DE MANEJO DE CUENTAS', '', 0, 'C', 1);
//        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(10, 6, 'N°', 'TL', 0, 'C', 0);
        $this->Cell(63, 6, 'Nombre', 'T', 0, 'C', 0);
        $this->Cell(40, 6, 'Parentesco' , 'TL', 0, 'C', 0);
        $this->Cell(72, 6, 'Motivo', 'TLR', 0, 'C', 0);
        $this->ln();

        $this->SetFont('dejavusans', '', 6);

        if ( $cuentas && count( (array) $cuentas) > 0 )
        {
            $contador = 0;

            foreach ( $cuentas as $cuenta )
            {
                $contador++;
                $this->Cell(10, 6, $contador, 'TL', 0, 'L', 0);
                $this->Cell(63, 6, $cuenta->nombre, 'T', 0, 'L', 0);
                $this->Cell(40, 6, $cuenta->Parentesco->parentesco , 'TRL', 0, 'L', 0);
                $this->Cell(72, 6, $cuenta->motivo , 'TR', 0, 'L', 0);
                $this->ln();
            }
        } else
        {
            $this->Cell(185, 6, 'No hay detalle de cuentas' , 'TLR', 0, 'C', 0);
            $this->ln();
        }

        $this->Cell(185, 0, '', 'T');
    }

    function fuenteDeIngreso()
    {
        $this->ln(3);

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(185, 7, 'FUENTE DE INGRESOS DEL ASPIRANTE', 'LR', 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(16, 126, 20);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 6, 'Fuente de ingresos', 'L', 0, 'C', 0);
        $this->Cell(30, 6, 'Monto en US $', 'LR', 0, 'C', 0);
        $this->Cell(95, 6, 'Detalle', 'R', 0, 'C', 0);
        $this->ln();

        $this->SetFont('dejavusans', '', 7);
        $this->Cell(60, 6, 'Remuneración Mensual', 'L', 0, 'C', 0);
        $this->Cell(30, 6, '$ '.$this->solicitud->remuneracionMensual, 'LR', 0, 'C', 0);
        $this->Cell(95, 6, $this->solicitud->detalleRemuneracion, 'R', 0, 'L', 0);
        $this->ln();

        $this->Cell(60, 6, 'Otros ingresos mensuales', 'L', 0, 'C', 0);
        $this->Cell(30, 6, $this->solicitud->otrosIngresos, 'LR', 0, 'C', 0);
        $this->Cell(95, 6, $this->solicitud->detalleOtrosIngresos, 'R', 0, 'L', 0);
        $this->ln();

        $this->Cell(60, 6, 'Total de ingresos mensuales', 'TL', 0, 'C', 0);
        $this->Cell(30, 6,  '$ '. number_format( ((float) $this->solicitud->remuneracionMensual + (float) $this->solicitud->otrosIngresos ), 2 ), 'TLR', 0, 'C', 0);
        $this->Cell(95, 6, '', 'TR', 0, 'L', 0);

        $this->ln();

        $this->Cell(185, 0, '', 'T');
    }

    function productosContratados()
    {
        $this->ln(3);

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(185, 6, 'PRODUCTOS A CONTRATAR POR EL CLIENTE PEP O RELACIONADO', 'LR', 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(16, 126, 20);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(10, 6, 'N°', '', 0, 'C', 0);
        $this->Cell(50, 6, 'Producto/Servicio', '', 0, 'C', 0);
        $this->Cell(45, 6, 'Productos a contratar', '', 0, 'C', 0);
        $this->Cell(80, 6, 'Propósito del producto/servicio a contratar', '', 0, 'L', 0);
        $this->ln();


        $contador = 0;

        foreach ( $this->productos as $producto )
        {
            $this->SetFont('dejavusans', '', 7);
            $contador++;
            $producto->esSeleccionado = false;
            $producto->proposito = '';

            foreach ( $this->solicitud->productos as $productoSolicitud )
            {
                if ( $productoSolicitud->Servicio->id == $producto->id )
                {
                    $producto->esSeleccionado = true;
                    $producto->proposito = $productoSolicitud->proposito;
                }
            }

            $this->Cell(10, 6, $contador, 'T', 0, 'C', 0);
            $this->Cell(50, 6, $producto->nombre, 'T', 0, 'L', 0);

            if( $producto->esSeleccionado )
            {
                $this->Cell(45, 6, 'X', 'T', 0, 'C', 0);

            } else
            {
                $this->Cell(45, 6, '', 'T', 0, 'C', 0);
            }
            $this->SetFont('dejavusans', '', 7);
            $this->writeHTMLCell(80, 6, '', '', '<p style="text-align: justify">'.$producto->proposito.'</p>', 'T', 1, 0, true, 'L', 0);
        }
        $this->Cell(185, 0, '', 'T');
    }

    function representante()
    {
        $this->ln(5);

        $representante = $this->solicitud->representante ??  null;

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);

        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B', 7);

        $this->Cell(175, 7, 'DATOS DEL REPRESENTANTE / APODERADO', 1, 0, 'C', 1);
        $this->Cell(10, 7, '', 1, 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Nombre del apoderado', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7,  count((array) $representante) > 0 ? $representante->nombres.' '.$representante->apellidos : '', 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Pasaporte/DUI', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, count((array) $representante) > 0  ? $representante->tipoDocumento : '', 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Numéro de documento', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, count((array) $representante) > 0  ? $representante->numeroDocumentoPrimario : '', 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Teléfono', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, count((array) $representante) > 0  ? $representante->telefono : '', 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, 'Email', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, count((array) $representante) > 0  ? $representante->email : '', 'LR', 0, 'L', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 8, 'Dirección', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $direccionCompleta = count( (array) $representante) > 0  ? $representante->direccion.', '.$representante->municipio.', '.$representante->departamento : '';
        $this->writeHTMLCell(125, 7, '', '', '<p style="text-align: justify">'. $direccionCompleta .'</p>', 'LR', 1, 1, true, 'L', 0);

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(60, 7, '¿Tiene otra nacionalidad?', 'LR', 0, 'L', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(125, 7, count((array) $representante) > 0  ? count( $representante->residencias ) > 0 ? 'SI' : 'NO' : '', 'LR', 0, 'L', 1);
        $this->ln();


        $this->Cell(185, 7, 'En caso que SÍ, liste los países en los que tiene otra nacionalidad', 'TRL', 0, 'C', 0);
        $this->ln();

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);
        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B');

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(20, 6, 'N°', 'TLR', 0, 'C', 1);
        $this->Cell(60, 6, 'País', 'TLR', 0, 'C', 1);
        $this->Cell(105, 6, 'Dirección de residencia', 'TLR', 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);

        if ( count((array) $representante) > 0  && count( (array) $representante->residencias ) > 0 )
        {
            $contador = 0;
            foreach ( $representante->residencias as $residencia )
            {
                $contador++;
                $this->SetFont('dejavusans', '', 7);
                $this->Cell(20, 7, $contador, 'TLR', 0, 'C', 0);
                $this->Cell(60, 7, $residencia->pais, 'TLR', 0, 'C', 0);
                $this->Cell(105, 7, $residencia->direccion, 'TLR', 0, 'C', 0);
                $this->ln();
            }
            $this->Cell(185, 0, '', 'T');
        } else
        {
            $this->SetFont('dejavusans', '', 7);
            $this->Cell(185, 6, 'No hay nacionalidades', 'TLR', 0, 'C', 0);
            $this->ln();
            $this->Cell(185, 0, '', 'T');
        }

        $this->ln();

        $this->SetFillColor(0,128,57);
        $this->SetTextColor(255);

        $this->SetDrawColor(16, 126, 20);
        $this->SetLineWidth(0.3);
        $this->SetFont('dejavusans', 'B', 7);

        $this->Cell(175, 7, '¿Usted ejerce o ha ejercido un cargo político?', 1, 0, 'C', 1);
        $this->Cell(10, 7, '', 1, 0, 'C', 1);
        $this->ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);

        $this->SetFont('dejavusans', '', 7);
        $this->Cell(185, 7, count((array) $representante) > 0  ? count( (array) $representante->cargosPoliticos ) > 0 ? 'SI': 'NO' : '','LR', 0, 'C', 1);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(20, 6, 'N°', 'TLR', 0, 'C', 0);
        $this->Cell(90, 6, 'Cargo', 'TLR', 0, 'C', 0);
        $this->Cell(75, 6, 'Periodo de gestión', 'TLR', 0, 'C', 0);
        $this->ln();

        if ( count((array) $representante) > 0  && count( (array) $representante->cargosPoliticos) > 0 )
        {
            $contador = 0;

            foreach ( $representante->cargosPoliticos as $cargoPolitico )
            {
                $contador++;
                $this->SetFont('dejavusans', '', 7);
                $this->Cell(20, 6, $contador, 'TLR', 0, 'C', 0);
                $this->Cell(90, 6, $cargoPolitico->Cargo->cargo, 'TLR', 0, 'C', 0);
                $this->Cell(75, 6, $cargoPolitico->inicioGestion.' - '. $cargoPolitico->finGestion, 'TLR', 0, 'C', 0);
                $this->ln();
            }
        }


        $this->Cell(185, 0, '', 'T');

    }

    function manifiesto ()
    {
        $this->ln(8);


        $uno = '1-  Manifiesto tener conocimiento que esta solicitud ha sido completada en cumplimiento al Marco Legal, Normativa Nacionalidad y a la política para
            Gestión de Personas Expuestas Políticamente, establecida por ACACYPAC N.C de R.L, por tal razón, mi afiliación a la Cooperativa queda
            sujeta a la resolución del Consejo de Administración, la cual me será posteriormente informada.';

        $dos = '2-  Me comprometo a proporcionar la documentación e información requerida para anexar a esta solicitud. Además, me comprometo a
            mantener actualizada mi información y autorizo a ACACYPAC para que confirme la veracidad de la información a través de los medios que
            considere conveniente
        ';

        $tres = '3- Declaro que mi nombre no está incluido en la Oficiona de Control de Activos en el Exterior (OFAC) o en cualquier otra lista de similar naturaleza 
        o en alguna de carácter nacional y/o internacional en la que se publiquen los datos de las personas a quienes se le haya iniciado proceso
        judicial o que hayan sido sansionadas y/o condenadas por las autoridades nacionales e internacionales, de manera directa o indirecta, con 
        actividades ilegales, tales como narcotráfico, terrorismo o su financiación, lavado de dinero y activos, tráfico de estupefacientes, secuestro,
        extorciones, trata de personas, entre otras.
        ';

        $this->SetFont('dejavusans', '', 7);
        $this->writeHTMLCell(185, 10, '', '', '<p style="text-align: justify">'. $uno .'</p>', 'TLR', 1, 0, true, 'L', 0);

        $this->writeHTMLCell(185, 10, '', '', '<p style="text-align: justify">'. $dos .'</p>', 'LR', 1, 1, true, 'L', 1);

        $this->writeHTMLCell(185, 10, '', '', '<p style="text-align: justify">'. $tres .'</p>', 'LR', 1, 0, true, 'L', 0);

        $this->Cell(185, 0, '', 'T');

    }

    function firmaRepresentante()
    {
        $this->ln(15);

        $this->setFont('dejavusans', 'B', 9);
        $this->Cell(92, 6, '____________________________', false, 0, 'C', 0);
        $this->Cell(92, 6, '____________________________', false, 0, 'C', 0);
        $this->ln();

        $this->setFont('dejavusans', '', 6);
        $this->Cell(92, 6, 'Firma del solicitante/Representante Legal/Apoderado', false, 0, 'C', 0);
        $this->Cell(92, 6, 'Lugar y fecha', false, 0, 'C', 0);
        $this->ln();

        $this->ln(5);

        $this->setFont('dejavusans', '', 6);
        $this->Cell(30, 6, 'Nombre del empleado:', 'TLR', 0, 'L', 0);
        $this->Cell(70, 6, $this->solicitud->nombreUsuario.' '.$this->solicitud->apellidoUsuario, 'TLR', 0, 'L', 0);
        $this->ln();

        $this->setFont('dejavusans', '', 6);
        $this->Cell(30, 10, 'Firma:', 'TLR', 0, 'C', 0);
        $this->Cell(70, 10, '', 'TLR', 0, 'L', 0);
        $this->ln();

        $this->setFont('dejavusans', '', 6);
        $this->Cell(30, 15, 'Sello:', 'TLR', 0, 'C', 0);
        $this->Cell(70, 15, '', 'TLR', 0, 'L', 0);
        $this->ln();

        $this->Cell(100, 0, '', 'T');
    }

    function resolución()
    {

        $this->SetFont('dejavusans', 'B', 10);
        $this->Cell(185, 7, 'USO EXCLUSIVO DE QUIEN ANALIZA LA SOLICITUD', 0, 0, 'C', 0);
        $this->ln();

        $this->ln(15);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Consejo de Administración', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(24, 6, '__________________', 0, 0, 'C', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Gerencia General', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(24, 6, '__________________', 0, 0, 'C', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Gerencia de Negocios', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(24, 6, '__________________', 0, 0, 'C', 0);
        $this->ln();

        $this->ln(10);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(185, 6, 'Después de haber analizado la información del asociado, se resuelve:', 0, 0, 'L', 0);
        $this->ln();

        $this->ln(8);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Aprobada', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(24, 6, '__________________', 0, 0, 'C', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Pendiente', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(24, 6, '__________________', 0, 0, 'C', 0);
        $this->SetFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Denegada', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(24, 6, '__________________', 0, 0, 'C', 0);
        $this->ln();

        $this->ln(5);
        $this->setFont('dejavusans', '', 7);
        $this->Cell(36, 6, 'Fecha de aprobación:', 0, 0, 'C', 1);
        $this->SetFont('dejavusans', 'B', 7);
        $this->Cell(65, 6, '______________________________________________', 0, 0, 'C', 0);
        $this->ln();

        $this->ln(8);
        $this->setFont('dejavusans', 'B', 7);
        $this->Write(0, 'Observaciones:', '', 0, 'L', true, 0, false, false, 0);
        $this->ln(5);
        $this->setFont('dejavusans', '', 7);
        $this->writeHTML("<hr>", true, true, true, true, '');
        $this->ln(3);
        $this->writeHTML("<hr>", true, true, true, true, '');
        $this->ln(3);
        $this->writeHTML("<hr>", true, true, true, true, '');
        $this->ln(3);
        $this->writeHTML("<hr>", true, true, true, true, '');
        $this->ln(3);
        $this->writeHTML("<hr>", true, true, true, true, '');

        $this->ln(20);

        $this->SetFont('dejavusans', '', 10);
        $this->Cell(180, 8, '_______________________________________________________________', 0, 0, 'C', 0);
        $this->ln();

        $this->SetFont('dejavusans', 'B', 8);
        $this->Cell(180, 8, 'Firma', 0, 0, 'C', 0);
    }
}