<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = ['solicitudes' => [], 'solicitudesResueltas' => [] ];

if ( isset($_SESSION['index'])  && $_SESSION['index']->locked )
{
    //include "../../../code/connectionSqlServer.php";


    require_once 'Models/SolicitudPep.php';

    $fechaActual = date("d-m-Y");
    $fecha3MesesAtras =  date("d-m-Y",strtotime($fechaActual."- 3 month"));

    $idApartado = base64_decode(urldecode($_GET["idApartado"]));
    $tipoApartado = $_GET['tipoApartado'];

    $agencias = (array) $_SESSION["index"]->agencias;
    $roles = $_SESSION["index"]->roles;

    $solicitud = new SolicitudPep();
    $solicitud->setIdApartado( $idApartado );
    $solicitud->setTipoApartado($tipoApartado);
    $solicitud->setAgencias( $agencias );
    $solicitud->idUsuario = $_SESSION['index']->id;
    $respuesta['solicitudes'] = $solicitud->obtenerDatosSolicitudPep( $roles );
    $respuesta['solicitudesResueltas'] = $solicitud->obtenerSolicitudesAprobadas( $roles, $fecha3MesesAtras);

}

echo json_encode( $respuesta );