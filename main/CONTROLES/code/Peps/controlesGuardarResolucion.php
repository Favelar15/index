<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();

$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked)
{
    include "../../../code/connectionSqlServer.php";
    require_once "./Models/SolicitudPep.php";

    $idApartado =  (int) base64_decode( urldecode( $input['idApartado'] ));
    $tipoApartado = $input['tipoApartado'];

    $solicitud = new SolicitudPep();
    $solicitud->setIdApartado( $idApartado );
    $solicitud->setTipoApartado( $tipoApartado );
    $solicitud->id = $input['id'];
    $solicitud->idUsuario = $_SESSION['index']->id;
    $solicitud->ipOrdenador = generalObtenerIp();
    $idArea = $input['cboArea']['value'];
    $idResolucion = $input['cboResolucion']['value'];
    $fechaResolucion = date('Y-m-d', strtotime( $input['txtFechaResolucion']['value'] ) );
    $observacion = $input['txtObservacion']['value'];
    $respuesta = $solicitud->guardarResolucion( $idArea, $idResolucion, $fechaResolucion, $observacion );

} else {
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );