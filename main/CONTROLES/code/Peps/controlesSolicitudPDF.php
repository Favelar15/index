<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = [];

if ( isset($_SESSION['index'])  && $_SESSION['index']->locked )
{
    if ( isset( $_GET['idSolicitud'] ))
    {
        require_once 'Models/CargoPolitico.php';
        require_once 'Models/SolicitudPep.php';
        require_once 'Models/Parentezco.php';
        require_once 'Models/ProductoServicio.php';

        $SolicitudPep = new SolicitudPep();
        $SolicitudPep->id = $_GET['idSolicitud'];
        $respuesta = $SolicitudPep->generarPdf();

    } else {
        $respuesta = ["respuesta" => "Error al construir archivo", "nombreArchivo" => null];
    }
} else
{
    $respuesta['respuesta'] = 'SESION';
}

 echo json_encode( $respuesta  );
