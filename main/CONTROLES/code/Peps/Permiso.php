<?php

include '../../../code/connectionSqlServer.php';

class Permiso
{
    protected $idApartado, $tipoPermiso, $administrativo = false;
    protected $tipoApartado;
    private $conexion;


    public function __construct()
    {
        global $conexion;
        $this->conexion = $conexion;
    }

    function isAdministrativo ( $roles )
    {
        foreach ( $roles as $rol )
        {
            if ( in_array( $rol->id, $this->obtenerRoles() ))
            {
                if ( $rol->tipo == 'Administrativo' )
                {
                    $this->administrativo = true;
                    break;
                }
            }
        }
        //$this->administrativo = $this->obtenerRoles();
    }
    // TIPO PERMISO ES EL MALO, CAMBIAR TIPO APARTADO POR TIPO PERMISO

    private function obtenerRoles ()
    {
        $vista = $this->tipoPermiso == 'Sub' ? 'general_viewRolesSubApartados' : 'general_viewRolesApartados';
        $query = "SELECT top(1) * from " . $vista . " WHERE id = " . $this->idApartado;
        $result = $this->conexion->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $result->execute();

        if( $result->rowCount() > 0  )
        {
            $dataRoles = $result->fetch(PDO::FETCH_ASSOC);
            //return $dataRoles;
            return explode('@@@', $dataRoles["roles"]);
        }
        return [];
    }

}