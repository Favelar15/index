<?php

header("Content-type: application/json; charset=utf-8");
include "../../../code/generalParameters.php";
session_start();

$respuesta = [];


if ( isset($_SESSION['index'])  && $_SESSION['index']->locked )
{
    require_once 'Models/CargoPolitico.php';
    require_once 'Models/SolicitudPep.php';
    require_once 'Models/Parentezco.php';
    require_once 'Models/ProductoServicio.php';

    $solicitud = new SolicitudPep();
    $solicitud->id = (int) $_GET['idSolicitud'];
    $solicitud->idUsuario = $_SESSION['index']->id;
    $respuesta['solicitud'] = $solicitud->obtenerDatosSolicitud();

}

echo json_encode( $respuesta );


