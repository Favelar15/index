<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();

$respuesta = [ 'solicitudes' => [] ];

if (isset($_SESSION['index']) && $_SESSION["index"]->locked)
{
    $idAgencias = [];

    if ( $input['cboAgencia']['value'] == 'all')
    {
        $agenciasAsignadas = $_SESSION['index']->agencias;

        foreach ( $agenciasAsignadas as $agencia )
        {
            array_push( $idAgencias, (int) $agencia->id);
        }
    } else
    {
        array_push( $idAgencias, (int) $input['cboAgencia']['value'] );
    }

    require_once '../../../code/connectionSqlServer.php';
    require_once 'Models/SolicitudPep.php';

    $solicitudPep = new SolicitudPep();
    $solicitudPep->setAgencias( $idAgencias );
    $desde = date('Y-m-d', strtotime( $input['txtInicio']['value'] ));
    $hasta =  date('Y-m-d', strtotime( $input['txtFin']['value'] ));
    $respuesta['solicitudes'] = $solicitudPep->rptSolicitudesPep($input['accion'], $desde, $hasta);
}

echo json_encode( $respuesta );
