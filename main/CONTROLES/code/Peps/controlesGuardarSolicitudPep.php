<?php

header("Content-type: application/json; charset=utf-8");
$input = json_decode(file_get_contents("php://input"), true);
include "../../../code/generalParameters.php";

session_start();
$respuesta = [];

if (isset($_SESSION['index']) && $_SESSION['index']->locked )
{
    include "../../../code/connectionSqlServer.php";

    $datosGenerales = $input['datosGenerales'];
    $datosPersonales = $input['datosPersonales'];


    // echo json_encode( $antecedentesPep); die;

    $antecedentesPep = null;
    $representanteDB = '';

    $id = (int) $datosGenerales['id'];

    if ( $datosGenerales['cboRepresentante']['value'] == 'S' )
    {
        $representante = $input['Representante'];

        $Rtelefono =    $representante['txtTelefonoRepresentante']['value'] ?? 'NULL';
        $Remail =       $representante['txtEmailRepresentante']['value'] ?? 'NULL';
        $Rresidencias = $representante['residencias'];
        $RcargosPoliticos = $representante['cargosPoliciticos'];

        $RresidenciasDB  = ''; $cargosPoliticosDB = '';

        if ( count( $Rresidencias ) > 0 )
        {
            $tempRresidencias = [];

            foreach ( $Rresidencias as $residencia )
            {
                array_push( $tempRresidencias, $residencia['id'].'###'.$residencia['idPais'].'###'.$residencia['residencia']);
            }
            $RresidenciasDB = implode('%%%', $tempRresidencias );
        }

        if ( count( $RcargosPoliticos ) > 0 )
        {
            $tempCargos = [];
            $inicio = null; $fin = null;

            foreach ( $RcargosPoliticos as $cargo )
            {
                $inicio = date('Y-m-d', strtotime( $cargo['inicioGestion'] ) );
                $fin = date('Y-m-d', strtotime( $cargo['finGestion'] ) );

                array_push( $tempCargos, $cargo['id'].'###'.$cargo['idCargo'].'###'.$inicio.'###'.$fin);
            }
            $cargosPoliticosDB = implode('%%%', $tempCargos );
        }


        $representanteDB =   $representante['idRepresentante'].'@@@'.$representante['txtNombresRepresentante']['value'].'@@@'.$representante['txtApellidosRepresentante']['value'].'@@@'
            .$representante['cboTipoDocumentoRepresentante']['value'].'@@@'.$representante['txtDocumentoPrimarioRepresentante']['value'].'@@@'
            .$Rtelefono.'@@@'.$Remail.'@@@'.$representante['cboPaisRepresentante']['value'].'@@@'
            .$representante['cboDepartamentoRepresentante']['value'].'@@@'.$representante['cboMunicipioRepresentante']['value'].'@@@'
            .$representante['txtDireccionRepresentante']['value'].'@@@'.$RresidenciasDB.'@@@'.$cargosPoliticosDB;
    }

    if ( $datosGenerales['cboTipoAfiliacion']['value'] == 1 )
    {
        $antecedentesPep = $input['antecedentes'];

    }

    require_once 'Models/SolicitudPep.php';

    $solicitud = new SolicitudPep();
    $solicitud->id = $id;
    $solicitud->representante = $representanteDB;
    $solicitud->idTipoAfiliacion = (int) $datosGenerales['cboTipoAfiliacion']['value'];
    $solicitud->idTipoSolicitud = (int) $datosGenerales['cboTipoSolicitud']['value'];
    $solicitud->codigoCliente = $datosPersonales['txtCodigoAsociado']['value'] ?? NULL;
    $solicitud->nombres = $datosPersonales['txtNombres']['value'];
    $solicitud->apellidos = $datosPersonales['txtApellidos']['value'];
    $solicitud->idTipoDocumentoPrimario = $datosPersonales['cboTipoDocumento']['value'];
    $solicitud->numeroDocumentoPrimario = $datosPersonales['txtDocumentoPrimario']['value'];
    $solicitud->fechaExpedicion = date('Y-m-d', strtotime( $datosPersonales['txtFechaExpedicion']['value'] ) );
    $solicitud->lugarExpedicion = $datosPersonales['txtLugarExpedicion']['value'];
    $solicitud->telefono = $datosPersonales['txtTelefono']['value'];
    $solicitud->email = $datosPersonales['txtEmail']['value'];
    $solicitud->fechaNacimiento = date('Y-m-d', strtotime( $datosPersonales['txtFechaNacimiento']['value'] ) );
    $solicitud->lugarDeNacimiento = $datosPersonales['txtLugarNacimiento']['value'];
    $solicitud->idEstadoCivil = $datosPersonales['cboEstadoCivil']['value'];
    $solicitud->nacionalidad = (int) $datosPersonales['cboGentilicio']['value'];
    $solicitud->idPais = $datosPersonales['cboPais']['value'];
    $solicitud->idDepartamento = $datosPersonales['cboDepartamento']['value'];
    $solicitud->idMunicipio = $datosPersonales['cboMunicipio']['value'];
    $solicitud->direccion = $datosPersonales['txtDireccion']['value'];
    $solicitud->profesion = $datosPersonales['txtProfesion']['value'];
    $solicitud->lugarDeTrabajo = $datosPersonales['txtLugarDeTrabajo']['value'];
    $solicitud->idActividadEconomicaPrimaria = $datosPersonales['cboActividadEconomicaPrincipal']['value'];
    $solicitud->idActividadEconomicaSecundaria = $datosPersonales['cboActividadEconomicaSecundaria']['value'];


    $solicitud->fondosPoliticos = $datosPersonales['cboFondosPoliticos']['value'];
    $solicitud->remuneracionMensual = $datosPersonales['txtRemuneracionMensual']['value'];
    $solicitud->detalleRemuneracion = $datosPersonales['txtDetalleRemuneracion']['value'];
    $solicitud->otrosIngresos = $datosPersonales['txtOtrosIngresos']['value'];
    $solicitud->detalleOtrosIngresos = $datosPersonales['txtDetalleOtrosIngresos']['value'];

    require_once 'Models/CargoPolitico.php';

    $antecedente = new AntecedentePep();

    if ( $antecedentesPep != null )
    {
        $antecedente->id = $antecedentesPep['idAntecedentes'];
        $antecedente->entidad = $antecedentesPep['txtEntidad']['value'];
        $antecedente->Cargo->id = (int) $antecedentesPep['cboCargoPepAntecedentes']['value'];
        $antecedente->nombre = $antecedentesPep['txtNombresAntecedentes']['value'];
        $antecedente->ejercicio = $antecedentesPep['txtEjecicioAntecedentes']['value'];
        $antecedente->inicioGestion = $antecedentesPep['txtInicioGestionAntecedentes']['value'];
        $antecedente->finGestion = $antecedentesPep['txtFinGestionAntecedentes']['value'];
        $antecedente->CargoReemplazante->id = (int) $antecedentesPep['cboCargoPoliticoReem']['value'];
        $solicitud->Antecedente = $antecedente;
    } else
    {
        $solicitud->Antecedente = $antecedente;
    }

    $solicitud->idUsuario = $_SESSION['index']->id;
    $solicitud->idAgencia = $_SESSION['index']->agenciaActual->id;
    $solicitud->ipOrdenador = generalObtenerIp();

    // TABLAS hIJAS
    $datosResidencias = $input['residencias'];

    $solicitud->otraNacionalidad = count($datosResidencias) > 0 ? 'S' : 'N' ;

    $datosPersonaRelacionada = $input['personaRelacionada'];
    $datosOtrasCuentas = $input['otrasCuentas'];

    $solicitud->cuentaDeTerceros = count( $datosOtrasCuentas ) > 0 ? 'S' : 'N';

    $datosSociedades = $input['sociedades'];
    $datosProductos = $input['productos'];

    $residenciasDB = ''; $personasRelacionadasDB = ''; $cuentasDB = ''; $sociedadesDB = ''; $productoDB = '';

    if ( count( $datosResidencias) > 0 )
    {
        $residenciaTemp = [];

        foreach ( $datosResidencias as $residencia )
        {
            array_push( $residenciaTemp, $residencia['id'].'%%%'.$residencia['idPais'].'%%%'. $residencia['residencia']);
        }
        $residenciasDB = implode('@@@', $residenciaTemp );
    }

    $solicitud->residencias = $residenciasDB;


    if ( count( $datosPersonaRelacionada) > 0 )
    {
        $personaTemp = [];

        foreach ( $datosPersonaRelacionada as $persona  )
        {
            $fechaInicio = $persona['inicioGestion'] ? date('Y-m-d', strtotime( $persona['inicioGestion'] )) : 'NULL';
            $fechaFinal = $persona['finGestion'] ? date('Y-m-d', strtotime( $persona['finGestion'] )) : 'NULL';

            array_push( $personaTemp, $persona['id'].'%%%'.$persona['idCargo'].'%%%'.$persona['idVinculo'].'%%%'.$persona['idParentesco'].'%%%'.$persona['nombre'].'%%%'. $fechaInicio.'%%%'. $fechaFinal );
        }
        $personasRelacionadasDB = implode('@@@', $personaTemp);
    }

    $solicitud->personasRelacionadas = $personasRelacionadasDB;

    if ( count( $datosSociedades) > 0 )
    {
        $sociedadesTemp = [];

        foreach( $datosSociedades as $sociedades )
        {
            array_push( $sociedadesTemp, $sociedades['id'].'%%%'.$sociedades['entidad'].'%%%'.$sociedades['porcentaje']);
        }
        $sociedadesDB = implode('@@@', $sociedadesTemp );
    }

    $solicitud->sociedades = $sociedadesDB;

    if ( count( $datosOtrasCuentas ) > 0 )
    {
        $cuentasTemp = [];

        foreach ( $datosOtrasCuentas as $otraCuenta )
        {
            array_push( $cuentasTemp, $otraCuenta['id'].'%%%'.$otraCuenta['nombre'].'%%%'.$otraCuenta['idParentesco'].'%%%'.$otraCuenta['motivo']);
        }
        $cuentasDB = implode('@@@', $cuentasTemp );
    }

    $solicitud->otrasCuentas = $cuentasDB;

    if ( count( $datosProductos ) > 0 )
    {
        $productosTemp = [];

        foreach ( $datosProductos as $producto )
        {
            array_push( $productosTemp, $producto['id'].'%%%'.$producto['idProducto'].'%%%'.$producto['proposito'] );
        }
        $productoDB = implode('@@@', $productosTemp );
    }

    $solicitud->productos = $productoDB;

    $idApartado =  (int) base64_decode( urldecode( $input['idApartado'] ));
    $tipoApartado = $input['tipoApartado'];

    $respuesta = $solicitud->guardarSolicitud( $idApartado, $tipoApartado );

}
else{
    $respuesta['respuesta'] = 'SESION';
}

echo json_encode( $respuesta );


