let tblCreditos;

window.onload = () => {
    const gettingCats = initDatable().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "CONTROLES",
                archivo: "controlesGetCats",
                solicitados: ['tiposDocumento', 'geograficos', 'empresas', 'prestamos']
            }).then(({
                         tiposDocumento: documentos,
                         datosGeograficos: geograficos,
                         prestamos,
                         empresas
                     }) => {
                $(".preloader").fadeIn("fast");
                const cboEmpresas = document.querySelectorAll('select.cboEmpresas');
                const cboTipoDePrestamo = document.querySelectorAll('select.cboTipoDePrestamo');

                const cboPais = document.querySelectorAll('select.cboPais');
                const cboTipoDocumento = document.querySelector('select.cboTipoDocumento');

                if (cboEmpresas.length > 0) {

                    empresas.forEach( empresa => {
                        configEmpresa.catEmpresas[ empresa.id ] = { ...empresa }
                    })
                    configEmpresa.cargarCbo()
                }

                if (cboTipoDePrestamo.length > 0) {
                    let opciones = [`<option value="" disabled selected >seleccione</option>`];

                    prestamos.forEach(prestamo => {
                        opciones.push(`<option value="${prestamo.id}">${prestamo.prestamo}</option>`);
                    });

                    cboTipoDePrestamo.forEach(cbo => {
                        cbo.innerHTML = opciones.join('');
                        if (cbo.className.includes('selectpicker')) {
                            $("#" + cbo.id).selectpicker("refresh");
                        }
                    });
                }

                if (cboTipoDocumento.length > 0) {
                    let opciones = ['<option selected value="" disabled>seleccione</option>'];

                    documentos.forEach(documento => {
                        tiposDocumento[documento.id] = {
                            ...documento
                        }
                        if (documento.tipoDocumento == 'DUI' || documento.tipoDocumento == 'Pasaporte') {
                            opciones.push(` <option value="${documento.id}">${documento.tipoDocumento}</option> `)
                        }
                    });

                    cboTipoDocumento.innerHTML = opciones.join('');
                    if (cboTipoDocumento.className.includes('selectpicker')) {
                        $(`#${cboTipoDocumento.id}`).selectpicker('refresh');
                    }
                }

                if (geograficos) {
                    datosGeograficos = {
                        ...geograficos
                    };

                    let opciones = ['<option selected value="" disabled>seleccione</option>'];

                    for (let key in geograficos) {
                        opciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
                    }

                    cboPais.forEach(cbo => {
                        cbo.innerHTML = opciones.join('');
                        if (cbo.className.includes('selectpicker')) {
                            $(`#${cbo.id}`).selectpicker('refresh');
                        }
                    })
                }
                resolve();
            }).catch(reject);
        });
    });

    gettingCats.then(() => {
        configCredito.init().then(() => {
            $(".preloader").fadeOut("fast");
        }).catch(generalMostrarError);
    }).catch(generalMostrarError);
}

function initDatable() {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblCreditos').length > 0) {
                tblCreditos = $('#tblCreditos').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                    },
                        {
                            targets: [8],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblCreditos.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configCredito = {
    id: 0,
    cnt: 0,
    creditos: {},
    tabCreditos: document.getElementById('listadoCreditos-tab'),
    tabFormulario: document.getElementById('addCredito-tab'),
    init: function () {
        return new Promise((resolve, reject) => {
            try {
                fetchActions.get({
                    modulo: "CONTROLES",
                    archivo: '/Creditos/controlesObtenerDatosCreditos',
                    params: {}
                }).then(({
                             creditos
                         }) => {
                    this.cnt = 0;
                    this.creditos = {};
                    tblCreditos.clear().draw();
                    creditos.forEach(credito => {
                        this.cnt++;
                        this.creditos[this.cnt] = {
                            ...credito
                        };
                        this.addRowTbl({
                            ...credito,
                            nFila: this.cnt
                        });
                    });
                    tblCreditos.columns.adjust().draw();
                    resolve();
                }).catch(reject);
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        if (tipoPermiso === 'ESCRITURA') {
            formActions.clean('frmCredito').then(() => {
                this.id = id;
                let leyendo = new Promise((resolve, reject) => {
                    if (this.id == 0) {
                        let boton = `<button class="input-group-text btn btn-outline-dark" type="submit" form="frmCredito">
                        <i class="fas fa-search"></i>
                    </button>`;
                        $("#txtCodigoClienteCredito").parent().find(".input-group-append").html(boton);
                        this.evalCredito();
                        document.getElementById("txtCodigoClienteCredito").readOnly = false;
                        resolve();
                    } else {
                        let tmp = {
                            ...this.creditos[this.id]
                        };
                        document.getElementById("txtCodigoClienteCredito").value = tmp.codigoCliente;
                        document.getElementById("frmCredito").submit();
                        document.getElementById("txtNumeroDePrestamo").value = tmp.numeroDePrestamo;
                        document.getElementById("cboEmpresas").value = tmp.Empresa.id;
                        $("#cboEmpresas").selectpicker("refresh");
                        document.getElementById("cboTipoDePrestamo").value = tmp.Prestamo.id;
                        $("#cboTipoDePrestamo").selectpicker("refresh");
                        document.getElementById("txtMonto").value = tmp.monto;

                        let campos = document.querySelectorAll('#divCredito .form-control');

                        campos = [...campos, ...document.querySelectorAll("#divCredito .form-select")];

                        campos.forEach(campo => {
                            if (campo.id) {
                                if (campo.type == 'select-one') {
                                    $("#" + campo.id).data("valorInicial", campo.options[campo.selectedIndex].text);
                                } else {
                                    $("#" + campo.id).data("valorInicial", campo.value);
                                }
                            }
                        });
                        resolve();
                    }
                });

                leyendo.then(() => {
                    this.tabFormulario.classList.remove('disabled');
                    $("#" + this.tabFormulario.id).trigger("click");
                    this.tabCreditos.classList.add("disabled");
                }).catch(generalMostrarError);
            });

        } else {
            toastr.error('Modo sólo lectura, no tienes permisos crear nuevos detalle de créditos');
        }
    },
    save: function () {
        formActions.validate('frmCredito').then(({
                                                     errores
                                                 }) => {
            if (errores == 0) {
                let {
                    txtCodigoClienteCredito,
                    txtNombres,
                    txtApellidos,
                    txtNumeroDePrestamo,
                    cboEmpresas,
                    cboTipoDePrestamo,
                    txtMonto
                } = formActions.getJSON('frmCredito');

                const datos = {
                    id: this.id > 0 ? (this.creditos[this.id] ? this.creditos[this.id].id : 0) : 0,
                    txtCodigoClienteCredito,
                    txtNumeroDePrestamo,
                    cboEmpresas,
                    cboTipoDePrestamo,
                    txtMonto,
                    idApartado,
                    tipoApartado,
                    logs: {}
                }
                if (this.id > 0) {
                    datos.logs = obtenerLogs();
                }

                fetchActions.set({
                    modulo: "CONTROLES",
                    archivo: 'Creditos/controlesGuardarCredito',
                    datos
                }).then((response) => {
                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'Datos guardado exitosamente',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(i => {
                                this.init().then(() => {
                                    configCredito.showPrincipal();
                                    if (cboEmpresas.value == 0 )
                                    {
                                        configEmpresa.getCatEmpresas()
                                    }
                                });
                            });
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
                             nFila,
                             fechaRegistro,
                             numeroDePrestamo,
                             nombresAsociado,
                             apellidosAsociado,
                             Empresa: {
                                 empresa
                             },
                             Prestamo: {
                                 prestamo
                             },
                             monto,
                             propietario,
                             Agencia: {
                                 agencia
                             }
                         }) {
        return new Promise((resolve, reject) => {
            try {
                let botonEditar = `<h6><span class="badge bg-primary">Modo solo lectura</span></h6>`;
                let botonEliminar = '';

                if (tipoPermiso == 'ESCRITURA' && propietario == 'S') {
                    botonEditar = `<button type="button" class="btnTbl" onclick='configCredito.edit(${nFila})'> <i class="fa fa-edit"></i></button>`;
                    botonEliminar = `<button type="button" class="btnTbl" onclick="configCredito.delete(${nFila});"> <i class="fa fa-trash"></i></button>`;
                } else if (tipoPermiso == 'ESCRITURA') {
                    botonEditar = `<button type="button" class="btnTbl" onclick='configCredito.edit(${nFila})'> <i class="fa fa-edit"></i></button>`;
                }

                tblCreditos.row.add([
                    nFila,
                    fechaRegistro,
                    agencia,
                    numeroDePrestamo,
                    nombresAsociado + ' ' + apellidosAsociado,
                    prestamo,
                    empresa,
                    monto,
                    '<div class="tblButtonContainer">' +
                    botonEditar + botonEliminar +
                    '</div>'
                ]).node().id = `trCredito${nFila}`;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (tmpId = 0) {// PENDIENTE DE TERMINAR POR JONATHAN
        Swal.fire({
            title: 'IMPORTANTE',
            text: '¿Estás seguro que quieres eliminar este registro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            cancelButtonColor: '#f54343',
            confirmButtonText: 'Eliminar'
        }).then((result) => {
            if (result.isConfirmed) {
                const id = this.creditos[tmpId].id;
                fetchActions.set({
                    modulo: 'CONTROLES',
                    archivo: 'Creditos/controlesEliminarCreditos',
                    datos: {
                        id,
                        idApartado,
                        tipoApartado
                    }
                }).then((response) => {
                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'Registro de crédito eliminado con exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(() => {
                                delete configCredito.creditos[tmpId];
                                tblCreditos.row(`#trCredito${tmpId}`).remove().draw();
                            });
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    cancel: function () {
        Swal.fire({
            title: 'IMPORTANTE',
            text: '¿Está seguro que quiere cancelar en registro de crédito?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            cancelButtonColor: '#f54343',
            confirmButtonText: `<i class="fa fa-check-circle"></i> SI`,
            cancelButtonText: `<i class="fa fa-times-circle"></i> NO`
        }).then((result) => {
            if (result.isConfirmed) {
                this.showPrincipal();
            }
        });
    },
    showPrincipal: function () {
        this.tabCreditos.classList.remove('disabled');
        $("#" + this.tabCreditos.id).trigger("click");
        this.tabFormulario.classList.add("disabled");
        document.getElementById("btnGuardar").classList.add('disabled');
        document.getElementById('frmCredito').action = 'javascript:codigoCliente.search();';


        configEmpresa.divCbo.style.display = 'block'
        configEmpresa.divTxt.style.display = 'none'
    },
    clean: function () {
        formActions.clean('frmCredito').then(() => {
            document.getElementById("frmCredito").action = 'javascript:codigoCliente.search();';
            document.getElementById("btnGuardar").classList.add("disabled");
            let campoCodigoCliente = document.getElementById("txtCodigoClienteCredito");
            campoCodigoCliente.readOnly = false;
            let boton = `<button class="input-group-text btn btn-outline-dark" type="submit" form="frmCredito">
                        <i class="fas fa-search"></i>
                    </button>`;
            $("#" + campoCodigoCliente.id).parent().find(".input-group-append").html(boton);
            configCredito.evalCredito();
        });
    },
    evalCredito: function (newState = true) {
        const inputs = document.querySelectorAll('div#divCredito input.form-control');
        const cbos = document.querySelectorAll('div#divCredito select.form-control');

        cbos.forEach(cbo => {
            if (cbo.id) {
                cbo.disabled = newState;
                if (cbo.className.includes('selectpicker')) {
                    $(`#${cbo.id}`).selectpicker('refresh');
                }
            }
        });
        inputs.forEach(input => {
            if (input.id) {
                input.readOnly = newState;
            }
        });
    }
}

const codigoCliente = {
    search: function () {
        formActions.validate('frmCredito').then(({
                                                     errores
                                                 }) => {
            if (errores == 0) {
                const txtCodigoCliente = document.getElementById('txtCodigoClienteCredito');

                generalVerificarCodigoCliente(txtCodigoCliente.value)
                    .then(data => {
                        $('.preloader').fadeOut('fast');
                        if (data.respuesta) {

                            txtCodigoCliente.readOnly = true;

                            let {
                                nombresAsociado,
                                apellidosAsociado,
                                tipoDocumentoPrimario,
                                numeroDocumentoPrimario,
                                numeroDocumentoSecundario,
                                agencia,
                                telefonoFijo,
                                telefonoMovil,
                                pais,
                                departamento,
                                municipio,
                                direccionCompleta

                            } = data.asociado;

                            document.getElementById('txtNombres').value = nombresAsociado;
                            document.getElementById('txtApellidos').value = apellidosAsociado;

                            document.getElementById('txtNit').value = numeroDocumentoSecundario;
                            document.getElementById('txtTelefonoFijo').value = telefonoFijo;
                            document.getElementById('txtTelefonoMovil').value = telefonoMovil;
                            document.getElementById('txtAgenciaAfiliacionCredito').value = agencia;

                            document.getElementById('cboPais').value = pais;
                            $("#cboPais").trigger("change");
                            document.getElementById('cboDepartamento').value = departamento;
                            $("#cboDepartamento").trigger("change");
                            document.getElementById('cboMunicipio').value = municipio;

                            document.getElementById('txtDireccionCompleta').value = direccionCompleta;

                            document.getElementById('cboTipoDocumento').value = tipoDocumentoPrimario
                            document.getElementById('txtDocumentoPrimario').value = numeroDocumentoPrimario;
                            document.getElementById('txtDocumentoPrimario').readOnly = true;


                            let boton = `<button class="input-group-text btn btn-outline-dark" type="button" onclick="configCredito.clean();">
                        <i class="fas fa-trash"></i>
                    </button>`;
                            $("#txtCodigoClienteCredito").parent().find(".input-group-append").html(boton);
                            if (configCredito.id > 0) {
                                $("#txtCodigoClienteCredito").parent().find(".input-group-append").html('');
                            }
                            document.getElementById("btnGuardar").classList.remove('disabled');
                            document.getElementById('frmCredito').action = 'javascript:configCredito.save();';
                            configCredito.evalCredito(false);
                        } else if (!data.respuesta && data.asociado.length === 0) {
                            txtCodigoCliente.readOnly = false;
                            Swal.fire({
                                title: 'INFORMACION',
                                text: `No existe un asociado con el código ${txtCodigoCliente.value} en la base de datos. ¿Desea agregarlo?`,
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#46b1c3',
                                cancelButtonColor: '#f54343',
                                confirmButtonText: `<i class="fa fa-check-circle"></i> SI`,
                                cancelButtonText: `<i class="fa fa-times-circle"></i> NO`
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.href = `?page=controlesAsociados&mod=${encodeURIComponent(btoa('CONTROLES'))}`;
                                }
                            });
                        }
                    });
            }
        }).catch(generalMostrarError);
    },
}

const configEmpresa = {
    catEmpresas: {},
    divCbo: document.getElementById('divCbo'),
    divTxt: document.getElementById('divTxt'),
    txtEmpresa: document.getElementById('txtEmpresa'),
    cboEmpresas: document.getElementById('cboEmpresas'),
    form: document.getElementById('frmCredito'),
    btnGuardar: document.getElementById('btnGuardar'),
    estado: false,
    getCatEmpresas()
    {
      fetchActions.getCats({
          modulo: 'CONTROLES', archivo: 'controlesGetCats', solicitados: ['empresas']
      }).then( ( { empresas = [] } ) => {

          this.catEmpresas = {}

          empresas.forEach( empresa => {
              this.catEmpresas[ empresa.id ] = {...empresa }
          })

          this.cargarCbo()

      }).catch( generalMostrarError )
    },
    cargarCbo() {

        let opciones = [`<option value="" disabled selected>seleccione</option>`];
        opciones.push(`<option value="X" >AGREGAR</option>`)

        for ( const key in this.catEmpresas )
        {
            opciones.push(`<option value="${this.catEmpresas[key].id }">${ this.catEmpresas[key].empresa }</option>`);
        }

        this.cboEmpresas.innerHTML = opciones.join('');
        if (this.cboEmpresas.className.includes('selectpicker')) {
            $("#" + this.cboEmpresas.id).selectpicker("refresh");
        }
    },
    changeCboEmpresa(cbo) {

        this.form.classList.remove('was-validated')

        if (cbo.value === 'X') {
           this.btnGuardar.disabled = true
            this.estado = true
            this.txtEmpresa.value = ''
            this.divCbo.style.display = 'none'
            this.divTxt.style.display = 'block'

        } else {
            this.btnGuardar.disabled = false
            this.estado = false
        }
        this.txtEmpresa.required = this.estado
    },
    checkEmpresa() {

        if (this.txtEmpresa.value.length > 0) {

            let existe = false

            for (const key in this.catEmpresas )
            {
                if ( this.catEmpresas[key].empresa === this.txtEmpresa.value )
                {
                    existe = true
                    break
                }
            }

            if( !existe )
            {
                toastr.success('Empresa agregada exitosamente')
                this.divTxt.style.display = 'none'
                this.divCbo.style.display = 'block'

                this.catEmpresas[0] = { id: 0, empresa: this.txtEmpresa.value }
                this.cargarCbo();

                this.cboEmpresas.value = 0;
                $(`#${this.cboEmpresas.id}`).trigger('change')
                $("#" + this.cboEmpresas.id).selectpicker("refresh");
            } else
            {
                toastr.warning('La empresa ya existe en el catalogo')
            }
        } else {
            this.txtEmpresa.classList.remove('is-valid')
            this.txtEmpresa.classList.add('is-invalid')
            toastr.warning('¡Ingrese el nombre de la empresa!')
        }
        this.form.classList.remove('was-validated')
    }
}

const obtenerLogs = () => {

    let camposEditados = [];
    let campos = document.querySelectorAll('#divCredito .form-control');

    campos = [...campos, ...document.querySelectorAll("#divCredito .form-select")];

    campos.forEach(campo => {
        if (campo.id) {
            let valorActual = campo.type == 'select-one' ? campo.options[campo.selectedIndex].text : $("#" + campo.id).val();

            if ($("#" + campo.id).data("valorInicial")) {
                let valorAnterior = $("#" + campo.id).data("valorInicial");

                if (valorActual != valorAnterior) {
                    let label = $("#" + campo.id).parent().parent().find("label").length > 0 ? $("#" + campo.id).parent().parent().find("label:eq(0)").text() : ($("#" + campo.id).parent().parent().parent().find("label").length > 0 ? $("#" + campo.id).parent().parent().parent().find("label:eq(0)").text() : 'undefined');
                    label = (label.replace("*", "")).trim();
                    camposEditados.push({
                        campo: label,
                        valorAnterior,
                        valorNuevo: valorActual
                    })
                }
            }
        }
    });
    return camposEditados;
}