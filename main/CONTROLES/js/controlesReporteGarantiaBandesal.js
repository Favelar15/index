
let tblGarantiaBandesal
const modulo = 'CONTROLES'

window.onload = () => {

    const cboAgencia = document.getElementById('cboAgencia')

    const inicializando = initDataTable().then( () => {
        return new Promise ( ( resolve, reject ) => {
            try {

                fetchActions.getCats({
                    modulo, archivo: 'controlesGetCats', solicitados: ['agenciasAsignadas']
                }).then(({agenciasAsignadas = []}) => {

                    let opciones = [`<option value="all" selected>Todas las agencias</option>`]

                    agenciasAsignadas.forEach(agencia => {
                        if (agencia.id != 700) {
                            opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                        }
                    })
                    cboAgencia.innerHTML = opciones.join('')
                    $(`#${cboAgencia.id}`).selectpicker('refresh')

                    resolve()
                }).catch(reject)

            } catch (e) {
                reject(e.message)
            }
        })
    })

    inicializando.then( () => {
        $('.preloader').fadeOut('fast')
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblGarantiaBandesal').length > 0) {
                tblGarantiaBandesal = $('#tblGarantiaBandesal').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                        width: '8%',
                        className: "text-center",
                    }, {
                        targets: [1, 2, 3, 5,6,7],
                        className: "text-center"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de gerantía bandesal';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblGarantiaBandesal.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configGarantiaBandesal = {
    contador: 0,
    garantiasBandesal: {},
    btnExcel: document.getElementById('btnExcel'),
    cboAgencia: document.getElementById('cboAgencia'),
    txtInicio: document.getElementById('txtInicio'),
    txtFin: document.getElementById('txtFin'),
    generar()
    {
        formActions.validate('')
            .then( ( { errores } ) => {

                if ( errores == 0)
                {
                    fetchActions.set({
                        modulo, archivo: 'GarantiaBandesal/controlesRptGarantiasBandesal', datos: {
                            ...formActions.getJSON('formGarantiasBandesal'),
                            accion: 'GENERAR'
                        }
                    }).then( ( { garantiasBandesal = [] } ) => {

                        this.contador = 0
                        this.garantiasBandesal = {}

                        if ( garantiasBandesal.length > 0 )
                        {
                            this.btnExcel.classList.remove('disabled')
                            this.txtInicio.disabled = true
                            this.txtFin.disabled = true
                            this.cboAgencia.disabled = true
                            $(`#cboAgencia`).selectpicker('refresh')

                        } else
                        {
                            toastr.warning(`No hay datos para mostrar`)
                        }

                        garantiasBandesal.forEach( garantia => {
                            this.contador++
                            this.garantiasBandesal[this.contador] = { ...garantia }
                            this.agregarFila( {
                                contador: this.contador, ... garantia
                            })
                        })

                        tblGarantiaBandesal.columns.adjust().draw()
                    })
                }

            }).catch( generalMostrarError )
    },
    exportar()
    {
        if ( Object.keys(this.garantiasBandesal).length > 0 )
        {
            fetchActions.set({
                modulo, archivo: 'GarantiaBandesal/controlesRptGarantiasBandesal', datos: {
                    ...formActions.getJSON('formGarantiasBandesal'),
                    accion: 'EXPORTAR', garantiasBandesal: {...this.garantiasBandesal}
                }
            }).then( response => {

                switch (response.respuesta.trim()) {
                    case 'EXITO':
                        window.open('./main/CONTROLES/docs/garantiasBandesal/Excel/' + response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }
            }).catch( generalMostrarError )

        } else
        {
            toastr.warning(`No hay datos para exportar`)
        }
    },
    agregarFila( { contador, fechaDesembolso, numeroPrestamo, codigoCliente, nombreAsociados, idAgencia, codigo, tipoPrestamo } )
    {
        tblGarantiaBandesal.row.add([
            contador, fechaDesembolso, numeroPrestamo, codigoCliente, nombreAsociados, idAgencia, codigo, tipoPrestamo
        ]).node().id = `trGarantia${contador}`
    },
    cambiarFecha()
    {
        $("#txtFin").datepicker('destroy')

        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        this.txtFin.value = ''
        $("#txtFin").datepicker('update')
    },
    limpiar()
    {
        formActions.clean('formGarantiasBandesal')
            .then( () => {

                tblGarantiaBandesal.clear().draw()
                this.garantiasBandesal = {}
                this.contador = 0

                this.btnExcel.classList.add('disabled')
                this.txtInicio.disabled = false
                this.txtFin.disabled = false
                this.cboAgencia.disabled = false
                this.cboAgencia.value = 'all'
                $(`#cboAgencia`).selectpicker('refresh')
            })
    }
}
