
let tblInventario
const modulo = 'CONTROLES'

window.onload = () => {

    const cboAgencia = document.getElementById('cboAgencia')

    const inicializando = initDatatble().then( () => {
        return new Promise( (resolve, reject ) => {

            fetchActions.get({
                modulo,
                archivo: 'PYP/controlesObtenerInventarios',
                params: {}
            }).then( ( { inventarios = [] } ) => {

                let opciones = [`<option value="" selected disabled>Seleccione un inventario</option>`]

                inventarios.forEach( inventario => {
                    opciones.push(`<option value="${inventario.id}">${inventario.Agencia.agencia}</option>`)
                })

                cboAgencia.innerHTML = opciones.join()
                $(`#${cboAgencia.id}`).selectpicker('refresh')

                resolve()

            }).catch( reject )
        })
    })

    inicializando.then( () => {
        $(`.preloader`).fadeOut('fast')
    })
}


const initDatatble = () => {
    return new Promise ( ( resolve, reject ) => {
        try {
            if ($('#tblInventario').length > 0) {
                tblInventario = $('#tblInventario').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": false,
                    "ordering": true,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            'className': 'text-center',
                            'width': '5%'
                        },
                        {
                            "targets": [1, 2],
                            "className": "text-center"
                        },
                        {
                            targets: [3,4],
                            className: 'text-center',
                            width: '20%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Logs de inventario';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblInventario.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject( e.message )
        }
    })
}

const configReportePYP = {
    contador: 0,
    movimientos: {},
    txtInicio: document.getElementById('txtInicio'),
    obtenerMovimientos()
    {
        formActions.validate('formPYP')
            .then( ( { errores } ) => {

                if ( errores == 0 )
                {
                    fetchActions.set({
                        modulo, archivo: 'PYP/controlesReportePYP', datos: {
                            ...formActions.getJSON('formPYP'),
                            accion: 'GENERAR'
                        }
                    }).then( ( movimientos = {} ) => {

                        tblInventario.clear()
                        this.contador = 0
                        this.movimientos = { }

                        if (Object.keys( movimientos ).length > 0 )
                        {
                            this.resetFormulario(true)
                            for ( const key in movimientos )
                            {
                                const tempMovimiento = movimientos[key]

                                for ( const i in tempMovimiento )
                                {
                                    tempMovimiento[i].forEach( item => {
                                        this.contador++
                                        this.movimientos[this.contador ] = { ...item }
                                        this.agregarFila({
                                            contador: this.contador, ...item
                                        })
                                    })
                                }
                            }
                        } else {
                            toastr.warning('No hay datos para mostrar')
                        }

                        tblInventario.columns.adjust().draw()
                    }).catch( generalMostrarError )
                }
            })
    },
    agregarFila( { contador, Accion: { accion }, Agencia: { agencia }, stockPines, stockPlastico } )
    {
        tblInventario.row.add([
            contador, agencia, accion, stockPines, stockPlastico
        ]).node().id = `tr${contador}`
    },
    limpiar()
    {
        formActions.clean('formPYP')
            .then( () => {

                this.resetFormulario(false)

                this.contador = 0
                this.movimientos = {}
                tblInventario.clear().draw()
            })
    },
    resetFormulario( estado )
    {
        let inputs = document.querySelectorAll('form#formPYP .form-control')

        if (estado)
        {
            document.getElementById('btnExcel').classList.remove('disabled')
        }  else {
            document.getElementById('btnExcel').classList.add('disabled')
        }

        inputs.forEach( input => {

            input.disabled = estado

            if( input.className.includes('selectpicker'))
            {
                $(`#${input.id}`).selectpicker('refresh')
            }
        })
    },
    exportarDatos()
    {
        if ( Object.keys(this.movimientos).length > 0 )
        {
            fetchActions.set({
                modulo, archivo: 'PYP/controlesReportePYP', datos : {
                    ...formActions.getJSON('formPYP'),
                    movimientos : this.movimientos, accion: 'EXPORTAR'
                }
            }).then( response => {

                switch (response.respuesta.trim()) {
                    case 'EXITO':
                        window.open('./main/CONTROLES/docs/PYP/Excel/' + response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }
            }).catch( generalMostrarError )
        } else {
            toastr.warning('No hay datos para mostrar')
        }
    },
    cambiarFecha()
    {
        $("#txtFin").datepicker('destroy')

        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        document.getElementById('txtFin').value = ''
        $("#txtFin").datepicker('update')
    }
}
