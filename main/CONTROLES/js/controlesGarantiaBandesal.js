const modulo = 'CONTROLES'
let tblProyectos, tblGarantias

window.onload = () => {

    const cboTipoDocumento = document.getElementById('cboTipoDocumento')
    const cboPais = document.getElementById('cboPais')
    const cboAgencia = document.getElementById('cboAgencia')
    const cboDepartamentoProyecto = document.querySelector('form#formProyectos select.cboDepartamentoP')
    const cboLineaPrestamo = document.getElementById('cboLineaPrestamo')
    const cboPrograma = document.getElementById('cboPrograma')
    const cboTipoGarantia = document.getElementById('cboTipoGarantia')

    const inicializando = initDataTable().then(() => {
        return new Promise((resolve, reject) => {
            try {
                fetchActions.getCats({
                    modulo,
                    archivo: 'controlesGetCats',
                    solicitados: ['tiposDocumento', 'geograficos', 'agenciasAsignadas', 'prestamos', 'programasBandesal', 'tiposGarantias']
                }).then(({
                             tiposDocumento: documentos,
                             datosGeograficos: geograficos = [],
                             agenciasAsignadas = [],
                             prestamos = [],
                             programasBandesal = [],
                             tiposGarantias = []
                         }) => {

                    if (cboTipoDocumento) {
                        let opciones = [`<option selected value="" disabled>Seleccione</option>`]

                        documentos.forEach(documento => {
                            tiposDocumento[documento.id] = {...documento}
                            if (documento.tipoDocumento == 'DUI' || documento.tipoDocumento == 'Pasaporte') {
                                opciones.push(` <option value="${documento.id}">${documento.tipoDocumento}</option> `)
                            }
                        })

                        cboTipoDocumento.innerHTML = opciones.join('')
                        $(`#${cboTipoDocumento.id}`).selectpicker('refresh')
                    }

                    if (cboPais) {
                        datosGeograficos = {
                            ...geograficos
                        }
                        let opciones = ['<option selected value="" disabled>seleccione</option>']

                        for (let key in geograficos) {
                            opciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
                        }

                        cboPais.innerHTML = opciones.join('')
                        $(`#${cboPais.id}`).selectpicker('refresh')
                    }

                    if (cboAgencia) {
                        let opciones = [`<option selected disabled value="">seleccione</option>`]

                        agenciasAsignadas.forEach(agencia => {
                            if (agencia.id != 700) {
                                configAsesor.catAgencias[agencia.id] = {...agencia}
                                opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                            }
                        })

                        cboAgencia.innerHTML = opciones.join('')
                        $(`#${cboAgencia.id}`).selectpicker('refresh')
                    }

                    if (cboLineaPrestamo) {
                        let opciones = [`<option value="" disabled selected>seleccione</option>`]
                        prestamos.forEach(prestamo => {
                            opciones.push(`<option value="${prestamo.id}">${prestamo.prestamo}</option>`)
                        })

                        cboLineaPrestamo.innerHTML = opciones.join()
                        $(`#${cboLineaPrestamo.id}`).selectpicker('refresh')
                    }

                    if (cboPrograma) {
                        let opciones = ['<option value="" selected disabled>seleccione</option>']

                        programasBandesal.forEach(programa => {
                            opciones.push(`<option value="${programa.id}">${programa.programa}</option>`)
                        })

                        cboPrograma.innerHTML = opciones.join()
                        $(`#${cboPrograma.id}`).selectpicker('refresh')
                    }

                    if ( cboTipoGarantia )
                    {
                        let opciones = ['<option value="" selected disabled>seleccione</option>']

                        tiposGarantias.forEach( tipo => {
                            opciones.push(`<option value="${tipo.id}">${tipo.garantia}</option>`)
                        })

                        cboTipoGarantia.innerHTML = opciones.join()
                        $(`#${cboTipoGarantia.id}`).selectpicker('refresh')
                    }
                    resolve()

                }).catch(reject)
            } catch (e) {
                reject(e)
            }
        })
    }).catch(generalMostrarError)

    inicializando.then(() => {

        configGarantia.cargarDatos()

        if (Object.keys(datosGeograficos).length > 0) {
            const departamentos = datosGeograficos[1].departamentos
            let opciones = [`<option selected disabled value="">seleccione</option>`]

            for (const key in departamentos) {
                opciones.push(`<option value="${departamentos[key].id}">${departamentos[key].nombreDepartamento}</option>`)
            }
            cboDepartamentoProyecto.innerHTML = opciones.join('')
            $(`#${cboDepartamentoProyecto.id}`).selectpicker('refresh')
        }

    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {

            if ($('#tblGarantias').length > 0) {
                tblGarantias = $('#tblGarantias').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0, 1],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [6],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblGarantias.columns.adjust().draw();
            }

            if ($('#tblProyectos').length > 0) {
                tblProyectos = $('#tblProyectos').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [5],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblProyectos.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const configGarantia = {
    id: 0,
    contador: 0,
    idFila: 0,
    garantias: { },
    tabPagadurias: document.getElementById('garantias-tab'),
    tabFomulario: document.getElementById('formularioGarantia-tab'),
    cargarDatos()
    {
        tblGarantias.clear()
        this.garantias = {}
        this.contador = 0

        fetchActions.get({
            modulo, archivo: 'GarantiaBandesal/controlesObtenerGarantiasBandesal', params:{
                idApartado, tipoApartado
            }
        }).then( ( { garantiasBandesal = [] } ) => {

            garantiasBandesal.forEach( garantia => {
                this.contador++
                this.garantias[this.contador] = { ...garantia }
                this.agregarFila({ contador: this.contador, ...this.garantias[this.contador]})
            })
            tblGarantias.columns.adjust().draw()

        }).catch( generalMostrarError )

    },
    agregarFila( {contador, fechaDesembolso, numeroPrestamo, codigoCliente, nombreAsociado, agencia } )
    {
        let editar = ''
        let eliminar = ''

        if (tipoPermiso == 'ESCRITURA')
        {
            editar = `<button type="button"class="btnTbl" onclick="configGarantia.editar(${contador})"><i class="fas fa-edit"></i></button>`
            eliminar = `<button type="button" class="btnTbl" onclick="configGarantia.eliminar(${contador})"><i class="fas fa-trash-alt"></i></button>`
        }
        tblGarantias.row.add([
            contador, fechaDesembolso, numeroPrestamo, codigoCliente, nombreAsociado, agencia,
            `<div class='tblButtonContainer'> ${editar} ${eliminar} </div>`
        ]).node().id = `trGarantia${contador}`
    },
    irFormulario() {
        this.limpiar().then(() => {

            this.id = 0
            asociado.habilitarFormulario(true)

            this.tabPagadurias.classList.add('disabled')
            this.tabFomulario.classList.remove('disabled')
            $(`#${this.tabFomulario.id}`).trigger('click')
        }).catch(generalMostrarError)
    },
    limpiar() {
        return new Promise((resolve, reject) => {
            const limpiandoFormAsociado = formActions.clean('formAsociado')
            const limpiandoformGarantia = formActions.clean('formGarantia')

            Promise.all([
                limpiandoFormAsociado, limpiandoformGarantia
            ]).then( () => {

                this.id = 0

                proyecto.proyectos = {}
                proyecto.contador = 0
                tblProyectos.clear().draw()

                asociado.habilitarFormulario(true)
                asociado.txtCodigoCliente.readOnly = false
                asociado.btnBuscar.classList.remove('disabled')
                asociado.btnGuardar.classList.add('disabled')
                asociado.btnProyecto.classList.add('disabled')

                resolve()
            }).catch(reject)
        })
    },
    salir()
    {
        Swal.fire({
            title: 'INFORMACIÓN',
            html: `¿Quiere salir del formulario?`,
            icon: 'warning',
            showCloseButton: true,
            showCancelButton: true,
            showDenyButton: true,
            confirmButtonColor: '#46b1c3',
            confirmButtonText: `<i class="fa fa-check-circle"></i> Salir`,
            denyButtonText: `<i class="fa fa-times-circle"></i> Limpiar`,
            cancelButtonText: `<i class="fa fa-times-circle"></i> Cancelar`
        }).then((result) => {
            if (result.isConfirmed) {
                this.limpiar().then( () => {

                    //this.cargarDatos()

                    this.tabPagadurias.classList.remove('disabled')
                    this.tabFomulario.classList.add('disabled')
                    $(`#${this.tabPagadurias.id}`).trigger('click')
                })
            } else if (result.isDenied)
            {
                this.limpiar().then()
            }
        });
    },
    guardarGarantia() {
        formActions.validate('formAsociado')
            .then(({errores}) => {

                if (errores == 0) {
                    if (formActions.manualValidate('formGarantia')) {
                        if (Object.keys(proyecto.proyectos).length > 0) {
                            fetchActions.set({
                                modulo, archivo: 'GarantiaBandesal/controlesGuardarGarantia', datos: {
                                    id: this.id,
                                    idApartado, tipoApartado, id: this.id, ...formActions.getJSON('formGarantia'),
                                    codigoCliente: asociado.txtCodigoCliente.value,
                                    proyectos: { ...proyecto.proyectos }, logs: obtenerLogs()
                                }
                            }).then(response => {

                                switch (response.respuesta.trim()) {
                                    case 'EXITO':

                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: '¡Bien hecho!',
                                            text: 'Los datos de la garantía fueron almacenados con exito',
                                            showConfirmButton: false,
                                            timer: 1500
                                        }).then(() => {
                                            this.limpiar().then( () => {

                                                this.cargarDatos()

                                                this.tabPagadurias.classList.remove('disabled')
                                                this.tabFomulario.classList.add('disabled')
                                                $(`#${this.tabPagadurias.id}`).trigger('click')
                                            })
                                        })
                                        break;
                                    default:
                                        generalMostrarError(response);
                                        break;
                                }

                            }).catch(generalMostrarError)
                        } else {
                            document.getElementById('tblProyectos').scrollIntoView({
                                behavior: "smooth",
                                block: "center",
                                inline: "end"
                            })
                            toastr.warning('Agrege la descripción del proyecto')
                        }
                    }
                }
            })
    },
    editar( idFila )
    {
        if ( tipoPermiso == 'ESCRITURA')
        {
            const idGarantia = this.garantias[idFila].id

            this.irFormulario()
            this.idFila = idFila

            fetchActions.get({
                modulo, archivo: 'GarantiaBandesal/controlesObtenerGarantiasBandesal', params: {
                    idGarantia
                }
            }).then( ({ garantiasBandesal }) => {

                asociado.txtCodigoCliente.value = this.garantias[this.idFila].codigoCliente
                asociado.buscar()

                this.id = garantiasBandesal.id
                document.getElementById('txtComprobante').value = garantiasBandesal.comprobante
                document.getElementById('txtNumeroPrestamo').value = garantiasBandesal.numeroPrestamo
                document.getElementById('txtMonto').value = garantiasBandesal.monto
                document.getElementById('txtPorcentajeGarantizado').value = garantiasBandesal.porcentajeGarantizado
                document.getElementById('txtMontoGarantizado').value = garantiasBandesal.montoGarantizado
                document.getElementById('txtMontoAdicional').value = garantiasBandesal.montoAdicional
                document.getElementById('txtFechaDesembolso').value = garantiasBandesal.fechaDesembolso
                $('#txtFechaDesembolso').datepicker('update')

                document.getElementById('txtFechaVencimiento').value = garantiasBandesal.fechaDesembolso
                $('#txtFechaVencimiento').datepicker('update')

                document.getElementById('txtPorcentajeDescontado').value = garantiasBandesal.porcentajeDescontado
                document.getElementById('txtValorDescontado').value = garantiasBandesal.valorDescontado
                document.getElementById('txtAñosExperiencia').value = garantiasBandesal.añosDeExperiencia
                document.getElementById('cboTipoGarantia').value = garantiasBandesal.idTipoGarantia
                document.getElementById('cboPrograma').value = garantiasBandesal.idPrograma
                document.getElementById('cboLineaPrestamo').value = garantiasBandesal.idLineaPrestamo
                document.getElementById('cboTipoPrestamo').value = garantiasBandesal.tipoPrestamo
                $('#cboTipoPrestamo').change()
                document.getElementById('txtFechaVencimientoRotativa').value = garantiasBandesal.vencimientoLineaRotativa
                $('#txtFechaVencimientoRotativa').datepicker('update')

                document.getElementById('cboEsProgara').value = garantiasBandesal.tipoPrograma
                document.getElementById('txtDiasAtrasados').value = garantiasBandesal.diasAtrasados
                document.getElementById('txtRiesgo').value = garantiasBandesal.clasificacionRiesgo

                document.getElementById('cboAgencia').value = garantiasBandesal.idAgencia
                $(`#cboAgencia`).trigger('change')

                document.getElementById('cboAsesor').value = garantiasBandesal.idAsesor

                const selectsPicker = document.querySelectorAll('form#formGarantia select.selectpicker')

                selectsPicker.forEach( cbo => {
                    $(`#${cbo.id}`).selectpicker('refresh')
                })

                if (garantiasBandesal.proyectos.length > 0 )
                {
                    tblProyectos.clear()
                    proyecto.contador = 0
                    proyecto.proyectos = {}

                    garantiasBandesal.proyectos.forEach( proyectoBansesal => {
                        proyecto.contador++
                        proyecto.proyectos[proyecto.contador] = { ...proyectoBansesal }
                        proyecto.agregarFila({ contador: proyecto.contador, ...proyectoBansesal })
                    })
                    tblProyectos.columns.adjust().draw()
                }

                let campos = document.querySelectorAll('form#formGarantia input.form-control')
                campos = [...campos, ...document.querySelectorAll('form#formGarantia select.form-control') ]

                campos.forEach( campo => {
                    if (campo.id) {
                        if (campo.type == 'select-one') {
                            $(`#${campo.id}`).data("valorInicial", campo.options[campo.selectedIndex].text);
                        } else {
                            $(`#${campo.id}`).data("valorInicial", campo.value);
                        }
                    }
                })

            }).catch( generalMostrarError )

        } else {
            toastr.warning(`Modo sólo lectura, no tienes permiso para modificar registros`)
        }
    },
    eliminar( idFila)
    {
        const garantia = this.garantias[idFila]

        Swal.fire({
            title: 'INFORMACIÓN',
            html: `¿Esta seguro de eliminar la garantía a nombre de: <strong>${garantia.nombreAsociado}</strong>, fecha del desembolso: <strong>${garantia.fechaDesembolso}</strong>?`,
            icon: 'warning',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            confirmButtonText: `<i class="fa fa-check-circle"></i> Sí, eliminar`,
            cancelButtonText: `<i class="fa fa-times-circle"></i> Cancelar`,
            footer: `Fecha de registro: <strong> ${garantia.fechaRegistro}</strong>`
        }).then( result => {
            if (result.isConfirmed)
            {
                fetchActions.set({
                    modulo, archivo: 'GarantiaBandesal/controlesEliminarGarantia', datos: {
                        idApartado, tipoApartado, id: garantia.id
                    }
                }).then( response => {

                    switch (response.respuesta.trim()) {
                        case 'EXITO':

                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'Los datos de la garantía fueron almacenados con exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(() => {
                                delete this.garantias[idFila]
                                tblGarantias.row(`#trGarantia${idFila}`).remove().draw(false)
                            })
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }

                })
            }
        })
    }
}

const asociado = {
    txtCodigoCliente: document.getElementById('txtCodigoCliente'),
    btnGuardar: document.getElementById('btnGuardar'),
    btnBuscar: document.getElementById('btnBuscarAscociado'),
    btnProyecto: document.getElementById('btnAgregarProyecto'),
    cbos: document.querySelectorAll('form#formGarantia select.selectpicker'),
    inputs: document.querySelectorAll('form#formGarantia input.form-control'),
    buscar() {
        formActions.validate('formDatosAsociado')
            .then(({errores}) => {

                if (errores == 0) {
                    generalVerificarCodigoCliente(this.txtCodigoCliente.value)
                        .then(data => {
                            if (data.respuesta) {

                                this.txtCodigoCliente.readOnly = true;
                                this.btnBuscar.classList.add('disabled');

                                let {
                                    nombresAsociado,
                                    apellidosAsociado,
                                    tipoDocumentoPrimario,
                                    numeroDocumentoPrimario,
                                    numeroDocumentoSecundario,
                                    agencia,
                                    telefonoFijo,
                                    telefonoMovil,
                                    pais,
                                    departamento,
                                    municipio,
                                    direccionCompleta
                                } = data.asociado;


                                document.getElementById('txtNombres').value = nombresAsociado;
                                document.getElementById('txtApellidos').value = apellidosAsociado;
                                document.getElementById('txtTelefonoFijo').value = telefonoFijo;
                                document.getElementById('txtTelefonoMovil').value = telefonoMovil;
                                document.getElementById('cboTipoDocumento').value = tipoDocumentoPrimario
                                document.getElementById('txtDocumentoPrimario').value = numeroDocumentoPrimario;
                                document.getElementById('txtAgenciaAfiliacion').value = agencia;
                                document.getElementById('txtNit').value = numeroDocumentoSecundario;

                                document.getElementById('cboPais').value = pais;
                                $("#cboPais").trigger("change");
                                document.getElementById('cboDepartamento').value = departamento;
                                $("#cboDepartamento").trigger("change");
                                document.getElementById('cboMunicipio').value = municipio;

                                document.getElementById('txtDireccionCompleta').value = direccionCompleta;

                                const selects = document.querySelectorAll('form#formAsociado select.selectpicker')

                                selects.forEach(cbo => {
                                    $(`#${cbo.id}`).selectpicker('refresh')
                                })

                                if ( numeroDocumentoSecundario )
                                {
                                    this.btnGuardar.classList.remove('disabled')
                                    this.btnProyecto.classList.remove('disabled');
                                    this.habilitarFormulario(false)
                                } else
                                {
                                    Swal.fire({
                                        title: 'INFORMACIÓN',
                                        html: `Número de NIT no definido para el asociado ${nombresAsociado} ${apellidosAsociado}, ¿Desea actualizar sus datos?`,
                                        icon: 'warning',
                                        showCloseButton: true,
                                        showCancelButton: true,
                                        confirmButtonColor: '#46b1c3',
                                        confirmButtonText: `<i class="fa fa-thumbs-up"></i> SI`,
                                        cancelButtonText: `<i class="fa fa-thumbs-down"></i> NO`,
                                        allowOutsideClick: false
                                    }).then( result => {
                                        if ( result.isConfirmed )
                                        {
                                            window.location = `?page=controlesActualizacionAsociado&mod=${encodeURIComponent(btoa('CONTROLES'))}`
                                        } else
                                        {
                                            $(`#modal-infoNIT`).modal('show')
                                        }
                                    })
                                }

                            } else if (!data.respuesta) {
                                Swal.fire({
                                    title: 'INFORMACION',
                                    html: `No existe un asociado con el código <strong>${this.txtCodigoCliente.value}</strong> en la base de datos. ¿Desea agregarlo?`,
                                    icon: 'warning',
                                    showCloseButton: true,
                                    showCancelButton: true,
                                    confirmButtonColor: '#46b1c3',
                                    confirmButtonText: `<i class="fa fa-check-circle"></i> SI`,
                                    cancelButtonText: `<i class="fa fa-times-circle"></i> NO`
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.href = `?page=controlesAsociados&mod=${encodeURIComponent(btoa('CONTROLES'))}`;
                                    }
                                });
                            }
                        }).catch(generalMostrarError)
                }

            }).catch(generalMostrarError)
    },
    habilitarFormulario(estado = true) {
        this.cbos.forEach(cbo => {
            cbo.disabled = estado
            $(`#${cbo.id}`).selectpicker('refresh')
        })

        if (estado) {
            this.txtCodigoCliente.readOnly = false
            this.btnBuscar.classList.remove('disabled');
            this.btnGuardar.classList.add('disabled')
            this.btnProyecto.classList.add('disabled')
        }

        this.inputs.forEach(input => {
            if ( input.id === 'txtFechaDesembolso' || input.id == 'txtFechaVencimiento' )
            {
                input.disabled = estado
            } else {
                if (!estado)
                {
                    if (input.id != 'txtMontoGarantizado' && input.id != 'txtMontoAdicional' && input.id != 'txtRiesgo' && input.id != 'txtValorDescontado') {
                        input.readOnly = estado
                    }
                } else {
                    input.readOnly = estado
                }
            }
        })
    }
}

const proyecto = {
    idFila: 0,
    contador: 0,
    proyectos: {},
    cboDep: document.getElementById('cboDepartamentoP'),
    cboMunicipio: document.getElementById('cboMunicipioP'),
    txtDireccion: document.getElementById('txtDireccionP'),
    mostrarModal(idFila = 0) {
        this.idFila = idFila

        formActions.clean('formProyectos')
            .then(() => {

                if (this.idFila > 0) {
                    this.cboDep.value = this.proyectos[this.idFila].idDepartamento
                    $(`#${this.cboDep.id}`).trigger("change");
                    this.cboMunicipio.value = this.proyectos[this.idFila].idMunicipio
                    this.txtDireccion.value = this.proyectos[this.idFila].direccion

                    $(`#${this.cboDep.id}`).selectpicker('refresh')
                    $(`#${this.cboMunicipio.id}`).selectpicker('refresh')
                }

                $(`#modal-proyectos`).modal('show')

            }).catch(generalMostrarError)
    },
    cboDepartamento(cbo) {
        const departamentos = datosGeograficos[1].departamentos
        let municipios = []
        let opciones = [`<option value="" selected disabled>seleccione</option>`]

        if (cbo.value) {
            departamentos.forEach(departamento => {
                if (departamento.id == cbo.value) {
                    municipios = [...departamento.municipios]
                }
            })
        }

        municipios.forEach(municipio => {
            opciones.push(`<option value="${municipio.id}">${municipio.nombreMunicipio}</option>`)
        })

        this.cboMunicipio.innerHTML = opciones.join('')
        $(`#${this.cboMunicipio.id}`).selectpicker('refresh')
    },
    guardar() {
        formActions.validate('formProyectos')
            .then(({errores}) => {

                if (errores == 0) {
                    const {
                        txtDescripcion,
                        cboDepartamentoP,
                        cboMunicipioP,
                        txtDireccionP
                    } = formActions.getJSON('formProyectos')

                    let datos = {
                        descripcion: txtDescripcion.value,
                        idDepartamento: cboDepartamentoP.value, departamento: cboDepartamentoP.labelOption,
                        idMunicipio: cboMunicipioP.value, municipio: cboMunicipioP.labelOption,
                        direccion: txtDireccionP.value
                    }

                    if (this.idFila == 0) {
                        this.contador++
                        this.proyectos[this.contador] = {
                            id: 0, ...datos
                        }
                        this.agregarFila({contador: this.contador, ...this.proyectos[this.contador]})
                    } else {

                        this.proyectos[this.idFila] = {
                            ...this.proyectos[this.idFila],
                            ...datos
                        }
                        this.editar({contador: this.contador, ...this.proyectos[this.idFila]})
                    }

                    $('#modal-proyectos').modal('hide')
                    tblProyectos.columns.adjust().draw()
                }
            }).catch(generalMostrarError)
    },
    agregarFila({contador, descripcion, departamento, municipio, direccion}) {
        let editar = `<button type="button" class="btnTbl" onclick="proyecto.mostrarModal(${contador})"><i class="fas fa-edit"></i></button>`
        let eliminar = `<button type="button" class="btnTbl" onclick="proyecto.eliminar(${contador})"><i class="fas fa-trash"></i></button>`

        tblProyectos.row.add([
            contador, descripcion, departamento, municipio, direccion,
            `<div class="tblButtonContainer"> ${editar} ${eliminar}</div>`
        ]).node().id = `trProyecto${contador}`
    },
    editar({contador, descripcion, departamento, municipio, direccion}) {
        $(`#trProyecto${contador}`).find("td:eq(1)").html(descripcion)
        $(`#trProyecto${contador}`).find("td:eq(2)").html(departamento)
        $(`#trProyecto${contador}`).find("td:eq(3)").html(municipio)
        $(`#trProyecto${contador}`).find("td:eq(4)").html(direccion)

    },
    eliminar(idFila) {
        delete this.proyectos[idFila]
        tblProyectos.row(`#trProyecto${idFila}`).remove().draw(false)
    }
}

const configAsesor = {
    catAgencias: {},
    cboAsesor: document.getElementById('cboAsesor'),
    cboAgencia: document.getElementById('cboAgencia'),
    cargarAsesores() {

        const idAgencia = this.cboAgencia.value

        if (idAgencia) {
            const asesores = this.catAgencias[idAgencia].asesores

            let opciones = [`<option value="" selected disabled>seleccione</option>`]

            asesores.forEach(asesor => {
                opciones.push(`<option value="${asesor.id}">${asesor.nombres} ${asesor.apellidos}</option>`)
            })

            this.cboAsesor.innerHTML = opciones.join('')
            $(`#${this.cboAsesor.id}`).selectpicker('refresh')
        }
    }
}

const obtenerLogs = () => {

    let camposEditados = [];
    let campos = document.querySelectorAll('form#formGarantia input.form-control');

    campos = [...campos, ...document.querySelectorAll("form#formGarantia select.form-control")];

    campos.forEach(campo => {
        if (campo.id) {
            let valorActual = campo.type == 'select-one' ? campo.options[campo.selectedIndex].text : $("#" + campo.id).val();

            if ($(`#${campo.id}`).data("valorInicial")) {
                let valorAnterior = $(`#${campo.id}`).data("valorInicial");

                if (valorActual != valorAnterior) {
                    let label = $(`#${campo.id}`).parent().find("label").length > 0 ? $(`#${campo.id}`).parent().find("label:eq(0)").text() : ($(`#${campo.id}`).parent().parent().parent().find("label").length > 0 ? $(`#${campo.id}`).parent().parent().parent().find("label:eq(0)").text() : 'undefined');
                    label = (label.replace("*", "")).trim();
                    camposEditados.push({
                        campo: label,
                        valorAnterior,
                        valorNuevo: valorActual
                    })
                }
            }
        }
    });

    return camposEditados;
}

const configPorcentajeGarantizado = {
    monto: document.getElementById('txtMonto'),
    porcentajeGarantizado : document.getElementById('txtPorcentajeGarantizado'),
    montoGarantizado : document.getElementById('txtMontoGarantizado'),
    montoAdicional: document.getElementById('txtMontoAdicional'),
    porcentajeDescontado: document.getElementById('txtPorcentajeDescontado'),
    valorDescontado: document.getElementById('txtValorDescontado'),
    calcular()
    {

        this.montoGarantizado.value = ''
        this.montoAdicional.value = ''

        if ( this.monto.value && this.porcentajeGarantizado.value )
        {
            this.montoGarantizado.value = parseFloat( Number((this.monto.value * this.porcentajeGarantizado.value) / 100 ) ).toFixed(2)
            this.montoAdicional.value = parseFloat( Number( this.monto.value - this.montoGarantizado.value ) ).toFixed(2)
        }

        this.valorDescontado.value = ''

        if ( this.monto.value && this.porcentajeDescontado.value )
        {
            this.valorDescontado.value = parseFloat( Number((this.monto.value * this.porcentajeDescontado.value) / 100 ) ).toFixed(2)
        }
    }
}

const configTipoPrestamo = {
    tipoPrestamo : document.getElementById('cboTipoPrestamo'),
    vencimientoLineaRotativa: document.getElementById('txtFechaVencimientoRotativa'),
    isRotativa()
    {
        this.vencimientoLineaRotativa.value = ''
        this.vencimientoLineaRotativa.disabled = true

        if ( this.tipoPrestamo.value && this.tipoPrestamo.value == 'R')
        {
            this.vencimientoLineaRotativa.disabled = false
        }
        $('#txtFechaVencimientoRotativa').datepicker('update')
    }
}

const configRiesgo = {
    diasAtrasados : document.getElementById('txtDiasAtrasados'),
    clasificacion: document.getElementById('txtRiesgo'),
    clasificar()
    {
        this.clasificacion.value = ''

        if ( this.diasAtrasados.value )
        {
            this.clasificacion.value = this.calcular()
        }
    },
    calcular()
    {
        let clasificacion = ''

        switch ( true )
        {
            case Number(this.diasAtrasados.value) >= 0 && Number(this.diasAtrasados.value) <= 7 :
                clasificacion = 'A1'
                break
            case Number(this.diasAtrasados.value) >= 8 && Number(this.diasAtrasados.value) <= 14 :
                clasificacion = 'A2'
                break
            case Number(this.diasAtrasados.value) >= 15 && Number(this.diasAtrasados.value) <= 30 :
                clasificacion = 'B'
                break
            case Number(this.diasAtrasados.value) >= 31 && Number(this.diasAtrasados.value) <= 90 :
                clasificacion = 'C1'
                break
            case Number(this.diasAtrasados.value) >= 91 && Number(this.diasAtrasados.value) <= 120 :
                clasificacion = 'C2'
                break
            case Number(this.diasAtrasados.value) >= 121 && Number(this.diasAtrasados.value) <= 150 :
                clasificacion = 'D1'
                break
            case Number(this.diasAtrasados.value) >= 151 && Number(this.diasAtrasados.value) <= 180 :
                clasificacion = 'D2'
                break
            case Number(this.diasAtrasados.value) > 180 :
                clasificacion = 'E'
                break
        }

        return clasificacion
    }
}
