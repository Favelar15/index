
let tblPagadurias
const modulo = 'CONTROLES'

window.onload = () => {

    const cboTipoDocumento = document.getElementById('cboTipoDocumento')
    const cboPais = document.getElementById('cboPais')
    const cboAgencia = document.getElementById('cboAgencia')


    const inicializando = initDataTable().then(() => {
        return new Promise( (resolve, reject) => {
            try {
                fetchActions.getCats({
                    modulo, archivo: 'controlesGetCats', solicitados: ['tiposDocumento', 'geograficos', 'agenciasAsignadas']
                }).then(({tiposDocumento: documentos, datosGeograficos: geograficos = [], agenciasAsignadas = []}) => {

                    if (cboTipoDocumento) {
                        let opciones = [`<option selected value="" disabled>Seleccione</option>`]

                        documentos.forEach(documento => {
                            tiposDocumento[documento.id] = {...documento}
                            if (documento.tipoDocumento == 'DUI' || documento.tipoDocumento == 'Pasaporte') {
                                opciones.push(` <option value="${documento.id}">${documento.tipoDocumento}</option> `)
                            }
                        })

                        cboTipoDocumento.innerHTML = opciones.join('')
                        $(`#${cboTipoDocumento.id}`).selectpicker('refresh')
                    }

                    if (cboPais) {
                        datosGeograficos = {
                            ...geograficos
                        }
                        let opciones = ['<option selected value="" disabled>seleccione</option>']

                        for (let key in geograficos) {
                            opciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
                        }

                        cboPais.innerHTML = opciones.join('')
                        $(`#${cboPais.id}`).selectpicker('refresh')
                    }

                    if (cboAgencia) {
                        let opciones = [`<option selected disabled value="">seleccione</option>`]

                        agenciasAsignadas.forEach(agencia => {
                            if (agencia.id != 700) {
                                configAsesor.catAgencias[agencia.id] = {...agencia}
                                opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                            }
                        })

                        cboAgencia.innerHTML = opciones.join('')
                        $(`#${cboAgencia.id}`).selectpicker('refresh')
                    }

                    resolve()

                }).catch(reject)
            } catch (e) {
                reject(e)
            }
        })
    }).catch(generalMostrarError)

    inicializando.then( () => {
        pagaduria.cargarDatos()
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblPagadurias').length > 0) {
                tblPagadurias = $('#tblPagadurias').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0, 1],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [6],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblPagadurias.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const asociado = {
    txtCodigoCliente: document.getElementById('txtCodigoCliente'),
    btnGuardar: document.getElementById('btnGuardar'),
    btnBuscar: document.getElementById('btnBuscarAscociado'),
    cbos: document.querySelectorAll('form#formPagaduria select.selectpicker'),
    inputs: document.querySelectorAll('form#formPagaduria input.form-control'),
    buscar() {
        formActions.validate('formDatosAsociado')
            .then(({errores}) => {

                if (errores == 0) {
                    generalVerificarCodigoCliente(this.txtCodigoCliente.value)
                        .then(data => {
                            if (data.respuesta) {

                                this.txtCodigoCliente.readOnly = true;
                                this.btnBuscar.classList.add('disabled');
                                this.btnGuardar.classList.remove('disabled')
                                //this.btnAgregarAplicacion.classList.remove('disabled')

                                let {
                                    nombresAsociado,
                                    apellidosAsociado,
                                    tipoDocumentoPrimario,
                                    numeroDocumentoPrimario,
                                    numeroDocumentoSecundario,
                                    agencia,
                                    telefonoFijo,
                                    telefonoMovil,
                                    pais,
                                    departamento,
                                    municipio,
                                    direccionCompleta
                                } = data.asociado;

                                document.getElementById('txtNombres').value = nombresAsociado;
                                document.getElementById('txtApellidos').value = apellidosAsociado;
                                document.getElementById('txtTelefonoFijo').value = telefonoFijo;
                                document.getElementById('txtTelefonoMovil').value = telefonoMovil;
                                document.getElementById('cboTipoDocumento').value = tipoDocumentoPrimario
                                document.getElementById('txtDocumentoPrimario').value = numeroDocumentoPrimario;
                                document.getElementById('txtAgenciaAfiliacion').value = agencia;
                                document.getElementById('txtNit').value = numeroDocumentoSecundario;

                                document.getElementById('cboPais').value = pais;
                                $("#cboPais").trigger("change");
                                document.getElementById('cboDepartamento').value = departamento;
                                $("#cboDepartamento").trigger("change");
                                document.getElementById('cboMunicipio').value = municipio;

                                document.getElementById('txtDireccionCompleta').value = direccionCompleta;

                                const selects = document.querySelectorAll('form#formAsociado select.selectpicker')

                                selects.forEach(cbo => {
                                    $(`#${cbo.id}`).selectpicker('refresh')
                                })

                                this.habilitarFormulario(false)
                                //document.getElementById('formDatosAsociado').action = 'javascript:subAplicacion.guardar();';

                            } else if (!data.respuesta) {
                                Swal.fire({
                                    title: 'INFORMACION',
                                    html: `No existe un asociado con el código <strong>${this.txtCodigoCliente.value}</strong> en la base de datos. ¿Desea agregarlo?`,
                                    icon: 'warning',
                                    showCloseButton: true,
                                    showCancelButton: true,
                                    confirmButtonColor: '#46b1c3',
                                    confirmButtonText: `<i class="fa fa-check-circle"></i> SI`,
                                    cancelButtonText: `<i class="fa fa-times-circle"></i> NO`
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.href = `?page=controlesAsociados&mod=${encodeURIComponent(btoa('CONTROLES'))}`;
                                    }
                                });
                            }
                        }).catch(generalMostrarError)
                }

            }).catch(generalMostrarError)
    },
    habilitarFormulario(estado = true) {
        this.cbos.forEach(cbo => {
            cbo.disabled = estado
            $(`#${cbo.id}`).selectpicker('refresh')
        })

        if (estado) {
            this.txtCodigoCliente.readOnly = false
            this.btnBuscar.classList.remove('disabled');
            this.btnGuardar.classList.add('disabled')
        }

        this.inputs.forEach(input => {
            input.id === 'txtFechaDesembolso' ? input.disabled = estado : input.readOnly = estado
        })
    }
}

const pagaduria = {
    id: 0,
    idFila: 0,
    contador: 0,
    pagadurias: {},
    tabLinkPagadurias: document.getElementById('pagadurias-tab'),
    tabLinkFormulario: document.getElementById('formularioPagaduria-tab'),
    irFormulario() {
        this.limpiar()
        this.idFila = 0
        this.tabLinkFormulario.classList.remove('disabled')
        this.tabLinkPagadurias.classList.add('disabled')
        $(`#${this.tabLinkFormulario.id}`).trigger('click')
    },
    salir() {
        Swal.fire({
            title: '¿Está seguro de salir del formulario de pagadurías?',
            text: "¡Seleccione una opcion!",
            icon: 'warning',
            showCancelButton: true,
            showDenyButton: true,
            showCloseButton: true,
            confirmButtonColor: '#46b1c3',
            denyButtonText: `<i class="fas fa-trash"></i> Limpiar formulario`,
            confirmButtonText: `<i class="fas fa-check-circle"></i> Salir del formulario`,
            cancelButtonText: '<i class="fas fa-times-circle"></i> Cancelar'
        }).then(result => {
            if (result.isDenied) {
                this.limpiar()
            } else if (result.isConfirmed) {
                this.tabLinkPagadurias.classList.remove('disabled')
                this.tabLinkFormulario.classList.add('disabled')
                $(`#${this.tabLinkPagadurias.id}`).trigger('click')
            }
        })
    },
    limpiar() {
        const limpiarFormAsociado = formActions.clean('formAsociado')
        const limpandoFormPagaduria = formActions.clean('formPagaduria')

        this.id = 0
        // this.idFila = 0
        Promise.all([
            limpiarFormAsociado, limpandoFormPagaduria
        ]).then(() => {
            asociado.habilitarFormulario(true)
        })
    },
    guardar()
    {
        formActions.validate('formAsociado')
            .then( ({errores}) => {

                if( errores == 0 )
                {
                    if (formActions.manualValidate('formPagaduria'))
                    {
                        let logs = []
                        this.id = 0

                        if( this.idFila != 0 )
                        {
                            this.id = this.pagadurias[this.idFila].id
                            logs = obtenerLogs()
                        }

                        fetchActions.set({
                            modulo, archivo: 'Pagadurias/controlesGuardarPagaduria', datos: {
                                id: this.id,
                                ...formActions.getJSON('formPagaduria'),
                                codigoCliente: asociado.txtCodigoCliente.value,
                                idApartado, tipoApartado, logs
                            }
                        }).then( response => {
                            switch (response.respuesta.trim()) {
                                case 'EXITO':
                                    switch (response.respuesta.trim()) {
                                        case 'EXITO':
                                            Swal.fire({
                                                position: 'top-end',
                                                icon: 'success',
                                                title: '¡Bien hecho!',
                                                text: 'Datos guardados exitosamente',
                                                showConfirmButton: false,
                                                timer: 1500
                                            }).then(i => {
                                                this.cargarDatos()
                                                this.limpiar()
                                                this.tabLinkPagadurias.classList.remove('disabled')
                                                this.tabLinkFormulario.classList.add('disabled')
                                                $(`#${this.tabLinkPagadurias.id}`).trigger('click')
                                            });
                                            break;
                                        default:
                                            generalMostrarError(response);
                                            break;
                                    }
                                    break;
                                default:
                                    generalMostrarError(response);
                                    break;
                            }
                        }).catch( generalMostrarError )
                    }
                }
            }).catch( generalMostrarError )
    },
    cargarDatos()
    {
        this.pagadurias = {}
        this.contador = 0
        fetchActions.get({
            modulo, archivo:'Pagadurias/controlesObtenerPagadurias', params: {}
        }).then( ({pagadurias = []}) => {

            tblPagadurias.clear()

            pagadurias.forEach( pagaduria => {
                this.contador++
                this.pagadurias[this.contador] = { ...pagaduria }
                this.agregarFila({contador: this.contador, ...pagaduria})
            })
            tblPagadurias.columns.adjust().draw()
        }).catch( generalMostrarError)
    },
    agregarFila({contador, fechaDesembolso, numeroCredito, codigoCliente, nombresAsociados, agencia })
    {
        let editar = `<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`
        let eliminar = ``

        if(tipoPermiso == 'ESCRITURA')
        {
            editar = `<button type="button" class="btnTbl" onclick="pagaduria.editar(${contador})"><i class="fas fa-edit"></i></button>`
            eliminar = `<button type="button" class="btnTbl" onclick="pagaduria.eliminar(${contador})"><i class="fas fa-trash"></i></button>`
        }

        tblPagadurias.row.add([
            contador, fechaDesembolso, numeroCredito, codigoCliente, nombresAsociados, agencia,
            `<div class="tblButtonContainer">${editar} ${eliminar}</div>`
        ]).node().id = `trPagaduria${contador}`
    },
    editar( idFila )
    {
        if ( tipoPermiso == 'ESCRITURA')
        {

            this.irFormulario()
            this.idFila = idFila

            asociado.txtCodigoCliente.value = this.pagadurias[this.idFila].codigoCliente
            asociado.buscar()

            document.getElementById('txtNumeroCredito').value = this.pagadurias[this.idFila].numeroCredito
            document.getElementById('txtMonto').value = this.pagadurias[this.idFila].monto
            document.getElementById('txtCuota').value = this.pagadurias[this.idFila].cuota
            document.getElementById('txtTipoGarantia').value = this.pagadurias[this.idFila].tipoGarantia
            document.getElementById('txtInstitucion').value = this.pagadurias[this.idFila].institucion
            document.getElementById('txtFechaDesembolso').value = this.pagadurias[this.idFila].fechaDesembolso
            document.getElementById('cboAgencia').value  = this.pagadurias[this.idFila].idAgencia
            $('#cboAgencia').selectpicker('refresh')
            $('#cboAgencia').trigger('change')

            document.getElementById('cboAsesor').value = this.pagadurias[this.idFila].idAsesor
            $('#cboAsesor').selectpicker('refresh')

            $('#txtFechaDesembolso').datepicker('update')

            let campos = document.querySelectorAll('form#formPagaduria input.form-control')
            campos = [...campos, ...document.querySelectorAll('form#formPagaduria select.form-control') ]

            campos.forEach( campo => {
                if (campo.id) {
                    if (campo.type == 'select-one') {
                        $(`#${campo.id}`).data("valorInicial", campo.options[campo.selectedIndex].text);
                    } else {
                        $(`#${campo.id}`).data("valorInicial", campo.value);
                    }
                }
            })
        } else
        {
            toastr.warning(`Modo sólo lectura, no tienes permisos crear nuevos detalle de créditos`)
        }
    },
    eliminar(idFila)
    {
        const pagaduria = this.pagadurias[idFila];

        Swal.fire({
            title: 'IMPORTANTE',
            text: `¿Quiere eliminar la pagaduría a nombre de ${pagaduria.nombresAsociados}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            confirmButtonText: `<i class="fas fa-thumbs-up"></i> Eliminar`,
            cancelButtonText: `<i class="fas fa-thumbs-up"></i> Cancelar`
        }).then((result) => {
            if (result.isConfirmed) {
                fetchActions.set({
                    modulo, archivo: 'Pagadurias/controlesEliminarPagaduria', datos: {
                        id: this.pagadurias[idFila].id, idApartado, tipoApartado }
                }).then( response => {

                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            switch (response.respuesta.trim()) {
                                case 'EXITO':
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: '¡Bien hecho!',
                                        text: 'Pagaduría eliminada con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then(i => {
                                        delete this.pagadurias[idFila]
                                        tblPagadurias.row(`#trPagaduria${idFila}`).remove().draw(false)
                                        this.cargarDatos()
                                    });
                                    break;
                                default:
                                    generalMostrarError(response);
                                    break;
                            }
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }

                }).catch( generalMostrarError )
            }
        });
    }
}

const configAsesor = {
    catAgencias: {},
    cboAsesor: document.getElementById('cboAsesor'),
    cboAgencia: document.getElementById('cboAgencia'),
    cargarAsesores() {

        const idAgencia = this.cboAgencia.value

        if (idAgencia) {
            const asesores = this.catAgencias[idAgencia].asesores

            let opciones = [`<option value="" selected disabled>seleccione</option>`]

            asesores.forEach(asesor => {
                opciones.push(`<option value="${asesor.id}">${asesor.nombres} ${asesor.apellidos}</option>`)
            })

            this.cboAsesor.innerHTML = opciones.join('')
            $(`#${this.cboAsesor.id}`).selectpicker('refresh')
        }
    }
}

const obtenerLogs = () => {

    let camposEditados = [];
    let campos = document.querySelectorAll('form#formPagaduria input.form-control');

    campos = [...campos, ...document.querySelectorAll("form#formPagaduria select.form-control")];

    campos.forEach(campo => {
        if (campo.id) {
            let valorActual = campo.type == 'select-one' ? campo.options[campo.selectedIndex].text : $("#" + campo.id).val();

            if ($(`#${campo.id}`).data("valorInicial")) {
                let valorAnterior = $(`#${campo.id}`).data("valorInicial");

                if (valorActual != valorAnterior) {
                    let label = $(`#${campo.id}`).parent().parent().find("label").length > 0 ? $(`#${campo.id}`).parent().parent().find("label:eq(0)").text() : ($(`#${campo.id}`).parent().parent().parent().find("label").length > 0 ? $(`#${campo.id}`).parent().parent().parent().find("label:eq(0)").text() : 'undefined');
                    label = (label.replace("*", "")).trim();
                    camposEditados.push({
                        campo: label,
                        valorAnterior,
                        valorNuevo: valorActual
                    })
                }
            }
        }
    });

    return camposEditados;
}