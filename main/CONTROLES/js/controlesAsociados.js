window.onload = () => {
    document.getElementById('frmAsociado').reset();
    fetchActions.getCats({
        modulo: "CONTROLES",
        archivo: "controlesGetCats",
        solicitados: ['tiposDocumento', 'estadoCivil', 'actividadEconomica', 'geograficos', 'agencias']
    }).then(({
        tiposDocumento: tiposDoc,
        estadosCiviles,
        actividadesEconomicas,
        agencias,
        datosGeograficos: geograficos
    }) => {
        const opciones = [`<option selected disabled value="">seleccione</option>`];
        const selectpickerCbos = document.querySelectorAll("select.selectpicker");

        let tmpOpciones = [...opciones],
            cbos = document.querySelectorAll("select.cboTipoDocumento");
        tiposDoc.forEach(tipo => {
            tiposDocumento[tipo.id] = tipo;
            tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
        });

        cbos.forEach(cbo => {
            cbo.innerHTML = tmpOpciones.join("");
        });

        tmpOpciones = [...opciones];
        cbos = document.querySelectorAll("select.cboEstadoCivil");
        estadosCiviles.forEach(estado => {
            tmpOpciones.push(`<option value="${estado.id}">${estado.estadoCivil}</option>`);
        });

        cbos.forEach(cbo => {
            cbo.innerHTML = tmpOpciones.join("");
        });

        tmpOpciones = [...opciones];
        cbos = document.querySelectorAll("select.cboActividadEconomica");
        actividadesEconomicas.forEach(actividad => {
            tmpOpciones.push(`<option value="${actividad.id}">${actividad.actividadEconomica}</option>`);
        });

        cbos.forEach(cbo => {
            cbo.innerHTML = tmpOpciones.join("");
        });

        tmpOpciones = [...opciones];
        cbos = document.querySelectorAll("select.cboAgencia");
        agencias.forEach(agencia => {
            if (agencia.id != 700) {
                tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`);
            }

        });

        cbos.forEach(cbo => {
            cbo.innerHTML = tmpOpciones.join("");
        });

        tmpOpciones = [...opciones];
        cbos = document.querySelectorAll("select.cboPais");

        datosGeograficos = {
            ...geograficos
        };

        for (let key in datosGeograficos) {
            tmpOpciones.push(`<option value="${datosGeograficos[key].id}">${datosGeograficos[key].nombrePais}</option>`);
        }

        cbos.forEach(cbo => {
            cbo.innerHTML = tmpOpciones.join("");
        });

        selectpickerCbos.forEach(cbo => {
            $("#" + cbo.id).selectpicker("refresh");
        });

        $("#txtFechaNacimiento").on('change', function () {
            let edad = generalCalcularEdad($(this).val());
            document.getElementById('txtEdad').value = edad;
        });
    }).catch(generalMostrarError);
}

const configAsociado = {
    campoCodigoCliente: document.getElementById("txtCodigoCliente"),
    campoEmail: document.getElementById("txtEmail"),
    verifyCode: function () {
        let codigoCliente = document.getElementById('txtCodigoAsociado').value;
        if (codigoCliente.length > 0) {
            fetchActions.get({
                modulo: 'CONTROLES',
                archivo: 'controlesBuscarAsociado',
                params: {
                    codigoCliente: encodeURIComponent(codigoCliente),
                    tipo: "Register"
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta) {
                        case "EXITO":
                            toastr.success(`Codigo de cliente disponible`);
                            break;
                        case "Registrado":
                            toastr.error(`Ya existe un asociado con el codigo ${codigoCliente}`);
                            break;
                        case "Retirado":
                            Swal.fire({
                                title: "¡Atención!",
                                icon: "warning",
                                html: `Ya existe un asociado con el codigo <b>${codigoCliente}</b>, pero se encuentra retirado.`
                            });
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            toastr.warning(" ¡Completa el campo!");
        }
    },
    save: function () {
        formActions.validate('frmAsociado').then(({
            errores
        }) => {
            if (errores == 0) {
                if (this.campoEmail.value.length > 0) {
                    if (!generalValidateEmail(this.campoEmail.value)) {
                        errores++;
                        let spnError = $("#" + this.campoEmail.id).parent().parent().find("span.spnError");
                        spnError.html("Email no válido");
                        spnError.fadeIn("fast");
                    }
                }

                if (errores == 0) {
                    let datosAsociados = formActions.getJSON('frmAsociado');
                    datosAsociados["accion"] = "Nuevo";
                    datosAsociados["descripcionLog"] = "Registro de asociado";
                    datosAsociados["detallesLog"] = [];
                    datosAsociados["tipoApartado"] = tipoApartado;
                    datosAsociados["idApartado"] = idApartado;
                    $(".preloader").fadeIn("fast");
                    fetchActions.set({
                        modulo: "CONTROLES",
                        archivo: 'controlesGuardarAsociados',
                        datos: datosAsociados
                    }).then((datosRespuesta) => {
                        if (datosRespuesta.respuesta) {
                            switch (datosRespuesta.respuesta.trim()) {
                                case 'EXITO':
                                    Swal.fire({
                                        title: "¡Bien hecho!",
                                        html: "Asociado guardado exitosamente",
                                        icon: "success"
                                    }).then(() => {
                                        window.top.location.href = './';
                                    });
                                    break;
                                case 'Duplicado':
                                    toastr.error(`El codigo de usuario ya existe`);
                                    break;
                                default:
                                    generalMostrarError(datosRespuesta);
                                    break;
                            }
                        } else {
                            generalMostrarError(datosRespuesta);
                        }
                    }).catch(generalMostrarError);
                } else {
                    toastr.warning("Formulario con errores");
                }
            }
        }).catch(generalMostrarError);
    },
    limpiar() {
        window.location = `?page=controlesAsociados&mod=${ encodeURIComponent(btoa('CONTROLES')) }`
    }
};