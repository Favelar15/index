
const modulo = 'CONTROLES'

window.onload = () => {

    document.getElementById('formReactivacion').reset()
    const cboTipoDocumento = document.getElementById('cboTipoDocumento')
    const cboPais = document.getElementById('cboPais')

    fetchActions.getCats({
        modulo, archivo: 'controlesGetCats', solicitados: ['tiposDocumento', 'geograficos' ]
    }).then( ({tiposDocumento: documentos, datosGeograficos: geograficos =[],
                              }) => {

        if ( cboTipoDocumento )
        {
            let opciones = [`<option selected value="" disabled>Seleccione</option>`]

            documentos.forEach( documento => {
                tiposDocumento[documento.id] = {...documento}
                if (documento.tipoDocumento == 'DUI' || documento.tipoDocumento == 'Pasaporte') {
                    opciones.push(` <option value="${documento.id}">${documento.tipoDocumento}</option> `)
                }
            })

            cboTipoDocumento.innerHTML = opciones.join('')
            $(`#${cboTipoDocumento.id}`).selectpicker('refresh')
        }

        if( cboPais )
        {
            datosGeograficos = {
                ...geograficos
            }
            let opciones = ['<option selected value="" disabled>seleccione</option>']

            for (let key in geograficos) {
                opciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
            }

            cboPais.innerHTML = opciones.join('')
            $(`#${cboPais.id}`).selectpicker('refresh')
        }

    }).catch( generalMostrarError )

        $('.preloader').fadeOut('fast')
}

const asociado  = {
    txtCodigoCliente: document.getElementById('txtCodigoCliente'),
    btnBuscar: document.getElementById('btnBuscarAscociado'),
    txtMontoPendiente: document.getElementById('txtMontoPendiente'),
    txtAbono : document.getElementById('txtAbono'),
    formReactivacion: document.getElementById('formReactivacion'),
    buscar()
    {
        formActions.validate('formReactivacion')
            .then( ( { errores }) => {
                if (errores == 0)
                {
                    generalVerificarCodigoCliente(txtCodigoCliente.value)
                        .then( data => {

                            if (data.respuesta)
                            {
                                let {
                                    nombresAsociado,
                                    apellidosAsociado,
                                    tipoDocumentoPrimario,
                                    numeroDocumentoPrimario,
                                    numeroDocumentoSecundario,
                                    agencia,
                                    telefonoFijo,
                                    telefonoMovil,
                                    pais,
                                    departamento,
                                    municipio,
                                    direccionCompleta,
                                    deudaAportaciones
                                } = data.asociado;


                                this.txtCodigoCliente.readOnly = true
                                this.btnBuscar.disabled = true

                                document.getElementById('txtNombres').value = nombresAsociado;
                                document.getElementById('txtApellidos').value = apellidosAsociado;
                                document.getElementById('txtTelefonoFijo').value = telefonoFijo;
                                document.getElementById('txtTelefonoMovil').value = telefonoMovil;
                                document.getElementById('cboTipoDocumento').value = tipoDocumentoPrimario
                                document.getElementById('txtDocumentoPrimario').value = numeroDocumentoPrimario;
                                document.getElementById('txtAgenciaAfiliacion').value = agencia;
                                document.getElementById('txtNit').value = numeroDocumentoSecundario;

                                document.getElementById('cboPais').value = pais;
                                $("#cboPais").trigger("change");
                                document.getElementById('cboDepartamento').value = departamento;
                                $("#cboDepartamento").trigger("change");
                                document.getElementById('cboMunicipio').value = municipio;

                                document.getElementById('txtDireccionCompleta').value = direccionCompleta;

                                this.txtMontoPendiente.value = deudaAportaciones;

                                const selects = document.querySelectorAll('form#formReactivacion select.selectpicker')

                                selects.forEach( cbo => {
                                    $(`#${cbo.id}`).selectpicker('refresh')
                                })

                                if ( deudaAportaciones == 0 )
                                {
                                    document.getElementById('btnGuardar').disabled = true
                                    Swal.fire({
                                        title: 'INFORMACION',
                                        text: `El asociado ${nombresAsociado} ${apellidosAsociado} está al día con la aportaciones`,
                                        icon: 'success',
                                        showConfirmButton: true,
                                        confirmButtonText: `<i class="fa fa-check-circle"></i> OK`,
                                        confirmButtonColor: '#46b1c3',
                                    })
                                } else
                                {
                                    document.getElementById('formReactivacion').action = 'javascript:asociado.guardar();';
                                    this.txtAbono.readOnly = false
                                }
                            } else
                            {
                                this.txtCodigoCliente.value = ''

                                Swal.fire({
                                    title: 'INFORMACION',
                                    text: `No existe un asociado con el código ${this.txtCodigoCliente.value} en la base de datos. ¿Desea agregarlo?`,
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#46b1c3',
                                    cancelButtonColor: '#f54343',
                                    confirmButtonText: `<i class="fa fa-check-circle"></i> SI`,
                                    cancelButtonText: `<i class="fa fa-times-circle"></i> NO`
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.href = `?page=controlesAsociados&mod=${encodeURIComponent(btoa('CONTROLES'))}`;
                                    }
                                });
                            }
                        }).catch( generalMostrarError )

                }
            }).catch( generalMostrarError )
    },
    cancelar()
    {
        formActions.clean('formReactivacion')
            .then( () => {
                this.txtCodigoCliente.readOnly = false
                this.btnBuscar.disabled = false
                this.txtAbono.readOnly = true
                document.getElementById('btnGuardar').disabled = false
                this.formReactivacion.classList.remove('was-validated')
                this.txtAbono.classList.remove('is-valid')
                document.getElementById('formReactivacion').action = 'javascript:asociado.buscar();';
            }).catch( generalMostrarError)
    },
    guardar()
    {
        formActions.validate('formReactivacion')
            .then( ( { errores } )  => {

                if (errores == 0 )
                {
                    const {
                        txtCodigoCliente,
                        txtMontoPendiente,
                        txtAbono
                    } = formActions.getJSON('formReactivacion')

                    let montoValido = true

                    if ( Number(txtMontoPendiente.value) > Number(txtAbono.value) )
                    {
                        this.formReactivacion.classList.remove('was-validated')
                        this.txtAbono.classList.remove('is-valid')
                        this.txtAbono.classList.add('is-invalid')
                        montoValido = false
                    } else
                    {
                        this.txtAbono.classList.remove('is-invalid')
                        this.txtAbono.classList.add('is-valid')
                    }

                    if ( montoValido )
                    {
                        fetchActions.set({
                            modulo, archivo: 'ReactivacionAportaciones/controlesGuardarReactivacion',
                            datos: { txtCodigoCliente, txtAbono, txtMontoPendiente, idApartado, tipoApartado }
                        }).then( response => {

                            switch (response.respuesta.trim()) {
                                case 'EXITO':
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: '¡Bien hecho!',
                                        text: 'Reactivación de cuenta aplicada con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then( () => {
                                        this.cancelar()
                                    });
                                    break;
                                default:
                                    generalMostrarError(response);
                                    break;
                            }
                        }).catch( generalMostrarError )
                    }
                }
            }).catch( generalMostrarError)
    }
}