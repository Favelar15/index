let tblResidenciaExtrangero
let tblPersonaRelacionada
let tblSociedades
let tblCuentasTerceros
let tblProductosContratos
let tblCargosPoliticos

let tblResidenciaRepresentante
const modulo = 'CONTROLES';
let datosSolicitud = {}
let idSolicitud = 0, idRepresentante = 0, idAntecedentes = 0;


window.onload = () => {

    document.getElementById('formDatosPersonales').reset()

    const cboTipoDocumento = document.querySelectorAll('select.cboTipoDocumento');
    const cboEstadoCivil = document.querySelectorAll('select.cboEstadoCivil');
    const cboPais = document.querySelectorAll('select.cboPais');
    const cboNacionalidad = document.querySelectorAll('select.cboNacionalidad');
    const cboGentilicio = document.querySelectorAll('select.cboGentilicio');
    const cboActividadEconomica = document.querySelectorAll('select.cboActividadEconomica');
    const cboTipoSolicitud = document.querySelectorAll('select.cboTipoSolicitud')
    const cboTipoAfiliacion = document.querySelectorAll('select.cboTipoAfiliacion')
    const cboCargosPoliticos = document.querySelectorAll('select.cboCargoPolitico')
    const cboParentezcos = document.querySelectorAll('select.cboParentezcos')
    const cboProductos = document.querySelectorAll('select.cboProductoContratados')
    const cboPersonaRelacionada = document.querySelectorAll('select.cboPersonaRelacionada')

    const r = initDataTable().then(() => {
        return new Promise( ( resolve, reject ) => {
            try {
                fetchActions.getCats({
                    modulo,
                    archivo: 'controlesGetCats',
                    solicitados: [
                        'tiposDocumento', 'estadoCivil', 'actividadEconomica', 'geograficos', 'tiposAfiliacion', 'tiposSolicitudes', 'cargosPoliticos', 'parentezcos', 'productosServicios', 'tipoRelacion'
                    ]
                }).then(({
                             tiposDocumento: documentos,
                             estadosCiviles,
                             actividadesEconomicas,
                             datosGeograficos: geograficos,
                             tiposAfiliacion,
                             tiposSolicitudes,
                             cargosPoliticos,
                             parentezcos,
                             productosServicios,
                             tipoRelacion
                         }) => {

                    if (cboTipoDocumento.length > 0) {
                        let opciones = [`<option selected value="" disabled>Seleccione</option>`]

                        documentos.forEach(documento => {
                            tiposDocumento[documento.id] = {...documento}
                            if (documento.tipoDocumento == 'DUI' || documento.tipoDocumento == 'Pasaporte') {
                                opciones.push(` <option value="${documento.id}">${documento.tipoDocumento}</option> `)
                            }
                        })

                        cboTipoDocumento.forEach(cbo => {
                            cbo.innerHTML = opciones.join('')
                            if (cbo.className.includes('selectpicker')) {
                                $(`#${cbo.id}`).selectpicker('refresh');
                            }
                        })
                    }

                    if (cboEstadoCivil.length > 0) {
                        let opciones = [`<option selected value="" disabled>Seleccione</option>`]

                        estadosCiviles.forEach(estadoCivil => {
                            opciones.push(`<option value="${estadoCivil.id}"> ${estadoCivil.estadoCivil} </option>`)
                        })

                        cboEstadoCivil.forEach(cbo => {
                            cbo.innerHTML = opciones.join('')
                            if (cbo.className.includes('selectpicker')) {
                                $(`#${cbo.id}`).selectpicker('refresh');
                            }
                        })
                    }

                    if (cboActividadEconomica.length > 0) {
                        let opciones = [`<option selected disabled value="">Seleccione</option>`]
                        actividadesEconomicas.forEach(actividad => {
                            opciones.push([`<option value="${actividad.id}">${actividad.actividadEconomica}</option>`])
                        })

                        cboActividadEconomica.forEach(cbo => {
                            cbo.innerHTML = opciones.join('')
                            if (cbo.className.includes('selectpicker')) {
                                $(`#${cbo.id}`).selectpicker('refresh');
                            }
                        })
                    }

                    if (cboPais.length > 0) {
                        datosGeograficos = {
                            ...geograficos
                        }

                        let opciones = ['<option selected value="" disabled>seleccione</option>']

                        for (let key in geograficos) {
                            opciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
                        }

                        cboPais.forEach(cbo => {
                            cbo.innerHTML = opciones.join('');
                            if (cbo.className.includes('selectpicker')) {
                                $(`#${cbo.id}`).selectpicker('refresh')
                            }
                        })
                    }

                    if (cboTipoAfiliacion.length > 0) {

                        let opciones = ['<option selected value="" disabled>seleccione</option>'];

                        tiposAfiliacion.forEach(tipoAfiliacion => {
                            opciones.push(`<option value="${tipoAfiliacion.id}">${tipoAfiliacion.tipoAfiliacion}</option>`)
                        })

                        cboTipoAfiliacion.forEach(cbo => {
                            cbo.innerHTML = opciones.join('');
                            if (cbo.className.includes('selectpicker')) {
                                $(`#${cbo.id}`).selectpicker('refresh');
                            }
                        })
                    }

                    if (cboTipoSolicitud.length > 0) {
                        let opciones = ['<option selected value="" disabled>seleccione</option>'];

                        tiposSolicitudes.forEach(tipoSolicitud => {
                            opciones.push(`<option value="${tipoSolicitud.id}">${tipoSolicitud.tipoSolicitud}</option>`)
                        })

                        cboTipoSolicitud.forEach(cbo => {
                            cbo.innerHTML = opciones.join('');
                            if (cbo.className.includes('selectpicker')) {
                                $(`#${cbo.id}`).selectpicker('refresh');
                            }
                        })
                    }

                    if (cboCargosPoliticos.length > 0) {
                        let opciones = ['<option selected value="" disabled>seleccione</option>'];

                        cargosPoliticos.forEach(cargo => {
                            opciones.push(`<option value="${cargo.id}">${cargo.cargo}</option>`)
                        })

                        cboCargosPoliticos.forEach(cbo => {
                            cbo.innerHTML = opciones.join('');
                            if (cbo.className.includes('selectpicker')) {
                                $(`#${cbo.id}`).selectpicker('refresh');
                            }
                        })
                    }

                    if (cboParentezcos.length > 0) {
                        let opciones = ['<option selected value="" disabled>seleccione</option>'];

                        parentezcos.forEach(parantezco => {
                            opciones.push(`<option value="${parantezco.id}">${parantezco.parentesco}</option>`)
                        })

                        cboParentezcos.forEach(cbo => {
                            cbo.innerHTML = opciones.join('');
                            if (cbo.className.includes('selectpicker')) {
                                $(`#${cbo.id}`).selectpicker('refresh');
                            }
                        })
                    }

                    if (cboProductos.length > 0) {

                        productosServicios.forEach(producto => {
                            gestionTblProductos.catalogo[producto.id] = {...producto}
                        })
                    }

                    if (cboPersonaRelacionada.length > 0) {
                        let opciones = [`<option selected value="" disabled >seleccione</option>`]

                        tipoRelacion.forEach(relacion => {
                            opciones.push(`<option value="${relacion.id}"> ${relacion.tipoRelacion} </option>`)
                        })

                        cboPersonaRelacionada.forEach(cbo => {
                            cbo.innerHTML = opciones.join('');
                            if (cbo.className.includes('selectpicker')) {
                                $(`#${cbo.id}`).selectpicker('refresh');
                            }
                        })
                    }
                    resolve()

                }).catch(reject)
            } catch (e) {
                reject(e.message)
            }
        })

    }).catch(generalMostrarError)

    r.then( editar ).catch( generalMostrarError )
}

const editar = () => {
    return new Promise( ( resolve, reject ) => {
        try {

            const id = atob(getParameterByName('solicitud'))

            if (id.length > 0) {

                fetchActions.get({
                    modulo, archivo: 'Peps/controlesObtenerDatosSolicitud', params: { idSolicitud: id  }
                }).then( ( { solicitud = { } } )  => {

                    idSolicitud = id

                    const cboTipoSolicitud = document.getElementById('cboTipoSolicitud')
                    const cboTipoAfiliacion = document.getElementById('cboTipoAfiliacion')
                    const cboRepresentante = document.getElementById('cboRepresentante')

                    cboTipoSolicitud.value = solicitud.idTipoSolicitud
                    cboTipoAfiliacion.value = solicitud.idTipoAfiliacion
                    cboRepresentante.value = solicitud.cboRepresentante

                    $("#cboTipoAfiliacion").trigger("change")
                    $('#cboRepresentante').trigger('change')


                    cboTipoSolicitud.disabled = true
                    cboTipoAfiliacion.disabled = true
                    cboRepresentante.disabled = true

                    tipoSolicitud.cambiar('NUEVO')

                    document.getElementById('txtCodigoAsociado').value = solicitud.codigoCliente
                    document.getElementById('txtNombres').value = solicitud.nombres
                    document.getElementById('txtApellidos').value = solicitud.apellidos
                    document.getElementById('txtDocumentoPrimario').value = solicitud.numeroDocumentoPrimario
                    document.getElementById('cboTipoDocumento').value = solicitud.idTipoDocumentoPrimario
                    document.getElementById('txtFechaExpedicion').value = solicitud.fechaExpedicion
                    document.getElementById('txtLugarExpedicion').value = solicitud.lugarExpedicion
                    document.getElementById('txtTelefono').value = solicitud.telefono
                    document.getElementById('txtEmail').value = solicitud.email
                    document.getElementById('txtLugarNacimiento').value = solicitud.lugarDeNacimiento
                    document.getElementById('txtFechaNacimiento').value = solicitud.fechaNacimiento
                    document.getElementById('cboEstadoCivil').value = solicitud.idEstadoCivil
                    document.getElementById('cboPais').value = solicitud.idPais
                    document.getElementById('cboGentilicio').value = solicitud.nacionalidad

                    $(`#txtFechaExpedicion`).datepicker('update', solicitud.fechaExpedicion)
                    $(`#txtFechaNacimiento`).datepicker('update', solicitud.fechaNacimiento)

                    $("#cboPais").trigger("change")
                    document.getElementById('cboDepartamento').value = solicitud.idDepartamento
                    $("#cboDepartamento").trigger("change")
                    document.getElementById('cboMunicipio').value = solicitud.idMunicipio
                    document.getElementById('txtDireccion').value = solicitud.direccion
                    document.getElementById('txtProfesion').value = solicitud.profesion
                    document.getElementById('cboActividadEconomicaPrincipal').value = solicitud.idActividadEconomicaPrimaria
                    document.getElementById('cboActividadEconomicaSecundaria').value = solicitud.idActividadEconomicaSecundaria
                    document.getElementById('txtLugarDeTrabajo').value = solicitud.lugarDeTrabajo
                    document.getElementById('cboFondosPoliticos').value = solicitud.fondosPoliticos
                    document.getElementById('txtRemuneracionMensual').value = solicitud.remuneracionMensual
                    document.getElementById('txtDetalleRemuneracion').value = solicitud.detalleRemuneracion
                    document.getElementById('txtOtrosIngresos').value = solicitud.otrosIngresos
                    document.getElementById('txtDetalleOtrosIngresos').value = solicitud.detalleOtrosIngresos
                    $('#txtOtrosIngresos').trigger('change')


                    if (solicitud.residencias && solicitud.residencias.length > 0) {
                        solicitud.residencias.forEach(({id, idPais, pais, direccion, propietario}) => {
                            gestionTblResidencia.contador++

                            const datosResidencia = {
                                contador: gestionTblResidencia.contador,
                                id,
                                idPais,
                                pais,
                                residencia: direccion,
                                propietario
                            }
                            gestionTblResidencia.residencias[gestionTblResidencia.contador] = {...datosResidencia}
                            gestionTblResidencia.agregarFila({...datosResidencia})
                        })
                        tblResidenciaExtrangero.columns.adjust().draw()
                    }

                    if (solicitud.Antecedente && Object.keys(solicitud.Antecedente).length > 0) {
                        const {Antecedente} = solicitud
                        idAntecedentes = Antecedente.id
                        document.getElementById('cboCargoPepAntecedentes').value = Antecedente.Cargo.id
                        document.getElementById('txtEntidad').value = Antecedente.entidad
                        document.getElementById('txtInicioGestionAntecedentes').value = Antecedente.inicioGestion
                        document.getElementById('txtFinGestionAntecedentes').value = Antecedente.finGestion

                        if ( Antecedente.nombre )
                        {
                            document.getElementById('txtNombresAntecedentes').value = Antecedente.nombre
                        }

                        if (Antecedente.CargoReemplazante.id )
                        {
                            document.getElementById('cboCargoPoliticoReem').value = Antecedente.CargoReemplazante.id
                        }

                        if (Antecedente.ejercicio)
                        {
                            document.getElementById('txtEjecicioAntecedentes').value = Antecedente.ejercicio
                        }

                        const cboAntecedentes = document.querySelectorAll('#antecedentes select.selectpicker')

                        cboAntecedentes.forEach(cbo => {
                            $(`#${cbo.id}`).selectpicker('refresh')
                        })
                    }

                    if (solicitud.personasRelacionadas && solicitud.personasRelacionadas.length > 0) {
                        solicitud.personasRelacionadas.forEach(persona => {
                            gestionTblPersonaRelacionada.contador++

                            const datos = {
                                id: persona.id,
                                contador: gestionTblPersonaRelacionada.contador,
                                idParentesco: persona.Parentesco.id, parentesco: persona.Parentesco.parentesco,
                                idVinculo: persona.Vinculo.id, vinculo: persona.Vinculo.vinculo,
                                idCargo: persona.Cargo.id, cargo: persona.Cargo.cargo,
                                nombre: persona.nombre,
                                inicioGestion: persona.inicioGestion,
                                finGestion: persona.finGestion,
                                propietario: persona.propietario
                            }
                            gestionTblPersonaRelacionada.personasRelacionadas[gestionTblPersonaRelacionada.contador] = {...datos}
                            gestionTblPersonaRelacionada.add({...datos})
                        })
                        tblPersonaRelacionada.columns.adjust().draw();
                    }

                    if (solicitud.sociedades && solicitud.sociedades.length > 0) {
                        solicitud.sociedades.forEach(sociedad => {
                            gestionTblSociedades.contador++

                            const datos = {
                                contador: gestionTblSociedades.contador,
                                id: sociedad.id,
                                entidad: sociedad.sociedad,
                                porcentaje: sociedad.porcentaje,
                                propietario: sociedad.propietario
                            }
                            gestionTblSociedades.sociedades[gestionTblSociedades.contador] = {...datos}
                            gestionTblSociedades.agregarFila({...datos})
                        })
                        tblSociedades.columns.adjust().draw()
                    }

                    if (solicitud.otrasCuentas && solicitud.otrasCuentas.length > 0) {
                        solicitud.otrasCuentas.forEach(otraCuenta => {
                            gestionTblCuentaTerceros.contador++

                            const datosOtrasCuentas = {
                                contador: gestionTblCuentaTerceros.contador,
                                id: otraCuenta.id,
                                nombre: otraCuenta.nombre,
                                idParentesco: otraCuenta.Parentesco.id,
                                parentesco: otraCuenta.Parentesco.parentesco,
                                motivo: otraCuenta.motivo,
                                propietario: otraCuenta.propietario
                            }

                            gestionTblCuentaTerceros.agregarFila({
                                ...datosOtrasCuentas
                            })
                            gestionTblCuentaTerceros.cat[gestionTblCuentaTerceros.contador] = {...datosOtrasCuentas}
                        })
                        tblCuentasTerceros.columns.adjust().draw()
                    }


                    if (solicitud.productos && solicitud.productos.length > 0) {

                        solicitud.productos.forEach(producto => {

                            gestionTblProductos.contador++

                            const datosProductos = {
                                contador: gestionTblProductos.contador,
                                id: producto.id,
                                idProducto: producto.Servicio.id,
                                producto: producto.Servicio.nombre,
                                proposito: producto.proposito,
                                propietario: producto.propietario
                            }

                            gestionTblProductos.productos[gestionTblProductos.contador] = {...datosProductos}
                            gestionTblProductos.agregarFila({...datosProductos})
                        })
                        tblProductosContratos.columns.adjust().draw()
                    }

                    if (Object.keys(solicitud.representante).length > 0) {
                        console.log('tiene datos Representante')

                        const {representante} = solicitud

                        idRepresentante = representante.id
                        document.querySelector('form#frmDatosRepresentante input#txtNombresRepresentante').value = representante.nombres
                        document.querySelector('form#frmDatosRepresentante #txtApellidosRepresentante').value = representante.apellidos
                        document.querySelector('form#frmDatosRepresentante #cboTipoDocumentoRepresentante').value = representante.idTipoDocumentoPrimario
                        document.querySelector('form#frmDatosRepresentante #txtDocumentoPrimarioRepresentante').value = representante.numeroDocumentoPrimario
                        document.querySelector('form#frmDatosRepresentante #txtTelefonoRepresentante').value = representante.telefono
                        document.querySelector('form#frmDatosRepresentante #txtEmailRepresentante').value = representante.email
                        document.querySelector('form#frmDatosRepresentante #cboPaisRepresentante').value = representante.idPais
                        $(`#cboPaisRepresentante`).trigger('change')
                        document.querySelector('#cboDepartamentoRepresentante').value = representante.idDepartamento
                        $(`form#frmDatosRepresentante #cboDepartamentoRepresentante`).trigger('change')
                        document.querySelector('form#frmDatosRepresentante #cboMunicipioRepresentante').value = representante.idMunicipio
                        document.querySelector('form#frmDatosRepresentante #txtDireccionRepresentante').value = representante.direccion

                        if (representante.residencias.length > 0) {
                            representante.residencias.forEach(residencia => {

                                gestionResidenciaRepresentante.contador++

                                const datosResidencia = {
                                    contador: gestionResidenciaRepresentante.contador,
                                    id: residencia.id,
                                    idPais: residencia.idPais,
                                    pais: residencia.pais,
                                    residencia: residencia.direccion
                                }

                                gestionResidenciaRepresentante.residencias[gestionResidenciaRepresentante.contador] = {...datosResidencia}
                                gestionResidenciaRepresentante.addFila({...datosResidencia})
                            })
                            tblResidenciaRepresentante.columns.adjust().draw()
                        }

                        if (representante.cargosPoliticos.length > 0) {
                            representante.cargosPoliticos.forEach(cargoPolitico => {
                                gestionTblCargosRepresentante.contador++
                                const datos = {
                                    contador: gestionTblCargosRepresentante.contador,
                                    id: cargoPolitico.id,
                                    idCargo: cargoPolitico.Cargo.id,
                                    cargo: cargoPolitico.Cargo.cargo,
                                    inicioGestion: cargoPolitico.inicioGestion,
                                    finGestion: cargoPolitico.finGestion
                                }

                                gestionTblCargosRepresentante.cargosPoliticos[gestionTblCargosRepresentante.contador] = {...datos}
                                gestionTblCargosRepresentante.agregarFila({...datos})
                            })
                            tblCargosPoliticos.columns.adjust().draw()
                        }
                    }

                    const cboSelectPickers = document.querySelectorAll('select.selectpicker')

                    cboSelectPickers.forEach(cbo => {
                        $(`#${cbo.id}`).selectpicker('refresh')
                    })

                }).catch( generalMostrarError )

            }

            resolve()
        } catch (e) {
            reject()
        }
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {

            if ($('#tblResidenciaExtrangero').length > 0) {
                tblResidenciaExtrangero = $('#tblResidenciaExtrangero').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            width: '10%'
                        },
                        {
                            targets: [3],
                            width: '10%'
                        }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblResidenciaExtrangero.columns.adjust().draw();
            }

            if ($('#tblPersonaRelacionada').length > 0) {
                tblPersonaRelacionada = $('#tblPersonaRelacionada').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblPersonaRelacionada.columns.adjust().draw();
            }

            if ($('#tblSociedades').length > 0) {
                tblSociedades = $('#tblSociedades').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            width: '10%'
                        },
                        {
                            targets: [3],
                            className: 'text-center',
                            width: '10%'
                        }

                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSociedades.columns.adjust().draw();
            }

            if ($('#tblProductosContratos').length > 0) {
                tblProductosContratos = $('#tblProductosContratos').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                        },
                        {
                            targets: [3],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblProductosContratos.columns.adjust().draw();
            }

            if ($('#tblOtrasCuentas').length > 0) {
                tblCuentasTerceros = $('#tblOtrasCuentas').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            width: '8%'
                        },
                        {
                            targets: [4],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblCuentasTerceros.columns.adjust().draw();
            }

            if ($('#tblResidenciaRepresentante').length > 0) {
                tblResidenciaRepresentante = $('#tblResidenciaRepresentante').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            width: '10%'
                        },
                        {
                            targets: [3],
                            width: '10%'
                        }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblResidenciaRepresentante.columns.adjust().draw();
            }

            if ($('#tblCargosPoliticos').length > 0) {
                tblCargosPoliticos = $('#tblCargosPoliticos').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            width: '10%'
                        },
                        {
                            targets: [4],
                            width: '10%'
                        }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblCargosPoliticos.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const gestionTblPersonaRelacionada = {
    idFila: 0,
    personasRelacionadas: {},
    contador: 0,
    mostrarModal(idFila = 0) {
        this.idFila = idFila

        formActions.clean('frmPersonasRelacionadas')
            .then(() => {

                if (this.idFila > 0) {
                    document.querySelector('form#frmPersonasRelacionadas select#cboVinculo').value = this.personasRelacionadas[this.idFila].idVinculo
                    document.querySelector('form#frmPersonasRelacionadas select#cboParentesco').value = this.personasRelacionadas[this.idFila].idParentesco
                    document.querySelector('form#frmPersonasRelacionadas #txtNombre').value = this.personasRelacionadas[this.idFila].nombre
                    document.querySelector('form#frmPersonasRelacionadas #cboCargoPolitico').value = this.personasRelacionadas[this.idFila].idCargo
                    document.querySelector('form#frmPersonasRelacionadas #txtIncioGestion').value = this.personasRelacionadas[this.idFila].inicioGestion
                    document.querySelector('form#frmPersonasRelacionadas #txtFinGestion').value = this.personasRelacionadas[this.idFila].finGestion

                    const cbos = document.querySelectorAll('form#frmPersonasRelacionadas select.selectpicker')

                    $("form#frmPersonasRelacionadas #txtIncioGestion").datepicker('update')
                    $("form#frmPersonasRelacionadas #txtFinGestion").datepicker('update')

                    cbos.forEach(cbo => {
                        $(`#${cbo.id}`).selectpicker('refresh')
                    })
                }

                $('#modalPersonaRelacionada').modal('show')

            }).catch(generalMostrarError)
    },
    agregar() {
        formActions.validate('')
            .then(({errores}) => {

                if (errores == 0) {
                    let existe = false

                    let {
                        cboCargoPolitico: {value: idCargo, labelOption: cargo},
                        cboParentesco: {value: idParentesco, labelOption: parentesco},
                        cboVinculo: {value: idVinculo, labelOption: vinculo},
                        txtNombre: {value: nombre},
                        txtIncioGestion: {value: inicioGestion},
                        txtFinGestion: {value: finGestion}
                    } = formActions.getJSON('frmPersonasRelacionadas')

                    if (Object.keys(this.personasRelacionadas).length > 0) {
                        for (const key in this.personasRelacionadas) {
                            if (this.personasRelacionadas[key].contador != this.idFila && this.personasRelacionadas[key].nombre == nombre && this.personasRelacionadas[key].idCargo == idCargo) {
                                existe = true
                                break
                            }
                        }
                    }

                    if (existe) {
                        toastr.warning('La persona ya fue agregada')
                    } else {

                        if (idParentesco == 0) {
                            parentesco = ''
                        }

                        let datos = {
                            idParentesco, parentesco,
                            idVinculo, vinculo,
                            idCargo, cargo,
                            nombre,
                            inicioGestion, finGestion
                        }

                        if (this.idFila == 0) {
                            this.contador++
                            datos.contador = this.contador
                            datos.id = 0
                            datos.propietario = 'S'

                            this.personasRelacionadas[this.contador] = {...datos}
                            this.add({...datos})
                        } else {
                            this.personasRelacionadas[this.idFila] = {...this.personasRelacionadas[this.idFila], ...datos}
                            this.editar(this.idFila, {...datos})
                        }
                        tblPersonaRelacionada.columns.adjust().draw()
                        $('#modalPersonaRelacionada').modal('hide')
                    }
                }
            }).catch(generalMostrarError)
    },
    add({contador, vinculo, nombre, parentesco, cargo, inicioGestion, finGestion, propietario}) {

        const buttonEditar = `<button type="button" onclick="gestionTblPersonaRelacionada.mostrarModal(${contador})" class="btnTbl"><i class="fa fa-edit"></i></button>`
        let buttonEliminar = '';

        if (propietario == 'S') {
            buttonEliminar = `<button type="button" onclick="gestionTblPersonaRelacionada.eliminar(${contador})" class="btnTbl"><i class="fa fa-trash"></i></button>`
        }

        tblPersonaRelacionada.row.add([
            contador, vinculo, nombre, parentesco, cargo, inicioGestion, finGestion,
            `<div class="tblButtonContainer">
                ${buttonEditar} ${buttonEliminar}
            </div>`
        ]).node().id = `trPersonaRelacionada${contador}`
    },
    editar(idFila, {vinculo, nombre, parentesco, cargo, inicioGestion, finGestion}) {
        $(`#trPersonaRelacionada${idFila}`).find('td:eq(1)').html(vinculo)
        $(`#trPersonaRelacionada${idFila}`).find('td:eq(2)').html(nombre)
        $(`#trPersonaRelacionada${idFila}`).find('td:eq(3)').html(parentesco)
        $(`#trPersonaRelacionada${idFila}`).find('td:eq(4)').html(cargo)
        $(`#trPersonaRelacionada${idFila}`).find('td:eq(5)').html(inicioGestion)
        $(`#trPersonaRelacionada${idFila}`).find('td:eq(6)').html(finGestion)
    },
    eliminar(contador) {
        tblPersonaRelacionada.row(`#trPersonaRelacionada${contador}`).remove().draw(false)
        delete this.personasRelacionadas[contador]
    }
}

const gestionTblResidencia = {
    idFila: 0,
    contador: 0,
    residencias: {},
    cboNacionalidad: document.getElementById('cboNacionalidad'),
    txtResidencia: document.getElementById('txtLugarDeResidencia'),
    mostrarModal(idFila = 0) {

        this.idFila = idFila

        formActions.clean('formNacionalidades')
            .then(() => {

                this.cboNacionalidad.disabled = false

                let opciones = [`<option selected disabled value="">Seleccione</option>`]
                let idsIncluidos = []

                if (this.idFila == 0) {
                    for (const key in this.residencias) {
                        idsIncluidos.push(this.residencias[key].idPais)
                    }
                }

                for (const key in datosGeograficos) {
                    if (!idsIncluidos.includes(datosGeograficos[key].id)) {
                        opciones.push(`<option value="${datosGeograficos[key].id}">${datosGeograficos[key].nombrePais}</option>`)
                    }
                }

                this.cboNacionalidad.innerHTML = opciones.join('')

                if (this.idFila > 0) {
                    this.cboNacionalidad.value = this.residencias[this.idFila].idPais
                    this.cboNacionalidad.disabled = true

                    this.txtResidencia.value = this.residencias[this.idFila].residencia
                }

                $(`#cboNacionalidad`).selectpicker('refresh')

                $('#modal-residencias').modal('show')

            }).catch(generalMostrarError)
    },
    agregarResidencia() {
        formActions.validate('formNacionalidades')
            .then(({errores}) => {

                if (errores == 0) {
                    const {
                        cboNacionalidad: {
                            value: idPais,
                            labelOption: pais
                        },
                        txtLugarDeResidencia: {
                            value: residencia
                        }
                    } = formActions.getJSON('formNacionalidades');

                    if (this.idFila == 0) {
                        this.contador++
                        const datosResidencia = {
                            contador: this.contador,
                            id: 0,
                            idPais,
                            pais,
                            residencia,
                            propietario: 'S'
                        }
                        this.residencias[this.contador] = {...datosResidencia}
                        this.agregarFila({...datosResidencia})
                    } else {
                        this.residencias[this.idFila].residencia = residencia
                        this.editarFila(this.idFila, {residencia})
                    }

                    tblResidenciaExtrangero.columns.adjust().draw()
                    $('#modal-residencias').modal('hide')
                }

            }).catch(generalMostrarError)
    },
    agregarFila({contador, pais, residencia, propietario}) {

        const btnEdit = `<button type="button" class="btn btn-outline-dark btn-sm" onclick="gestionTblResidencia.mostrarModal(${contador})"><i class="fa fa-edit"></i></button>`;
        let btnEliminar = ''

        if (propietario == 'S') {
            btnEliminar = `<button class="btn btn-sm ms-1 btn-outline-dark" onclick="gestionTblResidencia.eliminarFila(${contador})"><i class="fa fa-trash"></i></button>`
        }

        tblResidenciaExtrangero.row.add([
            contador, pais, residencia,
            `<div class="tblButtonContainer">
                ${btnEdit} ${btnEliminar}
            </div>`
        ]).node().id = `trResidencia${contador}`
    },
    editarFila(idFila, {residencia}) {
        $(`#trResidencia${idFila}`).find('td:eq(2)').html(residencia)
    },
    editar(idFila, {entidad, porcentaje}) {
        $(`#trSociedades${idFila}`).find('td:eq(1)').html(entidad)
        $(`#trSociedades${idFila}`).find('td:eq(2)').html(porcentaje)
    },
    eliminarFila(contador) {
        tblResidenciaExtrangero.row(`#trResidencia${contador}`).remove().draw(false);
        delete this.residencias[contador];
    }
}

const gestionTblCuentaTerceros = {
    idFila: 0,
    contador: 0,
    cat: {},
    mostrarModal(idFila = 0) {
        formActions.clean('formCuentas')
            .then(() => {
                this.idFila = idFila

                if (this.idFila > 0) {
                    document.querySelector('form#formCuentas #txtNombreOtraCuenta').value = this.cat[this.idFila].nombre
                    document.querySelector('form#formCuentas select#cboParentezcosSociedades').value = this.cat[this.idFila].idParentesco
                    document.querySelector('form#formCuentas #txtMotivoOtrasCuenta').value = this.cat[this.idFila].motivo
                    $('#cboParentezcosSociedades').selectpicker('refresh')
                }
                $(`#modal-cuentas`).modal('show')
            }).catch(generalMostrarError)
    },
    agregarOtraCuenta() {

        formActions.validate('formCuentas')
            .then(({errores}) => {

                let existe = false

                if (errores == 0) {
                    const {
                        txtNombreOtraCuenta: {
                            value: nombre
                        },
                        cboParentezcosSociedades: {
                            value: idParentesco,
                            labelOption: parentesco
                        },
                        txtMotivoOtrasCuenta: {
                            value: motivo
                        }
                    } = formActions.getJSON('formCuentas')

                    if (Object.keys(this.cat).length > 0) {
                        for (const key in this.cat) {
                            if (this.cat[key].contador != this.idFila && this.cat[key].nombre == nombre && this.cat[key].parentesco == parentesco) {
                                existe = true
                                break
                            }
                        }
                    }

                    if (existe) {
                        toastr.warning(`La persona ${nombre} ya fue agregada`)
                    } else {
                        let datosOtrasCuentas = {
                            nombre, idParentesco, parentesco, motivo
                        }

                        if (this.idFila == 0) {
                            this.contador++
                            datosOtrasCuentas.contador = this.contador
                            datosOtrasCuentas.id = 0
                            datosOtrasCuentas.propietario = 'S'

                            this.cat[this.contador] = {
                                ...datosOtrasCuentas
                            }
                            this.agregarFila({...datosOtrasCuentas})
                        } else {
                            this.cat[this.idFila] = {...this.cat[this.idFila], ...datosOtrasCuentas}
                            this.editar(this.idFila, {...this.cat[this.idFila]})
                        }

                        tblCuentasTerceros.columns.adjust().draw()
                        $(`#modal-cuentas`).modal('hide')
                    }
                }
            }).catch(generalMostrarError)
    },
    agregarFila({contador, nombre, parentesco, motivo, propietario}) {
        const btnEditar = `<button type="button" class="btnTbl" onclick="gestionTblCuentaTerceros.mostrarModal(${contador})"><i class="fa fa-edit"></i></button>`
        let btnEliminar = ``

        if (propietario == 'S') {
            btnEliminar = `<button type="button" class="btnTbl" onclick="gestionTblCuentaTerceros.eliminar(${contador})"><i class="fa fa-trash-alt"></i></button>`
        }

        tblCuentasTerceros.row.add([
            contador, nombre, parentesco, motivo,
            ` <div class="tblButtonContainer">
                    ${btnEditar} ${btnEliminar}
            </div>`
        ]).node().id = `trOtrasCuentas${contador}`
    },
    editar(idFila, {nombre, parentesco, motivo}) {
        $(`#trOtrasCuentas${idFila}`).find('td:eq(1)').html(nombre)
        $(`#trOtrasCuentas${idFila}`).find('td:eq(2)').html(parentesco)
        $(`#trOtrasCuentas${idFila}`).find('td:eq(3)').html(motivo)
    },
    eliminar(contador) {
        tblCuentasTerceros.row(`#trOtrasCuentas${contador}`).remove().draw(false);
        delete this.cat[contador];
    }
}

const gestionTblSociedades = {
    contador: 0,
    idFila: 0,
    sociedades: {},
    mostrarModal(idFila = 0) {
        this.idFila = idFila

        formActions.clean('formSociedades')
            .then(() => {

                if (this.idFila > 0) {
                    document.querySelector('form#formSociedades input#txtSociedad').value = this.sociedades[this.idFila].entidad
                    document.querySelector('form#formSociedades input#txtProcentaje').value = this.sociedades[this.idFila].porcentaje
                }

                $('#modal-sociedades').modal('show')

            }).catch(generalMostrarError)
    },
    agregarSociedades() {
        formActions.validate('formSociedades')
            .then(({errores}) => {

                if (errores == 0) {
                    let existe = false

                    let {
                        txtSociedad: {
                            value: entidad,
                        },
                        txtProcentaje: {
                            value: porcentaje
                        }
                    } = formActions.getJSON('formSociedades')

                    if (Object.keys(this.sociedades).length > 0) {
                        for (const key in this.sociedades) {
                            if (this.sociedades[key].contador != this.idFila && this.sociedades[key].entidad == entidad) {
                                existe = true
                                break
                            }
                        }
                    }

                    if (existe) {
                        toastr.warning(`Ya existe una empresa con el nombre ${entidad}`)
                    } else {
                        let datosSociedad = {
                            entidad,
                            porcentaje
                        }

                        if (this.idFila == 0) {
                            this.contador++
                            datosSociedad.contador = this.contador
                            datosSociedad.id = 0
                            datosSociedad.propietario = 'S'

                            this.sociedades[this.contador] = {...datosSociedad}
                            this.agregarFila({...datosSociedad})
                        } else {
                            this.sociedades[this.idFila] = {...this.sociedades[this.idFila], ...datosSociedad}
                            this.editar(this.idFila, {...this.sociedades[this.idFila]})
                        }

                        $(`#modal-sociedades`).modal('hide')
                        tblSociedades.columns.adjust().draw()
                    }
                }
            }).catch(generalMostrarError)
    },
    agregarFila({contador, entidad, porcentaje, propietario}) {
        const btnEdit = `<button type="button" class="btnTbl" onclick="gestionTblSociedades.mostrarModal( ${contador})"><i class="fa fa-edit"></i></button>`;
        let btnEliminar = ''

        if (propietario == 'S') {
            btnEliminar = `<button type="button" class="btnTbl" onclick="gestionTblSociedades.eliminarFila( '${contador}')"><i class="fa fa-trash-alt"></i></button>`;
        }

        tblSociedades.row.add([
            contador, entidad, porcentaje,
            `<div class="tblButtonContainer">
                ${btnEdit} ${btnEliminar}
            </div>`
        ]).node().id = `trSociedades${contador}`
    },
    editar(idFila, {entidad, porcentaje}) {
        $(`#trSociedades${idFila}`).find('td:eq(1)').html(entidad)
        $(`#trSociedades${idFila}`).find('td:eq(2)').html(porcentaje)
    },
    eliminarFila(contador) {
        tblSociedades.row(`#trSociedades${contador}`).remove().draw()
        delete this.sociedades[contador]
    }
}

const gestionTblProductos = {
    idFila: 0,
    contador: 0,
    catalogo: {},
    productos: {},
    cboProductos: document.querySelector('form#formProductos select#cboProductoContratados'),
    txtProposito: document.querySelector('form#formProductos input#txtProposito'),
    mostrarModal(idFila = 0) {
        this.idFila = idFila

        formActions.clean('formProductos')
            .then(() => {

                this.cboProductos.disabled = false
                let opciones = [`<option selected disabled value="">Seleccione</option>`]
                let idsIncluidos = []

                if (this.idFila == 0) {
                    for (const key in this.productos) {
                        idsIncluidos.push(this.productos[key].idProducto)
                    }
                }

                for (const key in this.catalogo) {
                    if (!idsIncluidos.includes(this.catalogo[key].id)) {
                        opciones.push(`<option value="${this.catalogo[key].id}">${this.catalogo[key].nombre}</option>`)
                    }
                }

                this.cboProductos.innerHTML = opciones.join('')

                if (this.idFila > 0) {
                    if (this.productos[this.idFila].id > 0) {
                        this.cboProductos.disabled = true
                    }
                    this.cboProductos.value = this.productos[this.idFila].idProducto
                    this.txtProposito.value = this.productos[this.idFila].proposito
                }

                $(`#${this.cboProductos.id}`).selectpicker('refresh')
                $(`#modal-productos`).modal('show')

            }).catch(generalMostrarError)
    },
    agregar() {
        formActions.validate('formProductos')
            .then(({errores}) => {

                if (errores == 0) {
                    const {
                        cboProductoContratados: {
                            value: id,
                            labelOption: producto
                        },
                        txtProposito: {
                            value: proposito
                        }
                    } = formActions.getJSON('formProductos')

                    let datosProductos = {
                        idProducto: id, producto, proposito
                    }

                    if (this.idFila == 0) {
                        this.contador++
                        datosProductos.id = 0
                        datosProductos.contador = this.contador
                        datosProductos.propietario = 'S'

                        this.productos[this.contador] = {
                            ...datosProductos
                        }
                        this.agregarFila({...datosProductos})

                    } else {
                        this.productos[this.idFila].proposito = proposito
                        this.editar(this.idFila, {proposito})
                    }

                    $(`#modal-productos`).modal('hide')
                    tblProductosContratos.columns.adjust().draw()
                }
            }).catch(generalMostrarError)
    },
    agregarFila({contador, producto, proposito, propietario}) {
        const btnEditar = `<button type="button" class="btnTbl" onclick="gestionTblProductos.mostrarModal(${contador})"><i class="fa fa-edit"></i></button>`
        let btnEliminar = ``

        if (propietario == 'S') {
            btnEliminar = `<button type="button" class="btnTbl" onclick="gestionTblProductos.eliminar(${contador})"><i class="fa fa-trash-alt"></i></button>`
        }

        tblProductosContratos.row.add([
            contador, producto, proposito,
            `<div class="tblButtonContainer">
                ${btnEditar} ${btnEliminar}
             </div>`
        ]).node().id = `trProductos${contador}`

    },
    editar(idFila, {proposito}) {
        $(`#trProductos${idFila}`).find('td:eq(2)').html(proposito)
    },
    eliminar(contador) {
        tblProductosContratos.row(`#trProductos${contador}`).remove().draw(false);
        delete this.productos[contador];
    }
}

const gestionResidenciaRepresentante = {
    idFila: 0,
    residencias: {},
    contador: 0,
    cboNacionalidad: document.querySelector('form#formNacionalidadesRepresentante select#cboNacionalidadRepresentante'),
    txtResidencia: document.querySelector('form#formNacionalidadesRepresentante #txtResidenciaRepresentante'),
    mostrarModal(idFila = 0) {
        this.idFila = idFila
        formActions.clean('formNacionalidadesRepresentante')
            .then(() => {

                this.cboNacionalidad.disabled = false

                let opciones = [`<option selected disabled value="">Seleccione</option>`]
                let idsIncluidos = []

                if (this.idFila == 0) {
                    for (const key in this.residencias) {
                        idsIncluidos.push(this.residencias[key].idPais)
                    }
                }

                for (const key in datosGeograficos) {
                    if (!idsIncluidos.includes(datosGeograficos[key].id)) {
                        opciones.push(`<option value="${datosGeograficos[key].id}">${datosGeograficos[key].nombrePais}</option>`)
                    }
                }

                this.cboNacionalidad.innerHTML = opciones.join('')

                if (this.idFila > 0) {
                    if (this.residencias[this.idFila].id > 0) {
                        this.cboNacionalidad.disabled = true
                    }
                    this.cboNacionalidad.value = this.residencias[this.idFila].idPais
                    this.txtResidencia.value = this.residencias[this.idFila].residencia
                }

                $(`#cboNacionalidadRepresentante`).selectpicker('refresh')

                $('#modal-representante').modal('show')

            }).catch(generalMostrarError)
    },
    agregar() {

        formActions.validate('formNacionalidadesRepresentante')
            .then(({errores}) => {

                if (errores == 0) {
                    const {
                        cboNacionalidadRepresentante: {
                            value: idPais,
                            labelOption: pais
                        },
                        txtResidenciaRepresentante: {
                            value: residencia
                        }
                    } = formActions.getJSON('formNacionalidadesRepresentante')

                    let datosResidencia = {
                        idPais, pais, residencia
                    }

                    if (this.idFila == 0) {
                        this.contador++
                        datosResidencia.contador = this.contador
                        datosResidencia.id = 0
                        this.residencias[this.contador] = {...datosResidencia}
                        this.addFila({...datosResidencia})
                    } else {
                        this.residencias[this.idFila] = {...this.residencias[this.idFila], ...datosResidencia}
                        this.editar(this.idFila, {...this.residencias[this.idFila]})
                    }

                    tblResidenciaRepresentante.columns.adjust().draw()
                    $(`#modal-representante`).modal('hide')

                }
            }).catch(generalMostrarError)
    },
    addFila({contador, pais, residencia}) {

        const btnEditar = `<button type="button" class="btnTbl" onclick="gestionResidenciaRepresentante.mostrarModal(${contador})"><i class="fa fa-edit"></i></button>`
        const btnEliminar = `<button type="button" class="btnTbl" onclick="gestionResidenciaRepresentante.eliminar(${contador})"><i class="fa fa-trash-alt"></i></button>`

        tblResidenciaRepresentante.row.add([
            contador, pais, residencia,
            `<div class="tblButtonContainer">
                ${btnEditar} ${btnEliminar}
            </div>`
        ]).node().id = `trNcionalidad${contador}`
    },
    editar(idFila, {pais, residencia}) {
        $(`#trNcionalidad${idFila}`).find('td:eq(1)').html(pais)
        $(`#trNcionalidad${idFila}`).find('td:eq(2)').html(residencia)
    },
    eliminar(contador) {
        tblResidenciaRepresentante.row(`#trNcionalidad${contador}`).remove().draw();
        delete this.residencias[contador];
    }
}

const gestionTblCargosRepresentante = {
    idFila: 0,
    contador: 0,
    cargosPoliticos: {},
    mostrarModal(idFila = 0) {

        formActions.clean('formCargosRepresentante')
            .then(() => {
                this.idFila = idFila

                if (this.idFila > 0) {
                    document.querySelector('#formCargosRepresentante select#cboCargoPoliticoRepresentante').value = this.cargosPoliticos[idFila].idCargo;
                    document.querySelector('#formCargosRepresentante input#txtIncioGestion').value = this.cargosPoliticos[idFila].inicioGestion;
                    document.querySelector('#formCargosRepresentante input#txtFinGestion').value = this.cargosPoliticos[idFila].finGestion;

                    $('#cboCargoPoliticoRepresentante').selectpicker('refresh');
                }

                $('#formCargosRepresentante input#txtIncioGestion').datepicker('update')
                $('#formCargosRepresentante input#txtFinGestion').datepicker('update')
                $('#modal-representante-cargos').modal('show')

            }).catch(generalMostrarError)
    },
    agregar() {
        formActions.validate('formCargosRepresentante')
            .then(({errores}) => {

                if (errores == 0) {
                    let existe = false

                    const {
                        cboCargoPoliticoRepresentante: {
                            value: idCargo, labelOption: cargo
                        },
                        txtIncioGestion: {value: inicioGestion},
                        txtFinGestion: {value: finGestion}
                    } = formActions.getJSON('formCargosRepresentante')

                    if (Object.keys(this.cargosPoliticos).length > 0) {
                        for (const key in this.cargosPoliticos) {
                            if (this.cargosPoliticos[key].contador != this.idFila && this.cargosPoliticos[key].idCargo == idCargo) {
                                existe = true
                                break
                            }
                        }
                    }

                    if (existe) {
                        toastr.warning('El cargo ya fue agregado, verifique la información')
                    } else {
                        const datos = {
                            idCargo, cargo, inicioGestion, finGestion
                        }

                        if (this.idFila == 0) {
                            this.contador++
                            datos.contador = this.contador;
                            datos.id = 0
                            this.cargosPoliticos[this.contador] = {...datos}
                            this.agregarFila({...datos})

                        } else {
                            this.cargosPoliticos[this.idFila] = {...this.cargosPoliticos[this.idFila], ...datos}
                            this.editar(this.cargosPoliticos[this.idFila])
                        }
                        tblCargosPoliticos.columns.adjust().draw();
                        $('#modal-representante-cargos').modal('hide')
                        // generalLimpiarContenedor('formCargosRepresentante')
                    }

                }

            }).catch(generalMostrarError)
    },
    agregarFila({contador, cargo, inicioGestion, finGestion}) {
        const buttonEditar = `<button type="button" onclick="gestionTblCargosRepresentante.mostrarModal(${contador})" class="btnTbl"><i class="fa fa-edit"></i></button>`
        const buttonEliminar = `<button type="button" onclick="gestionTblCargosRepresentante.eliminar(${contador})" class="btnTbl"><i class="fa fa-trash"></i></button>`

        tblCargosPoliticos.row.add([
            contador, cargo, inicioGestion, finGestion,
            `<div class="tblButtonContainer">
                ${buttonEditar} ${buttonEliminar}
            </div>`
        ]).node().id = `trRepresentanteCargos${contador}`
    },
    editar({contador, cargo, inicioGestion, finGestion}) {
        $(`#trRepresentanteCargos${contador}`).find('td:eq(1)').html(cargo)
        $(`#trRepresentanteCargos${contador}`).find('td:eq(2)').html(inicioGestion)
        $(`#trRepresentanteCargos${contador}`).find('td:eq(3)').html(finGestion)
    },
    eliminar(idFila) {
        tblCargosPoliticos.row(`#trRepresentanteCargos${idFila}`).remove().draw();
        delete this.cargosPoliticos[idFila];
    }
}

const limpiarSolicitud = () => {

    Swal.fire({
        title: '¿Estas seguro de cancelar la solicitud?',
        text: "¡ Los datos del formulario serán eliminados !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Limpiar y salir',
        cancelButtonText: 'Limpiar'
    }).then((result) => {
        if (result.isConfirmed) {
            window.location = `?page=controlesListSolicitudesPeps&mod=${btoa('CONTROLES')}`

        } else if (result.dismiss === Swal.DismissReason.cancel) {
            window.location = `?page=controlesPeps&mod=${btoa('CONTROLES')}`
        }
    })
}

const esPep = (cbo) => {

    formActions.clean('formAntecedentes')
        .then(() => {

            const inputs = document.querySelectorAll('form#formAntecedentes .form-control')
            const selects = document.querySelectorAll('form#formAntecedentes .selectpicker')

            let estado = true

            if (cbo.options[cbo.selectedIndex].text === 'PEP') {
                estado = false
            }

            inputs.forEach(input => {
                if (input.id != 'txtInicioGestionAntecedentes' && input.id != 'txtFinGestionAntecedentes') {
                    input.readOnly = estado
                } else {
                    input.disabled = estado
                }
            })

            selects.forEach(select => {
                select.disabled = estado
                $('#' + select.id).selectpicker('refresh')
            })

        }).catch(generalMostrarError)

}

const tipoSolicitud = {
    contador: 0,
    valorAnterior: 0,
    inputs: document.querySelectorAll('form#formDatosPersonales input.form-control'),
    selects: document.querySelectorAll('form#formDatosPersonales select.selectpicker'),
    buttons: document.querySelectorAll('button.buttonModalPrincipal'),
    btnBuscar: document.getElementById('btnBuscarAscociado'),
    cambiar(opctionSeleccionado) {
        this.contador++

        formActions.clean('formDatosPersonales')
            .then(() => {

                let estado = true
                let nuevo = true
                this.btnBuscar.disabled = true

                if (opctionSeleccionado === 'RECURRENTE') {
                    estado = false
                    nuevo = false
                    this.btnBuscar.disabled = false
                }

                this.inputs.forEach(input => {
                    if (nuevo) {
                        input.readOnly = input.id != 'txtCodigoAsociado' && input.id != 'txtDocumentoPrimario' ? !estado : estado

                        if (input.id == 'txtFechaExpedicion' || input.id == 'txtFechaNacimiento') {
                            input.disabled = false
                        }
                    } else {
                        if (input.id == 'txtFechaExpedicion' || input.id == 'txtFechaNacimiento') {
                            input.disabled = true
                        } else {
                            input.readOnly = input.id == 'txtCodigoAsociado' ? estado : !estado
                        }
                    }
                })

                this.selects.forEach(select => {
                    select.disabled = !estado
                    $(`#${select.id}`).selectpicker('refresh')
                })

                this.buttons.forEach(btn => btn.disabled = false)

            }).catch(generalMostrarError)
    },
    change(cbo) {

        if (this.contador > 0) {
            Swal.fire({
                title: '¿Estas seguro de cambiar el tipo de solicitud?',
                text: "¡ Los datos del formulario se perderán!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(result => {
                if (result.isConfirmed) {
                    this.valorAnterior = cbo.value
                    this.cambiar(cbo.options[cbo.selectedIndex].text)
                } else {
                    document.getElementById('cboTipoSolicitud').value = this.valorAnterior;
                    $(`#cboTipoSolicitud`).selectpicker('refresh')
                }
            })
        } else {
            this.valorAnterior = cbo.value
            this.cambiar(cbo.options[cbo.selectedIndex].text)
        }
    }
}

const esRepresentante = {
    contador: 0,
    confirm: false,
    valorAnterior: 0,
    cambiar(optionSeleccionado) {

        this.contador++

        formActions.clean('frmDatosRepresentante')
            .then(() => {

                let estado = true
                const inputs = document.querySelectorAll('form#frmDatosRepresentante .form-control')
                const selects = document.querySelectorAll('form#frmDatosRepresentante .selectpicker')
                const buttons = document.querySelectorAll('.buttonModal')

                if (optionSeleccionado === 'Representante/Apoderado') {
                    estado = false
                }

                inputs.forEach(input => {
                    if (input.id != 'txtDocumentoPrimarioRepresentante') {
                        input.readOnly = estado
                    }
                })

                selects.forEach(select => {
                    select.disabled = estado
                    $(`#${select.id}`).selectpicker('refresh')
                })

                buttons.forEach(btn => {
                    btn.disabled = estado
                })
            }).catch(generalMostrarError)
    },
    change(cbo) {

        if (this.contador > 0 && cbo.options[cbo.selectedIndex].text === 'Persona') {
            Swal.fire({
                title: '¿Estas seguro de ejecutar este cambio?',
                text: "¡ Los datos del formulario de representante se perderán!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Cambiar',
                cancelButtonText: 'Cancelar'
            }).then(result => {
                if (result.isConfirmed) {
                    this.valorAnterior = cbo.value
                    this.cambiar(cbo.options[cbo.selectedIndex].text)
                } else {
                    console.log(this.valorAnterior)
                    document.getElementById('cboRepresentante').value = this.valorAnterior;
                    $(`#cboRepresentante`).selectpicker('refresh')
                }
            })
        } else {
            this.valorAnterior = cbo.value
            this.cambiar(cbo.options[cbo.selectedIndex].text)
        }
    }
}

const cliente = {
    comprobarCodigoCliente() {

        const txtNombreAsociado = document.getElementById('txtCodigoAsociado')
        const codigo = txtNombreAsociado.value

        if (codigo) {
            generalVerificarCodigoCliente(codigo)
                .then(({respuesta, asociado}) => {

                    $('.preloader').fadeOut('fast');
                    $(".preloader").find("span").html("");

                    if (respuesta) {

                        document.getElementById('txtCodigoAsociado').value = codigo
                        document.getElementById('txtNombres').value = asociado.nombresAsociado
                        document.getElementById('txtApellidos').value = asociado.apellidosAsociado
                        document.getElementById('txtTelefono').value = asociado.telefonoFijo
                        document.getElementById('txtEmail').value = asociado.email
                        document.getElementById('txtFechaNacimiento').value = asociado.fechaNacimiento
                        $(`#txtFechaNacimiento`).datepicker('update')
                        document.getElementById('txtDireccion').value = asociado.direccionCompleta
                        document.getElementById('txtProfesion').value = asociado.profesion

                        document.getElementById('txtDocumentoPrimario').value = asociado.numeroDocumentoPrimario
                        document.getElementById('cboTipoDocumento').value = asociado.tipoDocumentoPrimario

                        $('#cboPais').selectpicker('val', asociado.pais);
                        $('#cboPais').trigger('change')

                        $('#cboDepartamento').selectpicker('val', asociado.departamento);
                        $('#cboDepartamento').trigger('change')

                        $('#cboMunicipio').selectpicker('val', asociado.municipio);
                        $('#cboActividadEconomicaPrincipal').selectpicker('val', asociado.actividadPrimaria);
                        $('#cboActividadEconomicaSecundaria').selectpicker('val', asociado.actividadSecundaria);
                        document.getElementById('cboEstadoCivil').value = asociado.estadoCivil

                        tipoSolicitud.inputs.forEach(input => {

                            if (input.id != 'txtFechaExpedicion' && input.id != 'txtFechaNacimiento') {
                                input.readOnly = false
                            } else {
                                input.disabled = false
                            }
                        })

                        tipoSolicitud.selects.forEach(select => {
                            select.disabled = false
                            $(`#${select.id}`).selectpicker('refresh')
                        })

                        txtNombreAsociado.readOnly = true

                    } else {
                        //btnBuscar.disabled = false
                        txtNombreAsociado.readOnly = false
                        toastr.warning('El asociado no existe')
                    }
                }).catch(console.error)
        } else {
            toastr.warning('Ingresa un codigo para comprobar')
        }

    },
    comprobarCodigoClientePorEnter() {
        if (event.keyCode == 13) {
            this.comprobarCodigoCliente()
        }
    }
}

const guardarSolicitud = {
    validacion: true,
    mensaje: '',
    cboRepresentante: document.getElementById('cboRepresentante'),
    cboTipoAfiliacion: document.getElementById('cboTipoAfiliacion'),
    guardar() {
        formActions.validate('formTiposSolicitud')
            .then( ( { errores } ) => {

                if (errores == 0) {
                    this.validacion = formActions.manualValidate('formDatosPersonales')

                    if (this.validacion && this.cboTipoAfiliacion.options[this.cboTipoAfiliacion.selectedIndex].text == 'PEP')
                    {
                        if ( this.validacion && !formActions.manualValidate('formAntecedentes'))
                        {
                            this.validacion = false
                            toastr.warning('Complete los campos')
                            document.getElementById('formAntecedentes').scrollIntoView({behavior: "auto", block: "center", inline: "end"})
                        }
                    } else if( this.validacion && this.cboTipoAfiliacion.options[this.cboTipoAfiliacion.selectedIndex].text == 'RELACIONADO A PEP' ) {

                        if ( Object.keys( gestionTblPersonaRelacionada.personasRelacionadas ).length == 0 )
                        {
                            toastr.warning('Debe agregar al una persona relacionada a la solicitud')
                            this.validacion = false
                            document.getElementById('tblPersonaRelacionada').scrollIntoView({behavior: "smooth", block: "center", inline: "end"})
                        }
                    }

                    if ( this.validacion &&  Object.keys( gestionTblProductos.productos).length == 0 )
                    {
                        this.validacion = false
                        toastr.warning('Agrega al menos un productos o servicio')
                        document.getElementById('contenedorTab').scrollIntoView({behavior: "smooth", block: "center", inline: "end"})
                        $('#productos-tab').trigger('click')
                    }

                    if (this.validacion && this.cboRepresentante.options[this.cboRepresentante.selectedIndex].text == 'Representante/Apoderado') {

                        if( ! formActions.manualValidate('frmDatosRepresentante') )
                        {
                            this.validacion = false
                            $('#representante-tab').trigger('click')
                            toastr.warning('Completa los datos del Representante/Apoderado')
                        }
                    }

                    if (this.validacion) {
                        datosSolicitud['datosGenerales'] = {id: idSolicitud, ...formActions.getJSON('formTiposSolicitud') }
                        datosSolicitud['datosPersonales'] = { ...formActions.getJSON('formDatosPersonales') }
                        datosSolicitud['antecedentes'] = {idAntecedentes, ...formActions.getJSON('formAntecedentes')}
                        datosSolicitud['residencias'] = {...gestionTblResidencia.residencias}
                        datosSolicitud['personaRelacionada'] = {...gestionTblPersonaRelacionada.personasRelacionadas}
                        datosSolicitud['otrasCuentas'] = {...gestionTblCuentaTerceros.cat}
                        datosSolicitud['sociedades'] = {...gestionTblSociedades.sociedades}
                        datosSolicitud['productos'] = {...gestionTblProductos.productos}
                        datosSolicitud['Representante'] = {
                            idRepresentante,
                            ... formActions.getJSON('frmDatosRepresentante'),
                            residencias: {...gestionResidenciaRepresentante.residencias},
                            cargosPoliciticos: {...gestionTblCargosRepresentante.cargosPoliticos}
                        }

                        // console.log( JSON.stringify( datosSolicitud ) )
                        // return

                        fetchActions.set({
                            modulo, archivo: 'Peps/controlesGuardarSolicitudPep',
                            datos: { ...datosSolicitud, idApartado, tipoApartado }
                        }).then( response => {
                            switch (response.respuesta.trim()) {
                                case 'EXITO':

                                    if ( idSolicitud == 0)
                                    {
                                        Swal.fire({
                                            title: "¡Bien hecho!",
                                            html: "Datos guardados exitosamente.<br />¿Desea salir del formulario?",
                                            icon: "success",
                                            showCancelButton: true,
                                            showDenyButton: true,
                                            focusConfirm: false,
                                            allowOutsideClick: false,
                                            confirmButtonText: '<i class="fa fa-edit"></i> Nueva solicitud',
                                            cancelButtonText: '<i class="fa fa-print"></i> Imprimir solicitud',
                                            denyButtonText: '<i class="fa fa-times"></i> Salir del formulario',
                                        }).then(result => {
                                            if (result.isDenied) {
                                                window.top.location.href = `./?page=controlesListSolicitudesPeps&mod=${encodeURIComponent(btoa('CONTROLES'))}`;
                                            } else if (!result.isConfirmed) {
                                                construirSolicitudPep( response.idSolicitud )
                                                //window.top.location.href = `./?page=controlesListSolicitudesPeps&mod=${encodeURIComponent(btoa('CONTROLES'))}`;
                                            } else
                                            {
                                                window.top.location.href = `./?page=controlesPeps&mod=${encodeURIComponent(btoa('CONTROLES'))}`;
                                            }
                                        })
                                    } else {
                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: '¡Bien hecho!',
                                            text: 'Los datos fueron actualizados con exito',
                                            showConfirmButton: false,
                                            timer: 1500
                                        }).then( () => {
                                            window.top.location.href = `./?page=controlesListSolicitudesPeps&mod=${encodeURIComponent(btoa('CONTROLES'))}`;
                                        })
                                    }
                                    break;
                                default:
                                    generalMostrarError(response);
                                    break;
                            }
                        }).catch( generalMostrarError )
                    }
                }
            }).catch(generalMostrarError)
    }
}

const construirSolicitudPep = ( idSolicitud ) => {
    fetchActions.get({
        modulo, archivo: 'Peps/controlesSolicitudPDF', params: { idSolicitud }
    }).then( response => {

        switch (response.respuesta.trim()) {
            case 'EXITO':
                    userActions.abrirArchivo( modulo, response.nombreArchivo, 'solicitudesPeps', 'S');
                break;
            default:
                generalMostrarError(response);
                break;
        }
    }).catch( generalMostrarError )
}

const calcularTotalDeIngresos = () => {

    const remuneracionMensual = document.getElementById('txtRemuneracionMensual'),
        otrosIngresos = document.getElementById('txtOtrosIngresos'),
        totalIngresos = document.getElementById('txtTotalIngresos');

    valorIngresosMensual = remuneracionMensual.value.length > 0 ? parseFloat(remuneracionMensual.value) : 0;
    valorOtrosIngresos = otrosIngresos.value.length > 0 ? parseFloat(otrosIngresos.value) : 0;

    totalIngresos.value = parseFloat(valorOtrosIngresos + valorIngresosMensual).toFixed(2);
}



