let tblReporteRetiroAsociados, tblSolicitudesPagadas
const modulo = 'CONTROLES'

window.onload = () => {

    document.getElementById('formSolcitudesPagadas').reset()
    document.getElementById('formReporteRetiroAsociados').reset()

    const cboAgencia = document.querySelectorAll('select.cboAgencia')

    initDataTable().then(() => {

        fetchActions.getCats({
            modulo, archivo: 'controlesGetCats', solicitados: ['agenciasAsignadas']
        }).then(({agenciasAsignadas = []}) => {

            let opciones = [`<option value="all" selected>Todas las agencias</option>`]

            agenciasAsignadas.forEach(agencia => {
                if (agencia.id != 700) {
                    opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                }
            })

            cboAgencia.forEach(cbo => {
                cbo.innerHTML = opciones.join()
                $(`#${cbo.id}`).selectpicker('refresh')
            })
        })
        $('.preloader').fadeOut('fast')
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblReporteRetiroAsociados').length > 0) {
                tblReporteRetiroAsociados = $('#tblReporteRetiroAsociados').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                    }, {
                        targets: [1, 2, 3, 4],
                        className: "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de la solicitud';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblReporteRetiroAsociados.columns.adjust().draw();
            }

            if ($('#tblSolicitudesPagadas').length > 0) {
                tblSolicitudesPagadas = $('#tblSolicitudesPagadas').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                    }, {
                        targets: [1, 2, 3, 4],
                        className: "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de la solicitud';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSolicitudesPagadas.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const solicitudesAprobadas = {
    contador: 0,
    solicitudes: {},
    txtInicio: document.getElementById('txtInicioG'),
    txtFin: document.getElementById('txtFinG'),
    cboAgencia: document.getElementById('cboAgencia'),
    cboEstado : document.getElementById('cboEstado'),
    obtenerSolicitudes() {

        formActions.validate('formReporteRetiroAsociados')
            .then(() => {

                const {
                    cboEstado: {labelOption: estado},
                    cboAgencia: {value: idAgencia},
                    txtInicioG, txtFinG
                } = formActions.getJSON('formReporteRetiroAsociados')

                fetchActions.set({
                    modulo, archivo: 'controlesReporteRetiroAsociados', datos: {
                        accion: 'SolicitudGeneral', idAgencia, txtInicioG, txtFinG, estado
                    }
                }).then(({solicitudes = []}) => {

                    solicitudes = Object.keys(solicitudes).map((key) => solicitudes[key]);

                    this.contador = 0
                    this.solicitudes = {}
                    tblReporteRetiroAsociados.clear()

                    if (solicitudes.length > 0) {
                        this.cboAgencia.disabled = true
                        $(`#${this.cboAgencia.id}`).selectpicker('refresh')

                        this.cboEstado.disabled = true
                        $(`#${this.cboEstado.id}`).selectpicker('refresh')

                        this.txtInicio.disabled = true
                        this.txtFin.disabled = true
                        document.getElementById('btnExcelG').classList.remove('disabled')

                        solicitudes.forEach(solicitud => {
                            if (solicitud.resolucion == estado) {
                                this.contador++
                                this.solicitudes[this.contador] = {...solicitud}
                                this.agregarFila({ contador: this.contador, ...solicitud })
                            }
                        })
                    } else {
                        toastr.warning('No hay datos para mostrar')
                        document.getElementById('btnExcelG').classList.add('disabled')
                    }


                    tblReporteRetiroAsociados.columns.adjust().draw()

                }).catch(generalMostrarError)
            })
    },
    generarReporte() {
        if (Object.keys(this.solicitudes).length > 0) {
            fetchActions.set({
                modulo, archivo: 'controlesExcelSolicitudesAprobadas', datos:
                    {
                        desde: this.txtInicio.value,
                        hasta: this.txtFin.value,
                        solicitudes: {...this.solicitudes}
                    }
            }).then(response => {

                switch (response.respuesta.trim()) {
                    case 'EXITO':
                        window.open('./main/CONTROLES/docs/solicitudRetiro/Excel/' + response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }
            }).catch(generalMostrarError)
        } else {
            toastr.warning('No hay datos para exportar')
        }
    },
    agregarFila({
                    contador,
                    fechaRegistro,
                    codigoCliente,
                    nombresAsociado,
                    agencia,
                    productos: {Aportaciones, Ahorros},
                    fechaResolucion
                }) {
        tblReporteRetiroAsociados.row.add([
            contador, fechaRegistro, codigoCliente, nombresAsociado, agencia, `$ ${Aportaciones}`, `$ ${Ahorros}`, fechaResolucion
        ]).node().id = `trSolicitudesAprobadas${contador}`
    },
    limpiar() {
        formActions.clean('formReporteRetiroAsociados')
            .then(() => {

                tblReporteRetiroAsociados.clear().draw()
                this.solicitudes = {}
                this.contador = 0

                this.cboAgencia.disabled = false
                $(`#${this.cboAgencia.id}`).selectpicker('refresh')

                this.cboEstado.disabled = false
                $(`#${this.cboEstado.id}`).selectpicker('refresh')

                this.txtInicio.disabled = false
                this.txtFin.disabled = false
            })
    },
    cambioFecha() {
        $("#txtFinG").datepicker('destroy')

        $("#txtFinG").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        document.getElementById('txtFinG').value = ''
        $("#txtFinG").datepicker('update')
    },
}

const solicitudesPagadas = {
    contador: 0,
    solicitudes: {},
    txtInicio: document.getElementById('txtFechaInicio'),
    txtFin: document.getElementById('txtFechaFin'),
    cboAgencia: document.getElementById('cboAgenciaP'),
    obtenerSolicitudes() {
        tblSolicitudesPagadas.clear()
        this.contador = 0
        this.solicitudes = {}

        const {
            cboAgenciaP: {value: idAgencia},
            txtFechaInicio, txtFechaFin
        } = formActions.getJSON('formSolcitudesPagadas')

        fetchActions.set({
            modulo, archivo: 'controlesReporteRetiroAsociados', datos: {
                idAgencia, txtFechaInicio, txtFechaFin
            }
        }).then(({ solicitudes = [] }) => {

            solicitudes = Object.keys(solicitudes).map((key) => solicitudes[key]);

            if (solicitudes.length > 0) {
                this.txtInicio.readOnly = true
                this.txtFin.readOnly = true

                this.cboAgencia.disabled = true;
                $(`#${this.cboAgencia.id}`).selectpicker('refresh')

                document.getElementById('btnExcelP').classList.remove('disabled')

                solicitudes.forEach(solicitud => {
                    this.contador++
                    this.solicitudes[this.contador] = {...solicitud}
                    this.agregarFila({
                        contador: this.contador, ...solicitud
                    })
                })

            } else {
                toastr.warning('No hay registro para mostrar')
            }

            tblSolicitudesPagadas.columns.adjust().draw()
        }).catch(generalMostrarError)
    },
    exportarDatos() {
        fetchActions.set({
            modulo, archivo: 'controlesExcelSolicitudesPagadas', datos: {
                desde: this.txtInicio.value,
                hasta: this.txtFin.value,
                solicitudes: {...solicitudesPagadas.solicitudes}
            }
        }).then(response => {

            switch (response.respuesta.trim()) {
                case 'EXITO':
                    window.open('./main/CONTROLES/docs/solicitudRetiro/Excel/' + response.nombreArchivo, '_blank');
                    break;
                default:
                    generalMostrarError(response);
                    break;
            }

        }).catch(generalMostrarError)
    },
    agregarFila({contador, codigoCliente, nombresAsociado, monto, agencia, fechaPago}) {
        tblSolicitudesPagadas.row.add([
            contador, codigoCliente, nombresAsociado, `$ ${monto}`, agencia, fechaPago
        ]).node().id = `trSolicitudPagada${contador}`
    },
    cambioFecha(txtfechaInicio) {
        $("#txtFechaFin").datepicker('destroy')

        $("#txtFechaFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: txtfechaInicio.value ? txtfechaInicio.value : false,
            endDate: 'now'
        });

        document.getElementById('txtFechaFin').value = ''
        $("#txtFechaFin").datepicker('update')
    },
    limpiar() {
        formActions.clean('formSolcitudesPagadas')
            .then(() => {
                document.getElementById('btnExcelP').classList.add('disabled')
                tblSolicitudesPagadas.clear().draw()
                solicitudesPagadas.solicitudes = {}

                this.txtInicio.readOnly = false
                this.txtFin.readOnly = false

                this.cboAgencia.disabled = false
                $(`#${this.cboAgencia.id}`).selectpicker('refresh')
            })
    }
}

