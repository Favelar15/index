let tblRoles, tblMotivos
const modulo = 'CONTROLES'

window.onload = () => {

    fetchActions.get({
        modulo, archivo: 'controlesGetConfiguracionSolicitud', params: {}
    }).then( ( { configuracion = [] }) => {

        configuracion.forEach( config => {
            configRoles.contador++

            configRoles.roles[configRoles.contador] = { idRol: config.idRol, rol: config.rol, motivos: { }}

            let motivosTemp = config.motivos
            motivosTemp.forEach( motivo => {
                configMotivos.contador++
                configRoles.roles[configRoles.contador].motivos[configMotivos.contador] = { idMotivo: motivo.idMotivo, motivo: motivo.motivo }
            })
            configRoles.agregarFila( configRoles.contador, {...config })
        })
        tblRoles.columns.adjust().draw()

    }).catch( generalMostrarError )

    initDataTable().then(() => {
        $('.preloader').fadeOut('fast')

        fetchActions.getCats({
            modulo, archivo: 'controlesGetCats', solicitados: ['roles', 'motivosRetiro', 'rolesCbo']
        }).then(({motivosRetiro = [], roles = []}) => {

            configMotivos.catMotivos = [...motivosRetiro]
            configRoles.catRoles = [...roles]

            $('#tblRoles tbody').on('click', 'td', function () {
                if ($(this).closest('tr').attr("id") && $(this).index() != 2) {
                    if ($(this).closest('tr').hasClass('selected')) {
                        $(this).closest('tr').removeClass('selected');
                        configRoles.idFila = 0
                        document.getElementById('btnAddMotivo').disabled = true
                    } else {
                        tblRoles.$('tr.selected').removeClass('selected');
                        $(this).closest('tr').addClass('selected');
                        let id = $(this).closest('tr').attr("id").replace("trRol", "");
                        configRoles.idFila = id
                        configMotivos.cargarMotivos ()
                        document.getElementById('btnAddMotivo').disabled = false
                    }
                }
            });
        }).catch(generalMostrarError)
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {

            if ($('#tblRoles').length > 0) {
                tblRoles = $('#tblRoles').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                        width: '10%'
                    },
                        {
                            targets: [2],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblRoles.columns.adjust().draw();
            }

            if ($('#tblMotivos').length > 0) {
                tblMotivos = $('#tblMotivos').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                        width: '10%'
                    },
                        {
                            targets: [2],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblMotivos.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const configRoles = {
    contador: 0,
    idFila : 0,
    catRoles: [],
    roles: {},
    cboRoles: document.getElementById('cboRoles'),
    mostrarModal() {
        let rolesIds = []
        let opciones = [`<option value="" selected disabled>seleccione</option>`]

        for (const key in this.roles) {
            rolesIds.push(this.roles[key].idRol)
        }
        for (const key in this.catRoles) {
            if ( ! rolesIds.includes(this.catRoles[key].id  ) ) {
                opciones.push(`<option value="${this.catRoles[key].id}">${this.catRoles[key].rol}</option>`)
            }
        }

        this.cboRoles.innerHTML = opciones.join('')
        $(`#${this.cboRoles.id}`).selectpicker('refresh')

        $(`#modal-roles`).modal('show')
    },
    guardar() {
        formActions.validate('formRoles')
            .then(({errores}) => {

                if (errores === 0) {
                    let {cboRoles: {value: idRol, labelOption: rol}} = formActions.getJSON('formRoles')
                    const datos = {id: 0, idRol, rol, motivos: {} }
                    this.contador++
                    this.roles[this.contador] = {...datos}
                    this.agregarFila(this.contador, {...datos})
                    tblRoles.columns.adjust().draw()
                    $(`#modal-roles`).modal('hide')
                }
            }).catch(generalMostrarError)
    },
    agregarFila(contador, {rol}) {
        let buttonEliminar = `<button class="btnTbl" onclick="configRoles.eliminar(${contador})" type="button" title="Eliminar rol"><i class="fas fa-trash-alt"></i></button>`

        tblRoles.row.add([
            contador, rol,
            `<div class="tblButtonContainer"> ${buttonEliminar} </div>`
        ]).node().id = `trRol${contador}`
    },
    eliminar( contador )
    {
        tblRoles.row(`#trRol${contador}`).remove().draw(false)
        delete configRoles.roles[contador]
        tblMotivos.clear().draw()
    }
}

const configMotivos = {
    contador: 0,
    idFila: 0,
    catMotivos: [],
    motivos: {},
    cboMotivos: document.getElementById('cboMotivos'),
    cargarMotivos ()
    {
        if ( configRoles.idFila > 0 )
        {
            this.motivos = {}
            tblMotivos.clear().draw();
            this.contador = 0

            this.motivos = {...configRoles.roles[configRoles.idFila].motivos }

            for ( const key in this.motivos )
            {
                this.contador++
                this.agregarFila( this.contador, {...this.motivos[key] } )
            }

            tblMotivos.columns.adjust().draw()
        }
    },
    mostrarModal()
    {
        let motivosIds = []
        let opciones = [`<option value="" selected disabled>seleccione</option>`]

        for (const key in configRoles.roles[configRoles.idFila].motivos )
        {
            motivosIds.push( configRoles.roles[configRoles.idFila].motivos[key].idMotivo )
        }

        for ( const key in this.catMotivos )
        {
            if(! motivosIds.includes(this.catMotivos[key].id ) )
            {
                opciones.push(`<option value="${this.catMotivos[key].id}">${this.catMotivos[key].motivoRetiro }</option>`)
            }
        }

        this.cboMotivos.innerHTML = opciones.join('')
        $(`#${this.cboMotivos.id}`).selectpicker('refresh')

        $('#modal-motivos').modal('show')
    },
    guardar()
    {
        formActions.validate('formMotivos')
            .then( ( { errores } ) => {
                if( errores == 0)
                {
                    const {
                        cboMotivos : { value: idMotivo, labelOption: motivo } } = formActions.getJSON('formMotivos')
                    this.contador++
                    configRoles.roles[configRoles.idFila].motivos[this.contador] = { idMotivo, motivo }
                    this.agregarFila( this.contador, { motivo })
                    tblMotivos.columns.adjust().draw()

                    $(`#modal-motivos`).modal('hide')
                }
            }).catch( generalMostrarError )
    },
    agregarFila( contador, { motivo } )
    {
        let buttonEliminar = `<button class="btnTbl" onclick="configMotivos.eliminar(${contador})" type="button" title="Eliminar motivo"><i class="fas fa-trash-alt"></i></button>`

        tblMotivos.row.add([
            contador, motivo,
            `<div class="tblButtonContainer">
                ${buttonEliminar}
            </div>`
        ]).node().id = `trMotivo${contador}`
    },
    eliminar( contador )
    {
        tblMotivos.row(`#trMotivo${contador}`).remove().draw(false);
        delete configRoles.roles[configRoles.idFila].motivos[contador];
    }
}

const guardarConfiguracion = () => {

    if ( Object.keys(configRoles.roles).length > 0  )
    {
        let esValido = true
        let mensaje = null

        for( const key in configRoles.roles )
        {
            if ( Object.keys( configRoles.roles[key].motivos).length == 0 )
            {
                mensaje = `El rol <strong>"${ configRoles.roles[key].rol }"</strong> no tiene asignado motivos`
                esValido = false;
                break;
            }
        }

        if( esValido )
        {
            fetchActions.set({
                modulo,
                archivo: 'controlesGuardarConfigSolicitud',
                datos: { roles: { ...configRoles.roles }, idApartado, tipoApartado }
            }).then( response => {
                switch (response.respuesta.trim()) {
                    case 'EXITO':
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: '¡Bien hecho!',
                            text: 'Configuración actualizada con exito',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }
            }).catch( generalMostrarError )
        } else
        {
            Swal.fire({
                title: "¡Advertencia!",
                html: `${mensaje}`,
                icon: "warning"
            });
        }

    } else {
        toastr.warning('Debe agregar un rol')
    }
}