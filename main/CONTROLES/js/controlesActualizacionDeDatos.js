const modulo = 'CONTROLES'

window.onload = () => {
    $(`.preloader`).fadeOut('fast')

    document.getElementById('frmActualizarAsociado').reset();

    fetchActions.getCats({
        modulo: "CONTROLES",
        archivo: "controlesGetCats",
        solicitados: ['tiposDocumento', 'estadoCivil', 'actividadEconomica', 'geograficos', 'agencias']
    }).then(({
        tiposDocumento: docucumentos,
        estadosCiviles,
        actividadesEconomicas,
        agencias,
        datosGeograficos: geograficos
    }) => {

        let opciones = [`<option selected disabled value="">seleccione</option>`]

        const cboTiposDocumentos = document.querySelectorAll('select.cboTipoDocumento')

        docucumentos.forEach(tipo => {
            tiposDocumento[tipo.id] = tipo
            opciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`)
        })

        cboTiposDocumentos.forEach(cbo => {
            cbo.innerHTML = opciones.join('')
            if (cbo.className.includes('selectpicker')) {
                $(`#${cbo.id}`).selectpicker('refresh')
            }
        })

        opciones = [`<option selected disabled value="">seleccione</option>`]

        const cboEstadosCiviles = document.querySelectorAll('select.cboEstadoCivil')

        estadosCiviles.forEach(estado => {
            opciones.push(`<option value="${estado.id}">${estado.estadoCivil}</option>`)
        })

        cboEstadosCiviles.forEach(cbo => {
            cbo.innerHTML = opciones.join('')
            if (cbo.className.includes('selectpicker')) {
                $(`#${cbo.id}`).selectpicker('refresh')
            }
        })

        opciones = [`<option selected disabled value="">seleccione</option>`]

        const cboActividadesEconomicas = document.querySelectorAll('select.cboActividadEconomica')

        actividadesEconomicas.forEach(actividad => {
            opciones.push(`<option value="${actividad.id}">${actividad.actividadEconomica}</option>`)
        })

        cboActividadesEconomicas.forEach(cbo => {
            cbo.innerHTML = opciones.join('')
            if (cbo.className.includes('selectpicker')) {
                $(`#${cbo.id}`).selectpicker('refresh')
            }
        })

        opciones = [`<option selected disabled value="">seleccione</option>`]

        const cboAgencias = document.querySelectorAll('select.cboAgencia')

        agencias.forEach(agencia => {
            opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
        })

        cboAgencias.forEach(cbo => {
            cbo.innerHTML = opciones.join('')
            if (cbo.className.includes('selectpicker')) {
                $(`#${cbo.id}`).selectpicker('refresh')
            }
        })

        opciones = [`<option selected disabled value="">seleccione</option>`]
        const cboPais = document.querySelectorAll('select.cboPais')

        datosGeograficos = {
            ...geograficos
        };

        for (let key in datosGeograficos) {
            opciones.push(`<option value="${datosGeograficos[key].id}">${datosGeograficos[key].nombrePais}</option>`);
        }

        cboPais.forEach(cbo => {
            cbo.innerHTML = opciones.join('')
            if (cbo.className.includes('selectpicker')) {
                $(`#${cbo.id}`).selectpicker('refresh')
            }
        })
    }).catch(generalMostrarError)

}

const configAsociado = {
    txtCodigoCliente: document.getElementById("txtCodigoAsociado"),
    cboFormAsociado: document.querySelectorAll('form#frmActualizarAsociado select.form-control'),
    inputsFormAsociado: document.querySelectorAll('form#frmActualizarAsociado, input.form-control'),
    comprobarCodigo() {
        if (this.txtCodigoCliente.value.length > 0) {
            let codigoCliente = this.txtCodigoCliente.value;
            fetchActions.get({
                modulo: 'CONTROLES',
                archivo: 'controlesBuscarAsociado',
                params: {
                    codigoCliente: encodeURIComponent(codigoCliente),
                    tipo: "Update"
                }
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta) {
                        case "EXITO":
                            this.txtCodigoCliente.parentNode.querySelector(".input-group-append").remove();
                            this.txtCodigoCliente.readOnly = true;
                            const datosAsociado = datosRespuesta.datosAsociado;

                            document.getElementById("cboTipoDocumento").value = datosAsociado.tipoDocumentoPrimario;
                            document.getElementById("txtDocumentoPrimario").value = datosAsociado.numeroDocumentoPrimario;
                            document.getElementById("txtNit").value = datosAsociado.numeroDocumentoSecundario;
                            document.getElementById("txtNombres").value = datosAsociado.nombresAsociado;
                            document.getElementById("txtApellidos").value = datosAsociado.apellidosAsociado;
                            document.getElementById("txtProfesion").value = datosAsociado.profesion;
                            document.getElementById("cboAgencia").value = datosAsociado.agencia;
                            document.getElementById("txtTelefonoFijo").value = datosAsociado.telefonoFijo;
                            document.getElementById("txtTelefonoMovil").value = datosAsociado.telefonoMovil;
                            document.getElementById("txtEmail").value = datosAsociado.email;

                            document.getElementById('cboEstadoCivil').value = datosAsociado.estadoCivil
                            document.getElementById('cboActividadEconomicaPrincipal').value = datosAsociado.actividadPrimaria
                            document.getElementById('cboActividadEconomicaSecundaria').value = datosAsociado.actividadSecundaria

                            document.getElementById("cboPais").value = datosAsociado.pais;
                            $("#cboPais").trigger("change");
                            document.getElementById("cboDepartamento").value = datosAsociado.departamento;
                            $("#cboDepartamento").trigger("change");
                            document.getElementById("cboMunicipio").value = datosAsociado.municipio;
                            document.getElementById("txtDireccion").value = datosAsociado.direccionCompleta;
                            document.getElementById("txtFechaAfiliacion").value = datosAsociado.fechaAfiliacion;

                            this.activarFormulario()

                            const campos = [...this.cboFormAsociado, ...this.inputsFormAsociado]

                            campos.forEach(campo => {
                                if (campo.id) {
                                    if (campo.type == 'select-one') {
                                        $("#" + campo.id).data("valorInicial", campo.options[campo.selectedIndex] ? campo.options[campo.selectedIndex].text : null);
                                    } else {
                                        $("#" + campo.id).data("valorInicial", campo.value);
                                    }
                                }
                            })
                            break;
                        case "NoAsociado":
                            toastr.warning(`No existe un asociado con el codigo de cliente ${this.txtCodigoCliente.value}`)
                            break;
                        case "Retirado":
                            Swal.fire({
                                title: "¡Atención!",
                                icon: "warning",
                                html: `Ya existe un asociado con el codigo <b>${codigoCliente}</b>, pero se encuentra retirado.`
                            });
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            toastr.warning(" ¡Completa el campo!")
        }
    },
    guardar() {
        formActions.validate('frmActualizarAsociado')
            .then(({
                errores
            }) => {

                if (errores == 0) {
                    const datosAsociado = formActions.getJSON('frmActualizarAsociado')

                    if (datosAsociado.cboTipoDocumento.value && datosAsociado.txtNombres.value.length > 0) {
                        fetchActions.set({
                            modulo,
                            archivo: 'controlesGuardarAsociados',
                            datos: {
                                ...datosAsociado,
                                descripcionLog: 'Actualización de datos',
                                detallesLog: [...this.obtenerLogs()],
                                accion: 'editar',
                                idApartado,
                                tipoApartado
                            }
                        }).then(response => {

                            switch (response.respuesta.trim()) {
                                case 'EXITO':
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: '¡Bien hecho!',
                                        text: 'Los datos del asociado fueron actualizados con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then(() => {
                                        this.limpiar()
                                    })
                                    break;
                                default:
                                    generalMostrarError(response);
                                    break;
                            }

                        }).catch(generalMostrarError)
                    } else {
                        this.comprobarCodigo()
                    }


                }
            }).catch(generalMostrarError)
    },
    activarFormulario(accion = false) {
        this.inputsFormAsociado.forEach(input => {
            if (input.id !== 'txtCodigoAsociado') {
                if (input.id == 'txtFechaAfiliacion') {
                    input.disabled = accion
                    $(`#${input.id}`).datepicker('update')
                } else {
                    input.readOnly = accion
                }
            }
        })
        this.cboFormAsociado.forEach(cbo => {
            cbo.disabled = accion

            if (cbo.className.includes('selectpicker')) {
                $(`#${cbo.id}`).selectpicker('refresh')
            }
        })
    },
    limpiar() {
        formActions.clean('frmActualizarAsociado')
            .then(() => {
                this.activarFormulario(true)
                this.txtCodigoCliente.focus();
                let boton = `<div class="input-group-append">
                <button class="input-group-text btn btn-outline-dark" onclick="configAsociado.comprobarCodigo()" type="button" id="btnBuscarAscociado"><i class="fas fa-search"></i></button>
            </div>`;

                this.txtCodigoCliente.readOnly = false;
                if (this.txtCodigoCliente.parentNode.querySelectorAll('.input-group-append').length == 0) {
                    $("#" + this.txtCodigoCliente.id).after(boton);
                }
            })
            .catch(generalMostrarError)
    },
    obtenerLogs() {
        const camposEditados = []
        const campos = [...this.cboFormAsociado, ...this.inputsFormAsociado]


        campos.forEach(campo => {

            if (campo.id) {

                let valorActual = campo.type == 'select-one' ? campo.options[campo.selectedIndex].text == 'seleccione' ? null : campo.options[campo.selectedIndex].text : campo.value
                let valorAnterior = $("#" + campo.id).data("valorInicial");

                if (valorActual != valorAnterior) {
                    let label = $("#" + campo.id).parent().find("label").length > 0 ? $("#" + campo.id).parent().find("label:eq(0)").text() : ($("#" + campo.id).parent().parent().parent().find("label").length > 0 ? $("#" + campo.id).parent().parent().parent().find("label:eq(0)").text() : 'undefined');
                    label = (label.replace("*", "")).trim();
                    label = (label.replace(":", "")).trim();
                    camposEditados.push({
                        campo: label,
                        valorAnterior,
                        valorNuevo: valorActual
                    })
                }
            }
        })
        return camposEditados
    }
}