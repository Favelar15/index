
let tblControlesRoles;
let modulo = 'CONTROLES';

window.onload = () => {

    inicializarDataTable();
}

const inicializarDataTable = () => {
    if ($('#tblControlesRoles').length > 0) {

        tblControlesRoles = $('#tblControlesRoles').DataTable({
            "paging": false,
            "bAutoWidth": false,
            "searching": true,
            "info": false,
            "ordering": false,
            dateFormat: 'uk',
            order: [1, "desc"],
            columnDefs: [
                {
                    "width": "5%",
                    "targets": 0,
                    "className": "text-center"
                },
                {
                    targets: [6], width: '15%'
                }
            ],
            sortable: true,
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Datos de colaborador';
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            }
        });

        tblControlesRoles.columns.adjust().draw();
    }
}

const tblRoles = {
    mostrarModal () {
        $('#modalControlesRoles').modal('show');
    }
}