
let tblSolicitudPep, tblSolicitudRelacionados
const modulo = 'CONTROLES'


window.onload = () => {

    const cboAgencia = document.getElementById('cboAgencia')

    const inicializando = initDataTable().then( () => {
        return new Promise( ( resolve, reject ) => {
            fetchActions.getCats({
                modulo, archivo: 'controlesGetCats', solicitados: ['agenciasAsignadas']
            }).then( ( { agenciasAsignadas = [] } ) => {

                let opciones = [`<option value="all" selected>Todas las agencias</option>`]

                agenciasAsignadas.forEach( agencia => {
                    if (agencia.id != 700) {
                        opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                    }
                })

                cboAgencia.innerHTML = opciones.join()
                $(`#${cboAgencia.id}`).selectpicker('refresh')

                resolve()

            }).catch( reject )
        })
    })

    inicializando.then( () => {
        $(`.preloader`).fadeOut('fast')
        configReporte.resetFormulario()
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblSolicitudPep').length > 0) {
                tblSolicitudPep = $('#tblSolicitudPep').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [
                        {
                            targets: [0],
                            orderable: true,
                            width: '8%'
                        },
                        {
                            targets: [1, 2, 3, 4,5],
                            className: "text-start"
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de la solicitud';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSolicitudPep.columns.adjust().draw();
            }

            if ($('#tblSolicitudRelacionados').length > 0) {
                tblSolicitudRelacionados = $('#tblSolicitudRelacionados').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [
                        {
                            targets: [0],
                            orderable: true,
                            width: '8%'
                        },
                        {
                            targets: [1, 2, 3, 4,5],
                            className: "text-start"
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de la solicitud';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSolicitudRelacionados.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}


const configReporte = {
    btnExcelRelacionados: document.getElementById('btnRelacionados'),
    btnPep: document.getElementById('btnPep'),
    txtInicio: document.getElementById('txtInicio'),
    obtenerSolicitudes()
    {
        formActions.validate('formSolicitudPep')
            .then( ( { errores = 0 } ) => {

                if ( errores == 0)
                {
                    const {
                        cboAgencia, cboEstado: { labelOption: estado },
                        txtInicio, txtFin
                    } = formActions.getJSON('formSolicitudPep')

                    fetchActions.set({
                        modulo, archivo: 'Peps/controlesReporteSolicitudes', datos: {
                            cboAgencia, accion: estado, txtInicio, txtFin
                        }
                    }).then( ( { solicitudes } ) => {

                        configPep.contador = 0
                        configPep.solicitudes = {}
                        configPersonasRelacionadas.contador = 0
                        configPersonasRelacionadas.solicitudes = {}
                        tblSolicitudRelacionados.clear()
                        tblSolicitudPep.clear()

                        if ( solicitudes.length > 0 )
                        {
                            this.resetFormulario( true )
                        } else {
                          toastr.warning(`No hay datos para mostrar`)
                        }
                        solicitudes.forEach( solicitud => {

                            if (solicitud.resolucion == estado)
                            {
                                if( solicitud.tipoAfiliacion == 'PEP' )
                                {
                                    configPep.contador++
                                    configPep.solicitudes[configPep.contador] = { ...solicitud }
                                    configPep.agregarFila({
                                        contador: configPep.contador, ...solicitud
                                    })
                                } else if ( solicitud.tipoAfiliacion == 'RELACIONADO A PEP') {
                                    configPersonasRelacionadas.contador++
                                    configPersonasRelacionadas.solicitudes[configPersonasRelacionadas.contador] = {...solicitud}
                                    configPersonasRelacionadas.agregarFila({
                                        contador: configPersonasRelacionadas.contador, ...solicitud
                                    })
                                }
                            }
                        })

                        if( Object.keys(configPep.solicitudes).length == 0)
                        {
                            this.btnPep.classList.add('disabled')
                        }

                        if( Object.keys(configPersonasRelacionadas.solicitudes).length == 0)
                        {
                            this.btnExcelRelacionados.classList.add('disabled')
                        }

                        tblSolicitudPep.columns.adjust().draw()
                        tblSolicitudRelacionados.columns.adjust().draw()

                    }).catch( generalMostrarError )
                }
            }).catch( generalMostrarError )
    },
    limpiar()
    {
        formActions.clean('formSolicitudPep')
            .then( () => {

                this.resetFormulario(false)
                tblSolicitudPep.clear().draw()
                tblSolicitudRelacionados.clear().draw()

                configPep.contador = 0
                configPep.solicitudes = {}

                configPersonasRelacionadas.contador = 0
                configPersonasRelacionadas.solicitudes = {}

            })
    },
    resetFormulario( estado )
    {
        let inputs = document.querySelectorAll('form#formSolicitudPep .form-control')

        if (estado)
        {
            this.btnPep.classList.remove('disabled')
            this.btnExcelRelacionados.classList.remove('disabled')
        }  else {
            this.btnPep.classList.add('disabled')
            this.btnExcelRelacionados.classList.add('disabled')
        }

        inputs.forEach( input => {

            input.disabled = estado

            if( input.className.includes('selectpicker'))
            {
                $(`#${input.id}`).selectpicker('refresh')
            }
        })
    },
    cambiosFecha()
    {
        $("#txtFin").datepicker('destroy')
        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        document.getElementById('txtFin').value = ''
        $("#txtFin").datepicker('update')
    }
}

const configPep = {
    contador: 0,
    solicitudes: {},
    agregarFila( { contador,fechaRegistro, codigoCliente, tipoSolicitud, agencia, nombres, numeroDocumentoPrimario, fechaResolucion } )
    {

        tblSolicitudPep.row.add([
            contador, fechaRegistro, codigoCliente, tipoSolicitud, agencia, nombres, numeroDocumentoPrimario, fechaResolucion
        ]).node().id = `trPep${contador}`
    },
    exportarExcel()
    {
        if( Object.keys(this.solicitudes).length > 0 )
        {
            fetchActions.set({
                modulo, archivo: 'Peps/controlesExcelSolicitudesPep', datos: {
                    ...formActions.getJSON('formSolicitudPep'),
                    solicitudes: this.solicitudes,
                    accion: 'PEP'
                }
            }).then( response => {
                switch (response.respuesta.trim()) {
                    case 'EXITO':
                        window.open('./main/CONTROLES/docs/solicitudesPeps/Excel/'+response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }
            }).catch( generalMostrarError )
        }
    }
}

const configPersonasRelacionadas = {
    contador: 0,
    solicitudes: {},
    agregarFila( { contador,fechaRegistro, codigoCliente, tipoSolicitud, agencia, nombres, numeroDocumentoPrimario, fechaResolucion } )
    {
        tblSolicitudRelacionados.row.add([
            contador, fechaRegistro, codigoCliente,tipoSolicitud, agencia, nombres, numeroDocumentoPrimario, fechaResolucion
        ]).node().id = `trPersonaRelacionada${contador}`
    },
    exportarExcel()
    {
        if( Object.keys(this.solicitudes).length > 0 )
        {
            fetchActions.set({
                modulo, archivo: 'Peps/controlesExcelSolicitudesPep', datos: {
                    ...formActions.getJSON('formSolicitudPep'),
                    solicitudes: this.solicitudes,
                    accion: 'RELACIONADOS'
                }
            }).then( response => {
                switch (response.respuesta.trim()) {
                    case 'EXITO':
                        window.open('./main/CONTROLES/docs/solicitudesPeps/Excel/'+response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }
            }).catch( generalMostrarError )
        }
    }
}