
let tblSubAplicacion
const modulo = 'CONTROLES'

window.onload = () => {

    initDataTable().then( () => {
        $(`.preloader`).fadeOut('fast')

        fetchActions.get({
            modulo, archivo: 'ActivacionDeCuentas/controlesObtenerSubAplicaciones', params : {}
        }).then( ( { subAplicaciones = [] }) => {

            subAplicaciones.forEach( sub => {
                subAplicacion.catSubAplicaciones[sub.idSubAplicacion] = { ...sub }
            })
            subAplicacion.cargarDatos()

        }).catch( generalMostrarError )
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {

            if ($('#tblSubAplicacion').length > 0) {
                tblSubAplicacion = $('#tblSubAplicacion').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                        width: '10%'
                    },
                        {
                            targets: [1],
                            className: 'text-center',
                            width: '15%'
                        },
                        {
                            targets: [5],
                            className: 'text-center',
                            width: '12%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSubAplicacion.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const subAplicacion = {
    idFila: 0,
    contador: 0,
    catSubAplicaciones : {},
    txtIdSubAplicacion: document.getElementById('txtIdSubAplicacion'),
    txtTipoCuenta: document.getElementById('txtTipoCuenta'),
    lblMensajeId : document.getElementById('mensajeCodigo'),
    cargarDatos()
    {
        tblSubAplicacion.clear().draw()
        this.contador = 0

        for (const key in this.catSubAplicaciones)
        {
            this.contador++
            this.agregarFila( this.contador, { ...this.catSubAplicaciones[key] } )
        }
        tblSubAplicacion.columns.adjust().draw()
    },
    mostrarModal( idFila = 0 )
    {
        formActions.clean('formSubAplicacion')
            .then( () => {

                this.idFila = idFila

                this.txtIdSubAplicacion.classList.remove('is-invalid', 'is-valid')
                this.txtTipoCuenta.classList.remove('is-invalid', 'is-valid')
                document.getElementById('formSubAplicacion').classList.remove('was-validated')

                this.txtIdSubAplicacion.readOnly = false

                if ( this.idFila > 0 )
                {
                    this.txtIdSubAplicacion.value = this.catSubAplicaciones[this.idFila].idSubAplicacion
                    this.txtIdSubAplicacion.readOnly = true
                    this.txtTipoCuenta.value = this.catSubAplicaciones[this.idFila].tipoCuenta
                }

                $(`#modal-subAplicaciones`).modal('show')

            }).catch( generalMostrarError )

    },
    guardar()
    {
        formActions.validate('formSubAplicacion')
            .then( ( { errores } ) => {

                if ( errores == 0 )
                {
                    const {
                        txtIdSubAplicacion, txtTipoCuenta
                    } = formActions.getJSON('formSubAplicacion')

                    let idSubAplicacionValido = true

                    if ( Object.keys( this.catSubAplicaciones).length > 0 && this.idFila == 0  )
                    {
                        for ( const key in this.catSubAplicaciones )
                        {
                            if (this.catSubAplicaciones[key].idSubAplicacion == txtIdSubAplicacion.value )
                            {
                                idSubAplicacionValido = false
                                break
                            }
                        }
                    }

                    if (idSubAplicacionValido)
                    {
                        fetchActions.set({
                            modulo, archivo: 'ActivacionDeCuentas/controlesGuardarSubAplicacion',
                            datos: {
                                idApartado, tipoApartado, txtIdSubAplicacion, txtTipoCuenta
                            }
                        }).then( response => {
                            switch (response.respuesta.trim()) {
                                case 'EXITO':
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: '¡Bien hecho!',
                                        text: 'Sub aplicación registrada con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then(i => {

                                        $(`#modal-subAplicaciones`).modal('hide')

                                        if ( this.idFila == 0 )
                                        {
                                            this.contador++
                                            this.catSubAplicaciones[txtIdSubAplicacion.value] = {
                                                idSubAplicacion: txtIdSubAplicacion.value,
                                                tipoCuenta:  txtTipoCuenta.value,
                                                fechaRegistro : response.fechaRegistro,
                                                fechaActualizacion : response.fechaActualizacion
                                            }
                                        } else
                                        {
                                            this.catSubAplicaciones[this.idFila] = { ...this.catSubAplicaciones[this.idFila], tipoCuenta: txtTipoCuenta.value, fechaActualizacion:response.fechaActualizacion }
                                        }
                                        this.cargarDatos()
                                    });
                                    break
                                default :
                                    generalMostrarError( response )
                                    break
                            }
                        }).catch( generalMostrarError )
                    } else
                    {
                        document.getElementById('mensajeCodigo').innerHTML = `Ya existe una sub aplicacion con el codigo ${txtIdSubAplicacion.value}`
                        this.txtIdSubAplicacion.classList.remove('is-valid')
                        this.txtIdSubAplicacion.classList.add('is-invalid')
                    }

                    this.lblMensajeId.innerHTML = 'Codigo de subAplicacion es requerido'
                }
            })
    },
    agregarFila( contador, { idSubAplicacion, tipoCuenta, fechaRegistro, fechaActualizacion } )
    {
        let editar = `<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`
        let eliminar = ``

        if ( tipoPermiso == 'ESCRITURA')
        {
            editar = `<button type="button" class="btnTbl" onclick="subAplicacion.mostrarModal(${idSubAplicacion})"><i class="fas fa-edit"></i></button>`
            eliminar = `<button type="button" class="btnTbl" onclick="subAplicacion.eliminar(${idSubAplicacion})"><i class="fas fa-trash"></i></button>`
        }

        tblSubAplicacion.row.add([
            contador, idSubAplicacion, tipoCuenta, fechaRegistro, fechaActualizacion,
            `<div class="tblButtonContainer"> ${editar} ${eliminar}</div>`
        ]).node().id = `trSubAplicacion${idSubAplicacion}`
    },
    eliminar( idSubAplicacion )
    {
        const subAplicacion = this.catSubAplicaciones[idSubAplicacion]

        Swal.fire({
            title: 'IMPORTANTE',
            icon: 'info',
            html: `¿Quieres eliminar la sub aplicación <strong>"${subAplicacion.idSubAplicacion} - ${subAplicacion.tipoCuenta}"</strong>?`,
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            cancelButtonColor: '#f54343',
            confirmButtonText:
                `<i class="fa fa-thumbs-up"></i> Eliminar`,
            cancelButtonText:
                `<i class="fa fa-thumbs-down"></i> Cancelar`,
        }).then( result => {
            if (result.isConfirmed)
            {
                fetchActions.set({
                    modulo, archivo: 'ActivacionDeCuentas/controlesEliminarSubApartado', datos: {
                        idSubAplicacion, idApartado, tipoApartado
                    }
                }).then( response => {
                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'Sub aplicación eliminada con exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then( () => {
                                tblSubAplicacion.row(`#trSubAplicacion${idSubAplicacion}`).remove().draw()
                                delete this.catSubAplicaciones[idSubAplicacion]
                            });
                            break
                        default :
                            generalMostrarError( response )
                            break
                    }
                }).catch( generalMostrarError )
            }
        })
    }
}