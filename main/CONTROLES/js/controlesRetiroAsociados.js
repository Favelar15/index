let tblSolicitudes,
    tblSolicitudesResueltas,
    tblMotivos,
    tblProductos,
    tblDeudores,
    tblGestiones,
    tblPagos;

let idSolicitud = 0,
    idFila = 0,
    cntSolicitudes = 0,
    cntSolicitudesResueltas = 0,
    cntMotivos = 0,
    cntProductos = 0,
    cntDeudores = 0,
    cntGestiones = 0;

let solicitudesProceso = {},
    solicitudesResueltas = {},
    catMotivosRetiro = {},
    catProductos = {},
    catAreas = {},
    catResoluciones = {},
    motivosRetiro = {},
    productos = {},
    gestiones = {},
    deudores = {};

let solicitudNueva = 'S';

let modulo = 'CONTROLES';

window.onload = () => {
    const selectpickerCbos = document.querySelectorAll("select.selectpicker");
    const gettingCats = initTables().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "CONTROLES",
                archivo: 'controlesGetCats',
                solicitados: ['tiposDocumento', 'agencias', 'geograficos', 'motivosRetiro', 'productos', 'areas', 'resoluciones']
            }).then((datosRespuesta) => {
                try {
                    let opciones = [`<option selected disabled value="">seleccione</option>`];
                    if (datosRespuesta.tiposDocumento) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboTipoDocumento");

                        datosRespuesta.tiposDocumento.forEach(tipo => {
                            tiposDocumento[tipo.id] = tipo;
                            tmpOpciones.push(`<option value="${tipo.id}">${tipo.tipoDocumento}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.agencias) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboAgencia");

                        datosRespuesta.agencias.forEach(agencia => {
                            tmpOpciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.datosGeograficos) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboPais");

                        datosGeograficos = {
                            ...datosRespuesta.datosGeograficos
                        };

                        for (let key in datosGeograficos) {
                            tmpOpciones.push(`<option value="${datosGeograficos[key].id}">${datosGeograficos[key].nombrePais}</option>`);
                        }

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.motivosRetiro) {
                        datosRespuesta.motivosRetiro.forEach(motivo => catMotivosRetiro[motivo.id] = motivo);
                    }

                    if (datosRespuesta.productos) {
                        let tmpCnt = 0;
                        datosRespuesta.productos.forEach(producto => {
                            tmpCnt++;
                            catProductos[producto.id] = producto;
                            tblPagos.row.add([
                                tmpCnt,
                                producto.tipoCuenta,
                                '',
                                `<input type="number" id="numProductoPago${producto.id}" name="numProductoPago${producto.id}" class="form-control" min="0" step="0.01" onkeypress="return generalSoloNumeros(event);" />`
                            ]).node().id = 'trPagoProducto' + producto.id;
                        });

                        tblPagos.columns.adjust().draw();
                    }

                    if (datosRespuesta.areas) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboArea");

                        datosRespuesta.areas.forEach(area => {
                            catAreas[area.id] = area;
                            tmpOpciones.push(`<option value="${area.id}">${area.area}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    if (datosRespuesta.resoluciones) {
                        let tmpOpciones = [...opciones],
                            cbos = document.querySelectorAll("select.cboResolucion");
                        datosRespuesta.resoluciones.forEach(resolucion => {
                            catResoluciones[resolucion.id] = {
                                ...resolucion
                            };
                            tmpOpciones.push(`<option value="${resolucion.id}">${resolucion.resolucion}</option>`);
                        });

                        cbos.forEach(cbo => {
                            cbo.innerHTML = tmpOpciones.join("");
                        });
                    }

                    resolve();
                } catch (err) {
                    console.error(err.message);
                    reject();
                }
            }).catch(reject);
        });
    });

    const gettingSolicitudes = gettingCats.then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: 'CONTROLES',
                archivo: '/RetiroAsociados/controlesGetSolicitud',
                solicitados: ['all', tipoApartado, idApartado]
            }).then((datosRespuesta) => {
                $(".preloader").fadeIn("fast");
                if (datosRespuesta.solicitudes) {
                    $(".preloader").find("span").html("Obteniendo solicitudes");
                    let construyendo = configSolicitudes.init([...datosRespuesta.solicitudes]);
                    construyendo.then(resolve).catch(reject);
                } else {
                    reject();
                }
            }).catch(reject);
        });
    }).catch(generalMostrarError);

    gettingSolicitudes.then(() => {
        selectpickerCbos.forEach(cbo => {
            $("#" + cbo.id).selectpicker("refresh");
        });
        $(".preloader").fadeOut("fast");
    }).catch(generalMostrarError);
}

function initTables() {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblSolicitudes').length > 0) {
                tblSolicitudes = $('#tblSolicitudes').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                    }, {
                        targets: [1, 2, 3, 4],
                        className: "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de la solicitud';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSolicitudes.columns.adjust().draw();
            }

            if ($('#tblSolicitudesResueltas').length > 0) {
                tblSolicitudesResueltas = $('#tblSolicitudesResueltas').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                    }, {
                        targets: [1, 2, 3, 4],
                        className: "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de la solicitud';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSolicitudesResueltas.columns.adjust().draw();
            }

            if ($('#tblMotivos').length > 0) {
                tblMotivos = $('#tblMotivos').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                    }, {
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Motivo de retiro';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblMotivos.columns.adjust().draw();
            }

            if ($('#tblProductos').length > 0) {
                tblProductos = $('#tblProductos').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                    }, {
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de producto';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblProductos.columns.adjust().draw();
            }

            if ($('#tblDeudores').length > 0) {
                tblDeudores = $('#tblDeudores').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                    }, {
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de producto';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblDeudores.columns.adjust().draw();
            }

            if ($('#tblGestiones').length > 0) {
                tblGestiones = $('#tblGestiones').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                    }, {
                        targets: [1, 2],
                        className: "text-start"
                    }],
                    searching: false,
                    bLengthChange: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de producto';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblGestiones.columns.adjust().draw();
            }

            if ($('#tblPagos').length > 0) {
                tblPagos = $('#tblPagos').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                            targets: [0],
                            orderable: true,
                        }, {
                            targets: [1],
                            className: "text-start"
                        },
                        {
                            targets: [2, 3],
                            width: "90px"
                        }
                    ],
                    searching: false,
                    bLengthChange: false,
                    paginate: false,
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Detalles de producto';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblPagos.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            console.error(err.message);
            reject();
        }
    });
}

const configSolicitudes = {
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            tmpDatos.forEach(solicitud => {
                if (solicitud.estado == 'RESUELTA') {
                    cntSolicitudesResueltas++;
                    solicitudesResueltas[solicitud.id] = {
                        ...solicitud
                    };
                    this.addRowTblResueltas({
                        ...solicitud,
                        idFila: cntSolicitudesResueltas
                    });
                } else {
                    cntSolicitudes++;
                    solicitudesProceso[solicitud.id] = {
                        ...solicitud
                    };
                    this.addRowTbl({
                        ...solicitud,
                        idFila: cntSolicitudes
                    });
                }
            });

            tblSolicitudes.columns.adjust().draw();
            tblSolicitudesResueltas.columns.adjust().draw();
            resolve();
        });
    },
    addRowTbl: function ({
        idFila,
        id,
        codigoCliente,
        nombreCompleto,
        estado,
        nombreAgencia,
        nombreUsuario,
        fechaRegistro,
        aprueba
    }) {
        return new Promise((resolve, reject) => {
            try {

                const btnEditar = `<span class='btnTbl' title='Editar' onclick='configSolicitud.edit(${id});'> <i class='fas fa-edit'></i> </span>`
                const btnImprimir = `<span class='btnTbl' title='Imprimir solicitud' onclick='configSolicitud.print(${id});'><i class='fas fa-print'></i> </span>`
                const btnResolucion =  aprueba == 'S' ? `<span class='btnTbl' title='Aplicar resolución' onclick='configSolicitud.resolucion(${id});'><i class='fas fa-check'></i></span>` : ''
                const btnEliminar = `<span class='btnTbl' title='Eliminar' onclick='configSolicitud.delete(${id});'><i class='fas fa-trash'></i> </span>`

                tblSolicitudes.row.add([
                    idFila,
                    codigoCliente,
                    nombreCompleto,
                    estado,
                    nombreAgencia,
                    nombreUsuario,
                    fechaRegistro,
                    `<div class='tblButtonContainer'>
                        ${btnEditar} ${btnImprimir} ${btnResolucion} ${btnEliminar}
                    </div>`
                ]).draw().node().id = 'trSolicitud' + id;
                resolve();
            } catch (err) {
                console.error(err.message);
                reject();
            }
        });
    },
    addRowTblResueltas: function ({
        idFila,
        id,
        codigoCliente,
        nombreCompleto,
        estado,
        resolucion,
        nombreAgencia,
        nombreUsuario,
        fechaRegistro
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Procesar pago' onclick='configSolicitud.pago(" + id + ");'>" +
                    "<i class='fas fa-hand-holding-usd'></i>" +
                    "</span>" +
                    "</div>";
                botones = resolucion == 'DENEGADA' ? 'No se puede procesar pago' : botones;
                tblSolicitudesResueltas.row.add([
                    idFila,
                    codigoCliente,
                    nombreCompleto,
                    estado + ' / ' + resolucion,
                    nombreAgencia,
                    nombreUsuario,
                    fechaRegistro,
                    botones
                ]).draw().node().id = 'trSolicitudResuelta' + id;
                resolve();
            } catch (err) {
                console.error(err.message);
                reject();
            }
        });
    },
    edit: function (id) {

    },
    delete: function (id) {

    }
}

const configSolicitud = {
    formulario: document.getElementById("frmSolicitud"),
    tabSolicitudes: document.getElementById("solicitudes-tab"),
    tabSolicitudesAprobadas: document.getElementById("solicitudesAprobadas-tab"),
    tabSolicitud: document.getElementById("solicitud-tab"),
    tabMotivos: document.getElementById("motivosRetiro-tab"),
    tabProductos: document.getElementById("productos-tab"),
    tabGestiones: document.getElementById("gestiones-tab"),
    campoCodigoCliente: document.getElementById("txtCodigoCliente"),
    camposEditables: ['txtNIT', 'txtTelefonoFijo', 'txtTelefonoMovil', 'txtEmail', 'cboPais', 'cboDepartamento', 'cboMunicipio', 'txtDireccion'],
    cboTarjeta: document.getElementById("cboTarjeta"),
    botonGuardar: document.getElementById("btnGuardar"),
    botonesSecundarios: document.querySelectorAll("#tabButtonsSub .tabButtonContainer button"),
    botonBuscador: `<div class="input-group-append"><button type="submit" class="input-group-text btn btn-outline-dark"><i class="fas fa-search"></i></button></div>`,
    camposObligatorios: document.querySelectorAll(".obligatorio"),
    campoMontoPrestamos: document.getElementById("numMontoCreditos"),
    campoMontoCuentasCobrar: document.getElementById("montoCuentasCobrar"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                let datosAsociado = tmpDatos.datosAsociado ? {
                        ...tmpDatos.datosAsociado
                    } : {
                        ...tmpDatos
                    },
                    selectpickerCbos = document.querySelectorAll("select.selectpicker");

                !datosAsociado.tipoDocumentoPrimario && (datosAsociado.tipoDocumentoPrimario = tmpDatos.tipoDocumento);
                !datosAsociado.numeroDocumentoPrimario && (datosAsociado.numeroDocumentoPrimario = tmpDatos.numeroDocumento);
                !datosAsociado.numeroDocumentoSecundario && (datosAsociado.numeroDocumentoSecundario = tmpDatos.NIT);

                this.campoCodigoCliente.readOnly = true;
                document.getElementById("txtCodigoCliente").value = datosAsociado.codigoCliente;
                document.getElementById("cboTipoDocumento").value = datosAsociado.tipoDocumentoPrimario;
                document.getElementById("txtDocumentoPrimario").value = datosAsociado.numeroDocumentoPrimario;
                document.getElementById("txtNIT").value = datosAsociado.numeroDocumentoSecundario;
                document.getElementById("txtNombres").value = datosAsociado.nombresAsociado;
                document.getElementById("txtApellidos").value = datosAsociado.apellidosAsociado;
                document.getElementById("txtProfesion").value = datosAsociado.profesion;
                document.getElementById("cboAgencia").value = datosAsociado.agencia;
                document.getElementById("txtFechaAfiliacion").value = datosAsociado.fechaAfiliacion;
                document.getElementById("txtTelefonoFijo").value = datosAsociado.telefonoFijo;
                document.getElementById("txtTelefonoMovil").value = datosAsociado.telefonoMovil;
                document.getElementById("txtEmail").value = datosAsociado.email;
                document.getElementById("cboPais").value = datosAsociado.pais;
                $("#cboPais").trigger("change");
                document.getElementById("cboDepartamento").value = datosAsociado.departamento;
                $("#cboDepartamento").trigger("change");
                document.getElementById("cboMunicipio").value = datosAsociado.municipio;
                document.getElementById("txtDireccion").value = datosAsociado.direccionCompleta;
                datosAsociado.tarjetaDebito && (this.cboTarjeta.value = datosAsociado.tarjetaDebito);
                this.cboTarjeta.disabled = false;

                selectpickerCbos.forEach(cbo => {
                    $("#" + cbo.id).selectpicker("refresh");
                });

                this.campoCodigoCliente.parentNode.querySelector(".input-group-append").remove();

                this.configEditables(false);

                this.botonGuardar.classList.remove('disabled');
                this.botonGuardar.disabled = false;
                this.formulario.action = 'javascript:configSolicitud.save()';
                this.botonesSecundarios.forEach(boton => {
                    boton.disabled = false;
                });

                this.camposObligatorios.forEach(campo => {
                    if (campo.id) {
                        campo.required = true;
                    }
                });
                if (tmpDatos.motivosRetiro) {
                    const inicializandoMotivos = configMotivoRetiro.init([...tmpDatos.motivosRetiro]);
                    const inicializandoProductos = configProducto.init([...tmpDatos.productos]);
                    const inicializandoDeudores = configDeudor.init([...tmpDatos.deudores]);
                    const inicializandoGestiones = configGestion.init([...tmpDatos.gestiones]);
                }
                resolve();
            } catch (err) {
                console.error(err.message);
                reject();
            }
        });
    },
    edit: function (id = 0) {
        this.formulario.action = "javascript:configSolicitud.searchAsociado();";
        formActions.clean('frmSolicitud').then(() => {
            this.tabSolicitudes.classList.add("disabled");
            this.tabSolicitudesAprobadas.classList.add("disabled");
            this.tabSolicitud.classList.remove("disabled");
            generalShowHideButtonTab('btnsTabs', 'btnsSecondary');
            tblMotivos.clear().draw();
            tblProductos.clear().draw();
            tblDeudores.clear().draw();
            tblGestiones.clear().draw();
            motivosRetiro = {};
            productos = {};
            deudores = {};
            gestiones = {};
            cntMotivos = 0;
            cntProductos = 0;
            cntDeudores = 0;
            cntGestiones = 0;
            idSolicitud = id;
            solicitudNueva = 'S';
            this.botonesSecundarios.forEach(boton => {
                boton.disabled = true;
            });
            this.configEditables(true);
            this.cboTarjeta.disabled = true;
            this.campoCodigoCliente.readOnly = false;
            if (this.campoCodigoCliente.parentNode.querySelectorAll("div.input-group-append").length == 0) {
                $("#" + this.campoCodigoCliente.id).parent().append(this.botonBuscador);
            }

            this.camposObligatorios.forEach(campo => {
                if (campo.id) {
                    campo.required = true;
                }
            });
            this.botonGuardar.disabled = true;

            const leyendo = new Promise((resolve, reject) => {
                if (id > 0) {
                    $(".preloader").find("span").html("Obteniendo los datos de la solicitud");
                    $(".preloader").fadeIn("fast");
                    solicitudNueva = 'N';
                    let tmpId = encodeURIComponent(btoa(id));

                    let construyendo = fetchActions.getCats({
                        modulo: "CONTROLES",
                        archivo: '/RetiroAsociados/controlesGetSolicitud',
                        solicitados: [tmpId]
                    }).then((datosRespuesta) => {
                        configSolicitud.init({
                            ...datosRespuesta.solicitud
                        }).then(resolve).catch(reject);
                    }).catch(reject);
                    construyendo.then(() => {
                        $(".preloader").fadeOut("fast");
                        $(".preloader").find("span").html("");
                        resolve();
                    }).catch(reject);
                } else {
                    resolve();
                }
            });

            leyendo.then(() => {
                let camposEditados = document.querySelectorAll(".input-group-append button .fa-lock");
                if (camposEditados.length > 0) {
                    camposEditados.forEach(campo => {
                        generalHabilitarDeshabilitarEscritura(campo.parentNode);
                    });
                }
                $("#" + this.tabSolicitud.id).trigger("click");
                $("#" + this.tabMotivos.id).trigger("click");
            }).catch(generalMostrarError);

        });
    },
    searchAsociado: function () {
        formActions.validate('frmSolicitud').then(({
            errores
        }) => {
            if (errores == 0) {
                let codigoCliente = encodeURIComponent(btoa(this.campoCodigoCliente.value));
                fetchActions.getCats({
                    modulo: "CONTROLES",
                    archivo: "controlesComprobarSolicitud",
                    solicitados: [codigoCliente]
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                this.init({
                                    datosAsociado: {
                                        ...datosRespuesta.datosAsociado
                                    }
                                });
                                break;
                            case "PROCESO":
                                let nombreAsociado = datosRespuesta.estado.nombreCompleto,
                                    msg = "tiene una solicitud de retiro ya <strong>APROBADA</strong>, pendiente de pago.";
                                switch (datosRespuesta.estado.estado.trim()) {
                                    case "PENDIENTE":
                                        msg = "tiene una solicitud de retiro en proceso. <br/><strong>NOTA: </strong>Sólo es posible llevar una solicitud de retiro a la vez.";
                                        break;
                                }
                                Swal.fire({
                                    title: "¡Atención!",
                                    icon: "warning",
                                    html: `Nuestro asociado/a <strong>${nombreAsociado}</strong> ` + msg
                                });
                                break;
                            case "NoAsociado":
                                Swal.fire({
                                    title: "¡Atención!",
                                    icon: "info",
                                    html: "Asociado no registrado o retirado"
                                });
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    configEditables: function (estado = false) {
        this.camposEditables.forEach(campo => {
            let campoCompleto = document.getElementById(campo);
            if (campoCompleto.className.includes("selectpicker")) {
                campoCompleto.parentNode.parentNode.querySelector("button.input-group-text").disabled = estado;
            } else {
                campoCompleto.parentNode.querySelector("button.input-group-text").disabled = estado;
            }
        });
    },
    print: function (id = 0) {
        let tmpId = encodeURIComponent(btoa(id));
        fetchActions.getCats({
            modulo: "CONTROLES",
            archivo: '/RetiroAsociados/controlesGetSolicitud',
            solicitados: [tmpId, 'pdf']
        }).then((datosRespuesta) => {
            if (datosRespuesta.respuesta) {
                switch (datosRespuesta.respuesta.trim()) {
                    case "EXITO":
                        userActions.abrirArchivo('CONTROLES', datosRespuesta.nombreArchivo, 'solicitudRetiro', 'S');
                        break;
                    default:
                        generalMostrarError(datosRespuesta);
                        break;
                }
            } else {
                generalMostrarError(datosRespuesta);
            }
        }).catch();
    },
    cancel: function () {
        Swal.fire({
            title: 'Cancelar proceso',
            icon: 'warning',
            html: '¿Seguro/a que quieres cancelar?<br />Los cambios se perderán',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                window.top.location.href='./';
                // this.tabSolicitudes.classList.remove("disabled");
                // this.tabSolicitudesAprobadas.classList.remove("disabled");
                // this.tabSolicitud.classList.add("disabled");
                // generalShowHideButtonTab('btnsTabs', 'btnsPrincipal');
                // $("#" + this.tabSolicitudes.id).trigger("click");
            }
        })
    },
    save: function () {
        formActions.validate('frmSolicitud').then(({
            errores,
        }) => {
            if (errores == 0) {
                if (Object.keys(motivosRetiro).length == 0) {
                    errores++;
                    toastr.warning("Debe ingresar al menos un motivo de retiro");
                    $("#" + this.tabMotivos.id).trigger("click");
                }

                if (Object.keys(productos).length == 0 && errores == 0) {
                    errores++;
                    toastr.warning("Debe ingresar al menos un producto");
                    $("#" + this.tabProductos.id).trigger("click");
                }

                if (Object.keys(gestiones).length == 0 && errores == 0) {
                    errores++;
                    toastr.warning("Debe ingresar al menos una gestión");
                    $("#" + this.tabGestiones.id).trigger("click");
                }

                if (errores == 0) {
                    let datosPersonales = formActions.getJSON('frmSolicitud'),
                        detalleCreditos = {
                            montoPrestamos: this.campoMontoPrestamos.value,
                            montoCuentasCobrar: this.campoMontoCuentasCobrar.value,
                            deudores
                        },
                        datosCompletos = {
                            tipoApartado,
                            idApartado,
                            idSolicitud,
                            solicitudNueva,
                            datosPersonales,
                            motivosRetiro,
                            productos,
                            detalleCreditos,
                            gestiones,
                        };

                    fetchActions.set({
                        modulo: "CONTROLES",
                        archivo: 'controlesGuardarSolicitud',
                        datos: datosCompletos
                    }).then((datosRespuesta) => {
                        if (datosRespuesta.respuesta) {
                            switch (datosRespuesta.respuesta.trim()) {
                                case "EXITO":
                                    idSolicitud = datosRespuesta.idSolicitud;
                                    if (solicitudNueva == 'S') {
                                        Swal.fire({
                                            title: "¡Bien hecho!",
                                            html: "Datos guardados exitosamente.<br />¿Desea salir del formulario?",
                                            icon: "success",
                                            showCancelButton: true,
                                            showDenyButton: true,
                                            focusConfirm: false,
                                            allowOutsideClick: false,
                                            confirmButtonText: '<i class="fa fa-edit"></i> Seguir editando',
                                            cancelButtonText: '<i class="fa fa-print"></i> Imprimir solicitud',
                                            denyButtonText: '<i class="fa fa-times"></i> Salir del formulario',
                                        }).then((result) => {
                                            if (result.isDenied) {
                                                let tmpModulo = encodeURIComponent(btoa('CONTROLES'));
                                                window.top.location.href = `./?page=controlesRetiroAsociados&mod=${tmpModulo}`;
                                            } else if (!result.isConfirmed) {
                                                this.print(idSolicitud);
                                            }
                                        });
                                    } else {
                                        Swal.fire({
                                            title: "¡Bien hecho!",
                                            html: "Datos guardados exitosamente.",
                                            icon: "success",
                                            showCloseButton: false,
                                            showDenyButton: true,
                                            focusConfirm: false,
                                            allowOutsideClick: false,
                                            confirmButtonText: '<i class="fa fa-edit"></i> Seguir editando',
                                            denyButtonText: '<i class="fa fa-times"></i> Salir del formulario',
                                        }).then((result) => {
                                            if (result.isDenied) {
                                                let tmpModulo = encodeURIComponent(btoa('CONTROLES'));
                                                window.top.location.href = `./?page=controlesRetiroAsociados&mod=${tmpModulo}`;
                                            }
                                        });
                                    }
                                    break;
                                default:
                                    generalMostrarError(datosRespuesta);
                                    break;
                            }
                        } else {
                            generalMostrarError(datosRespuesta);
                        }
                    }).catch(generalMostrarError);
                }
            }
        }).catch(generalMostrarError);
    },
    resolucion: function (id) {
        formActions.clean("mdlResolucion").then(() => {
            let campoTituloMdl = document.querySelector("#mdlResolucion .modal-title span"),
                codigoCliente = $("#trSolicitud" + id).find("td:eq(1)").text(),
                nombreAsociado = $("#trSolicitud" + id).find("td:eq(2)").text();

            campoTituloMdl.innerHTML = codigoCliente + " - " + nombreAsociado;
            idSolicitud = id;
            $("#mdlResolucion").modal("show");
        });
    },
    saveResolucion: function () {
        formActions.validate('frmResolucion').then(({
            errores
        }) => {
            if (errores == 0) {
                let idResolucion = document.getElementById("cboResolucion").value,
                    observaciones = document.getElementById("txtObservacionResolucion").value,
                    fechaResolucion = document.getElementById('txtFechaResolucion').value;
                let datos = {
                    id: idSolicitud,
                    idResolucion,
                    observaciones,
                    fechaResolucion,
                    tipoApartado,
                    idApartado
                };

                fetchActions.set({
                    modulo: "CONTROLES",
                    archivo: 'controlesSaveResolucionSolicitud',
                    datos
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                Swal.fire({
                                    title: "¡Bien hecho!",
                                    html: "Datos guardados exitosamente",
                                    icon: "success"
                                }).then(() => {
                                    location.reload();
                                });
                                break;
                            default:
                                generalMostrarError(datosRespuesta);
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    delete: function (id) {
        let codigoCliente = $("#trSolicitud" + id).find("td:eq(1)").text(),
            nombreAsociado = $("#trSolicitud" + id).find("td:eq(2)").text();
        Swal.fire({
            title: 'Eliminar solicitud',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la solicitud de retiro de <strong>' + codigoCliente + ' - ' + nombreAsociado + '</strong>?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                let datos = {
                    id,
                    tipoApartado,
                    idApartado
                };
                fetchActions.set({
                    modulo: "CONTROLES",
                    archivo: "controlesDeleteSolicitudRetiro",
                    datos
                }).then((datosRespuesta) => {
                    if (datosRespuesta.respuesta) {
                        switch (datosRespuesta.respuesta.trim()) {
                            case "EXITO":
                                tblSolicitudes.row('#trSolicitud' + id).remove().draw();
                                break;
                            default:
                                generalMostrarError(datosRespuesta)
                                break;
                        }
                    } else {
                        generalMostrarError(datosRespuesta);
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    pago: function (id) {
        formActions.clean("mdlPago").then(() => {
            let campoTituloMdl = document.querySelector("#mdlPago .modal-title span"),
                codigoCliente = $("#trSolicitudResuelta" + id).find("td:eq(1)").text(),
                nombreAsociado = $("#trSolicitudResuelta" + id).find("td:eq(2)").text();
            idSolicitud = id;
            campoTituloMdl.innerHTML = codigoCliente + ' - ' + nombreAsociado;

            tblPagos.rows().iterator('row', function (context, index) {
                let node = $(this.row(index).node());
                let tmpId = node.attr("id").replace("trPagoProducto", ""),
                    montoProducto = "$0.00";

                solicitudesResueltas[id].productos.forEach(producto => {
                    if (producto.idProducto == tmpId) {
                        montoProducto = producto.monto.length > 0 ? "$" + parseFloat(producto.monto).toFixed(2) : "$0.00";
                    }
                });

                node.find("td:eq(2)").html(montoProducto);
                node.find("td:eq(3)").find("input").val("");
            });
            $("#mdlPago").modal("show");
        });
    },
    savePago: function () {
        let tmpProductos = {};
        tblPagos.rows().iterator('row', function (context, index) {
            let node = $(this.row(index).node());
            let tmpId = node.attr("id").replace("trPagoProducto", "");
            let inputMonto = node.find("td:eq(3)").find("input");

            if (inputMonto.val().length > 0) {
                tmpProductos[tmpId] = {
                    id: tmpId,
                    monto: inputMonto.val()
                };
            }
        });

        if (Object.keys(tmpProductos).length > 0) {
            let datos = {
                id: idSolicitud,
                tipoApartado,
                idApartado,
                productos: tmpProductos
            };
            fetchActions.set({
                modulo: "CONTROLES",
                datos,
                archivo: 'controlesSavePagoSolicitudRetiro'
            }).then((datosRespuesta) => {
                if (datosRespuesta.respuesta) {
                    switch (datosRespuesta.respuesta.trim()) {
                        case "EXITO":
                            Swal.fire({
                                title: "¡Bien hecho!",
                                icon: "success",
                                html: "Datos guardados exitosamente"
                            }).then(() => {
                                tblSolicitudesResueltas.row('#trSolicitudResuelta' + idSolicitud).remove().draw();
                                $("#mdlPago").modal("hide");
                            });
                            break;
                        default:
                            generalMostrarError(datosRespuesta);
                            break;
                    }
                } else {
                    generalMostrarError(datosRespuesta);
                }
            }).catch(generalMostrarError);
        } else {
            toastr.warning("Debe especificar al menos un pago");
        }
    }
}

const configMotivoRetiro = {
    cboMotivoRetiro: document.getElementById("cboMotivoRetiro"),
    campoExplicacion: document.getElementById("txtExplicacionMotivo"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            try {
                tmpDatos.forEach(motivo => {
                    cntMotivos++;
                    motivosRetiro[cntMotivos] = {
                        ...motivo,
                        motivoRetiro: catMotivosRetiro[motivo.idMotivo].motivoRetiro
                    };
                    let agregando = this.addRowTbl({
                        ...motivosRetiro[cntMotivos],
                        idFila: cntMotivos
                    });
                });

                tblMotivos.columns.adjust().draw();
                resolve();
            } catch (err) {
                console.error(err.message);
                reject();
            }
        });
    },
    edit: function (id = 0) {
        formActions.clean('mdlMotivoRetiro').then(() => {
            idFila = id;
            this.cboMotivoRetiro.disabled = false;
            let opciones = [`<option selected disabled value="">seleccione</option>`],
                incluidos = [];

            for (let key in motivosRetiro) {
                if (key != id) {
                    incluidos.push(motivosRetiro[key].idMotivo);
                }
            }

            for (let key in catMotivosRetiro) {
                if (!incluidos.includes(catMotivosRetiro[key].id)) {
                    opciones.push(`<option value="${catMotivosRetiro[key].id}">${catMotivosRetiro[key].motivoRetiro}</option>`);
                }
            }


            this.cboMotivoRetiro.innerHTML = opciones.join("");

            const leyendo = new Promise((resolve, reject) => {
                if (id == 0) {
                    resolve();
                } else {
                    this.cboMotivoRetiro.value = motivosRetiro[id].idMotivo;
                    this.campoExplicacion.value = motivosRetiro[id].explicacion;
                    this.cboMotivoRetiro.disabled = true;
                    resolve();
                }
            });

            leyendo.then(() => {
                if (this.cboMotivoRetiro.className.includes("selectpicker")) {
                    $("#" + this.cboMotivoRetiro.id).selectpicker("refresh");
                }
                $("#mdlMotivoRetiro").modal("show");
            });
        });
    },
    evalExplicacion: function (id = 0) {
        this.campoExplicacion.classList.remove("campoRequerido");
        this.campoExplicacion.required = false;

        if (catMotivosRetiro[id] && catMotivosRetiro[id].requiereExplicacion == 'S') {
            this.campoExplicacion.required = true;
            if (!this.campoExplicacion.className.includes("campoRequerido")) {
                this.campoExplicacion.classList.add("campoRequerido");
            }
        }
    },
    save: function () {
        formActions.validate('frmMotivoRetiro').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmpDatos = {
                    id: idFila > 0 ? motivosRetiro[idFila].id : 0,
                    idMotivo: this.cboMotivoRetiro.value,
                    motivoRetiro: this.cboMotivoRetiro.options[this.cboMotivoRetiro.options.selectedIndex].text,
                    explicacion: this.campoExplicacion.value,
                    propietario: idFila > 0 ? motivosRetiro[idFila].propietario : 'S'
                };
                let guardando = new Promise((resolve, reject) => {
                    if (idFila == 0) {
                        cntMotivos++;
                        idFila = cntMotivos;
                        let agregando = this.addRowTbl({
                            ...tmpDatos,
                            idFila: cntMotivos
                        });
                        agregando.then(() => {
                            tblMotivos.columns.adjust().draw(false);
                            resolve();
                        }).catch(reject);
                    } else {
                        $("#trMotivo" + idFila).find("td:eq(1)").html(tmpDatos.motivoRetiro);
                        $("#trMotivo" + idFila).find("td:eq(2)").html(tmpDatos.explicacion);
                        resolve();
                    }
                });

                guardando.then(() => {
                    motivosRetiro[idFila] = {
                        ...tmpDatos
                    };
                    $("#mdlMotivoRetiro").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        motivoRetiro,
        explicacion
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configMotivoRetiro.edit(" + idFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configMotivoRetiro.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblMotivos.row.add([
                    idFila,
                    motivoRetiro,
                    explicacion,
                    botones,
                ]).node().id = 'trMotivo' + idFila;
                resolve();
            } catch (err) {
                console.error(err.message);
                reject();
            }
        });
    },
    delete: function (id = 0) {
        let motivoRetiroLabel = motivosRetiro[id].motivoRetiro;
        Swal.fire({
            title: 'Eliminar motivo de retiro',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el motivo de retiro ' + motivoRetiroLabel + '?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> Eliminar',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                tblMotivos.row('#trMotivo' + id).remove().draw();
                tblMotivos.columns.adjust().draw();
                delete motivosRetiro[id];
            }
        });
    }
}

const configProducto = {
    cboProducto: document.getElementById("cboProducto"),
    campoMonto: document.getElementById("numMonto"),
    campoMontoTotal: document.getElementById("numMontoTotal"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            tmpDatos.forEach(producto => {
                cntProductos++;
                productos[cntProductos] = {
                    ...producto,
                    producto: catProductos[producto.idProducto].tipoCuenta
                };
                let agregar = this.addRowTbl({
                    ...productos[cntProductos],
                    idFila: cntProductos
                });
            });
            tblProductos.columns.adjust().draw();
            this.evalMontoTotal();
            resolve();
        });
    },
    edit: function (id = 0) {
        const leyendo = formActions.clean('frmProducto').then(() => {
            return new Promise((resolve, reject) => {
                idFila = id;
                this.cboProducto.disabled = false;
                let opciones = [`<option value="" selected disabled>seleccione</option>`],
                    incluidos = [];

                for (let key in productos) {
                    if (key != id) {
                        incluidos.push(productos[key].idProducto);
                    }
                }

                for (let key in catProductos) {
                    if (!incluidos.includes(catProductos[key].id)) {
                        opciones.push(`<option value="${catProductos[key].id}">${catProductos[key].tipoCuenta}</option>`);
                    }
                }

                this.cboProducto.innerHTML = opciones.join("");

                if (id == 0) {
                    resolve();
                } else {
                    this.cboProducto.value = productos[id].idProducto;
                    this.campoMonto.value = productos[id].monto;
                    this.cboProducto.disabled = true;
                    resolve();
                }
            });
        });
        leyendo.then(() => {
            if (this.cboProducto.className.includes("selectpicker")) {
                $("#" + this.cboProducto.id).selectpicker("refresh");
            }
            $("#mdlProducto").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmProducto').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmpDatos = {
                    id: idFila > 0 ? productos[idFila].id : 0,
                    idProducto: this.cboProducto.value,
                    producto: this.cboProducto.options[this.cboProducto.options.selectedIndex].text,
                    monto: this.campoMonto.value,
                    propietario: idFila > 0 ? productos[idFila].propietario : 'S'
                };
                let guardando = new Promise((resolve, reject) => {
                    if (idFila == 0) {
                        cntProductos++;
                        idFila = cntProductos;
                        let agregar = this.addRowTbl({
                            ...tmpDatos,
                            idFila: cntProductos
                        });
                        agregar.then(() => {
                            tblProductos.columns.adjust().draw(false);
                            resolve();
                        }).catch(reject);
                    } else {
                        $("#trProducto" + idFila).find("td:eq(1)").html(tmpDatos.producto);
                        $("#trProducto" + idFila).find("td:eq(2)").html("$" + parseFloat(tmpDatos.monto).toFixed(2));
                        resolve();
                    }
                });
                guardando.then(() => {
                    productos[idFila] = {
                        ...tmpDatos
                    };
                    this.evalMontoTotal();
                    $("#mdlProducto").modal("hide");
                });
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        producto,
        monto
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configProducto.edit(" + idFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configProducto.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblProductos.row.add([
                    idFila,
                    producto,
                    "$" + parseFloat(monto).toFixed(2),
                    botones
                ]).node().id = 'trProducto' + idFila;
                resolve();
            } catch (err) {
                console.error(err.message);
                reject();
            }
        });
    },
    delete: function (id = 0) {
        let productoLabel = productos[id].producto;
        Swal.fire({
            title: 'Eliminar producto',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> el producto ' + productoLabel + '?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                tblProductos.row('#trProducto' + id).remove();
                tblProductos.columns.adjust().draw();
                delete productos[id];
                this.evalMontoTotal();
            }
        });
    },
    evalMontoTotal: function () {
        let montoTotal = 0;
        for (let key in productos) {
            let monto = parseFloat(productos[key].monto);
            montoTotal += monto;
        }
        this.campoMontoTotal.value = montoTotal.toFixed(2);
        return;
    }
}

const configDeudor = {
    campoNombre: document.getElementById("txtDeudor"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            tmpDatos.forEach(deudor => {
                cntDeudores++;
                deudores[cntDeudores] = {
                    ...deudor
                };

                let agregar = this.addRowTbl({
                    ...deudor,
                    idFila: cntDeudores
                });
            });
            tblDeudores.columns.adjust().draw();
            resolve();
        });
    },
    save: function () {
        formActions.validate('frmDeudor').then(({
            errores
        }) => {
            if (errores == 0) {
                cntDeudores++;
                let tmpDatos = {
                    id: 0,
                    nombreDeudor: this.campoNombre.value,
                    propietario: "S",
                };
                deudores[cntDeudores] = {
                    ...tmpDatos
                };
                let agregar = this.addRowTbl({
                    ...tmpDatos,
                    idFila: cntDeudores
                });
                agregar.then(() => {
                    tblDeudores.columns.adjust().draw(false);
                    this.campoNombre.value = "";
                });
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        nombreDeudor
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configDeudor.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;
                tblDeudores.row.add([
                    idFila,
                    nombreDeudor,
                    botones
                ]).node().id = 'trDeudor' + idFila;
                resolve();
            } catch (err) {
                console.error(err.message);
                reject();
            }
        });
    },
    delete: function (id = 0) {
        let nombre = deudores[id].nombreDeudor;
        Swal.fire({
            title: 'Eliminar deudor',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> al deudor ' + nombre + '?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                tblDeudores.row('#trDeudor' + id).remove();
                tblDeudores.columns.adjust().draw();
                delete deudores[id];
            }
        });
    }
}

const configGestion = {
    cboArea: document.getElementById("cboArea"),
    campoGestion: document.getElementById("txtGestion"),
    init: function (tmpDatos) {
        return new Promise((resolve, reject) => {
            tmpDatos.forEach(gestion => {
                cntGestiones++;
                gestiones[cntGestiones] = {
                    ...gestion
                }

                let agregar = this.addRowTbl({
                    ...gestion,
                    idFila: cntGestiones
                });
            });
            tblGestiones.columns.adjust().draw();
            resolve();
        });
    },
    edit: function (id = 0) {
        const leyendo = formActions.clean("mdlGestion").then(() => {
            return new Promise((resolve, reject) => {
                idFila = id;
                this.cboArea.disabled = false;
                if (idFila == 0) {
                    resolve();
                } else {
                    let tmpDatos = {
                        ...gestiones[idFila]
                    };
                    this.cboArea.value = tmpDatos.area;
                    this.campoGestion.value = tmpDatos.gestion;

                    this.cboArea.disabled = true;
                    resolve();
                }
            });
        });
        leyendo.then(() => {
            if (this.cboArea.className.includes("selectpicker")) {
                $("#" + this.cboArea.id).selectpicker("refresh");
            }
            $("#mdlGestion").modal("show");
        });
    },
    save: function () {
        formActions.validate('mdlGestion').then(({
            errores
        }) => {
            if (errores == 0) {
                let currentDate = new Date(),
                    fechaActualizacion = ("0" + currentDate.getDate()).slice(-2) + "-" + ("0" + (currentDate.getMonth() + 1)).slice(-2) + "-" + currentDate.getFullYear() + " " + ("0" + currentDate.getHours()).slice(-2) + ":" + ("0" + currentDate.getMinutes()).slice(-2);
                let tmpDatos = {
                    id: idFila > 0 ? gestiones[idFila].id : idFila,
                    area: this.cboArea.value,
                    gestion: this.campoGestion.value,
                    fechaActualizacion: fechaActualizacion,
                    fechaRegistro: idFila > 0 ? gestiones[idFila].fechaRegistro : fechaActualizacion,
                    propietario: 'S',
                };

                let guardando = new Promise((resolve, reject) => {
                    if (idFila == 0) {
                        cntGestiones++;
                        idFila = cntGestiones;
                        let agregando = this.addRowTbl({
                            ...tmpDatos,
                            idFila: cntGestiones
                        });
                        agregando.then(resolve).catch(reject);
                    } else {
                        $("#trGestion"+idFila).find("td:eq(2)").html(tmpDatos.gestion);
                        resolve();
                    }
                });

                guardando.then(() => {
                    gestiones[idFila] = {
                        ...tmpDatos
                    };
                    $("#mdlGestion").modal("hide");
                }).catch(generalMostrarError);
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        idFila,
        area,
        gestion,
        fechaRegistro
    }) {
        return new Promise((resolve, reject) => {
            try {
                let nombreArea = catAreas[area].area,
                    botones = "<div class='tblButtonContainer'>" +
                    "<span class='btnTbl' title='Editar' onclick='configGestion.edit(" + idFila + ");'>" +
                    "<i class='fas fa-edit'></i>" +
                    "</span>" +
                    "<span class='btnTbl' title='Eliminar' onclick='configGestion.delete(" + idFila + ");'>" +
                    "<i class='fas fa-trash'></i>" +
                    "</span>" +
                    "</div>";
                botones = tipoPermiso != 'ESCRITURA' ? '' : botones;

                tblGestiones.row.add([
                    idFila,
                    nombreArea,
                    gestion,
                    fechaRegistro,
                    botones
                ]).draw().node().id = 'trGestion' + idFila;
                resolve();
            } catch (err) {
                console.error(err.message);
                reject();
            }
        });
    },
    delete: function (id) {
        Swal.fire({
            title: 'Eliminar gestión',
            icon: 'warning',
            html: '¿Seguro/a que quieres <b>eliminar</b> la gestión?.',
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: '<i class="fa fa-thumbs-up"></i> SI',
            cancelButtonText: '<i class="fa fa-thumbs-down"></i> NO',
        }).then((result) => {
            if (result.isConfirmed) {
                tblGestiones.row('#trGestion' + id).remove();
                tblGestiones.columns.adjust().draw();
                delete gestiones[id];
            }
        });
    }
}