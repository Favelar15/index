
let tblReporteCreditos
const modulo = 'CONTROLES'

window.onload = () => {

    document.getElementById('formCreditos').reset()

    const inicializando = initDataTable().then( () => {
        return new Promise((resolve, reject) => {

            fetchActions.getCats({
                modulo, archivo: 'controlesGetCats', solicitados: ['agenciasAsignadas']
            }).then(({agenciasAsignadas = []}) => {

                let opciones = [`<option value="all" selected>Todas las agencias</option>`]

                agenciasAsignadas.forEach(agencia => {
                    if (agencia.id != 700) {
                        opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                    }
                })

                reporteCreditos.cboAgencia.innerHTML = opciones.join('')
                $(`#${reporteCreditos.cboAgencia.id}`).selectpicker('refresh')

                resolve()
            }).catch(reject)
        })
    })

    inicializando.then( () => {
        $('.preloader').fadeOut('fast')
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblReporteCreditos').length > 0) {
                tblReporteCreditos = $('#tblReporteCreditos').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                    }, {
                        targets: [1, 2, 3, 4],
                        className: "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de la solicitud';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblReporteCreditos.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const reporteCreditos = {
    contador: 0,
    creditos: {},
    cboAgencia: document.getElementById('cboAgencia'),
    txtInicio: document.getElementById('txtInicio'),
    txtFin: document.getElementById('txtFin'),
    btnExcel: document.getElementById('btnExcel'),
    obtenerCreditos()
    {
        formActions.validate('formCreditos')
            .then( ( { errores = 0 }) => {

                if ( errores == 0)
                {
                    fetchActions.set({
                        modulo, archivo: 'Creditos/controlesReporteCreditos', datos: {
                            ...formActions.getJSON('formCreditos'),
                            accion: 'GENERAR'
                        }
                    }).then( ( { creditos = [] } ) => {

                        this.contador = 0
                        this.creditos = {}
                        tblReporteCreditos.clear()

                        if ( creditos.length > 0 )
                        {
                            this.txtInicio.disabled = true
                            this.txtFin.disabled = true

                            this.cboAgencia.disabled = true
                            $(`#${this.cboAgencia.id}`).selectpicker('refresh')
                            this.btnExcel.classList.remove('disabled')

                            creditos.forEach( credito => {
                                this.contador++
                                this.creditos[this.contador] = { ...credito }
                                this.agregarFila({
                                    contador: this.contador, ...credito
                                })
                            })
                        } else {
                            toastr.warning('No hay registros para mostrar')
                        }

                        tblReporteCreditos.columns.adjust().draw()

                    }).catch( generalMostrarError )
                }
            }).catch( generalMostrarError )
    },
    exportarCreditos()
    {
        if ( Object.keys(this.creditos).length > 0 )
        {
            fetchActions.set({
                modulo, archivo: 'Creditos/controlesReporteCreditos', datos: {
                    ...formActions.getJSON('formCreditos'),
                    creditos: { ...this.creditos },
                    accion: 'EXPORTAR'
                }
            }).then( response => {

                switch (response.respuesta.trim()) {
                    case 'EXITO':
                        window.open('./main/CONTROLES/docs/creditos/Excel/' + response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }
            }).catch( generalMostrarError )

        } else {
            toastr.warning('No hay registros para exportar')
        }
    },
    agregarFila( {contador, fechaRegistro, Agencia: { agencia }, numeroDePrestamo, nombresAsociado, apellidosAsociado,
                 Prestamo: { prestamo }, Empresa: { empresa }, monto, usuario } )
    {
        tblReporteCreditos.row.add([
            contador, fechaRegistro, agencia, numeroDePrestamo, `${nombresAsociado} ${apellidosAsociado}`,
            prestamo, empresa, `$ ${monto}`, usuario
        ]).node().id = `trReporteRemesas${contador}`
    },
    limpiar()
    {
        formActions.clean('formCreditos')
            .then( () => {
                this.contador = 0
                this.creditos = {}
                tblReporteCreditos.clear().draw()

                this.txtInicio.disabled = false
                this.txtFin.disabled = false

                this.cboAgencia.disabled = false
                $(`#${this.cboAgencia.id}`).selectpicker('refresh')
                this.btnExcel.classList.add('disabled')
            })
    },
    cambiosFecha()
    {
        $("#txtFin").datepicker('destroy')

        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        this.txtFin.value = ''
        $("#txtFin").datepicker('update')
    }
}