
let tblPagadurias
const modulo = 'CONTROLES'

window.onload = () => {

    const cboAgencia = document.getElementById('cboAgencia')

    const inicializando = initDataTable().then( () => {
        return new Promise( ( resolve, reject ) => {
            fetchActions.getCats({
                modulo, archivo: 'controlesGetCats', solicitados: ['agencias']
            }).then( ( { agencias = [] } ) => {

                let opciones = [`<option value="all" selected>Todas las agencias</option>`]

                agencias.forEach( agencia => {
                    if (agencia.id != 700) {
                        opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                    }
                })

                cboAgencia.innerHTML = opciones.join()
                $(`#${cboAgencia.id}`).selectpicker('refresh')

                resolve()

            }).catch( reject )
        })

        inicializando.then( () => {
            $('.preloader').fadeOut('fast')
        })
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblPagadurias').length > 0) {
                tblPagadurias = $('#tblPagadurias').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [
                        {
                            "targets": [0, 1],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblPagadurias.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const configReportePagadurias = {
    contador: 0,
    pagadurias: {},
    btnExportar: document.getElementById('btnExcel'),
    txtInicio: document.getElementById('txtInicio'),
    obtenerReportes()
    {
        formActions.validate('formPagadurias')
            .then( ( { errores } ) => {

                if( errores == 0 )
                {
                    fetchActions.set({
                        modulo, archivo: 'Pagadurias/controlesReportePagadurias', datos: {
                            ...formActions.getJSON('formPagadurias'),
                            accion: 'GENERAR'
                        }
                    }).then( ( { pagadurias } ) => {

                        tblPagadurias.clear()
                        this.pagadurias = {}
                        this.contador = 0

                        if( pagadurias.length > 0 )
                        {
                            this.resetearFormulario( true )

                            pagadurias.forEach( pagaduria => {
                                this.contador++
                                this.pagadurias[this.contador] = { ...pagaduria }
                                this.agregarFila( {
                                    contador: this.contador, ...pagaduria
                                })
                            })
                        } else {
                            toastr.warning('No hay registros para mostrar')
                        }

                        tblPagadurias.columns.adjust().draw()

                    }).catch( generalMostrarError )
                }
            })
    },
    agregarFila( { contador, fechaDesembolso, numeroCredito, codigoCliente, nombresAsociados, agencia } )
    {
        tblPagadurias.row.add([
            contador, fechaDesembolso, numeroCredito, codigoCliente, nombresAsociados, agencia
        ]).node().id = `tr${contador}`
    },
    exportarPagadurias()
    {
        fetchActions.set({
            modulo, archivo: 'Pagadurias/controlesReportePagadurias', datos: {
                ...formActions.getJSON('formPagadurias'),
                pagadurias: this.pagadurias,
                accion: 'EXPORTAR'
            }
        }).then( response => {

            switch (response.respuesta.trim()) {
                case 'EXITO':
                    window.open('./main/CONTROLES/docs/Pagadurias/Excel/' + response.nombreArchivo, '_blank');
                    break;
                default:
                    generalMostrarError(response);
                    break;
            }

        }).catch( generalMostrarError )
    },
    limpiar()
    {
        formActions.clean('formPagadurias')
                .then( () => {

                    this.resetearFormulario( false )

                    tblPagadurias.clear().draw()
                    this.pagadurias = {}
                    this.contador = 0

                })
    },
    resetearFormulario( estado )
    {
        let inputs = document.querySelectorAll('form#formPagadurias .form-control')

        if (estado)
        {
            this.btnExportar.classList.remove('disabled')
        }  else {
            this.btnExportar.classList.add('disabled')
        }

        inputs.forEach( input => {
            input.disabled = estado

            if( input.className.includes('selectpicker'))
            {
                $(`#${input.id}`).selectpicker('refresh')
            }
        })
    },
    cambiarFecha()
    {
        $("#txtFin").datepicker('destroy')

        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        document.getElementById('txtFin').value = ''
        $("#txtFin").datepicker('update')
    }
}