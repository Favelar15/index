let tblSolicitudesEnProceso
let tblSolicitudesCompletas

const modulo = 'CONTROLES'

window.onload = () => {

    const cboArea = document.querySelector('select.cboArea')
    const cboResolucion = document.querySelector('select.cboResolucion')

    initDatatable().then( () => {
        fetchActions.getCats( {
            modulo, archivo: 'controlesGetCats', solicitados: [ 'areas', 'resoluciones' ]
        }).then( ( { areas = [], resoluciones = [] } ) => {

            let opciones = [`<option value="" selected disabled>seleccione</option>`]

            areas.forEach(area => {
                if (area.area == 'Área de negocios' || area.area == 'Consejo de administración' || area.area == 'Gerencia general') {
                    opciones.push(`<option value="${area.id}">${area.area}</option>`)
                }
            })

            cboArea.innerHTML = opciones.join('')
            $(`#${cboArea.id}`).selectpicker('refresh')

            opciones = [`<option value="" selected disabled>seleccione</option>`]

            resoluciones.forEach(resolucion => {
                opciones.push(`<option value="${resolucion.id}">${resolucion.resolucion}</option>`)
            })



            cboResolucion.innerHTML = opciones.join('')
            $(`#${cboResolucion.id}`).selectpicker('refresh')

        }).catch( generalMostrarError )

        cargarListado()
    })

}

const cargarListado = () => {

    fetchActions.get({
        modulo,
        archivo: 'Peps/controlesObtenerListadoDeSolicitud',
        params: {
            idApartado: localStorage.getItem('idApartado'), tipoApartado: localStorage.getItem('tipoApartado')
        }
    }).then( ( { solicitudes = [], solicitudesResueltas = [] } ) => {

        gestionSolicitudEnProceso.solicitudesEnProceso = {}
        gestionSolicitudesAprobadas.solicitudesAprobadas = {}

        solicitudes.forEach( solicitud => {
            gestionSolicitudEnProceso.contador++
            gestionSolicitudEnProceso.solicitudesEnProceso[gestionSolicitudEnProceso.contador] = {
                contador: gestionSolicitudEnProceso.contador, ...solicitud
            }
        })

        solicitudesResueltas.forEach( solicitud => {
            gestionSolicitudesAprobadas.contador++
            gestionSolicitudesAprobadas.solicitudesAprobadas[gestionSolicitudesAprobadas.contador ] = {
                contador: gestionSolicitudesAprobadas.contador, ...solicitud
            }
        })

        gestionSolicitudEnProceso.cargarDatos()
        gestionSolicitudesAprobadas.cargarDatos()

    }).catch( generalMostrarError )
}

const gestionSolicitudEnProceso = {
    contador: 0,
    solicitudesEnProceso: {},
    cargarDatos() {
        tblSolicitudesEnProceso.clear().draw()

        for (const key in this.solicitudesEnProceso) {
            this.agregarFila({...this.solicitudesEnProceso[key]})
        }
        tblSolicitudesEnProceso.columns.adjust().draw()
    },
    agregarFila({contador, tipoSolicitud, nombres, apellidos, estado, resolucion, agencia, fechaRegistro, propietario}) {
        let botonEditar = `<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`;
        let botonEliminar = '', botonImprimir = '', botonAprobar = '';

        if (tipoPermiso === 'ESCRITURA' && propietario == 'S') {
            botonEditar = `<button class="btnTbl" onclick="gestionSolicitudEnProceso.editar(${contador})"><i class="fa fa-edit"></i></button>`
            botonImprimir = `<button class="btnTbl" onclick="gestionSolicitudEnProceso.generarPdf(${contador})"><i class="fas fa-print"></i></button>`
            botonEliminar = `<button class="btnTbl" onclick="gestionSolicitudEnProceso.eliminar(${contador})"><i class="fa fa-trash-alt"></i></button>`
            botonAprobar = `<button class="btnTbl" onclick="gestionSolicitudEnProceso.resolucion(${contador})"><i class="fas fa-check"></i></button>`
        } else if (tipoPermiso == 'ESCRITURA') {
            botonEditar = `<button class="btnTbl" onclick="gestionSolicitudEnProceso.editar(${contador})"><i class="fa fa-edit"></i></button>`
            botonAprobar = `<button class="btnTbl" onclick="gestionSolicitudEnProceso.resolucion(${contador})"><i class="fas fa-check"></i></button>`
            botonImprimir = `<button class="btnTbl" onclick="gestionSolicitudEnProceso.generarPdf(${contador})"><i class="fas fa-print"></i></button>`
        }

        tblSolicitudesEnProceso.row.add([
            contador, `${tipoSolicitud}`, `${nombres} ${apellidos}`, `${estado} ${ resolucion ? '/'+resolucion :'' }`, agencia, fechaRegistro,
            `<div class="tblButtonContainer">
                    ${botonEditar} ${botonAprobar} ${botonImprimir} ${botonEliminar}
            </div>`
        ]).node().id = `trSolicitudesPro${contador}`
    },
    eliminar( contador )
    {
        const solicitud = gestionSolicitudEnProceso.solicitudesEnProceso[contador]

        Swal.fire({
            title: 'IMPORTANTE',
            text: `¿Quieres eliminar la solicitud PEP a nombre de: ${solicitud.nombres} ${solicitud.apellidos}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            cancelButtonColor: '#f54343',
            confirmButtonText: 'Eliminar'
        }).then( result => {
            if (result.isConfirmed)
            {
                fetchActions.set({
                    modulo, archivo: 'Peps/controlesEliminarSolicitud',
                    datos: { id: solicitud.id, idApartado, tipoApartado }
                }).then( response => {

                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'Solicitud Pep eliminada con exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(i => {
                                tblSolicitudesEnProceso.row(`#trSolicitudesPro${contador}`).remove().draw()
                                delete this.solicitudesEnProceso[contador]
                            });
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }

                }).catch( generalMostrarError )
            }
        })
    },
    nuevaSoliccitud() {
        window.location = `?page=controlesPeps&mod=${btoa(modulo)}`
    },
    editar( contador )
    {
        const idSolicitud = gestionSolicitudEnProceso.solicitudesEnProceso[contador].id
        window.location = `?page=controlesPeps&mod=${btoa(modulo)}&solicitud=${btoa(idSolicitud)}`
    },
    resolucion( contador ) {

        formActions.clean('formResolucion')
            .then( () => {

                this.idFila = contador

                $(`#modal-aprobarSolicitud`).modal('show')

            }).catch( generalMostrarError )
    },
    guardarResolucion()
    {
        formActions.validate('formResolucion')
            .then( ( { errores }) => {

                if ( errores == 0 )
                {
                    let datosResolucion = formActions.getJSON('formResolucion')

                    const id = gestionSolicitudEnProceso.solicitudesEnProceso[this.idFila].id

                    fetchActions.set({
                        modulo, archivo: 'Peps/controlesGuardarResolucion', datos: { id, ...datosResolucion, idApartado, tipoApartado }
                    }).then( response => {

                        switch (response.respuesta.trim()) {
                            case 'EXITO':
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: '¡Bien hecho!',
                                    text: 'Datos guardado exitosamente',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(i => {
                                    cargarListado()
                                    $(`#modal-aprobarSolicitud`).modal('hide')
                                });
                                break;
                            default:
                                generalMostrarError(response);
                                break;
                        }
                    }).catch( generalMostrarError )
                }
            })
    },
    generarPdf( contador )
    {
        const idSolicitud = gestionSolicitudEnProceso.solicitudesEnProceso[contador].id

        console.log( idSolicitud )

        fetchActions.get({
            modulo, archivo: 'Peps/controlesSolicitudPDF', params: { idSolicitud }
        }).then( response => {

            switch (response.respuesta.trim()) {
                case 'EXITO':
                    userActions.abrirArchivo( modulo, response.nombreArchivo, 'solicitudesPeps', 'S');
                    break;
                default:
                    generalMostrarError(response);
                    break;
            }
        }).catch( generalMostrarError )
    }
}
const gestionSolicitudesAprobadas = {
    contador: 0,
    solicitudesAprobadas: {},
    cargarDatos()
    {
        tblSolicitudesCompletas.clear().draw()
        for ( const key in this.solicitudesAprobadas )
        {
            this.agregarFila( {... this.solicitudesAprobadas[key] } )
        }
        tblSolicitudesCompletas.columns.adjust().draw()
    },
    agregarFila({contador, tipoSolicitud, nombres, apellidos, estado, resolucion, agencia, fechaResolucion, propietario}) {

        tblSolicitudesCompletas.row.add([
            contador, `${tipoSolicitud}`, `${nombres} ${apellidos}`, `${estado} / ${resolucion}`, agencia, fechaResolucion
        ]).node().id = `trSolicitudesAprobadas${contador}`
    }
}
const initDatatable = () => {
    return new Promise( ( resolve, reject ) => {
        try {
            if ($('#tblSolicitudesEnProceso').length > 0) {
                tblSolicitudesEnProceso = $('#tblSolicitudesEnProceso').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": true,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            width: '5%'
                        },
                        { targets: [4], width: '12%' }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSolicitudesEnProceso.columns.adjust().draw();
            }

            if ($('#tblSolicitudesCompletas').length > 0) {
                tblSolicitudesCompletas = $('#tblSolicitudesCompletas').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": true,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            width: '5%'
                        },
                        {
                            targets: [1, 3], orderable: false
                        },
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblSolicitudesCompletas.columns.adjust().draw();
            }
            resolve()
        } catch (e) {
            reject( e.message )
        }
    })
}
