
let tblAplicaciones, tblActivaciones
const modulo = 'CONTROLES'

window.onload = () => {

    $('.preloader').fadeOut('fast')

    document.getElementById('formDatosAsociado').reset()
    const cboTipoDocumento = document.getElementById('cboTipoDocumento')
    const cboPais = document.getElementById('cboPais')
    const cboSubAplicacion = document.getElementById('cboSubAplicacion')
    const cboAgencia = document.getElementById('cboAgencia')

    fetchActions.getCats({
        modulo, archivo: 'controlesGetCats', solicitados: ['tiposDocumento', 'geograficos', 'agencias', 'subAplicaciones']
    }).then( ({tiposDocumento: documentos, datosGeograficos: geograficos =[], agencias = [], subAplicaciones = []
              }) => {

        if ( cboTipoDocumento )
        {
            let opciones = [`<option selected value="" disabled>Seleccione</option>`]

            documentos.forEach( documento => {
                tiposDocumento[documento.id] = {...documento}
                if (documento.tipoDocumento == 'DUI' || documento.tipoDocumento == 'Pasaporte') {
                    opciones.push(` <option value="${documento.id}">${documento.tipoDocumento}</option> `)
                }
            })

            cboTipoDocumento.innerHTML = opciones.join('')
            $(`#${cboTipoDocumento.id}`).selectpicker('refresh')
        }

        if( cboPais )
        {
            datosGeograficos = {
                ...geograficos
            }
            let opciones = ['<option selected value="" disabled>seleccione</option>']

            for (let key in geograficos) {
                opciones.push(`<option value="${geograficos[key].id}">${geograficos[key].nombrePais}</option>`);
            }

            cboPais.innerHTML = opciones.join('')
            $(`#${cboPais.id}`).selectpicker('refresh')
        }

        if(cboAgencia)
        {
            let opciones = [`<option selected disabled value="">seleccione</option>`]

            agencias.forEach( agencia => {
                if (agencia.id != 700) {
                    opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                }
            })

            cboAgencia.innerHTML = opciones.join('')
            $(`#${cboAgencia.id}`).selectpicker('refresh')
        }

        subAplicacion.catAgencias = [...agencias]

        let opciones = [`<option value="" disabled selected>seleccione</option>`]

        subAplicaciones.forEach( sub => {
            opciones.push(`<option value="${sub.idSubAplicacion}"> ${sub.idSubAplicacion} -  ${sub.tipoCuenta}</option>`)
        })

        cboSubAplicacion.innerHTML = opciones.join('')
        $(`#${cboSubAplicacion.id}`).selectpicker('refresh')

    }).catch( generalMostrarError )

    initDataTable().then( () => {

        activacion.cargarDatos()

    }).catch( generalMostrarError )
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {

            if ($('#tblAplicaciones').length > 0) {
                tblAplicaciones = $('#tblAplicaciones').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                        width: '10%'
                    },
                        {
                            targets: [4],
                            className: 'text-center',
                            width: '12%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblAplicaciones.columns.adjust().draw();
            }

            if ($('#tblActivaciones').length > 0) {
                tblActivaciones = $('#tblActivaciones').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                        width: '10%'
                    },
                        {
                            targets: [5],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblActivaciones.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const asociado = {
    txtCodigoCliente: document.getElementById('txtCodigoCliente'),
    btnGuardar: document.getElementById('btnGuardar'),
    btnBuscar: document.getElementById('btnBuscarAscociado'),
    btnAgregarAplicacion: document.getElementById('btnAgregarAplicacion'),
    buscar()
    {
        formActions.validate('formDatosAsociado')
            .then( ( { errores } ) => {

                if ( errores == 0 )
                {
                    generalVerificarCodigoCliente(this.txtCodigoCliente.value)
                        .then( data => {
                            if (data.respuesta) {

                                this.txtCodigoCliente.readOnly = true;
                                this.btnBuscar.classList.add('disabled');
                                this.btnGuardar.classList.remove('disabled')
                                this.btnAgregarAplicacion.classList.remove('disabled')

                                let {
                                    nombresAsociado,
                                    apellidosAsociado,
                                    tipoDocumentoPrimario,
                                    numeroDocumentoPrimario,
                                    numeroDocumentoSecundario,
                                    agencia,
                                    telefonoFijo,
                                    telefonoMovil,
                                    pais,
                                    departamento,
                                    municipio,
                                    direccionCompleta
                                } = data.asociado;

                                document.getElementById('txtNombres').value = nombresAsociado;
                                document.getElementById('txtApellidos').value = apellidosAsociado;
                                document.getElementById('txtTelefonoFijo').value = telefonoFijo;
                                document.getElementById('txtTelefonoMovil').value = telefonoMovil;
                                document.getElementById('cboTipoDocumento').value = tipoDocumentoPrimario
                                document.getElementById('txtDocumentoPrimario').value = numeroDocumentoPrimario;
                                document.getElementById('txtAgenciaAfiliacion').value = agencia;
                                document.getElementById('txtNit').value = numeroDocumentoSecundario;

                                document.getElementById('cboPais').value = pais;
                                $("#cboPais").trigger("change");
                                document.getElementById('cboDepartamento').value = departamento;
                                $("#cboDepartamento").trigger("change");
                                document.getElementById('cboMunicipio').value = municipio;

                                document.getElementById('txtDireccionCompleta').value = direccionCompleta;

                                const selects = document.querySelectorAll('form#formDatosAsociado select.selectpicker')

                                selects.forEach( cbo => {
                                    $(`#${cbo.id}`).selectpicker('refresh')
                                })

                                document.getElementById('formDatosAsociado').action = 'javascript:subAplicacion.guardar();';

                            } else if ( !data.respuesta )
                            {
                                Swal.fire({
                                    title: 'INFORMACION',
                                    html: `No existe un asociado con el código <strong>${this.txtCodigoCliente.value}</strong> en la base de datos. ¿Desea agregarlo?`,
                                    icon: 'warning',
                                    showCloseButton: true,
                                    showCancelButton: true,
                                    confirmButtonColor: '#46b1c3',
                                    confirmButtonText: `<i class="fa fa-check-circle"></i> SI`,
                                    cancelButtonText: `<i class="fa fa-times-circle"></i> NO`
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.href = `?page=controlesAsociados&mod=${encodeURIComponent(btoa('CONTROLES'))}`;
                                    }
                                });
                            }
                        }).catch( generalMostrarError )
                }

        }).catch( generalMostrarError )
    }
}

const subAplicacion = {
    idFila: 0,
    contador: 0,
    catAgencias: [],
    catSubAplicaciones: [],
    subAplicaciones:{},
    cboAgencia : document.getElementById('cboAgencia'),
    cboSubAplicacion: document.getElementById('cboSubAplicacion'),
    txtNumeroCuenta : document.getElementById('txtNumeroCuenta'),
    mostrarModal( idFila = 0 )
    {
        this.idFila = idFila

        formActions.clean('formSubAplicaciones')
            .then( () => {

                this.cboAgencia.disabled = false

                if ( this.idFila > 0 )
                {
                    this.cboAgencia.value = this.subAplicaciones[this.idFila].idAgencia
                    this.cboAgencia.disabled = true
                    this.cboSubAplicacion.value = this.subAplicaciones[this.idFila].idSubAplicacion
                    this.txtNumeroCuenta.value = this.subAplicaciones[this.idFila].numeroCuenta
                    $(`#${this.cboSubAplicacion.id}`).selectpicker('refresh')
                }

                $(`#${this.cboAgencia.id}`).selectpicker('refresh')

                $(`#modal-aplicaciones`).modal('show')

            }).catch( generalMostrarError)
    },
    agregar()
    {
        formActions.validate('formSubAplicaciones')
            .then( ({errores}) => {

                if (errores == 0 )
                {
                    const {
                        cboAgencia, cboSubAplicacion, txtNumeroCuenta
                    } = formActions.getJSON('formSubAplicaciones')

                    if ( this.idFila == 0 )
                    {
                        this.contador++
                        this.subAplicaciones[this.contador] = {
                            idAgencia: cboAgencia.value,
                            agencia: cboAgencia.labelOption,
                            idSubAplicacion: cboSubAplicacion.value,
                            tipoCuenta: cboSubAplicacion.labelOption,
                            numeroCuenta: txtNumeroCuenta.value
                        }
                        this.agregarFila( {contador: this.contador, ...this.subAplicaciones[this.contador] } )
                        tblAplicaciones.columns.adjust().draw()
                    } else
                    {
                        this.subAplicaciones[this.idFila] = {...this.subAplicaciones[this.idFila], tipoCuenta: cboSubAplicacion.labelOption, numeroCuenta: txtNumeroCuenta.value }
                        this.editar( { ...this.subAplicaciones[this.idFila] })
                    }

                    $(`#modal-aplicaciones`).modal('hide')
                }
            }).catch( generalMostrarError )
    },
    agregarFila( { contador, agencia, tipoCuenta, numeroCuenta })
    {
        let editar = `<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`
        let eliminar = ``

        if (tipoPermiso == 'ESCRITURA')
        {
            editar = `<button type="button" class="btnTbl" onclick="subAplicacion.mostrarModal(${contador})"><i class="fas fa-edit"></i></button>`
            eliminar = `<button type="button" class="btnTbl" onclick="subAplicacion.eliminar(${contador})"><i class="fas fa-times-circle"></i></button>`
        }

        tblAplicaciones.row.add([
            contador, agencia, tipoCuenta, numeroCuenta,
            `<div class="tblButtonContainer"> ${editar} ${eliminar} </div>`
        ]).node().id = `trAplicacion${contador}`
    },
    editar( { tipoCuenta, numeroCuenta })
    {
        $(`#trAplicacion${this.idFila}`).find('td:eq(2)').html(tipoCuenta)
        $(`#trAplicacion${this.idFila}`).find('td:eq(3)').html(numeroCuenta)
    },
    eliminar( idFila )
    {
        delete this.subAplicaciones[idFila]
        tblAplicaciones.row(`#trAplicacion${idFila}`).remove().draw(false);
    },
    guardar()
    {
        formActions.validate('formDatosAsociado')
            .then( ( { errores } ) => {

                if( errores == 0)
                {
                    if (Object.keys(this.subAplicaciones).length > 0 )
                    {
                        fetchActions.set({
                            modulo, archivo: 'ActivacionDeCuentas/controlesGuardarReactivacion',
                            datos: { idApartado, tipoApartado, codigoCliente: asociado.txtCodigoCliente.value, aplicaciones: {...this.subAplicaciones } }
                        }).then( response => {

                            switch (response.respuesta.trim()) {
                                case 'EXITO':

                                    Swal.fire({
                                        title: "¡Bien hecho!",
                                        html: "La activación de la cuenta se realizó con exito.<br />¿Desea salir del formulario?",
                                        icon: "success",
                                        showCancelButton: true,
                                        showDenyButton: true,
                                        focusConfirm: false,
                                        allowOutsideClick: false,
                                        confirmButtonText: '<i class="fa fa-edit"></i> Nueva activación',
                                        cancelButtonText: '<i class="fa fa-print"></i> Imprimir solicitud',
                                        denyButtonText: '<i class="fa fa-times"></i> Salir del formulario',
                                    }).then(result => {
                                        if (result.isDenied) {
                                            this.limpiar()
                                            activacion.salirFormulario()
                                            activacion.cargarDatos()

                                        } else if (!result.isConfirmed) {
                                            contruirPDF(response.idSolicitud)
                                        } else
                                        {
                                            this.limpiar()
                                        }
                                    })
                                    break;
                                default:
                                    generalMostrarError(response);
                                    break;
                            }

                        }).catch( generalMostrarError )

                    } else
                    {
                        toastr.warning('¡Tiene que agregar al menos una Sub Aplicación!')
                        asociado.btnAgregarAplicacion.focus()
                        asociado.btnAgregarAplicacion.scrollIntoView({behavior: "auto", block: "center", inline: "end"})
                    }
                }
            }).catch( generalMostrarError )
    },
    limpiar()
    {
        formActions.clean('formDatosAsociado')
            .then( () => {
                asociado.txtCodigoCliente.value = ''
                asociado.txtCodigoCliente.readOnly = false
                asociado.btnGuardar.classList.add('disabled')
                asociado.btnBuscar.classList.remove('disabled')
                asociado.btnAgregarAplicacion.classList.add('disabled')

                this.subAplicaciones = {}
                this.contador = 0
                tblAplicaciones.clear().draw()
                document.getElementById('formDatosAsociado').action = 'javascript:asociado.buscar();';
            }).catch( generalMostrarError)
    },
    cancelar()
    {
        Swal.fire({
            title: '¿Está seguro de salir del formulario de activación?',
            text: "¡Seleccione una opcion!",
            icon: 'warning',
            showCancelButton: true,
            showDenyButton: true,
            showCloseButton: true,
            confirmButtonColor: '#46b1c3',
            denyButtonText: `<i class="fas fa-trash"></i> Limpiar formulario`,
            confirmButtonText: `<i class="fas fa-check-circle"></i> Salir del formulario`,
            cancelButtonText: '<i class="fas fa-times-circle"></i> Cancelar'
        }).then( result => {
            if ( result.isDenied )
            {
               this.limpiar()
            } else if (result.isConfirmed)
            {
                this.limpiar();
                activacion.salirFormulario();
                activacion.cargarDatos()
            }
        })
    }
}

const activacion = {
    tabLinkFormulario: document.getElementById('formularioActivacion-tab'),
    tabLinkListado: document.getElementById('ListadoActivaciones-tab'),
    idFila: 0,
    contador: 0,
    activaciones: {},
    cargarDatos()
    {

        fetchActions.get({
            modulo, archivo: 'ActivacionDeCuentas/controlesObtenerActivaciones', params: {
                idApartado, tipoApartado
            }
        }).then( ( { activaciones = [] } ) => {

            tblActivaciones.clear();

            activaciones.forEach( activ => {
                this.contador++
                this.activaciones[this.contador] = { ...activ }
                this.agregarFila( { contador: this.contador, ...activ } )
            })

            tblActivaciones.columns.adjust().draw()

        }).catch( generalMostrarError )

    },
    agregarFila({ contador, codigoCliente, nombreAsociado, agencia, fechaRegistro })
    {
        let eliminar  = `<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`
        let imprimir = ''

        if ( tipoPermiso == 'ESCRITURA')
        {
            imprimir = `<button type="button" class="btnTbl" onclick="activacion.imprimirSolicitud(${contador})"><i class="fas fa-print"></i></button>`
            eliminar = `<button class="btnTbl" onclick="activacion.eliminar(${contador})"><i class="fas fa-trash"></i></button>`
        }

        tblActivaciones.row.add([
            contador, codigoCliente, nombreAsociado, agencia, fechaRegistro,
            `<div class="tblButtonContainer"> ${imprimir} ${eliminar}</div>`
        ]).node().id = `trActivaciones${contador}`
    },
    eliminar( idFila )
    {
        const datosActivacion = this.activaciones[idFila];

        Swal.fire({
            title: 'IMPORTANTE',
            text: `¿Quieres anular la activación de cuenta de el asociado ${datosActivacion.nombreAsociado} ?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            confirmButtonText: `<i class="fas fa-thumbs-up"></i> Eliminar`,
            cancelButtonText: `<i class="fas fa-thumbs-up"></i> Cancelar`
        }).then((result) => {
            if (result.isConfirmed) {

                fetchActions.set({
                    modulo, archivo: 'ActivacionDeCuentas/controlesEliminarActivacion',
                    datos : {
                        idApartado, tipoApartado, idActivacion: datosActivacion.idActivacion
                    }
                }).then( response => {

                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                html: 'Se anulo correctamente la activación',
                                showConfirmButton: false,
                                timer: 1500
                            }).then( () => {
                                delete this.activaciones[this.idFila]
                                tblActivaciones.row(`#trActivaciones${idFila}`).remove().draw(false)
                            });
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }
                }).catch( generalMostrarError )
            }
        });

    },
    irFormulario()
    {
        this.tabLinkFormulario.classList.remove('disabled')
        this.tabLinkListado.classList.add('disabled')
        $(`#${this.tabLinkFormulario.id}`).trigger('click')
    },
    salirFormulario()
    {
        this.tabLinkListado.classList.remove('disabled')
        this.tabLinkFormulario.classList.add('disabled')
        $(`#${this.tabLinkListado.id}`).trigger('click')
    },
    imprimirSolicitud( idFila )
    {
        const id = this.activaciones[idFila].idActivacion

        contruirPDF(id)
    }
}

const contruirPDF = (idSolicitud) => {

    fetchActions.get({
        modulo, archivo: 'ActivacionDeCuentas/controlesSolicitudPDF', params : { idSolicitud }
    }).then( response => {

        switch (response.respuesta.trim()) {
            case 'EXITO':
                userActions.abrirArchivo(modulo, response.nombreArchivo, 'solicitudesActivacion', 'S');
                break;
            default:
                generalMostrarError(response);
                break;
        }
    }).catch( generalMostrarError)

}

