let tblMovimientos, tblInventario;

let contadorInventario = 0, datosInventario = {}
let idFila = 0;

let datosAgencias = []

let cboAgencias, cboAcciones, cboAgenciaInventario;

const modulo = 'CONTROLES';

window.onload = () => {

    cboAgenciaInventario = document.querySelector('select.cboAgenciaInventario')
    cboAcciones = document.querySelector('select.cboAcciones')
    cboAgencias = document.querySelector('select.cboAgencia')


    const obtenerCatalogos = initDatatble().then( () => {

        fetchActions.getCats({
            modulo,
            archivo: 'controlesGetCats',
            solicitados: [ 'acciones', 'agencias']
        }).then( ( { acciones = [], agencias = [] } ) => {
            agencias.push( { id: "1", nombreAgencia: 'FEDECACES' } )
            datosAgencias = [ ... agencias ]

            let opciones = [`<option value="" disabled selected>seleccione</option>`]

            acciones.forEach( accion => {
                opciones.push( `<option value="${ accion.id }">${ accion.accion }</option>` )
            })

            cboAcciones.innerHTML = opciones.join('');
            if (cboAcciones.className.includes('selectpicker')) {
                $( `#${cboAcciones.id}`).selectpicker("refresh");
            }

        }).catch( generalMostrarError )
    })

    obtenerCatalogos.then( () => {
        configInventario.init().then( () => {
            $(".preloader").fadeOut("fast")
        }).catch( generalMostrarError )
    })

}

const configInventario = {
    id: 0,
    idFila: 0,
    contador: 0,
    inventarios: {},
    txtCantidadPines: document.getElementById('txtCantidadPines'),
    txtCantidadPlastico: document.getElementById('txtCantidadPlastico'),
    init()
    {
        return new Promise( ( resolve, reject ) => {
            try {
                fetchActions.get({
                    modulo,
                    archivo: 'PYP/controlesObtenerInventarios',
                    params: {}
                }).then( ( { inventarios = [] } ) => {

                    tblInventario.clear().draw();
                    this.contador = 0;
                    this.inventarios = { }
                    let opciones = [ `<option value="0" selected disabled>seleccione</option>` ]

                    inventarios.forEach( inventario => {
                        this.contador++
                        this.inventarios[ this.contador ] = { ...inventario }
                        opciones.push( `<option value="${inventario.id}">${inventario.Agencia.agencia}</option>` )
                        this.agregarFila( { contador: this.contador, ...inventario} )
                    })
                    if ( cboAgenciaInventario.className.includes('selectpicker') )
                    {
                        cboAgenciaInventario.innerHTML = opciones.join('')
                        $(`#${cboAgenciaInventario.id}`).selectpicker('refresh')
                    }
                    tblInventario.columns.adjust().draw();
                    resolve()
                }).catch( reject )
            } catch (e) {
                reject( e.message )
            }
        })
    },
    save()
    {
        formActions.validate('frmInventario')
            .then( ( { errores } ) => {

                if ( errores == 0 )
                {
                    this.txtCantidadPlastico.classList.remove('is-invalid', 'is-valid')
                    this.txtCantidadPines.classList.remove('is-invalid', 'is-valid')

                    const datos = formActions.getJSON('frmInventario')
                    datos.idInventario = this.inventarios[this.idFila].id
                    datos.id = 0

                    const form = document.querySelector('form#frmInventario')
                    form.classList.remove('was-validated')


                    if ( datos.cboAcciones.labelOption == 'SALIDA')
                    {
                        if ( (this.inventarios[this.idFila].stockPines - datos.txtCantidadPines.value )  < 0  )
                        {
                            errores++
                            this.txtCantidadPines.classList.remove('is-valid')
                            this.txtCantidadPines.classList.add('is-invalid')
                        } else
                        {
                            this.txtCantidadPines.classList.add('is-valid')
                            this.txtCantidadPines.classList.remove('is-invalid')
                        }


                        if ( ( this.inventarios[this.idFila].stockPlastico - datos.txtCantidadPlastico.value ) < 0 )
                        {
                            console.log('error en plasticos')
                            this.txtCantidadPlastico.classList.remove('is-valid')
                            this.txtCantidadPlastico.classList.add('is-invalid')
                            errores++
                        }else {
                            this.txtCantidadPlastico.classList.add('is-valid')
                            this.txtCantidadPlastico.classList.remove('is-invalid')
                        }
                    }

                     if ( errores == 0 )
                     {
                         fetchActions.set({
                             modulo,
                             archivo: 'PYP/controlesGuardarInventario',
                             datos : { ...datos, idApartado, tipoApartado, logs: [] }
                         }).then( response => {

                             $('.preloader').fadeOut('fast')
                             $(".preloader").find("span").html("")

                             switch (response.respuesta.trim()) {
                                 case 'EXITO':
                                     Swal.fire({
                                         position: 'top-end',
                                         icon: 'success',
                                         title: '¡Bien hecho!',
                                         text: 'Movimiento realizado exitosamente',
                                         showConfirmButton: false,
                                         timer: 1500
                                     }).then(i => {

                                         if (datos.cboAcciones.value == 1) {
                                             this.inventarios[this.idFila].stockPines = parseInt(this.inventarios[this.idFila].stockPines) + parseInt(datos.txtCantidadPines.value)
                                             this.inventarios[this.idFila].stockPlastico = parseInt(this.inventarios[this.idFila].stockPlastico) + parseInt(datos.txtCantidadPlastico.value)

                                         } else {
                                             this.inventarios[this.idFila].stockPines = parseInt(this.inventarios[this.idFila].stockPines) - parseInt(datos.txtCantidadPines.value)
                                             this.inventarios[this.idFila].stockPlastico = parseInt(this.inventarios[this.idFila].stockPlastico) - parseInt(datos.txtCantidadPlastico.value)
                                         }

                                         $(`#trInventario${this.idFila}`).find('td:eq(2)').html(`<h5><span class="${crearBadge(this.inventarios[this.idFila].stockPlastico)}">${this.inventarios[this.idFila].stockPlastico}</span></h5>`)
                                         $(`#trInventario${this.idFila}`).find('td:eq(3)').html(`<h5><span class="${crearBadge(this.inventarios[this.idFila].stockPines)}">${this.inventarios[this.idFila].stockPines}</span></h5>`)

                                         tblInventario.columns.adjust().draw()

                                         cboAgenciaInventario.value = this.inventarios[this.idFila].id
                                         $(`#${cboAgenciaInventario.id}`).selectpicker('refresh')
                                         $(`#${cboAgenciaInventario.id}`).trigger('change')

                                         $(`#movimientos-Modal`).modal('hide')
                                     });
                                     break;
                                 default :
                                     generalMostrarError( response )
                                     break;
                             }

                         }).catch( generalMostrarError )
                     }
                }
            })
    },
    agregarFila(  {contador, Agencia: { agencia }, plastico, stockPlastico, stockPines} )
    {
        return new Promise( ( resolve, reject ) => {
            try {
                const badgePlastico = `<h5><span class="${crearBadge(stockPlastico)}">${stockPlastico}</span></h5>`;
                const badgePines = `<h5><span class="${crearBadge(stockPines)}">${stockPines}</span></h5>`;

                let botonConsumir = `<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`
                let botonMovimiento = ``
                let botonTraslado = ``
                let movimientos = ``

                if ( tipoPermiso == 'ESCRITURA')
                {
                    if ( agencia != 'Oficina Central')
                    {
                        botonTraslado = `<button class="btnTbl" onclick='configInventario.mostrarModal(  ${contador}, false, true)'><i class="fa fa-arrow-alt-circle-down"></i> </button>`
                    }
                    botonConsumir = `<button class="btnTbl" onclick='configInventario.mostrarModal( ${contador} )'><i class="fa fa-plus-circle"></i> </button>`;
                    botonMovimiento = `<button class="btnTbl" onclick='configInventario.mostrarModal(  ${contador}, false )'><i class="fas fa-sync"></i> </button>`
                    movimientos = `<button onclick='configInventario.verMovimientos( ${ this.inventarios[contador].id }, "${ agencia }" )' class="btnTbl"><i class="fa fa-tools"></i></button>`
                }

                tblInventario.row.add([
                    contador, agencia,
                    badgePlastico, badgePines,
                    '<div class="tblButtonContainer">'+
                    botonConsumir + botonMovimiento + botonTraslado + movimientos+
                    '</div>'
                ]).node().id = `trInventario${contador}`;

                resolve()
            } catch (e) {
                reject( e.message )
            }
        })
    },
    mostrarModal(idFila = 0, entrada = true, consumo = false )
    {
        this.idFila = idFila

        formActions.clean('frmInventario')
            .then( () => {

                this.txtCantidadPlastico.classList.remove('is-invalid', 'is-valid')
                this.txtCantidadPines.classList.remove('is-invalid', 'is-valid')

                let opciones = ['<option selected value="" disabled>seleccione</option>']
                const titulo = document.querySelector('#modalTitulo')

                cboAcciones.disabled = false
                cboAgencias.disabled = false

                let accion = '';

                if ( entrada )
                {
                    accion = 'Entrada'
                    datosAgencias.forEach( agencia => {

                        if ( this.inventarios[this.idFila].Agencia.agencia == 'Oficina Central' )
                        {
                            if ( agencia.id == "1" )
                            {
                                opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`);
                            }
                        } else
                        {
                            if ( agencia.id != this.inventarios[this.idFila].Agencia.id && agencia.id != '1')
                            {
                                opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`);
                            }
                        }
                    })
                    cboAcciones.value = 1
                    cboAcciones.disabled = true

                } else if ( !entrada && consumo )
                {
                    accion = 'Consumo'
                    opciones = []
                    datosAgencias.forEach(  agencia => {
                        if ( agencia.id == this.inventarios[this.idFila].Agencia.id )
                        {
                            opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                        }
                    })
                    cboAcciones.value = 2
                    cboAcciones.disabled = true

                    cboAgencias.value = this.inventarios[this.idFila].Agencia.id
                    cboAgencias.disabled = true
                } else
                {
                    accion = 'Traslado'
                    datosAgencias.forEach(agencia => {

                        if ( this.inventarios[this.idFila].Agencia.agencia == 'Oficina Central' )
                        {
                            let add = false

                            for (const key in this.inventarios )
                            {
                                if( this.inventarios[key].Agencia.id == agencia.id && agencia.id != 700 )
                                {
                                    add = true
                                    break
                                }
                            }

                            if ( add )
                            {
                                opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                            }
                        } else
                        {
                            if (agencia.id != 700 && agencia.id != 1 && agencia.id != this.inventarios[this.idFila].Agencia.id) {
                                opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                            }
                        }
                    })

                    cboAcciones.value = 2;
                    cboAcciones.disabled = true;
                }

                titulo.innerHTML = `Movimiento de inventario de agencia: ${this.inventarios[this.idFila].Agencia.agencia} | ${accion}`

                if (cboAcciones.className.includes('selectpicker') )
                {
                    $(`#${cboAcciones.id}`).selectpicker("refresh")
                }

                if (cboAgencias.className.includes('selectpicker')) {
                    cboAgencias.innerHTML = opciones.join('')
                    $(`#${cboAgencias.id}`).selectpicker("refresh");
                }
            })

        $('#movimientos-Modal').modal('show');
    },
    verMovimientos( idInventario, agencia )
    {
        window.top.location = `?page=controlesMovimientosPYP&mod=${btoa(modulo)}&id=${idInventario}&agencia=${ btoa( agencia ) }`;
    }
}

const configMovimientos = {
    id: 0,
    contador: 0,
    movimientos: {},
    init( idInventario = 0 )
    {
        return new Promise( ( resolve, reject ) => {
            try {
                fetchActions.get( {
                    modulo,
                    archivo: 'PYP/controlesObtenerMovimientos',
                    params: {
                        idInventario,
                        detalle: false
                    }
                }).then( ( { movimientos = [] }) => {
                    this.contador = 0
                    this.movimientos = { }
                    tblMovimientos.clear().draw()

                    movimientos.forEach( movimiento => {
                        this.contador++
                        this.movimientos[ this.contador ] = { ...movimiento  }
                        this.agregarFila( { contador: this.contador, ...movimiento } )
                    })
                    tblMovimientos.columns.adjust().draw()

                    resolve()
                }).catch( reject )
            } catch (e) {
                reject( e.message )
            }
        })
    },
    agregarFila( { contador, Agencia: { agencia }, Accion: { accion }, stockPines, stockPlastico } )
    {
        tblMovimientos.row.add([
            contador, agencia, accion, stockPines, stockPlastico
        ]).node().id = `trMovimiento${contador}`
    }
}

const cbocboAgenciaMovimiento = ( cbo ) => {
    configMovimientos.init( cbo.value ).then( () => {
        $(".preloader").fadeOut("fast")
    }).catch( generalMostrarError )
}

const initDatatble = () => {
    return new Promise ( ( resolve, reject ) => {
        try {
            if ($('#tblLogsInventario').length > 0) {
                tblMovimientos = $('#tblLogsInventario').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": false,
                    "ordering": true,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            'className': 'text-center',
                            'width': '5%'
                        },
                        {
                            "targets": [2, 3],
                            "className": "text-center"
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Logs de inventario';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblMovimientos.columns.adjust().draw();
            }

            if ($('#tblInventario').length > 0) {
                tblInventario = $('#tblInventario').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": false,
                    "ordering": true,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            className: 'text-center',
                            width: '10%'
                        },
                        {
                            targets: [2, 3],
                            className: 'text-center'
                        },
                        {
                            targets: [4],
                            className: 'text-center',
                            width: '20%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Logs de inventario';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblInventario.columns.adjust().draw();
            }

            resolve()

        } catch (e) {
            reject( e.message )
        }
    })
}

const crearBadge = (cantidad) => {

    let respuesta = '';

    switch (true) {
        case cantidad < 10:
            respuesta = 'badge bg-danger bg-sm text-white'
            break;
        case cantidad >= 10 && cantidad < 15:
            respuesta = 'badge bg-warning text-dark';
            break;
        case cantidad >= 20:
            respuesta = 'badge bg-success text-white';
            break;
        default:
            respuesta = 'badge bg-success badge-sm text-white'
            break;
    }
    return respuesta;
}
