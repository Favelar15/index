let tblRemesas, tblAllRemesas;

let contadorRegistroRemesas = 0
let contadorTblRemesas = 0

let isActualizacion = false;

window.onload = () => {
    const gettingCats = initDatable().then(() => {
        return new Promise((resolve, reject) => {
            fetchActions.getCats({
                modulo: "CONTROLES",
                archivo: "controlesGetCats",
                solicitados: ['transacciones', 'remesadores']
            }).then(({
                transacciones,
                remesadores
            }) => {
                $(".preloader").fadeIn("fast");
                const cboEdt = document.querySelectorAll('select.cboEdt');
                const cboTipoTransaccion = document.querySelectorAll('select.cboTipoTransaccion');
                let opciones = [`<option value="" selected disabled>seleccione</option>`];
                transacciones.forEach(transaccion => {
                    opciones.push(`<option value="${transaccion.id}"> ${transaccion.transaccion}</option>`);
                });

                cboTipoTransaccion.forEach(cbo => {
                    cbo.innerHTML = opciones.join('');
                    if (cbo.className.includes('selectpicker')) {
                        $(`#${cbo.id}`).selectpicker('refresh');
                    }
                });

                opciones = [`<option value="" selected disabled >Seleccione</option>`];
                remesadores.forEach(remesador => {
                    opciones.push(`<option value="${remesador.id}">${remesador.remesador}</option>`)
                });

                cboEdt.forEach(cbo => {
                    cbo.innerHTML = opciones.join('');
                    if (cbo.className.includes('selectpicker')) {
                        $(`#${cbo.id}`).selectpicker('refresh');
                    }
                });
                resolve();
            }).catch(reject);
        });
    });

    gettingCats.then(() => {
        configRemesa.init().then(() => {
            $(".preloader").fadeOut('"fast');
        }).catch(generalMostrarError);
    }).catch(generalMostrarError);
}

function initDatable() {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblAllRemesas').length > 0) {

                tblAllRemesas = $('#tblAllRemesas').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "desc"],
                    columnDefs: [{
                            "width": "5%",
                            "targets": 0,
                            "className": "text-center"
                        },
                        {
                            targets: [1],
                            width: '10%',
                            className: 'text-center'
                        },
                        {
                            targets: [2],
                            width: '15%',
                        },
                        {
                            "width": "10%",
                            "targets": [1, 3, 4, 5, 6 ],
                            "className": "text-center"
                        },
                        {
                            "targets": [3],
                            "className": "text-center"
                        },
                        {
                            className: 'text-center',
                            targets: [8],
                            width: '15%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblAllRemesas.columns.adjust().draw();
            }

            if ($('#tblTransaccionRemesas').length > 0) {
                tblRemesas = $('#tblTransaccionRemesas').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": false,
                    "info": false,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [
                        [0, "asc"]
                    ],
                    columnDefs: [{
                            "targets": [0],
                            "orderable": true,
                        },
                        {
                            "targets": [1],
                            "className": "text-start"
                        },
                        {
                            targets: [2, 3, 4],
                            className: 'text-center'
                        },
                        {
                            width: '10%',
                            "targets": [6],
                            "className": "text-start"
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });

                tblRemesas.columns.adjust().draw();
            }
            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const configRemesa = {
    id: 0,
    cnt: 0,
    datos: {},
    tabAllRemesas: document.getElementById('listadoRemesas-tab'),
    tabRemesa: document.getElementById('agregarRemesa-tab'),
    init: function () {
        return new Promise((resolve, reject) => {
            try {
                fetchActions.get({
                    modulo: "CONTROLES",
                    archivo: '/Remesas/controlesObtenerDatosRemesas',
                    params: {}
                }).then(({
                    remesas: tmpDatos
                }) => {
                    tblAllRemesas.clear().draw();
                    this.datos = {};
                    this.cnt = 0;
                    tmpDatos.forEach(remesa => {
                        this.cnt++;
                        this.datos[this.cnt] = {
                            ...remesa
                        };
                        this.addRowTbl({
                            ...remesa,
                            nFila: this.cnt
                        });
                    });
                    tblAllRemesas.columns.adjust().draw();
                    resolve();
                }).catch(reject);
            } catch (err) {
                reject(err.message);
            }
        });
    },
    edit: function (id = 0) {
        if (tipoPermiso == 'ESCRITURA') {
            formActions.clean('agregarRemesa').then(() => {
                this.id = id;
                this.tabRemesa.classList.remove('disabled');
                $("#" + this.tabRemesa.id).trigger("click");
                this.tabAllRemesas.classList.add('disabled');
                configTransaccion.id = 0;
                configTransaccion.cnt = 0;
                configTransaccion.transacciones = {};
            });
        } else {
            toastr.error('Modo sólo lectura, no tienes permisos crear nuevos registros');
        }
    },
    save: function () {
        if (Object.keys(configTransaccion.transacciones).length > 0) {
            let tmpDatos = [];
            for (let key in configTransaccion.transacciones) {
                tmpDatos.push({
                    ...configTransaccion.transacciones[key],
                    logs: []
                });
            }

            this.saveDB([...tmpDatos]).then(() => {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: '¡Bien hecho!',
                    text: 'Los datos fueron almacenados con exito',
                    showConfirmButton: false,
                    timer: 1500
                });
                this.init().then(() => {
                    this.showPrincipal();
                }).catch(generalMostrarError);
            }).catch(generalMostrarError);
        } else {
            Swal.fire({
                title: "¡Atención!",
                html: "Debe ingresar al menos una transacción",
                icon: "info"
            });
        }
    },
    saveDB: function (datos) {
        return new Promise((resolve, reject) => {
            fetchActions.set({
                modulo: 'CONTROLES',
                archivo: '/Remesas/controlesGuardarRemesa',
                datos: {
                    tipoApartado,
                    idApartado,
                    datosRemesas: datos
                }
            }).then(resolve).catch(reject);
        });
    },
    addRowTbl: function ({
        nFila,
        id,
        fecha,
        Transaccion: {
            transaccion
        },
        Remesador: {
            remesador
        },
        numeroDeOperaciones,
        monto,
        Agencia: {
            agencia
        }, usuario,
        propietario
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botonEditar = `<h5><span class="badge badge-primary">Modo solo lectura</span></h5>`;
                let botonEliminar = '';

                if (tipoPermiso == 'ESCRITURA' && propietario == 'S') {
                    botonEditar = `<button type="button" class="btnTbl" onclick='configTransaccion.edit({id:${nFila},storage:\"DB\"})'> <i class="fa fa-edit"></i></button>`;
                    botonEliminar = `<button type="button" class="btnTbl" onclick="configRemesa.delete(${nFila})"> <i class="fa fa-trash"></i></button>`;
                } else if (tipoPermiso == 'ESCRITURA') {
                    botonEditar = `<button type="button" class="btnTbl" onclick='configTransaccion.edit({id:${nFila},storage:\"DB\"})'> <i class="fa fa-edit"></i></button>`;
                }

                tblAllRemesas.row.add([
                    nFila,
                    fecha,
                    agencia,
                    transaccion,
                    remesador,
                    numeroDeOperaciones,
                    "$" + monto,
                    usuario,
                    '<div class="tblButtonContainer">' +
                    botonEditar + botonEliminar + '</div>'
                ]).node().id = `tr${nFila}`;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    cancel: function () {
        Swal.fire({
            title: '¡iNFORMACION!',
            text: "¿Estas seguro de cancelar el proceso?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Si, quiero cancelar!'
        }).then((result) => {
            if (result.isConfirmed) {
                this.showPrincipal();
            }
        })
    },
    delete: function (tmpId = 0) {
        Swal.fire({
            title: 'IMPORTANTE',
            text: '¿Estás seguro que quieres eliminar este registro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#46b1c3',
            cancelButtonColor: '#f54343',
            confirmButtonText: 'Eliminar'
        }).then((result) => {
            if (result.isConfirmed) {
                const id = this.datos[tmpId].id;
                fetchActions.set({
                    modulo: 'CONTROLES',
                    archivo: '/Remesas/controlesEliminarRemesa',
                    datos: {
                        id,
                        idApartado,
                        tipoApartado
                    }
                }).then((response) => {
                    switch (response.respuesta.trim()) {
                        case 'EXITO':
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'Remesa eliminada con exito',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            tblAllRemesas.row(`#tr${tmpId}`).remove().draw(false);
                            delete this.datos[tmpId];
                            break;
                        default:
                            generalMostrarError(response);
                            break;
                    }
                }).catch(generalMostrarError);
            }
        });
    },
    showPrincipal: function () {
        tblRemesas.clear().draw()
        configTransaccion.transacciones = {}
        configRemesa.tabAllRemesas.classList.remove('disabled');
        $("#" + this.tabAllRemesas.id).trigger("click");
        configRemesa.tabRemesa.classList.add('disabled');
    }
}

const configTransaccion = {
    id: 0,
    cnt: 0,
    transacciones: {},
    cboTipoTransaccion: document.getElementById('cboTipoTransaccion'),
    cboRemesador: document.getElementById('cboEdt'),
    campoNumeroOperaciones: document.getElementById('txtNumeroOperaciones'),
    campoMonto: document.getElementById('txtMonto'),
    campoFecha: document.getElementById('txtFecha'),
    storage: 'Local',
    edit: function ({
        id = 0,
        storage = 'Local'
    }) {
        formActions.clean('frmAddDetalleRemesa').then(() => {
            this.id = id;
            this.storage = storage;

            if (this.id > 0) {
                let tmp = storage == 'Local' ? {
                    ...this.transacciones[this.id]
                } : configRemesa.datos[this.id];
                this.cboTipoTransaccion.value = tmp.Transaccion.id;
                this.cboRemesador.value = tmp.Remesador.id;
                this.campoNumeroOperaciones.value = tmp.numeroDeOperaciones;
                this.campoMonto.value = tmp.monto;
                this.campoFecha.value = tmp.fecha;
                $("#" + this.campoFecha.id).datepicker("update", tmp.fecha);

                let campos = document.querySelectorAll("#frmAddDetalleRemesa .form-control");

                campos = [...campos, ...document.querySelectorAll("#frmAddDetalleRemesa .form-select")];

                campos.forEach(campo => {
                    if (campo.id) {
                        if (campo.type == 'select-one') {
                            $("#" + campo.id).data("valorInicial", campo.options[campo.options.selectedIndex].text);
                        } else {
                            $("#" + campo.id).data("valorInicial", campo.value);
                        }
                    }
                });
            }

            let allSelectpickerCbos = document.querySelectorAll('#frmAddDetalleRemesa select.selectpicker');
            allSelectpickerCbos.forEach(cbo => $("#" + cbo.id).selectpicker("refresh"));

            $("#addTransaccionRemesas").modal("show");
        });
    },
    save: function () {
        formActions.validate('frmAddDetalleRemesa').then(({
            errores
        }) => {
            if (errores == 0) {
                let tmp = {
                    id: this.storage == 'Local' ? (this.id > 0 ? (this.transacciones[this.id] ? this.transacciones[this.id].id : 0) : 0) : configRemesa.datos[this.id].id,
                    fecha: this.campoFecha.value,
                    Transaccion: {
                        id: this.cboTipoTransaccion.value,
                        transaccion: this.cboTipoTransaccion.options[this.cboTipoTransaccion.options.selectedIndex].text
                    },
                    Remesador: {
                        id: this.cboRemesador.value,
                        remesador: this.cboRemesador.options[this.cboRemesador.options.selectedIndex].text
                    },
                    numeroDeOperaciones: this.campoNumeroOperaciones.value,
                    monto: parseFloat(this.campoMonto.value).toFixed(2),
                };

                let guardar = new Promise((resolve, reject) => {
                    if (this.storage == 'DB') {
                        configRemesa.saveDB([{
                            ...tmp,
                            logs: obtenerLogsDeRemesa()
                        }]).then(() => {
                            $("#tr" + this.id).find("td:eq(1)").html(tmp.fecha);
                            $("#tr" + this.id).find("td:eq(3)").html(tmp.Transaccion.transaccion);
                            $("#tr" + this.id).find("td:eq(4)").html(tmp.Remesador.remesador);
                            $("#tr" + this.id).find("td:eq(5)").html(tmp.numeroDeOperaciones);
                            $("#tr" + this.id).find("td:eq(6)").html("$" + tmp.monto);
                            configRemesa.datos[this.id] = {
                                ...configRemesa.datos[this.id],
                                ...tmp
                            };
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡Bien hecho!',
                                text: 'Los datos fueron almacenados con exito',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            resolve();
                        }).catch(reject);
                    } else {
                        if (this.id == 0) {
                            this.cnt++;
                            this.id = this.cnt;
                            this.addRowTbl({
                                ...tmp,
                                nFila: this.cnt
                            }).then(() => {
                                tblRemesas.columns.adjust().draw();
                                resolve();
                            }).catch(reject);
                        } else {
                            $("#trTransaccion" + this.id).find("td:eq(1)").html(tmp.fecha);
                            $("#trTransaccion" + this.id).find("td:eq(2)").html(tmp.Transaccion.transaccion);
                            $("#trTransaccion" + this.id).find("td:eq(3)").html(tmp.Remesador.remesador);
                            $("#trTransaccion" + this.id).find("td:eq(4)").html(tmp.numeroDeOperaciones);
                            $("#trTransaccion" + this.id).find("td:eq(5)").html("$" + tmp.monto);
                            resolve();
                        }

                        this.transacciones[this.id] = {
                            ...tmp
                        };
                    }
                });

                guardar.then(() => {
                    $("#addTransaccionRemesas").modal("hide");
                });
            }
        }).catch(generalMostrarError);
    },
    addRowTbl: function ({
        nFila,
        fecha,
        Transaccion: {
            transaccion
        },
        Remesador: {
            remesador
        },
        numeroDeOperaciones,
        monto,
    }) {
        return new Promise((resolve, reject) => {
            try {
                let botonEditar = '',
                    botonEliminar = '';

                if (tipoPermiso == 'ESCRITURA') {
                    botonEditar = `<button type="button" onclick="configTransaccion.edit({id:${nFila},storage:'Local'})" class="btnTbl"><i class="fa fa-edit"></i></button>`
                    botonEliminar = `<button type="button" onclick="configTransaccion.delete(${nFila})" class="btnTbl"><i class="fa fa-trash"></i></button>`;
                }
                tblRemesas.row.add([
                    nFila,
                    fecha,
                    transaccion,
                    remesador,
                    numeroDeOperaciones,
                    `$ ${monto}`,
                    '<div class="tblButtonContainer">' +
                    botonEditar + botonEliminar +
                    '</div>'
                ]).node().id = `trTransaccion${nFila}`;
                resolve();
            } catch (err) {
                reject(err.message);
            }
        });
    },
    delete: function (id = 0) {
        tblRemesas.row(`#trTransaccion${id}`).remove().draw(false);
        delete this.transacciones[id];
    },
}

const obtenerLogsDeRemesa = () => {

    let camposEditados = [];
    let campos = document.querySelectorAll('#frmAddDetalleRemesa .form-control');
    campos = [...campos, ...document.querySelectorAll("#frmAddDetalleRemesa .form-select")];

    campos.forEach(campo => {
        if (campo.id) {
            let valorActual = campo.type == 'select-one' ? campo.options[campo.selectedIndex].text : $("#" + campo.id).val();

            if ($("#" + campo.id).data("valorInicial")) {
                let valorAnterior = $("#" + campo.id).data("valorInicial");

                if (valorActual != valorAnterior) {
                    let label = $("#" + campo.id).parent().find("label").length > 0 ? $("#" + campo.id).parent().find("label:eq(0)").text() : ($("#" + campo.id).parent().parent().find("label").length > 0 ? $("#" + campo.id).parent().parent().find("label:eq(0)").text() : 'undefined');
                    label = (label.replace("*", "")).trim();
                    camposEditados.push({
                        campo: label,
                        valorAnterior,
                        valorNuevo: valorActual
                    })
                }
            }
        }
    });

    return camposEditados;
}