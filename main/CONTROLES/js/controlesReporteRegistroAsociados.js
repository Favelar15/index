
let tblRegistroAsociados
const modulo = 'CONTROLES'

window.onload = () => {

    document.getElementById('formRegistroAsociados').reset()

    const cboAgencia = document.querySelectorAll('select.cboAgencia')

    const inicializando = initDataTable().then( () => {

        return new Promise( (resolve, reject ) => {
            fetchActions.getCats({
                modulo, archivo: 'controlesGetCats', solicitados: ['agenciasAsignadas']
            }).then(({agenciasAsignadas = []}) => {

                let opciones = [`<option value="all" selected>Todas las agencias</option>`]

                agenciasAsignadas.forEach(agencia => {
                    if (agencia.id != 700) {
                        opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                    }
                })

                cboAgencia.forEach(cbo => {
                    cbo.innerHTML = opciones.join()
                    $(`#${cbo.id}`).selectpicker('refresh')
                })

                resolve()
            }).catch( reject )
        }).then( () => {
            $(`.preloader`).fadeOut('fast')
        })

    })

    inicializando.then()
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblRegistroAsociados').length > 0) {
                tblRegistroAsociados = $('#tblRegistroAsociados').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [
                        {
                            targets: [0],
                            orderable: true,
                            width: '8%'
                        },
                        {
                            targets: [1, 2, 3, 4,5],
                            className: "text-start"
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de la solicitud';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblRegistroAsociados.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const asociado = {
    contador: 0,
    registros: {},
    reingresos : {},
    cboAgencia: document.getElementById('cboAgencia'),
    //cboTipoReporte : document.getElementById('cboTipoReporte'),
    txtInicio: document.getElementById('txtInicio'),
    txtFin: document.getElementById('txtFin'),
    btnExcel: document.getElementById('btnExcel'),
    obtenerRegistros() {

        const {
            cboAgencia: { value: idAgencia },
            txtInicio, txtFin
        } = formActions.getJSON('formRegistroAsociados')

        fetchActions.set({
            modulo, archivo: 'controlesReporteRegistroAsociados', datos: {
                idAgencia, txtInicio, txtFin
            }
        }).then( ( { registros = [] } ) => {

            registros = Object.keys( registros).map((key) => registros[key]);

            //console.log( registros )

            this.contador = 0
            this.registros = {}
            tblRegistroAsociados.clear()

            if( registros.length > 0 )
            {
                this.cboAgencia.disabled = true
                $(`#${this.cboAgencia.id}`).selectpicker('refresh')

                // this.cboTipoReporte.disabled = true
                // $(`#${this.cboTipoReporte.id}`).selectpicker('refresh')
                this.txtInicio.disabled = true
                this.txtFin.disabled = true
                this.btnExcel.classList.remove('disabled')

                registros.forEach( registro => {

                    this.contador++
                    this.registros[this.contador] = { ...registro }
                    this.agregar({
                        contador: this.contador, ...registro
                    })

                    //this.registros[this.contador] = {...registro }

                    // if (registro.reingreso == tipoReporte) {
                    //     this.contador++
                    //
                    //     if (registro.reingreso == 'NUEVO') {
                    //         registro.fechaActualizacion = ''
                    //         this.registros[this.contador] = {...registro}
                    //     } else {
                    //         this.reingresos[this.contador] = {...registro}
                    //     }
                    // }
                })

                // let registrosTemp = tipoReporte == 'NUEVO' ? {...this.registros} : {...this.reingresos}
                //
                // for ( const key in registrosTemp )
                // {
                //     this.agregar({
                //         contador: key,
                //         ...registrosTemp[key]
                //     })
                // }

            } else {
                toastr.warning('No hay registros para exportar')
            }

            tblRegistroAsociados.columns.adjust().draw()

        }).catch( generalMostrarError )
    },
    exportarExcel()
    {
        if( Object.keys(this.registros).length > 0 )
        {
            const {
                cboAgencia,
                txtInicio, txtFin
            } = formActions.getJSON('formRegistroAsociados')

            fetchActions.set({
                modulo, archivo: 'controlesExcelAsociados', datos: {
                    registros: this.registros,
                    txtInicio, txtFin, cboAgencia
                }
            }).then( response => {
                switch (response.respuesta.trim()) {
                    case 'EXITO':
                        window.open('./main/CONTROLES/docs/registrosAsociados/Excel/' + response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }
            }).catch( generalMostrarError )
        } else {
            toastr.warning('No hay registros para exportar')
        }
    },
    agregar( { contador, fechaAfiliacion, codigoCliente, nombresAsociado, agencia, genero, hojaLegal, tipoAfiliacion } )
    {
        tblRegistroAsociados.row.add([
            contador, fechaAfiliacion, codigoCliente, nombresAsociado, agencia, genero, hojaLegal, tipoAfiliacion
        ]).node().id = `trRegsitro${contador}`
    },
    limpiar()
    {
        formActions.clean('formRegistroAsociados')
            .then( () => {

                this.contador=0
                this.registros = {}
                this.reingresos = {}

                tblRegistroAsociados.clear().draw()

                this.cboAgencia.disabled = false
                $(`#${this.cboAgencia.id}`).selectpicker('refresh')
                // this.cboTipoReporte.disabled = false
                // $(`#${this.cboTipoReporte.id}`).selectpicker('refresh')
                this.txtInicio.disabled = false
                this.txtFin.disabled = false
                this.btnExcel.classList.add('disabled')

            })
    },
    cambioFecha()
    {
        $("#txtFin").datepicker('destroy')

        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        this.txtFin.value = ''
        $("#txtFin").datepicker('update')
    }
}

