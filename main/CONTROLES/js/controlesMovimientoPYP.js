let tblMovimientos

let contadorMovimientos = 0 , datosMovimientos = [  ]
let idFila = 0
let datosInventario = { }

let txtPines, txtPlastico
const modulo = 'CONTROLES'

window.onload = () => {

    let idInventario

    const titulo = document.getElementById('title')
    txtPines = document.getElementById('txtCantidadPines')
    txtPlastico = document.getElementById('txtCantidadPlastico')

    initDatable().then( () => {
        return new Promise( ( resolve, reject ) => {
          try {
              idInventario = getParameterByName('id')
              const agencia = atob( getParameterByName('agencia') )

              if ( idInventario )
              {
                  titulo.innerHTML = `<strong>Movimientos de inventario | ${ agencia }</strong>`

                  configMovimientos.init(idInventario).then( () => {
                      $('.preloader').fadeOut('fast')
                  })
              }
              resolve()
          }  catch (e)
          {
              reject(generalMostrarError())
          }
        })
        $('.preloader').fadeOut('fast')
    })
}

const initDatable = () => {
    return new Promise (( resolve, reject) => {
        try {
            if ($('#tblMovimientos').length > 0) {
                tblMovimientos = $('#tblMovimientos').DataTable({
                    "paging": false,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": false,
                    "ordering": true,
                    dateFormat: 'uk',
                    order: [0, "asc"],
                    columnDefs: [
                        {
                            "targets": [0],
                            "orderable": true,
                            'className': 'text-center',
                            'width': '5%'
                        },
                        {
                            "targets": [1, 2, 3, 4, 5 ],
                            "className": "text-center"
                        },
                        {
                            targets: [ 6 ],
                            className: 'text-center',
                            width: '10%'
                        }
                    ],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Logs de inventario';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblMovimientos.columns.adjust().draw();
            }

            resolve()
        } catch (e) {
            reject( e.message )
        }
    })
}

const configMovimientos = {
    contador: 0,
    idFila: 0,
    movimientos: {},
    inventario: {},

    init(idInventario)
    {
        return new Promise( ( resolve, reject ) => {
            try {
                fetchActions.get({
                    modulo,
                    archivo: 'PYP/controlesObtenerMovimientos',
                    params: {
                        idInventario,
                        detalle: true
                    }
                }).then( ( { movimientos = [] , inventario } ) => {

                    this.inventario = { ...inventario }

                    movimientos.forEach( movimiento => {
                        this.contador++
                        this.movimientos[ this.contador ] = { ...movimiento }
                        this.agregar( { contador: this.contador, ... movimiento })
                    })
                    tblMovimientos.columns.adjust().draw();
                    resolve()
                }).catch( reject )
            } catch (e) {
                reject( e.message )
            }
        })
    },
    mostrarModal ( idFila )
    {
        formActions.clean('frmEditarMovimiento')
            .then( () => {
                this.idFila = idFila
                const tituloModal = document.getElementById('titleModal')

                const {
                    fechaRegistro,
                    Accion: { accion },
                    stockPines, stockPlastico
                } = this.movimientos[this.idFila]

                tituloModal.innerText = `Movimiento de inventario | ${ accion } | ${ fechaRegistro }`

                txtPines. value = stockPines
                txtPlastico.value = stockPlastico

                gestionLogs.inicializarLogs('frmEditarMovimiento')

                $('#movimientosEdit-Modal').modal('show')
            })
    },
    guardar()
    {
        formActions.validate('frmEditarMovimiento')
            .then( ({ errores }) => {

                if ( errores == 0 )
                {
                    const form = document.querySelector('form#frmEditarMovimiento')

                    let nuevoStockPines = 0, nuevoStockPlastico = 0

                    const {
                        txtCantidadPines,
                        txtCantidadPlastico
                    } = formActions.getJSON('frmEditarMovimiento')

                    const valorInicialPlastico = parseInt( $('#txtCantidadPlastico').data("valorInicial") )
                    const valorInicialPines = parseInt( $('#txtCantidadPines').data("valorInicial") )

                    form.classList.remove('was-validated')
                    const inputPines = document.getElementById('txtCantidadPines')
                    const inputPlasticos = document.getElementById('txtCantidadPlastico')


                    if ( this.movimientos[this.idFila].Accion.id == 1 )
                    {
                        nuevoStockPines = parseInt(this.inventario.stockPines) - parseInt( valorInicialPines )
                        nuevoStockPlastico = parseInt(this.inventario.stockPlastico) - parseInt( valorInicialPlastico )


                        if ( nuevoStockPines + parseInt(txtCantidadPines.value) < 0 )
                        {
                            errores++
                            inputPines.classList.remove('is-valid')
                            inputPines.classList.add('is-invalid')
                        } else
                        {
                            nuevoStockPines += parseInt(txtCantidadPines.value )
                        }

                        if (  nuevoStockPlastico + parseInt(txtCantidadPlastico.value)  < 0 )
                        {
                            errores++
                            inputPlasticos.classList.remove('is-valid')
                            inputPlasticos.classList.add('is-invalid')
                        } else
                        {
                            nuevoStockPlastico += parseInt( txtCantidadPlastico.value )
                        }
                    }

                    else if ( this.movimientos[this.idFila].Accion.id == 2 )
                    {
                        nuevoStockPines = parseInt( valorInicialPines) + parseInt(this.inventario.stockPines )
                        nuevoStockPlastico = parseInt( valorInicialPlastico) + parseInt(this.inventario.stockPlastico)

                        //console.log( nuevoStockPines - parseInt(txtCantidadPines.value))

                        if ( nuevoStockPines - parseInt(txtCantidadPines.value) < 0 )
                        {
                            errores++
                            inputPines.classList.remove('is-valid')
                            inputPines.classList.add('is-invalid')
                        } else
                        {
                            nuevoStockPines -= parseInt(txtCantidadPines.value )
                            inputPines.classList.remove('is-invalid')
                            inputPines.classList.add('is-valid')
                        }

                        if (  nuevoStockPlastico - parseInt(txtCantidadPlastico.value)  < 0 )
                        {
                            errores++
                            inputPlasticos.classList.remove('is-valid')
                            inputPlasticos.classList.add('is-invalid')
                        } else
                        {
                            nuevoStockPlastico -= parseInt( txtCantidadPlastico.value )
                            inputPlasticos.classList.remove('is-invalid')
                            inputPlasticos.classList.add('is-valid')
                        }
                    }

                    // console.log('hasta aqui')
                    // return
                    if ( errores == 0 )
                    {
                        const datos = {
                            idApartado,
                            tipoApartado,
                            id: this.movimientos[this.idFila].id,
                            idInventario: this.inventario.id,
                            cboAcciones: { value : this.movimientos[this.idFila].Accion.id },
                            cboAgencia: { value : this.movimientos[this.idFila].Agencia.id },
                            txtCantidadPines,
                            txtCantidadPlastico,
                            nuevoStockPines,
                            nuevoStockPlastico,
                            logs: gestionLogs.obtenerLogs('frmEditarMovimiento')
                        }

                        fetchActions.set({
                            modulo,
                            archivo: 'PYP/controlesGuardarInventario',
                            datos
                        }).then( response => {

                            $('.preloader').fadeOut('fast');
                            $(".preloader").find("span").html("");

                            switch (response.respuesta.trim()) {
                                case 'EXITO':
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: '¡Bien hecho!',
                                        text: 'Movimiento actualizado con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then(i => {
                                        window.location = `?page=controlesPlasticoYPines&mod=${ btoa(modulo) }`
                                    });
                                    break
                                default :
                                    generalMostrarError( response )
                                    break
                            }
                        }).catch( generalMostrarError )
                    }
                }
            }).catch( generalMostrarError )
    },
    agregar( { contador, Accion: { accion }, Agencia: { agencia }, stockPines, stockPlastico, fechaRegistro} ){
        let botonEditar = ''
        if ( tipoPermiso == 'ESCRITURA' )
        {
            botonEditar = `<button type="button" class="btnTbl" onclick="configMovimientos.mostrarModal( ${contador} )"> <i class="fa fa-edit"></i> </button>`
        }

        tblMovimientos.row.add([
            contador, accion, agencia, stockPines, stockPlastico, fechaRegistro,
            `<div class="tblButtonContainer">
                ${botonEditar}
            </div>`
        ]).node().id = `trMovimiento${contador}`
    },
    salir()
    {
        window.top.location = `?page=controlesPlasticoYPines&mod=${btoa(modulo)}`;
    }
}


const gestionLogs = {
    inicializarLogs( idContenedor )
    {
        const inputs = document.querySelectorAll(`#${idContenedor} .form-control`)

        inputs.forEach( campo => {
            if (campo.id)
            {
                if (campo.type == 'select-one') {
                    $("#" + campo.id).data("valorInicial", campo.options[campo.selectedIndex].text);
                } else {
                    $("#" + campo.id).data("valorInicial", campo.value);
                }
            }
        })
    },
    obtenerLogs( idContenedor ) {

        let camposEditados = [];
        const campos = document.querySelectorAll(`#${idContenedor} .form-control`);

        campos.forEach(campo => {
            if (campo.id) {
                let valorActual = campo.type == 'select-one' ? campo.options[campo.selectedIndex].text : $("#" + campo.id).val();

                if ($("#" + campo.id).data("valorInicial")) {
                    let valorAnterior = $("#" + campo.id).data("valorInicial");

                    if (valorActual != valorAnterior) {
                        let label = $("#" + campo.id).parent().find("label").length > 0 ? $("#" + campo.id).parent().find("label:eq(0)").text() : ($("#" + campo.id).parent().parent().parent().find("label").length > 0 ? $("#" + campo.id).parent().parent().parent().find("label:eq(0)").text() : 'undefined');
                        label = (label.replace("*", "")).trim();
                        camposEditados.push({campo: label, valorAnterior, valorNuevo: valorActual})
                    }
                }
            }
        });

        return camposEditados;
    }
}

// const servicios = {
//     permisos: {
//         idApartado,
//         tipoApartado,
//         tipoPermiso: localStorage.getItem('tipoPermiso')
//     },
//     path: `./main/CONTROLES/code/PYP`,
//     obtenerMovimientos (id) {
//         return new Promise( (resolve, reject) => {
//             (async () => {
//                 try
//                 {
//                     const headers = {
//                         method  : 'GET',
//                         headers : {
//                             'Content-type' : 'application/json'
//                         }
//                     }
//                     const response = await fetch(`${this.path}/controlesObtenerMovimientos.php?idInventario=${id}&detalle=true`, headers)
//                     if ( response.ok )
//                     {
//                         resolve( await response.json() )
//                     } else
//                     {
//                         throw new Error( response.statusText )
//                     }
//                 } catch ( error )
//                 {
//                     console.error( error.message )
//                     reject('Error en la promesa obtener movimientos de inventario')
//                 }
//             })();
//         });
//     },
//     actualizarMovimiento(datos) {
//
//         $(".preloader").find("span").html("Procesando...");
//         if ($(".preloader").is(":visible") == false) {
//             $(".preloader").fadeIn("fast");
//         }
//
//         return new Promise((resolve, reject) => {
//             (async () => {
//                 try {
//                     const headers = {
//                         method: 'POST',
//                         headers: {
//                             'Content-type': 'application/json'
//                         },
//                         body: JSON.stringify({ ...datos, ...this.permisos } )
//                     }
//                     const response = await fetch(`${this.path}/controlesGuardarInventario.php`, headers);
//                     if (response.ok) {
//                         resolve(await response.json())
//                     } else {
//                         console.error(error.message);
//                         throw new Error(response.statusText)
//                     }
//                 } catch (e) {
//                     console.error(e.message)
//                     reject('Error en la promesa de guardar transaccion de inventario');
//                 }
//             })();
//         })
//     },
// }
