const modulo = 'CONTROLES'
let tblActivaciones

window.onload = () => {

    const cboAgencia = document.getElementById('cboAgencia')


    const inicializando = initDataTable().then( () => {
        return new Promise( ( resolve, reject ) => {
            fetchActions.getCats({
                modulo, archivo: 'controlesGetCats', solicitados: ['agenciasAsignadas']
            }).then( ( { agenciasAsignadas = [] } ) => {

                let opciones = [`<option value="all" selected>Todas las agencias</option>`]

                agenciasAsignadas.forEach( agencia => {
                    if (agencia.id != 700) {
                        opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                    }
                })

                cboAgencia.innerHTML = opciones.join()
                $(`#${cboAgencia.id}`).selectpicker('refresh')

                resolve()

            }).catch( reject )
        })
    })

    inicializando.then( () => {
        $('.preloader').fadeOut('fast')
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {

            if ($('#tblActivaciones').length > 0) {
                tblActivaciones = $('#tblActivaciones').DataTable({
                    "paging": true,
                    "bAutoWidth": false,
                    "searching": true,
                    "info": true,
                    "ordering": false,
                    dateFormat: 'uk',
                    order: [1, "asc"],
                    columnDefs: [{
                        "targets": [0],
                        "orderable": true,
                        width: '10%'
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de colaborador';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblActivaciones.columns.adjust().draw();
            }
            resolve()
        } catch (e) {
            reject(e.message)
        }
    })
}

const configReporteActivacion = {
    contador: 0,
    activaciones: {},
    btnExportar: document.getElementById('btnExcel'),
    txtInicio: document.getElementById('txtInicio'),
    obtenerActivaciones ()
    {
        formActions.validate('formActivacion')
            .then( ( { errores  } ) => {

                if( errores == 0)
                {
                    fetchActions.set({
                        modulo, archivo: 'ActivacionDeCuentas/controlesReporteActivacion', datos: {
                            ...formActions.getJSON('formActivacion'),
                            accion: 'GENERAR'
                        }
                    }).then( ( { activaciones = [] } ) => {

                        tblActivaciones.clear()
                        this.contador = 0
                        this.activaciones = {}

                        if ( activaciones.length > 0 ) {
                            this.resetearFormulario(true)

                            activaciones.forEach(activacion => {

                                this.contador++
                                this.activaciones[this.contador] = {...activacion}
                                this.agregarFila({
                                    contador: this.contador, ...activacion
                                })
                            })
                        }
                         else {
                            toastr.warning(`No hay registros para mostrar`)
                        }

                         tblActivaciones.columns.adjust().draw()

                    }).catch( generalMostrarError )
                }
            })
    },
    exportarExcel()
    {
        if ( Object.keys( this.activaciones ).length > 0  )
        {
            fetchActions.set({
                modulo, archivo: 'ActivacionDeCuentas/controlesReporteActivacion', datos: {
                    ...formActions.getJSON('formActivacion'),
                    activaciones: {...this.activaciones },
                    accion: 'EXPORTAR'
                }
            }).then( response => {

                switch (response.respuesta.trim()) {
                    case 'EXITO':
                        window.open('./main/CONTROLES/docs/solicitudesActivacion/Excel/' + response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }

            }).catch( generalMostrarError )
        } else {
            toastr.warning(`No hay registros para exportar`)
        }
    },
    agregarFila( { contador, codigoCliente, nombresAsociado, agencia, fechaRegistro, usuario } )
    {
        tblActivaciones.row.add([
            contador, codigoCliente, nombresAsociado, agencia, fechaRegistro, usuario
        ]).node().id = `trActivaciones${contador}`
    },
    limpiar()
    {
        formActions.clean('formActivacion')
            .then( () => {

                this.resetearFormulario( false )
                this.contador = 0
                this.activaciones = {}
                tblActivaciones.clear().draw()
            })
    },
    resetearFormulario(estado) {
        let inputs = document.querySelectorAll('form#formActivacion .form-control')

        if (estado)
        {
            this.btnExportar.classList.remove('disabled')
        }  else {
            this.btnExportar.classList.add('disabled')
        }

        inputs.forEach( input => {
            input.disabled = estado

            if( input.className.includes('selectpicker'))
            {
                $(`#${input.id}`).selectpicker('refresh')
            }
        })
    },
    cambiarFecha()
    {
        $("#txtFin").datepicker('destroy')

        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

       document.getElementById('txtFin').value = ''
        $("#txtFin").datepicker('update')
    }
}