let tblReporteRemesas

const modulo = 'CONTROLES'

window.onload = () => {

    document.getElementById('formRemesas').reset()

    const cboAgencia = document.querySelectorAll('select.cboAgencia')

    const inicializando = initDataTable().then( () => {
        return new Promise( (resolve, reject) => {

            fetchActions.getCats({
                modulo, archivo: 'controlesGetCats', solicitados: ['agenciasAsignadas']
            }).then(({agenciasAsignadas = []}) => {

                let opciones = [`<option value="all" selected>Todas las agencias</option>`]

                agenciasAsignadas.forEach(agencia => {
                    if (agencia.id != 700) {
                        opciones.push(`<option value="${agencia.id}">${agencia.nombreAgencia}</option>`)
                    }
                })

                cboAgencia.forEach(cbo => {
                    cbo.innerHTML = opciones.join()
                    $(`#${cbo.id}`).selectpicker('refresh')
                })

                resolve()
            }).catch( reject )
        })
    })

    inicializando.then( () => {
        $('.preloader').fadeOut('fast')
    })
}

const initDataTable = () => {
    return new Promise((resolve, reject) => {
        try {
            if ($('#tblDatosRemesas').length > 0) {
                tblReporteRemesas = $('#tblDatosRemesas').DataTable({
                    drawCallBack: function () {
                        $("[data-toggle='tooltip']").tooltip("dispose");
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    dateFormat: 'uk',
                    scrollY: true,
                    order: [0, 'asc'],
                    columnDefs: [{
                        targets: [0],
                        orderable: true,
                    }, {
                        targets: [1, 2, 3, 4],
                        className: "text-start"
                    }],
                    sortable: true,
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.modal({
                                header: function (row) {
                                    var data = row.data();
                                    return 'Datos de la solicitud';
                                }
                            }),
                            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                                tableClass: 'table'
                            })
                        }
                    }
                });
                tblReporteRemesas.columns.adjust().draw();
            }

            resolve();
        } catch (err) {
            reject(err.message);
        }
    });
}

const reporteRemesas = {
    contador: 0,
    remesas: {},
    cboAgencia: document.getElementById('cboAgencia'),
    txtInicio: document.getElementById('txtInicio'),
    txtFin: document.getElementById('txtFin'),
    btnExcel: document.getElementById('btnExcel'),
    obtenerRemesas()
    {
        formActions.validate('formRemesas')
            .then( ( { errores } ) => {

                if( errores ==  0)
                {
                    fetchActions.set({
                        modulo, archivo: 'Remesas/controlesReporteRemesas', datos: {
                            ...formActions.getJSON('formRemesas'),
                            accion: 'GENERAR'
                        }
                    }).then( ( { remesas = [] } ) => {

                        tblReporteRemesas.clear()
                        this.contador = 0
                        this.remesas = {}

                        if ( remesas.length > 0 )
                        {
                            this.cboAgencia.disabled = true
                            $(`#${this.cboAgencia.id}`).selectpicker('refresh')

                            this.txtInicio.disabled = true
                            this.txtFin.disabled = true
                            this.btnExcel.classList.remove('disabled')

                            remesas.forEach( remesa => {
                                this.contador++
                                this.remesas[ this.contador ] = { ...remesa }
                                this.agregarFila( {
                                    contador: this.contador,  ...remesa
                                })
                            })
                        } else {
                            toastr.warning('No hay registros para mostrar')
                        }

                        tblReporteRemesas.columns.adjust().draw()

                    }).catch( generalMostrarError )
                }
            })
    },
    exportarRemesas()
    {
        if ( Object.keys( this.remesas).length > 0 )
        {
            fetchActions.set({
                modulo, archivo: 'Remesas/controlesReporteRemesas', datos: {
                    remesas: {...this.remesas },
                    ...formActions.getJSON('formRemesas'),
                    accion: 'EXPORTAR'
                }
            }).then( response => {

                switch (response.respuesta.trim()) {
                    case 'EXITO':
                        window.open('./main/CONTROLES/docs/remesas/Excel/' + response.nombreArchivo, '_blank');
                        break;
                    default:
                        generalMostrarError(response);
                        break;
                }

            }).catch( generalMostrarError )
        } else
        {
            toastr.warning('No hay registros para exportar')
        }
    },
    limpiar()
    {
        formActions.clean('formRemesas')
            .then( () => {

                this.contador = 0
                this.remesas = {}
                tblReporteRemesas.clear().draw()

                this.cboAgencia.disabled = false
                $(`#${this.cboAgencia.id}`).selectpicker('refresh')

                this.txtInicio.disabled = false
                this.txtFin.disabled = false
                this.btnExcel.classList.add('disabled')
            })
    },
    agregarFila( { contador, fecha, Transaccion: { transaccion }, Remesador: { remesador }, numeroDeOperaciones, monto, Agencia:{ agencia }, usuario } )
    {
        tblReporteRemesas.row.add([
            contador, fecha, transaccion, remesador, numeroDeOperaciones, `$ ${monto}`, agencia, usuario
        ]).node().id = `trReporteRemesa${contador}`
    },
    cambiarFecha()
    {
        $("#txtFin").datepicker('destroy')

        $("#txtFin").datepicker({
            format: "dd-mm-yyyy",
            todayHighlight: true,
            autoclose: true,
            startDate: this.txtInicio.value ? this.txtInicio.value : false,
            endDate: 'now'
        });

        this.txtFin.value = ''
        $("#txtFin").datepicker('update')
    }
}