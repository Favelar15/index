<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1 id="title">Activación de cuentas</h1>
        </div>
        <div class="col-4 topBtnContainer">
            <button class="btn btn-sm btn-outline-dark float-end ms-2" type="button" title="Cancelar" onclick="configMovimientos.salir()">
                <i class="fas fa-times"></i> <span>Cancelar</span>
            </button>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item">Plásticos y pines</li>
            <li class="breadcrumb-item active">Movimientos</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="movimientos-tab" data-toggle="tab" href="#movimientos" role="tab"
                    aria-controls="Movimiento de inventario" aria-selected="true">
                Movimiento de inventario
            </button>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active " id="movimientos" role="tabpanel" aria-labelledby="evaluacion-tab">
            <table class="table table-bordered table-striped" id="tblMovimientos">
                <thead>
                <th>N°</th>
                <th>Acción</th>
                <th>Agencia destino</th>
                <th>Cantidad pines</th>
                <th>Cantidad plásticos</th>
                <th>Registro</th>
                <th>Acciones</th>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

</section>

<div class="modal fade" id="movimientosEdit-Modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titleModal">Movimientos de inventario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="frmEditarMovimiento" name="frmEditarMovimiento" accept-charset="utf-8" method="POST"
                      action="javascript:configMovimientos.guardar()" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <label for="txtPines">Pines <span class="requerido">*</span></label>
                            <input type="text" id="txtCantidadPines" name="txtCantidadPines" onkeypress="return generalSoloNumeros(event)" placeholder="Ingresa la cantidad" title="" class="form-control campoRequerido" required value="">
                            <div class="invalid-feedback">
                                Cantidad de pines no valido
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <label for="txtPlastico">Plásticos <span class="requerido">*</span></label>
                            <input type="text" id="txtCantidadPlastico" name="txtCantidadPlastico" onkeypress="return generalSoloNumeros(event)" placeholder="Ingresa la cantidad" title="" class="form-control campoRequerido" required value="">
                            <div class="invalid-feedback">
                                Cantidad de plásticos no valido
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmEditarMovimiento" class="btn btn-outline-primary btn-sm">
                    <i class="fa fa-save"></i> Guardar
                </button>

                <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                    <i  class="fa fa-times-circle"></i> Cancelar
                </button>

            </div>
        </div>
    </div>
</div>




<?php
$_GET['js'] = [ 'controlesMovimientoPYP' ];
?>
