<div class="pagetitle">
    <h1>Solicitud de afiliación PEP o relacionados a PEP</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item">Solicitudes PEP</li>
            <li class="breadcrumb-item active">Crear</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">


<section class="section">

    <form id="formTiposSolicitud" name="formTiposSolicitud" accept-charset="utf-8" method="post"
          class="needs-validation" novalidate action="javascript:guardarSolicitud.guardar()">

        <div class="row">

            <div class="col-lg-4 col-xl-4">
                <label class="form-label" for="cboTipoSolicitud">Tipo de solicitud: <span
                            class="requerido">*</span></label>
                <select onchange="tipoSolicitud.change( this )"
                        class="selectpicker form-control cboTipoSolicitud campoRequerido" id="cboTipoSolicitud"
                        name="cboTipoSolicitud" required title="Seleccione">
                    <option selected value="" disabled>Seleccione</option>
                </select>
                <div class="invalid-feedback">
                    Seleccione el tipo de solicitud
                </div>
            </div>

            <div class="col-lg-4 col-xl-4">
                <label class="form-label" for="cboTipoAfiliacion">Tipo de afilicion: <span
                            class="requerido">*</span></label>
                <select onchange="esPep(this)"
                        class="selectpicker form-control campoRequerido cboTipoAfiliacion"
                        id="cboTipoAfiliacion"
                        name="cboTipoAfiliacion" required title="Seleccione">
                    <option selected value="0" disabled>Seleccione</option>
                </select>
                <div class="invalid-feedback">
                    Seleccione el tipo de afiliación
                </div>
            </div>

            <div class="col-lg-4 col-xl-4">
                <label class="form-label" for="cboRepresentante">¿ Quien brinda los datos ?: <span
                            class="requerido">*</span></label>
                <select onchange="esRepresentante.change(this)"
                        class="selectpicker form-control campoRequerido"
                        id="cboRepresentante"
                        name="cboRepresentante" required title="Seleccione">
                    <option value="" disabled selected>seleccione</option>
                    <option value="N">Persona</option>
                    <option value="S">Representante/Apoderado</option>
                </select>
                <div class="invalid-feedback">
                    Seleccione quien esta brindando los datos
                </div>
            </div>

        </div>
    </form>

    <br>
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="datosPersonales-tab" data-bs-toggle="tab"
                    data-bs-target="#datosPersonales" role="tab" aria-controls="Datos personales" aria-selected="true">
                Datos personales
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="representante-tab" data-bs-toggle="tab"
                    data-bs-target="#datosRepresentantes" role="tab" aria-controls="Datos representantes"
                    aria-selected="true">
                Datos del representante
            </button>
        </li>
        <li class="nav-item ms-auto">
            <button type="submit" class="btn btn-sm btn-outline-success" form="formTiposSolicitud">
                <i class="fas fa-save"></i> Guardar
            </button>
            <button type="button" class="btn btn-sm btn-outline-danger ms-1" onclick="limpiarSolicitud()">
                <i class="fas fa-times-circle"></i> Cancelar
            </button>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">

        <div class="tab-pane fade show active" id="datosPersonales" role="tabpanel" aria-labelledby="-tab">

            <form id="formDatosPersonales" name="formDatosPersonales" accept-charset="utf-8" method="post"
                  class="needs-validation" action="javascript:void(0)" novalidate>

                <div class="row">

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtCodigoAsociado">Buscar asociado: <span
                                    class="requerido">*</span></label>
                        <div class="input-group form-group has-validation">
                            <input type="text" class="form-control" id="txtCodigoAsociado"
                                   onkeypress="return generalSoloNumeros(event)" name="txtCodigoAsociado"
                                   placeholder="Codigo del cliente"
                                   onkeyup="cliente.comprobarCodigoClientePorEnter()" required readonly>
                            <div class="input-group-append">
                                <button class="input-group-text btn btn-outline-dark"
                                        onclick="cliente.comprobarCodigoCliente()" type="button" disabled
                                        id="btnBuscarAscociado"><i class="fas fa-search"></i></button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el código de cliente
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtNombres">Nombres: </label>
                        <input type="text" class="form-control campoRequerido" id="txtNombres"
                               name="txtNombres"
                               placeholder="Nombres" required readonly>
                        <div class="invalid-feedback">
                            El nombre es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtApellidos">Apellidos: </label>
                        <input type="text" class="form-control campoRequerido" id="txtApellidos"
                               name="txtApellidos"
                               placeholder="Apellidos" required readonly>
                        <div class="invalid-feedback">
                            El apellido es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboTipoDocumento">Tipo de documento: <span
                                    class="requerido">*</span></label>
                        <select required class="selectpicker form-control campoRequerido cboTipoDocumento tipoPrimario"
                                id="cboTipoDocumento" disabled
                                onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumentoPrimario');">
                            <option value="" disabled selected>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            El tipo de documento es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtDocumentoPrimario">Número de documento: <span
                                    class="requerido">*</span></label>

                        <input type="text" class="form-control numeroDocumentoPrimario"
                               id="txtDocumentoPrimario" name="txtDocumentoPrimario"
                               placeholder="Ingrese el numero de documento" required readonly>
                        <div class="invalid-feedback">
                            El numero de documento es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtFechaExpedicion">Fecha de expedición: </label>
                        <input type="text" class="form-control readonly fechaFinLimitado"
                               id="txtFechaExpedicion"
                               name="txtFechaExpedicion"
                               placeholder="Fecha de expedicion del documento" disabled required>
                        <div class="invalid-feedback">
                            El numero de documento es requerido
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-6 mayusculas">
                        <label class="form-label" for="txtLugarExpedicion">Lugar de expedición: <span class="requerido">*</span></label>

                        <input type="text" class="form-control campoRequerido" id="txtLugarExpedicion"
                               name="txtLugarExpedicion" placeholder="Lugar de expedicion del documento" required
                               readonly />
                        <div class="invalid-feedback">
                            El lugar de expedición de documento es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefono">Teléfono: </label>
                        <input type="text" class="form-control telefono" id="txtTelefono"
                               name="txtTelefono"
                               placeholder="0000-0000" readonly>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtEmail">Email: </label>
                        <input type="text" class="form-control" id="txtEmail"
                               name="txtEmail"
                               placeholder="ejemplo@gmail.com" readonly>
                    </div>

                    <div class="col-lg-6 col-xl-6 mayusculas">
                        <label class="form-label" for="txtLugarNacimiento">Lugar de nacimiento: <span class="requerido">*</span>
                        </label>

                        <input type="text" class="form-control campoRequerido" id="txtLugarNacimiento"
                               name="txtLugarNacimiento" placeholder="Lugar de nacimiento" required readonly/>
                        <div class="invalid-feedback">
                            El lugar de nacimiento es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtFechaNacimiento">Fecha nacimiento: </label>
                        <input type="text" class="form-control readonly fechaFinLimitado"
                               id="txtFechaNacimiento"
                               name="txtFechaNacimiento" disabled
                               placeholder="dd/mm/YYYY">
                        <div class="invalid-feedback">
                            La fecha de nacimiento es requerida
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboEstadoCivil">Estado civil: <span
                                    class="requerido">*</span></label>

                        <select class="form-control selectpicker campoRequerido cboEstadoCivil"
                                id="cboEstadoCivil" disabled
                                name="cboEstadoCivil" required data-live-search="true">
                            <option selected>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            El estado civil es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboGentilicio">Nacionalidad: <span
                                    class="requerido">*</span></label>
                        <select required class="form-control selectpicker cboPais"
                                data-live-search="true" disabled
                                id="cboGentilicio" name="cboGentilicio"
                        <option value="" selected disabled>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            La nacionalidad es requerida
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtLugarDeTrabajo">Lugar de trabajo: <span
                                    class="requerido">*</span> </label>
                        <input type="text" class="form-control campoRequerido" id="txtLugarDeTrabajo"
                               name="txtLugarDeTrabajo"
                               placeholder="Lugar de trabajo" readonly>
                        <div class="invalid-feedback">
                            El lugar de trabajo es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtProfesion">Profesión u oficio: <span
                                    class="requerido">*</span> </label>
                        <input type="text" class="form-control campoRequerido" id="txtProfesion"
                               name="txtProfesion"
                               placeholder="Profesion" required readonly>
                        <div class="invalid-feedback">
                            La profesión es requerida
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboPais">País: <span class="requerido">*</span></label>

                        <select class="form-control selectpicker cboPais"
                                data-live-search="true"
                                id="cboPais" name="cboPais" disabled
                                onchange="generalMonitoreoPais(this.value,'cboDepartamento');" required>
                            <option value="" selected disabled>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione una país
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboDepartamento">Departamento de residencia: <span
                                    class="requerido">*</span> </label>
                        <select title="seleccione" class="form-control selectpicker campoRequerido" data-live-search="true"
                                id="cboDepartamento" name="cboDepartamento" disabled required
                                onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');">
                            <option value="" selected>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un departamento
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboMunicipio">Municipio de residencia: <span class="requerido">*</span></label>
                        <select title="seleccione" class="form-control selectpicker campoRequerido" data-live-search="true"
                                id="cboMunicipio" name="cboMunicipio" disabled required >
                            <option selected value="" disabled>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un municipio
                        </div>
                    </div>

                    <div class="col-lg-12 col-xl-12 mayusculas ">
                        <label class="form-label" for="txtDireccion">Direccion completa: <span
                                    class="requerido">*</span> </label>
                        <input type="text" class="form-control campoRequerido" id="txtDireccion"
                               name="txtDireccion"
                               placeholder="Detalle la direccion" required readonly/>
                        <div class="invalid-feedback">
                            Indique su dirección
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-6">
                        <label class="form-label" for="cboActividadEconomicaPrincipal">Actividad economica primaria:
                            <span
                                    class="requerido">*</span></label>
                        <select class="form-control selectpicker campoRequerido cboActividadEconomica"
                                data-live-search="true" id="cboActividadEconomicaPrincipal"
                                name="cboActividadEconomicaPrincipal" disabled required>
                            <option value="0" selected>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione la actividad primaria
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-6">
                        <label class="form-label" for="cboActividadEconomicaSecundaria">Actividad economica secundaria:
                            <span
                                    class="requerido">*</span></label>
                        <select class="form-control selectpicker campoRequerido cboActividadEconomica"
                                data-live-search="true" id="cboActividadEconomicaSecundaria" disabled required
                                name="cboActividadEconomicaSecundaria">
                            <option value="0" selected>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione la actividad secundaria
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-6">
                        <label class="form-label" for="txtRemuneracionMensual">Remuneracion mensual: <span
                                    class="requerido">*</span></label>
                        <input type="text" onchange="calcularTotalDeIngresos();" readonly required
                               onkeypress="return generalSoloNumeros(event)" class="form-control campoRequerido"
                               id="txtRemuneracionMensual" name="txtRemuneracionMensual" placeholder="0.00">
                        <div class="invalid-feedback">
                            Ingresos mensuales requerido
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-6 mayusculas">
                        <label class="form-label" for="txtDetalleRemuneracion">Detalle de ingresos mensuales: <span
                                    class="requerido">*</span></label>
                        <input type="text" class="form-control campoRequerido" id="txtDetalleRemuneracion"
                               name="txtDetalleRemuneracion"  required readonly placeholder="Descripcion de remuneracion mensual">
                        <div class="invalid-feedback">
                            Detalle de ingresos es requerido
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-6">
                        <label class="form-label" for="txtOtrosIngresos">Otros ingresos:</label>
                        <div class="form-group">
                            <input type="text" onchange="calcularTotalDeIngresos();"
                                   onkeypress="return generalSoloNumeros(event)" class="form-control"
                                   id="txtOtrosIngresos" readonly name="txtOtrosIngresos" placeholder="0.00">
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-6 mayusculas">
                        <label class="form-label" for="">Detalle de otros ingresos:</label>
                        <input type="text" class="form-control" id="txtDetalleOtrosIngresos"
                               name="txtDetalleOtrosIngresos" readonly placeholder="Descripcion de otros ingresos">
                    </div>

                    <div class="col-lg-6 col-xl-6">
                        <label class="form-label" for="txtTotalIngresos">Total de ingresos:</label>
                        <input type="text" class="form-control" id="txtTotalIngresos" name="txtTotalIngresos"
                               placeholder="0.00" disabled>
                    </div>

                    <div class="col-lg-6 col-xl-6">
                        <label class="form-label" for="cboFondosPoliticos">¿Esta encaminado a manejar recursos de campaña o
                            partidos
                            politicos? <span class="requerido">*</span> </label>
                        <select required class="selectpicker form-control"
                                id="cboFondosPoliticos" name="cboFondosPoliticos">
                            <option selected value="" disabled>Seleccione</option>
                            <option value="S">SI</option>
                            <option value="N">NO</option>
                        </select>
                        <div class="invalid-feedback">
                            Campo requerido
                        </div>
                    </div>
                </div>

            </form>

            <br>
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="antecedentes-tab" data-bs-toggle="tab"
                            onclick="generalShowHideButtonTab('mainButtonContainer','btnAntecedentes');"
                            data-bs-target="#antecedentes" role="tab" aria-controls="Residencias del PEP"
                            aria-selected="true">
                        Antecedentes del PEP
                    </button>
                </li>
            </ul>

            <div class="tab-content" style="margin-top: 20px;">

                <div class="tab-pane fade show active" id="antecedentes" role="tabpanel"
                     aria-labelledby="evaluacion-tab">
                    <form id="formAntecedentes" name="formAntecedentes" method="POST">
                        <div class="row">

                            <div class="col-lg-6 col-xl-6">
                                <label class="form-label" for="cboCargoPep">Cargo politico: <span
                                            class="requerido">*</span></label>
                                <select class="selectpicker form-control cboCargoPolitico campoRequerido"
                                        id="cboCargoPepAntecedentes"
                                        name="cboCargoPepAntecedentes" required disabled>
                                    <option value="0" selected disabled>Seleccione</option>
                                </select>
                                <div class="invalid-feedback">
                                    Cargo politico es requerido
                                </div>
                            </div>

                            <div class="col-lg-6 col-xl-6 mayusculas">
                                <label class="form-label" for="txtEntidad">Organismo donde se desempeña: </label>
                                <input type="text" class="form-control campoRequerido" id="txtEntidad"
                                       name="txtEntidad" readOnly
                                       placeholder="Nombre de la entidad" required>
                                <div class="invalid-feedback">
                                    Nombre del organismo es requerido
                                </div>
                            </div>

                            <div class="col-lg-6 col-xl-6">
                                <label class="form-label" for="txtInicioGestionAntecedentes">Inicio de gestion:</label>
                                <input type="text" class="form-control readonly fechaFinLimitado"
                                       id="txtInicioGestionAntecedentes"
                                       name="txtInicioGestionAntecedentes" placeholder="Seleccione Fecha" disabled>
                            </div>

                            <div class="col-lg-6 col-xl-6">
                                <label class="form-label" for="txtFinGestionAntecedentes">Fin de gestion: </label>
                                <input type="text" class="form-control readonly fechaAbierta" id="txtFinGestionAntecedentes"
                                       name="txtFinGestionAntecedentes" placeholder="Seleccione Fecha"  disabled>
                            </div>
                        </div>

                        <hr>
                        <h6 class="mt-1">Datos de la persona reemplazante en sus funciones de cargo publico</h6>
                        <hr>

                        <div class="row">
                            <div class="col-lg-4 col-xl-4 mayusculas">
                                <label class="form-label" for="txtNombresAntecedentes">Nombres completo: </label>

                                <input type="text" class="form-control campoRequerido" id="txtNombresAntecedentes"
                                       name="txtNombresAntecedentes"
                                       placeholder="Nombre completo" readonly>
                                <div class="invalid-feedback">
                                    Nombre de la persona reemplazante es requerido
                                </div>
                            </div>

                            <div class="col-lg-4 col-xl-4">
                                <label class="form-label" for="cboCargoPoliticoReem">Cargo que desempeña: <span
                                            class="requerido">*</span></label>
                                <select class="selectpicker form-control cboCargoPolitico campoRequerido"
                                        id="cboCargoPoliticoReem" disabled name="cboCargoPoliticoReem">
                                </select>
                                <div class="invalid-feedback">
                                    Cargo del reemplazante es requerido
                                </div>
                            </div>

                            <div class="col-lg-4 col-xl-4 mayusculas">
                                <label class="form-label" for="txtEjecicioAntecedentes">Ejercicio: </label>
                                <input type="text" class="form-control campoRequerido" id="txtEjecicioAntecedentes"
                                       name="txtEjecicioAntecedentes"
                                       placeholder="Ejercicio" readonly>
                                <div class="invalid-feedback">
                                    Ejecicio del reemplazante es requerido
                                </div>
                            </div>
                        </div>

                    </form>
                </div>

            </div>

            <br>
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="nacionalidades-tab" data-bs-toggle="tab"
                            onclick="generalShowHideButtonTab('mainButtonContainer','btnPersonaRelacionada');"
                            data-bs-target="#personaRelacionada" role="tab" aria-controls="Residencias del PEP"
                            aria-selected="true">
                        Personas relacionadas
                    </button>
                </li>
                <li class="nav-item ms-auto">
                    <button type="button" onclick="gestionTblPersonaRelacionada.mostrarModal()" disabled class="nav-link active buttonModalPrincipal">
                        <i class="fa fa-plus"></i> Agregar persona relacionada
                </li>
            </ul>

            <div class="tab-content" style="margin-top: 20px;">

                <div class="tab-pane fade show active" id="personaRelacionada" role="tabpanel"
                     aria-labelledby="evaluacion-tab">

                    <table class="table table-striped table-bordered mt-5" id="tblPersonaRelacionada">
                        <thead>
                        <th>N°</th>
                        <th>Vinculo</th>
                        <th>Nombre</th>
                        <th>Parentesco</th>
                        <th>Cargo Politico</th>
                        <th>Inicio gestión</th>
                        <th>Fin gestión</th>
                        <th>Acciones</th>
                        </thead>
                    </table>

                </div>

            </div>


            <br>
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="nacionalidades-tab" data-bs-toggle="tab"
                            onclick="generalShowHideButtonTab('mainButtonContainer','btnResidencias');"
                            data-bs-target="#nacionalidades" role="tab" aria-controls="Residencias del PEP"
                            aria-selected="true">
                        ¿Tiene otra nacionalidad?
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="cuentasTerceros-tab" data-bs-toggle="tab"
                            onclick="generalShowHideButtonTab('mainButtonContainer','btnOtrasCuentas');"
                            data-bs-target="#cuentasTerceros" role="tab" aria-controls="Cuentas de terceros"
                            aria-selected="true">
                        ¿Maneja cuentas de terceros?
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="relacionPatrimonial-tab" data-bs-toggle="tab"
                            onclick="generalShowHideButtonTab('mainButtonContainer','btnRelacionPatrimonial');"
                            data-bs-target="#relacionPatrimonial" role="tab" aria-controls="Relación patrimonial"
                            aria-selected="true">
                        Relación patrimonial
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="productos-tab" data-bs-toggle="tab"
                            onclick="generalShowHideButtonTab('mainButtonContainer','btnProductos');"
                            data-bs-target="#productos" role="tab" aria-controls="Productos/Servicios a contratar"
                            aria-selected="true">
                        Productos/Servicios a contratar
                    </button>
                </li>
                <li class="nav-item ms-auto" id="mainButtonContainer">
                    <div class="tabButtonContainer" id="btnResidencias">
                        <button type="button" onclick="gestionTblResidencia.mostrarModal()" class="nav-link active buttonModalPrincipal" disabled>
                            <i class="fa fa-plus"></i> Agregar residencia
                        </button>
                    </div>
                    <div class="tabButtonContainer" id="btnOtrasCuentas" style="display: none;">
                        <button type="button" onclick="gestionTblCuentaTerceros.mostrarModal()" class="nav-link active buttonModalPrincipal" disabled>
                            <i class="fa fa-plus"></i> Agregar detalle de cuenta
                        </button>
                    </div>
                    <div class="tabButtonContainer" id="btnRelacionPatrimonial" style="display: none;">
                        <button type="button" onclick="gestionTblSociedades.mostrarModal()" class="nav-link active buttonModalPrincipal" disabled>
                            <i class="fa fa-plus"></i> Agregar relación patrimonial
                        </button>
                    </div>
                    <div class="tabButtonContainer" id="btnProductos" style="display: none;">
                        <button type="button" onclick="gestionTblProductos.mostrarModal()" class="nav-link active buttonModalPrincipal" disabled>
                            <i class="fa fa-plus"></i> Agregar Producto/Servicio
                        </button>
                    </div>
                </li>
            </ul>

            <div class="tab-content" style="margin-top: 20px;" id="contenedorTab">

                <div class="tab-pane fade show active" id="nacionalidades" role="tabpanel"
                     aria-labelledby="evaluacion-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table id="tblResidenciaExtrangero"
                                   class="table table-bordered table-striped text-center">
                                <thead>
                                <th>N°</th>
                                <th>Nacionalidad</th>
                                <th>Dirección</th>
                                <th>Acciones</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="cuentasTerceros" role="tabpanel" aria-labelledby="evaluacion-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table class="table table-striped table-bordered" id="tblOtrasCuentas">
                                <thead>
                                <th>N°</th>
                                <th>Nombre</th>
                                <th>Parentesco</th>
                                <th>Motivo</th>
                                <th>Acciones</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="relacionPatrimonial" role="tabpanel" aria-labelledby="evaluacion-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table class="table table-striped table-bordered text-center" id="tblSociedades">
                                <thead>
                                <th>N°</th>
                                <th>Razón social</th>
                                <th>Porcentaje</th>
                                <th>Acciones</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="productos" role="tabpanel" aria-labelledby="evaluacion-tab">
                    <div class="row">
                        <div class="col-lg-12 col xl-12">
                            <table class="table table-bordered table-striped" id="tblProductosContratos">
                                <thead>
                                <th>N°</th>
                                <th>Producto</th>
                                <th>Propósito de contrato</th>
                                <th>Acciones</th>
                                </thead>
                                <body></body>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="tab-pane fade" id="datosRepresentantes" role="tabpanel" aria-labelledby="-tab">

            <form id="frmDatosRepresentante" name="frmDatosRepresentante" accept-charset="utf-8" method="post"
                  class="needs-validation" novalidate>

                <div class="row">
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtNombresRepresentante">Nombres: </label>
                        <input type="text" class="form-control" id="txtNombresRepresentante"
                               name="txtNombresRepresentante"
                               placeholder="Nombres" required readonly>
                        <div class="invalid-feedback">
                            El nombre es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtApellidosRepresentante">Apellidos: </label>
                        <input type="text" class="form-control" id="txtApellidosRepresentante"
                               name="txtApellidosRepresentante"
                               placeholder="Apellidos" required readonly>
                        <div class="invalid-feedback">
                            El apellido es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboTipoDocumentoRepresentante">Tipo de documento: <span
                                    class="requerido">*</span></label>
                        <select class="selectpicker form-control campoRequerido cboTipoDocumento tipoPrimario"
                                id="cboTipoDocumentoRepresentante"
                                onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumentoPrimarioRepresentante');"
                                required disabled>
                            <option value="" disabled selected>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            El tipo de documento es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtDocumentoPrimarioRepresentante">Numero de documento: <span
                                    class="requerido">*</span></label>
                        <input type="text" class="form-control campoRequerido numeroDocumentoPrimario"
                               id="txtDocumentoPrimarioRepresentante" name="txtDocumentoPrimarioRepresentante"
                               placeholder="Ingrese el numero de documento" readonly required>
                        <div class="invalid-feedback">
                            El numero de documento es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoRepresentante">Telefono: </label>
                        <input type="text" class="form-control telefono" id="txtTelefonoRepresentante"
                               name="txtTelefonoRepresentante"
                               placeholder="0000-0000" readonly>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtEmailRepresentante">Email: </label>
                        <input type="text" class="form-control" id="txtEmailRepresentante"
                               name="txtEmailRepresentante"
                               placeholder="ejemplo@gamil.com" readonly>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboPaisRepresentante">Pais: <span
                                    class="requerido">*</span></label>
                        <select class="form-control selectpicker cboPais"
                                data-live-search="true"  title="seleccione"
                                id="cboPaisRepresentante" name="cboPaisRepresentante" disabled required
                                onchange="generalMonitoreoPais(this.value,'cboDepartamentoRepresentante');">
                            <option value="" selected disabled>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un país
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboDepartamentoRepresentante">Departamento de residencia: <span
                                    class="requerido">*</span> </label>
                        <select class="form-control selectpicker" title="seleccione" data-live-search="true"
                                id="cboDepartamentoRepresentante" name="cboDepartamentoRepresentante" disabled required
                                onchange="generalMonitoreoDepartamento('cboPaisRepresentante',this.value,'cboMunicipioRepresentante');">
                            <option value="" selected disabled>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un departamento
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboMunicipioRepresentante">Municipio de residencia: <span
                                    class="requerido">*</span>
                        </label>
                        <select class="form-control selectpicker" title="seleccione" data-live-search="true"
                                id="cboMunicipioRepresentante" name="cboMunicipioRepresentante" disabled required>
                            <option selected value="" disabled>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un municipio
                        </div>
                    </div>

                    <div class="col-lg-9 col-xl-9 mayusculas ">
                        <label class="form-label" for="txtDireccionRepresentante">Dirección completa: <span
                                    class="requerido">*</span> </label>
                        <input type="text" class="form-control campoRequerido" id="txtDireccionRepresentante"
                               name="txtDireccionRepresentante"
                               placeholder="Ingrese la dirección" readonly required/>
                        <div class="invalid-feedback">
                            La direccíon es requerida
                        </div>
                    </div>
                </div>
            </form>

            <br>
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="residenciaRepresentante-tab" data-bs-toggle="tab"
                            data-bs-target="#nacionalidadesRelacionado" role="tab"
                            aria-controls="Residencias del representante" aria-selected="true"
                            onclick="generalShowHideButtonTab('mainButtonContainer','btnResidenciasRepresentante');">
                        ¿Tiene otra nacionalidad?
                    </button>
                </li>
                <li class="nav-item">
                    <button class="nav-link" id="cargoPoliticoRepresentante-tab" data-bs-toggle="tab"
                            data-bs-target="#cargoPoliticoRelacionado" role="tab"
                            aria-controls="Cargos politicos del representante"
                            aria-selected="true"
                            onclick="generalShowHideButtonTab('mainButtonContainer','btnCargosPoliticosRepresentante');">
                        ¿Ejerce o ha ejercido un cargo político?
                    </button>
                </li>
                <li class="nav-item ms-auto" id="mainButtonContainer">
                    <div class="tabButtonContainer" id="btnResidenciasRepresentante">
                        <button type="button" onclick="gestionResidenciaRepresentante.mostrarModal()"
                                class="nav-link active buttonModal" disabled>
                            <i class="fa fa-plus"></i> Agregar residencia
                        </button>
                    </div>
                    <div class="tabButtonContainer" id="btnCargosPoliticosRepresentante" style="display: none;">
                        <button type="button" onclick="gestionTblCargosRepresentante.mostrarModal()"
                                class="nav-link active buttonModal" disabled>
                            <i class="fa fa-plus"></i> Agregar cargo político
                        </button>
                    </div>
                </li>
            </ul>

            <div class="tab-content" style="margin-top: 20px;">
                <div class="tab-pane fade show active " id="nacionalidadesRelacionado" role="tabpanel"
                     aria-labelledby="evaluacion-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table class="table table-striped table-bordered" id="tblResidenciaRepresentante">
                                <thead>
                                <th>N°</th>
                                <th>País</th>
                                <th>Residencia en el extrangero</th>
                                <th>Acciones</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade " id="cargoPoliticoRelacionado" role="tabpanel"
                     aria-labelledby="evaluacion-tab">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table class="table table-striped table-bordered" id="tblCargosPoliticos">
                                <thead>
                                <th>N°</th>
                                <th>Cargo</th>
                                <th>Inicio Gestón</th>
                                <th>Fin Gestión</th>
                                <th>Acciones</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>


    <div class="modal fade" id="modal-residencias" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
         aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar otra nacionalidad</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="javascript:gestionTblResidencia.agregarResidencia()" id="formNacionalidades"
                          name="formNacionalidades" accept-charset="utf-8" method="post" class="needs-validation"
                          novalidate>

                        <div class="row">

                            <div class="col-lg-12 col-xl-12">
                                <div class="ui_kit_select_search form-group">
                                    <label class="form-label" for="cboNacionalidad">Nacionalidad: <span
                                                class="requerido">*</span></label>
                                    <div class="form-group">
                                        <select class="form-control selectpicker campoRequerido cboPais" required
                                                data-live-search="true" id="cboNacionalidad" name="cboNacionalidad">
                                            <option value="" disabled selected>Seleccione</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Seleccione un país
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-xl-12 mayusculas">
                                <label class="form-label" for="txtLugarDeResidencia">Residencia en el extrangero: <span
                                            class="requerido">*</span> </label>
                                <input type="text" class="form-control campoRequerido" id="txtLugarDeResidencia"
                                       name="txtLugarDeResidencia"
                                       placeholder="Lugar de residencia en el extrangero" required/>
                                <div class="invalid-feedback">
                                    Lugar de residencia requerido
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-outline-primary btn-sm" form="formNacionalidades">
                        <i class="fas fa-plus-circle"></i> Guardar
                    </button>

                    <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-cuentas" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
         aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Cuentas de terceros</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="javascript:gestionTblCuentaTerceros.agregarOtraCuenta()" id="formCuentas"
                          name="formCuentas" accept-charset="utf-8" method="post" class="needs-validation"
                          novalidate>
                        <div class="row">
                            <div class="col-lg-12 col-xl-12 mayusculas">
                                <label class="form-label" for="txtNombreOtraCuenta">Nombres: <span
                                            class="requerido">*</span></label>
                                <input type="text" class="form-control" id="txtNombreOtraCuenta"
                                       name="txtNombreOtraCuenta"
                                       placeholder="Nombres de la persona" required>
                                <div class="invalid-feedback">
                                    El nombre es requerido
                                </div>
                            </div>

                            <div class="col-lg-12 col-xl-12">
                                <label class="form-label" for="cboParentezcos">Seleccione el parentezco: <span
                                            class="requerido">*</span></label>
                                <select class="selectpicker form-control campoRequerido cboParentezcos"
                                        id="cboParentezcosSociedades"
                                        name="cboParentezcosSociedades" onchange=""
                                        required data-live-search="true">
                                    <option selected value="0" disabled>Seleccione</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione el parentesco
                                </div>
                            </div>

                            <div class="col-lg-12 col-xl-12 mayusculas">
                                <label class="form-label" for="txtMotivoOtrasCuenta">Motivo <span
                                            class="requerido">*</span></label>
                                <input type="text" class="form-control campoRequerido"
                                       onkeypress=""
                                       id="txtMotivoOtrasCuenta" name="txtMotivoOtrasCuenta"
                                       placeholder="Ingrese Motivo" required>
                                <div class="invalid-feedback">
                                    El motivo es requerido
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-outline-primary btn-sm" form="formCuentas">
                        <i class="fas fa-plus-circle"></i> Guardar
                    </button>

                    <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-sociedades" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
         aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar sociedad</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="javascript:gestionTblSociedades.agregarSociedades()" id="formSociedades"
                          name="formSociedades" accept-charset="utf-8" method="post" class="needs-validation"
                          novalidate>
                        <div class="row">

                            <div class="col-lg-12 col-xl-12 mayusculas">
                                <label class="form-label" for="txtSociedad"> Ingrese el nombre una empresa
                                    <span class="requerido">*</span> </label>
                                <input type="text" class="form-control"
                                       id="txtSociedad"
                                       name="txtSociedad" placeholder="Nombre de la empresa" required/>
                                <div class="invalid-feedback">
                                    El nombre de la empresa es requerido
                                </div>

                            </div>

                            <div class="col-lg-12 col-xl-12">
                                <label class="form-label" for="txtProcentaje">Porcentaje de participación:</label>
                                <input type="text" class="form-control campoRequerido"
                                       id="txtProcentaje"
                                       name="txtProcentaje" onkeypress="return generalSoloNumeros(event)"
                                       placeholder=" 0 %" required>
                                <div class="invalid-feedback">
                                    Campo obligatorio
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-outline-primary btn-sm" form="formSociedades">
                        <i class="fas fa-plus-circle"></i> Guardar
                    </button>

                    <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-productos" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
         aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Productos a contratar</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="javascript:gestionTblProductos.agregar()" id="formProductos" name="formProductos"
                          accept-charset="utf-8" method="post" class="needs-validation" novalidate>

                        <div class="row">

                            <div class="col-lg-12 col-xl-12">
                                <label class="form-label" for="cboProductoContratados">Producto/Servicio:</label>
                                <select class="selectpicker form-control cboProductoContratados"
                                        id="cboProductoContratados"
                                        name="cboProductoContratados" required>
                                    <option value="" disabled selected>Seleccione</option>
                                </select>
                                <div class="invalid-feedback">
                                    Producto/Servicio es requerido
                                </div>
                            </div>

                            <div class="col-lg-12 col-xl-12 mayusculas">
                                <label class="form-label" for="txtProposito">Proposito del producto a contratar</label>
                                <input type="text" class="form-control" id="txtProposito"
                                       name="txtProposito"
                                       placeholder="Indicar el proposito del producto o servicio a contratar" required>
                                <div class="invalid-feedback">
                                    Campo requerido
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-outline-primary btn-sm" form="formProductos">
                        <i class="fas fa-plus-circle"></i> Guardar
                    </button>

                    <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalPersonaRelacionada" data-bs-backdrop="static" data-bs-keyboard="false"
         tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Formulario persona relacionada</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="frmPersonasRelacionadas" name="frmPersonasRelacionadas" accept-charset="utf-8" method="POST"
                      action="javascript:gestionTblPersonaRelacionada.agregar()" class="needs-validation" novalidate>

                    <div class="modal-body">
                        <div class="row">

                            <div class="col-lg-6 col-xl-6">
                                <label class="form-label" for="cboVinculo">¿Indique su vinculo con la persona
                                    relacionada? <span
                                            class="requerido">*</span></label>
                                <select
                                        class="selectpicker cboPersonaRelacionada form-control campoRequerido"
                                        id="cboVinculo" required name="cboVinculo">
                                    <option selected value="" disabled>Seleccione</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione un vinculo
                                </div>
                            </div>

                            <div class="col-lg-6 col-xl-6">
                                <label class="form-label" for="cboParentesco">Seleccione el parentezco: <span
                                            class="requerido">*</span></label>
                                <select class="selectpicker form-control cboParentezcos"
                                        id="cboParentesco"
                                        name="cboParentesco"
                                        data-live-search="true">
                                    <option selected value="" disabled>Seleccione</option>
                                </select>
                            </div>

                            <div class="col-lg-6 col-xl-6 mayusculas">
                                <label class="form-label" for="txtNombre">Nombre del familiar o relacionado <span
                                            class="requerido">*</span></label>
                                <input type="text" class="form-control campoRequerido" id="txtNombre"
                                       name="txtNombre"
                                       placeholder="Nombres" required>
                                <div class="invalid-feedback">
                                    Nombre es requerido
                                </div>
                            </div>

                            <div class="col-lg-6 col-xl-6">
                                <label class="form-label" for="cboCargoPolitico">Cargo político del familiar o
                                    relacionado: <span class="requerido">*</span> <span
                                            class="requerido">*</span></label>
                                <select required class="selectpicker form-control cboCargoPolitico"
                                        id="cboCargoPolitico"
                                        name="cboCargoPolitico" data-live-search="true">
                                </select>
                                <div class="invalid-feedback">
                                    Cargo politico es requerido
                                </div>
                            </div>

                            <div class="col-lg-6 col-xl-6">
                                <label class="form-label" for="txtIncioGestion">Inicio de gestión: </label>
                                <input type="text" class="form-control readonly fechaFinLimitado" id="txtIncioGestion"
                                       name="txtIncioGestion" placeholder="Seleccione Fecha">
                            </div>

                            <div class="col-lg-6 col-xl-6">
                                <label class="form-label" for="txtFinGestion">Fin de gestión: </label>

                                <input type="text" class="form-control readonly fechaAbierta" id="txtFinGestion"
                                       name="txtFinGestion" placeholder="Seleccione Fecha">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">

                        <button type="submit" class="btn btn-outline-primary btn-sm"><i
                                    class="fa fa-plus-circle"></i>
                            Agregar
                        </button>

                        <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                            <i class="fas fa-times-circle"></i> Cancelar
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-representante" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
         aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar nacionalidades</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="javascript:gestionResidenciaRepresentante.agregar()"
                          id="formNacionalidadesRepresentante"
                          name="formNacionalidadesRepresentante" class="needs-validation" novalidate>
                        <div class="row">

                            <div class="col-lg-12 col-xl-12">
                                <label class="form-label" for="cboNacionalidadRepresentante">Nacionalidad: <span
                                            class="requerido">*</span></label>
                                <select class="form-control selectpicker campoRequerido cboNacionalidad"
                                        data-live-search="true" id="cboNacionalidadRepresentante"
                                        name="cboNacionalidadRepresentante" required>
                                    <option value="" disabled selected>Seleccione</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione un país
                                </div>
                            </div>

                            <div class="col-lg-12 col-xl-12 mayusculas">
                                <label class="form-label" for="txtResidenciaRepresentante">Residencia en el extrangero:
                                    <span
                                            class="requerido">*</span> </label>
                                <input type="text" class="form-control"
                                       id="txtResidenciaRepresentante"
                                       name="txtResidenciaRepresentante"
                                       placeholder="Lugar de residencia en el extrangero" required/>
                                <div class="invalid-feedback">
                                    Campo requerido
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-outline-primary btn-sm" form="formNacionalidadesRepresentante">
                        <i class="fas fa-plus-circle"></i> Guardar
                    </button>

                    <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-representante-cargos" data-bs-backdrop="static" data-bs-keyboard="false"
         tabindex="-1"
         aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar gestión política</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="javascript:gestionTblCargosRepresentante.agregar()" id="formCargosRepresentante"
                          name="formCargosRepresentante" class="needs-validation" novalidate>
                        <div class="row">

                            <div class="col-lg-12 col-xl-12">
                                <label class="form-label" for="cboCargoPoliticoRepresentante">Cargo político del familiar o
                                    relacionado: <span class="requerido">*</span> <span
                                            class="requerido">*</span></label>
                                <select class="selectpicker form-control campoRequerido cboCargoPolitico"
                                        id="cboCargoPoliticoRepresentante"
                                        name="cboCargoPoliticoRepresentante" data-live-search="true">
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione un cargo político
                                </div>
                            </div>

                            <div class="col-lg-6 col-xl-6">
                                <label class="form-label" for="txtIncioGestion">Inicio de gestión: </label>
                                <input type="text" class="form-control readonly fechaFinLimitado"
                                       id="txtIncioGestion"
                                       name="txtIncioGestion" placeholder="Seleccione Fecha" required>
                                <div class="invalid-feedback">
                                    Seleccione una fecha
                                </div>
                            </div>

                            <div class="col-lg-6 col-xl-6">
                                <label class="form-label" for="txtFinGestion">Fin de gestión: </label>
                                <input type="text" class="form-control readonly fechaAbierta" id="txtFinGestion"
                                       name="txtFinGestion" placeholder="Seleccione Fecha" required>
                                <div class="invalid-feedback">
                                    Seleccione una fecha
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-outline-primary btn-sm" form="formCargosRepresentante">
                        <i class="fas fa-plus-circle"></i> Guardar
                    </button>

                    <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>

</section>


<?php
$_GET['js'] = ['controlesPeps'];