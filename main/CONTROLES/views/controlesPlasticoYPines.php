<div class="pagetitle">
    <h1>Plásticos y pines</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Plásticos y pines</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="inventario-tab" data-toggle="tab" href="#inventario" role="tab"
                    aria-controls="Inventario de Platico y pines" aria-selected="true">
                Datos de inventario
            </button>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active " id="listadoCreditos" role="tabpanel" aria-labelledby="evaluacion-tab">
            <table class="mt-4 table table-striped table-bordered" id="tblInventario">
                <thead>
                <th>N°</th>
                <th>Agencia</th>
                <th>Plástico</th>
                <th>Pines</th>
                <th>Acciones</th>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="inventario-tab" data-toggle="tab" href="#inventario" role="tab"
                    aria-controls="Inventario de Platico y pines" aria-selected="true">
                Movimientos de inventario
            </button>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active " id="listadoCreditos" role="tabpanel" aria-labelledby="evaluacion-tab">
            <div class="row">
                <div class="col-lg-4 col-xl-4">
                    <label for='cboAgenciaInventario' class="form-label">Seleccione una agencia:</label>
                    <div class="input-group form-group">
                        <select id="cboAgenciaInventario" onchange="cbocboAgenciaMovimiento( this )"
                                name="cboAgenciaInventario"
                                class="cboAgenciaInventario selectpicker form-control campoRequerido" required
                                data-live-search="true">
                            <option selected value="0">Seleccione</option>
                        </select>
                    </div>
                </div>
            </div>

            <table id="tblLogsInventario" class="table table-bordered table-striped text-center">
                <thead>
                <th>N°</th>
                <th>Agencia</th>
                <th>Acción</th>
                <th>Cantidad de Pines</th>
                <th>Cantidad de plástico</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>


<!-- MODAL CARGAR INVENTARIO -->
<div class="modal fade" id="movimientos-Modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitulo">Movimientos de inventario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="frmInventario" name="frmInventario" accept-charset="utf-8" method="POST"
                      action="javascript:configInventario.save()" class="needs-validation" novalidate>
                    <div class="row">

                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for='cboAcciones'>Acción:<span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboAcciones" name="cboAcciones" title="seleccione" class="cboAcciones selectpicker form-control" required
                                        data-live-search="true" >
                                    <option selected value="" disabled>Seleccione</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for='cboAgencia'>Agencia detalle:<span class="requerido">*</span></label>
                            <select id="cboAgencia" name="cboAgencia" required
                                    class="cboAgencia selectpicker form-control" required
                                    data-live-search="true">
                                <option selected value="">Seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una agencia
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for="txtCantidadPines">Cantidad pines: <span
                                        class="requerido">*</span></label>
                            <input type="text" id="txtCantidadPines" name="txtCantidadPines"
                                   onkeypress="return generalSoloNumeros(event)" placeholder="Ingresa la cantidad"
                                   title="" class="form-control" required value="">
                            <div class="invalid-feedback">
                                Cantidad de pines no es valido
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for="txtCantidadPlastico">Cantidad plásticos: <span
                                        class="requerido">*</span></label>
                            <input type="text" id="txtCantidadPlastico" name="txtCantidadPlastico"
                                   onkeypress="return generalSoloNumeros(event)" placeholder="Ingresa la cantidad"
                                   title="" class="form-control" required value="">
                            <div class="invalid-feedback">
                                Cantidad de plásticos no es valido
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="frmInventario" class="btn btn-outline-primary btn-sm"><i
                            class="fa fa-save"></i> Guardar
                </button>

                <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                    <i  class="fa fa-times-circle"></i> Cancelar
                </button>

            </div>
        </div>
    </div>
</div>

<?php
$_GET['js'] = ['controlesPlasticoYPines'];
?>