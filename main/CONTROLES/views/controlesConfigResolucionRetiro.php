
<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1><h1>Configuración de retiro de asociado</h1></h1>
        </div>
        <div class="col-3 topBtnContainer">
            <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="">
                <i class="fas fa-times"></i> <span>Cancelar</span>
            </button>
            <button class="btn btn-sm btn-outline-success float-end" onclick="guardarConfiguracion()" title="Guardar" type="button">
                <i class="fas fa-save"></i> <span>Guardar</span>
            </button>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item">Retiro de asociado</li>
            <li class="breadcrumb-item active">Configuración</li>
        </ol>
    </nav>
</div>
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="configuracion-tab" data-bs-toggle="tab" data-bs-target="#configuracion" role="tab" aria-controls="Configuración solicitudes de retiro" aria-selected="true">
                Configuración
            </button>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="configuracion" role="tabpanel" aria-labelledby="configuracion-tab">

            <div class="row">
                <div class="col-lg-6 col-xl-6">
                    <div class="row">
                        <div class="col d-flex align-items-end">
                            <label class="form-label">Roles del sistema</label>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-sm btn-outline-primary float-end" onclick="configRoles.mostrarModal()">
                                <i class="fas fa-plus"></i> Rol
                            </button>
                        </div>
                    </div>
                    <hr class="mt-0">
                    <div class="row">
                        <div class="col-12">
                            <table id="tblRoles" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Rol</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
                <div class="col-lg-6 col-xl-6">
                    <div class="row">
                        <div class="col d-flex align-items-end">
                            <label class="form-label">Motivos de retiro</label>
                        </div>
                        <div class="col">
                            <button type="button" id="btnAddMotivo" class="btn btn-sm btn-outline-primary float-end" onclick="configMotivos.mostrarModal()" disabled>
                                <i class="fas fa-plus"></i> Motivo
                            </button>
                        </div>
                    </div>
                    <hr class="mt-0">
                    <div class="row">
                        <div class="col-12">
                            <table id="tblMotivos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Motivo</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-roles" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal roles" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Roles</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="formRoles" name="formRoles" accept-charset="utf-8" method="POST" action="javascript:configRoles.guardar()" class="needs-validation" novalidate>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for='cboRoles'>Roles <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboRoles" name="cboRoles" title="" class="cboRoles selectpicker form-control" required  data-live-search="true">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Guardar</button>
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-motivos" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal roles" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Roles</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="formMotivos" name="formMotivos" accept-charset="utf-8" method="POST" action="javascript:configMotivos.guardar()" class="needs-validation" novalidate>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for='cboMotivos'>Motivos <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboMotivos" name="cboMotivos" title="seleccione" class="cboMotivos selectpicker form-control" required  data-live-search="true">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Guardar</button>
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
$_GET["js"] = ['controlesConfigRetiroAsociados'];
