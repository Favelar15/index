<div class="pagetitle">
    <h1>Reactivación aportaciones</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Reactivación aportaciones</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">

<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="formReactivación-tab" data-toggle="tab" href="#formularioReactivacion" role="tab"
                    aria-controls="Inventario de Platico y pines" aria-selected="true">
                Formulario de reactivación
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="primaryButtons">
                <button type="submit" id="btnGuardar" form="formReactivacion" class="btn btn-sm btn-outline-success">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button type="button" onclick="asociado.cancelar()" class="btn btn-sm btn-outline-danger">
                    <i class="fa fa-times"></i> Cancelar
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active " id="formularioReactivacion" role="tabpanel" aria-labelledby="evaluacion-tab">
            <form id="formReactivacion" name="formReactivacion" accept-charset="utf-8" method="post" action="javascript:asociado.buscar()" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtCodigoCliente">Buscar asociado: <span class="requerido">*</span></label>
                        <div class="input-group form-group has-validation">
                            <input type="text" class="form-control" id="txtCodigoCliente" onkeypress="return generalSoloNumeros(event)" name="txtCodigoCliente" placeholder="Codigo del cliente" required>
                            <div class="input-group-append">
                                <button class="input-group-text btn btn-outline-dark" type="submit"
                                        id="btnBuscarAscociado"><i class="fas fa-search"></i></button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el código de cliente
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNombres">Nombres: </label>
                        <input type="text" class="form-control" id="txtNombres" name="txtNombres" placeholder="Nombres" readonly>
                        <div class="invalid-feedback">
                            Ingrese los nombres
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtApellidos">Apellidos: </label>
                        <input type="text" class="form-control" id="txtApellidos" name="txtApellidos" placeholder="Apellidos" readonly>
                        <div class="invalid-feedback">
                            Ingrese los apellidos
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoFijo">Teléfono fijo: </label>
                        <input type="text" class="form-control" id="txtTelefonoFijo" name="txtTelefonoFijo" placeholder="0000-0000" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de teléfono
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoMovil">Teléfono movil: </label>
                        <input type="text" class="form-control" id="txtTelefonoMovil" name="txtTelefonoMovil" placeholder="0000-0000" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de teléfono
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboTipoDocumento">Tipo de documento: <span class="requerido">*</span></label>
                        <select disabled title="seleccione" class="selectpicker form-control cboTipoDocumento tipoPrimario" id="cboTipoDocumento" onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumentoPrimario');" required>
                            <option selected disabled value=''>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el tipo de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtDocumentoPrimario">Número de documento: </label>
                        <input type="text" class="form-control numeroDocumentoPrimario" id="txtDocumentoPrimario" name="txtDocumentoPrimario" placeholder="Ingrese el numero de documento" readonly required>
                        <div class="invalid-feedback">
                            Ingrese el número de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtAgenciaAfiliacion">Agencia de afiliación: </label>
                        <input type="text" class="form-control" id="txtAgenciaAfiliacion" name="txtAgenciaAfiliacion" placeholder="Agencia de Afiliacion" readonly>
                        <div class="invalid-feedback">
                            Ingrese el nombre de la agencia
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNit">Numero NIT: </label>
                        <input type="text" class="form-control" id="txtNit" name="txtNit" placeholder="0000-000000-000-0" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de NIT
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboPais">País: <span class="requerido">*</span></label>
                        <select title="seleccione" class="selectpicker form-control cboPais" title="seleccione" data-live-search="true" id="cboPais" name="cboPais" onchange="generalMonitoreoPais(this.value,'cboDepartamento');" required disabled>
                            <option value="" selected disabled>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un país
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboDepartamento">Departamento de residencia: <span class="requerido">*</span>
                        </label>
                        <select title="seleccione" class="form-select" data-live-search="true" id="cboDepartamento" name="cboDepartamento" onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');" required data-live-search="true" disabled>
                            <option value="" selected disabled>seleccione un país</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un departamento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboMunicipio">Municipio de residencia: <span class="requerido">*</span></label>
                        <select title="seleccione" disabled class="form-select" data-live-search="true" id="cboMunicipio" name="cboMunicipio" required data-live-search="true">
                            <option selected value="" disabled>seleccione un departamento</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un municipio
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mayusculas">
                        <label class="form-label" for="txtDireccionCompleta">Dirección completa: <span class="requerido">*</span></label>
                        <input readonly type="text" class="form-control" id="txtDireccionCompleta" name="txtDireccionCompleta" placeholder="Detalle la dirección" required />
                        <div class="invalid-feedback">
                            Ingrese la dirección completa
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtMontoPendiente">Monto pendiente: </label>
                        <input type="text" class="form-control" id="txtMontoPendiente" name="txtMontoPendiente" placeholder="USD" readonly>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtAbono">Abono a cuenta de aportaciones: </label>
                        <input type="text" class="form-control" id="txtAbono" name="txtAbono" placeholder="USD" readonly>
                        <div class="invalid-feedback">
                            El abono es insuficiente para reactivar la cuenta
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<?php
$_GET['js'] = ['controlesReactivacionAportaciones'];
