<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Pagadurías</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">CONTROLES</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Pagadurías</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="pagadurias-tab" data-bs-toggle="tab"
                    data-bs-target="#listPagadurias" role="tab"
                    aria-controls="Pagadurias" aria-selected="true"
                    onclick="generalShowHideButtonTab('mainButtonContainer','btnFormulario');">
                Pagadurías
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="formularioPagaduria-tab" data-bs-toggle="tab"
                    data-bs-target="#formPagadurias" role="tab"
                    aria-controls="Formulario pagadurías" aria-selected="true"
                    onclick="generalShowHideButtonTab('mainButtonContainer','btnAccciones');">
                Nueva Pagaduría
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="btnFormulario">
                <button type="button" onclick="pagaduria.irFormulario()" class="btn btn-outline-dark btn-sm buttonModalPrincipal">
                    <i class="fa fa-plus"></i> Formulario
                </button>
            </div>
            <div class="tabButtonContainer" id="btnAccciones" style="display: none;">
                <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="pagaduria.salir()">
                    <i class="fas fa-times"></i> <span>Cancelar</span>
                </button>
                <button class="btn disabled btn-sm btn-outline-success float-end" id="btnGuardar" title="Guardar" type="submit" form="formPagaduria">
                    <i class="fas fa-save"></i> <span>Guardar</span>
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">

        <div class="tab-pane fade show active" id="listPagadurias" role="tabpanel" aria-labelledby="evaluacion-tab">
            <table class="table table-striped table-bordered mt-2" id="tblPagadurias">
                <thead>
                <th>N°</th>
                <th>Fecha</th>
                <th>Numero de crédito</th>
                <th>Código cliente</th>
                <th>Nombres del asociado</th>
                <th>Agencia</th>
                <th>Acciones</th>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <div class="tab-pane fade" id="formPagadurias" role="tabpanel" aria-labelledby="evaluacion-tab">
            <form id="formAsociado" name="formAsociado" accept-charset="utf-8" method="post" action="javascript:asociado.buscar()" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtCodigoCliente">Buscar asociado: <span class="requerido">*</span></label>
                        <div class="input-group form-group has-validation">
                            <input type="text" class="form-control" id="txtCodigoCliente" onkeypress="return generalSoloNumeros(event)" name="txtCodigoCliente" placeholder="Codigo del cliente" required>
                            <div class="input-group-append">
                                <button class="input-group-text btn btn-outline-dark" type="submit"
                                        id="btnBuscarAscociado"><i class="fas fa-search"></i></button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el código de cliente
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNombres">Nombres: </label>
                        <input type="text" class="form-control" id="txtNombres" name="txtNombres" placeholder="Nombres" readonly>
                        <div class="invalid-feedback">
                            Ingrese los nombres
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtApellidos">Apellidos: </label>
                        <input type="text" class="form-control" id="txtApellidos" name="txtApellidos" placeholder="Apellidos" readonly>
                        <div class="invalid-feedback">
                            Ingrese los apellidos
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoFijo">Teléfono fijo: </label>
                        <input type="text" class="form-control" id="txtTelefonoFijo" name="txtTelefonoFijo" placeholder="0000-0000" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de teléfono
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoMovil">Teléfono movil: </label>
                        <input type="text" class="form-control" id="txtTelefonoMovil" name="txtTelefonoMovil" placeholder="0000-0000" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de teléfono
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboTipoDocumento">Tipo de documento: <span class="requerido">*</span></label>
                        <select disabled class="selectpicker form-control cboTipoDocumento tipoPrimario" id="cboTipoDocumento" onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumentoPrimario');" required>
                            <option selected disabled value=''>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el tipo de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtDocumentoPrimario">Número de documento: </label>
                        <input type="text" class="form-control numeroDocumentoPrimario" id="txtDocumentoPrimario" name="txtDocumentoPrimario" placeholder="Ingrese el numero de documento" readonly required>
                        <div class="invalid-feedback">
                            Ingrese el número de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtAgenciaAfiliacion">Agencia de afiliación: </label>
                        <input type="text" class="form-control" id="txtAgenciaAfiliacion" name="txtAgenciaAfiliacion" placeholder="Agencia de Afiliacion" readonly>
                        <div class="invalid-feedback">
                            Ingrese el nombre de la agencia
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNit">Numero NIT: </label>
                        <input type="text" class="form-control" id="txtNit" name="txtNit" placeholder="0000-000000-000-0" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de NIT
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboPais">País: <span class="requerido">*</span></label>
                        <select class="selectpicker form-control cboPais" title="seleccione" data-live-search="true" id="cboPais" name="cboPais" onchange="generalMonitoreoPais(this.value,'cboDepartamento');" required disabled>
                            <option value="" selected disabled>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un país
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboDepartamento">Departamento de residencia: <span class="requerido">*</span>
                        </label>
                        <select class="form-select" data-live-search="true" id="cboDepartamento" name="cboDepartamento" onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');" required data-live-search="true" disabled>
                            <option value="" selected disabled>seleccione un país</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un departamento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboMunicipio">Municipio de residencia: <span class="requerido">*</span></label>
                        <select disabled class="form-select" data-live-search="true" id="cboMunicipio" name="cboMunicipio" required data-live-search="true">
                            <option selected value="" disabled>seleccione un departamento</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un municipio
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mayusculas">
                        <label class="form-label" for="txtDireccionCompleta">Dirección completa: <span class="requerido">*</span></label>
                        <input readonly type="text" class="form-control" id="txtDireccionCompleta" name="txtDireccionCompleta" placeholder="Detalle la dirección" required />
                        <div class="invalid-feedback">
                            Ingrese la dirección completa
                        </div>
                    </div>
                </div>
            </form>

            <form id="formPagaduria" name="formPagaduria" accept-charset="utf-8" method="post" action="javascript:pagaduria.guardar()" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNumeroCredito">Numéro de credito: </label>
                        <input type="text" class="form-control cuentaAcacypac" id="txtNumeroCredito" name="txtNumeroCredito" placeholder="000-000-00000" required readonly>
                        <div class="invalid-feedback">
                            Numéro de credito es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtMonto">Monto: </label>
                        <input type="text" class="form-control" onkeypress="return generalSoloNumeros(event);" id="txtMonto" name="txtMonto" placeholder="USD" required readonly>
                        <div class="invalid-feedback">
                            El monto es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtCuota">Cuota: </label>
                        <input type="text" class="form-control" onkeypress="return generalSoloNumeros(event);" id="txtCuota" name="txtCuota" required placeholder="USD" readonly>
                        <div class="invalid-feedback">
                            La cuota es requerida
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtTipoGarantia">Tipo de garantia: </label>
                        <input type="text" class="form-control" onkeypress="return generalSoloLetras(event);" id="txtTipoGarantia" name="txtTipoGarantia" placeholder="" required readonly>
                        <div class="invalid-feedback">
                            El tipo de garantia es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtInstitucion">Orden de descuento para: </label>
                        <input type="text" class="form-control" id="txtInstitucion" name="txtInstitucion" required placeholder="" readonly>
                        <div class="invalid-feedback">
                            El nombre de la institución es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtFechaDesembolso" class="form-label">Fecha del desembolso: <span class="requerido">*</span></label>
                        <input type="text" id="txtFechaDesembolso" name="txtFechaDesembolso" class="form-control fechaFinLimitado readonly fecha" required disabled placeholder="dd/mm/yyyy">
                        <div class="invalid-feedback">
                            Ingrese la fecha del desembolso
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboAgencia">Agencia: <span class="requerido">*</span></label>
                        <select disabled class="selectpicker form-control cboAgencia" id="cboAgencia" onchange="configAsesor.cargarAsesores()" required>
                            <option selected disabled value=''>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un agencia
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboAsesor">Asesor de credito: <span class="requerido">*</span></label>
                        <select disabled class="selectpicker form-control cboAsesor" id="cboAsesor" required>
                            <option selected disabled value=''>seleccione una agencia</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el asesor
                        </div>
                    </div>
                </div>
            </form>

        </div>

    </div>

</section>

<?php
$_GET['js'] = ['pagadurias'];