<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Garantías bandesal</h1>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">CONTROLES</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Garantías bandesal</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="garantias-tab" data-bs-toggle="tab"
                    data-bs-target="#listGarantias" role="tab"
                    aria-controls="Pagadurias" aria-selected="true"
                    onclick="generalShowHideButtonTab('mainButtonContainer','btnFormulario');">
                Garantías bandesal
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="formularioGarantia-tab" data-bs-toggle="tab"
                    data-bs-target="#formularioGarantia" role="tab"
                    aria-controls="Formulario pagadurías" aria-selected="true"
                    onclick="generalShowHideButtonTab('mainButtonContainer','btnAccciones');">
                Nueva Garantías
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="btnFormulario">
                <button type="button" onclick="configGarantia.irFormulario()"
                        class="btn btn-outline-dark btn-sm buttonModalPrincipal">
                    <i class="fa fa-plus"></i> Formulario
                </button>
            </div>
            <div class="tabButtonContainer" id="btnAccciones" style="display: none;">
                <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar"
                        onclick="configGarantia.salir()">
                    <i class="fas fa-times"></i> <span>Cancelar</span>
                </button>
                <button class="btn disabled btn-sm btn-outline-success float-end" id="btnGuardar" title="Guardar"
                        type="submit" form="formGarantia">
                    <i class="fas fa-save"></i> <span>Guardar</span>
                </button>
            </div>
        </li>
    </ul>


    <div class="tab-content" style="margin-top: 20px;">

        <div class="tab-pane fade show active" id="listGarantias" role="tabpanel" aria-labelledby="evaluacion-tab">
            <table class="table table-bordered table-striped" id="tblGarantias">
                <thead>
                <th>N°</th>
                <th>Fecha</th>
                <th>Numero de préstamo</th>
                <th>Código cliente</th>
                <th>Nombre del asociado</th>
                <th>Agencia</th>
                <th>Acciones</th>
                </thead>
            </table>
        </div>

        <div class="tab-pane fade" id="formularioGarantia" role="tabpanel" aria-labelledby="evaluacion-tab">
            <form id="formAsociado" name="formAsociado" accept-charset="utf-8" method="post"
                  action="javascript:asociado.buscar()"
                  class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtCodigoCliente">Buscar asociado: <span
                                    class="requerido">*</span></label>
                        <div class="input-group form-group has-validation">
                            <input type="text" class="form-control" id="txtCodigoCliente"
                                   onkeypress="return generalSoloNumeros(event)" name="txtCodigoCliente"
                                   placeholder="Codigo del cliente" required>
                            <div class="input-group-append">
                                <button class="input-group-text btn btn-outline-dark" type="submit"
                                        id="btnBuscarAscociado"><i class="fas fa-search"></i></button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el código de cliente
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNombres">Nombres: </label>
                        <input type="text" class="form-control" id="txtNombres" name="txtNombres" placeholder="Nombres"
                               readonly>
                        <div class="invalid-feedback">
                            Ingrese los nombres
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtApellidos">Apellidos: </label>
                        <input type="text" class="form-control" id="txtApellidos" name="txtApellidos"
                               placeholder="Apellidos" readonly>
                        <div class="invalid-feedback">
                            Ingrese los apellidos
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoFijo">Teléfono fijo: </label>
                        <input type="text" class="form-control" id="txtTelefonoFijo" name="txtTelefonoFijo"
                               placeholder="0000-0000" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de teléfono
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoMovil">Teléfono movil: </label>
                        <input type="text" class="form-control" id="txtTelefonoMovil" name="txtTelefonoMovil"
                               placeholder="0000-0000" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de teléfono
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboTipoDocumento">Tipo de documento: <span
                                    class="requerido">*</span></label>
                        <select disabled class="selectpicker form-control cboTipoDocumento tipoPrimario"
                                id="cboTipoDocumento"
                                onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumentoPrimario');" required>
                            <option selected disabled value=''>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el tipo de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtDocumentoPrimario">Número de documento: </label>
                        <input type="text" class="form-control numeroDocumentoPrimario" id="txtDocumentoPrimario"
                               name="txtDocumentoPrimario" placeholder="Ingrese el numero de documento" readonly
                               required>
                        <div class="invalid-feedback">
                            Ingrese el número de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtAgenciaAfiliacion">Agencia de afiliación: </label>
                        <input type="text" class="form-control" id="txtAgenciaAfiliacion" name="txtAgenciaAfiliacion"
                               placeholder="Agencia de Afiliacion" readonly>
                        <div class="invalid-feedback">
                            Ingrese el nombre de la agencia
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNit">Numero NIT: </label>
                        <input type="text" class="form-control" id="txtNit" name="txtNit"
                               placeholder="0000-000000-000-0" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de NIT
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboPais">País: <span class="requerido">*</span></label>
                        <select class="selectpicker form-control cboPais" title="seleccione" data-live-search="true"
                                id="cboPais" name="cboPais"
                                onchange="generalMonitoreoPais(this.value,'cboDepartamento');" required disabled>
                            <option value="" selected disabled>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un país
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboDepartamento">Departamento de residencia: <span
                                    class="requerido">*</span>
                        </label>
                        <select class="form-select" data-live-search="true" id="cboDepartamento" name="cboDepartamento"
                                onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');" required
                                data-live-search="true" disabled>
                            <option value="" selected disabled>seleccione un país</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un departamento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboMunicipio">Municipio de residencia: <span
                                    class="requerido">*</span></label>
                        <select disabled class="form-select" data-live-search="true" id="cboMunicipio"
                                name="cboMunicipio" required data-live-search="true">
                            <option selected value="" disabled>seleccione un departamento</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un municipio
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mayusculas">
                        <label class="form-label" for="txtDireccionCompleta">Dirección completa: <span
                                    class="requerido">*</span></label>
                        <input readonly type="text" class="form-control" id="txtDireccionCompleta"
                               name="txtDireccionCompleta" placeholder="Detalle la dirección" required/>
                        <div class="invalid-feedback">
                            Ingrese la dirección completa
                        </div>
                    </div>
                </div>
            </form>

            <form id="formGarantia" name="formGarantia" accept-charset="utf-8" method="post"
                  action="javascript:configGarantia.guardarGarantia()" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtComprobante">Comprobante de la reserva: <span
                                    class="requerido">*</span> </label>
                        <input type="text" class="form-control" onkeypress="return generalSoloNumeros(event);"
                               id="txtComprobante" name="txtComprobante" placeholder="Numero de comprabante" required
                               readonly>
                        <div class="invalid-feedback">
                            El monto es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNumeroPrestamo">Numéro de prestamo: <span
                                    class="requerido">*</span></label>
                        <input type="text" class="form-control cuentaAcacypac" id="txtNumeroPrestamo"
                               name="txtNumeroPrestamo" placeholder="000-000-00000" required readonly>
                        <div class="invalid-feedback">
                            Numéro de credito es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtMonto">Monto del crédito: <span class="requerido">*</span>
                        </label>
                        <input type="text" class="form-control" onkeypress="return generalSoloNumeros(event);"
                               onkeyup="configPorcentajeGarantizado.calcular()"
                               id="txtMonto" name="txtMonto" placeholder="USD" required readonly>
                        <div class="invalid-feedback">
                            El monto es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtPorcentajeGarantizado">Porcentaje garantizado: <span
                                    class="requerido">*</span></label>
                        <input type="text" class="form-control" onkeypress="return generalSoloNumeros(event);"
                               id="txtPorcentajeGarantizado" name="txtPorcentajeGarantizado" placeholder="%" required
                               readonly onkeyup="configPorcentajeGarantizado.calcular()">
                        <div class="invalid-feedback">
                            El porcentaje garantizado es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtMontoGarantizado">Monto a garantizar: <span
                                    class="requerido">*</span></label>
                        <input type="text" class="form-control"
                               id="txtMontoGarantizado" name="txtMontoGarantizado" placeholder="USD" required
                               readonly>
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtMontoAdicional">Monto de la garantia adicional: <span
                                    class="requerido">*</span></label>
                        <input type="text" class="form-control"
                               id="txtMontoAdicional" name="txtMontoAdicional" placeholder="USD" required
                               readonly>
                        <div class="invalid-feedback">
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtFechaDesembolso" class="form-label">Fecha del desembolso: <span
                                    class="requerido">*</span></label>
                        <input type="text" id="txtFechaDesembolso" name="txtFechaDesembolso"
                               class="form-control fechaFinLimitado readonly fecha" required disabled
                               placeholder="dd/mm/yyyy">
                        <div class="invalid-feedback">
                            Ingrese la fecha del desembolso
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtFechaVencimiento" class="form-label">Fecha del vencimiento: <span class="requerido">*</span></label>
                        <input type="text" id="txtFechaVencimiento" name="txtFechaVencimiento"
                               class="form-control readonly fechaAbierta" required disabled
                               placeholder="dd/mm/yyyy">
                        <div class="invalid-feedback">
                            Ingrese la fecha de vencimiento
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtPorcentajeDescontado">Porcentaje descontado: <span class="requerido">*</span></label>
                        <input type="text" class="form-control" onkeypress="return generalSoloNumeros(event);" onkeyup="configPorcentajeGarantizado.calcular()"
                               id="txtPorcentajeDescontado" name="txtPorcentajeDescontado" placeholder="%" required
                               readonly>
                        <div class="invalid-feedback">
                            El porcentaje descontado es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtValorDescontado">Valor descontado en el desembolso: <span class="requerido">*</span></label>
                        <input type="text" class="form-control"
                               id="txtValorDescontado" name="txtValorDescontado" placeholder="USD" required readonly>
                        <div class="invalid-feedback">
                            El valor descontado es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtAñosExperiencia">Años de experiencia
                            financiado: <span class="requerido">*</span></label></label>
                        <input type="text" class="form-control"
                               id="txtAñosExperiencia" onkeypress="return generalSoloNumeros(event); name="txtAñosExperiencia" placeholder="" required readonly>
                        <div class="invalid-feedback">
                            El tipo de garantia es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboTipoGarantia">Tipo de garantia: </label>
                        <select disabled class="selectpicker form-control cboTipoGarantia" id="cboTipoGarantia"
                                required>
                            <option selected disabled value=''>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            El tipo de garantia es requerido
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="cboPrograma">Programas: </label>
                        <select disabled class="selectpicker form-control cboPrograma" id="cboPrograma"
                                required>
                            <option selected disabled value=''>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            El tipo de programa es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboLineaPrestamo">Línea de préstamo: <span
                                    class="requerido">*</span> </label>
                        <select disabled class="selectpicker form-control cboLineaPrestamo" id="cboLineaPrestamo"
                                required>
                            <option selected disabled value=''>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione la linea de préstamo
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboTipoPrestamo">Tipo de préstamo: <span
                                    class="requerido">*</span> </label>
                        <select disabled class="selectpicker form-control cboTipoPrestamo" id="cboTipoPrestamo"
                                required onchange="configTipoPrestamo.isRotativa()">
                            <option selected disabled value=''>seleccione</option>
                            <option value="R">Línea rotativa</option>
                            <option value="D">Decreciente</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el tipo de préstamo
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label for="txtFechaVencimientoRotativa" class="form-label">Vencimiento de la línea rotativa: <span class="requerido">*</span></label>
                        <input type="text" id="txtFechaVencimientoRotativa" name="txtFechaVencimientoRotativa"
                               class="form-control readonly fechaAbierta" required disabled
                               placeholder="dd/mm/yyyy">
                        <div class="invalid-feedback">
                            Ingrese la fecha de vencimiento
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboEsProgara">FSG/PROGARA: <span
                                    class="requerido">*</span> </label>
                        <select disabled class="selectpicker form-control cboEsProgara" id="cboEsProgara"
                                required>
                            <option selected disabled value=''>seleccione</option>
                            <option value="FSG">FSG</option>
                            <option value="PROGARA">PROGARA</option>
                        </select>
                        <div class="invalid-feedback">
                            Campo obligatorio
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtDiasAtrasados">Días de atraso: <span class="requerido">*</span></label>
                        <input type="text" class="form-control" onkeypress="return generalSoloNumeros(event);"
                               id="txtDiasAtrasados" name="txtDiasAtrasados" placeholder="Número de días" required
                               readonly onkeyup="configRiesgo.clasificar()">
                        <div class="invalid-feedback">
                           Campo obligatorio
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtRiesgo">Clasificación de riesgo: <span class="requerido">*</span></label>
                        <input type="text" class="form-control" onkeypress="return generalSoloNumeros(event);"
                               id="txtRiesgo" name="txtRiesgo" placeholder="Ingrese los dias de atraso" required readonly>
                        <div class="invalid-feedback">
                            El valor descontado es requerido
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboAgencia">Agencia: <span class="requerido">*</span></label>
                        <select disabled onchange="configAsesor.cargarAsesores()" class="selectpicker form-control cboAgencia" id="cboAgencia"
                                onchange="" required>
                            <option selected disabled value=''>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un agencia
                        </div>
                    </div>

                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboAsesor">Asesor de credito: <span
                                    class="requerido">*</span></label>
                        <select disabled class="selectpicker form-control cboAsesor" id="cboAsesor" required>
                            <option selected disabled value=''>seleccione una agencia</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el asesor
                        </div>
                    </div>
                </div>
            </form>

            <hr>
            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                <li class="nav-item">
                    <button class="nav-link active" id="proyectos-tab" data-bs-toggle="tab"
                            data-bs-target="#proyectos" role="tab"
                            aria-controls="Proyectos" aria-selected="true">
                        Proyectos
                    </button>
                </li>
                <li class="nav-item ms-auto" id="mainButtonContainer">
                    <div class="tabButtonContainer">
                        <button type="button" id="btnAgregarProyecto" onclick="proyecto.mostrarModal()" class="btn btn-outline-dark btn-sm disabled">
                            <i class="fa fa-plus"></i> Agregar
                        </button>
                    </div>
                </li>
            </ul>

            <div class="tab-content" style="margin-top: 20px;">
                <div class="tab-pane fade show active" id="proyectos" role="tabpanel" aria-labelledby="evaluacion-tab">
                    <table class="table table-bordered table-striped" id="tblProyectos">
                        <thead>
                        <th>N°</th>
                        <th>Descripción</th>
                        <th>Departamento</th>
                        <th>Municipio</th>
                        <th>Direción</th>
                        <th>Acciones</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div>

</section>

<div class="modal fade" id="modal-proyectos" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Descripción de proyecto</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="javascript:proyecto.guardar()" id="formProyectos"
                      name="formProyectos" accept-charset="utf-8" method="post" class="needs-validation"
                      novalidate>

                    <div class="row">

                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for="txtDescripcion">Descripción del proyecto:</label>
                            <input type="text" class="form-control" id="txtDescripcion"
                                   name="txtDescripcion"
                                   placeholder="Descripción del proyecto"/>
                        </div>

                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for="cboDepartamentoP">Departamento: <span
                                        class="requerido">*</span></label>
                            <select class="form-control selectpicker cboDepartamentoP" required
                                    onchange="proyecto.cboDepartamento(this)"
                                    data-live-search="true" id="cboDepartamentoP" name="cboDepartamentoP">
                                <option value="" disabled selected>seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un departamento
                            </div>
                        </div>

                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for="cboMunicipioP">Municipio: <span
                                        class="requerido">*</span></label>
                            <select class="form-control selectpicker cboMunicipioP" required
                                    data-live-search="true" id="cboMunicipioP" name="cboMunicipioP">
                                <option value="" disabled selected>Seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un municipio
                            </div>
                        </div>

                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for="txtDireccionP">Dirección completa: <span
                                        class="requerido">*</span> </label>
                            <input type="text" class="form-control" id="txtDireccionP"
                                   name="txtDireccionP"
                                   placeholder="Dirección de residencia" required/>
                            <div class="invalid-feedback">
                                La derección es requerida
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-outline-primary btn-sm" form="formProyectos">
                    <i class="fas fa-plus-circle"></i> Guardar
                </button>

                <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                    <i class="fas fa-times-circle"></i> Cancelar
                </button>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-infoNIT" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">INFORMACIÓN</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>No se puede procesar el registro, porque el asociado no tiene definido el número de NIT; para completar el registro primero debe dirigirse al apartado
                de ACTUALIZACIÓN DE DATOS y agregar el número de NIT</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>


<?php
$_GET['js'] = ['controlesGarantiaBandesal'];

