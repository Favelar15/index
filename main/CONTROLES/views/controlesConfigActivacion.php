<div class="pagetitle">
    <h1>Activación de cuentas</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item">Activación de cuentas</li>
            <li class="breadcrumb-item active">Configuración</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="cuenta-tab" data-bs-toggle="tab" data-bs-target="#cuenta" role="tab" aria-controls="Configuración solicitudes de retiro" aria-selected="true">
                Cuentas
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="btnCuenta">
                <button type="button" onclick="subAplicacion.mostrarModal()" class="btn btn-outline-dark btn-sm buttonModalPrincipal">
                    <i class="fa fa-plus"></i> Agregar subAplicación
                </button>
            </div>
        </li>
    </ul>
    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="cuenta" role="tabpanel" aria-labelledby="cuenta-tab">
            <div class="row">
                <div class="col-12">
                    <table id="tblSubAplicacion" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                        <thead>
                        <tr>
                            <th>N°</th>
                            <th>Aplicación</th>
                            <th>Tipo de Cuenta</th>
                            <th>Fecha de registro</th>
                            <th>Fecha de actualización</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="modal-subAplicaciones" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal-subAplicaciones" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Sub aplicación</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="formSubAplicacion" name="formSubAplicacion" accept-charset="utf-8" method="POST" action="javascript:subAplicacion.guardar()" class="needs-validation" novalidate>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label for="txtIdSubAplicacion" class="form-label">Código de Sub Aplicación <span class="requerido">*</span></label>
                            <input onkeypress="return generalSoloNumeros(event);" type="text" class="form-control" id="txtIdSubAplicacion" name="txtIdSubAplicacion" placeholder="Codigo de aplicación" required>
                            <div class="invalid-feedback" id="mensajeCodigo">
                                Codigo de subAplicacion es requerido
                            </div>
                        </div>

                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label for="txtTipoCuentatxtTipoCuenta" class="form-label">Tipo la cuenta: <span class="requerido">*</span></label>
                            <input type="text" id="txtTipoCuenta" name="txtTipoCuenta" class="form-control" placeholder="Nombre de la cuenta" required>
                            <div class="invalid-feedback">
                                Nombre de la cuenta es requerido
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm"> <i class=" fas fa-save"></i> Guardar</button>
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal"><i class="fas fa-times"></i> Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
$_GET['js'] = ['controlesConfigActivacion'];