<div class="pagetitle">
    <h1>Pago y envío de remesas</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Remesas</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="listadoRemesas-tab" data-bs-toggle="tab" data-bs-target="#listadoRemesas" role="tab" aria-controls="Lista de remesas" aria-selected="true" onclick="generalShowHideButtonTab('btnMainContainer','principalButtons');">
                Lista de pagos y remesas
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="agregarRemesa-tab" data-bs-toggle="tab" data-bs-target="#agregarRemesa" role="tab" aria-controls="Agregar detalle de remesas" aria-selected="true" onclick="generalShowHideButtonTab('btnMainContainer','secondaryButtons');">
                Agregar transacción de remesas
            </button>
        </li>
        <li class="nav-item ms-auto" id="btnMainContainer">
            <div class="tabButtonContainer" id="principalButtons">
                <button type="button" class="btn btn-outline-dark btn-sm" onclick="configRemesa.edit();">
                    <i class="fa fa-plus-circle"></i> Agregar registros
                </button>
            </div>
            <div class="tabButtonContainer" id="secondaryButtons" style="display: none;">
                <button type="button" onclick="configRemesa.save();" class="btn btn-sm btn-outline-primary">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button type="button" onclick="configRemesa.cancel();" class="btn btn-sm btn-outline-danger">
                    <i class="fa fa-times"></i> Cancelar
                </button>
            </div>
        </li>
    </ul>
    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="listadoRemesas" role="tabpanel" aria-labelledby="listadoRemesas-tab">
            <table class="table table-striped table-bordered" id="tblAllRemesas">
                <thead>
                    <th>ID</th>
                    <th>Fecha</th>
                    <th>Agencia</th>
                    <th>Tipo de Operación</th>
                    <th>Remesador</th>
                    <th>N° de operaciones</th>
                    <th>Monto</th>
                    <th>Usuario</th>
                    <th>Opciones</th>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="agregarRemesa" role="tabpanel" aria-labelledby="agregarRemesa-tab">
            <div class="row">
                <div class="col d-flex align-items-end">
                    <label class="form-label" class="form-label">Todas las transacciones</label>
                </div>
                <div class="col">
                    <button type="button" onclick="configTransaccion.edit({storage:'Local'});" class="btn btn-sm btn-outline-dark float-end">
                        <i class="fa fa-plus-circle"></i> Transacción
                    </button>
                </div>
            </div>
            <hr class="mt-0">
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-bordered table-striped" id="tblTransaccionRemesas">
                        <thead>
                            <th>N°</th>
                            <th>Fecha</th>
                            <th>Tipo de operación</th>
                            <th>Remesador</th>
                            <th>N° de operaciones</th>
                            <th>Monto</th>
                            <th>Acciones</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" data-bs-keyboard="false" data-bs-backdrop="static" id="addTransaccionRemesas" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Datos de transacción</h5>
            </div>
            <form id="frmAddDetalleRemesa" name="frmAddDetalleRemesa" accept-charset="utf-8" method="POST" action="javascript:configTransaccion.save()" class="needs-validation" novalidate>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for='cboAgencia'>Tipo de transacción <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboTipoTransaccion" title="" name="cboTipoTransaccion" class="cboTipoTransaccion selectpicker form-control" required data-live-search="true">
                                    <option selected disabled value="">seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for='cboEdt'>Remesador <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select id="cboEdt" name="cboEdt" title="" class="cboEdt selectpicker form-control" required  data-live-search="true">
                                    <option selected value="" disabled>seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label class="form-label" for="txtNumeroOperaciones">Numero de operaciones <span class="requerido">*</span></label>
                            <input type="text" id="txtNumeroOperaciones" name="txtNumeroOperaciones" onkeypress="return generalSoloNumeros(event)" placeholder="Ingrese numero de operaciones diarias" title="" class="form-control" required value="">
                            <div class="invalid-feedback">
                                Ingrese el número de operaciones
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label class="form-label" for="txtMonto">Monto <span class="requerido">*</span></label>
                            <input type="text" id="txtMonto" name="txtMonto" onkeypress="return generalSoloNumeros(event)" placeholder="Ingrese monto" title="" class="form-control" required value="">
                            <div class="invalid-feedback">
                                Ingrese el monto de la transacción
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <label class="form-label" for="">Fecha de la Transacción: <span class="requerido">*</span></label>
                            <input type="text" id="txtFecha" name="txtFecha" class="form-control fechaFinLimitado readonly" required placeholder="dd/mm/yyyy">
                            <div class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnGuardarRemesa" class="btn btn-primary btn-sm">
                        Guardar
                    </button>
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">
                        Cancelar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
$_GET['js'] = ['controlesRemesas'];
?>