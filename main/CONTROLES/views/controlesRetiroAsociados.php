<div class="pagetitle">
    <h1>Solicitudes de retiro de asociado</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Retiro de asociado</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="solicitudes-tab" data-bs-toggle="tab" data-bs-target="#solicitudes" role="tab" aria-controls="Solicitudes de retiro" aria-selected="true">
                Solicitudes en proceso
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="solicitudesAprobadas-tab" data-bs-toggle="tab" data-bs-target="#solicitudesAprobadas" role="tab" aria-controls="Solicitudes aprobadas" aria-selected="true">
                Solicitudes resueltas
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="solicitud-tab" data-bs-toggle="tab" data-bs-target="#solicitud" role="tab" aria-controls="Nueva solicitud" aria-selected="true">
                Formulario
            </button>
        </li>
        <li class="nav-item ms-auto" id="btnsTabs">
            <div id="btnsPrincipal" class="tabButtonContainer">
                <button type="button" onclick="configSolicitud.edit();" class="btn btn-outline-dark btn-sm">
                    <i class="fa fa-plus-circle"></i> Solicitud de retiro
                </button>
            </div>
            <div id="btnsSecondary" class="tabButtonContainer">

            </div>
        </li>
    </ul>
    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="solicitudes" role="tabpanel" aria-labelledby="solicitudes-tab">
            <table class="table table-striped table-bordered mt-3" id="tblSolicitudes">
                <thead>
                    <th>N°</th>
                    <th>Código de asociado</th>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th>Agencia</th>
                    <th>Usuario</th>
                    <th>Registro</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade " id="solicitudesAprobadas" role="tabpanel" aria-labelledby="solicitudesAprobadas-tab">
            <table class="table table-striped table-bordered mt-3" id="tblSolicitudesResueltas">
                <thead>
                    <th>N°</th>
                    <th>Código de asociado</th>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th>Agencia</th>
                    <th>Usuario</th>
                    <th>Registro</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="solicitud" role="tabpanel" aria-labelledby="solicitud-tab">
            <form id="frmSolicitud" name="frmSolicitud" method="POST" class="needs-validation" novalidate accept-charset="utf-8" action="javascript:configSolicitud.searchAsociado();">
                <div class="row">
                    <!-- Código de cliente -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtCodigoCliente">Código de cliente </label>
                        <div class="input-group form-group has-validation">
                            <input type="text" class="form-control obligatorio" id="txtCodigoCliente" name="txtCodigoCliente" placeholder="Código de cliente" required onkeypress="return generalSoloNumeros(event);">
                            <div class="input-group-append">
                                <button type="submit" class="input-group-text btn btn-outline-dark">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el código de cliente
                            </div>
                        </div>
                    </div>
                    <!-- Tipo de documento -->
                    <div class="col-lg-3 col-xl-3">
                        <div class="form-group">
                            <label class="form-label" for="cboTipoDocumento">Tipo de documento <span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select class="selectpicker form-control cboTipoDocumento tipoPrimario obligatorio" id="cboTipoDocumento" name="cboTipoDocumento" disabled onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumentoPrimario');">
                                    <option selected disabled value="">seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- Número de documento -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtDocumentoPrimario">Número de documento </label>
                        <input type="text" class="form-control numeroDocumentoPrimario obligatorio" id="txtDocumentoPrimario" name="txtDocumentoPrimario" placeholder="Ingrese el numero de documento" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de documento
                        </div>
                    </div>
                    <!-- NIT -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNIT">NIT </label>
                        <div class="input-group form-group has-validation">
                            <input type="text" class="form-control NIT numeroDocumentoSecundario obligatorio" id="txtNIT" name="txtNIT" placeholder="ingrese el numero de NIT" readonly>
                            <div class="input-group-append">
                                <button type="button" class="input-group-text btn btn-outline-dark" disabled onclick="generalHabilitarDeshabilitarEscritura(this);">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el número de NIT
                            </div>
                        </div>
                    </div>
                    <!-- Nombres de asociado -->
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtNombres">Nombres </label>
                        <input type="text" class="form-control obligatorio" id="txtNombres" name="txtNombres" placeholder="Nombres" readonly>
                        <div class="invalid-feedback">
                            Ingrese los nombres
                        </div>
                    </div>
                    <!-- Apellidos de asociado -->
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtApellidos">Apellidos </label>
                        <input type="text" class="form-control obligatorio" id="txtApellidos" name="txtApellidos" placeholder="Apellidos" readonly>
                        <div class="invalid-feedback">
                            Ingrese los apellidos
                        </div>
                    </div>
                    <!-- Profesión -->
                    <div class="col-lg-3 col-xl-3 mayusculas">
                        <label class="form-label" for="txtProfesion">Profesión </label>
                        <input type="text" class="form-control" id="txtProfesion" name="txtProfesion" placeholder="Profesión" readonly>
                        <div class="invalid-feedback">
                            Ingrese la profesión
                        </div>
                    </div>
                    <!-- Agencia origen -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboAgencia">Agencia de afiliación </label>
                        <div class="input-group form-group has-validation">
                            <select id="cboAgencia" name="cboAgencia" class="selectpicker form-control cboAgencia obligatorio" title="" disabled>
                                <option selected value="" disabled>seleccione</option>
                            </select>
                        </div>
                    </div>
                    <!-- Fecha de afiliación -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtFechaAfiliacion">Fecha de afiliación </label>
                        <input type="text" class="form-control obligatorio" id="txtFechaAfiliacion" name="txtFechaAfiliacion" placeholder="Fecha de afiliación" readonly>
                        <div class="invalid-feedback">
                            Seleccione la fecha de afiliación
                        </div>
                    </div>
                    <!-- Teléfono fijo -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoFijo">Teléfono fijo</label>
                        <div class="input-group form-group has-validation">
                            <input type="text" placeholder="0000-0000" id="txtTelefonoFijo" name="txtTelefonoFijo" class="form-control telefono" readonly>
                            <div class="input-group-append">
                                <button type="button" class="input-group-text btn btn-outline-dark" disabled onclick="generalHabilitarDeshabilitarEscritura(this);">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el número de teléfono
                            </div>
                        </div>
                    </div>
                    <!-- Teléfono móvil -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoMovil">Teléfono móvil</label>
                        <div class="input-group form-group has-validation">
                            <input type="text" placeholder="0000-0000" id="txtTelefonoMovil" name="txtTelefonoMovil" class="form-control telefono" readonly>
                            <div class="input-group-append">
                                <button type="button" class="input-group-text btn btn-outline-dark" disabled onclick="generalHabilitarDeshabilitarEscritura(this);">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el número de teléfono
                            </div>
                        </div>
                    </div>
                    <!-- Email -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtEmail">Email</label>
                        <div class="input-group form-group has-validation">
                            <input type="email" placeholder="ejemplo@gmail.com" id="txtEmail" name="txtEmail" class="form-control" readonly>
                            <div class="input-group-append">
                                <button type="button" class="input-group-text btn btn-outline-dark" disabled onclick="generalHabilitarDeshabilitarEscritura(this);">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese la dirección de correo electrónico
                            </div>
                        </div>
                    </div>
                    <!-- Tarjeta de débito -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboTarjeta">¿Tiene tarjeta de débito?</label>
                        <select class="form-select obligatorio" id="cboTarjeta" name="cboTarjeta" disabled>
                            <option value="" disabled selected>Seleccione</option>
                            <option value="S">SI</option>
                            <option value="N">NO</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione una opción
                        </div>
                    </div>
                    <!-- País -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboPais">País <span class="requerido">*</span></label>
                        <div class="input-group form-group has-validation">
                            <select disabled class="form-control selectpicker cboPais obligatorio" data-live-search="true" id="cboPais" name="cboPais" onchange="generalMonitoreoPais(this.value,'cboDepartamento');">
                                <option selected disabled value="">seleccione</option>
                            </select>
                            <div class="input-group-append">
                                <button type="button" class="input-group-text btn btn-outline-dark" disabled onclick="generalHabilitarDeshabilitarEscritura(this);">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">
                                Seleccione un país
                            </div>
                        </div>
                    </div>
                    <!-- Departamento -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboDepartamento">Departamento de residencia <span class="requerido">*</span> </label>
                        <div class="input-group form-group has-validation">
                            <select disabled class="form-control selectpicker obligatorio" data-live-search="true" id="cboDepartamento" name="cboDepartamento" onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');" data-live-search="true">
                                <option value="" selected disabled>seleccione un país</option>
                            </select>
                            <div class="input-group-append">
                                <button type="button" class="input-group-text btn btn-outline-dark" disabled onclick="generalHabilitarDeshabilitarEscritura(this);">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">
                                Seleccione un departamento
                            </div>
                        </div>
                    </div>
                    <!-- Municipio -->
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboMunicipio">Municipio de residencia <span class="requerido">*</span>
                        </label>
                        <div class="input-group form-group has-validation">
                            <select disabled class="form-control selectpicker obligatorio" data-live-search="true" id="cboMunicipio" name="cboMunicipio">
                                <option selected value="" disabled>seleccione un departamento</option>
                            </select>
                            <div class="input-group-append">
                                <button type="button" class="input-group-text btn btn-outline-dark" disabled onclick="generalHabilitarDeshabilitarEscritura(this);">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">
                                seleccione un municipio
                            </div>
                        </div>
                    </div>
                    <!-- Dirección -->
                    <div class="col-lg-6 col-xl-6 mayusculas">
                        <label class="form-label" for="txtDireccion">Dirección completa <span class="requerido">*</span> </label>
                        <div class="input-group form-group has-validation">
                            <input type="text" class="form-control obligatorio" id="txtDireccion" name="txtDireccion" placeholder="Detalle la dirección" readonly />
                            <div class="input-group-append">
                                <button type="button" class="input-group-text btn btn-outline-dark" disabled onclick="generalHabilitarDeshabilitarEscritura(this);">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">
                                Ingrese la dirección
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mt-2" align="center">
                        <button class="btn btn-sm btn-outline-success disabled" id="btnGuardar" disabled type="submit">
                            <i class="fas fa-save"></i> Guardar
                        </button>
                        <button class="btn btn-sm btn-outline-danger" type="button" onclick="configSolicitud.cancel();">
                            <i class="fas fa-times"></i> Cancelar
                        </button>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-lg-12 col-xl-12 mt-2">
                    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
                        <li class="nav-item">
                            <button class="nav-link active " id="motivosRetiro-tab" data-bs-toggle="tab" data-bs-target="#motivosRetiro" role="tab" aria-controls="motivosRetiro" aria-selected="true" onclick="generalShowHideButtonTab('tabButtonsSub','btnsMotivos');">
                                Motivo de retiro
                            </button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link" id="productos-tab" data-bs-toggle="tab" data-bs-target="#productos" role="tab" aria-controls="Productos" aria-selected="true" onclick="generalShowHideButtonTab('tabButtonsSub','btnsProductos');">
                                Productos
                            </button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link" id="detalleCreditos-tab" data-bs-toggle="tab" data-bs-target="#detalleCreditos" role="tab" aria-controls="Préstamos" aria-selected="true" onclick="generalShowHideButtonTab('tabButtonsSub','btnsPrestamos');">
                                Detalle de créditos
                            </button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link" id="gestiones-tab" data-bs-toggle="tab" data-bs-target="#gestiones" role="tab" aria-controls="Gestion" aria-selected="true" onclick="generalShowHideButtonTab('tabButtonsSub','btnsGestion');">
                                Gestión
                            </button>
                        </li>
                        <li class="nav-item ms-auto" id="tabButtonsSub">
                            <div class="tabButtonContainer" id="btnsMotivos">
                                <button type="button" onclick="configMotivoRetiro.edit();" class="btn btn-outline-primary btn-sm">
                                    <i class="fa fa-plus-circle"></i> Motivo
                                </button>
                            </div>
                            <div class="tabButtonContainer" id="btnsProductos" style="display: none;">
                                <button type="button" onclick="configProducto.edit();" class="btn btn-outline-primary btn-sm">
                                    <i class="fa fa-plus-circle"></i> Producto
                                </button>
                            </div>
                            <div class="tabButtonContainer" id="btnsPrestamos" style="display: none;">
                            </div>
                            <div class="tabButtonContainer" id="btnsGestion" style="display: none;">
                                <button type="button" onclick="configGestion.edit();" class="btn btn-outline-primary btn-sm">
                                    <i class="fa fa-plus-circle"></i> Gestión
                                </button>
                            </div>
                        </li>
                    </ul>
                    <div class="tab-content" style="margin-top: 20px;">
                        <div class="tab-pane fade show active" id="motivosRetiro" role="tabpanel" aria-labelledby="motivos-tab">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <table id="tblMotivos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <th>ID</th>
                                            <th>Motivo de retiro</th>
                                            <th>Detalle</th>
                                            <th>Opciones</th>
                                        </thead>

                                        <body>
                                        </body>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="productos" role="tabpanel" aria-labelledby="productos-tab">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <div class="input-group input-group-sm mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Monto total de ahorros: $</span>
                                        </div>
                                        <input type="number" disabled id="numMontoTotal" value="0.00" name="numMontoTotal" class="form-control" min="0" step="0.01">
                                    </div>
                                    <table id="tblProductos" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <th>N°</th>
                                            <th>Producto</th>
                                            <th>Monto</th>
                                            <th>Opciones</th>
                                        </thead>

                                        <body>
                                        </body>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="detalleCreditos" role="tabpanel" aria-labelledby="detalleCreditos-tab">
                            <div class="row">
                                <div class="col-lg-6 col-xl-6">
                                    <label class="form-label" for="numMontoCreditos">Monto de préstamos </label>
                                    <div class="input-group form-group">
                                        <input type="number" onkeypress="return generalSoloNumeros(event);" class="form-control" id="numMontoCreditos" name="numMontoCreditos" placeholder="Ingrese el monto de los préstamos" />
                                    </div>
                                </div>
                                <div class="col-lg-6 col-xl-6">
                                    <label class="form-label" for="montoCuentasCobrar">Monto de cuentas por cobrar </label>
                                    <div class="input-group form-group">
                                        <input type="number" onkeypress="return generalSoloNumeros(event);" class="form-control" id="montoCuentasCobrar" name="montoCuentasCobrar" placeholder="Ingrese el monto de cuentas por cobrar" />
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xl-12 mayusculas">
                                    <form id="frmDeudor" name="frmDeudor" class="needs-validation" novalidate accept-charset="utf-8" method="POST" action="javascript:configDeudor.save();">
                                        <label class="form-label" for="txtDeudor">Nombre de deudor <span class="requerido">*</span> </label>
                                        <div class="input-group form-group has-validation">
                                            <input type="text" class="form-control" id="txtDeudor" name="txtDeudor" placeholder="Nombre de deudor" required />
                                            <div class="input-group-append">
                                                <button type="submit" class="input-group-text btn btn-outline-dark">
                                                    <i class="fas fa-plus-circle"></i>
                                                </button>
                                            </div>
                                            <div class="invalid-feedback">
                                                Ingrese el nombre del deudor
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-12 col-xl-12">
                                    <label class="form-label">Personas a las que sirve de fiador</label>
                                    <table id="tblDeudores" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead>
                                            <th>N°</th>
                                            <th>Nombre</th>
                                            <th>Opciones</th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="gestiones" role="tabpanel" aria-labelledby="gestiones-tab">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    <table id="tblGestiones" class="table table-striped table-bordered table-hover" style="text-align:center; width:100% !important;">
                                        <thead class="justify-content-center">
                                            <th>N°</th>
                                            <th>Área</th>
                                            <th>Gestión</th>
                                            <th>Fecha de registro</th>
                                            <th>Acciones</th>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modales -->
<div class="modal fade" id="mdlMotivoRetiro" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmMotivoRetiro" class="needs-validation" novalidate name="frmMotivoRetiro" method="POST" action="javascript:configMotivoRetiro.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Motivo de retiro</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for="cboMotivoRetiro">Motivo de retiro <span class="requerido">*</span></label>
                            <select class="form-select cboMotivoRetiro" id="cboMotivoRetiro" required name="cboMotivoRetiro" onchange="configMotivoRetiro.evalExplicacion(this.value);">
                                <option selected disabled value="">seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una opción
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for="txtExplicacionMotivo">Explique</label>
                            <input type="text" id="txtExplicacionMotivo" name="txtExplicacionMotivo" class="form-control" title="">
                            <div class="invalid-feedback">
                                Ingrese la explicación
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlProducto" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmProducto" name="frmProducto" class="needs-validation" novalidate method="POST" action="javascript:configProducto.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Producto</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for="cboProducto">Producto <span class="requerido">*</span></label>
                            <select class="form-select cboProducto" id="cboProducto" name="cboProducto" required>
                                <option selected disabled value="">seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un producto
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for="numMonto">Monto <span class="requerido">*</span></label>
                            <input type="number" id="numMonto" name="numMonto" class="form-control" title="" onkeypress="return generalSoloNumeros(event);" required min="0" step="0.01">
                            <div class="invalid-feedback">
                                Ingrese el monto
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlGestion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <form id="frmGestion" name="frmGestion" class="needs-validation" novalidate method="POST" action="javascript:configGestion.save();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Gestión</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for="cboArea">Área <span class="requerido">*</span></label>
                            <select class="form-select cboArea" required id="cboArea" name="cboArea" required>
                                <option selected disabled value="">seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un área
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for="txtGestion">Gestión <span class="requerido">*</span></label>
                            <input type="text" id="txtGestion" name="txtGestion" class="form-control" title="" required>
                            <div class="invalid-feedback">
                                Ingrese la gestión
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlResolucion" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg">
        <form id="frmResolucion" name="frmResolucion" class="needs-validation" novalidate method="POST" action="javascript:configSolicitud.saveResolucion();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Resolución (<span style="font-weight: bold;"></span>)</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for="cboArea">Resolución <span class="requerido">*</span></label>
                            <select class="form-select cboResolucion" required id="cboResolucion" name="cboResolucion">
                                <option selected disabled value="">seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una opción
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for="txtFechaResolucion">Fecha de resolución: </label>
                            <input type="text" class="form-control readonly fechaFinLimitado"
                                   id="txtFechaResolucion"
                                   name="txtFechaResolucion"
                                   placeholder="dd/mm/YYYY" required>
                            <div class="invalid-feedback">
                                La fecha de resolución es requerida
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-12 mayusculas">
                            <label class="form-label" for="txtObservacionResolucion">Observaciones</label>
                            <input type="text" id="txtObservacionResolucion" name="txtObservacionResolucion" class="form-control" title="">
                            <div class="invalid-feedback">
                                Ingrese las observaciones
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="mdlPago" data-bs-keyboard="false" data-bs-backdrop="static">
    <div class="modal-dialog">
        <form id="frmPago" name="frmPago" class="needs-validation" novalidate method="POST" action="javascript:configSolicitud.savePago();">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pago de retiro (<span style="font-weight: bold;"></span>)</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <table class="table table-striped table-hover table-bordered mt-3" id="tblPagos" style="text-align:center; width:100% !important;">
                                <thead>
                                    <th>N°</th>
                                    <th>Producto</th>
                                    <th>Monto en solicitud</th>
                                    <th>Monto a pagar</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                    <button type="button" data-bs-dismiss="modal" class="btn btn-sm btn-secondary">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$_GET["js"] = ['controlesRetiroAsociados'];
