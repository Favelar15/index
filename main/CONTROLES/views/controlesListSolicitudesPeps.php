<div class="pagetitle">
    <h1>Solicitudes de afiliación PEP o relacionados</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Solicitudes PEP</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button type="button" class="nav-link active" id="solicitudesPendientes-tab" data-bs-toggle="tab"
                    data-bs-target="#solicitudesEnProceso" role="tab" aria-controls="Solicitudes en procesos"
                    aria-selected="true" onclick="">
                Solicitudes en proceso
            </button>
        </li>
        <li class="nav-item">
            <button type="button" class="nav-link" id="solicitudesResueltas-tab" data-bs-toggle="tab"
                    data-bs-target="#solicitudesAprobadas" role="tab" aria-controls="Solicitudes resueltas"
                    aria-selected="true">
                Solicitudes resueltas
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="primaryButtons">
                <button type="button" onclick="gestionSolicitudEnProceso.nuevaSoliccitud()" class="btn btn-sm btn-outline-dark">
                    <i class="fa fa-plus"></i> Nueva solicitud
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">

        <div class="tab-pane fade show active" id="solicitudesEnProceso" role="tabpanel"
             aria-labelledby="evaluacion-tab">
            <table class="table table-bordered table-striped" id="tblSolicitudesEnProceso">
                <thead>
                <th>N°</th>
                <th>Tipo de solicitud</th>
                <th>Solicitante</th>
                <th>Estado</th>
                <th>Agencia</th>
                <th>Registro</th>
                <th>Acciones</th>
                </thead>
            </table>
        </div>

        <div class="tab-pane fade" id="solicitudesAprobadas" role="tabpanel" aria-labelledby="evaluacion-tab">

            <table class="table table-bordered table-striped" id="tblSolicitudesCompletas">
                <thead>
                <th>N°</th>
                <th>Tipo de solicitud</th>
                <th>Solicitante</th>
                <th>Estado</th>
                <th>Agencia</th>
                <th>Fecha resolución</th>
                </thead>
            </table>

        </div>

    </div>
</section>


<div class="modal fade" id="modal-aprobarSolicitud" data-backdrop="static" data-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Resolución de la solicitud</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="javascript:gestionSolicitudEnProceso.guardarResolucion()" id="formResolucion"
                  accept-charset="utf-8" method="POST"
                  name="formResolucion" class="needs-validation" novalidate>

                <div class="modal-body">

                    <div class="row">

                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for="cboArea">
                                Area que analiza la solicitud: <span class="requerido">*</span> </label>
                            <select class="selectpicker form-control cboArea"
                                    id="cboArea" title="seleccione" required
                                    name="cboArea">
                                <option value="" selected disabled>seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione el área que analizo la solicitud
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for="cboResolucion">
                                Resolución: <span class="requerido">*</span> </label>
                            <select class="selectpicker form-control campoRequerido cboResolucion"
                                    id="cboResolucion" title="seleccione" required
                                    name="cboResolucion">
                                <option value="" selected disabled>seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Tipo de resolucion requerida
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-6">
                            <label class="form-label" for="txtFechaResolucion">Fecha de resolución: <span
                                        class="requerido">*</span> </label>
                            <input type="text" class="form-control readonly fechaFinLimitado" id="txtFechaResolucion"
                                   name="txtFechaResolucion" placeholder="dd/mm/YYYY" required>
                            <div class="invalid-feedback">
                                Fecha de resolución es requerida
                            </div>
                        </div>

                        <div class="col-lg-12 col-xl-12">
                            <label class="form-label" for="txtObservacion">Observacion: </label>
                            <input type="text" class="form-control" id="txtObservacion"
                                   name="txtObservacion" placeholder="Observacion de la solicitud">
                        </div>

                    </div>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-outline-primary btn-sm">
                        <i class="fa fa-save"></i> Guardar
                    </button>

                    <button type="button" class="btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>

                </div>
            </form>
        </div>
    </div>
</div>


<?php
$_GET['js'] = ['controlesListSolicitudesPeps'];
?>