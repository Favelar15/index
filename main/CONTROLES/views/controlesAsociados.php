<div class="pagetitle">
    <div class="row">
        <div class="col">
            <h1>Registro de asociados</h1>
        </div>
        <div class="col-4 topBtnContainer">
            <button class="btn btn-sm btn-outline-danger float-end ms-2" type="button" title="Cancelar" onclick="configAsociado.limpiar();">
                <i class="fas fa-times"></i> <span>Cancelar</span>
            </button>
            <button class="btn btn-sm btn-outline-success float-end" title="Guardar" type="submit" form="frmAsociado">
                <i class="fas fa-save"></i> <span>Guardar</span>
            </button>
        </div>
    </div>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">CONTROLES</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Registro de asociados</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section mt-2">
    <form action="javascript:configAsociado.save();" class="needs-validation" novalidate id="frmAsociado" name="frmAsociado" accept-charset="utf-8" method="post">
        <div class="row">
            <div class="col-lg-3 col-xl-3">
                <label for="txtCodigoAsociado" class="form-label">Código del cliente <span class="requerido">*</span></label>
                <div class="input-group has-validation">
                    <input onkeypress="return generalSoloNumeros(event);" type="text" class="form-control" id="txtCodigoAsociado" name="txtCodigoAsociado" placeholder="Código del cliente" required>
                    <div class="input-group-append">
                        <button class="input-group-text btn btn-outline-dark" onclick="configAsociado.verifyCode();" type="button" id="btnBuscarAscociado"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboTipoDocumento" class="form-label">Tipo de documento: <span class="requerido">*</span></label>
                <select class="form-select cboTipoDocumento tipoPrimario" id="cboTipoDocumento" onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumentoPrimario');" required>
                    <option selected disabled value=''>seleccione</option>
                </select>
                <div class="invalid-feedback">
                    Seleccione el tipo de documento
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtDocumentoPrimario" class="form-label">Número de documento: <span class="requerido">*</span></label>
                <input type="text" class="form-control numeroDocumentoPrimario" id="txtDocumentoPrimario" name="txtDocumentoPrimario" placeholder="Ingrese el número de documento" readonly required>
                <div class="invalid-feedback">
                    Ingrese el número de documento
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtNit" class="form-label">NIT: <span class="requerido">*</span></label>
                <input type="text" class="form-control NIT numeroDocumentoSecundario" id="txtNit" name="txtNit" placeholder="ingrese el número de NIT">
                <div class="invalid-feedback">
                    Ingrese el número de NIT
                </div>
            </div>
            <div class="col-lg-3 col-xl-3 mayusculas">
                <label for="txtNombres" class="form-label">Nombres: <span class="requerido">*</span></label>
                <input type="text" id="txtNombres" name="txtNombres" class="form-control" placeholder="Nombres" required>
                <div class="invalid-feedback">
                    Ingrese los nombres
                </div>
            </div>
            <div class="col-lg-3 col-xl-3 mayusculas">
                <label for="txtApellidos" class="form-label">Apellidos: <span class="requerido">*</span></label>
                <input type="text" id="txtApellidos" name="txtApellidos" class="form-control" placeholder="Apellidos" required>
                <div class="invalid-feedback">
                    Ingrese los apellidos
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtFechaNacimiento" class="form-label">Fecha de nacimiento: <span class="requerido">*</span></label>
                <input type="text" id="txtFechaNacimiento" name="txtFechaNacimiento" class="form-control fechaFinLimitado fecha" required readonly placeholder="dd/mm/yyyy">
                <div class="invalid-feedback">
                    Ingrese la fecha de nacimiento
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtEdad" class="form-label">Edad: <span class="requerido">*</span></label>
                <input type="text" id="txtEdad" name="txtEdad" class="form-control" placeholder="edad" readonly required>
                <div class="invalid-feedback">
                    Seleccione la fecha de nacimiento
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtTelefonoFijo" class="form-label">Teléfono fijo:</label>
                <input type="text" placeholder="0000-0000" id="txtTelefonoFijo" name="txtTelefonoFijo" class="form-control telefono">
                <div class="invalid-feedback">
                    Ingrese el teléfono fijo
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtTelefonoMovil" class="form-label">Teléfono movil:</label>
                <input type="text" placeholder="0000-0000" id="txtTelefonoMovil" name="txtTelefonoMovil" class="form-control telefono">
                <div class="invalid-feedback">
                    Ingrese el teléfono móvil
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtEmail" class="form-label">Correo electrónico: </label>
                <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="Correo electrónico" />
                <div class="invalid-feedback">
                    Ingrese la dirección de correo electrónico
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboGenero" class="form-label">Género: <span class="requerido">*</span></label>
                <select class="form-select" id="cboGenero" name="cboGenero" required>
                    <option selected value="" disabled>Selecciones una opción</option>
                    <option value="M">MASCULINO</option>
                    <option value="F">FEMENINO</option>
                </select>
                <div class="invalid-feedback">
                    Seleccione el género
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboEstadoCivil" class="form-label">Estado civil: <span class="requerido">*</span></label>
                <div class="form-group has-validation">
                    <select class="form-control selectpicker cboEstadoCivil" id="cboEstadoCivil" name="cboEstadoCivil" required data-live-search="true">
                        <option value="" disabled selected>Seleccione</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboPais" class="form-label">País: <span class="requerido">*</span></label>
                <div class="form-group has-validation">
                    <select class="form-control selectpicker cboPais" data-live-search="true" id="cboPais" name="cboPais" onchange="generalMonitoreoPais(this.value,'cboDepartamento');" required>
                        <option value="" selected disabled>Seleccione</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboDepartamento" class="form-label">Departamento de residencia: <span class="requerido">*</span> </label>
                <div class="form-group has-validation">
                    <select class="form-control selectpicker" data-live-search="true" id="cboDepartamento" name="cboDepartamento" onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');" required data-live-search="true">
                        <option value="" selected disabled>seleccione un país</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboMunicipio" class="form-label">Municipio de residencia: <span class="requerido">*</span></label>
                <div class="form-group has-validation">
                    <select class="form-control selectpicker" data-live-search="true" id="cboMunicipio" name="cboMunicipio" required data-live-search="true">
                        <option selected value="" disabled>seleccione un departamento</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 mayusculas">
                <label for="txtDireccion" class="form-label">Dirección completa: <span class="requerido">*</span> </label>
                <input type="text" class="form-control" id="txtDireccion" name="txtDireccion" placeholder="Detalle la dirección" required />
                <div class="invalid-feedback">
                    Ingrese la dirección completa
                </div>
            </div>
            <div class="col-lg-3 col-xl-3 mayusculas">
                <label for="txtProfesion" class="form-label">Profesión: <span class="requerido">*</span></label>
                <input type="text" class="form-control" id="txtProfesion" name="txtProfesion" placeholder="Ingrese la profesión" required>
                <div class="invalid-feedback">
                    Ingrese la profesión
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtHojaLegal" class="form-label">Hoja legal:</label>
                <input type="text" id="txtHojaLegal" onkeypress="return generalSoloNumeros(event);" name="txtHojaLegal" class="form-control">
                <div class="invalid-feedback">
                    Ingrese el número de hoja legal
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboActividadEconomicaPrincipal" class="form-label">Actividad ecónomica primaria: <span class="requerido">*</span></label>
                <div class="form-group has-validation">
                    <select class="form-control selectpicker cboActividadEconomica" data-live-search="true" id="cboActividadEconomicaPrincipal" name="cboActividadEconomicaPrincipal" required data-live-search="true">
                        <option value="0" selected>Seleccione</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboActividadEconomicaSecundaria" class="form-label">Actividad ecónomica secundaria: <span class="requerido">*</span></label>
                <div class="form-group has-validation">
                    <select class="form-control selectpicker cboActividadEconomica" data-live-search="true" id="cboActividadEconomicaSecundaria" name="cboActividadEconomicaSecundaria" required data-live-search="true">
                        <option value="0" selected>Seleccione</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="cboAgencia" class="form-label">Agencia: <span class="requerido">*</span></label>
                <div class="form-group has-validation">
                    <select class="form-control selectpicker cboAgencia" data-live-search="true" id="cboAgencia" name="cboAgencia" required data-live-search="true">
                        <option selected>Seleccione</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label for="txtFechaAfiliacion" class="form-label">Fecha de afiliación: <span class="requerido">*</span></label>
                <input type="text" id="txtFechaAfiliacion" name="txtFechaAfiliacion" readonly class="form-control fechaFinLimitado" placeholder="dd/mm/yyyy" required>
                <div class="invalid-feedback">
                    Seleccione la fecha de afiliación
                </div>
            </div>
        </div>
    </form>
</section>
<?php
$_GET['js'] = ['controlesAsociados'];
