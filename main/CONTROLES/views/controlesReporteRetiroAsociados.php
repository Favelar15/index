<div class="pagetitle">
    <h1>Reporte de retiro de asociados</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Reportes</li>
            <li class="breadcrumb-item active">Retiro de asociado</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="solicitudesPagadas-tab" data-bs-toggle="tab" data-bs-target="#solicitudesPagadas" role="tab" aria-controls="Solicitudes de retiro aprobadas" aria-selected="true">
                Solicitudes de retiros
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="solicitudesAprobadas-tab" data-bs-toggle="tab" data-bs-target="#solicitudesAprobadas" role="tab" aria-controls="Solicitudes de retiro pagadas" aria-selected="true">
                Solicitudes pagadas
            </button>
        </li>
    </ul>
    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="solicitudesPagadas" role="tabpanel" aria-labelledby="solicitudes-tab">

            <form action="javascript:solicitudesAprobadas.obtenerSolicitudes()" id="formReporteRetiroAsociados" name="formReporteRetiroAsociados"
                  accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboAgencia">Agencia</label>
                        <div class="form-group has-validation">
                            <select title="Todas las agencias" class="selectpicker form-control cboAgencia"
                                    id="cboAgencia" name="cboAgencia">
                                <option selected disabled value="all">Todas las agencias</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboEstado">Estado de la solicitud</label>
                        <div class="form-group has-validation">
                            <select class="selectpicker form-control cboEstado"
                                    id="cboEstado" name="cboEstado" required>
                                <option value="" disabled selected>Seleccione</option>
                                <option value="1">PENDIENTE</option>
                                <option value="0">APROBADA</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtInicioG">Desde <span class="requerido">*</span> </label>
                        <input type="text" class="form-control fechaFinLimitado readonly" onchange="solicitudesAprobadas.cambioFecha()" id="txtInicioG" name="txtInicioG" placeholder="Fecha de inicio" required>
                        <div class="invalid-feedback">
                            Seleccione la fecha de inicio
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtFinG">Hasta <span class="requerido">*</span></label>
                        <input type="text" class="form-control fechaFinLimitado readonly" id="txtFinG" name="txtFinG" placeholder="Fecha final" required>
                        <div class="invalid-feedback">
                            Seleccione la fecha final
                        </div>
                    </div>
                    <div class="col-lg- col-xl- mt-3" align="center">
                        <button type="submit" class="btn btn-sm btn-outline-primary">
                            <i class="fas fa-search"></i> Generar reporte
                        </button>
                        <button type="button" class="disabled btn btn-sm btn-outline-success" id="btnExcelG" onclick="solicitudesAprobadas.generarReporte()">
                            <i class="fas fa-file-excel"></i> Exportar a excel
                        </button>
                        <button type="button"  class="btn btn-sm btn-outline-danger" id="btnCancelarG" onclick="solicitudesAprobadas.limpiar()">
                            <i class="fas fa-times-circle"></i> Limpiar
                        </button>
                    </div>
                </div>
            </form>
            <hr class="mb-1 mt-3">
            <table class="table table-striped table-bordered" id="tblReporteRetiroAsociados">
                <thead>
                <th>N°</th>
                <th>Registro</th>
                <th>Código cliente</th>
                <th>Nombres</th>
                <th>Agencia</th>
                <th>Monto aportación</th>
                <th>Ahorro</th>
                <th>Fecha de aprobación</th>
                </thead>
                <body></body>
            </table>

        </div>
        <div class="tab-pane fade " id="solicitudesAprobadas" role="tabpanel" aria-labelledby="solicitudesAprobadas-tab">

            <form action="javascript:solicitudesPagadas.obtenerSolicitudes()" id="formSolcitudesPagadas" name="formSolcitudesPagadas"
                  accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-lg-4 col-xl-4">
                        <label class="form-label" for="cboAgencia">Agencia</label>
                        <div class="form-group has-validation">
                            <select title="Todas las agencias" class="selectpicker form-control cboAgencia"
                                    id="cboAgenciaP" name="cboAgencia" required>
                                <option selected disabled value="all">Todas las agencias</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xl-4">
                        <label class="form-label" for="txtFechaInicio">Desde <span class="requerido">*</span> </label>
                        <input type="text" class="form-control fechaFinLimitado readonly" onchange="solicitudesPagadas.cambioFecha(this)" id="txtFechaInicio" name="txtFechaInicio" placeholder="Fecha de afiliación" required>
                        <div class="invalid-feedback">
                            Seleccione una fecha
                        </div>
                    </div>
                    <div class="col-lg-4 col-xl-4">
                        <label class="form-label" for="txtFechaFin">Hasta <span class="requerido">*</span></label>
                        <input type="text" class="form-control fechaFinLimitado readonly" id="txtFechaFin" name="txtFechaFin" placeholder="Fecha de afiliación" required>
                        <div class="invalid-feedback">
                            Seleccione una fecha
                        </div>
                    </div>
                    <div class="col-lg- col-xl- mt-3" align="center">
                        <button type="submit" class="btn btn-sm btn-outline-primary">
                            <i class="fas fa-search"></i> Generar reporte
                        </button>
                        <button type="button"  class="btn btn-sm btn-outline-success disabled" id="btnExcelP" onclick="solicitudesPagadas.exportarDatos()">
                            <i class="fas fa-file-excel"></i> Exportar a excel
                        </button>
                        <button type="button"  class="btn btn-sm btn-outline-danger" id="btnCancelar" onclick="solicitudesPagadas.limpiar()">
                            <i class="fas fa-times-circle"></i> Limpiar
                        </button>
                    </div>
                </div>
            </form>
            <hr class="mb-1 mt-3">

            <table class="table table-striped table-bordered" id="tblSolicitudesPagadas">
                <thead>
                <th>N°</th>
                <th>Código cliente</th>
                <th>Nombres</th>
                <th>Monto</th>
                <th>Agencia</th>
                <th>Fecha de pago</th>
                </thead>
                <body></body>
            </table>
        </div>
    </div>


</section>
<?php
$_GET['js'] = ['controlesReporteRetiroAsociados'];