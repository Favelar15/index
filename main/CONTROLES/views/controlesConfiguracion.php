<div class="IndexContainer">
    <div class="row" style="margin-bottom: 0;">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class=''>
                <h5 align='center'><strong>Administración del módulo CONTROLES</strong> </h5>
            </div>
        </div>
    </div>
    <hr class="bg-dark">

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="listadoRemesas-tab" data-toggle="tab" href="#listadoRemesas" role="tab" aria-controls="Lista de remesas" aria-selected="true">Administración general</a>
        </li>
        <li class="nav-item ml-auto">
            <button type="button" class="btn btn-outline-danger btn-sm mr-2"> <i class="fa fa-times"></i> Cancelar</button>
            <button type="button" class="btn btn-outline-info btn-sm"> <i class="fa fa-save"></i> Guardar cambios</button>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="listadoRemesas" role="tabpanel" aria-labelledby="listadoRemesas-tab">
            <div class="card ">
                <div class="card-header bg-info text-white">
                    Administración general de CONTROLES
                </div>
                <div class="card-body ">
                    <form>
                        <div class="form-row mb-5">
                            <div class="col-md-6 col-xs-6">
                                <label for="">¿Los usuarios de controles pueden editar?</label>
                                <select class="custom-select" id="inlineFormCustomSelectPref">
                                    <option value="1">Nunca</option>
                                    <option value="1">1 dia despues de la fecha de registro</option>
                                    <option value="1">7 dia despues de la fecha de registro</option>
                                    <option value="1">15 dia despues de la fecha de registro</option>
                                    <option value="1">30 dias despues de la fecha de registro</option>
                                    <option value="1">60 dias despues de la fecha de registro</option>
                                    <option value="1">90 dias despues de la fecha de registro</option>
                                    <option selected>Siempre</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <label for="">¿Los usuarios de controles pueden eliminar?</label>
                                <select class="custom-select" id="inlineFormCustomSelectPref">
                                    <option value="1">Nunca</option>
                                    <option value="1">24 horas desde la fecha de registro</option>
                                    <option value="1">1 semana despues de la fecha de registro</option>
                                    <option value="1">1 mes despues de la fecha de registro</option>
                                    <option value="1">3 meses despues de la fecha de registro</option>
                                    <option value="1">6 meses despues de la fecha de registro</option>
                                    <option selected>Siempre</option>
                                </select>
                            </div>
                        </div>
                    </form>

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="listadoRemesas-tab" data-toggle="tab" href="#listadoRemesas" role="tab" aria-controls="Lista de remesas" aria-selected="true">Administración general</a>
                        </li>
                        <li class="nav-item ml-auto">
                            <button type="button" class="btn btn-outline-info" onclick="tblRoles.mostrarModal()">
                                <i class="fa fa-plus"></i> Agregar
                            </button>
                        </li>
                    </ul>

                    <div class="tab-content" style="margin-top: 20px;">
                        <div class="tab-pane fade show active" id="listadoRemesas" role="tabpanel" aria-labelledby="listadoRemesas-tab">
                            <table class="table table-striped table-bordered" id="tblControlesRoles">
                                <thead>
                                <th>N°</th>
                                <th>Rol</th>
                                <th>Clasificación</th>
                                <th>Acciones</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>GERENTE DE OPERACIONES</td>
                                        <td>Administrativo</td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-sm"> <i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>ATENCION AL CLIENTE</td>
                                        <td>Operativo</td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-sm"> <i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>


    <div class="modal fade" id="modalControlesRoles" tabindex="-1" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Configuración de roles</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formControlesRoles">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 col-xl-12">
                                <div class='ui_kit_select_search form-group'>
                                    <label for='cboEdt'>Remesador :<span class="requerido">*</span></label>
                                    <div class="input-group form-group">
                                        <select id="cboEdt" name="cboEdt" class="cboEdt selectpicker form-control campoRequerido" required data-live-search="true">
                                        </select>
                                    </div>
                                    <span class="spnError">*Campo requerido</span>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-12">
                                <div class='ui_kit_select_search form-group'>
                                    <label for='cboEdt'>Remesador :<span class="requerido">*</span></label>
                                    <div class="input-group form-group">
                                        <select id="cboEdt" name="cboEdt" class="cboEdt selectpicker form-control campoRequerido" required data-live-search="true">
                                        </select>
                                    </div>
                                    <span class="spnError">*Campo requerido</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal"> <i class="fa fa-times"></i> Cancelar</button>
                        <button type="button" id="btnGuardarRemesa" class="btn btn-outline-primary btn-sm" onclick=""> <i class="fa fa-save"></i> Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



</div>

<?php
$_GET['js'] = ['controlesConfiguracion'];
?>