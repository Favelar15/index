<div class="pagetitle">
    <h1>Créditos que nos cancelan</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Registros</li>
            <li class="breadcrumb-item active">Créditos</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="listadoCreditos-tab" data-bs-toggle="tab" data-bs-target="#listadoCreditos" role="tab" aria-controls="Lista de creditos" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','primaryButtons');">
                Créditos que nos cancelan
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link disabled" id="addCredito-tab" data-bs-toggle="tab" data-bs-target="#addCredito" role="tab" aria-controls="Agregar creditos" aria-selected="true" onclick="generalShowHideButtonTab('mainButtonContainer','secondaryButtons');">
                Formulario de crédito
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="primaryButtons">
                <button type="button" onclick="configCredito.edit()" class="btn btn-sm btn-outline-dark">
                    <i class="fa fa-plus"></i> Agregar crédito
                </button>
            </div>
            <div class="tabButtonContainer" id="secondaryButtons" style="display: none;">
                <button type="submit" id="btnGuardar" form="frmCredito" class="disabled btn btn-sm btn-outline-success">
                    <i class="fa fa-save"></i> Guardar
                </button>
                <button type="button" onclick="configCredito.cancel()" class="btn btn-sm btn-outline-danger">
                    <i class="fa fa-times"></i> Cancelar
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active " id="listadoCreditos" role="tabpanel" aria-labelledby="evaluacion-tab">
            <table class="table table-bordered table-striped" id="tblCreditos">
                <thead>
                    <th>N°</th>
                    <th>Fecha</th>
                    <th>Agencia</th>
                    <th>N° Préstamo</th>
                    <th>Asociado</th>
                    <th>Tipo de Préstamo</th>
                    <th>Empresa</th>
                    <th>Saldo</th>
                    <th>Acciones</th>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        <div class="tab-pane fade " id="addCredito" role="tabpanel" aria-labelledby="evaluacion-tab">
            <form id="frmCredito" name="frmCredito" accept-charset="utf-8" method="post" action="javascript:codigoCliente.search()" class="needs-validation" novalidate>
                <h6>Datos del asociado</h6>
                <hr class="bg-secundary">
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtCodigoClienteCredito">Buscar asociado: <span class="requerido">*</span></label>
                        <div class="input-group form-group has-validation">
                            <input type="text" class="form-control" id="txtCodigoClienteCredito" onkeypress="return generalSoloNumeros(event)" name="txtCodigoCliente" placeholder="Codigo del cliente" required>
                            <div class="input-group-append">
                            </div>
                            <div class="invalid-feedback">
                                Ingrese el código de cliente
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNombres">Nombres: </label>
                        <input type="text" class="form-control" id="txtNombres" name="txtNombres" placeholder="Nombres" readonly>
                        <div class="invalid-feedback">
                            Ingrese los nombres
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtApellidos">Apellidos: </label>
                        <input type="text" class="form-control" id="txtApellidos" name="txtApellidos" placeholder="Apellidos" readonly>
                        <div class="invalid-feedback">
                            Ingrese los apellidos
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoFijo">Teléfono fijo: </label>
                        <input type="text" class="form-control" id="txtTelefonoFijo" name="txtTelefonoFijo" placeholder="0000-0000" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de teléfono
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtTelefonoMovil">Teléfono movil: </label>
                        <input type="text" class="form-control" id="txtTelefonoMovil" name="txtTelefonoMovil" placeholder="0000-0000" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de teléfono
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboTipoDocumento">Tipo de documento: <span class="requerido">*</span></label>
                        <select disabled class="form-select cboTipoDocumento tipoPrimario" id="cboTipoDocumento" onchange="generalMonitoreoTipoDocumento(this.value,'txtDocumentoPrimario');" required>
                            <option selected disabled value=''>seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione el tipo de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtDocumentoPrimario">Número de documento: </label>
                        <input type="text" class="form-control numeroDocumentoPrimario" id="txtDocumentoPrimario" name="txtDocumentoPrimario" placeholder="Ingrese el numero de documento" readonly required>
                        <div class="invalid-feedback">
                            Ingrese el número de documento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtAgenciaAfiliacionCredito">Agencia de afiliación: </label>
                        <input type="text" class="form-control" id="txtAgenciaAfiliacionCredito" name="txtAgenciaAfiliacionCredito" placeholder="Agencia de Afiliacion" readonly>
                        <div class="invalid-feedback">
                            Ingrese el nombre de la agencia
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNit">Numero NIT: </label>
                        <input type="text" class="form-control" id="txtNit" name="txtNit" placeholder="0000-000000-000-0" readonly>
                        <div class="invalid-feedback">
                            Ingrese el número de NIT
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboPais">País: <span class="requerido">*</span></label>
                        <select class="form-select cboPais" data-live-search="true" id="cboPais" name="cboPais" onchange="generalMonitoreoPais(this.value,'cboDepartamento');" required disabled>
                            <option value="" selected disabled>Seleccione</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un país
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboDepartamento">Departamento de residencia: <span class="requerido">*</span>
                        </label>
                        <select class="form-select" data-live-search="true" id="cboDepartamento" name="cboDepartamento" onchange="generalMonitoreoDepartamento('cboPais',this.value,'cboMunicipio');" required data-live-search="true" disabled>
                            <option value="" selected disabled>seleccione un país</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un departamento
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="cboMunicipio">Municipio de residencia: <span class="requerido">*</span></label>
                        <select disabled class="form-select" data-live-search="true" id="cboMunicipio" name="cboMunicipio" required data-live-search="true">
                            <option selected value="" disabled>seleccione un departamento</option>
                        </select>
                        <div class="invalid-feedback">
                            Seleccione un municipio
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 mayusculas">
                        <label class="form-label" for="txtDireccionCompleta">Dirección completa: <span class="requerido">*</span></label>
                        <input readonly type="text" class="form-control" id="txtDireccionCompleta" name="txtDireccionCompleta" placeholder="Detalle la dirección" required />
                        <div class="invalid-feedback">
                            Ingrese la dirección completa
                        </div>
                    </div>
                </div>
                <h6 class="mt-2">Detalles del crédito</h6>
                <hr class="bg-secundary">

                <div class="row" id="divCredito">
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtNumeroDePrestamo">N° de prestamo: </label>
                        <input readonly type="text" class="cuentaAcacypac form-control" id="txtNumeroDePrestamo" name="txtNumeroDePrestamo" placeholder="000-000-00000" required>
                        <div class="invalid-feedback">
                            Ingrese el número de préstamo
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3" id="divCbo">
                        <label class="form-label" for='cboEmpresas'>Empresa que nos cancela:<span class="requerido">*</span></label>
                        <div class="form-group has-validation">
                            <select disabled id="cboEmpresas" name="cboEmpresas" onchange="configEmpresa.changeCboEmpresa(this)" class=" cboEmpresas selectpicker form-control" required data-live-search="true">
                                <option selected value="" disabled>seleccione</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3" id="divTxt"  style="display: none;">
                        <label class="form-label" for='cboEmpresas'>Empresa que nos cancela:<span class="requerido">*</span></label>
                        <div class="input-group form-group has-validation" id="divTxt">
                            <input type="text" class="form-control" id="txtEmpresa" name="txtEmpresa" required placeholder="Nombre de la empresa">
                            <div class="input-group-append">
                                <button class="input-group-text btn btn-outline-success" type="button" onclick="configEmpresa.checkEmpresa()"
                                        id="btnBuscarAscociado"><i class="fas fa-save"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <div class='ui_kit_select_search form-group'>
                            <label class="form-label" for='cboTipoDePrestamo'>Tipo de prestamos:<span class="requerido">*</span></label>
                            <div class="form-group has-validation">
                                <select disabled id="cboTipoDePrestamo" name="cboTipoDePrestamo" class="cboTipoDePrestamo selectpicker form-control" required data-live-search="true">
                                    <option selected disabled value="">seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <label class="form-label" for="txtMonto">Saldo cancelado: </label>
                        <input readonly type="text" class="form-control" id="txtMonto" name="txtMonto" onkeypress="return generalSoloNumeros(event)" placeholder="0.00" required>
                        <div class="invalid-feedback">
                            Ingrese el saldo cancelado
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<?php
$_GET['js'] = ['controlesCreditos'];
?>