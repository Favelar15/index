<div class="pagetitle">
    <h1>Reporte de solicutudes de afiliación PEP</h1>
    <nav>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item">Controles</li>
            <li class="breadcrumb-item">Reportes</li>
            <li class="breadcrumb-item active">Solicitudes de afiliación PEP</li>
        </ol>
    </nav>
</div>
<hr class="mb-1 mt-1">
<section class="section">

    <form action="javascript:configReporte.obtenerSolicitudes()" id="formSolicitudPep" name="formSolicitudPep"
          accept-charset="utf-8" method="POST" class="needs-validation" novalidate>
        <div class="row">
            <div class="col-lg-3 col-xl-3">
                <label class="form-label" for="cboAgencia">Agencia</label>
                <div class="form-group has-validation">
                    <select class="selectpicker form-control cboAgencia"
                            id="cboAgencia" name="cboAgencia">
                        <option selected disabled value="all">Todas las agencias</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label class="form-label" for="cboEstado">Estado de la solicitud</label>
                <div class="form-group has-validation">
                    <select class="selectpicker form-control cboEstado"
                            id="cboEstado" name="cboEstado" required>
                        <option selected disabled value="">Seleccione</option>
                        <option value="1">PENDIENTE</option>
                        <option value="2">APROBADA</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label class="form-label" for="txtInicioG">Desde <span class="requerido">*</span> </label>
                <input type="text" class="form-control fechaFinLimitado readonly" onchange="configReporte.cambiosFecha()" id="txtInicio" name="txtInicio" placeholder="Fecha de inicio" required>
                <div class="invalid-feedback">
                    Seleccione la fecha de inicio
                </div>
            </div>
            <div class="col-lg-3 col-xl-3">
                <label class="form-label" for="txtFinG">Hasta <span class="requerido">*</span></label>
                <input type="text" class="form-control fechaFinLimitado readonly" id="txtFin" name="txtFin" placeholder="Fecha final" required>
                <div class="invalid-feedback">
                    Seleccione la fecha final
                </div>
            </div>
            <div class="col-lg- col-xl- mt-3" align="center">
                <button type="submit" class="btn btn-sm btn-outline-primary">
                    <i class="fas fa-search"></i> Generar reporte
                </button>
                <button type="button"  class="btn btn-sm btn-outline-danger" id="btnCancelar" onclick="configReporte.limpiar()">
                    <i class="fas fa-times-circle"></i> Limpiar
                </button>
            </div>
        </div>
    </form>

    <hr class="mb-1 mt-3">

    <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">
        <li class="nav-item">
            <button class="nav-link active" id="solicitudesPep-tab" data-bs-toggle="tab" data-bs-target="#solicitudesPep"
                    role="tab" aria-controls="Solicitudes de PEP" aria-selected="true"
                    onclick="generalShowHideButtonTab('mainButtonContainer','btnExcelPep')">
                Solicitudes PEP
            </button>
        </li>
        <li class="nav-item">
            <button class="nav-link" id="solicitudesRelacionados-tab" data-bs-toggle="tab" data-bs-target="#solicitudesRelacionados"
                    role="tab" aria-controls="Solicitudes de personas relacionas" aria-selected="true"
                    onclick="generalShowHideButtonTab('mainButtonContainer','btnExcelRelacionados')">
                Solicitudes de relacionados a PEP
            </button>
        </li>
        <li class="nav-item ms-auto" id="mainButtonContainer">
            <div class="tabButtonContainer" id="btnExcelPep">
                <button type="button" id="btnPep" onclick="configPep.exportarExcel()" class="btn btn-sm btn-outline-success disabled">
                    <i class="fa fa-file-excel"></i> Exportar
                </button>
            </div>
            <div class="tabButtonContainer" id="btnExcelRelacionados" style="display: none;">
                <button type="button" id="btnRelacionados" onclick="configPersonasRelacionadas.exportarExcel()" class="btn btn-sm btn-outline-success disabled">
                    <i class="fa fa-file-excel"></i> Exportar
                </button>
            </div>
        </li>
    </ul>

    <div class="tab-content" style="margin-top: 20px;">
        <div class="tab-pane fade show active" id="solicitudesPep" role="tabpanel" aria-labelledby="solicitudes-tab">

            <table class="table table-striped table-bordered" id="tblSolicitudPep">
                <thead>
                <th>N°</th>
                <th>Registro</th>
                <th>Código cliente</th>
                <th>Tipo de solicitud</th>
                <th>Agencia</th>
                <th>Nombre del aspirante</th>
                <th>DUI</th>
                <th>Fecha de aprobación</th>
                </thead>
                <body></body>
            </table>

        </div>

        <div class="tab-pane fade" id="solicitudesRelacionados" role="tabpanel" aria-labelledby="solicitudes-tab">

            <table class="table table-striped table-bordered" id="tblSolicitudRelacionados">
                <thead>
                <th>N°</th>
                <th>Registro</th>
                <th>Código cliente</th>
                <th>Tipo de solicitud</th>
                <th>Agencia</th>
                <th>Nombre del aspirante</th>
                <th>DUI</th>
                <th>Fecha de aprobación</th>
                </thead>
                <body></body>
            </table>

        </div>
    </div>


</section>


<?php
$_GET['js'] = ['controlesReporteSolicitudesPep'];