<?php
require_once './main/code/generalParameters.php';
$ipsBloqueadas = [];
$ipActual = '127.0.0.1';

require_once __DIR__ . '/main/views/header.php';
if (!in_array($ipActual, $ipsBloqueadas)) {
    session_start();
    $page = 'login';
    if (isset($_SESSION["index"])) {
        if ($_SESSION["index"]->locked) {
            $page = 'main';
        } else {
            unset($_SESSION["index"]);
        }
    }
    $ruta = __DIR__ . '/main/views/' . $page . '.php';
    !file_exists($ruta) && $ruta = __DIR__ . '/main/views/404-not-found.php';
}
require_once $ruta;
require_once __DIR__ . '/main/views/footer.php';
